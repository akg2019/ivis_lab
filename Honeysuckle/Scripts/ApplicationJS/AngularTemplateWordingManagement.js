﻿var app = angular.module("TempWordingApp", []);

app.controller("TempWordingController", function ($scope, $http) {

    $http.get("/TemplateWording/GetTemplateWordingsForCompany").then(function (d) {
        $scope.record = d.data.Wordings;
    });

    $scope.Candelete = function (ID) {
        $http.get("/TemplateWording/CheckDelete/" + ID).then(function (d) {
            $scope.can_delete = d.data.CanDelete[0];
            var can_delete = Object.values(d.data.CanDelete[0])[0];

            if (can_delete) {
                $("<div>You are about to delete this wording. Are you sure you want to do this?</div>").dialog({
                    title: 'Alert!',
                    modal: true,
                    buttons: {
                        OK: function () {
                            var dia = $(this);
                            $(dia).dialog('close');
                            $.ajax({
                                url: "/TemplateWording/delete/" + ID
                            });
                            $(dia).dialog('destroy');
                        },
                        Cancel: function () {
                            $(this).dialog("destroy");
                        }
                    }
                });
            }
            else {
                $("<div>This wording can not be deleted</div>").dialog({
                    title: 'Alert!',
                    modal: true,
                    buttons: {
                        OK: function () {
                            var dia = $(this);
                            $(dia).dialog('close');
                            $(dia).dialog('destroy');
                        }
                    }
                });
            }
        });
    }

});