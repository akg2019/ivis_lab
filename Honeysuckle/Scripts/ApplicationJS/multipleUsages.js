﻿$(function () {

    $("#addusage").on('click', function () {

        var odd = '<div class="usages row data odd">';
        var even = '<div class="usages row data even">';
        var id = 0;

        $("<div></div>").load("/Usage/Create/").dialog({
            modal: true,
            title: "New Usage",
            buttons: {
                Save: function () {

                    var usageString = $(".usage-field").val();
                    var cid = $("#coverid").val();
                    var certType = $(".certcode-field").val();

                    if ((usageString || usageString != undefined || usageString != "" || usageString.length > 0) && (certType || certType != undefined || certType != "" || certType.length > 0)) {

                    $.ajax({
                        url: '/PolicyCover/SaveUsage/?usage=' + usageString + '&id=' + cid + '&CertType=' + certType,
                        cache: false,
                        dataType: "json",
                        async: false,
                        success: function (data) {
                            if (data.result)
                                id = data.usageID;
                            $.ajax({
                                url: '/Usage/Details/?id=' + data.id + '&cid=' + $("#coverid").val(),
                                cache: false,
                                dataType: "html",
                                async: false,
                                success: function (data) {
                                    if ($(".usages.row:last").hasClass('even')) $(".table-data-usages").append(odd + data + " </div>");
                                    else $(".table-data-usages").append(even + data + " </div>");
                                }

                            });
                            $(".usages.row:last").find(".usage-id").val(id);
                            $(".usages.row:last").find(".item.usages").html(usageString);
                        }
                    });

                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");

                    }
                },
                Cancel: function () {
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                }
            }

        });
    });

});