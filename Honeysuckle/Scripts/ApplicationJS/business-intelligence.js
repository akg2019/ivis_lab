﻿/* POTENTIAL BAR CHARTS */
var errorChart;
var inquryChart;
var certChart;
var coverChart;
var enquiryChart;

/* POTENTIAL GAUGES */
var inquryGauge;
var errorGauge;
var certGauge;
var cnoteGauge;
var doubleInsuredGauge;

/* STATIC VARIABLES OF COMPANIES DRAWN DOWN */
var companies;

function CompObj(id, name){
    this.id = id;
    this.name = name;
}

function nextInterval(interval, end) {
    return end + interval - (end % interval);
}

function create_gauges() {

    var red; var yellow; var green; var data; var end;

    if ($("#enquiryGauge")) {
        red = $(".enquiry.data.red").val();
        yellow = $(".enquiry.data.yellow").val();
        green = $(".enquiry.data.green").val();
        data = parseInt($(".enquiry.count").val());
        end = (data > nextInterval(10, parseInt(red))) ? nextInterval(10, data) : nextInterval(10, parseInt(red));

        if( (red !=0 && red != undefined) && ( yellow !=0 && yellow != undefined ) && ( green != 0 && green != undefined ) ){
        //if (red != 0 && yellow != 0 && green != 0) {

            inquryGauge = AmCharts.makeChart("enquiryGauge", {
                "type": "gauge",
                "theme": "none",
                "axes": [{
                    "axisThickness": 1,
                    "axisAlpha": 0.2,
                    "tickAlpha": 0.2,
                    "valueInterval": (end / 10),
                    "bands": [{
                        "color": "#0b8253",
                        "endValue": parseInt(green),
                        "startValue": 0
                    }, {
                        "color": "#da5400",
                        "endValue": parseInt(yellow),
                        "startValue": parseInt(green)
                    }, {
                        "color": "#f60000",
                        "endValue": end,
                        "innerRadius": "90%",
                        "startValue": parseInt(yellow)
                    }],
                    "bottomText": data + " Inquiries",
                    "bottomTextYOffset": 0,
                    "endValue": end
                }],
                "arrows": [{ "value": data}]
            });

        }
        else $("#enquiryGauge").html("Please go to the Business Intelligence Setup page in order to see this gauge");
    }

    if ($("#errorGauge")) {

        red = $(".error.data.red").val();
        yellow = $(".error.data.yellow").val();
        green = $(".error.data.green").val();
        data = parseInt($(".error.count").val());
        end = (data > nextInterval(10, parseInt(red))) ? nextInterval(10, data) : nextInterval(10, parseInt(red));

        if( (red !=0 && red != undefined) && ( yellow !=0 && yellow != undefined ) && ( green != 0 && green != undefined ) ){
            errorGauge = AmCharts.makeChart("errorGauge", {
                "type": "gauge",
                "theme": "none",
                "axes": [{
                    "axisThickness": 1,
                    "axisAlpha": 0.2,
                    "tickAlpha": 0.2,
                    "valueInterval": (end / 10),
                    "bands": [{
                        "color": "#0b8253",
                        "endValue": parseInt(green),
                        "startValue": 0
                    }, {
                        "color": "#da5400",
                        "endValue": parseInt(yellow),
                        "startValue": parseInt(green)
                    }, {
                        "color": "#f60000",
                        "endValue": end,
                        "innerRadius": "90%",
                        "startValue": parseInt(yellow)
                    }],
                    "bottomText": data + " Errors",
                    "bottomTextYOffset": 0,
                    "endValue": end
                }],
                "arrows": [{ "value": data}]
            });
        }
        else $("#errorGauge").html("Please go to the Business Intelligence Setup page in order to see this gauge");
    }

    if ($("#cnoteGauge")) {
        red = $(".covernote.data.red").val();
        yellow = $(".covernote.data.yellow").val();
        green = $(".covernote.data.green").val();
        data = parseInt($(".covernote.count").val());
        end = (data > nextInterval(10, parseInt(red))) ? nextInterval(10, data) : nextInterval(10, parseInt(red));

        if( (red !=0 && red != undefined) && ( yellow !=0 && yellow != undefined ) && ( green != 0 && green != undefined ) ){
            cnoteGauge = AmCharts.makeChart("cnoteGauge", {
                "type": "gauge",
                "theme": "none",
                "axes": [{
                    "axisThickness": 1,
                    "axisAlpha": 0.2,
                    "tickAlpha": 0.2,
                    "valueInterval": (end / 10),
                    "bands": [{
                        "color": "#0b8253",
                        "endValue": parseInt(green),
                        "startValue": 0
                    }, {
                        "color": "#da5400",
                        "endValue": parseInt(yellow),
                        "startValue": parseInt(green)
                    }, {
                        "color": "#f60000",
                        "endValue": end,
                        "innerRadius": "90%",
                        "startValue": parseInt(yellow)
                    }],
                    "bottomText": data + " Cover Notes",
                    "bottomTextYOffset": 0,
                    "endValue": end
                }],
                "arrows": [{ "value": data}]
            });
        }
        else $("#cnoteGauge").html("Please go to the Business Intelligence Setup page in order to see this gauge");
    }

    if ($("#certGauge")) {
        red = $(".certificate.data.red").val();
        yellow = $(".certificate.data.yellow").val();
        green = $(".certificate.data.green").val();
        data = parseInt($(".certificate.count").val());

        end = (data > nextInterval(10, parseInt(red))) ? nextInterval(10, data) : nextInterval(10, parseInt(red));

        if( (red !=0 && red != undefined) && ( yellow !=0 && yellow != undefined ) && ( green != 0 && green != undefined ) ){
            certGauge = AmCharts.makeChart("certGauge", {
                "type": "gauge",
                "theme": "none",
                "axes": [{
                    "axisThickness": 1,
                    "axisAlpha": 0.2,
                    "tickAlpha": 0.2,
                    "valueInterval": (end / 10),
                    "bands": [{
                        "color": "#0b8253",
                        "endValue": parseInt(green),
                        "startValue": 0
                    }, {
                        "color": "#da5400",
                        "endValue": parseInt(yellow),
                        "startValue": parseInt(green)
                    }, {
                        "color": "#f60000",
                        "endValue": end,
                        "innerRadius": "90%",
                        "startValue": parseInt(yellow)
                    }],
                    "bottomText": data + " Certificates",
                    "bottomTextYOffset": 0,
                    "endValue": end
                }],
                "arrows": [{ "value": data}]
            });
        }
        else $("#certGauge").html("Please go to the Business Intelligence Setup page in order to see this gauge");
    }

    if ($("#doubleInsuredGauge")) {
        red = $(".doubleInsured.data.red").val();
        yellow = $(".doubleInsured.data.yellow").val();
        green = $(".doubleInsured.data.green").val();
        data = parseInt($(".doubleInsured.count").val());
        end = (data > nextInterval(10, parseInt(red))) ? nextInterval(10, data) : nextInterval(10, parseInt(red));


        if( (red !=0 && red != undefined) && ( yellow !=0 && yellow != undefined ) && ( green != 0 && green != undefined ) ){

            doubleInsuredGauge = AmCharts.makeChart("doubleInsuredGauge", {
                "type": "gauge",
                "theme": "none",
                "axes": [{
                    "axisThickness": 1,
                    "axisAlpha": 0.2,
                    "tickAlpha": 0.2,
                    "valueInterval": "4k",
                    "bands": [{
                        "color": "#0b8253",
                        "endValue": parseInt(green),
                        "startValue": 0
                    }, {
                        "color": "#da5400",
                        "endValue": parseInt(yellow),
                        "startValue": parseInt(green)
                    }, {
                        "color": "#f60000",
                        "endValue": end,
                        "innerRadius": "90%",
                        "startValue": parseInt(yellow)
                    }],
                    "bottomText": data + " Vehicles",
                    "bottomTextYOffset": 0,
                    "endValue": end
                }],
                "arrows": [{ "value": data}]
            });

        }
        else $("#doubleInsuredGauge").html("Please go to the Business Intelligence Setup page in order to see this gauge");
    }

    if ($("#inter-cover-count")) {
        //run_chart_serial();
        get_chart_data();
    }

    if ($("#enquiry-count")) {
        get_enquiry_chart_data();
    }
}

function get_start_datetime(type, initial) {
    var start;
    if(type == "covercount"){
            if(initial != undefined && initial)
            {
                var s = $(".datepicker.covercount.start").val();
                s = (s.substr(s.indexOf(" ")+1) + "/" + s.substr(0,s.indexOf(" "))).replace(" ","/");
                start = new Date(s);
            }
            else
            {
                var s = $(".datepicker.covercount.start").val();
                var sa = s.split(" ");
                var s = changeWordDate(sa[1]) + "/" + sa[0] + "/" + sa[2];
                start = new Date(s);
            }
        }
    else {
        if(initial != undefined && initial)
            {
                var s = $(".datepicker.enquiry.start").val();
                s = (s.substr(s.indexOf(" ")+1) + "/" + s.substr(0,s.indexOf(" "))).replace(" ","/");
               start = new Date(s);
           }
           else
            {
                var s = $(".datepicker.enquiry.start").val();
                var sa = s.split(" ");
                var s = changeWordDate(sa[1]) + "/" + sa[0] + "/" + sa[2];
                start = new Date(s);
            }
       }
    return start;
}

function get_end_datetime(type, initial) {
    var end;
    if(type== "covercount"){
            if(initial != undefined && initial)
            {
                var s = $(".datepicker.covercount.end").val();
                s = (s.substr(s.indexOf(" ")+1) + "/" + s.substr(0,s.indexOf(" "))).replace(" ","/");
                end = new Date(s);
            }
           else
            {
                var s = $(".datepicker.covercount.end").val();
                var sa = s.split(" ");
                var s = changeWordDate(sa[1]) + "/" + sa[0] + "/" + sa[2];
                end = new Date(s);
            }
        }
    else{
            if(initial != undefined && initial)
            {
                var s = $(".datepicker.enquiry.end").val();
                s = (s.substr(s.indexOf(" ")+1) + "/" + s.substr(0,s.indexOf(" "))).replace(" ","/");
                end = new Date(s);
            }
           else
            {
                var s = $(".datepicker.enquiry.end").val();
                var sa = s.split(" ");
                var s = changeWordDate(sa[1]) + "/" + sa[0] + "/" + sa[2];
                end = new Date(s);
            }
        }
    return end;
}  

function changeWordDate(word)
{
    var n = 1;

    switch(word)
    {
        case "Jan": n = 1; break;
        case "Feb": n = 2; break;
        case "Mar": n = 3; break;
        case "Apr": n = 4; break;
        case "May": n = 5; break;
        case "Jun": n = 6; break;
        case "Jul": n = 7; break;
        case "Aug": n = 8; break;
        case "Sep": n = 9; break;
        case "Oct": n = 10; break;
        case "Nov": n = 11; break;
        case "Dec": n = 12; break;

    }

    return n;
}

function get_chart_data() {
   if($(".datepicker.covercount.start").length > 0){
        $(".datepicker.covercount.start").datetimepicker({
            format: 'd M Y',
            value: get_start_datetime("covercount",true).dateFormat('d M Y'),
            lang: 'en-GB',
            validateOnBlur: false,
            timepicker: false,
            closeOnDateSelect: true,
            closeOnWithoutSelect: true,
            onShow: function () {
                this.setOptions({
                    maxDate: get_end_datetime("covercount")
                });
            }
        });
    }
    if($(".datepicker.covercount.end").length > 0){
        $(".datepicker.covercount.end").datetimepicker({
            format: 'd M Y',
            lang: 'en-GB',
            value: get_end_datetime("covercount",true).dateFormat('d M Y'),
            validateOnBlur: false,
            timepicker: false,
            closeOnDateSelect: true,
            closeOnWithoutSelect: true,
            onShow: function () {
                this.setOptions({
                    minDate: get_start_datetime("covercount")
                });
            }
        });
    }
    
    var chardata = [];
    $.ajax({
        url: '/KPI/KPIData/',
        type: 'POST',
        data: { type : 'cover' },
        cache: false,
        success: function (json) {
            if (json.result) {
                var kpis = json.kpi;
                companies = [];
                for (var i = 0; i < kpis.length; i++) {
                    var comp_obj = CompObj(kpis[i].company.CompanyID, kpis[i].company.CompanyName);
                    companies.push(comp_obj);
                    chardata.push({ "company": kpis[i].company.CompanyName, "full_count": kpis[i].count, "cover_count": kpis[i].covercount, "certcount": kpis[i].certcount });
                }
            }
            run_chart_serial(chardata);
        }
    });
}


function update_chart_data() {
    var chardata = [];
    var start = get_start_datetime("covercount");
    var end = get_end_datetime("covercount");
    var ins = ($(".ins_brk").length > 0)? $(".ins_brk").val() : '';
    var imp = $(".import").is(":checked");
    var params = {  start : start.dateFormat('Y-m-d'), 
                    end : end.dateFormat('Y-m-d'),
                    ins : ins,
                    import: imp,
                    type: 'cover'
                 };
    if (start <= end){
        $.ajax({
            url: '/KPI/KPIData/',
            type : "POST",
            data: params,
            cache: false,
            success: function (json) {
                if (json.result) {
                    var kpis = json.kpi;
                    companies = [];
                    for (var i = 0; i < kpis.length; i++) {
                        var comp_obj = CompObj(kpis[i].company.CompanyID, kpis[i].company.CompanyName);
                        companies.push(comp_obj);
                        chardata.push({ "company": kpis[i].company.CompanyName, "full_count": kpis[i].count, "cover_count": kpis[i].covercount, "certcount": kpis[i].certcount });
                    }
                }
                update_chart(coverChart,chardata);
            }
        });
    }
    else {
        $("<div></div>").text("Please select a start date that is before the end date before updating the chart.").dialog({
            title:'Warning',
            modal:true,
            buttons: {
                Ok: function(){
                    $(this).dialog('close');
                }
            }
        });
    }
}


function update_enquiry_chart_data() {
    var chardata = [];
    var start = get_start_datetime("enquiry");
    var end = get_end_datetime("enquiry");
    var ins = ($(".ins_brk_enq").length > 0)? $(".ins_brk_enq").val(): '';
    if (start <= end){
        $.ajax({
            url: '/KPI/KPIData/',
            type: 'POST',
            data: { type : 'enquiry', start : start.dateFormat('Y-m-d'), end : end.dateFormat('Y-m-d'), ins: ins },
            cache: false,
            success: function (json) {
                if (json.result) {
                    var kpis = json.kpi;
                    for (var i = 0; i < kpis.length; i++) {
                         chardata.push({ "company": kpis[i].company.CompanyName, "count": kpis[i].count });
                    }
                }
                update_chart(enquiryChart,chardata);
            }
        });
    }
    else {
        $("<div></div>").text("Please select a start date that is before the end date before updating the chart.").dialog({
            title:'Warning',
            modal:true,
            buttons: {
                Ok: function(){
                    $(this).dialog('close');
                }
            }
        });
    }
}

function update_chart(chart,data){
    chart.dataProvider = data;
    chart.validateData();
    chart.animateAgain();
}

function run_chart_serial(data) {

    var chardata = data;
    
    coverChart = AmCharts.makeChart("inter-cover-count", {
        type: "serial",
        dataProvider: chardata,

        addClassNames: true,
        startDuration: 1,
        marginLeft: 0,

        categoryField: "company",
        categoryAxis: {
            autoGridCount: false,
            gridCount: 50,
            gridAlpha: 0.1,
            gridColor: "#FFFFFF",
            axisColor: "#555555",
            labelRotation:40,
        },
        valueAxes: [
            {
                id: "a1",
                title: "Number of Covers",
                precision: 0
            }
        ],
        graphs: [
            {
                id: "g1",
                valueField:  "full_count",
                type:  "column",
                fillAlphas:  0.75,
                valueAxis:  "a1",
                balloonText:  "[[value]] Covers",
                lineColor:  "#008080",
                color:  "#008080",
                alphaField:  "alpha",
                columnWidth:0.25,
                title: "Covers",
                borderColor: "Black",
            },
            {
                id: "g2",
                valueField: "cover_count",
                classNameField: "bulletClass",
                title: "Cover Notes",
                type: "line",
                valueAxis: "a2",
                lineColor: "#786c56",
                lineThickness: 1,
                legendValueText: "[[description]]/[[value]]",
                descriptionField: "townName",
                bullet: "round",
                bulletSizeField: "townSize",
                bulletBorderColor: "#786c56",
                bulletBorderAlpha: 1,
                bulletBorderThickness: 2,
                bulletColor: "#000000",
                labelText: "[[townName2]]",
                labelPosition: "right",
                balloonText: "Cover Notes:[[value]]",
                showBalloon: true,
                animationPlayed: true,
            },
            {
                id: "g3",
                title: "Certificates",
                valueField: "certcount",
                type: "line",
                valueAxis: "a3",
                lineColor: "#ff5755",
                balloonText: "[[value]]",
                lineThickness: 1,
                legendValueText: "[[value]]",
                bullet: "square",
                bulletBorderColor: "#ff5755",
                bulletBorderThickness: 1,
                bulletBorderAlpha: 1,
                dashLengthField: "dashLength",
                animationPlayed: true,
                balloonText: "Certificates:[[value]]",
            }
        ],
        legend: {
            bulletType: "round",
            equalWidths: true,
            useGraphSettings: true,
        }
    });
    /*
    coverChart.addListener('clickGraphItem', function (event){
        var start = get_start_datetime();
        var comp_id = companies[event.item.index];

    });
    */
}

function get_enquiry_chart_data() {

    if($(".datepicker.enquiry.start").length > 0){
        $(".datepicker.enquiry.start").datetimepicker({
            format: 'd M Y',
            value: get_start_datetime("enquiry",true).dateFormat('d M Y'),
            lang: 'en-GB',
            validateOnBlur: false,
            timepicker: false,
            closeOnDateSelect: true,
            closeOnWithoutSelect: true,
            onShow: function () {
                this.setOptions({
                    maxDate: get_end_datetime("enquiry")
                });
            }
        });
    }

    if($(".datepicker.enquiry.end").length > 0){
        $(".datepicker.enquiry.end").datetimepicker({
            format: 'd M Y',
            lang: 'en-GB',
            value: get_end_datetime("enquiry",true).dateFormat('d M Y'),
            validateOnBlur: false,
            timepicker: false,
            closeOnDateSelect: true,
            closeOnWithoutSelect: true,
            onShow: function () {
                this.setOptions({
                    minDate: get_start_datetime("enquiry")
                });
            }
        });
    }

    var chardata = [];
    $.ajax({
        url: '/KPI/KPIData/',
        type: 'post',
        data: { type: 'enquiry' },
        cache: false,
        success: function (json) {
            if (json.result) {
                var kpis = json.kpi;
                for (var i = 0; i < kpis.length; i++) {
                    chardata.push({ "company": kpis[i].company.CompanyName, "count": kpis[i].count });
                }
            }
            run_enquiry_chart(chardata);
        }
    });

}

function run_enquiry_chart(data) {

    var chardata = data;
    
    enquiryChart = AmCharts.makeChart("enquiry-count", {
        type: "serial",
        dataProvider: chardata,

        addClassNames: true,
        startDuration: 1,
        marginLeft: 0,

        categoryField: "company",
        categoryAxis: {
            autoGridCount: false,
            gridCount: 50,
            gridAlpha: 0.1,
            labelRotation:40,
            gridColor: "#FFFFFF",
            axisColor: "#555555",
            fontSize: 10
        },
        valueAxes: [
            {
                id: "y-count",
                title: "Number of Enquiries",
                precision: 0

            }
        ],
        graphs: [
            {
                id: "column-graph",
                valueField:  "count",
                type:  "column",
                fillAlphas:  0.5,
                valueAxis:  "y-count",
                balloonText:  "[[value]] enquiries",
                lineColor:  "#0071bc",
                alphaField:  "alpha",
                columnWidth:0.25,
                title: "Enquiries",                
            },
            
        ]
    });
}

$(function(){
    $("html").on('change', '.ins_brk', function(){
        update_chart_data();
    });
    $("html").on('change', '.ins_brk_enq', function(){
        update_enquiry_chart_data();
    });
    $("html").on("change", '.import', function(){
        update_chart_data();
    });
});
