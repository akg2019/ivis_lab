﻿$( function() {

function GetAutoCompleteData(){
      $.ajax({
            type: "POST",
            url: "/Usage/GetUsageCertTypesListDropDownData",
            cache: false,
            success: function (response) {
             let usage = JSON.parse(response).usage
             let cert = JSON.parse(response).cert
             
                //Usage Drop Down Autocomplete
                $( "._uDropdown" ).autocomplete({
                  source: usage
                });

               
                //Certs Drop Down Autocomplete
                $( "._certDropDown" ).autocomplete({
                  source: cert
                });
            },
            error: function (response) {
               
            }

        });
        
 }

 GetAutoCompleteData();


  } );