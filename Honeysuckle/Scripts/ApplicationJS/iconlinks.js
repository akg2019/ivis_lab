﻿$(function () {

    // THIS IS WHERE WE LAYOUT THE CSS TAGS THATE ARE PRESSED
    // IN ORDER TO AFFECT FLOW WITH LINKS AND ICONS

    $('body').on('click', '.covercertnote', function () {
        var id = $(this).find("#note-covercert-id").val();
        var page = $(this).find("#note-covercert").val();
        View(id, page);
    });

    $('body').on('click', '.view', function () {
        var id = $(this).parents(".row").find(".id").val();
        var page = getPage($(this));
        View(id, page);
    });

    $('body').on('click', '.edit', function () {
        var id = $(this).parents(".row").find(".id").val();
        var UCID = $(this).parents(".row").find(".certUCID").val();
       
        var page = getPage($(this));

        if (id === undefined && page == 'usages') var id = $(this).parents(".row").find(".usage-id").val();

        Edit(id, page,UCID);
    });

    $('body').on('click', '.viewlog', function () {
        var id = $(this).parents(".row").find(".id").val();
       // var page = getPage($(this));
        ViewLog(id);
    });

    $('body').on('click', '.delete', function () {
        var page = getPage($(this));

        var id = 0; var e;
        var func = true;
        switch (page) {
            case "usages":
                e = $(this).parents(".usages.row");
                id = $(this).parents(".usages.row").find(".usage-id").val();
                break;
            case "usages-manage":
                func = false;
                var $e = $(this);
                $("<div>Do you want to delete this usage?</div>").dialog({
                    title: 'Delete Usage',
                    cache: false,
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $($e).parents(".usage-row").remove();
                            $(this).dialog('close');
                        },
                        Cancel: function () {
                            $(this).dialog('close');
                        }

                    }
                });

                break;
            case "template_image":
                func = false;
                e = $(this).parents(".image");
                id = $(this).parents(".image").find(".image_id").val();
                DeleteImage(e, id);
                break;
            case "intermediary":
                id = $(this).parents(".row").find(".id").val();
                e = $(this).parents(".row");
                break;
            default:
                e = $(this).parents(".row");
                id = $(this).parents(".row").find(".id").val();

        }
        if (func) Delete(id, page, e);
    });

    $('body').on('click', '.resetpass', function () {
        var id = $(this).parents(".row").find(".id").val();
        ResendPassword(id);
    });

    $('body').on('click', '.resetquest', function () {
        var id = $(this).parents(".row").find(".id").val();
        ResendQuestion(id);
    });

      $('body').on('click', '.resetaccess', function () {
        var id = $(this).parents(".row").find(".id").val();
        ResetAccess(id);
    });


    $('body').on('click', '.disable', function () {
        var id = $(this).parents(".row").find(".id").val();
        var e = $(this).parents(".row");
        var page = getPage($(this));
        Disable(id, page, e);
    });

    $('body').on('click', '.enable', function () {
        var id = $(this).parents(".row").find(".id").val();
        var e = $(this).parents(".row");
        var page = getPage($(this));
        Enable(id, page, e);
    });

    $('body').on('click', '.purge', function () {
        var id = $(this).parents(".row").find(".id").val();
        var e = $(this).parents(".row");
        var page = getPage($(this));
        Purge(id, page, e);
    });

    $('body').on('click', '.assign', function () {
        var id = $(this).parents(".row").find(".id").val();
        var e = $(this).parents(".row");
        var page = getPage($(this));
        AssignToUsers(id, page, e);
    });

    $('body').on('click', '.takerole', function () {
        var id = $(this).parents(".row").find(".id").val();
        var e = $(this).parents(".row");
        var page = getPage($(this));
        TakeRole(id, page, e);
    });

    $('html, body').on('click', '.manageusages', function () {
        var id = $(this).parents(".row").find(".id").val();
        Usages(id);
    });

    $('html, body').on('click', '.assignrole', function () {
        var id = $(this).parents(".row").find(".id").val();
        AssignRoles(id);
    });

    $('body').on('click', '.vehicles', function () {
        var id = $(this).parents(".row").find(".id").val();
        Vehicles(id);
    });

    $('body').on('click', '.current-policy', function () {
        var id = $(this).parents(".row").find(".id").val();
        current_policy(id);
    });

    $('body').on('click', '.current-cover', function () {
        var id = $(this).parents(".row").find(".id").val();
        current_cover(id);
    });

    $('body').on('click', '.current-cover-history', function () {
        var id = $(this).parents(".row").find(".id").val();
        var polID = $('.id').parents(".policy").find(".id").val();
        current_cover_history(id,polID);
    });

    $('body').on('click', '.risk', function () {
        var id = $(this).parents(".row").find(".risk_id").val();
        View(id, "vehicle");
    });

    $('body').on('click', '.icon.policy', function () {
        var id = $(this).parents(".row").find(".policy_id").val();
        View(id, "policy");
    });

    $('body').on('click', '.policies', function () {
        var id = $(this).parents(".row").find(".base_id").val();
        View_Policies(id);
    });

    var allcovercerts = $(".covercert.row");
    var certRows = $(".CertHidtype").parent();
    var coverRows = $(".CoverHidtype").parent();

    $('html, body').on('click', '#filter-id', function () {

        var checked = $("input[name='filter']:checked").val();


        switch (checked) {
            case "Both":
                //rePrintcovercertDivs(allcovercerts);
                $('.CoverHidtype').each(function () {
                    $(this).parent().show();
                });

                $('.CertHidtype').each(function () {
                    $(this).parent().show();
                });

                break;

            case "Cover":
                //rePrintcovercertDivs(coverRows);
                $('.CertHidtype').each(function () {
                    $(this).parent().hide();
                });
                $('.CoverHidtype').each(function () {
                    $(this).parent().show();
                });

                break;

            case "Certificate":
                //rePrintcovercertDivs(certRows);
                $('.CoverHidtype').each(function () {
                    $(this).parent().hide();
                });
                $('.CertHidtype').each(function () {
                    $(this).parent().show();
                });

                break;
            default:
                $(this).parents(".row").show();
        }
        rePrintcovercertDivs();

    });

    $(".cover_usage").on("click", function () {
        var id = $(this).parents(".row").find(".id").val();
        cover_usage(id);
    });
});

function cover_usage(id) {
    window.location.replace('/TemplateWording/WordingToCoverUsages/' + id);
}

function DeleteImage(e, id) {
    var params = { id: id };
    $.ajax({
        url: '/TemplateWording/JSONImageDelete/',
        type: "POST",
        data: params,
        success: function (json) {
            if (json.result) {
                $(e).parents(".image-parent").find(".fileupload-parent").show();
                $(e).parent(".image-parent").find(".image-div").html("");
                $('<div>The image was successfully removed.</div>').dialog({
                    modal: true,
                    title: 'Image Removed!',
                    buttons: {
                        OK: function () {
                            $(this).dialog('close');
                        }
                    }
                });
            }
            else {
                $(e).append("<div class=\"error\">There was an error. Please refresh the page</div>");
            }
        }
    });
}


function current_policy(id) {
    window.location.replace('/Vehicle/CurrentPolicy/' + id);
}

function current_cover(id) {
    window.location.replace('/Vehicle/CurrentCover/' + id);
}

function current_cover_history(id, polID) {
    //thisll be a modal
    $("<div></div>").load('/VehicleCoverNote/HistoryPartial/?id=' + id + '&historyPage=dialog' + '&polid=' + polID, function (value) {

        $('.history-table').addClass('dialog-history-table');
        $('.covers-history').removeClass('main');

        $(".covers-history").perfectScrollbar({
            wheelSpeed: 0.3,
            wheelPropagation: true,
            minScrollbarLength: 10,
            suppressScrollX: true
        });


    }).dialog({
        modal: true,
        title: 'Cover Note History',
        width: 630,
        height: 500,
        buttons: [
            {
                text: "Close",
                click: function () {
                    $(this).dialog('close');
                }
            }
        ]
    });

    

}

function Vehicles(id) {
    
    $("<div></div>").load('/Policy/Vehicles/' + id).dialog({
        title: 'Vehicles Under Policy',
        modal: true,
        width: 550,
        height: 400,
        buttons: [
            {
                text: "Go To Policy",
                click: function () {
                    window.location.replace('/Policy/Details/' + id);
                }
            },
            {
                text: "Close",
                click: function () {
                    $(this).dialog('close');
                }
            }
        ]
    });

}

function AssignRoles(id) {
    window.location.replace('/User/AddUserRoles/' + id);
}

function TakeRole(id, page, e) {
    switch (page) {
        case 'usergroup':
            window.location.replace('/RoleManagement/TempRole/' + id);
            break;
        default:
            errorModal();
            break;
    }
}

function AssignToUsers(id, page, e) {
    switch (page) {
        case 'usergroup':
            window.location.replace('/RoleManagement/AssignToUsers/' + id);
            break;
        default:
            break;
    }
}

function Purge(id, page, e) {
    switch (page) {
        case 'usergroup':
            $.ajax({
                url: '/RoleManagement/Purge/' + id,
                cache: false,
                success: function (data) {
                    if (data.result) {
                        $("<div>" + data.mess + "</div>").dialog({
                            title: 'Success',
                            modal:true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                    else {
                        errorModal();
                    }
                },
                error: function () {
                    errorModal();
                }
            });
        default:
            break;
    }
}

function Usages(id) {
    window.location.replace('/PolicyCover/AssociateUsages/' + id);
}

function Disable(id, page, e){

    var activebtn = "<div class=\"icon enable\" title=\"Enable " + page.charAt(0).toUpperCase() + page.slice(1) + "\"> <img src=\"../../Content/more_images/enable_icon.png\" class=\"sprite\"/> </div> ";
    var promptAdd = "";
    var IAJcreated = false;
    if (page == "user") IAJcreated = $(e).find('.isIAJcreated').val() == "True";
    if (IAJcreated) promptAdd = "The user was created by an IAJ administrator and is a company administrator.";

    $("<div>You are about to disable this " + page.charAt(0).toUpperCase() + page.slice(1) + ". " + promptAdd + " Are you sure you want to do this? </div>").dialog({
        title: 'Disable!',
        modal: true,
        buttons: {
            OK: function () {

                var dia = $(this);

                var url = "";
                switch (page) {
                    case "user":
                        url = '/User/DeactivateUser/' + id;
                        break;
                    case "company":
                        url = '/Company/Disable/' + id;
                        break;
                    case "intermediary":
                        url = '/Intermediary/DisableIntermediary/' + id;
                        break;
                    default:
                        url = "def";
                }

                if (url != "def") {
                    $.getJSON(url, function (data) {
                        $(dia).dialog('close');
                        if (data.result) {
                            $(e).find(".disable.icon").replaceWith(activebtn);
                            $(e).addClass('disabled');
                            $("<div>The " + page.charAt(0).toUpperCase() + page.slice(1) + " has been disabled.</div>").dialog({
                                modal: true,
                                title: "Disabled",
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog("close");
                                        var haveEnabled = false;

                                        $('.row').each(function(){
                                            if(!$(this).hasClass('disabled')){
                                                haveEnabled = true;
                                                return;
                                                }
                                        });

                                        if (IAJcreated && !haveEnabled) {

                                            $("<div>Do you want to create a new administrator for this company now?</div>").dialog({
                                                modal: true,
                                                title: 'Alert!',
                                                buttons: {
                                                    Yes: function () {
                                                        window.location.replace('/User/Create/?UserCompId=' + $('.cid').val());
                                                        $(this).dialog("close");
                                                    },
                                                    No: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        }
                        else {
                            //ERROR
                            $("<div>The " + page.charAt(0).toUpperCase() + page.slice(1) + " could not be disabled.</div>").dialog({
                                title: "Error",
                                modal: true,
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        }
                    });
                }
                else {
                    $(dia).dialog('close');
                    errorModal();
                }
            },

            Cancel: function () {
                $(this).dialog("close");
            }
        }
    });  
}

function Enable(id, page, e) {

    var deactivatebtn = "<div class=\"icon disable\" title=\"Disable " + page.charAt(0).toUpperCase() + page.slice(1) + "\"> <img src=\"../../Content/more_images/disable_icon.png\" class=\"sprite\"/> </div> ";

    var action = "";
    if(page == "covernote" || page == "certificate"){
          action = "approve";
         }
         else{
         action = "enable";
         }

         $("<div>You are about to " + action + " this " + page.charAt(0).toUpperCase() + page.slice(1) + ". Are you sure you want to do this? </div>").dialog({
             title: "Enable",
             modal: true,
             buttons: {
                 OK: function () {
                     var dia = $(this);

                     var url = "";
                     switch (page) {
                         case "user":
                             url = '/User/ActivateUser/' + id;
                             break;
                         case "company":
                             url = '/Company/Enable/' + id;
                             break;
                         case "covernote":
                             url = '/VehicleCoverNote/Approve/' + id;
                             break;
                         case "certificate":
                             url = '/VehicleCertificate/ApproveCert/?certificateId=' + id;
                             break;
                         case "intermediary":
                             url = '/Intermediary/EnableIntermediary/' + id;
                             break;
                         default:
                             url = "def";
                     }




                     if (url != "def") {
                         $.getJSON(url, function (data) {
                             $(dia).dialog('close');
                             if (data.result) {
                                 if (page == "covernote" || page == "certificate") {

                                     $(e).remove();
                                     var covercertID = $(e).find(".id").val();
                                     var covercertType = $(e).find(".CoverCertHidType").val();

                                     $(".covercertnote").each(function () {
                                         if (($(this).find("#note-covercert-id").val() == covercertID) && ($(this).find("#note-hidden-type").val() == covercertType)) {
                                             $(this).remove();
                                         }
                                     });
                                     CoverCertCount();
                                     $("<div>The " + page.charAt(0).toUpperCase() + page.slice(1) + " has been approved. This record will be removed from the pending approval list</div>").dialog({
                                         modal: true,
                                         title: page.charAt(0).toUpperCase() + page.slice(1) + " Approved",
                                         buttons: {
                                             Ok: function () {
                                                 $(this).dialog("close");
                                             }
                                         }
                                     });
                                 }
                                 else {
                                     $(e).find(".enable.icon").replaceWith(deactivatebtn);
                                     $(e).removeClass('disabled');
                                     $("<div>The " + page.charAt(0).toUpperCase() + page.slice(1) + " has been enabled.</div>").dialog({
                                         modal: true,
                                         title: "Enabled!",
                                         buttons: {
                                             Ok: function () {
                                                 $(this).dialog("close");
                                             }
                                         }
                                     });
                                 }
                             }
                             else {
                                 //ERROR
                                 if (page == "user") {
                                     $("<div>The " + page.charAt(0).toUpperCase() + page.slice(1) + " is currently active at another company and cannot be enabled for this one.</div>").dialog({
                                         modal: true,
                                         title: 'Error!',
                                         buttons: {
                                             Ok: function () {
                                                 $(this).dialog("close");
                                             }
                                         }
                                     });
                                 }
                                 else
                                 {
                                     $("<div>The " + page.charAt(0).toUpperCase() + page.slice(1) + " could not be enabled.</div>").dialog({
                                         modal: true,
                                         title: 'Error!',
                                         buttons: {
                                             Ok: function () {
                                                 $(this).dialog("close");
                                             }
                                         }
                                     });
                                 }
                             }
                         });
                     }
                     else {
                         $(dia).dialog('close');
                         errorModal();
                     }
                 },
                 Cancel: function () {
                     $(this).dialog("close");
                 }
             }
         });  
 }


function View(id, page) {
    switch (page) {
        case "user":
            window.location.replace('/User/Details/' + id);
            break;
        case "company":
            window.location.replace('/Company/Details/' + id);
            break;
        case "policy":
            window.location.replace('/Policy/Details/' + id);
            break;
        case "vehicle":
            window.location.replace('/Vehicle/Details/' + id);
            break;
        case "covernote":
            window.location.replace('/VehicleCoverNote/Details/' + id);
            break;
        case "certificate":
            window.location.replace('/VehicleCertificate/Details/' + id);
            break;
        case "policycover":
            window.location.replace('/PolicyCover/Details/' + id);
            break;
        case "wording":
            window.location.replace('/TemplateWording/Details/' + id);
            break;
        case "person":
            window.location.replace('/People/Details/' + id);
            break;
        default:
            errorModal();
            break;
    }
}

function Edit(id, page,uid) {
    switch (page) {
        case "user":
            window.location.replace('/User/Modify/' + id);
            break;
        case "company":
            window.location.replace('/Company/Edit/' + id);
            break;
        case "policy":
            window.location.replace('/Policy/Edit/' + id);
            break;
        case "vehicle":
            window.location.replace('/Vehicle/Edit/' + id);
            break;
        case "covernote":
            window.location.replace('/VehicleCoverNote/Edit/' + id);
            break;
        case "certificate":
            window.location.replace('/VehicleCertificate/Edit/' + id);
            break;
        case "policycover":
            window.location.href = "/PolicyCover/Edit/" + id ;
            break;
        case "usergroup":
            window.location.replace('/RoleManagement/Edit/' + id);
            break;
        case "wording":
            window.location.replace('/TemplateWording/Edit/' + id);
            break;
        case "person":
            window.location.replace('/People/Edit/' + id);
            break;
        case "intermediary":
            window.location.replace('/Intermediary/Edit/' + id);
            break;
        case "usages":
            editUsage(id,uid);
            break;
        case "authword":
            $("<div></div>").load("/AuthorizedDriverWording/New/", function () {
                var word = "";
                $('.row.auth').each(function () {
                    if ($(this).find('.word_id').val() == id) {
                        word = $(this).find('.item.word').text().trim();
                        return false;
                    }
                });
                $(".wording-textarea").val(word);
            }).dialog({
                title: 'Edit Wording',
                modal: true,
                buttons: {
                    Save: function () {
                        var dia = $(this);
                        var wording = $(".wording-textarea").val().trim();
                        if (wording.length > 0) {
                            $.ajax({
                                url: '/AuthorizedDriverWording/UpdateAuthorizedWording/?ID=' + id + '&wording=' + wording,
                                dataType: "json",
                                success: function (data) {
                                    if (data.result) {

                                        $('.row.auth').each(function () {
                                            if ($(this).find('.word_id').val() == id) {
                                                $(this).find('.item.word').text(wording);
                                                return false;
                                            }
                                        });

                                        $("<div>Authorized Wording Saved!</div>").dialog({
                                            title: "Saved!",
                                            modal: true,
                                            buttons: {
                                                OK: function () {
                                                    $(this).dialog('close');
                                                    $(this).dialog('destroy');
                                                    dia.dialog('close');
                                                    dia.dialog('destroy');
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        $("<div>You do not have permissions to carry out this action</div>").dialog({
                                            title: "Alert!",
                                            modal: true,
                                            buttons: {
                                                OK: function () {
                                                    $(this).dialog('close');
                                                    $(this).dialog('destroy');
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                        else {
                            alert("Please enter some text into the dialog.");
                            return false;
                        }

                    },
                    Cancel: function () {
                        $(this).dialog('close');
                        $(this).dialog('destroy');
                    }
                }
            });
            break;
        default:
            errorModal();
            break;
    }
}


function ResetAccess(id){
    var errorrequest = "<div>There was an error with your request. If the problem persists please contact your administrator.</div>";

         
    $("<div>You are about to open the user's account. Are you sure you want to do this? </div>").dialog({
        title: 'Alert!',
        modal:true,
        buttons: {
           OK: function () {
               $(this).dialog('close');
               $(this).dialog('destroy');

            $.ajax({
                url: '/User/ResetAccess/' + id,
                cache: false,
                success: function (data) {
                    if (data.result) {
                        $("<div>The user can now login.</div>").dialog({
                            title: 'Success',
                            modal:true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                    else {
                        $(errorrequest).dialog({
                            title: 'Error',
                            modal:true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                },
                error: function () {
                    $(errorrequest).dialog({
                        title: 'Error',
                        modal:true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog('close');
                            }
                        }
                     });
                  }
               });
             },

          Cancel: function () {
            $(this).dialog("close");
            $(this).dialog().dialog("destroy");
              }
            }
        });   
}


function ResendQuestion(id){
    var errorrequest = "<div>There was an error with your request. If the problem persists please contact your administrator.</div>";

         
    $("<div>You are about to reset this user's questions. Are you sure you want to do this? </div>").dialog({
        title: 'Alert!',
        modal:true,
        buttons: {
           OK: function () {
               $(this).dialog('close');
               $(this).dialog('destroy');

            $.ajax({
                url: '/User/ResetQuestions/' + id,
                cache: false,
                success: function (data) {
                    if (data.result) {
                        $("<div>The user's security question has been reset.</div>").dialog({
                            title: 'Success',
                            modal:true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                    else {
                        $(errorrequest).dialog({
                            title: 'Error',
                            modal:true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                },
                error: function () {
                    $(errorrequest).dialog({
                        title: 'Error',
                        modal:true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog('close');
                            }
                        }
                     });
                  }
               });
             },

          Cancel: function () {
            $(this).dialog("close");
            $(this).dialog().dialog("destroy");
              }
            }
        });   
}

function ResendPassword(id) {
    var errorrequest = "<div>There was an error with your request. If the problem persists please contact your administrator.</div>";

    $("<div>You are about to resend this user's password to the user. Are you sure you want to do this? </div>").dialog({
        title: 'Alert!',
        modal: true,
        buttons: {
            OK: function () {

                $(this).dialog('close');
                $(this).dialog('destroy');
                StartSpinner();
                $.ajax({
                    url: '/User/ResendPassword/' + id,
                    cache: false,
                    success: function (data) {
                        StopSpinner();
                        if (data.result) {
                            if (data.mailSent) {
                                $("<div>A new password has been sent to the user.</div>").dialog({
                                    title: 'Success',
                                    modal: true,
                                    buttons: {
                                        Ok: function () {
                                            $(this).dialog('close');
                                        }
                                    }
                                });
                            }
                            else {

                                var tryAgain = false;

                                $("<div>Unfortuantely the password cannot be reset at this time. There was an error in reaching the users via email</div>").dialog({
                                    title: 'Error',
                                    modal: true,
                                    buttons: {
                                        'Try Again': function () {

                                            tryAgain = true;
                                            PasswordNotSent(id);
                                            $(this).dialog('close');


                                        },
                                        Cancel: function () {
                                            tryAgain = false;
                                            $(this).dialog('close');
                                        }
                                    }
                                });



                            }
                        }
                        else {
                            $(errorrequest).dialog({
                                title: 'Error',
                                modal: true,
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                    },
                    error: function () {
                        StopSpinner();
                        $(errorrequest).dialog({
                            title: 'Error',
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                });
                return false;
                
            },

            Cancel: function () {
                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
            }
        }
    });   
}

function Delete(id, page, e) {
    var url = "def";
    switch (page) {
        case "user":
            url = '/User/Delete/' + id;
            break;
        case "usages":
            url = '/Usage/Delete/' + id + '?cid=' + $("#coverid").val() + '&UCID=' + $(".ucid-field").html();
            break;
        case "policycover":
            url = '/PolicyCover/Delete/' + id;
            break;
        case "wording":
            url = '/TemplateWording/Delete/' + id;
            break;
        case "usergroup":
            url= '/RoleManagement/Delete/' + id;
            break;
        case "intermediary":
            //url = "/Intermediary/RemoveIntermediary/" + $(e).find(".hidcid").val() + "?cid=" + $("#mycompid").val();
            url = "/Intermediary/RemoveIntermediary/" + id;
            break;
        case "company":
            url = "/Company/Delete/" + id;
            break;
        case "authword":
            url = '/AuthorizedDriverWording/Delete/' + id;
            break;
        default:
            errorModal();
            break;
    }

    if (id == 0) {
        $(e).remove();
        return false;
    }

    if (page == "authword") page = "Authorized Driver Wording";
    $("<div>You are about to delete this " + page + ". Are you sure you want to do this?</div>").dialog({
        title: 'Alert!',
        modal: true,
        buttons: {
            OK: function () {
                var dia = $(this);
                $(dia).dialog('close');
                $.ajax({
                    url: url,
                    success: function (data) {
                        if (data.result) {
                            successDelete(page);
                            $(e).remove();
                            var inters = $(".inters.row");
                            if (page == "intermediary") {
                                rePrintInters(inters);
                            }

                            if (page == "usages") {
                                var usageRows = $(".usages.row");
                                rePrintUsages(usageRows);
                            }
                        }
                        else if (page == 'usergroup' && data.assigned) {
                            $('<div>You cannot delete this role now because it is currently assigned to users</div>').dialog({
                                modal: true,
                                title: 'Alert!',
                                buttons: {
                                    OK: function () {
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                        else {
                            errorModal(page);
                        }
                        return true;
                    }
                });
                $(dia).dialog('destroy');
            },
            Cancel: function () {
                $(this).dialog("destroy");
            }
        }
    });
}

function View_Policies(id) {
    $("<div></div>").load('/Policy/PoliciesToVehicle/?base_id=' + id + "&compid=" + $(".compID").val()).dialog({
        title: "Policies",
        modal: true,
        width: 550,
        height: 400,
        buttons: {
            Cancel: function () {
                $(this).dialog("destroy");
            }
        }

    });
}

function errorModal(page = "") 
{
    var Message = "<div>There was a problem. Please contact your administrator</div>";
    if(page == "usages") Message = "<div>This usage is currently being used by existing policies and therefore cannot be deleted.</div>";
    $(Message).dialog({
        title: 'Error',
        modal: true,
        buttons: {
            Ok: function () {
                $(this).dialog('destroy');
            }
        }
    });
}

function successDelete(page) {
    $("<div>The " + page + " was successfully deleted.</div>").dialog({
        title: 'Success',
        modal: true,
        buttons: {
            Ok: function () {
                $(this).dialog('destroy');
            }
        }
    });
}

function ViewLog(id) {

    $("#logModal").dialog({
        modal: true,
        autoOpen: false,
        width: 800,
        title: "Audit Log",
        height: 600,
        buttons: {
            OK: function () {
                $(this).dialog('close');
                $(this).dialog('destroy');
                $("#logModal").html('');
            }
        }
    });

    $("#logModal").load('/Audit/View/' + id, function () { }).dialog('open');
     

}


function getPage(e){
    var page = "def";

    if ($(e).parents(".row").hasClass("user")) page = "user";
    if ($(e).parents(".row").hasClass("company")) page = "company";
    if ($(e).parents(".row").hasClass("vehicle")) page = "vehicle";
    if ($(e).parents(".row").hasClass("covernote")) page = "covernote";
    if ($(e).parents(".row").hasClass("certificate")) page = "certificate";
    if ($(e).parents(".row").hasClass("policy")) page = "policy";
    if ($(e).parents(".row").hasClass("policycover")) page = "policycover";
    if ($(e).parents(".row").hasClass("user-group")) page = "usergroup";
    if ($(e).parents(".row").hasClass("person")) page = "person";
    if ($(e).parents(".usage-row").length > 0) page = "usages";
    if ($(e).parents(".row").hasClass("usages")) page = "usages";
    if ($(e).parents(".wording").length > 0) page = "wording";
    if ($(e).parents(".image").length > 0) page = "template_image";
    if ($(e).parents(".row").hasClass("inters")) page = "intermediary";
    if ($(e).parents(".row").hasClass("auth")) page = "authword";

    if ($(e).parents(".row").hasClass("covercert")) {
        if ($(e).parents(".row").find(".CoverHidtype").val() == "Cover") page = "covernote";
        if ($(e).parents(".row").find(".CertHidtype").val() == "Certificate") page = "certificate";
    }

    return page;
}

/*              EXTRA CODE FOR MISC PAGES           */

function editCompany() {
    var id = $(".hiddenid").val();
    window.location.replace('/Company/Edit/' + id);
}

function rePrintInters(inters) {

    var inter_list_html = "";
    for (var x = 0; x < inters.length; x++) {
        var comp = inters[x];
        if (($(inters[x]).hasClass("odd") && x % 2 == 1) || ($(inters[x]).hasClass("even") && x % 2 == 0)) inter_list_html += comp.outerHTML;
        if (($(inters[x]).hasClass("odd") && x % 2 == 0) || ($(inters[x]).hasClass("even") && x % 2 == 1)) {
            comp = $(comp.outerHTML).toggleClass("odd").toggleClass("even")[0];
            inter_list_html += comp.outerHTML;
        }
    }
    $("#inter-data").html(inter_list_html);
}

function rePrintUsages(usages) {

    var usage_list_html = "";
    for (var x = 0; x < usages.length; x++) {
        var comp = usages[x];
        if (($(usages[x]).hasClass("odd") && x % 2 == 1) || ($(usages[x]).hasClass("even") && x % 2 == 0)) usage_list_html += comp.outerHTML;
        if (($(usages[x]).hasClass("odd") && x % 2 == 0) || ($(usages[x]).hasClass("even") && x % 2 == 1)) {
            comp = $(comp.outerHTML).toggleClass("odd").toggleClass("even")[0];
            usage_list_html += comp.outerHTML;
        }
    }
    $(".table-data-usages").html(usage_list_html);
}

function PasswordNotSent(id) {

    var tryAgain = true;
    $.ajax({
        url: '/User/ResendPassword/?id=' + id + '&resend=true',
        async: false,
        cache: false,
        success: function (data) {

            if (data.result) {
                if (data.mailSent) {
                    $("<div>A new password has been sent to the user.</div>").dialog({
                        title: 'Success',
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                    tryAgain = false;
                } else {
                    $("<div>Password cannot be reset at this time!</div>").dialog({
                        title: "Error!",
                        modal: true,
                        buttons: {
                            OK: function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                }
            }
        }

    });
    
    return tryAgain;
}

function editUsage(id,uids) {
    $("<div></div>").load("/Usage/Create?id=" + id + "&cid=" + $('#coverid').val() + '&details=false&UCID=' + uids).dialog({
        modal: true,
        title: "Edit Usage",
        buttons: {
            Save: function () {
                
               var usageString = $(".usage-field").val();
               var cid = $("#coverid").val();
               var certType = $(".certcode-field").val();

               if ((usageString || usageString != undefined || usageString != "" || usageString.length > 0) && (certType || certType != undefined || certType != "" || certType.length > 0)) {

                $.ajax({
                    url: '/Usage/Edit?id=' + id + '&usage=' + usageString + '&details=false&CertCode=' + certType + "&cid=" + cid + "&UCID=" + uids,
                    cache: false,
                    async: false
                });

                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
                window.location.reload();

                }
            },
            Cancel: function () {
                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
            }
        }
    });
}
