﻿
angular.module('eGovApp', [])
.service('eGovService', function () { /* ... */ })
.controller('MyeGovController', ['eGovService', '$scope', '$http', function (eGovService, $scope, $http) {


$scope.GetMyIntermediaries = function(){
        $("#loadingMessage").show();
        $http.get("/company/GetMyIntermediaries")
        .then(function (response) {
            $scope.IM = response.data;
            $("#loadingMessage").hide();
            
        });
    
    }

    //People Insured Methods Starts here

    $scope.SearchInsured = function (e) {
        var filter = $(e).parents("#insured").find(".modalSearch").val();
        var SearchType = $("input[name='search-type']:checked").val();
        var cid = $("#compid").val();

        $http.get("/People/SearchInsured/?id=" + cid + "&SearchType=" + SearchType + "&SearchValue=" + filter).then(function (response) {
            $scope.items = response.data.data;
        });
    }


    $scope.CheckIsAdmin = function(){
        $http.get("/Company/HomeValues").then(function(response){
            
            $scope.HomeDefaultValues = response.data;
        
     });
        
    
    }

    $scope.DisableCompany = function(CompanyID){
        $http.get("/Company/Disable?id=" + CompanyID).then(function(response){
            
            $scope.GetMyIntermediaries();
        
     });

    }

$scope.EnableCompany = function(CompanyID){
        $http.get("/Company/Enable?id=" + CompanyID).then(function(response){
            
            $scope.GetMyIntermediaries();
        
     });

    }


    $scope.DeleteCompany = function(CompanyID){
        http.get("/Company/Delete?id=" + CompanyID).then(function(response){
            
            $scope.GetMyIntermediaries();
        
     });

    }


    $scope.EditCompany = function(CompanyID){
        window.open("/Company/Edit/" + CompanyID);
    }

    $scope.CheckIsAdmin();

    //People Insured Methods ends here

    var CreateUserObject =
    {
        "create_user": true,
        "admin_user": "",
        "admin_pass": "",
        "username": "",
        "password": ""
    }

    var Auth =
    {
        "username": "",
        "password": ""
    }


    $scope.CreateUser = function () {

        //Authenticate
        Auth.username = $scope.AdminUserName;
        Auth.password = $scope.AdminPassword;
        $http.post("/VehicleCoverData", Auth).then(function (response) {
            $scope.SessionKey = response.data.sessionkey;

            //Create New User
            CreateUserObject.username = $scope.UserName;
            CreateUserObject.password = $scope.Password;
            CreateUserObject.admin_user = $scope.AdminUserName;
            CreateUserObject.admin_pass = $scope.AdminPassword;

            $http.post("/VehicleCoverData", CreateUserObject).then(function (response) {
                if (response.data.success) { alert("user Created successfully"); $scope.GetUserList(); } else { alert(response.data.comment); }
            });

        });

    }




    $scope.GetUserList = function () {

        $http.get("/home/GeteGovUsers")
    .then(function (response) {
        $scope.Users = response.data.Users;
    });

    }

    //Loads eGov Users
    if (location.href.indexOf("eGovUsers") > 0) { $scope.GetUserList(); }


    //Check Coverage
    $scope.CheckCoverage = function () {

        $("#resltArea").addClass("HideArea");
        $("#err").addClass("HideArea");


        var Chas = "";
        var Plate = "";

        Chas = $("#chas").val();
        Plate = $("#plate").val();

        var reg = new RegExp('[^A-Za-z0-9 ]');
        var DataValid = false;


        if (Chas.length <= 0 && Plate.length <= 0) {
            $("#ErrMSG").html("Please enter a Valid Plate Number or Chassis Number to continue");
            //alert("Please enter a Valid Plate Number or Chassis Number to continue");

        }
        else if (Chas.length > 0) {
            if (reg.exec(Chas) == null) DataValid = true;
        }
        else if (Plate.length > 0) {
            if (reg.exec(Plate) == null) DataValid = true;

        }


        if (DataValid) {
            $("#ErrMSG").html("");
            $("#resltArea").addClass("HideArea");
            $("#err").addClass("HideArea");

            $("#loader").show();

            $http.post("/web/CheckCoverage?chassis=" + Chas + "&license=" + Plate)
            .then(function (response) {
                $("#resltArea").removeClass("HideArea");
                $("#err").removeClass("HideArea");
                $("#loader").hide();
                $scope.Coverage = response.data;
                $scope.Isvalid = $scope.Coverage.VehicleCoverage[0].Vehicle_Covered;

                $scope.InsuredCompany = $scope.Coverage.VehicleCoverage[0].COMPANY;

                if ($scope.InsuredCompany == '' || $scope.InsuredCompany == null) $scope.InsuredCompany = "No results found.";
            });

        } else {
            $("#ErrMSG").html("Please enter a Valid Plate Number or Chassis Number.\n Avoid the use of special characters eg (-!$%^&*()_+<>)");

        }

    }


    $scope.DisplayFiler = function(){
        if($('#FilterArea')[0].hasAttribute('hidden')) $("#FilterArea").removeAttr("hidden");
        else $("#FilterArea").attr("hidden","hidden");
         
    }

    try{
    $scope.TypeOfCover = "Both";
    $scope.Intermediary = "All";
    }catch(e){}

    //Generate Cover Data to be downloaded
     $scope.GenerateCoverReport = function () {
     
        $("#loadingmessage").html("Generating Cover Data Please Wait....");

         $http.get("/home/GenerateCoverReport?TypeOfCover=" + $scope.TypeOfCover + "&Intermediary=" + $scope.Intermediary + "&FromDate="+ $scope.FromDate + "&ToDate=" + $scope.ToDate)
        .then(function (response) {
      
                var Fi = response.data.file;
                var link = document.createElement('a');
                
                if(response.data.success == false || response.data.file == "") $("#loadingmessage").html("There was an error... Please try again.");

                //Downloads file
                if (link.download !== undefined) {
                    //Set HTML5 download attribute. This will prevent file from opening if supported.
                    var fileName = Fi.substring(Fi.lastIndexOf('\\') + 1, Fi.length);
                    link.href = "/GeneratedCoverReports/" + fileName ;
                    link.setAttribute('target', '_blank');
                    link.download = fileName;

                    location.href = link.href;

                    $("#loadingmessage").html("");
                }

        });

    }

    
     //Generate Cover Data to be downloaded
     $scope.GenerateImportTemplates = function (TypeOfTemplate) {
         $http.get("/home/GenerateImportTemplates?TypeOfTemplate=" + TypeOfTemplate)
        .then(function (response) {
      
                var Fi = response.data.file;
                var link = document.createElement('a');
                
                if(response.data.success == false || response.data.file == "") $("#loadingmessage").html("There was an error... Please try again.");

                //Downloads file
                if (link.download !== undefined) {
                    //Set HTML5 download attribute. This will prevent file from opening if supported.
                    var fileName = Fi.substring(Fi.lastIndexOf('\\') + 1, Fi.length);
                    link.href = "/GeneratedCoverReports/" + fileName ;
                    link.setAttribute('target', '_blank');
                    link.download = fileName;

                    location.href = link.href;

                }

        });

    }


    

    $scope.GetMyIntermediaries();


    $scope.GetIntID = function(){
    
        console.log($(".optionCompany").id);
    }


} ]);

