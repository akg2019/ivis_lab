﻿
var trnFocus = false;
var trnSet = false;
var trnIsMessage = "<div>The TRN you have entered already exists in the system. Would you like to have the Person's Details populate the form now?</div>";
var UserIsMessage = "<div>The TRN you have entered already exists in the system, belonging to a user, in another company. The user has to first be deleted from that company.</div>";
var TRNmessage = "<div>You are about to edit the TRN of this user. Are you certain you want to do this?</div>";
var TRNbadmessage = "<div>The TRN you have selected is already taken (Please enter a valid TRN) and it must have exactly nine digits</div>";
var trnEdit = false;
var origTRN = "";


$(function () {
    $("#trn").prop('readonly', true);
    $("#trn").addClass('read-only');
    $("#edit-trn").on('click', function () {
        $(TRNmessage).dialog({
            title: 'Edit TRN Dialog',
            resizable: false,
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    $("#modify_btn").prop('disabled', true);
                    $("#trn").prop('readonly', false);
                    $("#trn").removeClass('read-only');
                    $("#trn").focus();
                    origTRN = $("#trn").val();
                    trnEdit = true;

                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    });
    $("#trn").blur(function () {
        if (trnEdit) {
            //test trn
            trnEdit = false;
            var type = "user";
            var id = $("#hiddenid").val();
            if (id == undefined) {
                id = $(".hidden_person_id").val();
                type = "person";
            }
            var regexTRN = /^[01][0-9]{8}$/;
            if (regexTRN.test($("#trn").val())) {
                $.ajax({
                    url: '/People/TestTRN/' + id + '?trn=' + $("#trn").val() + '&type=' + type,
                    cache: false,
                    success: function (data) {
                        if (data.result) {
                            $("#modify_btn").prop('disabled', false);
                            $("#trn").prop('readonly', true);
                            $("#trn").addClass('read-only');
                        }
                        else {
                            $(TRNbadmessage).dialog({
                                title: 'Edit TRN Dialog',
                                resizable: false,
                                modal: true,
                                buttons: {
                                    'Try Again': function () {
                                        $("#modify_btn").prop('disabled', true);
                                        $(this).dialog("close");
                                        $("#trn").focus();
                                        trnEdit = true;

                                    },
                                    Cancel: function () {
                                        $("#modify_btn").prop('disabled', false);
                                        $("#trn").val(origTRN);
                                        $("#trn").prop('readonly', true);
                                        $("#trn").addClass('read-only');
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        }
                    }
                });
            }
            else {
                $(TRNbadmessage).dialog({
                    title: 'Edit TRN Dialog',
                    resizable: false,
                    modal: true,
                    buttons: {
                        'Try Again': function () {
                            $("#modify_btn").prop('disabled', true);
                            $(this).dialog("close");
                            $("#trn").focus();
                            trnEdit = true;

                        },
                        Cancel: function () {
                            $("#modify_btn").prop('disabled', false);
                            $("#trn").val(origTRN);
                            $(this).dialog("close");
                        }
                    }
                });
            }
        }
    });
});