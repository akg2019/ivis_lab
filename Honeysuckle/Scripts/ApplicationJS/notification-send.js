﻿$(function () {
    var sendFocus = false;

    $("#content").cleditor({ controls: "bold italic underline strikethrough" });
    $("#contact-groups").on('click', '.contact-group .contacts .contact .message', function (event) {
        var name = $(this).parent().children(".name").html();
        if ($("#sendto").val().length > 0) {


            if (!isNameInListYet(name)) $("#sendto").val($("#sendto").val() + name + ";");
        }
        else $("#sendto").val(name + ";");
    });
    $("#sendto").focus(function () {
        sendFocus = true;
    });
    $("#sendto").blur(function () {
        if (sendFocus) {
            var contents = $("#sendto").val();
            var cp_contents = contents;
            var index = contents.lastIndexOf(';');
            var name = "";
            if (index > 0) {
                var names = contents.split(';');
                var spot;
                for (var i = names.length - 1; i >= 0; i--) {
                    name = names[names.length - 1].trim();
                    if (name.length > 0) {
                        spot = i;
                        break;
                    }
                    else names.pop();
                }
                var chars = 0; var bool = contents.charAt(contents.length - 1) == ';';
                for (var i = names.length - 1; i >= spot; i--) {
                    chars += names[i].length;
                    if (bool) chars++;
                }
                contents = contents.substring(0, contents.length - chars).trim();
            }
            else {
                name = contents;
                contents = "";
            }
            $.ajax({
                url: '/User/UserExist/' + name,
                cache: false,
                success: function (data) {
                    if (!data.result) name = "";
                    else name = name + ';';
                    $("#sendto").val(contents + name);
                    sendFocus = false;
                }
            });


        }
    });
    $("#sendto").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Contact/GetAvailableContactList/' + req.term, function (data) {
                var values = []
                $.each(data, function (i, v) {
                    values.push(v.value + ';');
                })
                resp(values);
            });
        },
        focus: function (event, ui) {
            event.preventDefault();
        },
        select: function (event, ui) {
            AutoCompleteSelectHandler(event, ui);
        }
    });

    function isNameInListYet(name) {
        var set = false;
        var names = $("#sendto").val().split(";");
        for (var i = 0; i < names.length; i++) {
            if (names[i] == name) {
                set = true;
                break;
            }
        }
        return set;
    };
    function clearForm() {
        $('#sendto').val("");
        $('#heading').val("");
        $('#content').contents().children('#content').val("");
    };

    function AutoCompleteSelectHandler(event, ui) {
        var contents = $(event.target).val();
        var name = ui.item;
        var index = contents.lastIndexOf(';') + 1;
        if (index > 0) {
            var subcontents = contents.substring(0, index);
            $(event.target).val(contents.substring(0, index) + name.value);
        }
        else {
            if (index > 0) $(event.target).val(contents.substring(0, index) + name.value);
            else $(event.target).val(name.value);

        }
        event.preventDefault();
    };
});