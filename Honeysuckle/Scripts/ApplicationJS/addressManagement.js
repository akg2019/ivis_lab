
$(function () {
    $("#roadtype,.roadtype").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Address/GetRoadTypes?type=' + req.term, function (data) {
                var values = []
                $.each(data, function (i, v) {
                    values.push(v.value);
                })
                resp(values);
            });
        }
    })
});


$(function () {
    $("#roadname,.roadname").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Address/GetRoads?type=' + req.term, function (data) {
                var values = []
                $.each(data, function (i, v) {
                    values.push(v.value);
                })
                resp(values);
            });
        }
    })
});


$(function () {
    $("#city,.city").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Address/GetCities?type=' + req.term, function (data) {
                var values = []
                $.each(data, function (i, v) {
                    values.push(v.value);
                })
                resp(values);
            });
        }
    })
});

$(function () {
    $("#country,.country").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Address/GetCountries?type=' + req.term, function (data) {
                var values = []
                $.each(data, function (i, v) {
                    values.push(v.value);
                })
                resp(values);
            });
        }
    })
});

$(function () {
    $(".parish").each(function () {
        var parish = $(this).val();
        if (parish != 0) {
            $(this).parents('.editor-field').find('.parishes').children().each(function () {
                if ($(this).val() == parish) {
                    $(this).prop('selected', true);
                    return;
                }
            });
            $(".parishes").trigger("change");
        }
    });
});

$(function () {
    $('.parishes').change(function () {
        $('#parish').val($(this).val());
    });
});

$(function () {
    $(".policy-mailing-address .parishes").change(function () {
        var selectedOp = $(".policy-mailing-address .parishes option:selected").val(); 
        $(".mailing#parish,.mailing.parish").val(selectedOp);
    });
    $(".billing-address-section .parishes").change(function () {
        var selectedOp = $(".billing-address-section .parishes option:selected").val();
        $(".billing#parish,.billing.parish").val(selectedOp);
    });
});

$(function () {
    if ($(".vehdetails").length > 0) {
        $("#city").removeAttr('required');
        $(".parishes").removeAttr('required');
    }
});

$(function () {
    $('#city').val("Kingston");
    $('#parish').val("11");
});


$(function () {
try{
    $('#city').val("Kingston");
    $('#parish').val("11");
    $('#country').val("Jamaica");}catch(e){}
});


