$(function(){

    var IAJIDFocus = false;

    $(".IAJID-create").focus(function(){
        IAJIDFocus = true;
    });


    $(".IAJID-create").blur(function(){
        if(IAJIDFocus){
            $.getJSON("/Company/GetCompanyByIAJID/" + $(".IAJID-create").val(), function (data) {
                      if (data.result) {
                           $(".IAJID-create").removeClass('ok');
                            $("<div>The IAJID you have entered already exists in the system.</div>").dialog({
                                modal:true,
                                title: "Alert!",
                                buttons:{
                                    OK: function(){
                                        $(".IAJID-create").val("");
                                        $(".IAJID-create").focus();
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        }
            });
            IAJIDFocus = false;
        }
    });

    
});