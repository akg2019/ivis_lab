﻿var newContactFocus = false;
var newContactGroupFocus = false;

$(function () {
    $("#contact-groups").on('click', ".contact-group .contact-group-name", function (event) {
        if (newContactGroupFocus) ToggleContactGroup(this);
    });
    $("#contact-groups").on('click', ".contact-group .contacts .contact .add-contact", function () {
        if (newContactGroupFocus) ToggleNewContact(this);
    });
    $("#contact-groups").on("focus.autocomplete", ".contact-group .contacts .contact .new-contact .user-username", function () {
        $(this).autocomplete({
            source: function (req, resp) {
                var cgid = this.element.parent().parent().parent().parent().children("input").val();
                $.getJSON('/Contact/ContactsNotInGroup/' + cgid + '?type=' + req.term, function (data) {
                    var values = []
                    $.each(data, function (i, v) {
                        values.push(v.value);
                    })
                    resp(values);
                });
            },
            select: function (event, ui) {
                AutoCompleteSelectHandler(event, ui);
            }
        });
    });
    $("#contact-groups .contact-group .contacts .contact .new-contact .user-username").blur(function () {
        $(this).val("Add A New Contact");
        ToggleNewContactBox(this);
    });
    $("#contact-groups").on('click', ".contact-group", function () {
        if (!newContactGroupFocus) ToggleContactGroup(this);
    });
    $("#cl").on('click', ".new-contact-group .add-contact-group", function () {
        ToggleNewContactGroup(this);
    });
    $('.add-contact-group-box').blur(function () {
        if (newContactFocus) {
            SendContactGroupToBeCreated($(this).val());
            ToggleNewContactGroupBack(this);
        }
    });


    function AutoCompleteSelectHandler(event, ui) {
        var cgid = $(event.target).parent().parent().parent().parent().children("input").val();
        var name = ui.item;
        $.ajax({
            url: '/Contact/CreateContact/' + cgid + '?contact=' + name.value,
            cache: false,
            success: function (data) {
                if (data.result) {
                    $.ajax({
                        url: '/Contact/ShowContact/' + data.uid,
                        cache: false,
                        success: function (d, a) {
                            $(event.target).parent().parent().parent().prepend('<div class=\'contact\'>' + d + '</div>');
                            $(event.target).val("Add a new contact");
                            $(event.target).blur();
                            $(event.target).parent().parent().children(".add-contact").toggleClass("showcontact");
                            $(event.target).parent().parent().children(".new-contact").toggleClass("showcontact");
                        }
                    });
                    return false;
                }
                else return false;
            }
        });
        return false;
    };



    function ToggleNewContact(cgid, uid) {
        $.ajax({
            url: '/Contact/CreateContact/' + cgid + '?contact=' + uid,
            cache: false,
            success: function (data) {
                if (data.result) {
                    $.ajax({
                        url: '/Contact/ShowContact/' + data.uid,
                        cache: false,
                        success: function (d) {
                            $(this).append(d);
                        }
                    });
                    return false;
                }
                else return false;
            }
        });
        return false;
    };

    function ToggleNewContact(it) {
        $(it).toggleClass("showcontact");
        $(it).parent().children(".new-contact").toggleClass("showcontact");
        $(it).parent().children(".new-contact").children(".user-username").focus();
    };
    function ToggleNewContactBox(it) {
        $(it).parent().toggleClass("showcontact");
        $(it).parent().parent().children(".add-contact").toggleClass("showcontact");
    };
    function ToggleContact(it) {

    };

    function ToggleNewContactGroup(it) {
        $(it).toggleClass("showcontact");
        $(it).parent().children('.add-contact-group-box').toggleClass("showcontact");
        var box = $(it).parent().children('.add-contact-group-box');
        if (!newContactFocus) box.focus();
        newContactFocus = !newContactFocus;
    };

    function ToggleNewContactGroupBack(it) {
        $(it).val("Add a new contact");
        $(it).toggleClass("showcontact");
        $(it).parent().children('.add-contact-group').toggleClass("showcontact");
        newContactFocus = !newContactFocus;
    };

    function ToggleContactGroup(it) {
        $(it).children(".contacts").toggleClass("showcontact");
        newContactGroupFocus = !newContactGroupFocus;
    }

    function SendContactGroupToBeCreated(name) {
        if (!(name == "Add a new contact" || name == "")) {
            $.ajax({
                url: '/Contact/CreateContactGroup/?name=' + name,
                cache: false,
                success: function (json) {
                    if (json.result == 1) {
                        $.ajax({
                            url: '/Contact/ShowContactGroup/' + json.cgid,
                            cache: false,
                            success: function (d) {
                                var html = "<div class=\"contact-group\">" + d + "</div>";
                                $("#contact-groups").append(html);
                            }
                        });
                        return false;
                    }
                    else return false;
                }
            });
            return false;
        }
    };



});


