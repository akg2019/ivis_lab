﻿$(function () {

    var username = $(".usernameHidden").val();

    $('#edit-username').click(function () {
        $("<div>You are about to edit this username. Are sure you want to do this?</div>").dialog({
            modal: true,
            title: 'Alert!',
            buttons: {
                "Yes": function () {
                    //username = $("#username").val();
                    $("#username").prop('readonly', false);
                    $("#username").prop('required', true);
                    $("#username").addClass('email');
                    $("#username").removeClass('read-only');
                    $(this).dialog("close");
                    $("#username").focus();
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }

        });
    });

    $("#username").blur(function () {
        var user = $("#username").val();
        var id = $(".userid").val();

        if (user != username && !$("#username").hasClass('read-only')) {
            $.ajax({
                url: '/User/TestUsername/?username=' + user,
                cache: false,
                success: function (data) {
                    if (data.error == 0) {
                        if (data.result == 1) {
                            $("<div>This username already exists<div>").dialog({
                                modal: true,
                                title: 'Alert!',
                                buttons: {
                                    OK: function () {
                                        $(this).dialog("close");
                                        $("#username").removeClass('ok');
                                        $("#username").focus();
                                    }
                                }
                            });
                        }
                    }
                    else {
                        $("<div>You do not have permissions to edit update usernames<div>").dialog({
                            modal: true,
                            title: 'Alert!',
                            buttons: {
                                OK: function () {
                                    $(this).dialog("close");
                                    $("#username").val(username);
                                }
                            }
                        });
                    }
                }
            });
        }
    });

    if ($("#userExists").length > 0) {
        $("#userExists").dialog({
            modal: true,
            title: "Alert!",
            buttons: {
                OK: function () {
                    $(this).dialog("close");
                    $("#username").prop('readonly', false);
                    $("#username").prop('required', true);
                    $("#username").addClass('email');
                    $("#username").removeClass('read-only');
                    $("#username").removeClass('ok');
                    $("#username").focus();
                    $("#userExists").html("");
                }
            }
        });
    }

});

