﻿var chassisFocus = false;
var chassisSet = false;

$(function () {
enabledAll();
    //disableAll();
    if ($("#chassis").val().trim().length > 0) chassisSet = true;

    $("#chassis").focus(function () {
        chassisFocus = true;
    });

    $("#chassis").blur(function () {
        StartSpinner();
        if (chassisFocus) {
            var test = $("#chassis").val();
            if (test.trim().length > 0) {
                var result = false;
                $.getJSON("/Vehicle/GetVehicleByChassis/" + test.trim(), function (data) {
                    if (!data.error) {
                        //something exists

                        if (data.vehicle) {
                            //the vehicle exists in the system already
                            StopSpinner();
                            $("<div>The Vehicle you have entered already exists in the system. Would you like to view that vehicle's details?</div>").dialog({
                                modal: true,
                                title: "Alert",
                                buttons: {
                                    Ok: function () {
                                        window.location.replace("/Vehicle/Details/" + data.vid);
                                    },
                                    Cancel: function () {
                                        //$("#chassis").val("");
                                        //disableAll();
                                        enabledAll();
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        }

                        else {
                            //chassis FREE
                            if (data.TAJ) {
                                //TAJ success
                                $("#chassis").val(data.chassis);
                                $("#vehicleRegNum").val(data.regno);
                                $("#engineNo").val(data.engineno);
                                $("#vehicleYear").val(data.vehyear);
                                $("#vehiclemodel").val(data.model);
                                $("#fitnessExpiry").val(data.expiry);
                                $("#regOwner").val(data.owners);
                            }
                            if (test.trim().length != 0) chassisSet = true;
                            enabledAll();
                            StopSpinner();
                        }

                    }
                    else {
                        //ERROR
                        $("<div>" + data.message + "</div>").dialog({
                            modal: true,
                            buttons: {
                                Ok: function () {
                                   // $("#chassis").val("");
                                    $(this).dialog("close");
                                }
                            }
                        });

                    }
                });
                chassisFocus = false; //reset variable so that things dont go wonky
            }
            else {
                if (chassisSet) {
                    chassisSet = false;
                }
                disableAll();
                StopSpinner();
            }
        }

    });

    $("#make,#vehiclemodel,#modeltype,#bodytype,#extension,#vehicleRegNum,#colour,#cylinders,#engineNo,#CengineModified,#engineType,#mileage,#fitnessExpiry,#hpccUnit,#refno,#regLocation,#roofType,#seating,#seatingCert,#tonnage,#vehicleYear,#handDriveType,#tranType,#username,#fname,#mname,#lname,#RoadNo,#RoadName,#RoadType,#ZipCode,#CityName,#CountryName").focus(function () {
        if (!chassisSet) {
            $(this).blur();
            $("<div>Please fill the chassis first</div>").dialog({
                modal: true,
                title: "Alert",
                buttons: {
                    Ok: function () {
                        $("#chassis").val("");
                        $(this).dialog("close");
                    }
                }
            });
        }
    });
});


function StartSpinner() {
    $("body").append('<div class="ui-widget-overlay ui-front temp"></div>');
    var opts = {
        lines: 13, // The number of lines to draw
        length: 11, // The length of each line
        width: 7, // The line thickness
        radius: 26, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#fff', // #rgb or #rrggbb or array of colors
        speed: 0.7, // Rounds per second
        trail: 33, // Afterglow percentage
        shadow: true, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: '50%', // Top position relative to parent
        left: '50%' // Left position relative to parent
    };
    var target = document.getElementById('spinner');
    spinner = new Spinner(opts).spin(target);
}

function StopSpinner() {
    $(".ui-front.temp").remove();
    var target = document.getElementById('spinner');
    spinner.stop(target);

}

function enabledAll() {
    $("#make,#vehiclemodel,#modeltype,#bodytype,#extension,#vehicleRegNum,#colour,#cylinders,#engineNo,#engineModified,#engineType,#mileage,#fitnessExpiry,#hpccUnit,#refno,#regLocation,#roofType,#seating,#seatingCert,#tonnage,#vehicleYear,#handDriveType,#tranType,#regOwner").prop('disabled', false);
}

function disableAll() {
    //$("#vehiclemodel,#modeltype,#bodytype,#extension,#vehicleRegNum,#colour,#cylinders,#engineNo,#engineModified,#engineType,#mileage,#fitnessExpiry,#hpccUnit,#refno,#regLocation,#roofType,#seating,#seatingCert,#tonnage,#handDriveType,#tranType,#regOwner").prop('disabled', true);
}