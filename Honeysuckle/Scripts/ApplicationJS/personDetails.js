﻿

$(document).on("click", '.names', function () {
    //   $(".names").on("click", function () {
   
    var personView = $(this).parents('.insured');
    var personTRN = $(this).parents('.insured').find('.perTRN').val();
    var perImportID = $(this).parents('.insured').find('.perImportID').val();

    perId = $(this).parent().parent().children(".perId").val();
    personID = $(this).parent().parent().children(".base_per_id").val();
    $("#insured-modal").dialog({
        modal: true,
        autoOpen: false,
        width: 400,
        title: "Person Details",
        //height: 400,
        buttons:
             {
                 OK: function () {

                     $('#insured-modal').dialog('close');
                     $('#insured-modal').html("");
                     $('#insured-modal').dialog('destroy');

                 },
                 Edit: function () {
                   if(perImportID == "" && personID != "10000000"){
                     personTRN = $('.label.trn').html();
                     $("#insured-modal").dialog('close');
                     $("#insured-modal").html("");
                     $("#insured-modal").dialog({
                         modal: true,
                         width: 370,
                         height: 690,
                         title: "Edit Policy Holder (Person)",
                         buttons: {
                             Save: function () {
                                  
                                    var regexTRN = /^[01][0-9]{8}$/;
                                    var regex = /^[a-zA-Z][A-Za-z-.\' ]*[a-zA-Z]$/;
                                    var regex1 = /^[a-zA-Z][A-Za-z-.\' ]*[a-zA-Z]*$/;
                                    var regex2 = /^[a-zA-Z0-9][a-zA-Z0-9-\/#&. ]*$/;
                                    var regex3 = /^[a-zA-Z][a-zA-Z0-9.&,#\-\' ]*$/;
                                    var regex4 = /^[a-zA-Z][a-zA-Z.&,#\-\' ]*[a-zA-Z]$/;
                                    var regexAptNum = /^[a-zA-Z0-9-\/#&.\' ]*$/;
                                    var regexCity = /^(([a-zA-Z](([a-zA-Z0-9.& '-])*[a-zA-Z0-9])?)|([a-zA-Z](([a-zA-Z0-9.& '-])*([ ])?[(]([a-zA-Z0-9.& '-])+[a-zA-Z0-9][)])?))$/;

                                 var changeTRN = ($('#trnChange').val().toLowerCase() == "true") ? true : false;
                                // var trn =  $("#insured-modal").find("#trn-editPolHol.person-trn").val().trim(); //personTRN.trim();
                                var trn =  $("#insured-modal").find("#trn-editPolHol").val().trim();
                                var IsForeignID = $("#insured-modal").find("#trn-editPolHol").hasClass('foreignID') == true ? true : false;
                                 var fname = $("#insured-modal").find("#fname").val().trim();
                                 var mname = $("#insured-modal").find("#mname").val().trim();
                                 var lname = $("#insured-modal").find("#lname").val().trim();
                                 var roadno = $("#insured-modal").find("#RoadNumber").val().trim();
                                 var aptNumber = $("#insured-modal").find("#aptNumber").val().trim();
                                 var road = $("#insured-modal").find("#roadname").val().trim();
                                 var roadType = $("#insured-modal").find("#roadtype").val().trim();
                                 var zip = $("#insured-modal").find("#zipcode").val();
                                 var parish = $("#insured-modal").find(".parishes").val();
                                 var city = $("#insured-modal").find("#city").val().trim();
                                 var country = $("#insured-modal").find("#country").val().trim();
                                 var ret = 0;
                                 var count = 0;
                                 var validated = true;


                                 if ( (!regexTRN.test(trn) && !IsForeignID) || trn == '') {
                                     alert('Please enter a valid TRN/ID. Check the box if it should be foreign.');
                                     return false;
                                 }

                                 if (fname == '' || !regex.test(fname)) {
                                     alert('Please enter a valid first name. ');
                                     return false;
                                 }

                                 if (mname == '') { }
                                 else {
                                     if (!regex1.test(mname)) {
                                         alert('Please enter a valid middle name.');
                                         return false;
                                     }
                                 }

                                 if (lname == '' || !regex.test(lname)) {
                                     alert('Please enter a valid last name.');
                                     return false;
                                 }

                                 if (parish == 0) {
                                     alert('Please select a parish.');
                                     return false;
                                 }


                                 if (city == '' || !regexCity.test(city)) {
                                     alert('Please enter a valid city.');
                                     return false;
                                 }


                                 if (country == '' || !regex4.test(country)) {
                                     alert('Please enter a valid country name.');
                                     return false;
                                 }

                                 var parms =
                                  { trn: trn, fname: fname, mname: mname, lname: lname, roadno: roadno, aptNumber: aptNumber,
                                      road: road, roadType: roadType, zipcode: zip, city: city, country: country, parish: parish, foreignid: IsForeignID, update: true
                                  };


                                 $.ajax({
                                     type: "POST",
                                     traditional: true,
                                     url: "/People/AddPerson/",
                                     async: false,
                                     data: parms,
                                     dataType: "json",
                                     success: function (data) {

                                         $("#insured-modal").html("");
                                         $("#insured-modal").dialog("close");
                                         $("#insured-modal").dialog("destroy");
                                         $('<div>The policy holder\'s record has been saved</div>').dialog({
                                            modal:true,
                                            title:'Policy Holder Saved',
                                            buttons:{
                                                OK:function(){
                                                //window.location.replace('/Policy/Details/' + $('#hiddenid').val());
                                                //personView.remove();

                                                var perParams = 
                                                {
                                                 perId:perId, type: ($('#page').val() == "details") ? "details": null , changeTRN: changeTRN, trn: trn
                                                };

                                                $.ajax({
                                                type: "POST",
                                                traditional: true,
                                                url: "/People/personView/",
                                                async: false,
                                                data: perParams,
                                                dataType: "html",
                                                success: function (html) {
                                                        personView.replaceWith(html);
                                                       // $('#insured').append(html);

                                                       /*
                                                        $.ajax({
                                                            url:'/Policy/RmPolicyHolderToPolicy/' + $('#hiddenid').val() + '?pid=' + perId,
                                                            cache: false,
                                                            async:false
                                                        });
                                                        $.ajax({
                                                            url:'/Policy/AddPolicyHolderToPolicy/' + $('#hiddenid').val() + '?pid=' + $(html).find('.perId').val(),
                                                            cache: false
                                                        });
                                                        */
                                                    }
                                                });



                                               
                                                $(this).dialog('close');
                                                }
                                            }

                                         });
                                         
                                     }
                                 });
                             },
                             Cancel: function () {
                                 $("#insured-modal").html("");
                                 $("#insured-modal").dialog("close");
                                 $("#insured-modal").dialog("destroy");
                             }
                         }
                     });
                     $("#insured-modal").load('/People/Person/?type=' + 1 + '&edit=true', function () {
                         $.ajax({
                             type: "POST",
                             url: "/Vehicle/GetUserByTRN/" + personTRN.trim() + '?insured=' + true,
                             success: function (data) {
                                 if (!data.error) {
                                     if (data.result) {
                                         //something exists
                                         if (data.person) {
                                             $("#trn-editPolHol.person-trn").val(personTRN.trim());
                                             $("#fname").val(data.fname);
                                             $("#mname").val(data.mname);
                                             $("#lname").val(data.lname);
                                             if(data.longAddressUsed){
                                                     $('#longAddress').val(data.longAddress);
                                                     $('#perStructuredAddress').hide();
                                                     }
                                                     else{
                                             $("#RoadNumber").val(data.roadno);
                                             $("#roadname").val(data.roadname);
                                             $("#roadtype").val(data.roadtype);
                                             $("#city").val(data.city);
                                             $("#aptNumber").val(data.aptNumber);
                                             $("#parish").val(data.parishid);
                                             $("#country").val(data.country);
                                             $('#perCombinedAddress').hide();
                                             }

                                             //$("#trn,#fname, #mname, #lname").prop('disabled', true);
                                             $("#trn-editPolHol").prop('disabled', true);
                                             //$("#trn,#fname, #mname, #lname").toggleClass("read-only");
                                             $("#trn-editPolHol").toggleClass("read-only");
                                             $("#trn-editPolHol,#fname, #mname, #lname,#RoadNumber,#roadname,#roadtype,#city,#aptNumber,#parish,#country").trigger('keyup');
                                             

                                             var thisparishId = data.parishid;
                                             if (thisparishId != 0) {
                                                 $(".parishes").children().each(function () {
                                                     if ($(this).val() == thisparishId) {
                                                         $(this).prop('selected', true);
                                                         return;
                                                     }
                                                 });
                                             }
                                            $(".parishes").trigger('change');
                                         }
                                     }
                                 }
                             }
                         });
                     }).dialog('open');
                 }
                else{
                $('<div>You cannot edit this person\'s details here because it was imported and does not have all the relevant information.</div>').dialog({
                    modal:true,
                    title:'Alert!',
                    buttons:{
                        OK:function(){
                            $(this).dialog('close');
                        }
                    }
                });
               }

            }
         }
           
    });

    $("#insured-modal").load("/People/FullPersonDetails/" + perId).dialog('open');
});


$(function () {
    $(".perTRN").on("click", function () {

        perId = $(this).val();

        $("#person-modal").dialog({
            modal: true,
            autoOpen: false,
            width: 400,
            title: "Person Details",
            height: 400,
            buttons:
             {
                 OK:
              function () {

                  $('#person-modal').dialog('close');
                  $('#person-modal').html("");

              }
            }
        });

        $("#person-modal").load("/People/FullPersonDetails/" + perId).dialog('open');
    });
});


$(function () {
    $(".TRN").on("click", function () {

        perId = $(this).val();

        $("#person-modal").dialog({
            modal: true,
            autoOpen: false,
            width: 400,
            title: "Person Details",
            height: 400,
            buttons:
             {
                 OK:
              function () {

                  $('#person-modal').dialog('close');
                  $('#person-modal').html("");
                  $('#person-modal').dialog('destroy');

               }
            }
        });

        $("#person-modal").load("/People/FullPersonDetails/" + perId).dialog('open');
    });
});


$(document).on("click", '.vehData', function () {
  if(!($(this).parents('.newvehicle').hasClass('underPolicy'))){
    var vehicleCard = $(this).parents('.newvehicle');
    chassisNum = $(this).parents(".newvehicle").find(".vehId").val();
    var chassis = $(this).parents(".newvehicle").find('.chassisNumber').val();

    var vehID =  $(this).parents('.newvehicle').find('.vehId').val();
    var start = $('#start').val();
    var end = $('#end').val();
    var type = "details";
    var cover = $('#CoverID').val();
    var usage = $(this).parents('.newvehicle').find('.vehicle-usage-id').val();
    var polid = $('#hiddenid').val();
    var imported = $('#import').val();


    $("#vehicle-modal").load("/Vehicle/FullVehicleDetails/" + chassisNum).dialog({
        modal: true,
        width: 400,
        title: "Vehicle Details",
        //height: 400,
        buttons:
             {
                 OK:
              function () {

                  $('#vehicle-modal').dialog('close');
                  $('#vehicle-modal').html("");
                  $('#vehicle-modal').dialog("destroy");

              },
                 Edit:
              function () {
                if($('#page').val() != "details"){
                  $('#vehicle-modal').dialog('close');
                  $('#vehicle-modal').html("");
                  $('#vehicle-modal').load('/Vehicle/AddNewVehicle?PolCoverID=' + $(".covers_dropdown").val(), function () {

                     $('#city.address-details').prop('disabled',false);
                     $('.driver-details #parishes.parishes').prop('disabled',false);
                     $('#country.address-details').prop('disabled',false);
                     $('#aptNumber.address-details').prop('disabled',false);
                     $('#RoadNumber.address-details').prop('disabled',false);
                     $('#roadname.address-details').prop('disabled',false);
                     $('#aptNumber.address-details').prop('disabled',false);
                     $('#roadtype.address-details').prop('disabled',false);

                     $('#city.address-details').prop('readonly',false);
                     $('.driver-details #parishes.parishes').prop('readonly',false);
                     $('#country.address-details').prop('readonly',false);
                     $('#aptNumber.address-details').prop('readonly',false);
                     $('#RoadNumber.address-details').prop('readonly',false);
                     $('#roadname.address-details').prop('readonly',false);
                     $('#aptNumber.address-details').prop('readonly',false);
                     $('#roadtype.address-details').prop('readonly',false);

                      $.getJSON("/Vehicle/GetVehicleByChassis/" + chassis, function (data) {
                          if (!data.error) {
                              //something exists
                              if (data.vehicle) {
                                  $("#chassis").val(chassis);
                                  $("#prevRegNo").val(data.vehicle_data.VehicleRegNumber);
                                  $("#prevChassis").val(chassis.trim());
                                  $("#vin").val(data.vehicle_data.vin);
                                  $("#make").val(data.vehicle_data.make);
                                  $("#vehiclemodel").val(data.vehicle_data.VehicleModel);
                                  $("#vehicleYear").val(data.vehicle_data.VehicleYear);
                                  $("#vehicleRegNum").val(data.vehicle_data.VehicleRegNumber);
                                  $(".advanced-check").prop("checked", true);
                                  $("#modeltype").val(data.vehicle_data.modelType);
                                  $("#bodytype").val(data.vehicle_data.bodyType);
                                  $("#estimated_value").val(data.vehicle_data.estimatedValue);
                                  $("#extension").val(data.vehicle_data.extension);
                                  $("#colour").val(data.vehicle_data.colour);
                                  $("#fitnessExpiry").val(data.fitnessExpiry);
                                  $("#cylinders").val(data.vehicle_data.cylinders);
                                  $("#engineNo").val(data.vehicle_data.engineNo);
                                  $("#engineModified").prop("checked", data.vehicle_data.engineModified);
                                  $("#engineType").val(data.vehicle_data.engineType);
                                  $("#mileage").val(data.vehicle_data.mileage);
                                  $("#hpccUnit").val(data.vehicle_data.HPCCUnitType);
                                  $("#refno").val(data.vehicle_data.referenceNo);
                                  $("#regLocation").val(data.vehicle_data.registrationLocation);
                                  $("#roofType").val(data.vehicle_data.roofType);
                                  $("#seating").val(data.vehicle_data.seating);
                                  $("#seatingCert").val(data.vehicle_data.seatingCert);
                                  $("#tonnage").val(data.vehicle_data.tonnage);
                                  $("#handDriveType").val(data.vehicle_data.handDriveType);
                                  $("#tranType").val(data.vehicle_data.transmissionType.transmissionType);
                                  $("#regOwner").val(data.vehicle_data.RegisteredOwner);
                                  $("#restrictedDriver").prop("checked", data.vehicle_data.RestrictedDriving);

                                $('#trn').val(data.vehicle_data.mainDriver.person.TRN);
                                $('#fname').val(data.vehicle_data.mainDriver.person.fname);
                                $('#mname').val(data.vehicle_data.mainDriver.person.mname);
                                $('#lname').val(data.vehicle_data.mainDriver.person.lname);
                                $('#aptNumber').val(data.vehicle_data.mainDriver.person.address.ApartmentNumber);
                                $('#country').val(data.vehicle_data.mainDriver.person.address.country.CountryName);
                                $('#city').val(data.vehicle_data.mainDriver.person.address.city.CityName);
                                $('#RoadNumber').val(data.vehicle_data.mainDriver.person.address.roadnumber);
                                $('#roadname').val(data.vehicle_data.mainDriver.person.address.road.RoadName);
                                $('#roadtype').val(data.vehicle_data.mainDriver.person.address.roadtype.RoadTypeName);

                                $('.ui-dialog .parishes').val(data.vehicle_data.mainDriver.person.address.parish.ID);
                                $('.ui-dialog .parishes').trigger('change');

                                  $(".vehicle-advanced-data").show();

                                  //$("#bodytype,#extension,#vehicleRegNum,#colour,#cylinders,#engineNo,#CengineModified,#engineType,#mileage,#fitnessExpiry,#hpccUnit,#refno,#regLocation,#roofType,#seating,#seatingCert,#tonnage,#handDriveType,#tranType,#username,#fname,#mname,#lname,#RoadNo,#RoadName,#RoadType,#ZipCode,#CityName,#CountryName,#trn").prop('disabled', false);
                                  //$("#chassis").prop('disabled', true);
                                  //$("#chassis").prop('readonly', true);
                                  $("#chassis,#make,#vehiclemodel,#modeltype,#bodytype,#extension,#vehicleRegNum,#colour,#cylinders,#engineNo,#CengineModified,#engineType,#mileage,#fitnessExpiry,#hpccUnit,#refno,#regLocation,#roofType,#seating,#seatingCert,#tonnage,#vehicleYear,#handDriveType,#tranType,#username,#fname,#mname,#lname,#RoadNo,#RoadName,#RoadType,#ZipCode,#CityName,#CountryName,#trn,#roadno,#roadname,#city,#roadtype,#parishes,#country").trigger('keyup');

                              }
                          }
                      });

                  }).dialog({
                      modal: true,
                      width: 400,
                      height: 500,
                      title: "Edit Vehicle",
                      buttons: {
                          Save: function () {

                                     var regexTRN = /^[01][0-9]{8}$/;
                                    var regex = /^[a-zA-Z][A-Za-z-.\' ]*[a-zA-Z]$/;
                                    var regex1 = /^[a-zA-Z][A-Za-z-.\' ]*[a-zA-Z]*$/;
                                    var regex2 = /^[a-zA-Z0-9][a-zA-Z0-9-\/#&. ]*$/;
                                    var regex3 = /^[a-zA-Z][a-zA-Z0-9.&,#\-\' ]*$/;
                                    var regex4 = /^[a-zA-Z][a-zA-Z.&,#\-\' ]*[a-zA-Z]$/;
                                    var regexAptNum = /^[a-zA-Z0-9-\/#&.\' ]*$/;
                                    var regexCity = /^(([a-zA-Z](([a-zA-Z0-9.& '-])*[a-zA-Z0-9])?)|([a-zA-Z](([a-zA-Z0-9.& '-])*([ ])?[(]([a-zA-Z0-9.& '-])+[a-zA-Z0-9][)])?))$/;

                            //regExpressions for validation
                            
                                   
                                    var regex1 = /^[a-zA-Z][A-Za-z-.\' ]*[a-zA-Z]*$/;
                                    var regex2 = /^[a-zA-Z0-9][a-zA-Z0-9-\/#&. ]*$/;
                                    var regex3 = /^[a-zA-Z][a-zA-Z0-9.&,#\-\' ]*$/;
                                    var regex4 = /^[a-zA-Z][a-zA-Z.&,#\-\' ]*[a-zA-Z]$/;
                                    var regexAptNum = /^[a-zA-Z0-9-\/#&.\' ]*$/;
                                    var regexCity = /^(([a-zA-Z](([a-zA-Z0-9.& '-])*[a-zA-Z0-9])?)|([a-zA-Z](([a-zA-Z0-9.& '-])*([ ])?[(]([a-zA-Z0-9.& '-])+[a-zA-Z0-9][)])?))$/;



                                var regex = /^[a-zA-Z][A-Za-z-.\' ]*[a-zA-Z]$/;
                                var regexMake = /^[a-zA-Z0-9][a-zA-Z0-9-.,\'& ]*[a-zA-Z0-9]$/;
                                var regexModel = /^[a-zA-Z0-9][a-zA-Z0-9-.&\', ]*[a-zA-Z0-9]*$/;
                                var regexModelType = /^[a-zA-Z0-9][a-zA-Z0-9-,.\'& ]*[a-zA-Z0-9]*$/;
                                var regexbodyType = /^[a-zA-Z0-9][a-zA-Z0-9-,\/.\'& ]*[a-zA-Z0-9]$/;
                                var regexExtention = /^[a-zA-Z0-9][a-zA-Z0-9-,.\'& ]*[a-zA-Z0-9]$/;
                                var regNumber = /^[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]$/;
                                var regexVIN = /^[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]+$/;
                                var regexColour = /^[a-zA-Z-.,\/& ]+$/;
                                var regexChassis = /^[a-zA-Z0-9]+$/;
                                var regexEngineNo = /^[a-zA-Z0-9][a-zA-Z0-9- ]*[a-zA-Z0-9]*$/;
                                var regexEngineMod = /^[a-zA-Z][a-zA-Z0-9-]*$/;
                                var regexengineType = /^[a-zA-Z0-9][a-zA-Z0-9-&.,\' ]*[a-zA-Z0-9]+$/;
                                var regexHPCCUnitType = /^[a-zA-Z0-9][a-zA-Z0-9.,\' ]*[a-zA-Z0-9]+$/;
                                var regexregLoc = /^[a-zA-Z0-9][a-zA-Z0-9-&\-,#.\' ]*[a-zA-Z0-9]+$/;
                                var regexRoof = /^[a-zA-Z0-9][a-zA-Z0-9- ]*[a-zA-Z0-9]+$/;
                                var regexTransType = /^[a-zA-Z]{1,}[a-zA-Z-]*[a-zA-Z]$/;
                                var regexHDriveType = /^[a-zA-Z]{1,}[a-zA-Z-]*[a-zA-Z]$/;
                                var regexNumbers = /^[0-9]+$/;
                                var regexRegOwner = /^[a-zA-Z0-9][a-zA-Z0-9\-@!?\/#&$+,.\' ]*$/;
                                var regexTRN = /^[01][0-9]{8}$/;

                              var est_val = $("#estimated_value").val().trim();

                              var vin = $("#vin").val().trim();
                              var chassis = $("#chassis").val().trim();
                              var make = $("#make").val().trim();
                              var model = $("#vehiclemodel").val().trim();
                              var modelType = $("#modeltype").val().trim();
                              var bodyType = $("#bodytype").val().trim();
                              var Extension = $("#extension").val().trim();
                              var vehicleRegNumber = $("#vehicleRegNum").val().trim();
                              var colour = $("#colour").val().trim();
                              var cylinders = $("#cylinders").val().trim();
                              var engineNo = $("#engineNo").val().trim();
                              var engineModified = $("#engineModified").prop("checked");
                              var engineType = $("#engineType").val().trim();
                              var estAnnualMileage = $("#mileage").val().trim();
                              var expiryDateOfFitness = $("#fitnessExpiry").val().trim();
                              var HPCCUnitType = $("#hpccUnit").val().trim();

                              var ReferenceNo = $("#refno").val().trim();
                              var RegistrationLocation = $("#regLocation").val().trim();
                              var RoofType = $("#roofType").val().trim();
                              var seating = $("#seating").val().trim();
                              var seatingCert = $("#seatingCert").val().trim();
                              var tonnage = $("#tonnage").val().trim();
                              var VehicleYear = $("#vehicleYear").val().trim();
                              var handDriveType = $("#handDriveType").val().trim();
                              var transtype = $("#tranType").val().trim();
                              var regOwner = $("#regOwner").val().trim();
                              var restrictedDriving = $("#restrictedDriver").prop("checked");

                              var perId = $("#personid").val().trim();
                              var trn = $("#trn").val().trim();
                              var fname = $("#fname").val().trim();
                              var mname = $("#mname").val().trim();
                              var lname = $("#lname").val().trim();
                              var roadno = $("#RoadNumber").val().trim();
                              var road = $("#roadname").val().trim();
                              var aptNumber = $("#aptNumber").val().trim();
                              var roadType = $("#roadtype").val().trim();
                              var zip = $("#zipcode").val();
                              var city = $("#city").val().trim();
                              var country = $("#country").val().trim();
                              var parish = $("#parish.address-details").val().trim();
                              var prevChassis = $("#prevChassis").val().trim();
                              var prevRegNo = $("#prevRegNo").val().trim();

                              var countEms = 0;

                              var emails = [];
                              var primary = [];
                              var email_objects = [];
                              var validation = true;

                              var advanced = $(".advanced-check").is(":checked");


                              if (vin != '' && !regexVIN.test(vin)) {
                                  alert('Please enter a valid VIN.');
                                  return false;
                              }

                              if (chassis == '' || !regexChassis.test(chassis)) {
                                  //alert('Please enter a valid chassis (numbers and letters only) ');
                                  //return false;
                                  return true;
                              }

                              if (make == '' || !regexMake.test(make)) {
                                  alert('Please enter a valid make');
                                  return false;
                              }

                              if (model  != '' && !regexModel.test(model)) {
                                  alert('Please enter a valid model');
                                  return false;
                              }

                              if (VehicleYear == '' || !regexNumbers.test(VehicleYear)) {
                                  //alert('Please enter a valid Vehicle Year (numbers only) ');
                                  //return false;
                                  return true;
                              }

                              if (vehicleRegNumber != '' && !regNumber.test(vehicleRegNumber)) {
                                  alert('Please enter a valid  registration number');
                                  return false;
                              }

                              if (modelType == '') { }
                              else {
                                  if (!regexModelType.test(modelType)) {
                                      alert('Please enter a valid model type');
                                      return false;
                                  }
                              }

                              if (bodyType == '') { }
                              else {
                                  if (!regexbodyType.test(bodyType)) {
                                      alert('Please enter a valid body type');
                                      return false;
                                  }
                              }

                              if (Extension == '') { }
                              else {
                                  if (!regexExtention.test(Extension)) {
                                      alert('Please enter a valid  Extension');
                                      return false;
                                  }
                              }

                              if (colour == '') { }
                              else {
                                  if (!regexColour.test(colour)) {
                                      alert('Please enter a valid colour');
                                      return false;
                                  }
                              }

                              if (cylinders == '') { }
                              else {
                                  if (!regexNumbers.test(cylinders)) {
                                      alert('Please enter a valid cylinders (number) ');
                                      return false;
                                  }
                              }

                              if (engineNo == '') { }
                              else {
                                  if (!regexEngineNo.test(engineNo)) {
                                      alert('Please enter a valid engine number ');
                                      return false;
                                  }
                              }

                              if (engineType == '') { }
                              else {
                                  if (!regexengineType.test(engineType)) {
                                      alert('Please enter a valid engine type ');
                                      return false;
                                  }
                              }

                              if (estAnnualMileage == '') { }
                              else {
                                  if (!regexNumbers.test(estAnnualMileage)) {
                                      alert('Please enter a valid annual mileage value (numbers only) ');
                                      return false;
                                  }
                              }

                              if (HPCCUnitType == '') { }
                              else {
                                  if (!regexHPCCUnitType.test(HPCCUnitType)) {
                                      alert('Please enter a valid HPCCUnit value ');
                                      return false;
                                  }
                              }


                              if (ReferenceNo == '') { }
                              else {
                                  if (ReferenceNo == '' || !regexNumbers.test(ReferenceNo)) {
                                      alert('Please enter a valid reference no (numbers only)');
                                      return false;
                                  }
                              }

                              if (RegistrationLocation == '') { }
                              else {
                                  if (!regexregLoc.test(RegistrationLocation)) {
                                      alert('Please enter a valid registration location ');
                                      return false;
                                  }
                              }

                              if (RoofType == '') { }
                              else {
                                  if (!regexRoof.test(RoofType)) {
                                      alert('Please enter a valid roof type ');
                                      return false;
                                  }
                              }

                              if (seating == '' || seating == '0') { seating = '' }
                              else {
                                  if (!regexNumbers.test(seating)) {
                                      alert('Please enter a valid seating (number) ');
                                      return false;
                                  }
                              }

                              if (seatingCert == '' || seatingCert == '0') { seatingCert == '' }
                              else {
                                  if (!regexNumbers.test(seatingCert)) {
                                      alert('Please enter a valid seating cert (number) ');
                                      return false;
                                  }
                              }

                              if (tonnage == '' || tonnage == '0') { tonnage == '' }
                              else {
                                  if (!regexNumbers.test(tonnage)) {
                                      alert('Please enter a valid tonnage (number) ');
                                      return false;
                                  }
                              }

                              if (handDriveType == '') { }
                              else {
                                  if (handDriveType == '' || !regexTransType.test(handDriveType)) {
                                      alert('Please enter a valid handDriveType ');
                                      return false;
                                  }
                              }

                              if (transtype == '') { }
                              else {
                                  if (!regexHDriveType.test(transtype)) {
                                      alert('Please enter a valid transmission type ');
                                      return false;
                                  }
                              }

                              if (regOwner == '') { }
                              else {
                                  if (!regexRegOwner.test(regOwner)) {
                                      alert('Please enter a valid registered owner name ');
                                      return false;
                                  }
                              }

                              if (perId == null) {
                                  perId = 0;
                              }

                              var maindriver_test = false;
                              if (trn == '') { }
                              else {
                                  if (!regexTRN.test(trn)) {
                                      alert('Please enter a valid TRN. TRN Must be a nine digit number, beginning with a 0 or 1');
                                      return false;
                                  }
                                  else maindriver_test = true;
                              }

                              if (maindriver_test) {
                                  if (fname == '' || !regex.test(fname)) {
                                      alert('Please enter a valid first name.');
                                      return false;
                                  }

                                  if (mname == '') { }
                                  else {
                                      if (!regex1.test(mname)) {
                                          alert('Please enter a valid middle name.');
                                          return false;
                                      }
                                  }

                                  if (lname == '' || !regex.test(lname)) {
                                      alert('Please enter a valid last name.');
                                      return false;
                                  }

                                  if (roadno == '') { }
                                  else {
                                      if (!regex2.test(roadno)) {
                                          alert('Please enter a valid road number.');
                                          return false;
                                      }
                                  }

                                  if (aptNumber != '' && !regexAptNum.test(aptNumber)) {
                                      alert('Please enter a valid apartment/building number.');
                                      return false;
                                  }


                                  if (roadType == '') { }
                                  else {
                                      if (!regex.test(roadType)) {
                                          alert('Please enter a valid road type.');
                                          return false;
                                      }
                                  }

                                  if (zip == '') { }
                                  else {
                                      if (!regex2.test(zip)) {
                                          alert('Please enter a valid zip.');
                                          return false;
                                      }
                                  }


                                  if (city == '' && !regexCity.test(city)) {
                                      alert('Please enter a valid city.');
                                      return false;
                                  }

                                  if (parish == 0) {
                                      alert('Please select a parish.');
                                      return false;
                                  }

                                  if (country == '' || !regex4.test(country)) {
                                      alert('Please enter a valid country name.');
                                      return false;
                                  }
                              }
                              

                              var parms =
                    {
                        vin: vin,
                        chassis: chassis,
                        make: make,
                        model: model,
                        modelType: modelType,
                        bodyType: bodyType,
                        Extension: Extension,
                        vehicleRegNumber: vehicleRegNumber,
                        colour: colour,
                        cylinders: cylinders,
                        engineNo: engineNo,
                        engineModified: engineModified,
                        engineType: engineType,
                        estAnnualMileage: estAnnualMileage,
                        estimatedvalue: est_val,
                        expiryDateOfFitness: expiryDateOfFitness,
                        HPCCUnitType: HPCCUnitType,
                        ReferenceNo: ReferenceNo,
                        RegistrationLocation: RegistrationLocation,
                        RoofType: RoofType,
                        seating: seating,
                        seatingCert: seatingCert,
                        tonnage: tonnage,
                        VehicleYear: VehicleYear,
                        handDriveType: handDriveType,
                        transtype: transtype,
                        regOwner: regOwner,
                        restrictedDriving: restrictedDriving,

                        perId: perId,
                        trn: trn,
                        fname: fname,
                        mname: mname,
                        lname: lname,
                        roadno: roadno,
                        aptNumber: aptNumber,
                        road: road,
                        roadType: roadType,
                        zipcode: zip,
                        city: city,
                        country: country,
                        email: emails,
                        primary: primary,
                        parish: parish,

                        advanced: advanced,

                        prevChassis: prevChassis,
                        prevRegNo: prevRegNo,
                        update: true
                    };

                              $.ajax({
                                  type: "POST",
                                  traditional: true,
                                  url: "/Vehicle/AddVehicles/",
                                  async: false,
                                  data: parms,
                                  dataType: "json",
                                  success: function (data) {
                                      if (!data.result && data.regExist) {

                                          var prevRegNo = $("#prevRegNo").val();
                                          var prevChassis = $("#prevChassis").val();
                                          var currentChassis = $("#chassis").val();
                                          var RegNoString = $("#vehicleRegNum").val();

                                          if (prevChassis == "" ||
                                            (prevRegNo.trim() != RegNoString.trim() && prevChassis.trim() != currentChassis.trim()) ||
                                            (prevRegNo.trim() != RegNoString.trim() && prevChassis.trim() == currentChassis.trim()) ||
                                            (prevRegNo.trim() == RegNoString.trim() && prevChassis.trim() != currentChassis.trim()))
                                              $("<div> The Vehicle Registration Number is already in use.</div>").dialog({
                                                  modal: true,
                                                  title: "Alert",
                                                  buttons: {
                                                      Ok: function () {
                                                          $(this).dialog("close");
                                                      }
                                                  }
                                              });
                                      }
                                      else {
                                      
                                          
                                          $('#vehicle-modal').dialog("close");
                                          $('#vehicle-modal').html("");
                                          $('#vehicle-modal').dialog("destroy");
                                          $('<div>Vehicle has been modified!</div>').dialog({
                                            modal:true,
                                            title:'Alert',
                                            buttons:{
                                                OK:function(){
                                                //window.location.replace('/Policy/Details/' + $('#hiddenid').val());
                                                $(this).dialog('close');
                                                $(this).dialog("destroy");
                                                //$("#vehicle-modal").replaceWith('<div id=\"vehicle-modal\"></div>');
                                                var vehParams;
                                                switch($('#page').val())
                                                {
                                                    case "create":
                                                        vehParams = {test:vehID, startDate:start, endDate:end, type:"Add", VehicleUsage:usage, polid:polid, page:"create"};
                                                        break;
                                                    case "edit":
                                                        vehParams = {test:vehID, startDate:start, endDate:end, type:"Edit",CoverID: cover,VehicleUsage:usage, polid:polid};
                                                        break;
                                                    case "details":
                                                        vehParams = {test:vehID, startDate:start, endDate:end, type:"details", CoverID: cover, VehicleUsage:usage, polid:polid, imported:imported};
                                                        break;
                                                    default:
                                                    break;
                                                }

                                                
                                                $.ajax({
                                                type: "POST",
                                                traditional: true,
                                                url: "/Vehicle/Vehicle/",
                                                async: false,
                                                data: vehParams,
                                                dataType: "html",
                                                success: function (html) {
                                                        //html = $(html).find('.usage_dropdown').addClass('ok');
                                                        var vehiclePartial = $(html);
                                                        if($('#page').val() == 'details'){
                                                            $(vehiclePartial).find('.usage_dropdown').prop('disabled',true);
                                                            $(vehiclePartial).find('.vup_mortgagee').prop('disabled',true);
                                                            $(vehiclePartial).find('.vup_mortgagee').prop('readonly',true);
                                                        }
                                                        if($('#page').val() == 'create'){
                                                            loadDropDown(vehiclePartial.find('.usage_dropdown'));
                                                        }
                                                        vehicleCard.replaceWith(vehiclePartial);
                                                        $('.usage_dropdown').trigger('change');

                                                        $.ajax({
                                                            url: '/Policy/RmRiskToPolicy/' + $('#hiddenid').val() + '?veh_id=' + vehID,
                                                            cache: false,
                                                            async:false
                                                            });
                                                        $.ajax({
                                                            url: '/Policy/AddRiskToPolicy/' + $('#hiddenid').val() + '?veh_id=' + $(html).find('.vehId').val(),
                                                            cache: false,
                                                            async:false
                                                            });

                                                        
                                                    }
                                                });


                                                }
                                            }

                                         });
                                      }
                                  }
                              });

                          }, //DONE button end
                          Cancel: function () {
                              $('#vehicle-modal').html("");
                              $('#vehicle-modal').dialog("close");
                              $('#vehicle-modal').dialog("destroy");
                          }
                      }
                  });
                  //$('#vehicle-modal').dialog("destroy");
                  }
                  else{
                
                        $('<div>Please use the Edit policy Page to edit Vehicle</div>').dialog({
                            title:'Alert!',
                            modal:true,
                            buttons : {
                                OK:function(){
                                    $(this).dialog('close');
                                }
                            }
                        });
                     }
              }
             
             
            }
    });

    //$("#vehicle-modal").load("/Vehicle/FullVehicleDetails/" + chassisNum).dialog('open');
    }
});




$(document).on("click", '.companyNames', function () {
         var compView = $(this).parents('.company');
         compID = $(this).parent().parent().children(".comId").val();
         var CompTRN;
         var CompImportID = $(this).parents('.company').find('.comImportID').val();
        $("#company-modal").dialog({
            modal: true,
            autoOpen: false,
            width: 400,
            title: "Company Details",
            height: 400,
            buttons:
                    {
                         OK: function () {
                             $('#company-modal').dialog('close');
                             $('#company-modal').html("");
                             $('#company-modal').dialog("destroy");

                         },
                        Edit: function () {
                                if(CompImportID == ""){
                                     CompTRN = $('.company_trn').html().trim();
                                     $('#company-modal').dialog('close');
                                     $('#company-modal').html("");
                                     $('#company-modal').dialog('destroy');
                                     $('#company-modal').dialog({
                                                 modal: true,
                                                 width: 370,
                                                 title: "Edit Policy Holder (Company)",
                                                 height: 590,
                                                 buttons: {
                                                    Save: function () {
                                                        var regex = /^[a-zA-Z][A-Za-z-.\' ]*[a-zA-Z ]$/;
                                                        var regex1 = /^[a-zA-Z][A-Za-z-\']*[a-zA-Z ]*$/;
                                                        var regex2 = /^[a-zA-Z0-9][a-zA-Z0-9#\-/&. ]*$/;
                                                        var regex3 = /^[a-zA-Z][a-zA-Z0-9.&,#\-\' ]*$/;
                                                        var regex4 = /^[a-zA-Z][a-zA-Z.&,#\-\' ]*[a-zA-Z ]$/;
                                                        var regexAptNum = /^[a-zA-Z0-9-\/#&.\' ]*$/;
                                                        var regexComTRN = /^(([ ]*[01][0-9]{12}[ ]*)|([ ]*[0][0-9]{8}[ ]*))$/;
                                                        var regexComTRN1 = /^[ ]*[0][0-9]{8}[ ]*$/;
                                                        var regexCity = /^(([a-zA-Z](([a-zA-Z0-9.& '-])*[a-zA-Z0-9])?)|([a-zA-Z](([a-zA-Z0-9.& '-])*([ ])?[(]([a-zA-Z0-9.& '-])+[a-zA-Z0-9][)])?))$/;


                                                        var CompanyName = $("#CompanyName").val().trim();
                                                        var roadno = $("#company-modal").find("#RoadNumber").val().trim();
                                                        var aptNumber = $("#company-modal").find("#aptNumber").val().trim();
                                                        var road = $("#company-modal").find("#roadname").val().trim();
                                                        var roadType = $("#company-modal").find("#roadtype").val().trim();
                                                        var city = $("#company-modal").find("#city").val().trim();
                                                        var country = $("#company-modal").find("#country").val().trim();
                                                        var trn = $("#company-modal").find("#trn").val().trim();
                                                        var parish = $("#company-modal").find(".parishes").val().trim();

                                                        var compId = 0;
                                                        var editCompany = ($("#CompanyName").prop('disabled') == true);

                                                        /////// VALIDATION CHECKS ///////

                                                        
                                                        if (roadno == '') { }
                                                        else {
                                                            if (!regex2.test(roadno)) {
                                                                alert('Please enter a valid road number.');
                                                                return false;
                                                            }
                                                        }
                                                        if (aptNumber == '') { }
                                                        else if (!regexAptNum.test(aptNumber)) {
                                                            alert('Please enter a valid apartment/building number.');
                                                            return false;
                                                        }
                                                        if (parish == 0 || typeof parish == 'undefined') {
                                                            alert('Please select a parish.');
                                                            return false;
                                                        }
                                                        if (road == '') { }
                                                        else if (!regex3.test(road)) {
                                                            alert('Please enter a valid road name.');
                                                            return false;
                                                        }
                                                        if (roadType == '') { }
                                                        else {
                                                            if (!regex.test(roadType)) {
                                                                alert('Please enter a valid road type.');
                                                                return false;
                                                            }
                                                        }
                    
                                                        if (city == '' && !regexCity.test(city)) {
                                                            alert('Please enter a valid city.');
                                                            return false;
                                                        }
                                                        if (country == '' && !regex4.test(country)) {
                                                            alert('Please enter a valid country name.');
                                                            return false;
                                                        }
                                                        var parms = { 
                                                            CompanyName: CompanyName, 
                                                            TRN: trn, 
                                                            roadno: roadno, 
                                                            aptNumber: aptNumber,
                                                            road: road, 
                                                            roadType: roadType, 
                                                            city: city, 
                                                            country: country, 
                                                            parishes: parish
                                                        };

                                                        $.ajax({
                                                            type: "POST",
                                                            url: "/Company/AddNewCompany/",
                                                            data: parms,
                                                            dataType: "json",
                                                            success: function (data) {
                                                                if (data.result) {
                                                                    $('#company-modal').dialog('close');
                                                                    $('#company-modal').html("");
                                                                    $('#company-modal').dialog("destroy");
                                                                    $('<div>PolicyHolder has been modified!</div>').dialog({
                                                                         modal:true,
                                                                        title:'Alert',
                                                                         buttons:{
                                                                            OK:function(){
                                                                            //window.location.replace('/Policy/Details/' + $('#hiddenid').val());

                                                                            var compParams = 
                                                                            {
                                                                             CompanyIdNum:compID, type:  ($('#page').val() == "details") ? "details": null
                                                                            };

                                                                            $.ajax({
                                                                            type: "POST",
                                                                            traditional: true,
                                                                            url: "/Company/CompanyView/",
                                                                            async: false,
                                                                            data: compParams,
                                                                            dataType: "html",
                                                                            success: function (html) {
                                                                                    compView.replaceWith(html);
                                                                                    //$('#company').append(html);

                                                                                    $.ajax({
                                                                                        url:'/Policy/RmPolicyHolderToPolicy/' + $('#hiddenid').val() + '?cid=' + compID,
                                                                                        cache: false,
                                                                                        async:false
                                                                                    });
                                                                                    $.ajax({
                                                                                        url:'/Policy/AddPolicyHolderToPolicy/' + $('#hiddenid').val() + '?cid=' + $(html).find('.comId').val(),
                                                                                        cache: false
                                                                                    });
                                                                                }
                                                                            });

                                                                            $(this).dialog('close');
                                                                            }
                                                                       }

                                         });
                                                                }
                                                            }
                                                        });

                                                 },//END of Done ,
                                               Cancel: function () {
                                                 $('#company-modal').dialog('close');
                                                 $('#company-modal').html("");
                                                 $('#company-modal').dialog("destroy");
                                             }
                           
                                    } //end of Buttons for Edit
                                });//end of dialog


                                $('#company-modal').load('/Company/AddCompanies/',function(){
                                    $.getJSON("/Company/GetCompanyTRN/?TRN=" + CompTRN, function (data) {
                                         if (!data.error) {
                                            if (data.result) {
                                                   $('#CompanyName').val(data.compname);
                                                    $('#trn').val(CompTRN);

                                                    if(data.longAddressUsed)
                                                     $('#longAddress').val(data.longAddress);
                                                    else{
                                                        $('#RoadNumber').val(data.roadno);
                                                        $('#roadname').val(data.roadname);
                                                        $('#roadtype').val(data.roadtype);
                                                        $('#city').val(data.city);
                                                        $("#aptNumber").val(data.aptNumber);
                                                        $("#parish").val(data.parishid);
                                                        $("#country").val(data.country);
                                                    }

                                                    $('#CompanyName').prop('disabled', true);
                                                    $('#CompanyName').toggleClass("read-only");

                                                    $('#foreignIDCheck').prop('disabled',true);
                                                    $('#foreignIDCheck').toggleClass("read-only");

                                                   $("#trn").prop('disabled', true);
                                                   $("#trn").toggleClass("read-only");
                                                   $("#trn,#CompanyName,#RoadNumber,#roadname,#roadtype,#city,#aptNumber,#parish,#country").trigger('keyup');
                                             

                                                    var thisparishId = data.parishid;

                                                    if (thisparishId != 0) {
                                                        $(".parishes").children().each(function () {
                                                            if ($(this).val() == thisparishId) {
                                                                $(this).prop('selected', true);
                                                                return;
                                                            }
                                                        });
                                                   }
                                                   $(".parishes").trigger('change');
                                           }
                                        }
                                   });
                               }).dialog('open');
                            }
                            else{
                                $('<div>You cannot edit this person\'s details here because it was imported and does not have all the relevant information</div>').dialog({
                                    modal:true,
                                    title:'Alert!',
                                    buttons:{
                                        OK:function(){
                                            $(this).dialog('close');
                                        }
                                    }
                                });
                            }
                           }//End of Edit
                        }//End of buttons for first modal
                    }); //end of dialog
   

    $("#company-modal").load("/Company/FullCompanyDetails/" + compID).dialog('open');
});



    $(document).on('change', '.advanced-check', function () {
        if ($(".advanced-check").is(":checked")) $(".vehicle-advanced-data").show();
        else $(".vehicle-advanced-data").hide();
    });





