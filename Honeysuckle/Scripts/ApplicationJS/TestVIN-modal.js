﻿
function getValueModal(vid) {

    var result = false;
    var count = 0;
    
    //counting the amount of vehicles already added to the policy
    $(".vehId").each(function () {
        count++;
    });

    if (count > 0) {

        //storing all the already added Ids in an array
        for (var x = 0; x < count; x++) {
            if ($(".vehId")[x].value == vid) {
                result = true;
                break;
            }
        }
    }
    return result;

}

var vinFocus = false;
var vinSet = false;

$(function () {
    
    if ($("#vin").val().trim().length > 0) vinSet = true;

    $("#vin").focus(function () {
        vinFocus = true;
    })

    $("#vin").blur(function () {
        if (vinFocus && $("#vin").val().trim() != '') {
            var test = $("#vin").val();
            if (test.trim().length > 0) {
                var result = false;
                $.getJSON("/Vehicle/TestVin/?vin=" + test.trim(), function (data) {
                    if (!data.error) {
                        //something exists

                        if (data.vehicle) {
                            //the vehicle exists in the system already

                            $("#InformationDialog").dialog({
                                modal: true,
                                autoOpen: false,
                                width: 310,
                                title: "Vehicle Exists",
                                height: 350,
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog("close");
                                        $("#InformationDialog").html("");
                                        $("#vehicle-modal1").html("");
                                        $("#vehicle-modal1").dialog("close");
                                        AddVehicleSelected(data.vid, 1);

                                    },
                                    Cancel: function () {
                                        $("#vin").val("");
                                        $(this).dialog("close");
                                        $("#InformationDialog").html("");
                                    }
                                }
                            });
                            $("#InformationDialog").load('/Vehicle/FullVehicleDetails/' + data.vid + "?create=" + true, function (value) { }).dialog('open');
                        }
                        else {
                            //VIN FREE
                            if (test.trim().length != 0) vinSet = true;
                            if ($("#chassis").val().length != 0)
                                $("#make").focus();
                        }
                    }
                    else {
                        //ERROR
                        $("<div>" + data.message + "</div>").dialog({
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $("#vin").val("");
                                    $(this).dialog("close");
                                }
                            }
                        });

                    }
                });
                vinFocus = false;
            }
            else {
                if (vinSet) {
                    vinSet = false;
                }
            }
        }
    })

   $("#make,#vehiclemodel,#modeltype,#bodytype,#extension,#vehicleRegNum,#colour,#cylinders,#engineNo,#CengineModified,#engineType,#mileage,#fitnessExpiry,#hpccUnit,#refno,#regLocation,#roofType,#seating,#seatingCert,#tonnage,#vehicleYear,#handDriveType,#tranType,#username,#fname,#mname,#lname,#RoadNo,#RoadName,#RoadType,#ZipCode,#CityName,#CountryName").focus(function () {
    //if (!vinSet && ($("#chassis").val() == '')) { //Is this right? Or should the VIN be compulsary on the modal?
    //    $(this).blur();
    //    $("<div>Please fill the VIN first</div>").dialog({
    //        title: 'Alert',
    //        modal: true,
    //        buttons: {
    //            Ok: function () {
    //                $("#vin").val("");
    //                $(this).dialog("close");
    //            }
    //        }
    //    });
    //}
})
});