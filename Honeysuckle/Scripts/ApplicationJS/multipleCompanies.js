﻿checkIfCompAdded = 0;
compIDNums = [];
selectedComps = [];
companySelected = [];
scrollHeight = 0;
var noInsuredComp = "<div class=\"rowNone\">There Are No Companies. Please add one. </div>";
var InsuredHiddenComp = "<div class=\"rowNone\">Please search the system for existing companies, or add a new company. </div>";

$(function () {
    $("#addCompany").click(function () {
        cid = $("#compid").val();
        $("#company-modal").load('/Company/companyList/' + cid, function (value) {
            if (value.substring(2, 6) == 'fail') { //then user does not have the permissions
                $(this).dialog('close');
                $(this).html("");
                $(this).dialog().dialog("destroy");

                $("<div>You do not have the required permissions to complete this task. </div>").dialog({
                    title: 'Alert!',
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                            $(this).dialog().dialog("destroy");
                        }
                    }
                });
            }
            else {

                companySelected = [];
                var selectedInsureds = $('#company').find(".company");
                   if( selectedInsureds.length > 0 ){
                        
                         for( var i = 0; i < selectedInsureds.length; i++){
                            
                            companySelected.push($(selectedInsureds[i]).find('.comId').val());
                         }
                     }


                if ($(document).find(".company-row").length <= 50){
                 $(document).find(".company-row").show();

                 var companyRows =$(document).find(".company-row");
                  for( var x = 0; x < companySelected.length; x++){
                        for( var i = 0; i < companyRows.length; i++){
                       
                            
                            if( $(companyRows[i]).find('.company-check').find('input').prop('id') == companySelected[x])
                                 $(companyRows[i]).hide();

                        }
                    }
                 }
                else  $(document).find(".company-row").hide();

                scrollHeight = $("#companiesList").height();
                $("#companiesList").perfectScrollbar({
                    wheelSpeed: 0.3,
                    wheelPropagation: true,
                    minScrollbarLength: 10,
                    suppressScrollX: true
                });
                $("#selected-companies").perfectScrollbar({
                        wheelSpeed: 0.3,
                        wheelPropagation: true,
                        minScrollbarLength: 10,
                        suppressScrollX: true
                });
            }
        }).dialog({
            modal: true,
            width: 700,
            title: "Companies List",
            height: 620
        });
    });
});


//Search when enter-button is hit
$(document).on("keyup", ".compSearch", function (event) {
    var text = $(".compSearch").val();
    if (event.which == 13) {
        searchCompanyData(text);
    }
});

function updateCompView() {
    for (var x = 0; x < selectedComps.length; x++) {
        $("#selected-companies").append(selectedComps[x]);
    }
    $("#companiesList").children(".company-row.data").each(function () {
        //If the company is already on display, in the select box, then it doesn't need to be shown in the search area again
        testIfCompAdded($($(this).children(".company-check").children(".companycheck")).prop('id'));
        if (checkIfCompAdded == 1) $(this).hide();
        checkIfCompAdded = 0;
    });
    //Resizing the compaies list div, so the scroll works
    //$("#companiesList").height(scrollHeight);
    if ($("#companiesList").height() + $("#selected-companies").height() < 380) $("#companiesList").height($("#companiesList").height() + 20);
    else if ($("#companiesList").height() + $("#selected-companies").height() >= 380) $("#companiesList").height($("#companiesList").height() - 20);

    $("#companiesList").perfectScrollbar('update');
    $("#selected-companies").perfectScrollbar('update');
}

function testIfCompAdded(id) {
    for (var z = 0; z < compIDNums.length; z++) {
        if (compIDNums[z] == id) checkIfCompAdded = 1;
    }
}

function testBaseCompany(id) {
    var result = false;
    for (var y = 0; y < selectedComps.length; y++) {
        if (selectedComps[y].find(".baseid").val() == id) result = true;
    }
    return result;
}

function removeComp(id) { //This function removes the element from the arrays
    for (var y = 0; y < selectedComps.length; y++) {
        if (compIDNums[y] == id) {
            selectedComps.splice(y, 1);
            compIDNums.splice(y, 1);
        }
    }
    updateCompView();
}


function AddCompanySelected(id, addNew) {
    var err = "<div>There was an error with adding the company. Please try again. If the problem persists, please contact your administrator.</div>";

    //Removing the div that displayes the message (of either no company being present, or company count exceeding 50)
    if (typeof $(".rowNone").val() !== 'undefined') $(".rowNone").remove();

    $.ajax({
        type: "POST",
        url: '/Company/CompanyListItemSelected/' + id,
        cache: false,
        success: function (data) {
            var baseid = $(data).find(".baseid").val();
            if(testBaseCompany(baseid)) {
                $("#selected-companies .company-row, #companiesList .company-row").each(function () {
                    if ( $(this).find(".baseid").val() == baseid ) 
                    $(this).remove();
                });
                for (var y = 0; y < selectedComps.length; y++) {
                    if (selectedComps[y].find(".baseid").val() == baseid) 
                    {
                        selectedComps.splice(y, 1);
                        compIDNums.splice(y, 1);
                    }
                } 
            }
            selectedComps.push($(data));
            compIDNums.push(id);
            updateCompView();
        },
        error: function () {
            $(err).dialog({
                title: "Error",
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog('close');
                    }
                }
            });
        }
    });   //END OF AJAX CALL
    checkIfCompAdded = 0; //Reset the variable
}

function changeCompanyCheckBox(e) {

    //Stores search value, as it breaks if the value is not stored before the div is manipulated
    var searchValue = $(e).parents("#insured").find(".modalSearch").val();
    $(e).parents(".company-row").remove();
    if ($(e).parents(".company-row").hasClass("selected")) {
        var compAlreadyAdded = false;
        $("#companiesList").children(".company-row").each(function () {
            if ($(this).children(".company-check").children(".companycheck").prop('id') == e.id) {
                compAlreadyAdded = true;
                return false;
            }
        });
        if (compAlreadyAdded == false) {
            $("#companiesList").append($(e).parents(".company-row").toggleClass("selected"));
        }
        removeComp(e.id);
    }
    else {
        //$("#selected-companies").append($(e).parent().parent().toggleClass("selected"));
        AddCompanySelected(e.id, 0);
    }
    searchCompanyData(searchValue,true);
}

function searchCompanies(e) {
    var filter = $(e).parents("#insured").find(".modalSearch").val();
    searchCompanyData(filter);
}

function searchCompanyData(filter, checkbox) {
    var checkbox = typeof checkbox == 'undefined' ? false : checkbox;
    $(".rowNone").remove();
    if (filter.trim().length == 0) {
        if ($(document).find(".company-row").length <= 50) {
            $(document).find(".company-row").show();  
            if ($("#companiesList").height() + $("#selected-companies").height() < 380)  $("#companiesList").height(380);
        }
        else $("#companiesList").children(".company-row.data").hide();
       
    }
    else {
        if (typeof $(".rowNone").val() !== 'undefined') $(".rowNone").remove();
        $("#companiesList").children(".company-row.data").each(function () {

            //If the person is already on display, in the select box, then it doesn't need to be shown in the search area again
            testIfCompAdded($($(this).children(".company-check").children(".companycheck")).prop('id'));
            if (checkIfCompAdded != 1) {
                if (/*($(this).children(".company-IAJID").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) ||*/ ($(this).children(".companyName").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1)) {
                    $(this).show();
                    if ($("#companiesList").height() + $("#selected-companies").height() < 380)  $("#companiesList").height($("#companiesList").height() + 20);
                }
                else $(this).hide();
            }
            checkIfCompAdded = 0;

            //Added to test if insured added already, then dont show

            for( var y = 0; y < companySelected.length; y++ ){
                
                if($($(this).children(".company-check")).find('input').prop('id') == companySelected[y]){
                    $(this).hide();
                 }

            }

            /*****/
        });

    } //End of else
    redrawList();

    if($("#companiesList").height() == 0 && !$($(".company-row")[0]).is(':hidden')) $(document).find("#companiesList").prepend(noInsuredComp);
    else if (! $('.company-row').is(':visible') ) $(document).find("#companiesList").prepend(InsuredHiddenComp);
    
    if(!checkbox){
        $(".recordsFound").text("");
        if($("#companiesList").children(".company-row.data:visible").length <= 0) $(".rowNone").prepend("<div class=\"ResultModal\">No Companies found!</div>");
        else
         $(".recordsFound").text($("#companiesList").children(".company-row.data:visible").length + " record(s) found");
     }

    $("#companiesList").perfectScrollbar('update');
    $("#selected-companies").perfectScrollbar('update');
}

function searchOptionsCompany() {
    var names = $('#search-modal').val();

    $("#companiesList").html('<div style = "text-align:center; padding:5px;">Searching for <b>' + names + '</b> please wait....</div>');
    $(".recordsFound").html('waiting....');

    $.ajax({
        type: "POST",
        url: '/Company/GetcompanyList?id=' + cid + "&compType=null&Searchtext=" + names,
        cache: false,
        success: function (data) {
         
           try
           {
            var Response = JSON.parse(data);

           if(Response.Comp.length > 0)
            {
                var RowOneData = "";
                var BodyData = "";
                if(data.length <= 0 )
                {
                    RowOneData = '<div class="rowNone" id = "SearchBar">There Are No Companies. Please add one.</div>';
                }
                else
                {
                     $("#SearchBar").html("");
                     RowOneData = '<div></div>';
                }
                 
                for (var dt = 0; dt < Response.Comp.length; dt++)
                    {
                        var TRN = "";
                        if (Response.Comp[dt].trn == "1000000000000") TRN = "Not Available"; else TRN = Response.Comp[dt].trn;
                           
                        if (dt % 2 == 0) 
                        {  
                             BodyData += '<div class="company-row data even row">';  
                        } 
                        else 
                        {  
                            BodyData += '<div class="company-row data odd row">';  
                        } 
                    
                     
                            BodyData += '<div class="company-check select item">';
                            BodyData += '<input type="checkbox" class = "companycheck" id = "' + Response.Comp[dt].CompanyID + '" OnClick = "changeCompanyCheckBox(this)" />';
                            BodyData += '</div>';

                            BodyData += '<div class="company-trn trn item">';
                            BodyData += '<label>' + TRN + '</label>';
                            BodyData += '</div>';
                            BodyData += '<div class="companyName item">';
                            BodyData += '<label>' + Response.Comp[dt].CompanyName + '</label>';
                            BodyData += '</div>';
                            BodyData += '</div>';

                    }
                    $(".recordsFound").html('Found ' +  Response.Comp.length + ' Record(s).');
                    $("#companiesList").html(RowOneData  + BodyData );

            }else
            {
                $(".recordsFound").html('Found ' +  Response.Comp.length + ' Record(s).');
                $("#companiesList").html('<div style = "text-align:center; padding:5px; color:red;">No data found for <b>' + names + '</b>.</div>');
            }
           
           }catch(e)
           {
            $(".recordsFound").html('Found 0 Record(s).');
            $("#companiesList").html('<div style = "text-align:center; padding:5px; color:red;"> <b>There was an error. Please try again.</b> </div>');

           }
                

            

           
        },
        error: function () {
            $(err).dialog({
                title: "Error",
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog('close');
                    }
                }
            });
        }
    });

 
};

$(function () {
    $(document).on("click", ".remCompany", function (e) {
           
        e.preventDefault();
        var deleteItem = $(this).parents("div.company:first");
            
        $("<div>You are about to delete this user company. Are you sure you want to do this? </div>").dialog({
            modal: true,
            title: 'Alert!',
            buttons: {
                Ok: function () {
                    deleteItem.remove();
                    //if ($("#page").val() == "edit") rm_company_policyholder_ajax($("#hiddenid").val(), $(deleteItem).find(".comId").val());
                    RemoveCompanyHolder((deleteItem).find(".comId").val());
                    update_type_all();
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                },

                Cancel: function () {
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                }
            }
        });
    });
    
    $("#buttonAdd").click(function () {
        getValues();
    });
    
    $("#create").click(function () {
        create();
    });
});


//closing the dialog window
function closeDiag() {
    $("#company-modal").dialog("close");
    $("#company-modal").html("");

    selectedComps = [];
    compIDNums = [];
}

function create() {

    //regExpressions for validation
    var regexIAJID = /^[0-9][0-9\-]*[0-9]$/;
    var regexCompName = /^[a-zA-Z0-9][a-zA-Z0-9\-@!?\/#&$+,.\' ]*$/;

    var regex = /^[a-zA-Z][A-Za-z-.\' ]*[a-zA-Z ]$/;
    var regex1 = /^[a-zA-Z][A-Za-z-\']*[a-zA-Z ]*$/;
    var regex2 = /^[a-zA-Z0-9][a-zA-Z0-9#\-/&. ]*$/;
    var regex3 = /^[a-zA-Z][a-zA-Z0-9.&,#\-\' ]*$/;
    var regex4 = /^[a-zA-Z][a-zA-Z.&,#\-\' ]*[a-zA-Z ]$/;
    var regexAptNum = /^[a-zA-Z0-9-\/#&.\' ]*$/;
    var regexComTRN = /^[ ]*[01][0-9]{12}[ ]*$/;
    var regexComTRN1 = /^[ ]*[0][0-9]{8}[ ]*$/;
    var regexCity = /^(([a-zA-Z](([a-zA-Z0-9.& '-])*[a-zA-Z0-9])?)|([a-zA-Z](([a-zA-Z0-9.& '-])*([ ])?[(]([a-zA-Z0-9.& '-])+[a-zA-Z0-9][)])?))$/;

    $("#company-modal1").load('/Company/AddCompanies').dialog({
        modal: true,
        //autoOpen: false,
        width: 370,
        title: "Company Creation Form",
        height: 590,

        buttons: {
            Create: function () {

                var CompanyName = $("#CompanyName").val().trim();
                var IsForeignID = $("#company-modal1").find("#trn").hasClass('foreignID') == true ? true : false;
                var noTRN = $("#company-modal1").find("#trn").hasClass('blank-trn') == true ? true : false;
                var roadno = $("#company-modal1").find("#RoadNumber").val().trim();
                var aptNumber = $("#company-modal1").find("#aptNumber").val().trim();
                var road = $("#company-modal1").find("#roadname").val().trim();
                var roadType = $("#company-modal1").find("#roadtype").val().trim();
                var city = $("#company-modal1").find("#city").val().trim();
                var country = $("#company-modal1").find("#country").val().trim();
                var trn = $("#company-modal1").find("#trn").val().trim();
                var parish = $("#company-modal1").find(".parishes ").val().trim();
                //if(IsForeignID) $("#company-modal1").find(".parishes ").removeClass('parishes');

                var compId = 0;
                var editCompany = ($("#CompanyName").prop('disabled') == true);

                /////// VALIDATION CHECKS ///////

                if ( !IsForeignID && !noTRN && (!regexComTRN.test(trn) && !regexComTRN1.test(trn))) {
                    alert('Please enter a valid TRN. The Company TRN must be 13 digits long when \'Trading As\' applies, or begin with a 0 when 9 digits long.');
                    return false;
                }else if(trn == '' && !noTRN ){
                    alert('Please enter a valid TRN or ID (in case of foreigners)');
                    return false;
                }
                if (CompanyName == '' || !regexCompName.test(CompanyName)) {
                    alert('Please enter a valid company name.');
                    return false;
                }
                if (roadno == '') { }
                else {
                    if (!regex2.test(roadno)) {
                        alert('Please enter a valid road number.');
                        return false;
                    }
                }
                if (aptNumber == '') { }
                else if (!regexAptNum.test(aptNumber)) {
                    alert('Please enter a valid apartment/building number.');
                    return false;
                }
                if ((parish == 0 || typeof parish == 'undefined') && !IsForeignID) {
                    alert('Please select a parish.');
                    return false;
                }
                if (road == '') { }
                else if (!regex3.test(road)) {
                    alert('Please enter a valid road name.');
                    return false;
                }
                if (roadType == '') { }
                else {
                    if (!regex.test(roadType)) {
                        alert('Please enter a valid road type.');
                        return false;
                    }
                }
//                if (zip == '') { }
//                else {
//                    if (!regex2.test(zip)) {
//                        alert('Please enter a valid zip.');
//                        return false;
//                    }
//                }
                //if (city == '') { }
                //else {
                if (city == '' && !regexCity.test(city)) {
                    alert('Please enter a valid city.');
                    return false;
                }
                //}
                if (country == '' && !regex4.test(country)) {
                    alert('Please enter a valid country name.');
                    return false;
                }
                var parms = { 
                    CompanyName: CompanyName, 
                    TRN: trn, 
                    roadno: roadno, 
                    aptNumber: aptNumber,
                    road: road, 
                    roadType: roadType, 
                   // zipcode: zip, 
                    city: city, 
                    country: country, 
                    parishes: parish
                };

                $.ajax({
                    type: "POST",
                    url: "/Company/AddNewCompany/",
                    data: parms,
                    dataType: "json",
                    success: function (data) {
                        if (data.result) {
                            compId = data.compId;

                            
                            /*********************************************************************************/
                            if(!editCompany) {

                                $("#compContact").load('/Company/compContacts/' + compId).dialog({
                                    modal: true,
                                    title: "Company Contacts",
                                    height: 700,
                                    width:900,

                                    buttons: {
                                        Continue: function () {

                                            var regexEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9])+$/;
                                            var regexNumber = /^[(][0-9]{3}[)][0-9]{3}[-][0-9]{4}$/;
                                            var regexExt = /^([0-9-()+ ]*)$/;

                                            var comp = $("#compID").val();
                                            var compBase = $("#compBaseID").val();
                                            var validData = true;
                                            var count = 0;


                                            var emails = [];
                                            var primary = [];
                                            var phoneNum = [];
                                            var ext = [];
                                            var countryCode = [];
                                            var primaryPhone = [];

                                            $(".email").each(function () {
                                                emails[emails.length] = $(this).val();
                                                if (!regexEmail.test(emails[count])) {
                                                    alert('An email address entered was invalid.');
                                                    validData = false;
                                                    return false;
                                                }
                                                count++;
                                            });
                                           // if (validData){
                                                $(".telNum").each(function () {
                                                    phoneNum[phoneNum.length] = $(this).val();
                                                    if (!regexNumber.test(phoneNum[phoneNum.length - 1]) || phoneNum[phoneNum.length - 1] == '') {
                                                        alert('An phone number entered was invalid. Only numbers, an hyphen, and brackets allowed!. Eg. (876)567-4567');
                                                        validData = false;
                                                        return false;
                                                    }
                                                });
                                           // }
                                            //if (validData){
                                                $(".extNum").each(function () {
                                                    ext[ext.length] = $(this).val();
                                                    if (!regexExt.test(ext[ext.length - 1])) {
                                                        alert('An phone number extension entered was invalid or empty');
                                                        validData = false;
                                                        return false;
                                                    }
                                                });
                                            //}
                                            if (validData){
                                                $(".CountryCode").each(function () {
                                                    countryCode[countryCode.length] = $(this).val();
                                                    if (!regexNumber.test(countryCode[countryCode.length - 1])) {
                                                        alert('An country code entered was invalid. Numbers only!');
                                                        validData = false;
                                                        return false;
                                                    }
                                                });
                                            }
                                            if (validData) {
                                                $(".emailprimary").each(function () {
                                                    primary[primary.length] = $(this).prop("checked");
                                                });
                                                $(".phonePrimary").each(function () {
                                                    primaryPhone[primaryPhone.length] = $(this).prop("checked");
                                                });
                                                var parmeters = {
                                                    compId: compBase, 
                                                    email: emails, 
                                                    primary: primary, 
                                                    numbers: phoneNum, 
                                                    ext: ext,
                                                    countrycode: countryCode, 
                                                    primaryPhone: primaryPhone
                                                };
                                                $.ajax({
                                                    type: "POST",
                                                    traditional: true,
                                                    url: "/Company/CompanyContactInfo/",
                                                    async: false,
                                                    data: parmeters,
                                                    dataType: "json",
                                                    success: function (data) { }
                                                });
                                                $("#compContact").dialog('close');
                                                $("#company-modal1").html("");
                                                AddCompanySelected(comp, 1);
                                            } //END of If (validData Test)
                                        },
                                         Cancel: function(){
                                            try
                                            {
                                                $(this).dialog("close");
                                                $(this).dialog().dialog("destroy");
                                            }catch(e){}
                                    }
                                        
                                         // END of done


                                    } //END of buttons
                                });
                                //$("#compContact").load('/Company/compContacts/' + compId).dialog('open');
                            }// if(!editCompany) {
                            else{
                             AddCompanySelected(compId, 1);
                          }
                            /*******************************************************************************/
                        }
                        else { //The TRN/Company Name is already in the system ((((NEEDS WORKING ON)))
                            alert('The TRN you have entered already exists in the system. Please search for the company in the list, or enter another company TRN.');
                            return false;
                            $("#trn").val("");
                        } //END OF ELSE*/
                    }
                });

                if($("#editingPolCompany").val() == true || $("#editingPolCompany").val() == "true"){
                            
                                $('<div>The policy holder\'s record has been saved</div>').dialog({
                                    modal:true,
                                    title: 'Company Policy Holder Saved',
                                    buttons:{
                                        OK: function(){
                                            $(this).dialog('close');
                                        }
                                    }
                                 });
                  }
                $("#company-modal1").html("");
                $("#company-modal1").dialog('close');
            }, // END OF CREATE BUTTON
            Cancel: function () {
                $("#company-modal1").dialog("close");
                $("#company-modal1").html("");
                updateCompView();
            }
        } // END OF BUTTONS
    }); // END OF 'company-modal1' dialog
} // END OF Create function



function getValues() {

    var chkArray = [];
    var checkAdd = 0;
    var compIds = [];

    var valIds = []; //store the values of the ids added already
    var count = 0;
    var count1 = 0;

    //counting the amount of companies already added to the policy
    $(".comId").each(function () {
        count++;
    });


    //counting the amount of companies selected
    $(".companycheck:checked").each(function () {
        count1++;
    });

    if (count1 == 0) //No company has been selected 
    {
        $('<div>You have not selected a company to be added. Please do so, and try again.</div>').dialog({
            title: 'Alert',
            modal:true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    return false;
                }
            }
        });
    }
    else {
        if (count > 0) {
            var policyHolderReAdd = false;
            //stroring all the already added Ids in an array
            for (var x = 0; x < count; x++) 
            {
                valIds[x] = $(".base_comp_id")[x].value;
            }

            //checking each selected ids against the ones already added
            for (var y = 0; y < count1; y++) {
                for (var z = 0; z < count; z++) {
                    if ($($(".companycheck:checked")[y]).parents(".company-row").find(".baseid").val() == valIds[z]) 
                    {
                        checkAdd = 1;
                        policyHolderReAdd = true;
                    }
                }
                if (checkAdd != 1) compIds[compIds.length] = $(".companycheck:checked")[y].id;
                checkAdd = 0; //variable is reset
            } //END OF outer FOR
            //IF AT LEAST ONE OF THE POLICY HOLDERS WAS ALREADY ADDED TO THE PAGE
            if (policyHolderReAdd) {
                $('<div> One or more policyholder that you have selected has already been chosen to be added to this policy. This policyholder(s) will not be added again. However, any other chosen one will be. </div>').dialog({
                    title: 'Alert',
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }

            for (var x = 0; x < compIds.length; x++) {
                $.ajax({
                    type: "POST",
                    url: '/Company/CompanyView/?CompanyIdNum=' + compIds[x],
                    cache: false,
                    success: function (html) { 
                        $("#company").append(html); 
                        update_type_all();
                        AddCompanyPolicyHolderToDropDown($(html).find(".comId").val(), $(html).find(".companyNames").val());
                        //if ($("#page").val() == "edit") add_company_policyholder_ajax($("#hiddenid").val(), $(html).find(".comId").val());
                    }
                });
            }
        }
        else {
            $(".companycheck:checked").each(function () {
                $.ajax({
                    type: "POST",
                    url: '/Company/CompanyView/?CompanyIdNum=' + $(this).prop('id'),
                    cache: false,
                    success: function (html) { 
                        $("#company").append(html); 
                        AddCompanyPolicyHolderToDropDown($(html).find(".comId").val(), $(html).find(".companyNames").val());
                        update_type_all();
                        //if ($("#page").val() == "edit") add_company_policyholder_ajax($("#hiddenid").val(), $(html).find(".comId").val());
                    }
                });
            });
        }

        $("#company-modal").html("");
        $("#company-modal").dialog('close');

    }

    compIDNums = [];
    selectedComps = [];
    checkIfCompAdded = 0;
} // ENF OF 'getValues' function


function AddCompanyPolicyHolderToDropDown(id, name){
    var len = $("#insuredsdropdown option").length;
    var goahead = true;
    if (len == 0) {
        ActuallyAddHolder('c' + id, name);
    }
    else {
        $("#insuredsdropdown option").each(function(index, element){
            if ($(this).val() == 'c' + id && goahead) {
                goahead = false;
            }
            if (index == len -1) {
                if (goahead){
                    ActuallyAddHolder('c' + id, name);
                }
            }
        });
    }
}

function ActuallyAddCompanyHolder(id, name){
    $(".pidrop").append("<option value=\""+ id + "\">" + name + "</option>");
}

function RemoveCompanyHolder(id){
    $(".pidrop option").each(function(index, element){
        if ($(this).val() == 'c' + id) {
            if ($(this).prop('selected')) DefaultInsuredsDropDown();
            $(this).remove();
            return false;
        }
    });
}
/*
function add_company_policyholder_ajax(policy_id, company_id){
    $.ajax({
        url:'/Policy/AddPolicyHolderToPolicy/' + policy_id + '?cid=' + company_id,
        cache: false,
        success: function(json){
            if (json.result){
                policy_holder_message(json.name, 'add');
            }
            else {
                policy_holder_message("", 'err');
            }
        },
        error: function(){
            policy_holder_message("", 'err');
        }
    });
}

function rm_company_policyholder_ajax(policy_id, company_id) {
    $.ajax({
        url:'/Policy/RmPolicyHolderToPolicy/' + policy_id + '?cid=' + company_id,
        cache: false,
        success: function(json){
            if (json.result){
                policy_holder_message(json.name, 'rm');
            }
            else {
                policy_holder_message("", 'err');
            }
        },
        error: function(){
            policy_holder_message("", 'err');
        }
    });
}

function policy_holder_message(message, type){
    var _class = "";
    switch(type){
        case 'add':
            _class = "success-message";
            message = "The policy holder " + message + " has been added to the policy.";
            break;
        case 'rm':
            _class = "remove-message";
            message = "The policy holder " + message + " has been removed from the policy.";
            break;
        case 'err':
            _class = "error-message";
            message = "There was an error in adding the policy holder " + message + ". Please try again.";
            break;
        default:
            break;
    }
    $(".policyholders .processed-responses").append("<div class=\"" + _class + "\">" + message + "</div>");
    clean_up_server_responses_company()
}

function clean_up_server_responses_company() {
    $(".policyholders .processed-responses").find(".success-message:hidden,.error-message:hidden").remove();
    $(".policyholders .processed-responses").children().each(function () {
        var div = $(this);
        setTimeout(function () {
            company_response_fade_out(div);
        }, 5000);
    });
}

function company_response_fade_out(e) {
    $(e).fadeOut("slow");
}
*/