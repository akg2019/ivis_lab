﻿
$(function () {
    $("#approve").click(function () {

        var policyId = $("#policyId").val();
        var start = $("#start").val();
        var end = $("#end").val();

        var parms = {
            policyId: policyId,
            startDateTime: start, 
            endDateTime: end
        };

        $.ajax({
            type: "POST",
            traditional: true,
            url: "/Policy/ApprovePolicy/",
            async: false,
            data: parms,
            dataType: "json",
            success: function (data) {
                window.location.replace("/Policy/Index/");
            }
        });

    });
});  //end of MAIN Function
