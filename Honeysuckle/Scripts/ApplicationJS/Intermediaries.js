﻿
var checkIfCompAdded = 0;
var compIDNums = [];
var selectedComps = [];
var onpageComps = [];
var scrollHeight;

var savebtn = "<input type=\"button\" value=\"Save\" class=\"savechanges inter-btn\"/>";
var enablebtn = "<input type=\"button\" value=\"Enable\" class=\"enableInter inter-btn\"/>";
var disablebtn = "<input type=\"button\" value=\"Disable\" class=\"disableInter inter-btn\"/>";
var datasavesuccess = "<span class=\"success\">Success!</span>";
var noIntermediary = "<div class=\"rowNone\">There Are No Intermediaries. Please add one. </div>";
var ItermediaryHidden = "<div class=\"rowNone\">Please search the system for existing intermediaries. </div>";

function getCurrrentComps() {
    onpageComps = [];
    $(".hidcid").each(function () {
        onpageComps.push($(this).val());
    });
}
$(function () {


    $("body").on('mouseenter', '.success', function () {
        $(this).fadeOut(800);
    });
    
    $("#addIntermediary").click(function () {

        window.location.replace('/Intermediary/Create');

    });


    

    //Search when enter-button is hit
    $(document).on("keyup", ".modalSearch", function (event) {

        var text = $(".modalSearch").val();
        if (event.which == 13) {
            searchCompanyData(text);
        }
    });


    $(document).ready(function () {
        $("#buttonAdd").click(function () {
            getValues();
        });
    });

});

function closeDiag() {
    $("#company-modal").dialog("close");
    $("#company-modal").html("");

    selectedComps = [];
    compIDNums = [];
}


function updateCompView() {
    for (var x = 0; x < selectedComps.length; x++) {
        $("#selected-companies").append(selectedComps[x]);
    }

    //Resizing the compaies list div, so the scroll works

    if ($("#companiesList").height() + $("#selected-companies").height() < 380)
        $("#companiesList").height($("#companiesList").height() + 20);

    else if ($("#companiesList").height() + $("#selected-companies").height() >= 380)
        $("#companiesList").height($("#companiesList").height() - 20);

}

function testIfCompAdded(id) {
    for (var z = 0; z < compIDNums.length; z++) {
        if (compIDNums[z] == id)
            checkIfCompAdded = 1;
    }
}

function removeComp(id) { //This function removes the element from the arrays
    for (var y = 0; y < selectedComps.length; y++) {
        if (compIDNums[y] == id) {
            selectedComps.splice(y, 1);
            compIDNums.splice(y, 1);
            //scrollHeight += 20;
        }
    }
    updateCompView();
}


function AddCompanySelected(id) {
    var err = "<div>There was an error with adding the person. Please try again. If the problem persists, please contact your administrator.</div>"

    testIfCompAdded(id);

    $.ajax({
        type: "POST",
        url: '/Company/CompanyListItemSelected/' + id,
        cache: false,
        success: function (data) {
            if (checkIfCompAdded != 1) {
                selectedComps.push($(data));
                compIDNums.push(id);
               // scrollHeight -= 20;
            }
            updateCompView();
        },
        error: function () {
            $(err).dialog({
                modal:true,
                title: "Error",
                buttons: {
                    Ok: function () {
                        $(this).dialog('close');
                    }
                }
            })
        }
    });   //END OF AJAX CALL
    checkIfCompAdded = 0; //Reset the variable
}

function searchCompanies(e) {
    var filter = $(e).parents("#insured").find(".modalSearch").val();
    searchCompanyData(filter);
}

function searchCompanyData(filter) {
    $(".rowNone").remove();
    if (filter.trim().length == 0) {
        $("#companiesList").children(".company-row").show();
    }

    else {

        //if (typeof $(".rowNone").val() !== 'undefined') $(".rowNone").remove();

        $("#companiesList").children(".company-row.data").each(function () {
            if (($(this).children(".company-IAJID").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".companyName").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1)) {
                $(this).show();

                if ($("#companiesList").height() + $("#selected-companies").height() < 380)
                    $("#companiesList").height($("#companiesList").height() + 20);

            }
            else {
                $(this).hide();
            }
        });

    } //End of else

    redrawList();

    if ($("#companiesList").height() == 0 && !$($(".company-row")[0]).is(':hidden'))
        $(document).find("#companiesList").prepend(noIntermediary);
    
    else if (! $('.company-row').is(':visible') )
        $(document).find("#companiesList").prepend(ItermediaryHidden);

  }


function searchOptionsCompany() {
    var name = $(event.target).parent().children('#search').val().toLowerCase();
    $(event.target).parent().parent().children('#companiesList').children('.company-row').filter(function (div) {
        for (var i = 0; i < $(this).children().length; i++) {
            if ($($(this).children()[i]).text().toLowerCase().trim().indexOf(name) > -1) {
                //show
                $(this).show();
                break;
            }
            else {
                //uncheck and hide
                if ($(this).children().length - 1 == i) {
                    $(this).children('.company-check').children('input.companycheck').prop('checked', false);
                    $(this).hide();
                }
            }
        }
    });
};


function changeCompanyCheckBox(e) {

    //Stores search value, as it breaks if the value is not stored before the div is manipulated
    var searchValue = $(e).parents("#insured").find(".modalSearch").val();

    $(e).parent().parent().remove();
    if ($(e).parent().parent().hasClass("selected")) {
        var compAlreadyAdded = false;

        $("#companiesList").children(".company-row").each(function () {
            if ($($(this).children(".company-check").children(".companycheck")).prop('id') == e.id) {
                compAlreadyAdded = true;
                return false;
            }
            else compAlreadyAdded = false;
        });

        if (compAlreadyAdded == false) {
            $("#companiesList").append($(e).parent().parent().toggleClass("selected"));
        }
        removeComp(e.id);
    }
    else {
        //$("#selected-companies").append($(e).parent().parent().toggleClass("selected"));
        AddCompanySelected(e.id);
    }
    searchCompanyData(searchValue);
}


$(function () {
/*
    $(document).on("click", ".remCompany", function (e) {

        e.preventDefault();

        var comp = $("#compId").val();
        var intermediateID = $(this).parents("div.company:first").find(".comId").val();
        var Intermediary = $(this).parents("fieldset:first");
        var dia = $(this);
        $("<div>You are about to remove the association of this intermediary with your company. Are you sure you want to do this?</div>").dialog({
            modal: true,
            title: "Delete Alert!",
            buttons: {
                Ok: function () {
                    $(this).dialog("close");

                    var isid = $(Intermediary).find(".interid").val();
                    var id = $(Intermediary).find(".hidcid").val();
                    if (isid != 0) {
                        $.ajax({
                            url: '/Intermediary/RemoveIntermediary/' + id + "?cid=" + $("#compid").val(),
                            cache: false,
                            success: function (data) {
                                if (data.result) {
                                    //worked
                                    Intermediary.remove();
                                }
                                else {
                                    //failed
                                    $("<div>There was an error. Please try again. If the problem persists please contact your administrator.</div>").dialog({
                                        title: 'Error',
                                        buttons: {
                                            Close: function () {
                                                $(this).dialog('close');
                                            }
                                        }
                                    })

                                }

                                return false;
                            }
                        });
                    }
                    else {
                        Intermediary.remove();
                    }
                    $(dia).dialog('close');
                    return false;
                },

                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    });*/

    $("#done").click(function () {
        window.location.replace("/Home/");
    });
});

    function getValues() {

    var chkArray = [];
    var checkAdd = 0;
    var compIds = [];

    var valIds = []; //store the values of the ids added already
    var count = 0;
    var count1 = 0;

    //counting the amount of companies already added to the policy
    $(".hidcid").each(function () {
        count++;
    });

    //counting the amount of companies selected
    $(".companycheck:checked").each(function () {
        count1++;
    });

    if (count1 == 0) //No company has been selected 
    {

        $('<div> You have not selected a intermediary to be added. Please do so, and try again. </div>').dialog({
            title: 'Alert',
            modal:true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    return false;
                }
            }
        });
    }

    else {
        if (count > 0) {

            var IntermediaryReAdd = false;

            //stroring all the already added Ids in an array
            for (var x = 0; x < count; x++) {
                valIds[x] = $(".hidcid")[x].value;
            }

            //checking each selected ids against the ones already added
            for (var y = 0; y < count1; y++) {
                for (var z = 0; z < count; z++)

                    if ($(".companycheck:checked")[y].id == valIds[z]) {
                        checkAdd = 1;
                        IntermediaryReAdd = true;
                    }

                if (checkAdd != 1) {
                    compIds[compIds.length] = $(".companycheck:checked")[y].id;
                }

                checkAdd = 0; //variable is reset

            } //END OF outer FOR


            //IF AT LEAST ONE OF THE POLICY HOLDERS WAS ALREADY ADDED TO THE PAGE
            if (IntermediaryReAdd == true) {

                $('<div>One or more selected intermediary has previously been added to your list of intermediaries. These intermediary(s) therefore will not be added again. However, any other chosen intermediary will be added now.</div>').dialog({
                    title: 'Alert',
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }

            for (var x = 0; x < compIds.length; x++) {

                var goahead = true;
                for (var y = 0; y < onpageComps.length; y++) { //test if selected company is in list already on the page
                    goahead = (!(compIds[x] == onpageComps[y]));
                    if (!goahead) break;
                }

                if (goahead) {

                    var comp = $("#compId").val();
                    var vals = { compId: comp, intermediate: compIds[x] };
                    $.ajax({
                        type: "POST",
                        url: '/Intermediary/GenerateNewIntermediary/' + compIds[x],
                        cache: false,
                        success: function (html) { /*$("#intermediary-data").append(html); */$(html).insertBefore("#addCompany"); }
                    });
                }
            }
        }


        else {
            $(".companycheck:checked").each(function () {

                var goahead = true;
                for (var y = 0; y < onpageComps.length; y++) { //test if selected company is in list already on the page
                    goahead = (!($(this).prop('id') == onpageComps[y]));
                    if (!goahead) break;
                }

                if (goahead) {
                    var comp = $("#compId").val();
                    var vals = { compId: comp, intermediate: $(this).prop('id') };

                    $.ajax({
                        type: "POST",
                        url: '/Intermediary/GenerateNewIntermediary/' + $(this).prop('id'),
                        cache: false,
                        success: function (html) { /*$("#intermediary-data").append(html); */$(html).insertBefore("#addCompany"); }
                    });
                }

            });
        }

        $("#company-modal").html("");
        $("#company-modal").dialog('close');


    }

    selectedComps = [];
    compIDNums = [];

} // ENF OF 'getValues' 
   