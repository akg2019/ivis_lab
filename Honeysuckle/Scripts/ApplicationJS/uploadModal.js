﻿$(function () {

    $('#upload').click(function (e) {
        //$('#UploadDialog').dialog("destroy");
        e.preventDefault();
        $('#UploadDialog').dialog({
            modal: true,
            //autoOpen: false,
            width: 600,
            height: 190,
            title: "Upload Policy Data",
            buttons: {
                Close: function () {
                    $(this).dialog("destroy");
                    $(this).html("");
                }
            }
        });

        $("#UploadDialog").load('/PolicyUpload/Upload').dialog('open');

    });

    $('#checkupload').click(function (e) {
       // $('#UploadDialog').dialog("destroy");
        e.preventDefault();
        $('#UploadDialog').dialog({
            modal: true,
            //autoOpen: false,
            width: 853,
            height: 275,
            title: "Upload Progress",
            buttons: {
                Close: function () {
                    $(this).dialog("close");
                    $(this).html("");
                }
            }
        });

        $("#UploadDialog").load('/PolicyUpload/CheckUpload').dialog('open');

    });

});

$(function () {
    if ($('#UploadMessage').length > 0) {
    if ($('#UploadMessage').html().length > 0) {
        $('#UploadMessage').dialog({
            modal: true,
            title: 'Alert!',
            height:285,
            width:600,
            buttons: {
                OK: function () {
                    $(this).dialog("close");
                    $('#UploadMessage').html("");
                }
            }
        });
       }
    }
});