﻿
var chassisFocus = false;
var chassisSet = false;
var selectedId = "";

$(function () {
    if ($("#chassis").val().trim().length > 0) chassisSet = true;

    $("#chassis").focus(function () {
        chassisFocus = true;
    })

    $("#chassis").blur(function () {
        if (chassisFocus) {
            var test = $("#chassis").val();
            if (test.trim().length > 0) {
                $("body").append('<div class="ui-widget-overlay ui-front temp"><div class="tempmessage"><img src="../../Content/more_images/loading.gif" /><span>Processing Data</span></div></div>');
                var result = false;
                $.getJSON("/Vehicle/GetVehicleByChassis/" + test.trim(), function (data) {
                    $(".ui-front.temp").remove();
                    if (!data.error) {

                        //something exists
                        if (data.vehicle) {
                            //the vehicle exists in the system already
                            var buttons = {
                                Ok: function () {

                                    if ($('.dupChassischeck:checked').length <= 0 && $('#duplicatePartial').length > 0) {
                                        alert('Please select a record or cancel');
                                        return false;
                                    }

                                    if ($('#duplicatePartial').length > 0) {

                                        $.ajax({
                                            type: "POST",
                                            traditional: true,
                                            url: "/Vehicle/GetVehicleFullDetails/?id=" + selectedId,
                                            async: false,
                                            dataType: "json",
                                            success: function (rdata) {
                                                data = rdata;
                                            },
                                            error: function () {
                                                alert('Error receiving data');
                                            }
                                        });
                                    }

                                    $(this).dialog("close");
                                    $(this).dialog("destroy");
                                    $("#InformationDialog").html("");

                                    $("#vehicle-modal1").html("");
                                    $("#vehicle-modal1").dialog("close");
                                    AddVehicleSelected(data.vid, 1);

                                },
                                Edit: function () {

                                    if ($('.dupChassischeck:checked').length <= 0 && $('#duplicatePartial').length > 0) {
                                        alert('Please select a record or cancel');
                                        return false;
                                    }

                                    if ($('#duplicatePartial').length > 0) {

                                        $.ajax({
                                            type: "POST",
                                            traditional: true,
                                            url: "/Vehicle/GetVehicleFullDetails/?id=" + selectedId,
                                            async: false,
                                            dataType: "json",
                                            success: function (rdata) {
                                                data = rdata;
                                            },
                                            error: function () {
                                                alert('Error receiving data');
                                            }
                                        });
                                    }
                                    $("#prevRegNo").val(data.vehicle_data.VehicleRegNumber);
                                    $("#prevChassis").val(test.trim());
                                    $("#vin").val(data.vehicle_data.vin);
                                    $("#make").val(data.vehicle_data.make);
                                    $("#vehiclemodel").val(data.vehicle_data.VehicleModel);
                                    $("#vehicleYear").val(data.vehicle_data.VehicleYear);
                                    $("#vehicleRegNum").val(data.vehicle_data.VehicleRegNumber);
                                    $(".advanced-check").prop("checked", true);
                                    $("#modeltype").val(data.vehicle_data.modelType);
                                    $("#bodytype").val(data.vehicle_data.bodyType);
                                    $("#estimated_value").val(data.vehicle_data.estimatedValue);
                                    $("#extension").val(data.vehicle_data.extension);
                                    $("#colour").val(data.vehicle_data.colour);
                                    $("#fitnessExpiry").val(data.fitnessExpiry);
                                    $("#cylinders").val(data.vehicle_data.cylinders);
                                    $("#engineNo").val(data.vehicle_data.engineNo);
                                    $("#engineModified").prop("checked", data.vehicle_data.engineModified);
                                    $("#engineType").val(data.vehicle_data.engineType);
                                    $("#mileage").val(data.vehicle_data.mileage);
                                    $("#hpccUnit").val(data.vehicle_data.HPCCUnitType);
                                    $("#refno").val(data.vehicle_data.referenceNo);
                                    $("#regLocation").val(data.vehicle_data.registrationLocation);
                                    $("#roofType").val(data.vehicle_data.roofType);
                                    $("#seating").val(data.vehicle_data.seating);
                                    $("#seatingCert").val(data.vehicle_data.seatingCert);
                                    $("#tonnage").val(data.vehicle_data.tonnage);
                                    $("#handDriveType").val(data.vehicle_data.handDriveType);
                                    $("#tranType").val(data.vehicle_data.transmissionType.transmissionType);
                                    $("#regOwner").val(data.vehicle_data.RegisteredOwner);
                                    $("#restrictedDriver").prop("checked", data.vehicle_data.RestrictedDriving);

                                    $('#trn').val(data.vehicle_data.mainDriver.person.TRN);
                                    $('#fname').val(data.vehicle_data.mainDriver.person.fname);
                                    $('#mname').val(data.vehicle_data.mainDriver.person.mname);
                                    $('#lname').val(data.vehicle_data.mainDriver.person.lname);
                                    $('#aptNumber').val(data.vehicle_data.mainDriver.person.address.ApartmentNumber);
                                    $('#country').val(data.vehicle_data.mainDriver.person.address.country.CountryName);
                                    $('#city').val(data.vehicle_data.mainDriver.person.address.city.CityName);
                                    $('#RoadNumber').val(data.vehicle_data.mainDriver.person.address.roadnumber);
                                    $('#roadname').val(data.vehicle_data.mainDriver.person.address.road.RoadName);
                                    $('#roadtype').val(data.vehicle_data.mainDriver.person.address.roadtype.RoadTypeName);

                                    $('#trn,#fname,#mname,#lname,#roadtype,#roadname,#RoadNumber,#city,#aptNumber,#country,#parishes').prop('disabled', false);
                                    $('#trn,#fname,#mname,#lname,#roadtype,#roadname,#RoadNumber,#city,#aptNumber,#country,#parishes').prop('readonly', false);

                                    $('#vehicle-modal1 .parishes').val(data.vehicle_data.mainDriver.person.address.parish.ID);
                                    $('#vehicle-modal1 .parishes').trigger('change');

                                    AdvancedCheckRun(true); //function in MultipleVehicle.js
                                    //$("#make,#vehiclemodel,#modeltype,#bodytype,#extension,#vehicleRegNum,#colour,#cylinders,#engineNo,#CengineModified,#engineType,#mileage,#fitnessExpiry,#hpccUnit,#refno,#regLocation,#roofType,#seating,#seatingCert,#tonnage,#vehicleYear,#handDriveType,#tranType,#username,#fname,#mname,#lname,#RoadNo,#RoadName,#RoadType,#ZipCode,#CityName,#CountryName,#trn").prop('disabled', false);
                                    $("#make,#vehiclemodel,#modeltype,#bodytype,#extension,#vehicleRegNum,#colour,#cylinders,#engineNo,#CengineModified,#engineType,#mileage,#fitnessExpiry,#hpccUnit,#refno,#regLocation,#roofType,#seating,#seatingCert,#tonnage,#vehicleYear,#handDriveType,#tranType,#username,#fname,#mname,#lname,#RoadNo,#RoadName,#RoadType,#ZipCode,#CityName,#CountryName,#trn,#roadno,#roadname,#city,#roadtype,#parishes,#country").trigger('keyup');
                                    $(this).dialog("close");
                                    $(this).dialog("destroy");
                                    $("#InformationDialog").html("");
                                },
                                Cancel: function () {
                                    //$("#chassis").val("");
                                    $("#make,#vehiclemodel,#modeltype,#bodytype,#extension,#vehicleRegNum,#colour,#cylinders,#engineNo,#CengineModified,#engineType,#mileage,#fitnessExpiry,#hpccUnit,#refno,#regLocation,#roofType,#seating,#seatingCert,#tonnage,#vehicleYear,#handDriveType,#tranType,#username,#fname,#mname,#lname,#RoadNo,#RoadName,#RoadType,#ZipCode,#CityName,#CountryName,#trn").prop('disabled', false);
                                    $(this).dialog("close");
                                    $(this).dialog("destroy");
                                    $("#InformationDialog").html("");
                                }
                            };
                            //                            if (data.duplicate) buttons['View Duplicates'] = function () {

                            //                                $('#duplicates').load('/Vehicle/DuplicateVehicles/?chassis=' + test.trim());
                            //                                $('#duplicates').dialog({
                            //                                    modal: true,
                            //                                    width: 470,
                            //                                    height: 380,
                            //                                    title: 'Duplicates',
                            //                                    buttons: {
                            //                                        OK: function () {
                            //                                            $('#duplicates').html('');
                            //                                            $(this).dialog('close');
                            //                                            $(this).dialog('destroy');
                            //                                        }
                            //                                    }
                            //                                });


                            //                            };
                            $("#InformationDialog").dialog({
                                modal: true,
                                autoOpen: false,
                                width: 600,
                                title: "Vehicle Exists",
                                height: 395,
                                buttons: buttons
                            });

                            if (data.duplicate) {
                                $("#InformationDialog").load('/Vehicle/DuplicateVehicles/?chassis=' + test.trim(), function (value) { }).dialog('open');

                            }
                            else {
                                $("#InformationDialog").load('/Vehicle/FullVehicleDetails/' + data.vid + "?create=" + true + "&taj=" + data.TAJ + "&duplicate=" + data.duplicate, function (value) { }).dialog('open');

                            }



                        }

                        else {
                            //chassis FREE
                            if (test.trim().length != 0 && !data.TAJstatus) {
                                chassisSet = true;
                                $("#make,#vehiclemodel,#modeltype,#bodytype,#extension,#vehicleRegNum,#colour,#cylinders,#engineNo,#CengineModified,#engineType,#mileage,#fitnessExpiry,#hpccUnit,#refno,#regLocation,#roofType,#seating,#seatingCert,#tonnage,#vehicleYear,#handDriveType,#tranType,#username,#fname,#mname,#lname,#RoadNo,#RoadName,#RoadType,#ZipCode,#CityName,#CountryName,#trn").prop('disabled', false);
                                //$("#vin").focus();
                            }
                            if (data.TAJstatus) {
                                $("<div>TAJ service is down</div>").dialog({
                                    modal: true,
                                    title: 'Notice',
                                    buttons: {
                                        Ok: function () {
                                            $(this).dialog("close"); chassisSet = true;
                                            $("#make,#vehiclemodel,#modeltype,#bodytype,#extension,#vehicleRegNum,#colour,#cylinders,#engineNo,#CengineModified,#engineType,#mileage,#fitnessExpiry,#hpccUnit,#refno,#regLocation,#roofType,#seating,#seatingCert,#tonnage,#vehicleYear,#handDriveType,#tranType,#username,#fname,#mname,#lname,#RoadNo,#RoadName,#RoadType,#ZipCode,#CityName,#CountryName,#trn").prop('disabled', false);

                                        }
                                    }
                                });
                            }
                        }
                    }
                    else {
                        //ERROR
                        $("<div>" + data.message + "</div>").dialog({
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $("#chassis").val("");
                                    $(this).dialog("close");
                                }
                            }
                        });

                    }
                });
                chassisFocus = false; //reset variable so that things dont go wonky
            }
            else {
                if (chassisSet) {
                    chassisSet = false;
                }
            }
        }
    })

    //$("#make,#vehiclemodel,#modeltype,#bodytype,#extension,#vehicleRegNum,#colour,#cylinders,#engineNo,#CengineModified,#engineType,#mileage,#fitnessExpiry,#hpccUnit,#refno,#regLocation,#roofType,#seating,#seatingCert,#tonnage,#vehicleYear,#handDriveType,#tranType,#username,#fname,#mname,#lname,#RoadNo,#RoadName,#RoadType,#ZipCode,#CityName,#CountryName,#trn,#roadno,#roadname,#city,#roadtype,#parishes,#country").prop('disabled', true);
});
function selectDuplicateVehicle(e) {

    selectedId = $(e).prop('id');
    $('.dupChassischeck:not(#' + selectedId + ')').prop('checked', false);
}