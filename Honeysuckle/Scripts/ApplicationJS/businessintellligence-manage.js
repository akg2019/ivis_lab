﻿$(function () {
    var current_data = 0;
    var copy = true;
    $(".kpi-count .count").on('focus', function () {
        if (copy) current_data = parseInt($(this).val());
    });

    $(".kpi-count .count").on('blur', function () {
        var intRegex = /^\d+$/;
        if (!intRegex.test($(this).val())) {
            if ($(this).val() == "") {
                $(this).val("0")
                update_kpi_data($(this));
            }
            else {
                var kpi = $(this);
                $("<div></div>").text('You must enter a positive integer').dialog({
                    title: 'KPI Error',
                    modal: true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog('close');
                            $(this).dialog('destroy');
                            copy = false;
                            $(kpi).focus();
                        },
                        Cancel: function () {
                            $(kpi).val(current_data);
                            update_kpi_data($(kpi));
                            copy = true;
                            $(this).dialog('close');
                            $(this).dialog('destroy');
                        }

                    }
                });
            }
        }
        else {
            update_kpi_data($(this));
        }
    });

    $(".kpi-count .count").on('keydown', function (e) {
        if (e.keyCode == '9' || e.which == '9') {
            if ($(this).hasClass('green')) $(this).parents('.kpi').find('.count.yellow').focus();
            if ($(this).hasClass('yellow')) $(this).parents('.kpi').find('.count.red').focus();
            return false;
        }
    });
});

function update_kpi_data(e) {
    if ($(e).hasClass('green')) {
        if (parseInt($(e).parents('.kpi').find('.yellow').val()) < parseInt($(e).val())) {
            $(e).parents('.kpi').find('.yellow').val($(e).val());
            $(e).parents('.kpi').find('.red').parents('.kpi-count').find('.count-prev').val($(e).val())
        }
        if (parseInt($(e).parents('.kpi').find('.red').val()) < parseInt($(e).parents('.kpi').find('.yellow').val())) {
            $(e).parents('.kpi').find('.red').val($(e).parents('.kpi').find('.yellow').val());
        }
        $(e).parents('.kpi').find('.yellow').parents('.kpi-count').find('.count-prev').val($(e).val());
    }
    if ($(e).hasClass('yellow')) {
        if (parseInt($(e).parents('.kpi').find('.red').val()) < parseInt($(e).val())) {
            $(e).parents('.kpi').find('.red').val($(e).val());
        }
        $(e).parents('.kpi').find('.red').parents('.kpi-count').find('.count-prev').val($(e).val());
    }
}