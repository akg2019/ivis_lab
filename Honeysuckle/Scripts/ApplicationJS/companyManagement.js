$(function () {
    $("#CompanyType").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Company/GetCompanyTypes?type=' + req.term, function (data) {
                var values = []
                $.each(data, function (i, v) {
                    values.push(v.value);
                })
                resp(values);
            });
        }
    })

    $("#CompanyName").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Company/GetCompany?type=' + req.term, function (data) {
                var values = []
                $.each(data, function (i, v) {
                    values.push(v.value);
                })
                resp(values);
            });
        }
    })

    $("#enable").children("option").each(function () {
        var status = true;
        if ($("#hideenable").val() == "False")
            status = false;

        if(!(status) && $(this).val() == 'disabled') {
            $(this).prop('selected', true);
            return;
        }
    });
});


function UserListDisplay() {

        $("#user-modal").dialog({
            modal: true,
            autoOpen: false,
            width: 700,
            height: 600,
            title: "User List",
            buttons:
             {
               Ok:
              function () {

                  $('#user-modal').dialog('close');
                  $('#user-modal').html("");
                 }
              } //END OF BUTTONS
          });

        $("#user-modal").load('/User/UserList?Id=' + $("#compID").val()).dialog('open');
    }


    $(function () {
        if ($('#Alert').text().trim().length != 0) {

            $("#Alert").dialog({
                modal: true,
                autoOpen: false,
                title: "Alert!",
                buttons:
                     {
                         OK:
                      function () {

                          $('#Alert').dialog('close');
                          $('#Alert').html("");

                      }
                     }
            });

            $('#Alert').dialog('open');
        }
    });