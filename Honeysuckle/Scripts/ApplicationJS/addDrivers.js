﻿var policyid = 0;

$(function () {
    $("#Done").click(function () {

        var VehVin = $("#vin").val();

        $(".insured-auth").each(function () {

            var PerId = $(this).children("#per").val();
            var Relation = $(this).children("#rel").val();

            var values = { vin: VehVin, per: PerId, rel: Relation }

            $.ajax({
                type: "POST",
                traditional: true,
                url: "/Vehicle/AddRelation/",
                async: false,
                data: values,
                dataType: "json",
                success: function (data) {

                }
            });
        });


        $(".insured-excep").each(function () {

            var Per = $(this).children("#person").val();
            var Rel = $(this).children("#relation").val();

            var values1 = { vin: VehVin, per: Per, rel: Rel }

            $.ajax({
                type: "POST",
                traditional: true,
                url: "/Vehicle/AddRelation/",
                async: false,
                data: values1,
                dataType: "json",
                success: function (data) {

                }
            });
        });


        $(".insured-exclu").each(function () {

            var Pers = $(this).children("#pers").val();
            var Rela = $(this).children("#relat").val();

            var values2 = { vin: VehVin, per: Pers, rel: Rela }

            $.ajax({
                type: "POST",
                traditional: true,
                url: "/Vehicle/AddRelation/",
                async: false,
                data: values2,
                dataType: "json",
                success: function (data) {

                }
            });
        });

        window.location.replace("/Home/Index/");


    }); //// END OF ON-CLICK
});   //// END OF MAIN FUNCTION


$(function () {

    $("#authorized").click(function () {

        var vin = $("#vin").val();
        policyid = $("#policyid").val();

        $("#auth").dialog({
            modal: true,
            autoOpen: false,
            width: 800,
            height: 500,

            buttons:
        {
            Add: function () {
               var vin2= $(this).data('vin');

                $(".driverCheck:checked").each(function () {


                    //adding the authorized driver to the vehicle

                    var vals = { vin: vin2, PersonId: $(this).prop('id'), type: 1, pol: policyid };

                    $.ajax({
                        type: "POST",
                        traditional: true,
                        url: "/Vehicle/addDrivers/",
                        async: false,
                        data: vals,
                        dataType: "json",
                        success: function (data) {

                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: '/Driver/Drivers/' + $(this).prop('id'),
                        cache: false,
                        success: function (html) { $("#authorizedDriver").append(html); } //insured
                    });

                    $("#auth").dialog('close');
                    $("#auth").html("");

                });   //end function

             }

          } //end buttons
        }); //end dialog

        // $(".ui-dialog-content").dialog("close");
        $("#auth").data('vin', vin).load('/People/driverList/' + vin).dialog('open');

    });
});  //end of MAIN Function


$(function () {
    $("#excepted").click(function () {

        var vin = $("#vin").val();
        policyid = $("#policyid").val();

        $("#except").dialog({
            modal: true,
            autoOpen: false,
            width: 800,
            height: 500,

            buttons:
        {
            Add: function () {
                var vin2 = $(this).data('vin');

                $(".driverCheck:checked").each(function () {


                    //adding the authorized driver to the vehicle

                    var vals = { vin: vin2, PersonId: $(this).prop('id'), type: 2 };

                    $.ajax({
                        type: "POST",
                        traditional: true,
                        url: "/Vehicle/addDrivers/",
                        async: false,
                        data: vals,
                        dataType: "json",
                        success: function (data) {

                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: '/Driver/Excepted/' + $(this).prop('id'),
                        cache: false,
                        success: function (html) { $("#exceptedDriver").append(html); } //insured
                    });

                    $("#except").dialog('close');
                    $("#except").html("");

                });   //end function

            }

        } //end buttons
        }); //end dialog

        // $(".ui-dialog-content").dialog("close");
      $("#except").data('vin', vin).load('/People/driverList/' + vin).dialog('open');

    });
});  //end of MAIN Function


$(function () {
    $("#excluded").click(function () {

        var vin = $("#vin").val();
        policyid = $("#policyid").val();

        $("#excl").dialog({
            modal: true,
            autoOpen: false,
            width: 800,
            height: 500,

            buttons:
        {
            Add: function () {
                var vin2 = $(this).data('vin');

                $(".driverCheck:checked").each(function () {


                    //adding the authorized driver to the vehicle

                    var vals = { vin: vin2, PersonId: $(this).prop('id'), type: 3 };

                    $.ajax({
                        type: "POST",
                        traditional: true,
                        url: "/Vehicle/addDrivers/",
                        async: false,
                        data: vals,
                        dataType: "json",
                        success: function (data) {

                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: '/Driver/Excluded/' + $(this).prop('id'),
                        cache: false,
                        success: function (html) { $("#excludedDriver").append(html); } //insured
                    });

                    $("#excl").dialog('close');
                    $("#excl").html("");

                });   //end function

            }

        } //end buttons
        }); //end dialog

        // $(".ui-dialog-content").dialog("close");
    $("#excl").data('vin', vin).load('/People/driverList/' + vin).dialog('open');

    });
});  //end of MAIN Function



//deleting an authorized driver
$(function () {
    $(document).on("click", "a.remDriver", function () {

        var id1 = $(this).parents("div.newdriver:first").find(".authorizedId").val();

        var parameters = { vin: vin.value, type: 1, perId1: id1 };
        console.log(parameters);

        $.ajax({
            type: "POST",
            traditional: true,
            url: "/Vehicle/deleteDrivers/",
            async: false,
            data: parameters,
            dataType: "json",
            success: function (data) {

            }
        });

        $(this).parents("div.newdriver:first").remove();
        return false;
    });
});

//deleting an excepted driver
$(function () {
    $(document).on("click", "a.remDriverExcep", function () {

        var id1 = $(this).parents("div.newdriverExcep:first").find(".exceptedId").val();

        var parameters = { vin: vin.value, type: 2, perId1: id1 };
        console.log(parameters);

        $.ajax({
            type: "POST",
            traditional: true,
            url: "/Vehicle/deleteDrivers/",
            async: false,
            data: parameters,
            dataType: "json",
            success: function (data) {

            }
        });

        $(this).parents("div.newdriverExcep:first").remove();
        return false;
    });
});

//deleting an excluded driver
$(function () {
    $(document).on("click", "a.remDriverExc", function () {

        var id1 = $(this).parents("div.newdriverExc:first").find(".excludedId").val();

        var parameters = { vin: vin.value, type: 3, perId1: id1 };
        console.log(parameters);

        $.ajax({
            type: "POST",
            traditional: true,
            url: "/Vehicle/deleteDrivers/",
            async: false,
            data: parameters,
            dataType: "json",
            success: function (data) {

            }
        });

        $(this).parents("div.newdriverExc:first").remove();
        return false;
    });
});

$(document).ready(function () {
    $("#createDriv").click(function () {
        drivCreate()

    });
});

function drivCreate() {

    var regexTRN = /^[01][0-9]{8}$/;
    var regex = /^[a-zA-Z][A-Za-z-\']*[a-zA-Z]$/;
    var regex1 = /^[a-zA-Z][A-Za-z-\']*[a-zA-Z]*$/;
    var regex2 = /^[a-zA-Z0-9][a-zA-Z0-9\- ]*$/;
    var regex3 = /^[a-zA-Z][a-zA-Z0-9.&,#\-\' ]*[a-zA-Z]$/;
    var regex4 = /^[a-zA-Z][a-zA-Z.&,#\-\' ]*[a-zA-Z]$/;
    var regexEmail = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

    $("#createDriver").dialog({
        modal: true,
        autoOpen: false,
        width: 700,
        height: 700,

        buttons:
                {
                    Create:
              function () {

                  var trn = $("#trn").val();
                  var fname = $("#fname").val();
                  var mname = $("#mname").val();
                  var lname = $("#lname").val();
                  var roadno = $("#roadno").val();
                  var road = $("#roadname").val();
                  var roadType = $("#roadtype").val();
                  var zip = $("#zipcode").val();
                  var city = $("#city").val();
                  var country = $("#country").val();
                  var parish = $(".parishes").val();
                  var ret = 0;
                  var count = 0;
                  var perId = 0;

                  var emails = [];
                  var primary = [];
                  var email_objects = [];
                  var valid = true;

                  $(".email").each(function () {
                      emails[emails.length] = $(this).val();

                      if (!regexEmail.test(emails[count])) {

                          alert('An email address entered was invalid.');
                          valid = false;
                          return false;
                      }
                      count++;

                  });


                  if (valid)
                      $(".emailprimary").each(function () {
                          primary[primary.length] = $(this).prop("checked");
                      });


                  if (valid)
                      for (var i = 0; i < emails.length; i++) {
                          email_objects.push({ "email": emails[i], "primary": primary[i] });
                      }


                  // VALIDATION CHECKS
                  if (!valid) { return false; }
                  if (trn == '' || !regexTRN.test(trn)) {
                      alert('Please enter a valid TRN. TRN Must be a nine digit number, beginning with a 0 or 1');
                      return false;
                  }

                  if (fname == '' || !regex.test(fname)) {
                      alert('Please enter a valid first name.');
                      return false;
                  }

                  if (mname == '') { }
                  else {
                      if (!regex1.test(mname)) {
                          alert('Please enter a valid middle name.');
                          return false;
                      }
                  }

                  if (lname == '' || !regex.test(lname)) {
                      alert('Please enter a valid last name.');
                      return false;
                  }

                  if (roadno == '') { }
                  else {
                      if (!regex2.test(roadno)) {
                          alert('Please enter a valid road number.');
                          return false;
                      }
                  }

                  if (road == '' || !regex3.test(road)) {
                      alert('Please enter a valid road name.');
                      return false;
                  }

                  if (roadType == '') { }
                  else {
                      if (!regex.test(roadType)) {
                          alert('Please enter a valid road type.');
                          return false;
                      }
                  }

                  if (zip == '') { }
                  else {
                      if (!regex2.test(zip)) {
                          alert('Please enter a valid zip.');
                          return false;
                      }
                  }

                  if (city == '') { }
                  else {
                      if (!regex3.test(city)) {
                          alert('Please enter a valid city.');
                          return false;
                      }
                  }

                  if (country == '' || !regex4.test(country)) {
                      alert('Please enter a valid country name.');
                      return false;
                  }

                  var parms =
                  { trn: trn, fname: fname, mname: mname, lname: lname, roadno: roadno,
                      road: road, roadType: roadType, city: city, country: country, email: emails, primary: primary, parish: parish
                  };


                  $.ajax({
                      type: "POST",
                      traditional: true,
                      url: "/Vehicle/AddPersonDriver/",
                      // url: "/People/AddPerson/",
                      async: false,
                      data: parms,
                      dataType: "json",
                      success: function (data) {

                          if (data.result)
                              perId = data.perId;
                          // success
                      }
                  });

                  $("#createDriver").html("");
                  $("#createDriver").dialog("close");
                  // $(".ui-dialog-content").dialog("close");


                  /////////////////////////////////////////////////////////////////////////////
                  // Adding the newly created driver to the page- under the appropriate driver-type

                  if ($("#auth").html() != "") {

                      var vin1 = $("#vin").val();
                      var vals = { vin: vin1, PersonId: perId, type: 1 };

                      $.ajax({
                          type: "POST",
                          traditional: true,
                          url: "/Vehicle/addDrivers/",
                          async: false,
                          data: vals,
                          dataType: "json",
                          success: function (data) {

                          }
                      });


                      $.ajax({
                          type: "POST",
                          url: '/Driver/Drivers/' + perId,
                          cache: false,
                          success: function (html) { $("#authorizedDriver").append(html); } //insured

                      });

                      $("#auth").html("");

                      $("#auth").dialog("close");

                  } //end of authorized driver addition to page


                  if ($("#except").html() != "") {

                      var vin1 = $("#vin").val();
                      var vals = { vin: vin1, PersonId: perId, type: 2 };

                      $.ajax({
                          type: "POST",
                          traditional: true,
                          url: "/Vehicle/addDrivers/",
                          async: false,
                          data: vals,
                          dataType: "json",
                          success: function (data) {

                          }
                      });


                      $.ajax({
                          type: "POST",
                          url: '/Driver/Excepted/' + perId,
                          cache: false,
                          success: function (html) { $("#exceptedDriver").append(html); } //insured

                      });

                      $("#except").html("");

                      $("#except").dialog("close");

                  } //end of excepted driver addition to page


                  if ($("#excl").html() != "") {

                      var vin1 = $("#vin").val();
                      var vals = { vin: vin1, PersonId: perId, type: 3 };

                      $.ajax({
                          type: "POST",
                          traditional: true,
                          url: "/Vehicle/addDrivers/",
                          async: false,
                          data: vals,
                          dataType: "json",
                          success: function (data) {

                          }
                      });


                      $.ajax({
                          type: "POST",
                          url: '/Driver/Excluded/' + perId,
                          cache: false,
                          success: function (html) { $("#excludedDriver").append(html); } //insured

                      });

                      $("#excl").html("");

                      $("#excl").dialog("close");

                  } // END of excluded driver addition to page

                  ///////////////////////////////////////////////////////////////////////////////////////////////


              }
                }
    });

      $("#createDriver").load('/Driver/Driver').dialog('open');

} //end of main function

//search

function searchOptionsDrivers() {
    var name = $(event.target).parent().children('#search').val();
    $(event.target).parent().parent().children('.insured-row').filter(function (div) {
        for (var i = 0; i < $(this).children().length; i++) {
            if ($($(this).children()[i]).text().trim().toLowerCase().indexOf(name.toLowerCase()) > -1) {
                //show
                $(this).show();
                break;
            }
            else {
                //uncheck and hide
                if ($(this).children().length - 1 == i) {
                    $(this).children('.insured-check').children('input.driverCheck').prop('checked', false);
                    $(this).hide();
                }
            }
        }
    });
};