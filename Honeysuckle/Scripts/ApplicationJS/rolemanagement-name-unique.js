﻿$(function () {
    $("form").submit(function () {
        var result = false;
        var name = $(".user-group-name").val().trim();
        var id = 0;
        if ($(".hidid").val()) id = parseInt((".hidid").val().trim());

        $.ajax({
            url: '/RoleManagement/TestRoleName/' + id + '?name=' + name,
            async: false,
            dataType: 'json',
            success: function (json) {
                result = json.result;
                return false;
            },
            error: function () {
                return false;
            }
        });
        if (!result) alert("The name of the user group you have chosen is already in use. Please use a different name as user group names must be unique.");
        return result;
    });
});