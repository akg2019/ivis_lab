﻿
var IAJIDFocus = false;
var IAJIDSet = false;
var IAJIDisMessage = "<div>The IAJID you have entered already exists in the system.</div>";
var IAJIDmessage = "<div>You are about to edit the IAJID of this company. Are you certain you want to do this?</div>";
var IAJIDbadmessage = "<div>The IAJID you have selected is already taken (Please enter a valid IAJID).</div>";
var IAJIDEdit = false;
var origIAJID = "";


$(function () {
    $("#IAJID").prop('readonly', true);
    $("#edit-iajid").on('click', function () {
        $(IAJIDmessage).dialog({
            title: 'Edit IAJID Dialog',
            resizable: false,
            modal: true,
            buttons: {
                Ok: function () {
                    $("#IAJID").prop('readonly', false);
                    $("#IAJID").toggleClass('read-only');
                    $("#IAJID").focus();
                    origIAJID = $("#IAJID").val();
                    IAJIDEdit = true;
                    $(this).dialog("close");
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    });
    $("#IAJID").blur(function () {
        if (IAJIDEdit) {
            //test trn
            IAJIDEdit = false;
            var id = $("#compID").val();
            $.ajax({
                url: '/Company/TestIAJID/' + id + '?iajid=' + $("#IAJID").val(),
                cache: false,
                success: function (data) {
                    if (!data.result) {
                        $("#IAJID").toggleClass('read-only');
                        $("#IAJID").prop('readonly', true);
                    }
                    else {
                        $(IAJIDbadmessage).dialog({
                            title: 'Edit IAJID Dialog',
                            resizable: false,
                            modal: true,
                            buttons: {
                                'Try Again': function () {
                                    $(this).dialog("close");
                                    $("#IAJID").focus();
                                    IAJIDEdit = true;

                                },
                                Cancel: function () {
                                    $("#IAJID").toggleClass('read-only');
                                    $("#IAJID").val(origIAJID);
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }
                }
            });
        }

    });
});