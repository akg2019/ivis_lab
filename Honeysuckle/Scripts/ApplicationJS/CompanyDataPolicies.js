﻿



var app = angular.module("CompanyDataPolicies", []);

app.controller("CompanyDataController", function ($scope, $http) {
    $scope.FilterOption = "Policy";
    $("#LoadingMessage").hide();
    $("#LoadingMessage").hide();
    $("#P2").hide();
    $("#P1").hide();

    //Gets company data/Policy/Certificates/Cover Notes
    $scope.GetPolicies = function () {

        //Initiate animation
        switch ($scope.FilterOption) {
            case "Policy":
                $("#LoadingMessage").show();
                break;
            case "Certificate":
                $("#P1").show();
                break;
            case "Cover Note":
                $("#P2").show();
                break;
            default:

        }

        $scope.Policies = [];
        $scope.CertificateData = [];
        $scope.CoverNoteData = [];

        $http.get("/Company/GetPoliciesByRange?start=" + $("#start").val() + "&end=" + $("#end").val() + "&SearchObject=" + $scope.FilterOption + "&Searchvalue=" + $scope.FilteredText).then(function (d) {

            switch ($scope.FilterOption) {
                case "Policy":
                    $("#LoadingMessage").hide();
                    $scope.Policies = d.data;
                    break;
                case "Certificate":
                    $scope.CertificateData = d.data;
                    $("#P1").hide();
                    break;
                case "Cover Note":
                    $scope.CoverNoteData = d.data;
                    $("#P2").hide();
                    break;
                default:
            }

        });

    }

    


    //Initialzes getting policy data for the whole month upon page load
    $scope.GetPoliciesInit = function () {
        $("#LoadingMessage").show();
        $scope.Policies = [];
        $http.get("/Company/GetPoliciesByRange?start=null&end=null").then(function (d) {
            $scope.Policies = d.data;
            $("#LoadingMessage").hide();
        });
    }


 //Initialzes getting policy data for the whole month upon page load
    $scope.GetDataImportsByRangeInit = function () {
        $("#LoadingMessage").show();
        $scope.Policies = [];
        $http.get("/Company/GetDataImportsByRange?start=null&end=null").then(function (d) {

        if(d.data.success == true){
             $("#SesionEnd").hide(); 
             $scope.CertificateData = d.data.Imports;
        }
        else
        {
            //$("#SesionEnd").show();
        }
           
            $("#LoadingMessage").hide();
        });

        var isCheck = false;
        if (!$('#autorefresh').is(":checked"))
        {
           setTimeout(function(){ $scope.GetDataImportsByRangeInit();  }, 5000);
        }

        console.log();
    }

    $('#autorefresh').on('change', function() { setTimeout(function(){ $scope.GetDataImportsByRangeInit();  }, 5000);});

     $scope.GetDataImportsByRange = function () {
        $('#autorefresh').prop( "checked", true );
        $("#LoadingMessage").show();
        $scope.Policies = [];
        $http.get("/Company/GetDataImportsByRange?start=" + $("#start").val() + "&end=" + $("#end").val()).then(function (d) {

       


        if(d.data.success == true){
             $("#SesionEnd").hide(); 
             $scope.CertificateData = d.data.Imports;
        }
        else
        {
            //$("#SesionEnd").show();
        }
           
            $("#LoadingMessage").hide();
        });

    }

    //Download failure data import for an insurance company for a given period
    $scope.ExportFailedReport = function (ImportID = 0) {
           
            $http.get("/Company/ExportFailedReport?FromDate=" + $("#start").val() + "&EndDate=" + $("#end").val() + "&ImportID=" + ImportID).then(function (d) {

              if(d.data.success == false) alert(d.data.message);

            if(d.data.success == true){
                
                try{
                    var Fi = d.data.FileFailedCsv;
                    var link = document.createElement('a');

                    //Downloads file
                    if (link.download !== undefined) {
                        //Set HTML5 download attribute. This will prevent file from opening if supported.
                        var fileName = Fi.substring(Fi.lastIndexOf('\\') + 1, Fi.length);
                        link.href = "/FailedImportFiles/" + fileName ;
                        link.setAttribute('target', '_blank');
                        link.download = fileName;

                        //location.href = link.href;
                        window.open(link.href, "_blank");
                    }
                
                }catch(e){}

                try{
                    var Fii = d.data.FileFailedJson;
                    var linkk = document.createElement('a');
                
                    //Downloads file
                    if (linkk.download !== undefined) {
                        //Set HTML5 download attribute. This will prevent file from opening if supported.
                        var fileName = Fii.substring(Fii.lastIndexOf('\\') + 1, Fii.length);
                        linkk.href = "/FailedImportFiles/" + fileName ;
                        linkk.setAttribute('target', '_blank');
                        linkk.download = fileName;

                        window.open(linkk.href, "_blank");

                        //location.href = linkk.href;

                    }
                
                }catch(e){}

            }
            else
            {
                //$("#SesionEnd").show();
            }
           

            });

        }

   
    //Navigates to Policy Details
    $scope.GetPolicyView = function (policyID) {
        window.open("/Policy/Details?id=" + policyID);
    }

    //Prints a certificate
    $scope.PrintCert = function (CertID) {
        window.open("/VehicleCertificate/Details/" + CertID);
    }

     //Prints a Cover Note
    $scope.PrintCoverNote = function (CoverID) {
        window.open("/VehicleCoverNote/Details/" + CoverID);
    }

    //Edits a policy
    $scope.EditPolicy = function (policyID) {
        window.open("/Policy/Edit?id=" + policyID);
    }




});