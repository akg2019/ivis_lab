﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.User>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	UserList
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%
        bool reset_login_timer = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "Reset Login Timer"));
        bool reset_password = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "ResetPassword"));
        bool users_active = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "Activate"));
        bool users_delete = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "Delete"));
        bool users_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "Read"));
        bool users_update = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "Update"));
    %>    

    <div id= "form-header">
        <img src="../../Content/more_images/User.png" alt=""/>
        <div class="form-text-links">
            <span class="form-header-text">
                User List
            </span>
            <div class="user-search index-search search-box">
                <input type="text" placeholder="Search [user data]" id="quick-user-search" class="quick-search userlist" />
                <button type="button" class="quick-search-btn user-search-btn" onclick="clickSearchBtn()" ><img src="../../Content/more_images/Search_Icon.png" /></button>
                </div>
            </div>
        </div>
    <div class="field-data">        
        <div class="cname">
            <div class="editor-label">
                Company Name
            </div>
            <div class="editor-field">
                <%: Html.Hidden("cid", ViewData["cid"], new { @class = "cid dont-process" })%>
                <%: Html.TextBox("cname", ViewData["cname"], new { @class = "read-only", @readonly = "readonly" })%>
            </div>
            <div style="padding-bottom: 10px; padding-left: 8px;"><label class = "btn btn-primary Sha" id="ShowAll">Show All Users</label></div>
        </div>

        <div class="ivis-table userclist">
            <div class="menu">
                <div class="item username">
                    Username
                </div>
                <div class="item name">
                    Name
                </div>
                <div class="item address" hidden - "hidden">
                    Address
                </div>
                <div class="item functions"></div>
            </div>

            <div id="CompanyUsers" class="table-data">

                <% int i = 1; %>
                <% foreach (var item in Model) { %>
        

                    <% if (i % 2 == 0) { %> 
                        <% if (item.active) { %>
                            <div class="row data even user"> 
                        <% } %>
                        <% else { %>
                            <div class="row data even user disabled" hidden = hidden> 
                        <% } %>
                    <% } %>
                    <% else { %>
                        <% if (item.active) { %>
                            <div class="row data odd user"> 
                        <% } %>
                        <% else { %>
                            <div class="row data odd user disabled"> 
                        <% } %>
                    <% } %>
                        <% i++; %>
                
                        <%: Html.Hidden("hiddenid", item.ID, new { @class = "userid id dont-process" } ) %>
                        <%: Html.Hidden("hiddenIAJcreated", item.IAJcreated, new { @class = "isIAJcreated dont-process" })%>
                
                        <div class="item username">
                            <%: item.username %>
                        </div>
                        <div class="item name">
                            <%: item.employee.person.lname %>, <%: item.employee.person.fname %>
                        </div>
                        <div class="item functions">
                            <% if (users_update) { %> 
                                <div id="editImage" class="icon edit" title="Edit User"> 
                                    <img src="../../Content/more_images/edit_icon.png" class="sprite" />
                                </div>
                            <% } %>
                            <% if (users_read) { %> 
                                <div class="icon view user" title="View User Details"> 
                                    <img src="../../Content/more_images/view_icon.png" class="sprite" />
                                </div>
                            <% } %>
                            <% if (users_delete) { %> 
                                <div id="deleteImage" class="icon delete" title="Delete User"> 
                                    
                                </div>  
                            <% } %>
                            <% if (users_active) { %> 
                                <% if (item.active) { %>
                                    <div class="icon disable" title="Deactivate User"> 
                                        <img src="../../Content/more_images/disable_icon.png" class="sprite" />
                                    </div> 
                                <% } %>
                                <% else { %>
                                    <div class="icon enable" title="Activate User"> 
                                        <img src="../../Content/more_images/enable_icon.png" class="sprite" />
                                    </div> 
                                <% } %>
                            <% } %>
                            <% if (reset_password && item.active) { %> 
                                <div id="resetPassImage" class="icon resetpass" title="Reset Password">
                                    <img src="../../Content/more_images/reset_password_icon.png" class="sprite" />
                                </div> 
                            <% } %>
                            <% if (Honeysuckle.Models.UserModel.FirstLogin(item.username, item.ID)) { %>
                                <% if (reset_login_timer) { %> 
                                    <div id="resetLoginImage" class="icon resetquest" title="Reset First Login Timer"> 
                                        <img src="../../Content/more_images/reset_timer_icon.png" class="sprite" />
                                    </div> 
                                <% } %>
                            <% } %>
                             <div class="icon resetaccess" title="Reset Access"> 
                                        <img src="../../Content/more_images/reset_timer_icon.png" class="sprite" />
                                    </div> 
                        </div>
                    </div>

                <% } %>
                </div>
            </div>
        </div>
        <div class="btnlinks create1"> 
            <% Html.RenderAction("BackBtn", "Back"); %>
            <%//: Html.ActionLink("Create New", "Create") %>
        </div>
    </div>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/company-userlist.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/userlist.js" type="text/javascript"></script>
</asp:Content>
