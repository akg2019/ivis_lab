﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Company>" %>
        
<div class="displayPoldata">
    <%: Html.HiddenFor(model => model.CompanyID, new { @class = "dont-process" })%>
    <% string TRN = "";
       if (Model.trn == "1000000000000") TRN = "Not Available"; else TRN = Model.trn;
     %>
     <div class="container big nomtop">
        <div class="stripes"></div>
        <div class="data">
            <div class="header">TRN:</div>
            <div class="label trn company_trn"><%: TRN %></div>
            
         </div>
    </div>
    <%--<div class="headingDisplay"><%: ViewData ["HeadingMessage"] %> </div>--%>
    <div class="container small left">
        <div class="stripes"></div>
        <div class="data">
            <div class="header">Name:</div>
            <div class="label"><%: Model.CompanyName%></div><br /><br /><br /><br/><br/>
        </div>
   </div>
   <div class="container small">
        <div class="stripes"></div>
        <div class="data">
            <div class="header">Address:</div>
            <% if (Model.CompanyAddress.longAddressUsed)
               { %>
               <div class="label"><%: Model.CompanyAddress.longAddress%></div><br /><br /><br /><br /><br />
            <% }
               else
               {%>
            <div class="label"><%: Model.CompanyAddress.roadnumber%> <%: Model.CompanyAddress.road.RoadName%> <%: Model.CompanyAddress.roadtype.RoadTypeName%></div><br/>
             <div class="label"><%: Model.CompanyAddress.ApartmentNumber%></div><br/>
            <div class="label"><%: Model.CompanyAddress.city.CityName%></div><br/>
            <div class="label"><%: Model.CompanyAddress.parish.parish%></div><br />
            <div class="label"><%: Model.CompanyAddress.country.CountryName%></div>
            <% } %>
        </div>
   </div>
       


  