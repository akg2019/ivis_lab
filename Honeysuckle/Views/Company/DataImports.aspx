﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

   

     <div  ng-app="CompanyDataPolicies" ng-controller="CompanyDataController" ng-init="GetDataImportsByRangeInit()" >
       

       <div style="width:95%; margin:auto;">
          
        <table id="MyTable">
                   <tr>
                        <td>From Date</td>
                        <td>To Date</td>
                        <td></td>
                   </tr>
                    <tr>
                         <td>
                          <input type="date" id = "start" class="form-control TextBox" />
                        </td>
                         <td>
                           <input type="date" id = "end" class="form-control TextBox" />
                        </td>
                        <td>
                            <span ng-click="GetDataImportsByRange()" class="btn btn-sm btn-success" style="width:75%!important;">Filter</span>
                        </td>
                        <td>
                            <span ng-click="ExportFailedReport()" class="btn btn-sm btn-primary" style="width:75%!important;">Download</span>
                        </td>
                    </tr>
               </table>

               <hr />
               <span>Auto refreh off</span><input type = "checkbox"  id="autorefresh" />

            <div id="FilteredPanel">
                <div id="PolicyFIlter" ng-show="FilterOption =='Data Import'">
                    <p class="FilterTitle"><b>POLICY IMPORTS</b></p>
                    <p style="text-align:center;">Search results. Records found <b>{{Policies.length | number}}</b></p>
                    <p style="text-align:center;" id="LoadingMessage">Loading policy records please wait.....</p>
                    <input type="text" class="form-control TextBox2" placeholder="Type search here" ng-model="Pol"/>
                    
                 </div>

            </div>

             <div id="Div1">
                    <p class="FilterTitle"><b>{{FilterOption}}</b></p>
                    <p style="text-align:center;">Search results. Records found <b>{{CertificateData.length | number}}</b></p>
                    <p style="text-align:center;" id="P1">Loading Certificates records please wait.....</p>
                    <input type="text" class="form-control TextBox2" placeholder="Type search here" ng-model="CertFilter"/>
                    <table>
                        <tr class="HeaderTr">
                                <td>#</td>
                                <td>Date Created</td>
                                <td>Policies</td>
                                <td>Success</td>
                                <td>Failed</td>
                                <td>Certs</td>
                                <td>CN</td>
                                <td>Create By</td>
                                <td>Status</td>
                                <td>Imported</td>
                               <td>File</d>
                         </tr>
                        <tr ng-repeat = "c in CertificateData | filter:CertFilter" class="innerTd" ng-class-odd="'odds'">
                                <td class="Numbering">{{$index+1}}</td>
                                <td class="PolicyNumber">{{c.CreatedOn | date:'yyyy-MMM-dd hh:mm:ss a'}}</td>
                                <td class="PolicyNumber"><b>{{c.PolicyCount | number}}</b></td>
                                <td class="PolicyNumber" style = "color:green!important;font-weight:bold;">{{c.ProcessedCount | number}}</td>
                                <td class="PolicyNumber" style = "color:red!important;font-weight:bold;">{{c.FailedCount | number}}</td>
                                <td class="PolicyNumber">{{c.CertificateProcessed | number}}</td>
                                <td class="PolicyNumber">{{c.CoverNotesProcessed | number}}</td>
                                <td class="PolicyNumber">{{c.CreateByName}}</td>
                                <td class="PolicyNumber" ng-class="c.ImportStatus == 'Completed' ? 'Completed': 'Inprogress'">{{c.ImportStatus}}</td>
                                <td class="PolicyNumber" style = "color:green!important;font-weight:bold;text-align:right!important;">{{c.ImportPercentage}}</td>
                                <td><span class = "btn btn-sm btn-info" ng-click="ExportFailedReport(c.ImportID)">Download</span></td>
                         </tr>
                    </table>

                    <h4 hidden="hidden" id="SesionEnd" style = "text-align:center; color:red">Your session has ended. Please <a href="/home" class="btn btn-info" style="color:#fff;">Login<a/> to view data</h4>

                </div>





       </div>


    </div>

  <style type="text/css">
    table {border:none!important; width:100%!important; }
    td{  border:none!important;padding:0,5px,0,5px!important;}
    
    .TextBox{width:85%!important; margin-bottom:0!important;}
    
      .TextBox2{width:98%!important;}
    
    .FilterTitle
    {
        text-align:center;
        color:Orange;
    }
    
    .HeaderTr
    {
        border:solid;
        border-width:thin;
        padding:5px,1px,5px,1px;
        border-color:#e0d5d5;
        background:#bdbab7;
    }

    .Completed{color:#608e35fc; font-weight:bold;}
    .Inprogress{color:blue;font-style:Italics}
    
    .TdButtons
    {
        float:right;
        padding-right:5px;
    }
    
    
    .innerTd
    {
        border-bottom:solid;
        border-bottom-width:thin;
        border-bottom-color:#e0d5d5;
    }
    
    
    .innerTd:hover
    {
        background:lightgreen;
    }
    
    .PolicyNumber{font-size:12px;}
    
    .Numbering{color:#ea5e0b;font-weight:bold;}
    
    .odds{background:#e8e8e8;}
    
    #LoadingMessage{color:#ea5e0b;}

</style>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.js"></script>
  <script src="../../Scripts/ApplicationJS/CompanyDataPolicies.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
