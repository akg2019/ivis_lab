﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.Company>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<br /><br />
    <table>
        <tr>
            <th></th>
            <th>
                IAJID
            </th>
            <th>
                CompanyName
            </th>
            
        </tr>

    <% foreach (var item in Model) { %>
    
        <tr>
            <td>
                   <% if (Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user))
                       { %>
                            <%: Html.ActionLink("Edit", "Edit", new { id = item.CompanyID })%> |
                    <% } %>

                     <%: Html.ActionLink("Details", "Details", new { id = item.CompanyID })%> |


                <% if (Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user))
                       { %>
                            <%: Html.ActionLink("Delete", "Delete", new { id = item.CompanyID })%>
                    <% } %>

             
                <% if (Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user))
                     { %>
                        <%: Html.ActionLink("Edit Contact Info", "EditCompContactInfo", new { id = item.CompanyID })%> |
                  <% } %>

                <%: Html.ActionLink("View Contact Info", "ViewContactInfo", new { id = item.CompanyID })%> |

                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Edit")))
                   { %>
                    <% if (item.Enabled)
                       { %>
                        <%: Html.ActionLink("Disable Company", "DisableCompany", new { id = item.CompanyID })%> |
                    <% } %>
                    <% else 
                       { %>
                        <%: Html.ActionLink("Enable Company", "EnableCompany", new { id = item.CompanyID })%> |
                    <% } %>
                <% } %>

                <% if (Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user))
                  { %>
                    <% if (Honeysuckle.Models.CompanyModel.IsCompanyInsurer(item.CompanyID)) { %>
                         <%: Html.ActionLink("Edit Intermediaries", "Intermediaries", new { id = item.CompanyID })%> |
                    <% } %>
               <% } %>
            </td>
            <td>
                <%: item.IAJID %>
            </td>
            <td>
                <%: item.CompanyName %>
            </td>
        </tr>
    
    <% } %>

    </table>
    <br />
    <p>

        <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company","Create")))
           { %>
            <%: Html.ActionLink("Create New", "Create")%>
        <% } %>
    </p>

</asp:Content>


