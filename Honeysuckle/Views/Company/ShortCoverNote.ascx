﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.VehicleCoverNote>>" %>



    <%: Html.Hidden("count", Model.Count(), new { @class = "dont-process" })%>

<% 
    bool covernotes_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Read"));
    bool covernotes_update = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Update"));
    bool vehicles_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Vehicles", "Read"));
    bool policy_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Policies", "Read"));
%>
    
    <div class="ivis-table">
        <div class="shortcnote menu">
            <div class="item covernoteno">
                Note No.
            </div>
            <div class="effe item">
                Effective Date
            </div>
            <div class="item exp">
                Expiry Date
            </div>
            <div class="insurance item">
                Reg No.
            </div>
        </div>
        <div class="scroll-table">
        <% int i = 0; %>
        <% foreach (var item in Model) { %>
            <% i++; %>
            <%if(i%2 == 0){ %>
            <div class="shortcnote even data row covernote">
            <% } %>
            <% else{ %>
            <div class="shortcnote odd data row covernote">
            <% } %>
                <%: Html.Hidden("d", item.ID, new { @class = "id dont-process" }) %>
                <%:Html.Hidden("d",item.Policy != null ? item.Policy.ID : 0, new { @class = "policy_id dont-process" })%>
                <%: Html.Hidden("d", item.risk.ID, new { @class = "risk_id dont-process" })%>
                <%: Html.Hidden("cnid", item.ID, new { @class = "dont-process" })%>
                 <%: Html.Hidden("hidimport", item.imported, new { @class = "hidimport dont-process" })%>
                <div class="covernoteno item">
                    <%: item.covernoteid %>
                </div>
                <div class="effe item">
                    <%: String.Format("{0:dd MMM yyyy}", item.effectivedate) %>
                </div>
                <div class="exp item">
                    <%: String.Format("{0:dd MMM yyyy}", item.expirydate) %>
                </div>
                <div class="insurance item">
                    <%: item.risk.VehicleRegNumber %>
                </div>
                
                <div class="rowcnote functions item">
                    <% if (covernotes_update && !item.imported && Honeysuckle.Models.VehicleCoverNoteModel.IsCoverNoteActive(item.ID) && !item.cancelled && DateTime.Now < item.expirydate ) { %> 
                         <div id="editImage" class="icon edit covernote" title="Edit Cover Note">
                             <img src="../../Content/more_images/edit_icon.png" class="sprite"/>
                         </div>
                    <% } %>
                    <% if (covernotes_read) { %> 
                        <div class="icon view covernote" title="View Cover Note Details">
                            <img src="../../Content/more_images/view_icon.png" class="sprite"/>
                        </div>
                    <% } %>
                    <% if (vehicles_read) { %>
                        <div class="icon risk" title="View Related Vehicle Details">
                            <img src="../../Content/more_images/related-vehicle.png" class="sprite" />
                        </div>
                    <% } %>
                    <% if (policy_read) { %>
                        <div class="icon policy" title="View Related Policy Details">
                            <img src="../../Content/more_images/view_related_policy_icon.png" class="sprite"/>
                        </div>
                    <% } %>
                </div>
            </div>
    
        <% }%>
      </div>
    </div>
