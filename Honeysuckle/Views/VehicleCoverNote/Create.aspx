﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.VehicleCoverNote>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	IVIS | Cover Note Creation
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <div id= "form-header"><img src="../../Content/more_images/Cover_Note.png" />
    <div class="form-text-links">
        <span class="form-header-text"> Create Cover Note</span> 
        <div class="icon">
              <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
        </div>
    </div>
  </div>
       <div class="field-data">
        
        <% bool isInsurer = (Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID)); %>
        <% if (isInsurer) {  %>
            <%: Html.Hidden("isInsurer", "isInsurer", new { @id = "isInsurer", @class = "dont-process" })%> 
        <% } %>
        
        <div id= "NotePrompt"> 
            <%: ViewData["NoteExistsAlert"]%>
        </div>

       

        <% if (Model != null && Model.Policy != null) { %>

            <%: Html.Hidden("policyStartTime", Model.Policy.startDateTime.ToString("MM/dd/yyyy hh:mm:ss tt"), new { @value = Model.Policy.startDateTime.ToString("MM/dd/yyyy hh:mm:ss tt"), @id = "policyStartTime", @class = "dont-process" })%>
            <%: Html.Hidden("policyEndTime", Model.Policy.endDateTime.ToString("MM/dd/yyyy hh:mm:ss tt"), new { @value = Model.Policy.endDateTime.ToString("MM/dd/yyyy hh:mm:ss tt"), @id = "policyEndTime", @class = "dont-process" })%>

        <%} %>
        <% else { %>
            <%: Html.Hidden("policyStartTime", null, new { @id = "policyStartTime", @class = "dont-process" })%>
            <%: Html.Hidden("policyEndTime", null, new { @id = "policyEndTime", @class = "dont-process" })%>
        <% } %>

        <% if (ViewData["effsys"] == null) { %> <%: Html.Hidden("systemdate", DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt"), new { id = "sysdate", @class = "dont-process" })%> <% }  %>
        <% else { %> <%: Html.Hidden("systemdate", DateTime.Parse(ViewData["effsys"].ToString()).ToString("MM/dd/yyyy hh:mm:ss tt"), new { id = "sysdate", @class = "dont-process" })%> <% } %>
        
        <div id="vehicle-modal"></div>

        <% using (Html.BeginForm()) {%>
            <%: Html.ValidationSummary(true) %>

            <fieldset>
                 <%: Html.Hidden("proceed", "", new { id = "Continue", @class = "dont-process" })%> 
                <%: Html.HiddenFor(model => model.wording.ID, new { id = "wordingID", @class = "dont-process" })%>
                <%: Html.HiddenFor(model => Model.Policy.insuredby.CompanyID, new { id = "compId", @class = "dont-process" })%>
                    
                <% if (Honeysuckle.Models.Constants.user.newRoleMode) { %> <%: Html.Hidden("companyId", Honeysuckle.Models.Constants.user.tempUserGroup.company.CompanyID, new { @class = "dont-process" })%>  <%  } %>
                <% else { %> <%: Html.Hidden("companyId", Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID, new { @class = "dont-process" })%> <% } %>

                <div class="validate-section">
                    <div class="subheader"> 
                        <div class="arrow right down"></div>
                        Vehicle Information
                        <div class="valid"></div>
                     </div>
                    <div class="data">
                        <div id="vehicle-box">
                            <div id= "vehicles">  
                                <% if (ViewData["veherror"] != null) { %> 
                                    <%: Html.Hidden("veherror", "test", new { @class = "dont-process" })%> 
                                    <div id = "vehicle-error"> <%: ViewData["veherror"]%> </div>
                                <% } %>  

                                <% if (Model != null && Model.risk != null)
                                    {  %>
                                            <% Html.RenderAction("DisplayVehicle", "Vehicle", new { test = Model.risk.ID, type = "CertCreate", CoverID = Model.Policy.policyCover.ID, VehicleUsage = Model.risk.usage.ID, polid = Model.Policy.ID }); %>
                                <%  }  %>
                            </div>

                            <% if ((bool)ViewData["CleanCreate"]) {%>
                                <div id="addvehicle" class="add-dynamic small"><img src="../../Content/more_images/ivis_add_icon.png" /><span>Add Risk</span></div>
                            <% } %>
                        </div>
                 </div> <!-- end of data section -->
             </div> <!-- end of validate section -->       

                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right"></div>
                        Policy Information
                        <div class="valid"></div>
                   </div> 
                    <div class="data">
                        <%: Html.HiddenFor(model => model.Policy.ID, new { @class = "dont-process", id = "policyId" }) %>
                        <div class="editor-label">
                            Policy Number
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.Policy.policyNumber, new { @class = "read-only", id = "policyNo", @readonly = "readonly" })%>
                        </div>
                    </div> <!-- end of data section -->
               </div> <!-- end of validate section --> 

                <div class="validate-section">
                    <div class="subheader"> 
                        <div class="arrow right"></div>
                        Cover Note Data 
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="editor-label">
                            Cover Note Number
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.covernoteid, new { id = "covernoteid", @required = "required", @readonly = "readonly", @class = "read-only" })%>
                            <div class="nb">
                                <span>*</span>This is a temporary number. Once the record is created you will be given an official number.
                            </div>
                            <%: Html.ValidationMessageFor(model => model.covernoteid) %>
                        </div>

                        <div class="editor-label">Mortgagee:</div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.mortgagee.mortgagee, new { @readonly = "readonly", @class = "read-only" }) %>
                        </div>

                        <div class="editor-label">
                            Effective Date
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.effectivedate, new { id = "effectivedate", @required = "required", @readonly = "readonly", @class = "datetime" })%>
                            <%: Html.ValidationMessageFor(model => model.effectivedate) %>
                        </div>
                    
                            <div class="editor-label">
                                Days Effective
                            </div>
                            <div class="editor-field">
                                <%: Html.TextBoxFor(model => model.period, new { @required = "required", id = "period" }) %>
                                <button type="button" class="quickperiod submitbutton" value="7" id = "quickperiod-7" >7</button>
                                <button type="button" class="quickperiod submitbutton" value="14" id = "quickperiod-14">14</button>
                                <button type="button" class="quickperiod submitbutton" value="30" id = "quickperiod-30">30</button>
                                <%: Html.ValidationMessageFor(model => model.period) %>
                        
                        </div>

                        <div class="editor-label">
                            Expiry Date
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.expirydate, new { id = "expirydate", @required = "required", @readonly = "readonly", @class= "datetime" })%>
                            <%: Html.ValidationMessageFor(model => model.expirydate)%>
                        </div>

                        <div class="editor-label">
                            Alterations
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.alterations, new { id = "alterations"})%>
                            <%: Html.ValidationMessageFor(model => model.alterations)%>
                        </div>
            
                        <div class="editor-label">
                            Manual Cover Note No.
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.covernoteno, new { id = "covernoteno"})%>
                            <%: Html.ValidationMessageFor(model => model.covernoteno) %>
                        </div>
            
                        <div class="editor-label">
                            Cover Note Wording Template:
                        </div>
            
                        <div class="editor-field">
                            <% if (Model != null && Model.risk !=null) { %>
                                <% Html.RenderAction("TemplateDropDown", "TemplateWording", new{ id= Model.risk.ID, polId= Model.Policy.ID }); %> <!-- Filter based on certian policy items-->
                            <% } %> 
                            <% else { %>
                                <% Html.RenderAction("TemplateDropDown", "TemplateWording"); %> 
                            <% } %> 
                            <div class="view-template-wording icon" title="View Wording Details">
                                <%: Html.Hidden("wording-template-id", "0", new { id = "view-wording-temp", @class = "dont-process" })%>
                                <img hidden="hidden" src="../../Content/more_images/view_icon.png" class="sprite" /> 
                            </div>
                        </div>
                    </div> <!-- end of first data section -->
                </div> <!-- end of first validate section --> 
                <div class="btnlinks">
                    <% Html.RenderAction("BackBtn", "Back"); %>
                    <input type="button" value="Create Policy" class= "createNewPol"/>
                    <input type="submit" value="Create Cover Note" class="submitbutton" id="createCover" />
                </div>
            </fieldset>

        <% } %>
    
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript"></script>
    <link href="../../Scripts/datetimepicker-master/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
    
    <script src="../../Scripts/ApplicationJS/vehiclecovernote-create.js" type="text/javascript"></script>
    <link href="../../Content/vehiclecovernote-create.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/Policies.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
