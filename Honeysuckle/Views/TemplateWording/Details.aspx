﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.TemplateWording>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div id="wording-page">
        <div id="page-title">
            <img src="../../Content/more_images/Wording_Header.png" />
            <div class="form-text-links">
                 <span class="form-header-text">Template Wording Details</span>
                 <div class="icon">
                    <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                </div>
            </div>
        </div>
        
        <div class="field-data">
            <% using (Html.BeginForm()) {%>
                <%: Html.ValidationSummary(true) %>
        
                <fieldset>
                    <div class="validate-section">
                        <div class="subheader">
                            <div class="arrow right down"></div>
                            Main Template Wording Data
                            <div class="valid"></div>
                        </div>
                        <div class="data">
                            <%: Html.Hidden("page", "details", new { @class = "dont-process" })%>
                            <%: Html.HiddenFor(model => model.ID, new { @class = "dont-process" })%>

                            <div class="editor-label">
                                Certificate Type:
                            </div> 
                            <div class="editor-field input">
                                <%: Html.TextBoxFor(model => model.CertificateType, new { id = "certtype", @class = "read-only", @readonly = "readonly" })%>
                            </div>
                            <div class="editor-label">
                                Certificate Insurance Code:
                            </div> 
                            <div class="editor-field input">
                                <%: Html.TextBoxFor(model => model.CertificateInsuredCode, new { id = "certinscode", @class = "read-only", @readonly = "readonly" })%>
                            </div>
                            <div class="check-field">
                                <div class="editor-label">
                                    Excluded Driver:
                                </div> 
                                <div class="editor-field input">
                                    <%: Html.CheckBoxFor(model => model.ExcludedDriver, new { id = "excdri", @class = "word-check read-only", @readonly = "readonly", @disabled = "disabled" })%>
                                </div> 
                            </div>
                            <div class="check-field">
                                <div class="editor-label">
                                    Excluded Insured:
                                </div> 
                                <div class="editor-field input">
                                    <%: Html.CheckBoxFor(model => model.ExcludedInsured, new { id = "excins", @class = "word-check read-only", @readonly = "readonly", @disabled = "disabled" })%>
                                </div> 
                            </div>
                            <div class="check-field">
                                <div class="editor-label">
                                    Multiple Vehicle:
                                </div> 
                                <div class="editor-field input">
                                    <%: Html.CheckBoxFor(model => model.MultipleVehicle, new { id = "mulveh", @class = "word-check read-only", @readonly = "readonly", @disabled = "disabled" })%>
                                </div> 
                            </div>
                            <div class="check-field">
                                <div class="editor-label">
                                    Registered Owner:
                                </div> 
                                <div class="editor-field input">
                                    <%: Html.CheckBoxFor(model => model.RegisteredOwner, new { id = "regown", @class = "word-check read-only", @readonly = "readonly", @disabled = "disabled" })%>
                                </div> 
                            </div>
                            <div class="check-field">
                                <div class="editor-label">
                                    Restricted Driver:
                                </div>
                                <div class="editor-field input">
                                    <%: Html.CheckBoxFor(model => model.RestrictedDriver, new { id = "regdri", @class = "word-check read-only", @readonly = "readonly", @disabled = "disabled" })%>
                                </div> 
                            </div>
                            <div class="check-field">
                                <div class="editor-label">
                                    Scheme:
                                </div> 
                                <div class="editor-field input">
                                    <%: Html.CheckBoxFor(model => model.Scheme, new { id = "sch", @class = "word-check read-only", @readonly = "readonly", @disabled = "disabled" })%>
                                </div> 
                            </div>
                            <div class="editor-label">
                                Extension Code:
                            </div> 
                            <div class="editor-field input">
                                <%: Html.TextBoxFor(model => model.ExtensionCode, new { id = "extcode", @class = "read-only", @readonly = "readonly" })%>
                            </div>
                            <div class="editor-label">
                                Certificate ID:
                            </div> 
                            <div class="editor-field input">
                                <%: Html.TextBoxFor(model => model.CertificateID, new { id = "certid", @class = "read-only", @readonly = "readonly" })%>
                            </div>
                        </div>
                    </div>
                    <div class="validate-section">
                        <div class="subheader">
                            <div class="arrow right"></div>
                            Template
                            <div class="valid"></div>
                        </div>
                        <div class="data">
                            <div class="editor-label">
                                Vehicle Registration No:
                            </div> 
                            <div class="editor-field input">
                                <%: Html.TextAreaFor(model => model.VehRegNo, new { id = "vehregno", @class = "text-area-cleditor read-only", @readonly = "readonly" })%>
                            </div> 
                            <div class="editor-label">
                                Vehicle Description:
                            </div> 
                            <div class="editor-field input">
                                <%: Html.TextAreaFor(model => model.VehicleDesc, new { id = "vehdes", @class = "text-area-cleditor read-only", @readonly = "readonly" })%>
                            </div> 
                            <div class="editor-label">
                                Policy Holders:
                            </div> 
                            <div class="editor-field input">
                                <%: Html.TextAreaFor(model => model.PolicyHolders, new { id = "polhol", @class = "text-area-cleditor read-only", @readonly = "readonly" })%>
                            </div> 
                            <div class="editor-label">
                                Authorized Drivers:
                            </div> 
                            <div class="editor-field input">
                                <%: Html.TextAreaFor(model => model.AuthorizedDrivers, new { id = "autdri", @class = "text-area-cleditor read-only", @readonly = "readonly" })%>
                            </div> 
                            <div class="editor-label">
                                Limits of Use:
                            </div> 
                            <div class="editor-field input">
                                <%: Html.TextAreaFor(model => model.LimitsOfUse, new { id = "limuse", @class = "text-area-cleditor read-only", @readonly = "readonly" })%>
                            </div>  

                            <%: Html.HiddenFor(model => model.imported, new { @class = "import dont-process" })%>
                        </div>
                    </div>
                    <div class="btnlinks">
                        <% Html.RenderAction("BackBtn", "Back"); %>
                    </div>

                </fieldset>

            <% } %>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/wording.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/wording.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/cover-usages-update.js" type="text/javascript"></script>
</asp:Content>


