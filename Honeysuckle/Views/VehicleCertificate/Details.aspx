﻿<%@ Page Title="Certificate Details" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.VehicleCertificate>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Certificate Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<small style="text-align:center!important; padding-left:5px; color:red;float:right;padding-right:40px;">
             <% if (Model.policy.ID == 0)
                       {%>
                           <i>Imported to facilitate <b>Coverage</b> No Policy exists.</i>

                       <% } %>
        </small>
 <div id= "form-header">  <img src="../../Content/more_images/Certificate.png" alt="" />
     <div class="form-text-links" style="width:90%!important;">
        <span class="form-header-text">Vehicle Certificate Details</span> 
       
         <% if (Model.Cancelled)
               { %>
                  <div class = "cancelledRecord" title = "Cancelled Policy">Cancelled</div>
            <% } %>
        <div class="icon">
              <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
        </div>
     </div>
 </div>
    <div class="field-data">

        <div id = "SendMail"> </div>

        <fieldset>

           <% 
                bool allow = false;
                bool isExpired = (Model.ExpiryDate < DateTime.Now);
                bool CertActive = Honeysuckle.Models.VehicleCertificateModel.IsCertitificateActive(Model.VehicleCertificateID);
                bool CertCancelled = Honeysuckle.Models.VehicleCertificateModel.IsCertitificateCancelled(Model.VehicleCertificateID);
                if (Honeysuckle.Models.Constants.user.newRoleMode)  {
                    allow = (Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Honeysuckle.Models.Constants.user.tempUserGroup.company.CompanyID));
                }
                else
                {
                    allow = (
                                Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID) ||
                                (Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user))
                            );
                }

                bool canPrint = true;
                int CTID = Honeysuckle.Models.IntermediaryModel.TestIntermediaryToInsurer(Honeysuckle.Models.Constants.user.employee.company.CompanyID, Model.company.CompanyID);
                if (CTID != 0)
                {
                    canPrint = Honeysuckle.Models.IntermediaryModel.GetIntermediary(CTID).PrintCertificate;
                }
           %>
      
            <%: Html.HiddenFor(model => model.VehicleCertificateID, new { id = "certId", @class = "dont-process" })%>
            <%: Html.HiddenFor(model => model.Risk.ID, new { id = "vehId", @class = "dont-process" })%>

            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right down"></div>
                     Vehicle Information
                    <div class="valid"></div> 
                    <% if (Model.policy.ID > 0)
                       {
                           if (Model.policy.imported)
                           { %>
                            <img src="../../Content/more_images/imported.png" class ="imported" />
                    <% }
                       }
                        %>
                </div>
                <div class="data">
                    <div id = "company-display"></div>
                    <div id = "person-modal"></div>
                    <div id = "vehicle-modal"></div>

                    <div class="editor-label">
                        Chassis No
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.Risk.chassisno, new { @readonly = "readonly", id = "chassis", @class = "read-only" })%>
                    </div>
            
                    <div class="editor-label">
                        Make
                    </div>
                    <div class="editor-field">
                       <%: Html.TextBoxFor(model => model.Risk.make, new { @readonly = "readonly", @class = "read-only" })%>
                    </div>
            
                    <div class="editor-label"> 
                        Model
                    </div>
                    <div class="editor-field">
                       <%: Html.TextBoxFor(model => model.Risk.VehicleModel, new { @readonly = "readonly", @class = "read-only" })%>
                    </div>

                </div> <!-- end of data section -->
            </div> <!-- end of validate section -->   

            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right"></div>
                    Certificate Information 
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div class="editor-label"> Vehicle Certificate Number:</div> 
                    <div class="editor-field">
                       <%: Html.TextBoxFor(model => model.CertificateNo, new { @readonly = "readonly", @class = "read-only" })%>
                    </div>
            
                     <%
                        string LastPrintedByName;
                        string FirstPrintedByName;
                
                        if (Model.last_printed_by_flat == null) LastPrintedByName = "";
                        else LastPrintedByName = Model.last_printed_by_flat;

                        if (Model.first_printed_by_flat == null) FirstPrintedByName = "";
                        else FirstPrintedByName = Model.first_printed_by_flat;
                     %>
                     <div class="editor-label">Certificate Wording Template:</div> 
                     <div class="editor-field">
                       <%: Html.TextBoxFor(model => model.wording.CertificateType, new { @readonly = "readonly", @class = "read-only" })%>
                     </div>

                    <% if (Model.printcount > 0) { %>
                        <div class="editor-label">
                            First Printed By
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => Model.first_printed_by_flat, new { @readonly = "readonly", @class = "read-only", @Value = FirstPrintedByName })%>
                        </div>

                        <div class="editor-label">
                            Last Printed By 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => Model.last_printed_by_flat, new { @readonly = "readonly", @class = "read-only", @Value = LastPrintedByName })%>
                        </div>
                    <% } %>

                    <div class="editor-label">
                        Print Count
                    </div>
                    <div class="editor-field">
                        <% if (allow) { %>
                            <%: Html.TextBoxFor(model => model.printcountInsurer, new { @readonly = "readonly", @class = "read-only" })%>
                        <% } %>
                        <% else { %>
                            <%: Html.TextBoxFor(model => model.printcount, new { @readonly = "readonly", @class = "read-only" })%>
                        <% } %>
                    </div>

                    <div class="check-div">
                        <div class="editor-label">
                            Cancelled
                        </div>
                        <div class="editor-field">
                            <%: Html.CheckBox("cancelled", Model.Cancelled, new { @disabled = "disabled" })%>
                        </div>
                    </div>

                </div> <!-- end of data section -->
            </div> <!-- end of validate section --> 

            
       <% if (Model.Risk.mainDriver != null && Model.Risk.mainDriver.person.PersonID != 0)
          { %>
            <div class="validate-section">
               <div class="subheader">
                     <div class="arrow right"></div>
                     Main Driver
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div class="editor-label">
                       TRN
                    </div>
                    <div class="editor-field">
                       <%: Html.TextBoxFor(model => model.Risk.mainDriver.person.TRN, new { @readonly = "readonly", id = "maindriverTRN", @class = "read-only" })%>
                    </div>

                    <div class="editor-label">
                        First Name
                    </div>
                    <div class="editor-field">
                         <%: Html.TextBoxFor(model => model.Risk.mainDriver.person.fname, new { @readonly = "readonly", @class = "read-only" })%>
                    </div>
            
                    <div class="editor-label">
                        Last Name 
                    </div>
                    <div class="editor-field">
                       <%: Html.TextBoxFor(model => model.Risk.mainDriver.person.lname, new { @readonly = "readonly", @class = "read-only" })%>
                    </div>

                </div> <!-- end of data section -->
            </div> <!-- end of validate section -->   
            <% } %>

            <div class="validate-section">
                <div class="subheader"> 
                    <div class="arrow right"></div>
                    Insurance Company Information 
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div id="company">
                        <% Html.RenderAction("CompanyRead", "Company", new { id = Model.company.CompanyID }); %>
                    </div>
                </div> <!-- end of data section -->
            </div> <!-- end of validate section -->  

            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right"></div>
                     Policy Information 
                     <div class="valid"></div>
                </div>
                <div class="data">
                    <div id="policy">
                    <% if (Model.policy.ID > 0)
                       {%>
                            <% Html.RenderAction("ViewPolicy", "Policy", new { id = Model.policy.ID }); %>

                       <% } %>

                       
                    </div>

                    <div class="check-div">
                        <div class="editor-label"> 
                            Multiple Risk: 
                        </div>
                        <div class="editor-field">
                            <%: Html.CheckBox("MultipleRisk", (bool)ViewData["multipleRisk"], new { id = "multipleRisk", @disabled = "disabled" })%>
                        </div>
                    </div>
                </div> <!-- end of data section -->
            </div> <!-- end of validate section --> 

               <small style="text-align:center!important; padding-left:5px; color:red;float:right; padding-bottom:5px;">
             <% if (Model.policy.ID == 0)
                       {%>
                           <i>Imported to facilitate <b>Coverage</b> No Policy exists.</i>

                       <% } %>
        </small>

            <div class='btnlinks'> 

                <% Html.RenderAction("BackBtn", "Back"); %>

                <!------------------ ------------------->
                
                <% if (!isExpired)  { %>
                    <% if ((Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Approve"))))
                       { %>
                          <% if (!CertActive && !CertCancelled)
                               { %> 
                                    <input type="button" value="Approve" id="approve" onclick="ApproveCert()"/>
                          <%   }  %>  
                          <% else if(!CertCancelled)
                               { %> 
                                    <input type="button" value="Cancel Certificate" id="cancel" onclick="CancelCert()"/> 
                            <% } %> 
                    <% } %> 
                    <%  else 
                        { %>
                            <% if (!CertActive)
                               { %> 
                                    <input type="button" value="Request Approval" id = "reqApproval" onclick="RequestCertApproval();"/> 
                            <% } %>    
                    <%  } %>     
                <%  } %>     

             
                 <button type="button" class="print submitbutton" onclick="Print()">Print Certificate</button> 
                <% if ((CertActive || CertCancelled) && canPrint)
                   { %>
                      
                <%  } %>    

                <% if (!CertActive && !isExpired && !CertCancelled)
                   { %>  
                      <button class="editCertificate">Edit</button>
                <% } %>  


            </div>
   
        </fieldset>

    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/ApproveCertificate.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/personDetails.js" type="text/javascript"></script>
    <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/VehicleCertificate.js" type="text/javascript"></script>
    <link href="../../Content/Policies.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/print.js" type="text/javascript"></script>
</asp:Content>


