﻿<%@ Page Title="Create Certificate" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.VehicleCertificate>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	IVIS | Certificate Creation
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id= "form-header">
        <img src="../../Content/more_images/Certificate.png" alt="" />
        <div class="form-text-links">
            <span class="form-header-text"> 
                Create Vehicle Certificate
            </span> 
            <div class="icon">
                <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
            </div>
        </div>
    </div>

    <div class="field-data">

        <div id="vehicle-modal"></div>

        <% using (Html.BeginForm()) {%>
            <%: Html.ValidationSummary(true) %>

            <fieldset>  
               
                <%: Html.HiddenFor(model => model.Risk.ID, new { id = "vehicleId", @class = "dont-process" })%>
                <%: Html.HiddenFor(model => Model.company.CompanyID, new { id = "compId", @class = "dont-process" })%>
                <%-- <%: Html.HiddenFor(model => Model.policy.ID, new { id = "polId", @class = "dont-process" })%>--%>
                <%: Html.HiddenFor(model => Model.policy.policyCover.ID, new { id = "polCover", @class = "dont-process" })%>
                <%: Html.HiddenFor(model => model.wording.ID, new { id = "wordingID", @class = "dont-process" })%>
                
                <% if (Model.policy != null) { %>
                    <%: Html.Hidden("effective_date_policy", Model.policy.startDateTime.ToString("MM/dd/yyyy HH:mm:ss"), new { @class = "dont-process" })%>
                    <%: Html.Hidden("end_date_policy", Model.policy.endDateTime.ToString("MM/dd/yyyy HH:mm:ss"), new { @class = "dont-process" })%>
                <% } %>
                <% else { %>
                    <%: Html.Hidden("effective_date_policy", null, new { @class = "dont-process" })%>
                    <%: Html.Hidden("end_date_policy", null, new { @class = "dont-process" })%>
                <% } %>

                <%: Html.Hidden("now_date_time", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), new { @class = "dont-process" })%>
            

                <% if (Honeysuckle.Models.Constants.user.newRoleMode) { %> <%: Html.Hidden("companyId", Honeysuckle.Models.Constants.user.tempUserGroup.company.CompanyID, new { @class = "dont-process" })%>  <%  } %>
                <% else { %> <%: Html.Hidden("companyId", Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID, new { @class = "dont-process" })%> <% } %>
           
                <div id= "CertPrompt"> 
                    <%: ViewData["CertExistsAlert"]%>
                </div>

                <%: Html.Hidden("proceed", "", new { id = "Continue", @class = "dont-process" })%> 
                <div id= "Alert"> <%: ViewData["Message"]  %> <%: ViewData["Date Error"]  %> </div>
                
                <div class="validate-section">
                    <div class="subheader">
                         <div class="arrow right down"></div>
                         Vehicle Information
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div id="vehicle-box">
                            <div id="vehicles">
                                <% if (ViewData["veherror"] != null) { %>  
                                    <div id = "vehicle-error"> 
                                        <%: ViewData["veherror"]%> 
                                    </div> 
                                <% } %>
              
                                <% if (Model != null && Model.Risk !=null) { %>
                                    <% Html.RenderAction("DisplayVehicle", "Vehicle", new { test = Model.Risk.ID, type = "CertCreate", CoverID = Model.policy.policyCover.ID, VehicleUsage = Model.Risk.usage.ID }); %>
                                <% } %>
                            </div>

                            <%  if ((bool)ViewData["CleanCreate"]) { %>
                                <div id="addvehicle" class="add-dynamic small">
                                    <img src="../../Content/more_images/ivis_add_icon.png" />
                                    <span>
                                        Add Risk
                                    </span>
                                </div>
                            <% } %> 
                        </div>

                </div> <!-- end of data section -->
            </div> <!-- end of validate section -->

             <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right"></div>
                        Policy Information
                        <div class="valid"></div>
                   </div> 
                    <div class="data">
                        <%: Html.HiddenFor(model => model.policy.ID, new { @class = "dont-process", id = "policyId" }) %>
                        <div class="editor-label">
                            Policy Number
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.policy.policyNumber, new { @class = "read-only", id = "policyNo", @readonly = "readonly" })%>
                        </div>
                    </div> <!-- end of data section -->
               </div> <!-- end of validate section -->

                <div class="validate-section">
                    <div class="subheader"> 
                        <div class="arrow right"></div>
                        Certificate Information
                        <div class="valid"></div>
                     </div>
                    <div class="data">
                    <!--
                        <div class="editor-label">
                            Effective Date:
                        </div>
                        <div class="editor-field">
                           <%--  <%: Html.TextBoxFor(model => model.EffectiveDate, new { @required = "required", id = "effective", @class = "datetime" , @readonly = "readonly", @Value = Model.EffectiveDate.ToString("dddd, d MMM, yyyy, HH:mm tt") }) %> --%>
                        </div>
                        -->
                        <div class="editor-label">
                            Printed Paper Number:
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.PrintedPaperNo, new { id = "ppn"})%>
                            <%: Html.ValidationMessageFor(model => model.PrintedPaperNo) %>
                        </div>
            
                        <div class="editor-label">
                            Certificate Wording Template:
                        </div>
            
                        <div class="editor-field">
                            <% if (Model != null && Model.Risk !=null) { %>
                                <% Html.RenderAction("TemplateDropDown", "TemplateWording", new { id = Model.Risk.ID, polId = Model.policy.ID }); %> <!-- Filter based on certian policy items-->
                            <% } %> 
                            <% else { %>
                                <% Html.RenderAction("TemplateDropDown", "TemplateWording"); %> 
                            <% } %> 
                            <div class="view-template-wording icon" title="View Wording Details">
                                <%: Html.Hidden("wording-template-id", "0", new { id = "view-wording-temp", @class = "dont-process" }) %>
                                <img hidden = "hidden" src="../../Content/more_images/view_icon.png" class="sprite" /> 
                            </div>
                        </div>
                
            </div> <!-- end of first data section -->
        </div> <!-- end of first validate section -->  

                

                <div class="btnlinks">
                    <% Html.RenderAction("BackBtn", "Back"); %>
                    <% if ((bool)ViewData["CleanCreate"]) { %> 
                        <input type="button" value="Create Policy"class= "createNewPol"/> 
                    <% } %>
                    <input type="submit" value="Create Certificate" id= "Submit2"/>
                </div>

            </fieldset>

        <% } %>

    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">

    <link href="../../Scripts/datetimepicker-master/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript"></script> <!-- datepicker -->

    <script src="../../Scripts/ApplicationJS/vehicleCertificatePPN.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/VehicleCertificateCreate.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/personDetails.js" type="text/javascript"></script>
    <link href="../../Content/chosen.min.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/chosen.jquery.min.js" type="text/javascript"></script>
    <link href="../../Content/Policies.css" rel="stylesheet" type="text/css" />
     <link href="../../Content/Pages.css" rel="stylesheet" type="text/css" />
</asp:Content>


