﻿<%@ Page Title="Vehicles' Drivers" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Policy>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- MODALS -->
    <div id="vehicle-modal"></div>
    <div id="vehicle-modal1"></div>
    <div id="drivers"></div> 
    <div id="person-modal"></div>
    
    <% int insurer = Model.insuredby.CompanyID; %>
    
    <% using (Html.BeginForm()) {%>
    <%: Html.ValidationSummary(true) %>

    <fieldset>
        <div id= "form-header">
            <img src="../../Content/more_images/Policy.png" alt=""/>
            <div class="form-text-links">
                <span class="form-header-text">Add Policy's Vehicle's Drivers</span>
                <div class="icon">
                    <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
               </div>
           </div>
        </div>

        <div class="field-data">
            <%: Html.HiddenFor(model => model.ID, new { id = "policyid", @Value = Model.ID, @class = "dont-process" })%> <!-- THE ID OF THE POLICY -->
            <%: Html.HiddenFor(model => model.ID, new { id = "compid",@class="dont-process", @Value = Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID })%> 
          
            <% if ((bool)ViewData["EditPolicy"]) { %>
                <div id= "backToEditPol"> </div>
            <% } %>

            <div id="InformationDialog"></div>

            <div class="validate-section">
                <div class="subheader" > 
                    <div class="arrow right down"></div>
                    Policy Information 
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div class="editor-label">
                        Policy Number
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.policyNumber, new { @class = "read-only", @readonly = "readonly" })%>
                    </div>
                    <div class="editor-label">
                        Policy Prefix
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.policyCover.prefix, new { @class = "read-only", @readonly = "readonly" })%>
                    </div>
                </div>
            </div>
            <div class="validate-section policy-veh-drivers">
                <div class="subheader" > 
                    <div class="arrow right"></div>
                    Policy Vehicles 
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div id="vehiclesContainer">
                        <% if (Model.vehicles != null) { %>
                            <% foreach (Honeysuckle.Models.Vehicle vehicle in Model.vehicles) { %>
                                  <% Html.RenderAction("AddVehicleDrivers", "Vehicle", new { veh = vehicle, polId = Model.ID, insurer = insurer }); %>
                             <% } %>
                        <% } %>
                    </div>
                </div>
            </div>

            <!--Policy Insureds stored here for use in the modal list -->
            <% if (Model.insured != null) { %>
                <% foreach (Honeysuckle.Models.Person ins in Model.insured) { %>
                      <%: Html.Hidden("insuredPerson", ins.PersonID, new { @class = "PeopleInsuredList dont-process" })%>
                 <% } %>
            <% } %>

        </div>

        <div class="btnlinks">
            <input type="button" value="Back To Policy" id="backToPol" />
           <!-- <input type="button" value="Done" id="Done" /> -->
        </div>
    </fieldset>

<% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/Pages.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/Policies.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/addPolicyDrivers.js" type="text/javascript"></script>
</asp:Content>
