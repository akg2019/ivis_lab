﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.AddressRequired>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Index</title>
</head>
<body>
    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <legend>Fields</legend>
            
            <div class="editor-label">
                Road Number:
            </div>
            <div class="editor-field">
                <%: Html.CheckBoxFor(model => model.roadno) %>
                <%: Html.ValidationMessageFor(model => model.roadno) %>
            </div>
            
            <div class="editor-label">
                Road Name:
            </div>
            <div class="editor-field">
                <%: Html.CheckBoxFor(model => model.roadname)%>
                <%: Html.ValidationMessageFor(model => model.roadname) %>
            </div>
            
            <div class="editor-label">
                Road Type:
            </div>
            <div class="editor-field">
                <%: Html.CheckBoxFor(model => model.roadtype)%>
                <%: Html.ValidationMessageFor(model => model.roadtype) %>
            </div>
            
            <div class="editor-label">
                Zip Code:
            </div>
            <div class="editor-field">
                <%: Html.CheckBoxFor(model => model.zipcode)%>
                <%: Html.ValidationMessageFor(model => model.zipcode) %>
            </div>
            
            <div class="editor-label">
                City:
            </div>
            <div class="editor-field">
                <%: Html.CheckBoxFor(model => model.city)%>
                <%: Html.ValidationMessageFor(model => model.city) %>
            </div>
            
            <div class="editor-label">
                Country:
            </div>
            <div class="editor-field">
                <%: Html.CheckBoxFor(model => model.country)%>
                <%: Html.ValidationMessageFor(model => model.country) %>
            </div>
            
            <p>
                <input type="submit" value="Save" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Back to List", "Index") %>
    </div>

</body>
</html>

