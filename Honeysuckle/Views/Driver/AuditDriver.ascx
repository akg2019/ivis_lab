﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Driver>" %>

<div class="editor-label">
    <%: Html.LabelFor(model => model.licenseno) %>
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.licenseno) %>
</div>
            
<div class="editor-label">
    <%: Html.LabelFor(model => model.dateissued) %>
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.dateissued) %>
</div>
            
<div class="editor-label">
    <%: Html.LabelFor(model => model.expirydate) %>
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.expirydate) %>
</div>
            
<div class="editor-label">
    <%: Html.LabelFor(model => model.licenseclass) %>
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.licenseclass) %>
</div>
            
<div class="editor-label">
    <%: Html.LabelFor(model => model.relation) %>
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.relation) %>
</div>

<% if (Model.person != null) Html.RenderPartial("~/Views/People/AuditPeople.ascx", Model.person); %>
