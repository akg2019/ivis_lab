﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.ContactGroup>>" %>
<link href="../../Content/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
<link href="../../Content/chosen.min.css" rel="stylesheet" type="text/css" />
<script src="../../Scripts/jquery-1.11.1.min.js" type="text/javascript"></script>

<script src="../../Scripts/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $("#chosen-drop").chosen({
            no_results_text: "Oops, nothing found!" 
        });
    });
</script>


<select id="chosen-drop" multiple="true" style="width:200px;">
    <% for (int i = 0; i < Model.Count(); i++)
        { %>
        <option value="<%: Model.ElementAt(i).ID %>" class="group">
            <%: Model.ElementAt(i).name %>
        </option>
        <% for (int j = 0; j < Model.ElementAt(i).contacts.Count(); j++)
            { %>
            <option class="active-result group-option" value="<%: Model.ElementAt(i).contacts.ElementAt(j).ID %>">
                <%: Model.ElementAt(i).contacts.ElementAt(j).username %>
            </option>
        <% } %>
    <% }  %>
</select>