﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Notification>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	View Message
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      
    <div id= "form-header"><img src="../../Content/more_images/User.png" alt=""/>
            <div class="form-text-links">
                 <span class="form-header-text">Notification</span>
            </div>
    </div> 
          
    <div class="field-data"> 
        <fieldset>
       
            <%: Html.HiddenFor(it => Model.ID, new { @class = "note-id dont-process" })%>
            <div class="editor-label">
                Title
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.heading, new { @readonly = "readonly", @class = "read-only" }) %>
            </div>
            <div class="editor-label">
                Sent Time
            </div>
            <div class="editor-field">
                <%: Html.TextBox("time", Model.timestamp.ToString("MMMM dd, yyyy hh:mm tt"), new { @readonly = "readonly", @class = "read-only" })%>
            </div>
            <div class="editor-label">
                From
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.NoteFrom.username, new { @readonly = "readonly", @class = "read-only" })%>
            </div>
            <div class="editor-label">
                Message
            </div>
            <%: Html.TextArea("messageArea", Model.content, new { @readonly = "readonly", @class="contentView" })%>
            <!--<div class="contentView">
               <%--  <%: Model.content %>--%>
                
            </div>-->
            <div class="btnlinks">
                <button id="backToInbox">Back To Inbox</button>
            </div>
        </fieldset>
    </div>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/notification-view.css" rel="stylesheet" type="text/css" />
    <%--<script src="../../Scripts/ApplicationJS/NotificationView.js" type="text/javascript"></script> --%>
</asp:Content>

