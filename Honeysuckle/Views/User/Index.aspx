﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.User>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="credentials" style="display:none;"> 
        <%:ViewData["Sent"] %>
    </div>
    <div id="employeeactive" style="display:none;"> 
        <%:ViewData["employeeactive"]%>
    </div>

    <div class="searchbar">
        <% using(Html.BeginForm("Index","User",FormMethod.Post)) {
            %>
            <div class="search-label">SEARCH : </div>
            <div  class="search-box">
                <%= Html.TextBox("search") %>
            </div>
            <div class="search-button">
                <input value="Search" name="search" type="submit" />
            </div>
        <% }; %>

    </div>
    <br />
    <table>
        <tr>
            <th></th>
            <th>
                username
            </th>

        </tr>

    <% foreach (var item in Model) { %>
    
        <tr>
            <td>
                <% if ((Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users","Update"))) && !(Honeysuckle.Models.UserGroupModel.AmAnAdministrator(new Honeysuckle.Models.User(item.ID, item.username))))
                   { %>
                       <%: Html.ActionLink("Modify", "Modify", new { id=item.ID }) %> |
                <% } %>

                <%: Html.ActionLink("Details", "Details", new { id=item.ID })%> |

                <% if ((Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users","Delete"))) && !(Honeysuckle.Models.UserGroupModel.AmAnAdministrator(new Honeysuckle.Models.User(item.ID, item.username))))
                   { %>
                       <%: Html.ActionLink("Delete", "Delete", new { id=item.ID })%> |
                <% } %>

                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("UserGroups","Update")))
                   { %>
                        <%: Html.ActionLink("Add User Roles", "AddUserRoles", new { id=item.ID })%> |
                <% } %>

                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "Create")) && Honeysuckle.Models.UserModel.FirstLogin(Honeysuckle.Models.UserModel.GetUserName(item.ID)))
                   { %>
                       <%: Html.ActionLink("Reset First Login", "ResetFirstLoginTimer", new { id = item.ID }) %> |
                <% } %>

                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users","Update")))
                   { %>
                    <% if (item.active)
                       { %>
                           <%: Html.ActionLink("Deactivate User", "DeactivateUser", new {id = item.ID})%> 
                    <% } %>
                    <% else
                       { %>
                           <%: Html.ActionLink("Activate User", "ActivateUser", new {id = item.ID })%> 
                    <% } %>
               <% } %>

                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users","Update")))
                   { %>
                  |    <%: Html.ActionLink("Send New Password", "NewPassword", new { id = item.ID })%> 
                <% } %>
                
            </td>
            <td>
                <%: item.username %>
            </td>
           
          
        </tr>
    
    <% } %>

    </table>

    <p>
    <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users","Create")))
         { %>
             <%: Html.ActionLink("Create New", "Create") %>  
      <% } %>
    </p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
<script src="../../Scripts/ApplicationJS/EmailPopup.js" type="text/javascript"></script>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

