﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.User>" %>

<div class="user-row row selected">
    <div class="user-check select item">
        <%: Html.CheckBox("usercheck", true, new { @class = "usercheck", id = Model.ID.ToString(), @OnClick = "changeCheckBox(this)" })%>
    </div>
    <div class="username item">
        <%: Model.username %>
    </div>
    <div class="trn item">
        <%: Model.employee.person.TRN %>
    </div>
    <div class="name item">
        <%: Model.employee.person.lname %>, <%: Model.employee.person.fname %>
    </div>
    <br />  
</div>