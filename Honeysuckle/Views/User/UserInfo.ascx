﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.User>" %>

<% using (Html.BeginCollectionItem("user")) {%>
    <%: Html.ValidationSummary(true) %>
    <%: Html.HiddenFor(model => model.ID, new { @class = "userId dont-process" })%>
    <%: Html.HiddenFor(model => Model.employee.person.PersonID,new { @class = "personID dont-process" })%>
    <div class= "row-field"> 
        <span>Username:</span><%:Html.TextBoxFor(model => model.username, new { @readonly = "readonly", id = "usernameEmail", @class = "read-only" })%>  
    </div>
    <div class= "row-field">  
        <span>Name:</span><%: Html.TextBox("peoplecontact", Model.employee.person.fname + " " + Model.employee.person.lname, new { @readonly = "readonly", id = "name", @class = "read-only" })%>
    </div>

    <% foreach (var email in Model.employee.person.emails)
           {%>
            <% if (email.email != Model.username && email.primary)
               {%>
                <div class= "row-field">  
                    <span>Primary Email:</span><%: Html.TextBox("primary-email", email.email, new { @readonly = "readonly", id = "primary-email", @class = "read-only" })%>
                </div>
        <% } %>
        <%} %>
<% } %>
