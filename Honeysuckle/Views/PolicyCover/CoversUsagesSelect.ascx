﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.PolicyCover>" %>

 <div class="coverUsagePair">

    <%: Html.Hidden("cover_id",ViewData["coverID"], new { @class = "coverID dont-process" })%>

    <%: Html.Hidden("cover_id",int.Parse(ViewData["coverID"].ToString()), new { @class = "coverID dont-process" })%>

    <% if (!(bool)ViewData["new"]) { %>
        <%: Html.HiddenFor(Model => Model.usages[0].ID, new { @class = "usageID dont-process" })%>
    <% } %> 

    <% Html.RenderAction("CoversDropDown", "PolicyCover"); %>
    <% Html.RenderAction("UsageDropdown", "Usage", new { coverID =  ViewData["coverID"] }); %> 

    <% if (!(bool)ViewData["new"]) {  %>
        <input type="button" value="Delete" class="deletechanges inter-btn"/>
    <% } %>
    <% if ((bool)ViewData["new"]) { %>
        <input type="button" value="Save" class="savechanges inter-btn"/> 
    <% } %> 
</div>