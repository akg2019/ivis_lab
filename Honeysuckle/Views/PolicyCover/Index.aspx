﻿<%@ Page Title="Policy Covers" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.PolicyCover>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<%
    bool policycovers_update = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("PolicyCovers", "Update"));
    bool policycovers_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("PolicyCovers", "Read"));
    bool policycovers_delete = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("PolicyCovers", "Delete"));
    bool usages_create = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Usages", "Create"));

    
%>
    
    <div id= "form-header">
        <img src="../../Content/more_images/Policy.png" alt=""/>
        <div class="form-text-links">
            <span class="form-header-text">
                Policy Cover Management
            </span>
            <div class="search-box index-search">
                <%= Html.TextBox("covers-search", null, new { @PlaceHolder = "Search [Policy Covers]", @class ="quick-search policycover-index", @autocomplete="off" })%>
                <button type="button" class="quick-search-btn" onclick="clickSearchBtn(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
            </div>
        </div>
    </div>
      
    <div id="cover-list">
    
        <% if (ViewData["deleted"] != null) { %>
                <% if ((bool)ViewData["deleted"]){ %> <div class="deleted success"></div> <% } %>
                <% else { %><div class="deleted failed"></div> <% } %>
        <% } %>
        <% if (ViewData["create"] != null) { %> <div class="create"><%: ViewData["coverId"].ToString()%></div>  <% } %>
        
        <div id="covers" class="field-data">

            
        
            <div class="ivis-table">
                <div class="policycover menu ">
                    <div class="cover polpre item">
                        Prefix
                    </div>
                    <div class="cover polcov item">
                        Policy Cover
                    </div>
                    <div class="cover commercial item">
                        Commercial
                    </div>
                    <div class="cover commercial item">
                        Motor Trade
                    </div>
                    <div class="cover commercial item">
                        Enabled
                    </div>
                </div>
                <div id="covers-data">
                <% int i = 0; %>
                <% foreach (var item in Model) { %>
                    <% i++; %>
                    <% if (i % 2 == 0) { %>
                        <div class="policycover data row even">
                    <% } %>
                    <% else { %>
                        <div class="policycover data row odd">
                    <% } %>
                        <div class="cover polpre item">
                            <%: item.prefix %>
                        </div>
                        <div class="cover polcov item">
                            <%: item.cover %>
                        </div>
                        <%: Html.Hidden("pcid", item.ID, new { @class = "id dont-process" })%>
                        <div class="cover commercial item checkb">
                            <%: Html.CheckBox("check",item.isCommercialBusiness, new { @disabled = "disabled"}) %>
                        </div>
                         <div class="cover commercial item checkb">
                            <%: Html.CheckBox("check",item.IsMotorTrade, new { @disabled = "disabled"}) %>
                        </div>
                         <div class="cover commercial item checkb">
                            <%: Html.CheckBox("check",item.IsActive, new { @disabled = "disabled"}) %>
                        </div>

                        <div class="cover functions">
                            <% if (policycovers_update) { %> 
                                <div id="editImage" class="icon edit" title="Edit Cover"> 
                                    <img src="../../Content/more_images/edit_icon.png" class="sprite" />
                                </div> 
                            <% } %>
                            <% if (policycovers_read) { %> 
                                <div class="icon view policycover" title = "View Cover Details">
                                    <img src="../../Content/more_images/view_icon.png" class="sprite" />
                                </div>
                            <% } %>
                            <% if (policycovers_delete && item.can_be_deleted) { %> 
                                <div id="deleteImage" class="icon delete" title="Delete Cover"> 
                                    <img src="../../Content/more_images/delete_icon.png" class="sprite"/>
                                </div> 
                            <% } %>
                            <% if (usages_create) { %> 
                                <div class="icon manageusages policycover" title = "Manage Usages">
                                    <img src="../../Content/more_images/usages_icon.png" class="sprite" />
                                </div>
                            <% } %>
                        </div>
                    </div>
    
                <% } %>
                </div>
            </div>
        </div>

            <div class="btnlinks"> 
                <input type="submit" value="Create New" class="createpage" />
                <input type="text"  id ="RecordsReturned" 
                    style="border-style: none; float:left" readonly="readonly" 
                    class="RecordsFoundStyle"/></div>
            <div> </div>

        </div>
    </div>

    

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/covers.js" type="text/javascript"></script>
    <link href="../../Content/cover-index.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>



