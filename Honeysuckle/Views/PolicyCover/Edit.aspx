﻿<%@ Page Title="Edit PolicyCover" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.PolicyCover>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm()) { %>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
        
            <div id= "form-header">  <img src="../../Content/more_images/Policy.png" alt=""/>
                 <div class="form-text-links">
                        <span class="form-header-text">Policy Cover Edit</span> 
                        <div class="icon">
                            <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                        </div>
                 </div>
             </div>
            <div class="field-data">
                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right down"></div>
                        Main Cover Data
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <%: Html.HiddenFor(model => model.ID, new { @class = "dont-process" })%>
            
                        <div class="editor-label">
                            Prefix
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.prefix, new { @required = "required",@class= "prefix" })%>
                            <%: Html.ValidationMessageFor(model => model.prefix) %>
                        </div>

                        <div class="editor-label">
                            Policy Cover
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.cover, new { @required = "required",@class= "cover" })%>
                            <%: Html.ValidationMessageFor(model => model.cover) %>
                        </div>

                        <div class="check-field">
                            <div class="editor-label">
                                Is Commercial Business:
                            </div>
                            <div class="editor-field">
                                <%: Html.CheckBoxFor(model => model.isCommercialBusiness) %>
                                <%: Html.ValidationMessageFor(model => model.isCommercialBusiness) %>
                            </div>
                        </div>
                         <div class="check-field">
                            <div class="editor-label">
                                Is Motor Trade Business:
                            </div>
                            <div class="editor-field">
                                <%: Html.CheckBoxFor(model => model.IsMotorTrade) %>
                                <%: Html.ValidationMessageFor(model => model.IsMotorTrade) %>
                            </div>
                        </div>
                         <div class="check-field">
                            <div class="editor-label">
                                Is Active Cover:
                            </div>
                            <div class="editor-field">
                                <%: Html.CheckBoxFor(model => model.IsActive) %>
                                <%: Html.ValidationMessageFor(model => model.IsActive) %>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="btnlinks">
                    <%--<% Html.RenderAction("BackBtn", "Back"); %>--%>
                    <a href = "#" onclick="history.back();return false;" class="btn btn-xs btn-primary">Back</a>
                    <input type="submit" value="Save" id="Submit2" />
                </div>
            </div>

        </fieldset>

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/cover-usages.css" rel="stylesheet" type="text/css" />
</asp:Content>

