﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"  Inherits="System.Web.Mvc.ViewPage<List<Honeysuckle.Models.Search>>" %>
<asp:content id="Content1" contentplaceholderid="TitleContent" runat="server">
	Search Results
</asp:content>
<asp:content id="Content2" contentplaceholderid="MainContent" runat="server">

    <div id="page-content">
        <%: Html.Hidden("ASPage","advancedSearchPage", new {id="page"}) %>
        <div id="page-title"><img src="../../Content/more_images/Company.png" alt=""/>
             <div class="form-text-links">
                <span class="form-header-text">Search Results</span>
            </div>
            </div>
        <div class="field-data">
            <div id="advanced-search-results">
               <%Html.RenderPartial("SearchResults", Model); %>
            </div>
        </div>
    </div>

</asp:content>
<asp:content id="Content3" contentplaceholderid="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/advanced-search.js" type="text/javascript"></script>
    <link href="../../Content/advanced-search.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/user-active-deactive.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/password.js" type="text/javascript"></script>
    <link href="../../Content/covernote-history.css" rel="stylesheet" type="text/css" />
    <!--<link href="../../Content/Policies.css" rel="stylesheet" type="text/css" />-->
</asp:content>
<asp:content id="Content4" contentplaceholderid="ContentPlaceHolder1" runat="server">
</asp:content>
