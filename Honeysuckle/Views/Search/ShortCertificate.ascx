﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.VehicleCertificate>>" %>

<%// Html.RenderPartial("~/Views/VehicleCertificate/ShowShortCertificate.ascx", Model); %>

    <%: Html.Hidden("count", Model.Count(), new { @class = "dont-process" })%>

    <% 
        var policies_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Policies", "Read"));
        var vehicles_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Vehicles", "Read"));
        var certificates_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Read"));
    %>

    <div class="ivis-table">
        <div class="shortcert menu">
            <div class="certtype item">
                Cert. No.
            </div>
            <div class="effdate item">
                Effective
            </div>
            <div class="regno item">
                Reg. No.
            </div>
            <div class="compname item">
                Comp Name
            </div>
        </div>
        <div class="scroll-table">
            <% int i = 0; %>
            <% foreach(var item in Model){ %>
                <% i++; %>
                <% if(i%2 == 0)
                    { %>
                <div class="shortcert data even row certificate">
                <% } %>
                <% else
                    { %>
                <div class="shortcert data odd row certificate">
                <% } %>
                    <%: Html.Hidden("cert_id", item.VehicleCertificateID, new { @class = "id dont-process" })%>
                    <%: Html.Hidden("risk_id", item.Risk.ID, new { @class = "risk_id dont-process" })%>
                    <%: Html.Hidden("policy_id", item.policy.ID, new { @class = "policy_id dont-process" })%>
                    <div class="certrowdata">
                        <div class="certtype item"><%: item.CertificateNo%></div>
                        <div class="effdate item"><%: String.Format("{0:dd MMM yyyy}", item.EffectiveDate.Date)%></div>
                        <div class="regno item"><%: item.Risk.VehicleRegNumber%></div>
                        <div class="compname item"><%: item.company.CompanyName%></div>
                    </div>
                    <div class="certrow functions item">
                        <% if (policies_read && false) { %> 
                            <div class="icon policy" title="View Related Policy Details">
                                <img src="../../Content/more_images/view_related_policy_icon.png" class="sprite"/>
                            </div>
                        <% } %>
                        <% if (vehicles_read) { %> 
                            <div class="icon risk" title="View Related Vehicle Details">
                                <!--<img src="../../Content/more_images/view_related_vehicle_icon.png" class="sprite"/>-->
                                <img src="../../Content/more_images/related-vehicle.png" class="sprite" />
                            </div>
                        <% } %>
                        <% if (certificates_read) { %> 
                            <div class="icon view" title="View Certificate Details">
                                <img src="../../Content/more_images/view_icon.png" class="sprite"/>
                            </div>
                        <% } %>
                    </div>

                </div>
            <% } %>
        </div>
    </div>