﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Employee>" %>

<div class="editor-label">
    Active:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.Active) %>
</div>

<% Html.RenderPartial("~/Views/People/AuditPeople.ascx", Model.person); %>
