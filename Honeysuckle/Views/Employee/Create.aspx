﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Employee>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            <legend></legend>

            <h2>Create</h2>

             <div class="editor-label">
                <%: Html.LabelFor(model => model.person.TRN)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.person.TRN, new { @required = "required", id = "trn" })%>
                <%: Html.ValidationMessageFor(model => model.person.TRN)%>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.person.fname)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.person.fname, new { @required = "required", id = "fname" })%>
                <%: Html.ValidationMessageFor(model => model.person.fname)%>
            </div>

           <div class="editor-label">
                <%: Html.LabelFor(model => model.person.mname)%>
            </div>
            <div class="editor-field">
                <%: Html.HiddenFor(model => model.person.PersonID, new { id = "personid" })%>
                <%: Html.TextBoxFor(model => model.person.mname, new { id = "mname" })%>
                <%: Html.ValidationMessageFor(model => model.person.mname)%>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.person.lname)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.person.lname, new { @required = "required", id = "lname" })%>
                <%: Html.ValidationMessageFor(model => model.person.lname)%>
            </div>

            <!-- THIS IS CHECK FOR IF YOU'RE A SYSTEM ADMIN, WHICH MEANS YOU CAN MAKE EMPLOYEES FOR ANY CO. AS OPPOSED TO A THE COMPANY YOU ARE IN -->
            <%  if(Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user))
                { %>
                <div class="editor-label">
                    Company:
                </div>
                <div class="editor-field">
                   <% Html.RenderAction("CompanyDropDown", "Company"); %>
                   <%//  Html.DropDownList("dropdownlist", Model.companies, new { id = "companyID" } ) %>
                </div>
            <% } %>
            <% else
               { %>
                <!-- 
                    WHEN YOU ARE JUST A COMPANY ADMINISTRATOR, OR REGULAR USER GIVEN THE POWER TO CREATE EMPLOYEES
                    YOU WILL ONLY CREATE EMPLOYEES IN YOUR COMPANY.    
                    -->
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.company.CompanyName)%>
                </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.company.CompanyName, new {id = "companyname", @readonly = "readonly", Value = Honeysuckle.Models.Constants.user.employee.company.CompanyName}) %>
                </div>
            <% } %>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.person.address.roadnumber)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.person.address.roadnumber, new { id = "RoadNo" })%>
                <%: Html.ValidationMessageFor(model => model.person.address.roadnumber)%>
            </div>
            <div class="editor-label">
                <%: Html.LabelFor(model => model.person.address.road.RoadName)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.person.address.road.RoadName, new { id = "RoadName", @required = "required" })%>
                <%: Html.ValidationMessageFor(model => model.person.address.road.RoadName)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.person.address.roadtype.RoadTypeName)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.person.address.roadtype.RoadTypeName, new { id = "RoadType" })%>
                <%: Html.ValidationMessageFor(model => model.person.address.roadtype.RoadTypeName)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.person.address.zipcode.ZipCodeName)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.person.address.zipcode.ZipCodeName, new { id = "ZipCode" })%>
                <%: Html.ValidationMessageFor(model => model.person.address.zipcode.ZipCodeName)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.person.address.city.CityName)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.person.address.city.CityName, new { id = "CityName" })%>
                <%: Html.ValidationMessageFor(model => model.person.address.city.CityName)%>
            </div>
            <div class="editor-label">
                <%: Html.LabelFor(model => model.person.address.country.CountryName)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.person.address.country.CountryName, new { id = "CountryName", @required = "required" })%>
                <%: Html.ValidationMessageFor(model => model.person.address.country.CountryName)%>
            </div>
            
            <p>
                <input type="submit" value="Create" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Back to List", "Index") %>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/person-trn.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftBar" runat="server">
</asp:Content>

