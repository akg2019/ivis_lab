﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Usage>" %>

<div class="usage-row">
    <div class="editor-field">
        <b>Usage Description</b> <i>eg Social and Domestic</i>
        <br /><br />
        <%: Html.TextBoxFor(model => model.usage, new { @class = "usage-field _uDropdown", @required = "required" })%>
        <br />
        <b>Cert Type </b> <i>eg J.X.13</i>
        <br /><br />
         <%: Html.TextBoxFor(model => model.CertCode, new { @class = "certcode-field _certDropDown", @required = "required" })%>
        
        
    </div>
</div>
<script src="../../Scripts/ApplicationJS/LookupData.js" type="text/javascript"></script>