﻿<%@ Page Title="Role Management" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.UserGroup>>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

        <div id= "form-header">
            <img src="../../Content/more_images/User.png" alt=""/>
             <div class="form-text-links">
            <span class="form-header-text">
                Role Management
            </span>
            <div  class="search-box index-search">
                <%= Html.TextBox("search-rm", null, new { @PlaceHolder = "Search [Roles]", @autocomplete = "off", @class = "quick-search rolemanagement-index" })%>
                <button type="button" class="quick-search-btn user-search-btn" onclick="clickSearchBtn()" ><img src="../../Content/more_images/Search_Icon.png" /></button>
            </div>
            </div>
        </div>
           
        <div class="field-data">
            
            <% if (ViewData["error"] != null) { %> <div class="error"> <%: Html.Encode(ViewData["error"]) %> </div> <%} %>
            <% if (ViewData["message"] != null) { %> <div class="message"> <%: Html.Encode(ViewData["message"]) %> </div> <%} %>
            <div id="newUser" ><%:ViewData["userCreated"]%></div>

            <% 
                bool isAdmin = Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user);
                bool usergroupupdate = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("UserGroups", "Update"));
                bool usergrouppurge = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("UserGroups", "Purge"));
                bool usergroupdelete = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("UserGroups", "Delete"));
                bool usergroupcreate = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("UserGroups", "Create"));
                bool usergroupupload = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("UserGroups", "Upload"));
            %>
            
            <div class="ivis-table">
                <div class="menu">
                    <div class="user-group-name item">
                        User Group Name
                    </div>
                    <% if (isAdmin) { %>
                    <div class="company-name item">
                        Company
                    </div>
                    <div class="stock-group item">
                        Stock
                    </div>
                    <div class="system-group item">
                        System
                    </div>
                    <% } %>
                </div>
                <div id="user-groups">
                
                    <% for (int i = 0; i < Model.Count(); i++) { %>
                        <% if (i%2 == 0) { %> <div class="user-group even row data"> <% } %>
                        <% else { %> <div class="user-group odd row data"> <% } %>

                            <%: Html.Hidden("hide", Model.ElementAt(i).UserGroupID, new { @class = "usergroupid id dont-process" })%>
                            <div class="user-group-name item">
                                <%: Model.ElementAt(i).UserGroupName %>
                            </div>
                            <% if (isAdmin) { %>
                            <div class="company-name item">
                                <%: Model.ElementAt(i).company.CompanyName %>
                            </div>
                            <div class="stock-group item">
                                <%: Html.CheckBoxFor(model => model.ElementAt(i).stock, new { @disabled = "disabled" })%>
                            </div>
                            <div class="system-group item">
                                <%: Html.CheckBoxFor(model => model.ElementAt(i).system, new { @disabled = "disabled" })%>
                            </div>
                            <% } %>
                            <div class="item functions">
                                <% if (usergroupupdate) { %>
                                    <div class="icon edit" title="Edit Role"> 
                                        <img src="../../Content/more_images/edit_icon.png" class="sprite" />
                                    </div>
                                <% } %>
                                <% if (usergrouppurge) { %>
                                        <div class="icon purge" title="Purge Role">
                                            <img src="../../Content/more_images/purge_icon.png" class="sprite" />
                                        </div>
                                <% } %>
                                <% if (usergroupdelete) { %>
                                    <div class="icon delete" title="Delete Role"> 
                                        <img src="../../Content/more_images/delete_icon.png" class="sprite" />
                                    </div>
                                <% } %>
                                <% if (usergroupupdate) { %>
                                        <div class="icon assign" title="Assign Role to Users">
                                            <img src="../../Content/more_images/user_role_icon.png" class="sprite" />
                                        </div>
                                <% } %>
                                <% if (isAdmin)
                                    { %>
                                        <div class="icon takerole" title="Take Role">
                                            <img src="../../Content/more_images/take_role_icon.png" class="sprite" />
                                        </div>
                                <% } %>
                            </div>
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>
            <div class="btnlinks">
                <% if (usergroupcreate) { %>
                    <button onclick="NewRole()">New Role</button> 
                <% } %>
                <% if (usergroupupload && false) { %>
                    <button onclick="Upload()">Upload Role</button> 
                <% } %>

                
        </div>
    </div>
    
    
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/rolemanagement-index.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/ApplicationJS/rolemanagement-index.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
