﻿<%@ Page Title="First Log In" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.User>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <div id="pass"></div>
        <% if (ViewData["missmatched"] != null){ %>
            <% if ((bool)ViewData["missmatched"]){ %>
                <div class="missmatched"></div>
            <% } %>
        <% } %>
        <% if (ViewData["password"] != null){ %>
            <% if ((bool)ViewData["password"]){ %>
                <div class="password-failed-validation"></div>
            <% } %>
        <% } %>

        <div id="first-login-form">
            <div id= "questionHeading"> <div id="UserImg"><img src="../../Content/more_images/User.png" alt=""/></div><div id="sQuestion"> First Login</div></div>
            <div>
                <asp:Label ID="BreachLabel" runat="server" Text=""></asp:Label></div>
            <div class="field-data">
                <div class="editor-label">
                    Username
                </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.username, new { id = "username", @class = "read-only", @required = "required", @readonly = "readonly"})%>
                    <%:Html.ValidationMessageFor(model => model.username) %>
                </div>

               
               <div class="passRules"> Password must contain: numbers, capital & common letters, special characters (!,@,#,$ ,%,^,&,*,(,)) and at least 8 characters</div>
                <div class="editor-label">
                    New Password:
                </div>
                <div class="editor-field">
                    <%: Html.PasswordFor(model => model.password, new { id = "password1", @class= "password", @required = "required" })%>
                </div>
                <div class="generateLink"> <a href="#" class="generate" onclick="generatePassword();"> Generate Password</a></div> 
                
                <div class="editor-label">
                    Re-Enter Password:
                </div>
                <div class="editor-field">
                    <%: Html.PasswordFor(model => model.confirm_password, new { id = "confirm_password", @class= "password", @required = "required"})%>
                </div>
            
                <div class="editor-label">
                    Security Question
                </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.question, new { id = "question", @required = "required", @class = "anything" })%>
                    <%: Html.ValidationMessageFor(model => model.question)%>
                </div>
            
                <div class="editor-label">
                    Response
                </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.response, new { id = "response", @required = "required", @class = "anything" })%>
                    <%: Html.ValidationMessageFor(model => model.response)%>
                </div> 
            </div>
            
            <div class="btnlinks flogin">
                <input type="submit" value="Create" id="Submit2" />
            </div>
        </div>

    <% } %>
        
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/loginform.js" type="text/javascript"></script>
    <link href="../../Content/LoginPage.css" rel="stylesheet" type="text/css" />
</asp:Content>


