﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Duplicate>" %>

<% 
    switch(Model.type){ 
        case "policy":
            Html.RenderPartial("~/Views/Policy/AuditPolicy.ascx", Model.ref_obj);
            break;
        default:
            break;
    } 
%>