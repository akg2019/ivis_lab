﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Phone>" %>

    <div class="newphone">
        <% using (Html.BeginCollectionItem("phoneNums")) { %>
            <%: Html.ValidationSummary(true) %>
            
            <div class="row-field">
                <% if (Model.PhoneNumberId != 0 && ViewData["EditCompContact"] == null)
                   { %>
                   <% if (ViewData["Details"] != null && (bool)ViewData["Details"])
                      { %>
                        <div class="editor-label">
                         Phone #:
                        </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.PhoneNumber, new { @class = "telNum read-only", @required = "required",@readonly="readonly" })%>
                        <%: Html.ValidationMessageFor(model => model.PhoneNumber)%> 
                    </div>
                    <div class="editor-label">
                        Ext:
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.Extension, new { @class = "extNum read-only", @required = "required",@readonly="readonly" })%>
                        <%: Html.ValidationMessageFor(model => model.Extension)%> 
                    </div>
                    <div class="primaryTelNum">
                        <div class="editor-field">
                            Is Primary: 
                        </div>
                        <div class="editor-field">
                            <%: Html.CheckBoxFor(model => model.primary, new { @class = "phonePrimary read-only",@readonly="readonly",disabled = "disabled" })%>
                        </div>
                    </div>
                   <%}
                      else
                      { %>
                <div class="editor-label">
                    Phone #:
                </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.PhoneNumber, new { @class = "telNum", @required = "required" })%>
                    <%: Html.ValidationMessageFor(model => model.PhoneNumber)%> 
                </div>
                <div class="editor-label">
                    Ext:
                </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.Extension, new { @class = "extNum", @required = "required" })%>
                    <%: Html.ValidationMessageFor(model => model.Extension)%> 
                </div>
                <div class="primaryTelNum">
                    <div class="editor-field">
                        Is Primary: 
                    </div>
                    <div class="editor-field">
                        <%: Html.CheckBoxFor(model => model.primary, new { @class = "phonePrimary" })%>
                    </div>
                </div>

                <% }
                 
                 }
                   else
                   {%>
                    <div class="editor-label">
                    Phone #:
                </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.PhoneNumber, new { @class = "telNum editComp", @PlaceHolder = "(876)555-5555", @required = "required" })%>
                    <%: Html.ValidationMessageFor(model => model.PhoneNumber)%> 
                </div>
                <div class="editor-label">
                    Ext:
                </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.Extension, new { @class = "extNum editComp",@required="required" })%>
                    <%: Html.ValidationMessageFor(model => model.Extension)%> 
                </div>
                <div class="primaryTelNum">
                    <div class="editor-field">
                        Is Primary: 
                    </div>
                    <div class="editor-field">
                        <%: Html.CheckBoxFor(model => model.primary, new { @class = "phonePrimary"})%>
                    </div>
                </div>
                <% } %>
                
          </div>
            <% } %>
    </div>
