﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.ApplicationConfiguration>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Application Configuration
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="intermediary-page">
        <div id="form-header">
            <img src="../../Content/more_images/Company.png"/>
            <div class="form-text-links">
                <span class="form-header-text">
                    Intermediaries
                </span>
            
            <div  class="search-box inter-search">
                <%= Html.TextBox("search-inter", null, new { @PlaceHolder = "Search [Intermediaries]", @autocomplete = "off", @class = "quick-search intermediary-index" }) %>
                <button type="button" class="quick-search-btn user-search-btn" onclick="clickSearchBtn()" ><img src="../../Content/more_images/Search_Icon.png" /></button>
            </div>
            </div>
        </div>
        <!-- div id="below-header">-->
        <div class="field-data">
            <div id="company-modal"></div>
            <!--<div id="form">-->
                <%// using (Html.BeginForm()){ %>
                <%: Html.Hidden("compid", int.Parse(ViewData["ID"].ToString()), new { @class = "dont-process" })%>
                
                <div class="ivis-table">
                
            
                    <div class="menu">
                        <div class="inter-company item">
                             Company
                        </div>
                        <div class="print-cert item">
                             Print Certificates
                        </div>
                        <div class="print-note item">
                            Print CoverNotes
                        </div>
                        <div class="print-limit item">
                            Print Limit
                        </div>
                    </div>

                    <div id="inter-data">
                        <% int i = 0; %>
                        <% foreach (Honeysuckle.Models.ApplicationConfiguration  inter in Model)
                           { }%>
 
        </div>
    </div>
   </div>
    <div class="btnlinks">
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <!--link href="../../Content/intermediaries.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/Intermediaries.js" type="text/javascript"></script>-->
    <link href="../../Content/Pages.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

