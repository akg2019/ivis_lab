﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.TAJ>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	TAJ Credentials
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm("TAJCredentials", "TAJ", FormMethod.Post, new { @autocomplete = "off" })) { %>
        <%: Html.ValidationSummary(true)%>
        
        
        <fieldset>

            <div id= "form-header">
                <img src="../../Content/more_images/Company.png" alt=""/>
                <div class="form-text-links">
                    <span class="form-header-text">
                        TAJ Credentials
                    </span>
                </div>
            </div>

            <div class="field-data">
                
                <div class="validate-section">
                    <div class="subheader">
                        TAJ Credential Information
                        <div class="valid"></div>
                    </div>
            
                    <%: Html.HiddenFor(model => model.Company.CompanyID, new { id = "CompanyId", @class = "dont-process" })%>

                    <div class="editor-label">
                        Company Name
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.Company.CompanyName, new { @readonly = "readonly", @class = "read-only" })%>
                    </div>

                    <div class="editor-label">
                      User Name
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBox("hidden_username", Model.CompUserName, new { @class = "taj username dont-process", @autocomplete = "off", @style = "display:none" })%>
                        <%: Html.TextBoxFor(model => model.CompUserName, new { @required = "required", @class = "taj username", @autocomplete = "off" })%>
                        <%: Html.ValidationMessageFor(model => model.CompUserName)%>
                    </div>
            
                    <div class="editor-label">
                       Password
                    </div>
                    <div class="editor-field">
                        <%: Html.Password("hidden_password", Model.CompPassowrd, new { @class = "taj password dont-process", @autocomplete = "off", @style = "display:none" })%>
                        <%: Html.PasswordFor(model => model.CompPassowrd, new { @autocomplete = "off", @required = "required", @class = "taj password" })%>
                        <%: Html.ValidationMessageFor(model => model.CompPassowrd)%>
                    </div>

                    <div class="check-div">
                        <div class="editor-label autoCanel">
                            Automatic Search Option
                        </div>
                        <div class="editor-field">
                            <%: Html.CheckBoxFor(model => model.DefaultOption, new { @id = "automatic" })%>
                        </div>
                    </div>
                </div>

            </div>
            
            <div class="btnlinks">
                <% Html.RenderAction("BackBtn", "Back"); %>
                <input type="submit" value="Submit" class="submitbutton" />
            </div>

        </fieldset>

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
</asp:Content>


