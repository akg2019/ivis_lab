﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Vehicle>>" %>

  <div class="insured-modal">
     
   <div class="ivis-table">

    <div class="vehicle-check item selectItem selectAll">
        <%: Html.CheckBox("selectAll", false, new { @class = "selectAll", @OnClick = "changeSelectAll(this)" })%>
        Select All
    </div>

     <div class= "menu vehicle-row">
        <!--<div class="item selectItem">Select</div> -->
        <div class="make item">Risk </div>
        <div class="templateWording item"> Wording Template </div>
        <div class="effectiveDate item"> Effective Date</div>
        <div class="element item expiryPicker">Expiry Date</div>
        <% if (ViewData["type"].ToString() == "coverNote")
           { %>
            <div class="item coverperiod picker">Quick Select Period</div>
        <% } %>
     </div>

        <div id="vehiclesListSelect">

          <% if(Model.Count() == 0) { %>
                    <div class="rowNone">No Vehicles in Selection at this time</div>
            <% } %>

            <% int i = 0; %>
            <% foreach (Honeysuckle.Models.Vehicle item in Model){ %>
                <% if (i % 2 == 0) { %><div class="vehicle-row row even"> <% } %>
                <% else { %> <div class="vehicle-row row odd"> <% } %>
                  <% i++; %>

                   <div class="vehicle-check">
                       <%: Html.CheckBox("vehcheck", false, new { @class = "vehcheck polVehList", id = item.ID.ToString()})%>
                    </div>

                   <%: Html.HiddenFor(model => item.ID, new { @class = "Vehicle_ID dont-process" })%> 

                    <div class="make item cover">
                        <b><%: item.VehicleRegNumber %></b>  <%: item.make %>   <%: item.VehicleModel %> <br /><b> <span style="color:blue;"> <%: item.chassisno %></span></b>
                    </div>
                   <%-- <div class="make item cover">
                        <%: item.chassisno %>  <%: item.make %>   <%: item.VehicleModel %>
                    </div>--%>
              
                    <div class="templateWording item" style="padding-right:5px;">
                        <% Html.RenderAction("TemplateDropDown", "TemplateWording", new { id = item.ID, polId = ViewData["PolID"]}); %>
                    </div>
                    <div class="effectiveDate item">
                    <%: Html.TextBox("thisEffectiveDate", DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt"), new { @class = "effective", @readonly = "readonly" })%>
                   </div>

                   <div class="expiryDate">
                    <%: Html.TextBox("thisExpiryDate", DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt"), new { @class = "expiryPicker", @readonly = "readonly" })%>
                   </div>
                   <% if (ViewData["type"].ToString() == "coverNote")
                      {%>
                        <div class="coverperiod coverbuttons item">
                               
                                    <button type="button" class="quickperiod coverdays" value="7" id = "quickperiod-coverdays-7" >7</button>
                                    <button type="button" class="quickperiod coverdays" value="14" id = "quickperiod-coverdays-14">14</button>
                                    <button type="button" class="quickperiod coverdays" value="30" id = "quickperiod-coverdays-30">30</button>
                      
                       </div>
                   <%} %>

                </div>
            <% } %>
         </div>
      </div> 
   </div> 
   
</div>

<script src="../../Scripts/ApplicationJS/quickcoverbuttons.js" type="text/javascript"></script>
