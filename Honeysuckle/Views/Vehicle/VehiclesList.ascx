﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Vehicle>>" %>

<div id="insured" class="insured-modal">
     <table style="width:100%!important; border-style:none!important;">
                    <tr>
                        <td style="width:30%!important;">&nbsp;Search By <b><i style="color:Green;">Reg#</i>  </b> <%:Html.RadioButton("search-type","reg",true) %></td>
                        <td style="width:30%!important;">&nbsp;Search By <b><i style="color:Green;">Chassis#</i> </b> <%:Html.RadioButton("search-type","chassis") %></td>
                        <td>
                            <div id="Div1" class="index-search">
                            <%: Html.TextBox("search-modal", null, new { @placeholder = "Search Vehicles", @autocomplete = "off", @class = "modalSearch vehSearch" })%> 
                            <button type="button" class="quick-search-btn" onclick="searchVehicle(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
                            </div>
                        </td>
                    </tr>
                   
               </table>
               <br />
               <div style ="height:2px;"></div>
    <div class="line">
        <span class="mess"><%: ViewData["HeadingMessage"]  %></span>
        <%--<div id="searching" class="index-search">
            <%: Html.TextBox("search-modal", null, new { @placeholder = "Search Vehicles", @autocomplete = "off", @class = "modalSearch vehSearch" })%> 
            <button type="button" class="quick-search-btn" onclick="searchVehicle(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
        </div>--%>
    </div>

         <div>
            
              <%-- <%: Html.TextBox("search-modal", null, new { @placeholder = "Search vehicle", @autocomplete = "off", @class = "modalSearch vehSearch" })%> 
            <button type="button" class="quick-search-btn" onclick="searchVehicle(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
            <span class="mess">&nbsp;&nbsp; Please search the system for existing vehicles, or add a new vehicle.:</span>--%>
            <div style ="height:10px;"></div>
        </div>

     
    <div class="ivis-table modal-list">
        <div class= "menu" > 
            <div class="select item">Select</div>
            <div class="chassisno item">Chassis No</div>
            <div class="make item">Make</div>
            <div class="model item">Model</div>
            <div class="regno item">Reg. No.</div>
        </div>
        <div id="vehiclelist-selected" class="static selectedSection"></div>

        <div id="vehiclesList">
            <% if(Model.Count() == 0) { %>
                    <div class="rowNone">There Are No Vehicles. Please add one.</div>
            <% } %>
            <% else  { %>
                <% if(Model.Count() > 50) { %>
                    <div class="rowNone">
                        <%--Please search the system for existing vehicles, or add a new vehicle. --%>
                    </div>
                <% } %>

                <% int i = 0; %>
                <% foreach (Honeysuckle.Models.Vehicle item in Model){ %>
                    <% if (i % 2 == 0) { %><div class="vehicle-row row even"> <% } %>
                    <% else { %> <div class="vehicle-row row odd"> <% } %>
                        <% i++; %>

                        <%  string vehmodel = "Not Available", regNo = "Not Available";
                            if (item.VehicleModel != null && item.VehicleModel != "") vehmodel = item.VehicleModel;
                            if (item.VehicleRegNumber != null && item.VehicleRegNumber != "") regNo = item.VehicleRegNumber; %>

                        <div class="vehicle-check select item">
                            <%: Html.CheckBox("vehcheck", false, new { @class = "vehcheck", id = item.ID.ToString(), @OnClick = "changeVehicleCheckBox(this)" })%>
                        </div>
                        <%: Html.HiddenFor(model => item.ID, new { @class = "vehicle_Id dont-process" })%>

                        <div class="chassisno item">
                            <%: item.chassisno %>
                        </div>
                        <div class="make item">
                            <%: item.make %>
                        </div>
                        <div class="model item">
                            <%: vehmodel %>
                        </div>
                        <div class="regno item">
                                <%: regNo %>
                        </div>
                     </div>
                  <% } %>
                 <% } %>
             </div>
          </div>
     </div>
  </div>
  <div class="recordsFound ResultModal green"></div>
<div class="btnlinks">
     <input type="button" value="Cancel" class="cancelButton" onclick="closeD()" />
     <input type="button" value="Create New" id="create" onclick="vehCreate()" />
     <input type="button" value="Add Selected" id="buttonAdd" onclick="getValue()" />
</div>