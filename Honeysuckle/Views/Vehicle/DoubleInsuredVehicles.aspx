﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.Vehicle>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	DoubleInsuredVehicles
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="double-insured-page">
        <div id="form-header">
            <img src="../../Content/more_images/Vehicle.png" alt="">
            <div class="form-text-links">
                <span class="form-header-text">
                    Double Insured Vehicles
                </span>
            </div>
        </div>

        <% 
            bool canView = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Vehicles", "Read"));
            bool canViewPolicy = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Policies", "Read"));
        %>
        <%: Html.Hidden("compID", Honeysuckle.Models.Constants.user.employee.company.CompanyID, new { @class="compID" }) %>
       
        <div class="field-data">
            <div class="ivis-table">
                <div class="menu">
                    <div class="di-chassisno item">Chassis No</div>
                    <div class="di-make item">Make</div>
                    <div class="di-model item">Model</div>
                    <div class="di-regno item">Reg. No.</div>
                </div>
                <div id="double-insured-data">
                    <% int i = 0; %>
                    <% foreach (Honeysuckle.Models.Vehicle vehicle in Model)
                       { %>
                            <% if (i % 2 == 1)
                               { %> <div class="double-inured vehicle data odd row"><% } %>
                            <% else
                               {%> <div class="double-inured vehicle data even row"><% } %>

                                <%: Html.HiddenFor(model => vehicle.ID, new {  @class = "id dont-process" })%>
                                <%: Html.HiddenFor(model => vehicle.base_id, new { @class= "base_id"}) %>

                                <div class="di-chassisno item"> <%: vehicle.chassisno %> </div>
                                <div class="di-make item"> <%: vehicle.make %> </div>
                                <div class="di-model item"> <%: vehicle.modelType %> </div>
                                <div class="di-regno item"> <%: vehicle.VehicleRegNumber %></div>

                                <div class="item functions">
                                    <% if (canView)
                                       { %>
                                        <div class="icon view" title="View Vehicle">
                                            <img src="../../Content/more_images/view_icon.png" class="sprite"/>
                                        </div>
                                    <% } %>

                                    <% if (canViewPolicy)
                                       { %>
                                       <div class="icon policies" title="View Policies">
                                            <img src="../../Content/more_images/view-related-policy-icon.png" class="sprite">
                                        </div>
                                    <% } %>
                                </div>
                            </div>

                       <% i++; %>
                      <% } %>
                </div>
            </div>
        
        </div>

        <div class="btnlinks">
                <% Html.RenderAction("BackBtn", "Back"); %>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/policyDialog.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
