﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Vehicle>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	EditDrivers
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <legend>Edit Drivers</legend>
            
            <div class="editor-label">
                <%: Html.HiddenFor(model => model.ID, new { @class = "dont-process" })%>
                <%: Html.Hidden("policyid", int.Parse(ViewData["pol"].ToString()), new { @class = "dont-process" })%>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.VIN) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.VIN, new { @readonly = "readonly", id= "vin"})%>
                <%: Html.ValidationMessageFor(model => model.VIN) %>
            </div>
           
           <div id="person-modal"></div>
            <div id="authorizedDriv">
              <div id="auth"></div>
               <h6><b>Main Driver (TRN):</b>  <%: Html.Display("mainDriver", null, new { id = "main", @readonly = "readonly", @Value = ViewData["mainDriver"] })%></h6>
                    <% if (Model.AuthorizedDrivers != null)
                       { %>
                     <div id = "authorized1">   <h4> Authorized Drivers</h4> </div>
                       <input type="button" value="Add New" id="authorized"/>
                        <br />
                        <% foreach (Honeysuckle.Models.Driver AuthorizedDriver in Model.AuthorizedDrivers)
                           { %>
                                <% Html.RenderAction("Drivers", "Driver", new { id = AuthorizedDriver.person.PersonID, vehId = Model.ID, type = 1 }); %>
                        <% } %>
                    <% } %>
                   <div id="authorizedDriver"></div>
                </div>
        
        <div id="exceptedDriv">
          <div id="except"></div>
                    <% if (Model.ExceptedDrivers != null)
                       { %>
                       <div id = "excepted1">  <h4> Excepted Drivers</h4> </div>
                       <input type="button" value="Add New" id="excepted"/>
                        <% foreach (Honeysuckle.Models.Driver ExceptedDriver in Model.ExceptedDrivers)
                           { %>
                            <% Html.RenderAction("Excepted", "Driver", new { id = ExceptedDriver.person.PersonID, vehId = Model.ID, type = 2 }); %>
                        <% } %>
                    <% } %>
                    <div id="exceptedDriver"></div>
                </div>
        
        <div id="exclDriv">
            <div id="excl"></div>
                    <% if (Model.ExcludedDrivers != null)
                       { %>
                      <div id = "excluded1"><h4> Excluded Drivers </h4></div>
                      <input type="button" value="Add New" id="excluded"/>
                        <% foreach (Honeysuckle.Models.Driver ExcludedDriver in Model.ExcludedDrivers)
                           { %>
                            <% Html.RenderAction("Excluded", "Driver", new { id = ExcludedDriver.person.PersonID, vehId = Model.ID, type = 3 }); %>
                        <% } %>
                    <% } %>
                   <div id="excludedDriver"></div>
                </div>

            <div id="createDriv"></div>
            <div id="createDriver"></div>
            <br /> <br /><br />
            
            <p>
                <input type="button" value="Back" id="backButton" onclick=" window.history.back();"/>
                <input type="button" value="Done" id="Done"/>
            </p>
        </fieldset>

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
<script src="../../Scripts/ApplicationJS/addDrivers.js" type="text/javascript"></script>
<script src="../../Scripts/ApplicationJS/personDetails.js" type="text/javascript"></script>
</asp:Content>


