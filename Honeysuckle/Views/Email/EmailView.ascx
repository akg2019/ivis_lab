﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Email>" %>

    <div class="newemail view">
        <% using (Html.BeginCollectionItem("emails")) 
           { %>
            <%: Html.ValidationSummary(true) %>
            <div class="email-data">
                <% if (Model.emailID != 0)
                    { %>
                    <div class="editor-field">
                        <%: Html.HiddenFor(model => model.emailID, new { @class = "dont-process" })%>
                        <%: Html.TextBoxFor(model => model.email, new { @readonly = "readonly", id="email", @class = "read-only" })%>
                        <%: Html.ValidationMessageFor(model => model.email)%>
                    </div>
                <% } %>
            </div>
            <div class="primaryLabel">
                <%: Html.CheckBoxFor(model => model.primary, new { @class = "emailprimary", @disabled = "disabled", @title = "if selected this is the primary contact email for this person" })%>
            </div>
        <% } %>
    </div>


