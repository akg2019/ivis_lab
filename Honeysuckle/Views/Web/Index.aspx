﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head id="Head1" runat="server">
        <title>External Access</title>

        <!-- JQUERY -->
        <script src="../../Scripts/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../Scripts/jquery-ui-1.10.4.min.js" type="text/javascript"></script>
        <link href="../../Content/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

        <script src="../../Scripts/ApplicationJS/external.js" type="text/javascript"></script>
        <link href="../../Content/ExternalAccess.css" rel="stylesheet" type="text/css" />
        <link href="../../Content/ExternalAccess-PartialWidth.css" rel="stylesheet" media="screen and (max-width:1024px)" type="text/css" />
        <link href="../../Content/Site.css" rel="stylesheet" type="text/css" />

        <link href="../../Content/maindiv-hd.css" rel="stylesheet" type="text/css" />
        <link href="../../Content/maindiv-fullheight.css" media='screen and (min-height: 900px) and (max-height: 1000px)' rel="stylesheet" type="text/css" />
        <link href="../../Content/maindiv-partialheight.css" rel="stylesheet" media="screen and (max-height:900px)" type="text/css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    
    
    </head>

    <style>


   


/* Center the loader */
#loader {
  position: absolute;
  left: 50%;
  top: 50%;
  z-index: 1;
  width: 150px;
  height: 150px;
  margin: -75px 0 0 -75px;
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid yellowgreen;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
  
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

/* Add animation to "page content" */
.animate-bottom {
  position: relative;
  -webkit-animation-name: animatebottom;
  -webkit-animation-duration: 1s;
  animation-name: animatebottom;
  animation-duration: 1s
}

@-webkit-keyframes animatebottom {
  from { bottom:-100px; opacity:0 } 
  to { bottom:0px; opacity:1 }
}

@keyframes animatebottom { 
  from{ bottom:-100px; opacity:0 } 
  to{ bottom:0; opacity:1 }
}

#myDiv {
  display: none;
  text-align: center;
}
</style>

    <body onload = "HideSpinner()" ng-app="eGovApp" ng-controller="MyeGovController" >
        <div class="page">
            <div id="header">
                <div id="header-content">
                    <a href="/Home/"> <img src='/Content/more_images/IVIS_Logo.png' id = "IconContainer" /> </a>
                    <div id="selfdata"></div>
                </div>
            </div>
            <div id="main-div">
                <div id="maincontent">
                    <div id="maincontent-insert">
                        <div class="field-data">
                            <% using(Html.BeginForm()) { %>
                                <div id="searchModal" hidden="hidden"><%: ViewData["searchDone"].ToString() %></div>
                                <div id= "WelcomeMessage" hidden ="hidden">  Welcome to the vehicle certificate and cover note verification sytem.</div>
                                <div id="instructions"  hidden="hidden"> Follow the instructions below to verify if the vehicle is under cover by an insurance company </div>
                               
                               <h3 style="width:90%; margin:auto; text-align:center; ">Vehicle Certificate and Cover Note Verification System.</h3>
                                <br />
                                <p class="alert alert-info" style="width:90%; margin:auto;">
                                    Enter your vehicle registration number or chassis number to check your vehicle insurance status.
                                    <%--Enter both vehicle registration number and chassis number for additional details--%>
                                </p>



                                <br /><br />

                                <div id="TestRisk"  hidden="hidden">
                                    <div class= "Steps"> 
                                       <div class="rectangle" id="rec1">Step</div>
                                       <div class="triangles" id="tri1"></div> 
                                       <div class="numbers">01</div> 
                                    </div>
                                    <%--<div class="dataField"> Enter the license plate number  <%: Html.TextBox("license") %>  </div>
                                        <div id="OrDiv"> - OR - </div>
                                    <div class="dataField"> Enter the chassis number  <%: Html.TextBox("chassis") %> </div>  --%>
                                </div>

                                <div id="submitRisk" hidden="hidden">
                                    <div class= "Steps">
                                    <div class="rectangle" id="rec2">Step</div>
                                    <div class="triangles" id="tri2"></div>
                                    <div class="numbers">02</div> 
                                    </div>
                                    <div class="dataField1"><br /> Click Search 
                                    <input type="submit" value="Search" disabled="disabled" id="subbtn"  onclick="ClearText()" />
                                    </div>
                                </div>

                                <div id="Results" hidden="hidden">
                                    <div class= "Steps">
                                        <div class="rectangle" id="rec3">Step</div>
                                        <div class="triangles" id="tri3"></div>
                                        <div class="numbers">03</div> 
                                    </div>
                                    <div class="dataField1">Your results will appear here: </div>

                                    <% string Answer= ""; %>
                                    <% 
                                        bool DataErrors = false;
                                        try { DataErrors = (bool)ViewData["error"]; }
                                        catch (Exception) { }
                                   
                                        if (ViewData["model"] != null && !DataErrors )
                                        {
                                            var CoverResponse = Newtonsoft.Json.Linq.JArray.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(ViewData["model"]));
                                            bool IsCovered = false;

                                            try { IsCovered = (bool)CoverResponse[0]["Vehicle_Covered"]; }
                                            catch (Exception) { }
                                                                                        
                                            if (IsCovered)
                                            {
                                                Answer =  " Status:  Covered"
                                                + Environment.NewLine + "  Company: " + CoverResponse[0]["CompanyName"]; 
                                            }
                                            else { Answer =  " Status: Not Covered"; }
                                        } 
                                    %>
                                    <%: Html.TextArea("reply", Answer, new {id= "ResponseField", @readonly= "readonly"}) %>


                                </div>

                                   
                                    <div class="row list-group-item CoverStatus">

                                    <div class="SearchBar">
                                          <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <%: Html.TextBox("license", "", new { @class = "form-control", @PlaceHolder = "Reg/Plate Number", @id = "plate", @onchange = "CheckValidity()" })%>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                               <%: Html.TextBox("chassis", "", new { @class = "form-control", @PlaceHolder = "Chassis Number", @id = "chas" })%>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <p class="btn btn-success" ng-click= "CheckCoverage()" id="subbtn22"  style="width:100%;">SEARCH</p>
                                            </div>

                                         </div>
                                          <p style="text-align:center; color:Red; font-size:14px; word-break:break-word" id="ErrMSG"></p>
                                      </div>
                                      <p style="text-align:left" id="SearchStatus" hidden ="hidden"></p>

                                     

                                    <hr />
                                      
                                  

                                    <% 
                                        bool DataError = false;
                                        string ExpiryDates = "";
                                       
                                   
                                        try {DataError = (bool)ViewData["error"]; }
                                        catch (Exception) { }

                                        if (ViewData["model"] != null && !DataError)
                                        {
                                            var CoverResponse = Newtonsoft.Json.Linq.JArray.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(ViewData["model"]));

                                            bool IsCovered = false;
                                            bool FullDetails = false;

                                            try { FullDetails = (bool)ViewData["FullDetails"]; }
                                            catch (Exception) { }

                                            try { IsCovered = (bool)CoverResponse[0]["Vehicle_Covered"]; }
                                            catch (Exception) { }

                                            try { ExpiryDates = Convert.ToDateTime(CoverResponse[0]["ExpiryDate"].ToString()).ToLongDateString(); }
                                            catch (Exception) { ExpiryDates = "Contact us for information"; }

                                            if (IsCovered)
                                            {
                                                Answer = " Status:  Covered"
                                                + Environment.NewLine + "  Company: " + CoverResponse[0]["CompanyName"]; %>
                                       <div class="row" id="Covered" ng-show="Coverage.VehicleCoverage[0].Vehicle_Covered">
                                                <div style="width:255px; margin:auto; padding-right:15px;">
                                                    <img src="../../Content/images/IVIS_COVERED.png" alt="Your vehicle is covered" class="Covered"  style="max-height:250px; background-position:center;"/>
                                                    <p style="color:green; text-align:center; padding-left:7px; padding-bottom:10px;"><b style="color:green;"><%: Html.Label("COVERED!")%></b></p>
                                                    
                                                </div>
                                                 

                                           <div class="col-md-12">
                                            
                                                
                                                <h6 style="text-align:center; padding-bottom:10px">Your vehicle is currently covered...</h6>
                                                <%--<p><%: Html.Label("Company: " + CoverResponse[0]["CompanyName"].ToString())%></p>--%>

                                                
                                                   <% if (FullDetails) 
                                                    { 
                                                    %>
                                                  
                                                    <div class="row">
                                                        <div class="col-md-5"><b>Make</b></div>
                                                        <div class="col-md-7"><%: Html.Label(CoverResponse[0]["Make"].ToString()) %></div>
                                                    </div>
                                                     <div class="row">
                                                        <div class="col-md-5"><b>Model</b></div>
                                                        <div class="col-md-7"><%: Html.Label(CoverResponse[0]["Model"].ToString())%></div>
                                                    </div>
                                                     <div class="row">
                                                        <div class="col-md-5"><b>Year</b></div>
                                                        <div class="col-md-7"><%: Html.Label(CoverResponse[0]["VehicleYear"].ToString())%></div>
                                                    </div>
                                                     <div class="row">
                                                        <div class="col-md-5"><b>Chassis</b></div>
                                                        <div class="col-md-7"><%: Html.Label(CoverResponse[0]["ChassisNo"].ToString())%></div>
                                                    </div>
                                                     <div class="row">
                                                        <div class="col-md-5"><b>Vehicle Registration#</b></div>
                                                        <div class="col-md-7"><%: Html.Label(CoverResponse[0]["VehicleRegistrationNo"].ToString())%></div>
                                                    </div>
                                                     <div class="row">
                                                        <div class="col-md-5"><b>Vehicle Colour</b></div>
                                                        <div class="col-md-7"><%: Html.Label(CoverResponse[0]["Colour"].ToString())%></div>
                                                    </div>

                                                     <div class="row">
                                                        <div class="col-md-5"><b>Insurance Company</b></div>
                                                        <div class="col-md-7"><%: Html.Label(CoverResponse[0]["CompanyName"].ToString())%></div>
                                                    </div>
                                                   
                                                     <div class="row">
                                                        <div class="col-md-5"><b>Cover Expiry Date</b></div>
                                                        <div class="col-md-7"><%:Html.Label(ExpiryDates)%></div>
                                                                                  
                                                                                  
                                                                                  
                                                                              
                                                    </div>
                                                
                                                    <%}%>
                                                
                                            </div>
                                        </div>
                                        

                                            <%}
                                            else
                                            { %>
                                                
                                        <div class="row" id="NotCovered" ng-show="!Coverage.VehicleCoverage[0].Vehicle_Covered">
                                            <div style="width:255px; margin:auto; padding-right:15px;">
                                                    <img src="../../Content/images/IVIS_NOTCOVERED.png" alt="Your vehicle is covered" class="Covered"  style="max-height:250px; background-position:center;"/>
                                                    <p style="color:red; text-align:center; padding-left:7px; padding-bottom:10px;"><b style="color:red;"><%: Html.Label("NOT COVERED")%></b></p>
                                            </div>
                                            <div class="col-md-12">
                                                <h6 style="text-align:center; padding-bottom:10px">Your vehicle is <b style="color:Red">NOT</b> covered.</h6>
                                            </div>
                                           
                                        </div>
                                            
                                            
                                            <%}
                                        }
                                        else if(DataError)
                                        { %>
                                            
                                            
                                            <div style="width:455px; margin:auto; padding-right:15px;" id="ErrorDiv">
                                                    <img src="../../Content/images/IVIS_OOPS.png" alt="Your vehicle is covered" class="Covered"  style="max-height:450px; background-position:center;"/>
                                                   
                                                    
                                            </div>
                                              
                                        
                                       <% }
                                    %>

                                       
                            <% } %>


                            <div id="resltArea" class="HideArea">

                            <div ng-show="!Coverage.error">
                             <div class="row" id="Covered" ng-show="Isvalid==true" >
                                                <div style="width:255px; margin:auto; padding-right:15px;">
                                                    <img src="../../Content/images/IVIS_COVERED.png" alt="Your vehicle is covered" class="Covered"  style="max-height:250px; background-position:center;"/>
                                                    <p style="color:green; text-align:center; padding-left:7px; padding-bottom:10px;"><b style="color:green;"><%: Html.Label("COVERED!")%></b></p>
                                                    <p style="color:green; text-align:center; padding-left:7px; padding-bottom:10px;"><b>Insurer: </b> {{InsuredCompany}}</p>
                                               </div>
                                                 
                                        
                                        
                            </div>        
                              
                               <div class="row" id="NotCovered" ng-show="Isvalid == false">
                                            <div style="width:255px; margin:auto; padding-right:15px;">
                                                    <img src="../../Content/images/IVIS_NOTCOVERED.png" alt="Your vehicle is covered" class="Covered"  style="max-height:250px; background-position:center;"/>
                                                    <p style="color:red; text-align:center; padding-left:7px; padding-bottom:10px;"><b style="color:red;"><%: Html.Label("NOT COVERED")%></b></p>
                                            </div>
                                            <div class="col-md-12">
                                                <h6 style="text-align:center; padding-bottom:10px">Your vehicle is <b style="color:Red">NOT</b> covered.</h6>
                                                <h5 style="text-align:center; padding-bottom:10px"><b>Insurer: </b>{{InsuredCompany}}</h5>
                                            </div>
                                           
                                        </div>    
                            </div>

                            </div>

                            <div ng-show="Coverage.error" class="HideArea" id="err">
                              <div style="width:455px; margin:auto; padding-right:15px;" id="ErrorDiv">
                                                    <img src="../../Content/images/IVIS_OOPS.png" alt="Your vehicle is covered" class="Covered"  style="max-height:450px; background-position:center;"/>
                                                   
                                                    
                              </div>

                            </div>

                            
                        </div>

                         <div>
                                <div id="loader"></div>
                             </div>
                    </div>
                    </div>
                </div>
            </div>
            <div id="footer">
                <div id="whitespace"></div>
                <div id="inner-footer">
                    <div id="Logofooter">
                        <img src="../../Content/more_images/IVIS_Logo_cropped.png" />
                        <p style="text-align:center">2014 IVIS, All Rights Reserved</p>
                    </div> 
                    <div class="footlinks">
                        <%: Html.ActionLink("Send Feedback", "Index", "Feedback", null, new { id = "feedback" })%>
                        <%: Html.ActionLink("Help", "Index", "Help", null, new { id = "help" })%>
                    </div>
                </div>
            </div>      
        </div>
</div>


        
  <script type="text/jscript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script type="text/jscript"src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script type="text/jscript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.js"></script>
  <script type="text/javascript" src="../../Scripts/ApplicationJS/AngularDataManagement.js"></script> 
        
         <script type="text/javascript">

             function ClearText() {
                 document.getElementById("SearchStatus").value = "Searching........";

                 $("#SearchStatus").html("Searching........");
                 $("#Covered").hide();
                 $("#NotCovered").hide();
                 $("#loader").show();
                 $("#ErrorDiv").hide();
             }

             function HideSpinner() { $("#loader").hide(); }

             function CheckValidity() {
                 if (document.getElementById("palte").length < 4) { }


             }

        </script>


    </body>
</html>
