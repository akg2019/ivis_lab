﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<% 
    int id = Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID;
    bool inter_cover_count = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("BI", "Cover Chart"));
    bool enquiry_chart = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("BI", "Enquiry Chart"));
    bool error_gauge = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("BI", "Error Gauge"));
    bool cover_note_gauge = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("BI", "Cover Note Gauge"));
    bool cert_gauge = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("BI", "Certificate Gauge"));
    bool dbl_ins_gauge = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("BI", "Double Insured Gauge"));
    bool enquiry_gauge = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("BI", "Inquiries Gauge"));
    bool is_iaj = Honeysuckle.Models.CompanyModel.is_company_iaj(id);
%>


<div class="container big">
    <div class="stripes"></div>
    <div class="form-header">
        <img src="../../Content/more_images/BIM_ICON.png" />
        <div class="form-text-links">
            <span class="form-header-text">BMI KPI</span>
        </div>
    </div>
    <div class="push-left">
        <% 
            if (inter_cover_count) { 
                Html.RenderAction("IntermediaryCoverCount", "BI", new { });  
            }
            if (enquiry_chart) {
                Html.RenderAction("CompanyEnquiryCount", "BI", new { });
            }
            if (enquiry_gauge) {
                Html.RenderAction("Gauge", "BI", new { id = id, datemode = "month", type = "enquiry", iaj = is_iaj });        
            }
            if (error_gauge) {
                Html.RenderAction("Gauge", "BI", new { id = id, datemode = "month", type = "error", iaj = is_iaj });
            }
            if (cover_note_gauge) {
                Html.RenderAction("Gauge", "BI", new { id = id, datemode = "month", type = "covernote", iaj = is_iaj });
            }
            if(cert_gauge) {
                Html.RenderAction("Gauge", "BI", new { id = id, datemode = "month", type = "certificate", iaj = is_iaj });
            }
            if (dbl_ins_gauge) {
                Html.RenderAction("Gauge", "BI", new { id = id, datemode = "month", type = "doubleInsured", iaj = is_iaj });
            }
        %>
    </div>
</div>
