﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Person>>" %>

   <div style="color:Red;">Record has duplicates. Check a row to use the corresponding record or edit it.</div>
   <br />
    <br />
<%: Html.Hidden("duplicatePartial", null,new{ id="duplicatePartial" }) %>
    <table>
        <tr>

            <th>
                Select
            </th>
            <th>
                First Name
            </th>
            <th>
                Middle 0Name
            </th>
            <th>
                Last Name
            </th>
            <th>
                Created
            </th>
            
        </tr>

    <% foreach (var item in Model) { %>
    
        <tr>
           <td>
                <%: Html.CheckBox("dupTRNselect", false, new { @class = "dupTRNcheck", id = item.PersonID.ToString(), @OnClick = "selectDuplicateVeh(this)" })%>
            </td>
            
            <td>
                <%: item.fname %>
            </td>
            <td>
                <%: item.mname %>
            </td>
            <td>
                <%: item.lname %>
            </td>
            <td>
                <%: item.CreatedOn %>
            </td>
            
        </tr>
    
    <% } %>

    </table>
    


