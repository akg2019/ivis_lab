﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Person>" %>

<div class="editor-label">
    First Name:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.fname) %>
</div>
            
<div class="editor-label">
    Middle Name:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.mname) %>
</div>
            
<div class="editor-label">
    Last Name:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.lname) %>
</div>
            
<div class="editor-label">
    TRN:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.TRN) %>
</div>

<% Html.RenderPartial("~/Views/Address/AuditAddress.ascx", Model.address); %>