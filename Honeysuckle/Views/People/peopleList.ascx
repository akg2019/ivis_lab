﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Person>>" %>

<div id="insured" class="insured-modal" style="height:300px!important">

        <div>
             <table style="width:100%!important; border-style:none!important;">
                    <tr>
                        <td>&nbsp;Search By <b><i style="color:Green;">Trn</i> </b><%:Html.RadioButton("search-type","Trn",true) %></td>
                        <td>&nbsp;Search By <b><i style="color:Green;">First Name</i>  </b> <%:Html.RadioButton("search-type","Fn") %></td>
                        <td>&nbsp;Search By <b><i style="color:Green;">Last Name</i> </b> <%:Html.RadioButton("search-type","ln") %></td>
                       
                        
                    </tr>
                   
               </table>
               <br />
               <div style ="height:2px;"></div>
               <%: Html.TextBox("search-modal", null, new { @placeholder = "Search People", @autocomplete = "off", @class = "modalSearch peopleSearch" })%> 

            <button type="button" class="quick-search-btn" onclick="searchPeople(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
            <span class="mess">&nbsp;&nbsp; Please tick the appropriate person(s):</span>
            <div style ="height:10px;"></div>
        </div>

    <div class="ivis-table modal-list">
        <div class="menu"> 
            <div class="select item">Select</div>
            <div class="trn item" >TRN</div>
            <div class="fname item" >First Name</div>
            <div class="fname item" >Last Name</div>
        </div>

        <div id="selected-insured" class="static selectedSection"></div>

        <div id="insureds" class="dynamic">
            <% if(Model.Count() == 0) { %>
                    <div class="rowNone">There Are No Insured. Please add one.</div>
            <% } %>
            <% else { %>
                <% if(Model.Count() > 50) { %>
                    <div class="rowNone">
                        Please search the system for existing insureds, or add a new insured. 
                    </div>
                <% } %>

                <% int i = 0; %>
                <% foreach (Honeysuckle.Models.Person item in Model) { %>
                    <% string TRN = "";
                       if (item.TRN == "10000000") TRN = "Not Available"; else TRN = item.TRN;
                        %>
                    <% if (i%2 == 0) { %> <div class="insured-row row even" > <%} %>
                    <% else { %> <div class="insured-row row odd" > <%} %>
                    <% i++; %>
                        <div class="insured-check select item" >
                            <%: Html.CheckBox("insuredcheck", false, new { @class = "insuredcheck", id = item.PersonID.ToString(), @OnClick="changeCheckBox(this)" })%>
                        </div>
                        <div class="insured-trn trn item" >
                            <%: TRN %>
                        </div>
                        <div class="insured-fname fname item" >
                            <%: item.fname %> 
                        </div>
                        <div class="insured-lname lname item" >
                            <%: item.lname %>
                        </div>
                     </div>
                  <% } %>
                <% } %>
            </div>
         </div>
     </div>
 </div>
 <div class="recordsFound ResultModal green"></div>
<div  style="position:absolute;bottom:0; padding:7px; float:right;">
   
    
    <input type="button" value="Cancel" class="cancelButton" onclick="closeDialog()" />
    <input type="button" value="Create New" id="create"  onclick="loadCreate()" />
    <input type="button" value="Add Selected" id="buttonAdd"  onclick="getValueUsingClass()" />
</div>