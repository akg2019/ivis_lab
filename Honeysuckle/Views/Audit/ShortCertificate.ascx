﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Audit>>" %>

<%: Html.Hidden("count", Model.Count(), new { @class = "dont-process" })%>
<% bool certificates_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Read")); %>

<div class="ivis-table">
        <div class="shortcert menu">
            <div class="action item">
                Action
            </div>
            <div class="uniqueID item">
                Certificate No
            </div>
            <div class="timestamp item">
                Date Time
            </div>
            <div class="ipaddress item">
                IP Address
            </div>
        </div>
        <div class="scroll-table">
            <% int i = 0; %>
            <% foreach (var item in Model)
               { %>
                    <% i++; %>
                    <% if(i%2 == 0)
                    { %>
                    <div class="shortAuditCert data even row certificate">
                    <% } %>
                    <% else
                        { %>
                    <div class="shortAuditCert data odd row certificate">
                    <% } %>

                    <%: Html.Hidden("AuditID", item.ID, new { @class = "id dont-process" })%>
                    <%: Html.Hidden("AuditRefID", item.reference_id, new { @class = "referenceID dont-process" })%>

                    <div class="auditrow">
                        <div class="action item"><%: item.action.ToUpper() %></div>
                        <div class="uniqueID item"><%: item.specifcRecordUniqueID %></div>
                        <div class="timestamp item"><%: String.Format("{0:dd MMM yyyy hh:mm:ss tt}", item.timestamp)%></div>
                        <div class="ipaddress item"><%: item.ip_address %></div>
                    </div>

                    <div class="auditrow functions item">
                    <% if (item.action == "update")
                       { %>
                        <div class="icon viewlog" title="View Changes">
                                <img src="../../Content/more_images/View_Changes.png" class="sprite"/>
                            </div>
                    <% } %>
                        <% if (certificates_read && item.action != "delete")
                           { %>
                            <div class="icon view" title="View Related Certificate">
                                <img src="../../Content/more_images/view_icon.png" class="sprite"/>
                            </div>
                        <% } %>
                    </div>

                    </div>
            <% } %>
        </div>
</div>