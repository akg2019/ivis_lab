﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Audit>>" %>

<div class="ivis-table">
    <div class="menu">
        <div class="timestamp item">Timestamp</div>
        <div class="type item">Type</div>
        <div class="person item">Name</div>
    </div>
    <div class="log">
        <% for(int i = 0; i < Model.Count(); i++){ %>
            <% if (i % 2 == 0) { %> <div class="audit even row"> <% } %>
            <% else { %> <div class="audit odd row"> <% } %>
                <%: Html.Hidden("auditid", Model.ElementAt(i).ID, new { @class = "id dont-process" })%>
                <div class="timestamp item"><%: Model.ElementAt(i).timestamp.ToString("dd/MM/yyyy hh:mm tt") %></div>
                <div class="type item"><%: Model.ElementAt(i).action %></div>
                <div class="person item"><%: Model.ElementAt(i).person.lname %>, <%: Model.ElementAt(i).person.fname %></div>
            </div>

        <% } %>
    </div>
</div>