﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using System.Web.Routing;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

namespace Honeysuckle.Controllers
{
    public class SearchController : Controller
    {
        //
        // GET: /Search/


        public ActionResult AdvancedSearch(string search = null, string andor = null, string type = null, string Chassiss = null, string Reg = null, string Company = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Search", "Search")))
            {
                List<Search> results;
                if (search.Trim().Length > 0) results = SearchModel.GeneralSearchNoLimit(search.Trim(), Chassiss, Reg, Company);
                else results = results = SearchModel.GeneralSearchNoLimit(search.Trim(), Chassiss, Reg, Company);

                if (type != null && type != "all") results = SearchModel.filterResults(results, type);

                if (andor != null) Constants.searchresults = SearchModel.mergeResults(Constants.searchresults, results, andor);
                else Constants.searchresults = results;

                if (search != null) return View("SearchResults", Constants.searchresults);
                return new EmptyResult();
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShortPeople()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read"))) return View(SearchModel.GetSearch(Constants.searchresults, "people").Select(x => x.person).ToList());
            else return new EmptyResult();
        }

        public ActionResult ShortPolicies()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Read"))) return View(SearchModel.GetSearch(Constants.searchresults, "policy").Select(x => x.policy).ToList());
            else return new EmptyResult();
        }

        public ActionResult ShortCompany()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read"))) return View(SearchModel.GetSearch(Constants.searchresults, "company").Select(x => x.company).ToList());
            else return new EmptyResult();
        }

        public ActionResult ShortCoverNote()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Read"))) return View(SearchModel.GetSearch(Constants.searchresults, "covernote").Select(x => x.covernote).ToList());
            else return new EmptyResult();
        }

        public ActionResult ShortCertificate()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Read"))) return View(SearchModel.GetSearch(Constants.searchresults, "certificate").Select(x => x.certificate).ToList());
            else return new EmptyResult();
        }

        public ActionResult ShortVehicle()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read"))) return View(SearchModel.GetSearch(Constants.searchresults, "vehicle").Select(x => x.vehicle).ToList());
            else return new EmptyResult();
        }

        public ActionResult ShortUser()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Read")) || UserGroupModel.AmAnIAJAdministrator(Constants.user)) return View(SearchModel.GetSearch(Constants.searchresults, "user").Select(x => x.user).ToList());
            else return new EmptyResult();
        }

        public ActionResult Advanced(string type,string search = null)
        {
            Constants.user.UserGroups = UserModel.GetGroupsAndPermissions(Constants.user.ID);
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Search", "Search")))
            {
                List<Search> results;
                var companyId = Constants.user.CompanyID.ToString();
                if(type.Equals("policy"))
                    results = SearchModel.GeneralSearchNoLimit(search.Trim(), "", "", companyId, policysearch: true);
                else if (search.Trim().Length > 0) results = SearchModel.GeneralSearchNoLimit(search.Trim(), "", search.Trim(), companyId);
                else results = results = SearchModel.GeneralSearchNoLimit(search.Trim(), "", "", "");

                //if (type != null && type != "all") results = SearchModel.filterResults(results, type);

                //if (andor != null) Constants.searchresults = SearchModel.mergeResults(Constants.searchresults, results, andor);

                Constants.searchresults = results;

                if (string.IsNullOrEmpty(search) || !results.Any())
                    return View("Advanced", new List<Search>());
                else
                    return View("Advanced", Constants.searchresults);
            }
            else return RedirectToAction("AccessDenied", "Error", new { });
        }

        public ActionResult AdvancedSearchBar(string search = null)
        {
            if (search != null) ViewData["search"] = search;
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Search", "Search"))) return View();
            else return new EmptyResult();
        }

        public ActionResult AddField()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Search", "Search"))) return View();
            else return new EmptyResult();
        }

        /// <summary>
        /// render partial that shows the search bar
        /// </summary>
        public ActionResult Search()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Search", "Search"))) return View();
            else return new EmptyResult();
        }

        /// <summary>
        /// render general search results for autocomplete
        /// </summary>
        public ActionResult GeneralSearch(string id = null, string type = null)
        {
            if (id != null)
            {
                List<Search> results = SearchModel.GeneralSearch(id, type);
                var json = results.Select(r => new { label = SearchModel.GenerateName(r), value = SearchModel.GenerateLink(r) });
                return Json(json, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { }, JsonRequestBehavior.AllowGet);
        }

    }
}
