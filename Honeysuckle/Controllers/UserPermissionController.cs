﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class UserPermissionController : Controller
    {

        /// <summary>
        /// This function is used to retrieve and return user permissions, for the particular user group in question
        /// </summary>
        /// <param name="id">usergroup id</param>
        public ActionResult MyListBoxUserPermissions(int id = 0, string search = null, bool system = false)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups", "Read")))
            {
                List<UserPermission> userpermissions = new List<UserPermission>();
                if (id != 0) userpermissions = UserPermissionModel.GetAllPermissionsForGroup(new UserGroup(id), system);
                else userpermissions = UserPermissionModel.GetRestOfUserPermissions(UserPermissionModel.GetNotPermissionsForGroup(new UserGroup(id)), system);
                if (search != null) userpermissions = userpermissions.Where(x => x.actiontable.ToLower().Contains(search.ToLower())).ToList();
                return View(userpermissions);
            }
            return RedirectToAction("Index");
        }


        /// <summary>
        /// This function is used to retrieve user permissions
        /// </summary>
        /// <param name="id">usergroup id</param>
        public ActionResult ListBoxUserPermissions(int id = 0, string search = null, bool system = false)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups", "Read")))
            {
                List<UserPermission> userpermissions = new List<UserPermission>();
                if (id != 0) userpermissions = UserPermissionModel.GetNotPermissionsForGroup(new UserGroup(id), system);
                else userpermissions = UserPermissionModel.GetAllUserPermissions();                
                if (search != null) userpermissions = userpermissions.Where(x => x.actiontable.ToLower().Contains(search.ToLower())).ToList();
                return View(userpermissions);
            }
            return RedirectToAction("Index");
        }

    }
}
