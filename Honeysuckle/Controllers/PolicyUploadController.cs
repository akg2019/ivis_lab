﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Honeysuckle.Models;
using OmaxFramework;

namespace Honeysuckle.Controllers
{
    public class PolicyUploadController : Controller
    {
        //
        // GET: /PolicyUpload/

        public ActionResult Upload()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Import", "Import")))
                return View();
            else
            {
                TempData["FileUpload"] = "No permission to do this action";
                return RedirectToAction("Index", "Home");
            }
        }

        //[HttpPost]
        //public ActionResult Upload(List<HttpPostedFileBase> files = null, int kid = 0, string key = null)
        //{
        //    if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Import", "Import")))
        //    {
        //        if (files == null || files[0] == null)
        //        {
        //            TempData["FileUpload"] = "Error. You did not upload a file.";
        //            return RedirectToAction("Index", "Home");
        //        }

        //        List<FileControl> fcs = FileControl.storeFiles(files, kid, key);
        //        TempData["FileUpload"] = "";
        //        foreach(FileControl fc in fcs)
        //        {
        //            TempData["FileUpload"] += fc.fileName + fc.status + Environment.NewLine + Environment.NewLine;
        //        }
        //        return RedirectToAction("Index", "Home");
        //    }
        //    else
        //    {
        //        TempData["FileUpload"] = "You do not have permission to execute this action";
        //        return RedirectToAction("Index", "Home");
        //    }
        //}


        [HttpPost]
        public ActionResult Upload(List<HttpPostedFileBase> files = null, int kid = 0, string key = null)
        {
            //HttpPostedFileBase file = null;
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Import", "Import")))
            {


                //string jsonData;


                JSON json = new JSON();
                JSON js = new JSON();
                Import import = new Import();


                //OmaxFramework.Utilities omax = new OmaxFramework.Utilities();
                if (files == null || files[0] == null)
                {
                    TempData["FileUpload"] = "Error. You did not upload a file.";
                    return RedirectToAction("Index", "Home");
                }

                import.processFiles(files, json, kid, key);


                TempData["FileUpload"] = "You will be notified of the outcome of the files uploaded";
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["FileUpload"] = "You do not have permission to execute this action";
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult CheckUpload()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Import", "Import")))
                return View(JSONModel.CheckJsonProgress(Constants.user.ID));
            else
            {
                TempData["FileUpload"] = "No permission to do this action";
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult SearchUpload(int id)
        {

            JSON js = JSONModel.CheckJsonProgress(Constants.user.ID, id);
            if (js != null && js.ID != 0)
                return Json(new { haveData = 1, js = js, createdTime = (js.createdTime != null) ? js.createdTime.ToString() : "", completedTime = (js.completedTime != null) ? js.completedTime.ToString() : "" }, JsonRequestBehavior.DenyGet);
            else
                return Json(new { haveData = 0 }, JsonRequestBehavior.DenyGet);


        }

    }
}
