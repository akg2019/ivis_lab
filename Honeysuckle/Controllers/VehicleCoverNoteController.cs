﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PdfSharp.Pdf;
using Honeysuckle.Models;
using System.Web.Script.Serialization;
using System.Globalization;

namespace Honeysuckle.Controllers
{
    public class VehicleCoverNoteController : Controller
    {
        //
        // GET: /VehicleCoverNote/

        public ActionResult ViewPDF(byte[] pdf)
        {
            byte[] data = System.IO.File.ReadAllBytes(System.IO.Directory.GetCurrentDirectory() +  "../pdf/file.pdf");
            return File(pdf, "file.pdf", "application/pdf");
        }

        public ActionResult Edit(int id = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Update")))
            {
                VehicleCoverNote covernote = VehicleCoverNoteModel.GetVehicleCoverNoteByNumber(id);
                if (covernote != null)
                {
                    if ((PolicyModel.CanIViewPolicy(covernote.Policy.ID) || UserGroupModel.AmAnAdministrator(Constants.user)))
                    {
                        if (false) return RedirectToAction("Details", "VehicleCoverNote", new { id = id });
                        else return View(covernote);
                    }
                    else return RedirectToAction("AccessDenied", "Error");
                }
                else return RedirectToAction("AccessDenied", "Error");
            }
            else return RedirectToAction("AccessDenied", "Error");
        }

        [HttpPost]
        public ActionResult Edit(VehicleCoverNote vcn, int wording = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Update")) && (PolicyModel.CanIViewPolicy(vcn.Policy.ID) || UserGroupModel.AmAnAdministrator(Constants.user)))
            {
                vcn.wording = new TemplateWording(wording);

                if (!vcn.approved)
                {
                    VehicleCoverNoteModel.UpdateVehicleCoverNote(vcn);
                    //TempData["EmailSent"] = "Vehicle covernote updated";
                    //return RedirectToAction("Index", "Home");
                    return RedirectToAction("Details", new { id = vcn.ID });
                }
                else
                {
                    ViewData["approved"] = "b";
                    return View(vcn);
                }
            }
            else return RedirectToAction("AccessDenied", "Error");
        }


        public ActionResult GenerateCoverNoteNo(int id = 0)
        {
            if (id != 0)
            {
                string covernoteId = VehicleCoverNoteModel.GenerateCoverNoteNo(new Company(id));
                return Json(new { result = 1, covernoteID = covernoteId }, JsonRequestBehavior.AllowGet);
            }
            
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Create(int id = 0, int polid = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Create")) && VehicleCoverNoteModel.CanIGenerateCoverNote(new Vehicle(id), new Policy(polid))) {
                if (id == 0) {
                    ViewData["vehId"] = id;
                    ViewData["CleanCreate"] = true;
                    return View();
                }
                else {
                    //if (VehicleModel.IsVehicleUnderPolicy(id, polid, DateTime.Now, DateTime.Now)) {
                        ViewData["vehid"] = id;
                        ViewData["CleanCreate"] = false;
                        ViewData["type"] = "CertCreate";

                        VehicleCoverNote vcn = new VehicleCoverNote();
                        if (polid != 0)
                        {
                            vcn.Policy = PolicyModel.getPolicyFromID(polid);
                        }
                        else vcn.Policy = VehicleCertificateModel.PullVehiclePolData(id).policy;

                        vcn.risk = new Vehicle(id);

                        if (vcn.Policy != null && vcn.Policy.ID != 0)
                        {
                            vcn.risk.usage = new Usage(VehicleModel.GetUsage(vcn.risk.ID, vcn.Policy.ID));
                            vcn.mortgagee = VehicleModel.getMortgagee(vcn.risk.ID, vcn.Policy.ID);
                        }

                        if (Constants.user.newRoleMode) vcn.covernoteid = VehicleCoverNoteModel.GenerateCoverNoteNo(Constants.user.tempUserGroup.company);
                        else vcn.covernoteid = VehicleCoverNoteModel.GenerateCoverNoteNo(CompanyModel.GetMyCompany(Constants.user));
                            
                        return View(vcn);
                    //}
                    //else return RedirectToAction("AccessDenied", "Error");
                }
            }
            else return RedirectToAction("AccessDenied", "Error");
        }


        [HttpPost]
        public ActionResult Create(VehicleCoverNote covernote, int wording = 0, IEnumerable<Vehicle> vehicles = null, string proceed="")
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Create")))
            {
                ViewData["effsys"] = covernote.effectivedate;

                ModelState.Remove("wording.ID");
                ModelState.Remove("Policy.insuredby.CompanyID");
                ModelState.Remove("wording");

                ViewData["CleanCreate"] = true;
                covernote.Policy = PolicyModel.getPolicyFromID(covernote.Policy.ID);
                covernote.wording = new TemplateWording(wording);

                if (vehicles != null) covernote.risk = vehicles.ToArray()[0];
                else
                {
                    ViewData["veherror"] = "A risk must be added to create a cover note!";
                    return View(covernote);
                }

                if (wording == 0)
                {
                    ViewData["veherror"] = "A wording template must be selected!";
                    ViewData["type"] = "CertCreateClean";
                    return View(covernote);
                }
                else covernote.wording = new TemplateWording(wording); 

                if(covernote.period == 0)
                {
                    ViewData["veherror"] = "Please select a period for this covernote!";
                    ViewData["type"] = "CertCreateClean";
                    return View(covernote);
                }

                if (!CompanyModel.IsCompanyShortSet(new Company (covernote.Policy.insuredby.CompanyID)))
                {
                    ViewData["veherror"] = "The insurance company, for the policy that has this vehicle, does not have a company Shortening code. Please contact your admin for assistance!";
                    ViewData["type"] = "CertCreateClean";
                    return View(covernote);
                }


                if (ModelState.IsValid) {
                    //if (VehicleCoverNoteModel.TestCoverNotePossible(covernote)) {
                   if( PolicyModel.IsVehicleCovered(covernote.risk.ID, covernote.effectivedate, covernote.expirydate))
                    {

                        if (proceed == "" && VehicleCoverNoteModel.IsCoverNoteExistNow(covernote.risk.ID, covernote.effectivedate, covernote.expirydate))
                        {
                            ViewData["NoteExistsAlert"] = "The seleced risk already has an active cover note during this time period. Would you like to proceed?";
                            return View(covernote);
                        }

                        if (covernote.covernoteno == null || VehicleCoverNoteModel.TestManualCoverNoteNo(covernote)) {
                            int id = 0; // VehicleCoverNoteModel.AddVehicleCoverNote(covernote, covernote.Policy.ID, covernote.Policy.insuredby.CompanyID, true);

                            string NamesOnPolicy = "";
                            string DriverNames = "";
                            string AuthDrivers = "";
                            string ExcludedDrivers = "";
                            string ExceptedDrivers = "";
                            string PolicyHoldersAddress = "";

                            
                           

                            //Gets polcy holders address
                            try
                            {
                                if (covernote.Policy.mailingAddress != null) {
                                    if (covernote.Policy.mailingAddress.city != null) {
                                        PolicyHoldersAddress += (covernote.Policy.mailingAddress.ApartmentNumber + " " + covernote.Policy.mailingAddress.city.CityName);
                                    }

                                    if (covernote.Policy.mailingAddress.parish != null) {
                                        PolicyHoldersAddress += (" " + covernote.Policy.mailingAddress.parish.parish);
                                    }

                                    if (covernote.Policy.mailingAddress.country != null)
                                    {
                                        PolicyHoldersAddress += (" " + covernote.Policy.mailingAddress.country.CountryName);
                                    }
                                
                                }
                            }
                            catch (Exception)
                            {
                               
                            }

                            

                            try { DriverNames = covernote.risk.mainDriver.person.fname + " " + covernote.risk.mainDriver.person.mname + " " + covernote.risk.mainDriver.person.lname + " and "; }
                            catch (Exception) { }
                            string PolicyHolderNames = "";

                            string CompanyNames = "";
                            try { foreach (var N in covernote.Policy.company) { CompanyNames += N.CompanyName + " and "; } }
                            catch (Exception) { }
                            try { CompanyNames = CompanyNames.Substring(0, CompanyNames.Length - 4); }
                            catch (Exception) { }
                            PolicyHolderNames += CompanyNames;

                            try { foreach (var N in covernote.Policy.insured) { if (!PolicyHolderNames.Contains((N.fname + " " + N.mname + " " + N.lname))) { PolicyHolderNames += N.fname + " " + N.mname + " " + N.lname + " and "; } } }
                            catch (Exception) { }
                            try { foreach (var N in covernote.Policy.vehicles) { foreach (var d in N.AuthorizedDrivers) { if (!PolicyHolderNames.Contains((d.person.fname + " " + d.person.mname + " " + d.person.lname))) { PolicyHolderNames += d.person.fname + " " + d.person.mname + " " + d.person.lname + " and "; } } } }
                            catch (Exception) { }

                            try { foreach (var N in covernote.Policy.vehicles) { foreach (var d in N.AuthorizedDrivers) { if (!AuthDrivers.Contains((d.person.fname + " " + d.person.mname + " " + d.person.lname))) { AuthDrivers += d.person.fname + " " + d.person.mname + " " + d.person.lname + " and "; } } } }
                            catch (Exception) { }

                            try { DriverNames = covernote.risk.mainDriver.person.fname + " " + covernote.risk.mainDriver.person.mname + " " + covernote.risk.mainDriver.person.lname; }
                            catch (Exception) { }

                            try { foreach (var N in covernote.risk.ExceptedDrivers) { if (!ExceptedDrivers.Contains((N.person.fname + " " + N.person.mname + " " + N.person.lname))) { ExceptedDrivers += N.person.fname + " " + N.person.mname + " " + N.person.lname + " and "; } } }
                            catch (Exception) { }
                            try { foreach (var N in covernote.risk.ExcludedDrivers) { if (!ExcludedDrivers.Contains((N.person.fname + " " + N.person.mname + " " + N.person.lname))) { ExcludedDrivers += N.person.fname + " " + N.person.mname + " " + N.person.lname + " and "; } } }
                            catch (Exception) { }

                            try { if (PolicyHolderNames.Substring(PolicyHolderNames.Length - 4) == "and ") { PolicyHolderNames = PolicyHolderNames.Substring(0, PolicyHolderNames.Length - 4); } }
                            catch (Exception) { }

                            try { if (DriverNames.Substring(DriverNames.Length - 4) == "and ") { DriverNames = DriverNames.Substring(0, DriverNames.Length - 4); } }
                            catch (Exception) { }
                            try { if (AuthDrivers.Substring(AuthDrivers.Length - 4) == "and ") { AuthDrivers = AuthDrivers.Substring(0, AuthDrivers.Length - 4); } }
                            catch (Exception) { }
                            try { if (ExcludedDrivers.Substring(ExcludedDrivers.Length - 4) == "and ") { ExcludedDrivers = ExcludedDrivers.Substring(0, ExcludedDrivers.Length - 4); } }
                            catch (Exception) { }
                            try { if (ExceptedDrivers.Substring(ExceptedDrivers.Length - 4) == "and ") { ExceptedDrivers = ExceptedDrivers.Substring(0, ExceptedDrivers.Length - 4); } }
                            catch (Exception) { }
                            try { DriverNames = DriverNames.Substring(0, DriverNames.Length - 4); }
                            catch (Exception) { }

                            string MainInsured = ""; try { var mainInsured = covernote.Policy.insured.OrderBy(p => p.PersonID).Take(1).FirstOrDefault(); MainInsured = mainInsured.fname + " " + mainInsured.lname; }
                            catch (Exception) { }

                            try { if (string.IsNullOrEmpty(MainInsured) && !string.IsNullOrEmpty(CompanyNames.Trim())) MainInsured = CompanyNames; }catch(Exception){}

                            //Get Wording/Cert Template
                            TemplateWording Word = new TemplateWording();
                            try { Word = TemplateWordingModel.GetWording(covernote.wording.ID); }catch(Exception){}
                            
                            //Adds Generated CoverNote record here
                            VehicleCoverNoteGenerated vcng = new VehicleCoverNoteGenerated();
                            vcng.Approved = true;
                            
                            try {vcng.authorizedwording = Word.AuthorizedDrivers; }catch(Exception){}
                            try {vcng.authorizedwording = Word.AuthorizedDrivers;  }catch(Exception){}
                            try {vcng.bodyType= covernote.risk.bodyType; }catch(Exception){}
                            try {vcng.cancelled = covernote.cancelled; }catch(Exception){}
                            try {vcng.CancelledStatus = covernote.cancelled; }catch(Exception){}
                            try {vcng.certificateCode = Word.CertificateInsuredCode + ":" + Word.ExtensionCode; }catch(Exception){}
                            try {vcng.certificateType = Word.CertificateType; }catch(Exception){}
                            try {vcng.chassisNo = covernote.risk.chassisno; }catch(Exception){}
                            //vcng.certitificateNo = covernote.;
                            try {vcng.companyCreatedBy = Constants.user.employee.company.CompanyName; }catch(Exception){}
                            try {vcng.CompanyName = covernote.Policy.insuredby.CompanyName; }catch(Exception){}
                            try {vcng.cover = covernote.Policy.policyCover.cover;}catch(Exception){}
                            try {vcng.CovernoteNo = covernote.covernoteno; }catch(Exception){}
                            try {vcng.drivers= DriverNames; }catch(Exception){}
                            try {vcng.effectivedate = covernote.effectivedate.ToString(); }catch(Exception){}
                            try {vcng.effectiveTime = covernote.effectivedate.ToShortTimeString(); }catch(Exception){}
                            try {vcng.endorsementNo= covernote.endorsementno;}catch(Exception){}
                            try {vcng.engineNo = covernote.endorsementno; }catch(Exception){}
                            try {vcng.estimatedValue = covernote.risk.estimatedValue; }catch(Exception){}
                            try {vcng.expirydate = covernote.expirydate.ToString(); }catch(Exception){}
                            try {vcng.expiryTime = covernote.expirydate.ToShortTimeString(); }catch(Exception){}
                            try {vcng.HPCC = covernote.risk.HPCCUnitType; }catch(Exception){}
                            try {vcng.limitsofuse = Word.LimitsOfUse; }catch(Exception){}
                            try {vcng.Period = covernote.period.ToString(); }catch(Exception){}
                            try {vcng.PolicyHolders = PolicyHolderNames; }catch(Exception){}
                            try {vcng.PolicyHoldersAddress = covernote.Policy.mailingAddressShort; }catch(Exception){}
                            try {vcng.PolicyID = covernote.Policy.ID; }catch(Exception){}
                            try {vcng.PolicyNo = covernote.Policy.policyNumber; }catch(Exception){}
                            try {vcng.PolicyTypes = covernote.Policy.insuredType; }catch(Exception){}
                            try {vcng.referenceNo = covernote.risk.referenceNo; }catch(Exception){}
                            try {vcng.seating = covernote.risk.seating.ToString(); }catch(Exception){}
                            try {vcng.Usage = covernote.risk.usage.usage; }catch(Exception){}
                            try { vcng.vehdesc = covernote.Policy.vehicles[0].VehicleYear + " " + covernote.Policy.vehicles[0].make + " " + covernote.Policy.vehicles[0].VehicleModel + " " + covernote.Policy.vehicles[0].extension; }
                            catch (Exception) { }
                            try { vcng.WordID = covernote.wording.ID;}catch(Exception){}                         
                            try {vcng.vehExt = covernote.Policy.vehicles[0].extension; }catch(Exception){}
                            try {vcng.VehicleMetaDataID = covernote.risk.ID; }catch(Exception){}
                            try {vcng.VehicleID = covernote.Policy.vehicles[0].ID; }catch(Exception){}
                            try {vcng.vehmake = covernote.Policy.vehicles[0].make; }catch(Exception){}
                            try {vcng.vehmodel = covernote.Policy.vehicles[0].VehicleModel; }catch(Exception){}
                            try {vcng.vehmodeltype = covernote.Policy.vehicles[0].modelType; }catch(Exception){}
                            try {vcng.vehregno = covernote.Policy.vehicles[0].VehicleRegNumber; }catch(Exception){}
                            try {vcng.vehyear = covernote.Policy.vehicles[0].VehicleYear.ToString(); }catch(Exception){}
                            try {vcng.VIN = covernote.Policy.vehicles[0].VIN; }catch(Exception){} 
                            try {vcng.ManualCoverNoteNumber = covernote.printedpaperno; }catch(Exception){}
                            try {vcng.PrintCount = covernote.printcount; }catch(Exception){}
                            try {vcng.LastPrintedBy = covernote.lastprintflat; }catch(Exception){}
                            try {vcng.DateLastPrinted = DateTime.Now; }catch(Exception){}
                            try {vcng.DateFirstPrinted = DateTime.Now; }catch(Exception){}
                            try {vcng.CompanyID = covernote.Policy.insuredby.CompanyID; }catch(Exception){}
                            try {vcng.IntermediaryID = Constants.user.employee.company.CompanyID; }catch(Exception){}
                            try { vcng.limitsofuse = TemplateWordingModel.ReplaceBraceTags(vcng.limitsofuse,DriverNames,AuthDrivers,ExcludedDrivers,ExceptedDrivers,PolicyHolderNames, MainInsured); }catch (Exception) { }
                            try { vcng.authorizedwording = TemplateWordingModel.ReplaceBraceTags(vcng.authorizedwording,DriverNames,AuthDrivers,ExcludedDrivers,ExceptedDrivers,PolicyHolderNames, MainInsured); }catch (Exception) { }
                            try { vcng.authorizedwording = TemplateWordingModel.ReplaceBraceTags(vcng.authorizedwording,DriverNames,AuthDrivers,ExcludedDrivers,ExceptedDrivers,PolicyHolderNames, MainInsured); }catch (Exception) { }
                            try {vcng.PolicyHoldersAddress = PolicyHoldersAddress; }catch(Exception){}
                            //VehicleCoverNoteGeneratedModel.InsertGenCoverNote(vcng, 0);

                            try
                            {
                                if (string.IsNullOrEmpty(vcng.CovernoteNo))
                                {
                                  vcng.CovernoteNo = DateTime.Now.ToString("ddMMyyhhmmss");
                                }

                                if (string.IsNullOrEmpty(vcng.ManualCoverNoteNumber))
                                {
                                  vcng.ManualCoverNoteNumber = vcng.CovernoteNo;
                                }
                            }
                            catch (Exception)
                            {

                            }


                            id = VehicleCoverNoteGeneratedModel.InsertGenCoverNoteGenerated(vcng, 0,Constants.user.ID);
                                
                           
                            //Removing Approval of Cover Process Indicated by Mr. Donaldson

                            //bool canAutoApprove = UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "AutoApprove"));
                            //if (canAutoApprove)
                            //    Honeysuckle.Models.VehicleCoverNoteModel.ApproveCoverNote(new VehicleCoverNote(id));
                    
                            return RedirectToAction("Details", new { id = id });
                        }
                        else
                        {
                            ViewData["vehid"] = vehicles;
                            ModelState.AddModelError("covernoteno", "This Manual Cover Note No already exists in the system. Please enter one that is not presently in the system");
                            return View(covernote);
                        }
                    }
                    else
                    {
                        //break and go back to view
                        //ModelState.AddModelError("vehicles","This Risk either already has an active cover note during the time period selected or will no longer be covered by the current policy. Please select a more appropriate Risk");
                        ViewData["veherror"] = "Please ensure that the vehicle is covered under the period you have selected"; //mess with view for modals
                        ViewData["vehid"] = vehicles;
                        return View(covernote);
                    }
                }
                else return View(covernote);
            }
            else return RedirectToAction("AccessDenied", "Error");
        }

        public ActionResult Details(int id = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Read")))
            {
                VehicleCoverNote covernote = VehicleCoverNoteModel.GetVehicleCoverNoteByNumber(id);

                if (covernote.Policy != null)
                {
                    if (PolicyModel.CanIViewPolicy(covernote.Policy.ID) || UserGroupModel.AmAnAdministrator(Constants.user))
                    {
                        covernote.ID = id;
                        return View(covernote);
                    }
                    else
                    {
                        TempData["permissionMessage"] = "You cannot view this record.";
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    TempData["permissionMessage"] = "This record does not exist";
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {

                TempData["permissionMessage"] = "You do not have any permission to access this page";
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Approve(int id = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Approve")))
            {
                //VehicleCoverNoteModel.ApproveCoverNote(new VehicleCoverNote(id));
                //return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
            //else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Cancel(int id = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Approve")))
            {
                VehicleCoverNoteModel.CancelCoverNote(new VehicleCoverNote(id));
                return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult VehicleCoverNotes(int id = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Read"))) return View(VehicleCoverNoteModel.GetVehiclesCoverNotesHistory(new Vehicle(id)));
            else return new EmptyResult();
        }

        //public ActionResult VehicleCoverNoteGenSearch(string id = null)
        //{
        //    if (id != null && UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Read"))) return View(VehicleCoverNoteModel.CoverNoteGenSearch(Server.UrlDecode(id)));
        //    else return new EmptyResult();
        //}

        /// <summary>
        /// This function is used to pull all emails of the company admins of a particular company (which has a particular Risk cover note of a Risk)
        /// </summary>
        /// <param name="id">Risk Covernote Id</param>
        public ActionResult CompanyAdmins(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Read")))
            {
              List<string> Admins = new List<string>();
              Admins = PolicyModel.getAdmins(id, "CoverNote");
              return Json(new { result = 1, admins = Admins }, JsonRequestBehavior.AllowGet);
            }

           else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        //pull all emails of company brokers

        public ActionResult CompanyBrokers(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Read")))
            {
                List<List<string>> Admins = new List<List<string>>();
                Admins = PolicyModel.getBrokers(id, "CoverNote");
                return Json(new { result = 1, admins = Admins }, JsonRequestBehavior.AllowGet);
            }

            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This function is used to create a pdf for a vehicle cover note -and update a vehicle cover note's print information 
        /// </summary>
        /// <param name="id">Vehicle Certificate ID</param>
        public ActionResult PrintCoverNote(string coverNotes = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Print")) && coverNotes != "")
            {
               
                List<VehicleCoverNoteGenerated> generatedNoteList = new List<VehicleCoverNoteGenerated>();
                List<VehicleCoverNote> VehCoveNotes = new List<VehicleCoverNote>();
                TemplateWording Wording = new TemplateWording();

                // THIS PART IS ADDED FOR WHEN THERE ARE MORE THAN ONE CERTS BEING SENT TO BE PRINTED (MASS PRINT)
                int i = 0;
                foreach (string s in coverNotes.Split(',').ToList()) 
                {
                    if (int.TryParse(s, out i))
                    {
                        //Gets CoverNote to be Printed

                        if (i > 0) {
                            VehCoveNotes.Add(VehicleCoverNoteModel.GetVehicleCoverNoteByNumber(i));
                        }
                       
                    }
                }

                foreach (VehicleCoverNote VehCoveNote in VehCoveNotes)
                {
                   
                    VehicleCoverNoteGenerated VehCovGenerated = new VehicleCoverNoteGenerated();
                    //Pulling the image locations for the images to be added to the page
                    List<TemplateImage> images = TemplateImageModel.GetCompanyTemplateImages(VehCoveNote.company);

                    if (VehicleCoverNoteGeneratedModel.IsGenerated(VehCoveNote.ID, "CoverNote"))
                    {
                        VehCovGenerated = VehicleCoverNoteGeneratedModel.GetGenCoverNote(VehCoveNote.ID);

                        //try { VehCoveNote.risk = VehicleModel.GetVehicle(VehCoveNote.risk.ID); }catch (Exception) { }
                        

                        //Get Relevant information for to Generate Certificates
                        string NamesOnPolicy = "";
                        string DriverNames = "";
                        string AuthDrivers = "";
                        string ExcludedDrivers = "";
                        string ExceptedDrivers = "";
                        string PolicyHoldersAddress = "";
                        //Gets polcy holders address
                        try
                        {
                            if (VehCoveNote.Policy.mailingAddress != null)
                            {
                                if (VehCoveNote.Policy.mailingAddress != null)
                                {
                                    PolicyHoldersAddress += (VehCoveNote.Policy.mailingAddress.ApartmentNumber + ", " + (!string.IsNullOrEmpty(VehCoveNote.Policy.mailingAddress.roadnumber) ? VehCoveNote.Policy.mailingAddress.roadnumber : "") + " " + (VehCoveNote.Policy.mailingAddress.road.RoadName != null ? VehCoveNote.Policy.mailingAddress.road.RoadName : "") + " " + (!string.IsNullOrEmpty(VehCoveNote.Policy.mailingAddress.roadtype.RoadTypeName) ? VehCoveNote.Policy.mailingAddress.roadtype.RoadTypeName + "," : ",") + " " + (VehCoveNote.Policy.mailingAddress.city.CityName != null ? VehCoveNote.Policy.mailingAddress.city.CityName + "," : ","));
                                }

                                if (VehCoveNote.Policy.mailingAddress.parish != null)
                                {
                                    PolicyHoldersAddress += (" " + VehCoveNote.Policy.mailingAddress.parish.parish);
                                }

                                if (VehCoveNote.Policy.mailingAddress.country != null)
                                {
                                    PolicyHoldersAddress += (" " + VehCoveNote.Policy.mailingAddress.country.CountryName);
                                }

                            }
                        }
                        catch (Exception)
                        {

                        }

                        try { DriverNames = VehCoveNote.risk.mainDriver.person.fname + " " + VehCoveNote.risk.mainDriver.person.mname + " " + VehCoveNote.risk.mainDriver.person.lname + " and "; }
                        catch (Exception) { }
                        string PolicyHolderNames = "";

                        string CompanyNames = "";
                        try { foreach (var N in VehCoveNote.Policy.company) { CompanyNames += N.CompanyName + " and "; } }
                        catch (Exception) { }
                        try { CompanyNames = CompanyNames.Substring(0, CompanyNames.Length - 4); }
                        catch (Exception) { }
                        PolicyHolderNames += CompanyNames;

                        try { foreach (var N in VehCoveNote.Policy.insured) { if (!PolicyHolderNames.Contains((N.fname + " " + N.mname + " " + N.lname))) { PolicyHolderNames += N.fname + " " + N.mname + " " + N.lname + " and "; } } }
                        catch (Exception) { }
                        //try { foreach (var N in VehCoveNote.Policy.vehicles) { foreach (var d in N.AuthorizedDrivers) { if (!PolicyHolderNames.Contains((d.person.fname + " " + d.person.mname + " " + d.person.lname))) { PolicyHolderNames += d.person.fname + " " + d.person.mname + " " + d.person.lname + " and "; } } } }
                        //catch (Exception) { }

                        AuthDrivers += PolicyHolderNames;

                        try { foreach (var N in VehCoveNote.Policy.vehicles) { foreach (var d in N.AuthorizedDrivers) { if (!AuthDrivers.Contains((d.person.fname + " " + d.person.mname + " " + d.person.lname))) { AuthDrivers += d.person.fname + " " + d.person.mname + " " + d.person.lname + " and "; } } } }
                        catch (Exception) { }

                        try { DriverNames = VehCoveNote.risk.mainDriver.person.fname + " " + VehCoveNote.risk.mainDriver.person.mname + " " + VehCoveNote.risk.mainDriver.person.lname; }
                        catch (Exception) { }

                        try { foreach (var N in VehCoveNote.risk.ExceptedDrivers) { if (!ExceptedDrivers.Contains((N.person.fname + " " + N.person.mname + " " + N.person.lname))) { ExceptedDrivers += N.person.fname + " " + N.person.mname + " " + N.person.lname + " and "; } } }
                        catch (Exception) { }
                        try { foreach (var N in VehCoveNote.Policy.vehicles[0].ExcludedDrivers) { if (!ExcludedDrivers.Contains((N.person.fname + " " + N.person.mname + " " + N.person.lname))) { ExcludedDrivers += N.person.fname + " " + N.person.mname + " " + N.person.lname + " and "; } } }
                        catch (Exception) { }

                        try { if (PolicyHolderNames.Substring(PolicyHolderNames.Length - 4) == "and ") { PolicyHolderNames = PolicyHolderNames.Substring(0, PolicyHolderNames.Length - 4); } }
                        catch (Exception) { }

                        try { if (DriverNames.Substring(DriverNames.Length - 4) == "and ") { DriverNames = DriverNames.Substring(0, DriverNames.Length - 4); } }
                        catch (Exception) { }
                        try { if (AuthDrivers.Substring(AuthDrivers.Length - 4) == "and ") { AuthDrivers = AuthDrivers.Substring(0, AuthDrivers.Length - 4); } }
                        catch (Exception) { }
                        try { if (ExcludedDrivers.Substring(ExcludedDrivers.Length - 4) == "and ") { ExcludedDrivers = ExcludedDrivers.Substring(0, ExcludedDrivers.Length - 4); } }
                        catch (Exception) { }
                        try { if (ExceptedDrivers.Substring(ExceptedDrivers.Length - 4) == "and ") { ExceptedDrivers = ExceptedDrivers.Substring(0, ExceptedDrivers.Length - 4); } }
                        catch (Exception) { }


                        try { PolicyHolderNames = PolicyHolderNames.Replace("'", ""); }
                        catch (Exception) { }

                        try { DriverNames = DriverNames.Replace("'", ""); }
                        catch (Exception) { }

                        if (VehCoveNote.risk.mainDriver != null && NamesOnPolicy == "")
                        {
                            try
                            {
                                VehCoveNote.PolicyHolders = VehCoveNote.risk.mainDriver.person.fname + " " + VehCoveNote.risk.mainDriver.person.mname + " " + VehCoveNote.risk.mainDriver.person.lname; ;
                            }
                            catch (Exception)
                            {
                                VehCoveNote.PolicyHolders = "";
                            }

                        }
                        
                        string MainInsured = ""; try { var mainInsured = VehCoveNote.Policy.insured.OrderBy(p => p.PersonID).Take(1).FirstOrDefault(); MainInsured = mainInsured.fname + " " + mainInsured.lname; }catch(Exception){}
                        //try { if (string.IsNullOrEmpty(MainInsured) && !string.IsNullOrEmpty(CompanyNames.Trim())) MainInsured = CompanyNames; }
                        //catch (Exception) { }

                        try { VehCovGenerated.cancelled = VehCoveNote.cancelled; } catch (Exception) { }
                        try { VehCovGenerated.effectivedate = VehCoveNote.effectivedate.ToString(); }catch(Exception){}
                        try { VehCovGenerated.expirydate = VehCoveNote.expirydate.ToString(); }catch(Exception){}
                        try { VehCovGenerated.certitificateNo =VehCoveNote.covernoteno; }catch(Exception){}
                        try { VehCovGenerated.VIN = VehCoveNote.VIN; }catch(Exception){}
                        try { VehCovGenerated.chassisNo = VehCoveNote.chassisNo; }catch(Exception){}
                        try { VehCovGenerated.vehmake = VehCoveNote.vehmake; }catch(Exception){}
                        try { VehCovGenerated.vehmodel = VehCoveNote.vehmodel; }catch(Exception){}
                        try { VehCovGenerated.vehyear = VehCoveNote.vehyear; }catch(Exception){}
                        try { VehCovGenerated.Usage = VehCoveNote.Usages; }catch(Exception){}
                        try { VehCovGenerated.vehregno = VehCoveNote.vehregno; }catch(Exception){}
                        try { VehCovGenerated.engineNo = VehCoveNote.engineNo; }catch(Exception){}
                        try { VehCovGenerated.seating = VehCoveNote.seating; }catch(Exception){}
                        try { VehCovGenerated.HPCC = VehCoveNote.HPCC; }catch(Exception){}
                        try { VehCovGenerated.Approved = VehCoveNote.approved; }catch(Exception){}
                        try { VehCovGenerated.authorizedwording =  VehCoveNote.authorizedwording; }catch(Exception){}
                        try { VehCovGenerated.authorizedwording =  VehCoveNote.authorizedwording; }catch(Exception){}
                            try { VehCovGenerated.bodyType=  VehCoveNote.bodyType; }catch(Exception){}
                            try { VehCovGenerated.CancelledStatus = VehCoveNote.cancelled; }catch(Exception){}
                            try { VehCovGenerated.certificateCode =  VehCoveNote.certificateCode; }catch(Exception){}
                            try { VehCovGenerated.certificateType = VehCoveNote.certificateType; }catch(Exception){}
                            try { VehCovGenerated.chassisNo =  VehCoveNote.chassisNo; }catch(Exception){}
                            try {  VehCovGenerated.certitificateNo = VehCoveNote.certitificateNo; }catch(Exception){}
                            try { VehCovGenerated.CompanyName =  VehCoveNote.CompanyName; }catch(Exception){}
                            try { VehCovGenerated.cover =  VehCoveNote.Cover;}catch(Exception){}
                            try { VehCovGenerated.CovernoteNo =  VehCoveNote.covernoteno; }catch(Exception){}
                            try { VehCovGenerated.drivers=  VehCoveNote.drivers; }catch(Exception){}
                            try { VehCovGenerated.effectivedate =  VehCoveNote.effectivedate.ToString(); }catch(Exception){}
                            try { VehCovGenerated.effectiveTime = VehCoveNote.effectiveTime; }catch(Exception){}
                            try { VehCovGenerated.endorsementNo=  VehCoveNote.endorsementno;}catch(Exception){}
                            try { VehCovGenerated.engineNo =  VehCoveNote.engineNo; }catch(Exception){}
                            try { VehCovGenerated.expirydate = VehCoveNote.expirydate.ToString(); }catch(Exception){}
                            try { VehCovGenerated.expiryTime =  VehCoveNote.expiryTime; }catch(Exception){}
                            try { VehCovGenerated.HPCC = VehCoveNote.HPCC; }catch(Exception){}
                            try { VehCovGenerated.limitsofuse = VehCoveNote.limitsofuse; }catch(Exception){}
                            try { VehCovGenerated.PolicyHolders =  VehCoveNote.PolicyHolders; }catch(Exception){}
                            try { VehCovGenerated.PolicyHoldersAddress = PolicyHoldersAddress; }catch(Exception){}
                            try { VehCovGenerated.PolicyID = VehCoveNote.Policy.ID; }catch(Exception){}
                            try { VehCovGenerated.PolicyNo =  VehCoveNote.PolicyNo; }catch(Exception){}
                            try { VehCovGenerated.referenceNo =""; }catch(Exception){}
                            try { VehCovGenerated.seating =  VehCoveNote.seating; }catch(Exception){}
                            try { VehCovGenerated.Usage = VehCoveNote.Usages; }catch(Exception){}
                            try {  VehCovGenerated.vehdesc = VehCoveNote.vehdesc; }
                            catch (Exception) { }
                            try {  VehCovGenerated.WordID = VehCoveNote.WordID;}catch(Exception){}                         
                            try { VehCovGenerated.vehExt =  VehCoveNote.vehExt; }catch(Exception){}
                            try { VehCovGenerated.VehicleMetaDataID = VehCoveNote.VehicleMetaDataID; }catch(Exception){}
                            try { VehCovGenerated.VehicleID =  VehCoveNote.risk.ID; }catch(Exception){}
                            try { VehCovGenerated.vehmake =  VehCoveNote.vehmake; }catch(Exception){}
                            try { VehCovGenerated.vehmodel =  VehCoveNote.vehmodel; }catch(Exception){}
                            try { VehCovGenerated.vehmodeltype =  VehCoveNote.vehmodeltype; }catch(Exception){}
                            try { VehCovGenerated.vehregno =  VehCoveNote.vehregno; }catch(Exception){}
                            try { VehCovGenerated.vehyear =  VehCoveNote.vehyear; }catch(Exception){}
                            try { VehCovGenerated.VIN =  VehCoveNote.VIN; }catch(Exception){} 
                            try { VehCovGenerated.ManualCoverNoteNumber = VehCoveNote.ManualCoverNoteNumber; }catch(Exception){}
                            try { VehCovGenerated.DateLastPrinted = DateTime.Now; }catch(Exception){}
                            try { VehCovGenerated.DateFirstPrinted = DateTime.Now; }catch(Exception){}
                            try { VehCovGenerated.IntermediaryID= VehCoveNote.IntermediaryID; }catch(Exception){}
                            try { VehCovGenerated.estimatedValue = VehCoveNote.risk.estimatedValue; }catch(Exception){}
                         
                         try { VehCovGenerated.limitsofuse = TemplateWordingModel.ReplaceBraceTags(VehCovGenerated.limitsofuse,DriverNames,AuthDrivers,ExcludedDrivers,ExceptedDrivers,PolicyHolderNames,MainInsured).ToUpper(); }catch (Exception) { }
                         try { VehCovGenerated.authorizedwording = TemplateWordingModel.ReplaceBraceTags(VehCovGenerated.authorizedwording,DriverNames,AuthDrivers,ExcludedDrivers,ExceptedDrivers,PolicyHolderNames, MainInsured).ToUpper(); }catch (Exception) { }
                           


                        //string NamesOnPolicy = "";

                        try
                        {
                            //Main Driver
                            NamesOnPolicy = VehCoveNote.risk.mainDriver.person.fname + " " + VehCoveNote.risk.mainDriver.person.mname + " " + VehCoveNote.risk.mainDriver.person.lname + " and ";

                            //Authorized Drivers Information
                            if (VehCoveNote.risk.AuthorizedDrivers != null) { foreach (var N in VehCoveNote.risk.AuthorizedDrivers) { NamesOnPolicy += (N.person.fname + " " + N.person.mname + N.person.lname + " and "); } }

                            //Excepted Drivers Information
                            if (VehCoveNote.risk.ExceptedDrivers != null) { foreach (var N in VehCoveNote.risk.ExceptedDrivers) { NamesOnPolicy += (N.person.fname + " " + N.person.mname + N.person.lname + " and "); } }

                            //Excluded Drivers Information
                            if (VehCoveNote.risk.ExcludedDrivers != null) { foreach (var N in VehCoveNote.risk.ExcludedDrivers) { NamesOnPolicy += (N.person.fname + " " + N.person.mname + N.person.lname + " and "); } }

                            NamesOnPolicy = NamesOnPolicy.Substring(0, NamesOnPolicy.Length - 4); //Removes the last 'and' from the string.

                            //Main Driver Information
                            if(string.IsNullOrEmpty(VehCoveNote.PolicyHolders.Trim())){
                                 VehCovGenerated.PolicyHolders = NamesOnPolicy;
                            }

                           
                        }
                        catch (Exception)
                        {

                        }

                        if (VehCoveNote.risk.mainDriver != null && NamesOnPolicy =="")
                        {
                            try
                            {
                                VehCovGenerated.PolicyHolders = VehCoveNote.risk.mainDriver.person.fname + " " + VehCoveNote.risk.mainDriver.person.mname + " " + VehCoveNote.risk.mainDriver.person.lname;
                            }
                            catch (Exception)
                            {
                               
                            }

                        }
                       
                        generatedNoteList.Add(VehCovGenerated);
                    }
                       

                    else
                    {
                        VehCoveNote.Policy = PolicyModel.getPolicyFromID(VehCoveNote.Policy.ID);
                        VehCoveNote.risk.AuthorizedDrivers = VehicleModel.GetDriverList(VehCoveNote.risk.ID, VehCoveNote.Policy.ID, 1);
                        VehCoveNote.PolicyNo = VehCoveNote.Policy.policyNumber;

                        try { Wording = TemplateWordingModel.GetWording(VehCoveNote.wording.ID); }catch(Exception){}
                        VehCovGenerated.HeaderImgLocation = TemplateImageModel.get_image_location(VehCoveNote.Policy.insuredby, "header", "cover note");
                        VehCovGenerated.FooterImgLocation = TemplateImageModel.get_image_location(VehCoveNote.Policy.insuredby, "footer", "cover note");
                        VehCovGenerated.LogoLocation = TemplateImageModel.get_image_location(VehCoveNote.Policy.insuredby, "logo", "cover note");
                        if (VehCovGenerated.cover == null) VehCovGenerated.cover = "";
                        if (VehCovGenerated.PolicyHoldersAddress == null) VehCovGenerated.PolicyHoldersAddress = "";
                        VehCovGenerated.vehmake = VehCoveNote.risk.make;
                        VehCovGenerated.vehregno = VehCoveNote.risk.VehicleRegNumber;
                        VehCovGenerated.vehyear = VehCoveNote.risk.VehicleYear.ToString();
                        VehCovGenerated.vehmodel = VehCoveNote.risk.VehicleModel;
                        VehCovGenerated.vehExt = VehCoveNote.risk.extension;
                        VehCovGenerated.PolicyID = VehCoveNote.Policy.ID;
                        VehCovGenerated.VehicleID = VehCoveNote.risk.ID;
                        VehCovGenerated.Approved = VehCoveNote.approved;
                        VehCovGenerated.CompanyName = VehCoveNote.company.CompanyName;

                         try { VehCoveNote.risk = VehicleModel.GetVehicle(VehCoveNote.risk.ID); }catch (Exception) { }
                        
                        //Get Relevant information for to Generate Certificates
                        string NamesOnPolicy = "";
                        string DriverNames = "";
                        string AuthDrivers = "";
                        string ExcludedDrivers = "";
                        string ExceptedDrivers = "";
                        string PolicyHoldersAddress = "";
                        //Gets polcy holders address
                        try
                        {
                            if (VehCoveNote.Policy.mailingAddress != null)
                            {
                                if (VehCoveNote.Policy.mailingAddress.city != null)
                                {
                                    PolicyHoldersAddress += ((!String.IsNullOrEmpty(VehCoveNote.Policy.mailingAddress.ApartmentNumber) ? VehCoveNote.Policy.mailingAddress.ApartmentNumber + ", " : "") + (!string.IsNullOrEmpty(VehCoveNote.Policy.mailingAddress.roadnumber) ? VehCoveNote.Policy.mailingAddress.roadnumber : "") + " " + (VehCoveNote.Policy.mailingAddress.road.RoadName != null ? VehCoveNote.Policy.mailingAddress.road.RoadName : "") + " " + (!string.IsNullOrEmpty(VehCoveNote.Policy.mailingAddress.roadtype.RoadTypeName) ? VehCoveNote.Policy.mailingAddress.roadtype.RoadTypeName + "," : ",") + " " + (VehCoveNote.Policy.mailingAddress.city.CityName != null ? VehCoveNote.Policy.mailingAddress.city.CityName + "," : ","));
                                }

                                if (VehCoveNote.Policy.mailingAddress.parish != null)
                                {
                                    PolicyHoldersAddress += (" " + VehCoveNote.Policy.mailingAddress.parish.parish);
                                }

                                if (VehCoveNote.Policy.mailingAddress.country != null)
                                {
                                    PolicyHoldersAddress += (" " + VehCoveNote.Policy.mailingAddress.country.CountryName);
                                }

                            }
                        }
                        catch (Exception)
                        {

                        }

                        try { DriverNames = VehCoveNote.risk.mainDriver.person.fname + " " + VehCoveNote.risk.mainDriver.person.mname + " " + VehCoveNote.risk.mainDriver.person.lname + " and "; }
                        catch (Exception) { }
                        string PolicyHolderNames = "";

                        string CompanyNames = "";
                        try { foreach (var N in VehCoveNote.Policy.company) { CompanyNames += N.CompanyName + " and "; } }
                        catch (Exception) { }
                        try { CompanyNames = CompanyNames.Substring(0, CompanyNames.Length - 4); }
                        catch (Exception) { }

                        

                        try { foreach (var N in VehCoveNote.Policy.insured) { if (!PolicyHolderNames.Contains((N.fname + " " + N.mname + " " + N.lname))) { PolicyHolderNames += N.fname + " " + N.mname + " " + N.lname + " and "; } } }
                        catch (Exception) { }
                        //try { foreach (var N in VehCoveNote.Policy.vehicles) { foreach (var d in N.AuthorizedDrivers) { if (!PolicyHolderNames.Contains((d.person.fname + " " + d.person.mname + " " + d.person.lname))) { PolicyHolderNames += d.person.fname + " " + d.person.mname + " " + d.person.lname + " and "; } } } }
                        //catch (Exception) { }
                        
                        AuthDrivers += PolicyHolderNames;
                        
                        PolicyHolderNames += CompanyNames;
                        

                        try { foreach (var N in VehCoveNote.Policy.vehicles) { foreach (var d in N.AuthorizedDrivers) { if (!AuthDrivers.Contains((d.person.fname + " " + d.person.mname + " " + d.person.lname))) { AuthDrivers += d.person.fname + " " + d.person.mname + " " + d.person.lname + " and "; } } } }
                        catch (Exception) { }


                        try { DriverNames = VehCoveNote.risk.mainDriver.person.fname + " " + VehCoveNote.risk.mainDriver.person.mname + " " + VehCoveNote.risk.mainDriver.person.lname; }
                        catch (Exception) { }

                        try { foreach (var N in VehCoveNote.risk.ExceptedDrivers) { if (!ExceptedDrivers.Contains((N.person.fname + " " + N.person.mname + " " + N.person.lname))) { ExceptedDrivers += N.person.fname + " " + N.person.mname + " " + N.person.lname + " and "; } } }
                        catch (Exception) { }
                        try { foreach (var N in VehCoveNote.Policy.vehicles[0].ExcludedDrivers) { if (!ExcludedDrivers.Contains((N.person.fname + " " + N.person.mname + " " + N.person.lname))) { ExcludedDrivers += N.person.fname + " " + N.person.mname + " " + N.person.lname + " and "; } } }
                        catch (Exception) { }

                        try { if (PolicyHolderNames.Substring(PolicyHolderNames.Length - 4) == "and ") { PolicyHolderNames = PolicyHolderNames.Substring(0, PolicyHolderNames.Length - 4); } }
                        catch (Exception) { }

                        try { if (DriverNames.Substring(DriverNames.Length - 4) == "and ") { DriverNames = DriverNames.Substring(0, DriverNames.Length - 4); } }
                        catch (Exception) { }
                        try { if (AuthDrivers.Substring(AuthDrivers.Length - 4) == "and ") { AuthDrivers = AuthDrivers.Substring(0, AuthDrivers.Length - 4); } }
                        catch (Exception) { }
                        try { if (ExcludedDrivers.Substring(ExcludedDrivers.Length - 4) == "and ") { ExcludedDrivers = ExcludedDrivers.Substring(0, ExcludedDrivers.Length - 4); } }
                        catch (Exception) { }
                        try { if (ExceptedDrivers.Substring(ExceptedDrivers.Length - 4) == "and ") { ExceptedDrivers = ExceptedDrivers.Substring(0, ExceptedDrivers.Length - 4); } }
                        catch (Exception) { }


                        try { PolicyHolderNames = PolicyHolderNames.Replace("'", ""); }
                        catch (Exception) { }

                        try { DriverNames = DriverNames.Replace("'", ""); }
                        catch (Exception) { }

                        if (VehCoveNote.risk.mainDriver != null && NamesOnPolicy == "")
                        {
                            try
                            {
                                VehCoveNote.PolicyHolders = VehCoveNote.risk.mainDriver.person.fname + " " + VehCoveNote.risk.mainDriver.person.mname + " " + VehCoveNote.risk.mainDriver.person.lname; ;
                            }
                            catch (Exception)
                            {
                                VehCoveNote.PolicyHolders = "";
                            }

                        }

                        //Get Mortgagees
                        string Mortgagees = "";
                        try
                        {
                             var R =  PolicyModel.GetInsuredVehicles(VehCoveNote.Policy.ID);
                             if (R != null) 
                             {
                                 foreach (var M in R) 
                                 {
                                     try { Mortgagees += (M.mortgagee.mortgagee + Environment.NewLine); }catch(Exception){}
                                 }
                             }
                        }
                        catch (Exception)
                        {
                           
                        }
                            string MainInsured = ""; try { var mainInsured = VehCoveNote.Policy.insured.OrderByDescending(p => p.PersonID).Take(1).FirstOrDefault(); MainInsured = mainInsured.fname + " " + mainInsured.lname; }catch(Exception){}
                            //try { if (string.IsNullOrEmpty(MainInsured) && !string.IsNullOrEmpty(CompanyNames.Trim())) MainInsured = CompanyNames; }
                            //catch (Exception) { }

                            VehCovGenerated.mortgagees = Mortgagees;
                            VehCoveNote.mortgagees = Mortgagees;
                            try { VehCovGenerated.effectivedate = VehCoveNote.effectivedate.ToString(); }catch(Exception){}
                            try { VehCovGenerated.expirydate = VehCoveNote.expirydate.ToString(); }catch(Exception){}
                            try { VehCovGenerated.certitificateNo =VehCoveNote.covernoteno; }catch(Exception){}
                            try { VehCovGenerated.VIN = VehCoveNote.VIN; }catch(Exception){}
                            try { VehCovGenerated.chassisNo = VehCoveNote.chassisNo; }catch(Exception){}
                            try { VehCovGenerated.vehmake = VehCoveNote.vehmake; }catch(Exception){}
                            try { VehCovGenerated.vehmodel = VehCoveNote.vehmodel; }catch(Exception){}
                            try { VehCovGenerated.vehyear = VehCoveNote.vehyear; }catch(Exception){}
                            try { VehCovGenerated.Usage = VehCoveNote.Usages; }catch(Exception){}
                            try { VehCovGenerated.vehregno = VehCoveNote.vehregno; }catch(Exception){}
                            try { VehCovGenerated.engineNo = VehCoveNote.engineNo; }catch(Exception){}
                            try { VehCovGenerated.seating = VehCoveNote.risk.seating.ToString(); }catch(Exception){}
                            try { VehCovGenerated.HPCC = VehCoveNote.HPCC; }catch(Exception){}
                            try { VehCovGenerated.Approved = VehCoveNote.approved; }catch(Exception){}
                            try { VehCovGenerated.authorizedwording =  VehCoveNote.authorizedwording; }catch(Exception){}
                            try { VehCovGenerated.authorizedwording =  VehCoveNote.authorizedwording; }catch(Exception){}
                            try { VehCovGenerated.bodyType =  VehCoveNote.risk.bodyType; }catch(Exception){}
                            try { VehCovGenerated.CancelledStatus = VehCoveNote.cancelled; }catch(Exception){}
                            try { VehCovGenerated.certificateCode =  VehCoveNote.certificateCode; }catch(Exception){}
                            try { VehCovGenerated.certificateType = VehCoveNote.certificateType; }catch(Exception){}
                            try { VehCovGenerated.chassisNo =  VehCoveNote.chassisNo; }catch(Exception){}
                            try {  VehCovGenerated.certitificateNo = VehCoveNote.certitificateNo; }catch(Exception){}
                            try { VehCovGenerated.CompanyName =  VehCoveNote.CompanyName; }catch(Exception){}
                            try { VehCovGenerated.cover =  VehCoveNote.Cover;}catch(Exception){}
                            try { VehCovGenerated.CovernoteNo =  VehCoveNote.covernoteno; }catch(Exception){}
                            try { VehCovGenerated.drivers=  VehCoveNote.drivers; }catch(Exception){}
                            try { VehCovGenerated.effectivedate =  VehCoveNote.effectivedate.ToString(); }catch(Exception){}
                            try { VehCovGenerated.effectiveTime = VehCoveNote.effectiveTime; }catch(Exception){}
                            try { VehCovGenerated.endorsementNo=  VehCoveNote.endorsementno;}catch(Exception){}
                            try { VehCovGenerated.engineNo =  VehCoveNote.engineNo; }catch(Exception){}
                            try { VehCovGenerated.expirydate = VehCoveNote.expirydate.ToString(); }catch(Exception){}
                            try { VehCovGenerated.expiryTime =  VehCoveNote.expiryTime; }catch(Exception){}
                            try { VehCovGenerated.HPCC = VehCoveNote.HPCC; }catch(Exception){}
                            try { VehCovGenerated.limitsofuse = VehCoveNote.limitsofuse; }catch(Exception){}
                            try { VehCovGenerated.PolicyHolders =  PolicyHolderNames;}catch(Exception){}//!string.IsNullOrEmpty(VehCoveNote.PolicyHolders.Trim()) ? VehCoveNote.PolicyHolders : PolicyHolderNames  ; }catch(Exception){}
                            try { VehCovGenerated.PolicyHoldersAddress = PolicyHoldersAddress; }catch(Exception){}
                            try { VehCovGenerated.PolicyID = VehCoveNote.Policy.ID; }catch(Exception){}
                            try { VehCovGenerated.PolicyNo =  VehCoveNote.PolicyNo; }catch(Exception){}
                            try { VehCovGenerated.referenceNo =""; }catch(Exception){}
                            try { VehCovGenerated.seating =  VehCoveNote.seating; }catch(Exception){}
                            try { VehCovGenerated.Usage = VehCoveNote.Usages; }catch(Exception){}
                            try {  VehCovGenerated.vehdesc = VehCoveNote.vehdesc; }
                            catch (Exception) { }
                            try {  VehCovGenerated.WordID = VehCoveNote.WordID;}catch(Exception){}                         
                            try { VehCovGenerated.vehExt =  VehCoveNote.vehExt; }catch(Exception){}
                            try { VehCovGenerated.VehicleMetaDataID = VehCoveNote.VehicleMetaDataID; }catch(Exception){}
                            try { VehCovGenerated.VehicleID =  VehCoveNote.risk.ID; }catch(Exception){}
                            try { VehCovGenerated.vehmake =  VehCoveNote.vehmake; }catch(Exception){}
                            try { VehCovGenerated.vehmodel =  VehCoveNote.vehmodel; }catch(Exception){}
                            try { VehCovGenerated.vehmodeltype =  VehCoveNote.vehmodeltype; }catch(Exception){}
                            try { VehCovGenerated.vehregno =  VehCoveNote.vehregno; }catch(Exception){}
                            try { VehCovGenerated.vehyear =  VehCoveNote.vehyear; }catch(Exception){}
                            try { VehCovGenerated.VIN =  VehCoveNote.VIN; }catch(Exception){} 
                            try { VehCovGenerated.ManualCoverNoteNumber = VehCoveNote.ManualCoverNoteNumber; }catch(Exception){}
                            try { VehCovGenerated.DateLastPrinted = DateTime.Now; }catch(Exception){}
                            try { VehCovGenerated.DateFirstPrinted = DateTime.Now; }catch(Exception){}
                            try { VehCovGenerated.IntermediaryID= VehCoveNote.IntermediaryID; }catch(Exception){}
                            try { VehCovGenerated.estimatedValue = VehCoveNote.risk.estimatedValue; }catch(Exception){}
                            try { VehCovGenerated.Period = VehCoveNote.period.ToString(); }catch(Exception){}
                            
                            
                            if(string.IsNullOrEmpty(DriverNames.Trim())){DriverNames = AuthDrivers;}

                            if (VehCoveNote.wording != null) 
                            {
                                if (!string.IsNullOrEmpty(Wording.AuthorizedDrivers)) 
                                {
                                    VehCovGenerated.authorizedwording = Wording.AuthorizedDrivers;
                                }
                                if (!string.IsNullOrEmpty(Wording.LimitsOfUse))
                                {
                                    VehCovGenerated.limitsofuse = Wording.LimitsOfUse;
                                } 
                            }

                         try { VehCovGenerated.limitsofuse = TemplateWordingModel.ReplaceBraceTags(VehCovGenerated.limitsofuse,DriverNames,AuthDrivers,ExcludedDrivers,ExceptedDrivers,PolicyHolderNames,MainInsured); }catch (Exception) { }
                         try { VehCovGenerated.authorizedwording = TemplateWordingModel.ReplaceBraceTags(VehCovGenerated.authorizedwording,DriverNames,AuthDrivers,ExcludedDrivers,ExceptedDrivers,PolicyHolderNames, MainInsured); }catch (Exception) { }
                           
                        
                        try
                        {
                            //Main Driver
                            //NamesOnPolicy = VehCoveNote.risk.mainDriver.person.fname + " " + VehCoveNote.risk.mainDriver.person.mname + " " + VehCoveNote.risk.mainDriver.person.lname + " and ";

                            //Authorized Drivers Information
                            if (VehCoveNote.risk.AuthorizedDrivers != null) { foreach (var N in VehCoveNote.risk.AuthorizedDrivers) { NamesOnPolicy += (N.person.fname + " " + N.person.mname + N.person.lname + " and "); } }

                            //Excepted Drivers Information
                            if (VehCoveNote.risk.ExceptedDrivers != null) { foreach (var N in VehCoveNote.risk.ExceptedDrivers) { NamesOnPolicy += (N.person.fname + " " + N.person.mname + N.person.lname + " and "); } }

                            //Excluded Drivers Information
                            if (VehCoveNote.risk.ExcludedDrivers != null) { foreach (var N in VehCoveNote.risk.ExcludedDrivers) { NamesOnPolicy += (N.person.fname + " " + N.person.mname + N.person.lname + " and "); } }
                            
                            NamesOnPolicy = NamesOnPolicy.Substring(0, NamesOnPolicy.Length - 4); //Removes the last 'and' from the string.

                            //Main Driver Information
                            if (string.IsNullOrEmpty(VehCovGenerated.PolicyHolders.Trim())) { VehCovGenerated.PolicyHolders = NamesOnPolicy; }
                        }
                        catch (Exception)
                        {
                          
                        }

                        if (string.IsNullOrEmpty(VehCovGenerated.PolicyHolders.Trim())) { VehCovGenerated.PolicyHolders = NamesOnPolicy; }

                        if (VehCoveNote.risk.mainDriver != null && string.IsNullOrEmpty(VehCovGenerated.PolicyHolders.Trim()))
                        {
                            try
                            {
                                VehCovGenerated.PolicyHolders = VehCoveNote.risk.mainDriver.person.fname + " " + VehCoveNote.risk.mainDriver.person.mname + " " + VehCoveNote.risk.mainDriver.person.lname; ;
                            }
                            catch (Exception)
                            {
                               
                            }

                        }

                        //VehCovGenerated.limitsofuse = TemplateWordingModel.ReplaceBraceTags(VehCovGenerated.limitsofuse, "", "", "", "", NamesOnPolicy);
                        generatedNoteList.Add(VehCovGenerated);

                        //Adds Cover Note to the system. After Printing
                       // VehicleCoverNoteGeneratedModel.InsertGenCoverNote(VehCovGenerated, VehCoveNote.ID);
                    }

                    //Testing if the cover note has been cancelled
                    generatedNoteList[generatedNoteList.Count - 1].cancelled = VehCoveNote.cancelled;

                    // Adding the image locationg to the generated cert model
                    for (int x = 0; x < images.Count(); x++)
                    {
                        if (images[x].type == "Cover Note")
                        {
                            switch (images[x].position)
                            {
                                case "header":
                                    generatedNoteList[generatedNoteList.Count - 1].HeaderImgLocation = Server.MapPath("/files/" + images[x].location);
                                    break;

                                case "footer":
                                    generatedNoteList[generatedNoteList.Count - 1].FooterImgLocation = Server.MapPath("/files/" + images[x].location);
                                    break;

                                case "logo":
                                    generatedNoteList[generatedNoteList.Count - 1].LogoLocation = Server.MapPath("/files/" + images[x].location);
                                    break;
                            }
                        }

                    }

                    /////////////////////////////////////////////////////////////////////////

                    
                }

                if (VehCoveNotes != null && VehCoveNotes.Count != 0)
                Constants.document = PDF.PrintCoverNoteToPDF(generatedNoteList, PolicyModel.getPolicyFromID(VehCoveNotes[0].Policy.ID).insuredby.CompanyID); //If it's mass creation , then all notes will have the same comp Ids, so just send the first one
                
                if (Constants.document.PageCount == 0) // If there was an error in printing
                {
                    if (Constants.document.Tag == "NO CODE")
                    {
                        Constants.document = new PdfDocument();
                        return Json(new { result = 0, nocode = 1 }, JsonRequestBehavior.AllowGet);
                    }
                    else return Json(new { result = 0, nocode = 0 }, JsonRequestBehavior.AllowGet);
                }

                //Updating the print count of the certificate
                 foreach(VehicleCoverNote VehCoveNote in VehCoveNotes){
                     VehicleCoverNoteModel.PrintCoverNote(VehCoveNote);
                 }
                return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                TempData["CertInactive"] = "You do not have the required permissions to complete this request";
                //return RedirectToAction("AccessDenied", "Error");
                return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }

        }


        /// <summary>
        /// This function is used to enable the printing for a particular Risk covernote
        /// </summary>
        /// <param name="id">Risk Cover note Id</param>
        public ActionResult EnablePrinting(int id = 0)
        {
             if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Approve")) && (VehicleCoverNoteModel.IsCoverNoteActive(id))))
             {

                 bool allow = false;

                 if (Constants.user.newRoleMode)
                 {
                     allow = CompanyModel.IsCompanyInsurer(Constants.user.tempUserGroup.company.CompanyID);
                 }
                 else
                     allow = CompanyModel.IsCompanyInsurer(UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID);

                 if (allow)
                 {
                     if (VehicleCoverNoteModel.EnablePrint(id)) return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                     else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
                 }
                 else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
             }
             else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This function retrieves the vehicle cover note of a vehicle, under a given time frame
        /// </summary>
        public ActionResult GetThisActiveNote(int id = 0, string start = null, string end = null, int policyid = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Read")))
            {
                VehicleCoverNote vcn = new VehicleCoverNote();
                vcn = VehicleCoverNoteModel.GetActiveCoverNote(id, start, end, policyid);
                return RedirectToAction("Details", "VehicleCoverNote", new { id = vcn.ID});
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult PrintRequest(int id = 0)
        {
            if (VehicleCoverNoteModel.IsCoverNoteActive(id))
            {
                VehicleCoverNoteModel.PrintRequest(id);
                return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function creates all the vehicle cover notes for risks under a given policy
        /// </summary>
        /// <param name="id">Vehicle's id </param>
        public ActionResult CreateAllCovNotes(List<string> vehiclesList = null, List<string> templatewordings = null, int polId = 0, List<DateTime> EffectiveDates = null, List<DateTime> ExpiryDates = null, string proceed = "")
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Create")))
            {
                List<VehicleCoverNote> vehicleCoverNotes = new List<VehicleCoverNote>();
                string thisMessage = "";
                List<int> NoteIDs = new List<int>();
                bool autoApp = false; bool canPrint = false; bool canApprove = false;

                if (vehiclesList != null && templatewordings != null)
                {

                   Policy Policy = new Policy();
                   Policy = PolicyModel.getPolicyFromID(polId);

                   if (!CompanyModel.IsCompanyShortSet(Policy.insuredby))
                       return Json(new { compShortening = 1, result = 0, wordingError = 0, Exists = 0}, JsonRequestBehavior.AllowGet);

                     templatewordings.ToArray();
                     EffectiveDates.ToArray();

                    for (int i = 0; i < vehiclesList.Count; i++)
                    {
                        if ((templatewordings == null || EffectiveDates == null || ExpiryDates == null) || (templatewordings[i] == "" || EffectiveDates[i].ToString() =="" || ExpiryDates[i].ToString() ==""))
                        {
                            return Json(new { wordingError = 1, result = 0, compShortening = 0, Exists = 0 }, JsonRequestBehavior.AllowGet);
                        }

                        VehicleCoverNote vcn = new VehicleCoverNote ();
                        vcn.Policy = PolicyModel.getPolicyFromID(polId);
                        vcn.risk = new Vehicle(int.Parse(vehiclesList[i]));
                        vcn.wording = new TemplateWording(int.Parse(templatewordings[i]));
                        vcn.effectivedate = EffectiveDates[i];
                        vcn.expirydate = Convert.ToDateTime(ExpiryDates[i].AddDays(-1).ToString("MM/dd/yyyy") + " 11:59 PM");
                        vcn.period = (ExpiryDates[i] - EffectiveDates[i]).Days;
                        vcn.company = Policy.insuredby; 
                        vehicleCoverNotes.Add(vcn);

                        //Testing if a previous cover note exists in the time frame
                        if (proceed == "")
                        {
                            if (VehicleCoverNoteModel.IsCoverNoteExistNow(vcn.risk.ID, vcn.effectivedate, vcn.expirydate))
                            {
                                Vehicle Thisveh = VehicleModel.GetVehicle(vcn.risk.ID);
                                thisMessage = thisMessage + "The vehicle : " + Thisveh.VehicleYear + " " + Thisveh.make + " " + Thisveh.VehicleModel + "has an existing, active covernote during the time frame selected. ";
                            }
                        }
                    }

                    //If previous notes exists, then alert the user
                    if (thisMessage != "")
                    {
                        return Json(new { wordingError = 0, result = 0, compShortening = 0, Exists = 1, noteMessage = thisMessage }, JsonRequestBehavior.AllowGet);
                    }

                    canPrint =  UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Print"));
                    autoApp = UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "AutoApprove"));
                    canApprove = UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Approve"));

                    //Creating all the cover notes
                    foreach (var covernote in vehicleCoverNotes)
                    {


                        if (PolicyModel.IsVehicleCovered(covernote.risk.ID, covernote.effectivedate, covernote.expirydate))
                        {

                            if (proceed == "" && VehicleCoverNoteModel.IsCoverNoteExistNow(covernote.risk.ID, covernote.effectivedate, covernote.expirydate))
                            {
                                ViewData["NoteExistsAlert"] = "The seleced risk already has an active cover note during this time period. Would you like to proceed?";
                                return View(covernote);
                            }

                            if (covernote.covernoteno == null || VehicleCoverNoteModel.TestManualCoverNoteNo(covernote))
                            {
                                int id = 0; // VehicleCoverNoteModel.AddVehicleCoverNote(covernote, covernote.Policy.ID, covernote.Policy.insuredby.CompanyID, true);

                                //Gets Covernote Risk Data
                                 try {covernote.risk = VehicleModel.GetVehicle(covernote.risk.ID); }catch(Exception){}

                                string NamesOnPolicy = "";
                                string DriverNames = "";
                                string AuthDrivers = "";
                                string ExcludedDrivers = "";
                                string ExceptedDrivers = "";
                                string PolicyHoldersAddress = "";

                                //Gets polcy holders address
                                try
                                {
                                    if (covernote.Policy.mailingAddress != null)
                                    {
                                        if (covernote.Policy.mailingAddress.city != null)
                                        {
                                            PolicyHoldersAddress += (covernote.Policy.mailingAddress.ApartmentNumber + " " + covernote.Policy.mailingAddress.city.CityName);
                                        }

                                        if (covernote.Policy.mailingAddress.parish != null)
                                        {
                                            PolicyHoldersAddress += (" " + covernote.Policy.mailingAddress.parish.parish);
                                        }

                                        if (covernote.Policy.mailingAddress.country != null)
                                        {
                                            PolicyHoldersAddress += (" " + covernote.Policy.mailingAddress.country.CountryName);
                                        }

                                    }
                                }
                                catch (Exception)
                                {

                                }



                                try { DriverNames = covernote.risk.mainDriver.person.fname + " " + covernote.risk.mainDriver.person.mname + " " + covernote.risk.mainDriver.person.lname + " and "; }
                                catch (Exception) { }
                                string PolicyHolderNames = "";

                                string CompanyNames = "";
                                try { foreach (var N in covernote.Policy.company) { CompanyNames += N.CompanyName + " and "; } }
                                catch (Exception) { }
                                try { CompanyNames = CompanyNames.Substring(0, CompanyNames.Length - 4); }
                                catch (Exception) { }
                                PolicyHolderNames += CompanyNames;

                                try { foreach (var N in covernote.Policy.insured) { if (!PolicyHolderNames.Contains((N.fname + " " + N.mname + " " + N.lname))) { PolicyHolderNames += N.fname + " " + N.mname + " " + N.lname + " and "; } } }
                                catch (Exception) { }
                                try { foreach (var N in covernote.Policy.vehicles) { foreach (var d in N.AuthorizedDrivers) { if (!PolicyHolderNames.Contains((d.person.fname + " " + d.person.mname + " " + d.person.lname))) { PolicyHolderNames += d.person.fname + " " + d.person.mname + " " + d.person.lname + " and "; } } } }
                                catch (Exception) { }

                                try { foreach (var N in covernote.Policy.vehicles) { foreach (var d in N.AuthorizedDrivers) { if (!AuthDrivers.Contains((d.person.fname + " " + d.person.mname + " " + d.person.lname))) { AuthDrivers += d.person.fname + " " + d.person.mname + " " + d.person.lname + " and "; } } } }
                                catch (Exception) { }

                                try { DriverNames = covernote.risk.mainDriver.person.fname + " " + covernote.risk.mainDriver.person.mname + " " + covernote.risk.mainDriver.person.lname; }
                                catch (Exception) { }

                                try { foreach (var N in covernote.risk.ExceptedDrivers) { if (!ExceptedDrivers.Contains((N.person.fname + " " + N.person.mname + " " + N.person.lname))) { ExceptedDrivers += N.person.fname + " " + N.person.mname + " " + N.person.lname + " and "; } } }
                                catch (Exception) { }
                                try { foreach (var N in covernote.risk.ExcludedDrivers) { if (!ExcludedDrivers.Contains((N.person.fname + " " + N.person.mname + " " + N.person.lname))) { ExcludedDrivers += N.person.fname + " " + N.person.mname + " " + N.person.lname + " and "; } } }
                                catch (Exception) { }

                                try { if (PolicyHolderNames.Substring(PolicyHolderNames.Length - 4) == "and ") { PolicyHolderNames = PolicyHolderNames.Substring(0, PolicyHolderNames.Length - 4); } }
                                catch (Exception) { }

                                try { if (DriverNames.Substring(DriverNames.Length - 4) == "and ") { DriverNames = DriverNames.Substring(0, DriverNames.Length - 4); } }
                                catch (Exception) { }
                                try { if (AuthDrivers.Substring(AuthDrivers.Length - 4) == "and ") { AuthDrivers = AuthDrivers.Substring(0, AuthDrivers.Length - 4); } }
                                catch (Exception) { }
                                try { if (ExcludedDrivers.Substring(ExcludedDrivers.Length - 4) == "and ") { ExcludedDrivers = ExcludedDrivers.Substring(0, ExcludedDrivers.Length - 4); } }
                                catch (Exception) { }
                                try { if (ExceptedDrivers.Substring(ExceptedDrivers.Length - 4) == "and ") { ExceptedDrivers = ExceptedDrivers.Substring(0, ExceptedDrivers.Length - 4); } }
                                catch (Exception) { }
                                try { DriverNames = DriverNames.Substring(0, DriverNames.Length - 4); }
                                catch (Exception) { }

                                string MainInsured = ""; try { var mainInsured = covernote.Policy.insured.OrderBy(p => p.PersonID).Take(1).FirstOrDefault(); MainInsured = mainInsured.fname + " " + mainInsured.lname; }
                                catch (Exception) { }

                                try { if (string.IsNullOrEmpty(MainInsured) && !string.IsNullOrEmpty(CompanyNames.Trim())) MainInsured = CompanyNames; }
                                catch (Exception) { }

                                if (DriverNames.Trim() == "") { DriverNames = AuthDrivers; }

                                //Get Wording/Cert Template
                                TemplateWording Word = new TemplateWording();
                                try { Word = TemplateWordingModel.GetWording(covernote.wording.ID); }
                                catch (Exception) { }

                                //Adds Generated CoverNote record here
                                VehicleCoverNoteGenerated vcng = new VehicleCoverNoteGenerated();
                                vcng.Approved = true;

                                try { vcng.authorizedwording = Word.AuthorizedDrivers; }
                                catch (Exception) { }
                                try { vcng.authorizedwording = Word.AuthorizedDrivers; }
                                catch (Exception) { }
                                try { vcng.bodyType = string.IsNullOrEmpty(covernote.risk.bodyType) ? covernote.Policy.vehicles[0].bodyType : covernote.risk.bodyType; }
                                catch (Exception) { }
                                try { vcng.cancelled = covernote.cancelled; }
                                catch (Exception) { }
                                try { vcng.CancelledStatus = covernote.cancelled; }
                                catch (Exception) { }
                                try { vcng.certificateCode = Word.CertificateInsuredCode + ":" + Word.ExtensionCode; }
                                catch (Exception) { }
                                try { vcng.certificateType = Word.CertificateType; }
                                catch (Exception) { }
                                try { vcng.chassisNo = string.IsNullOrEmpty(covernote.risk.chassisno) ? covernote.Policy.vehicles[0].chassisno : covernote.risk.chassisno; }
                                catch (Exception) { }
                                //vcng.certitificateNo = covernote.;
                                try { vcng.companyCreatedBy = Constants.user.employee.company.CompanyName; }
                                catch (Exception) { }
                                try { vcng.CompanyName = covernote.Policy.insuredby.CompanyName; }
                                catch (Exception) { }
                                try { vcng.cover = covernote.Policy.policyCover.cover; }
                                catch (Exception) { }
                                try { vcng.CovernoteNo = covernote.covernoteid; }
                                catch (Exception) { }
                                try { vcng.drivers = DriverNames; }
                                catch (Exception) { }
                                try { vcng.effectivedate = covernote.effectivedate.ToString(); }
                                catch (Exception) { }
                                try { vcng.effectiveTime = covernote.effectivedate.ToShortTimeString(); }
                                catch (Exception) { }
                                try { vcng.endorsementNo = covernote.endorsementno; }
                                catch (Exception) { }
                                try { vcng.engineNo = covernote.endorsementno; }
                                catch (Exception) { }
                                try { vcng.estimatedValue = covernote.risk.estimatedValue; }
                                catch (Exception) { }
                                try { vcng.expirydate = covernote.expirydate.ToString(); }
                                catch (Exception) { }
                                try { vcng.expiryTime = covernote.expirydate.ToShortTimeString(); }
                                catch (Exception) { }
                                try { vcng.HPCC = covernote.risk.HPCCUnitType; }
                                catch (Exception) { }
                                try { vcng.limitsofuse = Word.LimitsOfUse; }
                                catch (Exception) { }
                                try { vcng.Period = covernote.period.ToString(); }
                                catch (Exception) { }
                                try { vcng.PolicyHolders = PolicyHolderNames; }
                                catch (Exception) { }
                                try { vcng.PolicyHoldersAddress = covernote.Policy.mailingAddressShort; }
                                catch (Exception) { }
                                try { vcng.PolicyID = covernote.Policy.ID; }
                                catch (Exception) { }
                                try { vcng.PolicyNo = covernote.Policy.policyNumber; }
                                catch (Exception) { }
                                try { vcng.PolicyTypes = covernote.Policy.insuredType; }
                                catch (Exception) { }
                                try { vcng.referenceNo = covernote.risk.referenceNo; }
                                catch (Exception) { }
                                try { vcng.seating = covernote.risk.seating.ToString(); }
                                catch (Exception) { }
                                try { vcng.Usage = string.IsNullOrEmpty(covernote.risk.usage.usage) ? covernote.Policy.vehicles[0].usage.usage : covernote.risk.usage.usage ; }
                                catch (Exception) { }
                                try { vcng.vehdesc = covernote.risk.VehicleYear + " " + covernote.risk.make + " " + covernote.risk.VehicleModel + " " + covernote.risk.extension; }
                                catch (Exception) { }
                                try { vcng.WordID = covernote.wording.ID; }
                                catch (Exception) { }
                                try { vcng.vehExt = covernote.risk.extension; }
                                catch (Exception) { }
                                try { vcng.VehicleMetaDataID = covernote.risk.ID; }
                                catch (Exception) { }
                                try { vcng.VehicleID = covernote.risk.ID; }
                                catch (Exception) { }
                                try { vcng.vehmake = covernote.risk.make; }
                                catch (Exception) { }
                                try { vcng.vehmodel = covernote.risk.VehicleModel; }
                                catch (Exception) { }
                                try { vcng.vehmodeltype = covernote.risk.modelType; }
                                catch (Exception) { }
                                try { vcng.vehregno = covernote.risk.VehicleRegNumber; }
                                catch (Exception) { }
                                try { vcng.vehyear = covernote.risk.VehicleYear.ToString(); }
                                catch (Exception) { }
                                try { vcng.VIN = covernote.risk.VIN; }
                                catch (Exception) { }
                                try { vcng.ManualCoverNoteNumber = covernote.printedpaperno; }
                                catch (Exception) { }
                                try { vcng.PrintCount = covernote.printcount; }
                                catch (Exception) { }
                                try { vcng.LastPrintedBy = covernote.lastprintflat; }
                                catch (Exception) { }
                                try { vcng.DateLastPrinted = DateTime.Now; }
                                catch (Exception) { }
                                try { vcng.DateFirstPrinted = DateTime.Now; }
                                catch (Exception) { }
                                try { vcng.CompanyID = covernote.Policy.insuredby.CompanyID; }
                                catch (Exception) { }
                                try { vcng.IntermediaryID = Constants.user.employee.company.CompanyID; }
                                catch (Exception) { }
                                try { vcng.limitsofuse = TemplateWordingModel.ReplaceBraceTags(vcng.limitsofuse, DriverNames, AuthDrivers, ExcludedDrivers, ExceptedDrivers, PolicyHolderNames, MainInsured).ToUpper(); }
                                catch (Exception) { }
                                try { vcng.authorizedwording = TemplateWordingModel.ReplaceBraceTags(vcng.authorizedwording, DriverNames, AuthDrivers, ExcludedDrivers, ExceptedDrivers, PolicyHolderNames, MainInsured).ToUpper(); }
                                catch (Exception) { }
                                try { vcng.authorizedwording = TemplateWordingModel.ReplaceBraceTags(vcng.authorizedwording, DriverNames, AuthDrivers, ExcludedDrivers, ExceptedDrivers, PolicyHolderNames, MainInsured).ToUpper(); }
                                catch (Exception) { }
                                try { vcng.PolicyHoldersAddress = PolicyHoldersAddress; }
                                catch (Exception) { }
                                //VehicleCoverNoteGeneratedModel.InsertGenCoverNote(vcng, 0);
                                
                                try
                                {
                                    if (string.IsNullOrEmpty(vcng.CovernoteNo))
                                    {
                                        vcng.CovernoteNo = DateTime.Now.ToString("ddMMyyhhmmss");
                                    }

                                    if (string.IsNullOrEmpty(vcng.ManualCoverNoteNumber))
                                    {
                                        vcng.ManualCoverNoteNumber = vcng.CovernoteNo;
                                    }
                                }
                                catch (Exception)
                                {

                                }

                                try
                                {
                                    vcng.LastPrintedBy = Constants.user.employee.person.fname + " " + Constants.user.employee.person.fname;
                                }
                                catch (Exception)
                                {

                                }

                                id = VehicleCoverNoteGeneratedModel.InsertGenCoverNoteGenerated(vcng, 0, Constants.user.ID);

                                if (id != 0) NoteIDs.Add(id);

                                //Removing Approval of Cover Process Indicated by Mr. Donaldson

                                //bool canAutoApprove = UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "AutoApprove"));
                                //if (canAutoApprove)
                                //    Honeysuckle.Models.VehicleCoverNoteModel.ApproveCoverNote(new VehicleCoverNote(id));
                                //return RedirectToAction("Details", new { id = id });
                            }
                            else
                            {
                                //ViewData["vehid"] = vehicles;
                                ModelState.AddModelError("covernoteno", "This Manual Cover Note No already exists in the system. Please enter one that is not presently in the system");
                                return View(covernote);
                            }
                        }
                        else
                        {
                            //break and go back to view
                            //ModelState.AddModelError("vehicles","This Risk either already has an active cover note during the time period selected or will no longer be covered by the current policy. Please select a more appropriate Risk");
                            ViewData["veherror"] = "Please ensure that the vehicle is covered under the period you have selected"; //mess with view for modals
                            return Json(new { wordingError = 0, result = 0, compShortening = 0, Exists = 1, noteMessage = "<b style = \"color:red;\">Invalid Date Range</b><br/><br/>Please ensure that the vehicle is covered under the period you have selected <b/>Period:<br/>" + covernote.effectivedate.ToString("dd-MMM-yyyy hh:mm:ss ttt") + " To " + covernote.expirydate.ToString("dd-MMM-yyyy hh:mm:ss ttt") + "<br/><br/><a href =\"" + "/Policy/Edit/" + Policy.ID + "\">Click here to go back to the <b>Policy</b> to address this issue.</a>" }, JsonRequestBehavior.AllowGet);

                            //ViewData["vehid"] = vehicles;
                            //return View(covernote);
                        }


                        //int CoverId = id//VehicleCoverNoteModel.AddVehicleCoverNote(vehicleCoverNotes[vcn], polId, PolicyModel.GetCompanyCreatedByPolicy(vehicleCoverNotes[vcn].Policy).CompanyID, true, false);
                        //if (CoverId!= 0) NoteIDs.Add(CoverId);

                        //if (autoApp && CoverId!=0)
                        //{
                        //    VehicleCoverNoteModel.ApproveCoverNote(new VehicleCoverNote(CoverId));
                        //}
                      
                    }

                    return Json(new { wordingError = 0, result = 1, Exists = 0, compShortening = 0, autoApp = autoApp, canPrint = canPrint, canApprove = canApprove, NoteIDs = NoteIDs }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { result = 0, wordingError = 0, compShortening = 0 }, JsonRequestBehavior.AllowGet);

            }
            else return Json(new { result = 0, wordingError = 0, compShortening = 1 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult History(int id = 0, int polid = 0)
        {   
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes","Read")))
            {
                VehicleCoverNote vcn = new VehicleCoverNote();
                vcn.risk = new Vehicle(id);
                vcn.Policy = new Policy(polid);
                return View(vcn);

            }
            else return RedirectToAction("AccessDenied", "Error");
        }

        public ActionResult HistoryPartial(int id = 0,string historyPage = "", int polid = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Read")))
            {
                List<VehicleCoverNote> covers = new List<VehicleCoverNote>();

                
                   
                        if(polid != 0)
                            covers = VehicleCoverNoteModel.CoverNoteHistory(new Vehicle(id),new Policy(polid));
                        else
                            covers = VehicleCoverNoteModel.CoverNoteHistory(new Vehicle(id));
                        
                        ViewData["historyPage"] = historyPage;
                //test if vehicle is under policy
                return View(covers);
            }
            else return new EmptyResult();
        }

        public ActionResult GetcoverNoteDates(int id = 0, int pol = 0)
        {
            if (id != 0 && pol != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Read")))
            {
                List<VehicleCoverNote> covers = new List<VehicleCoverNote>();
                JavaScriptSerializer jsonList = new JavaScriptSerializer();

                covers = VehicleCoverNoteModel.CoverNoteHistory(new Vehicle(id),new Policy(pol));

                foreach (VehicleCoverNote cover in covers)
                {
                    cover.effectivedate.ToString("MM/dd/yyyy hh:mm:ss tt");
                    cover.expirydate.ToString("MM/dd/yyyy hh:mm:ss tt");
                }

                string jsonFile = jsonList.Serialize(covers);
                return Json(new { result = jsonFile }, JsonRequestBehavior.AllowGet);
            }
            else return new EmptyResult();
        }

        /// <summary>
        /// This function allows for mass approval of a number of cover notes
        /// </summary>
        /// <param name="id">Cert id </param>
        public ActionResult MassApproval(string coverNotes = null)
        {
            List<VehicleCoverNote> VehNote = new List<VehicleCoverNote>();
            List<int> policies = new List<int>();
            int i = 0;

            foreach (string s in coverNotes.Split(',').ToList())
            {
                if (int.TryParse(s, out i))
                {
                    VehNote.Add(VehicleCoverNoteModel.GetVehicleCoverNoteByNumber(i));
                }
            }

            return View(VehNote);
        }


        /// <summary>
        /// This function allows for mass printing of a number of cover notes
        /// </summary>
        /// <param name="id">Cert id </param>
        public ActionResult MassPrint(string coverNotes = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Print")))
            {
                List<VehicleCoverNote> VehNote = new List<VehicleCoverNote>();
                List<int> policies = new List<int>();
                int i = 0;

                foreach (string s in coverNotes.Split(',').ToList())
                {
                    if (int.TryParse(s, out i))
                    {
                        VehNote.Add(VehicleCoverNoteModel.GetVehicleCoverNoteByNumber(i));
                    }
                }

                return View(VehNote);
            }
            else return new EmptyResult();
        }

        public ActionResult GetNotesForApproval(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Approve")) && id != 0)
            {
                Policy policy = PolicyModel.getPolicyFromID(id);
                List<int> NoteIds = new List<int>();
                bool canPrint = UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Print"));

                foreach (Vehicle vehicle in policy.vehicles)
                {
                    if (!VehicleCertificateModel.IsVehicleUnderActiveCert(vehicle.ID, policy.startDateTime, policy.endDateTime, policy.ID) && !VehicleCertificateModel.IsPolicyCertMade(vehicle.ID, policy.ID) && VehicleCoverNoteModel.IsInactiveNoteExists(vehicle.ID, policy.ID))
                    {
                        int noteID = VehicleCoverNoteModel.GetThisInactiveNote(vehicle.ID, policy.ID).ID;
                        if (noteID != 0) NoteIds.Add(noteID);
                    }
                }

                return Json(new { result = 1, NoteIds = NoteIds, canPrint = canPrint }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetNotesForPrinting(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Print")) && id != 0)
            {
                Policy policy = PolicyModel.getPolicyFromID(id);
                List<int> NoteIds = new List<int>();

                foreach (Vehicle vehicle in policy.vehicles)
                {
                    if (VehicleCoverNoteModel.IsActiveCoverNoteExist(vehicle.ID, policy.ID))
                    {
                       int noteId = VehicleCoverNoteModel.GetActiveCoverNote(vehicle.ID, policy.startDateTime.ToString(), policy.endDateTime.ToString(), policy.ID).ID;
                       if(noteId != 0) NoteIds.Add(noteId);
                    }
                }

                return Json(new { result = 1, NoteIds = NoteIds}, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Read")))
            {
                Company company = CompanyModel.GetMyCompany(Constants.user);
                List<VehicleCoverNote> covernotes = VehicleCoverNoteModel.CoverNoteEffectiveList(company, DateTime.Now.ToString("MMMM", CultureInfo.InvariantCulture), DateTime.Now.ToString("yyy", CultureInfo.InvariantCulture));
                return View(covernotes);
            }
            else return RedirectToAction("Error", "AccessDenied");
        }

    }
}
