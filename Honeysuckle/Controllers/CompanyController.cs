﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Honeysuckle.Controllers
{
    public class CompanyController : Controller
    {

        public ActionResult AuditCompanyPolicyHolder()
        {
            return View();
        }

        public ActionResult AuditCompany()
        {
            return View();
        }

        public ActionResult CompanyListItemSelected(int id = 0)
        {
            if (id != 0) return View(CompanyModel.GetCompany(id, true));
            return new EmptyResult();
        }

        public ActionResult TestIAJID(int id = 0, string iajid = null)
        {
            if (id != 0 && iajid != null) return Json(new { result = CompanyModel.CheckCompanyByIAJID(iajid, id) }, JsonRequestBehavior.AllowGet);
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetCompanyTRN(string TRN = null,int compID = 0,bool insured = true,bool test = false)
        {
            bool duplicates = false;
            if (TRN != null) 
            {
                if (TRN == "1000000000000" && test) return Json(new { result = 0,error = 1,invalid = 1 }, JsonRequestBehavior.AllowGet);
                Company comp = CompanyModel.GetCompanyByTRN(TRN, compID , insured);
                duplicates = comp.duplicate == true ? true : false;
                if (comp.CompanyID != 0)
                    {
                        comp = CompanyModel.GetCompany(comp.CompanyID, true);

                        if (comp.CompanyAddress.longAddressUsed)
                            return Json(new { result = 1, compId = comp.CompanyID, compname = comp.CompanyName, comptrn = comp.trn, longAddress = comp.CompanyAddress.longAddress, longAddressUsed = comp.CompanyAddress.longAddressUsed, duplicate = comp.duplicate }, JsonRequestBehavior.AllowGet);
                        else
                    return Json(new { 
                            result = 1, 
                            compId = comp.CompanyID,
                            compname = comp.CompanyName,
                            comptrn = comp.trn,
                            roadno = comp.CompanyAddress.roadnumber,
                            aptNumber = comp.CompanyAddress.ApartmentNumber,
                            roadname = comp.CompanyAddress.road.RoadName,
                            roadtype = comp.CompanyAddress.roadtype.RoadTypeName,
                            zipcode = comp.CompanyAddress.zipcode.ZipCodeName,
                            city = comp.CompanyAddress.city.CityName,
                            country = comp.CompanyAddress.country.CountryName,
                            parishid = comp.CompanyAddress.parish.ID,
                            parish = comp.CompanyAddress.parish.parish,
                            duplicate = duplicates

                        }, JsonRequestBehavior.AllowGet);
                    }
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }
        

        public ActionResult UserList(int id = 0)
        {
            bool allow = false;

            if (Constants.user.newRoleMode)
            {
                allow = (((UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Read")) && Constants.user.tempUserGroup.company.CompanyID == id) || (Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user) || Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user))) && id != 0);
            }
            else allow = (((UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Read")) && CompanyModel.GetMyCompany(Constants.user).CompanyID == id) || (Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user) || Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user))) && id != 0);
          
            if (allow)
            {
                ViewData["cname"] = CompanyModel.GetCompany(id).CompanyName;
                ViewData["cid"] = id;
                return View(UserModel.GetCompUsers(id));
            }
            else return RedirectToAction("AccessDenied", "Error");
        }
        
        public ActionResult CompanyMultiSelect()
        {
            if (UserGroupModel.AmAnAdministrator(Constants.user)) return View(CompanyModel.GetAllCompanies());
            else return new EmptyResult();
        }

        
        /// <summary>
        /// This function renders the email partial
        /// </summary>
        /// <param name="email">The email that is added</param>
        public ActionResult Email(Email email = null)
        {
            if (email != null) return RedirectToAction("NewEmail", "Email", new { id = email.emailID });
            else return RedirectToAction("NewEmail", "Email", new { id = 0 });
        }


        /// <summary>
        /// Generates a partial drop down list with all companies
        /// </summary>
        public ActionResult CompanyDropDown(string type= null, bool notDisabled = false)
        {
            List<UserPermission> permissions = new List<UserPermission>();
            permissions.Add(new UserPermission("Company", "Read"));
            permissions.Add(new UserPermission("Policies", "Read"));
            permissions.Add(new UserPermission("Policies", "Create"));

            if (UserModel.TestUserPermissions(Constants.user, permissions))
            {
                switch (type)
                {
                    case "InsuranceCompanies":
                        return View(CompanyModel.GetInsurers(notDisabled));
                    case "IntermediaryCompanies":
                        List<Company> companies = new List<Company>();
                        companies.Add (new Company());
                        if (Constants.user.newRoleMode) //If user has a temp role
                        {
                            companies.Add(Constants.user.tempUserGroup.company); //the current user's company
                            companies.AddRange(CompanyModel.GetIntermediates(Constants.user.tempUserGroup.company.CompanyID)); //the intermediaries
                        }
                        else
                        {
                            companies.Add(CompanyModel.GetMyCompany(Constants.user)); //the current user's company
                            companies.AddRange(CompanyModel.GetIntermediates(CompanyModel.GetMyCompany(Constants.user).CompanyID)); //the intermediaries
                        }
                        ViewData["source"] = true;
                        return View(companies);
                    case "InsureAndInterComp":
                        List<Company> companyList = new List<Company>();
                        companyList.AddRange(CompanyModel.IntermediaryList());
                        companyList.AddRange(CompanyModel.InsurersList());
                        return View(companyList);
                    default:
                        return View(CompanyModel.GetAllCompanies());
                }
            }
            else return new EmptyResult(); // return empty if not secure
        }

        /// <summary>
        /// This returns a partial view with the company of a particular employee details
        /// </summary>
        public ActionResult EmployeeCompany(int compId = 0)
        {
            if (compId != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read"))) return View(CompanyModel.GetCompanyName(new Company(compId)));
            else return View(new Company());

        }

        /// <summary>
        /// This function is used to display the information of the company that employs the current user of the system
        /// </summary>
        /// <param name="id">Company Id</param>
        public ActionResult CompanyRead(int id = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read"))) return View(CompanyModel.GetCompanyName(new Company(id)));
            else 
            {
                if (Constants.user.username != "not initialized")
                {
                    if (Constants.user.employee != null) if (Constants.user.employee.company != null) if (Constants.user.employee.company.CompanyID != 0)
                            {
                                return View(CompanyModel.GetCompanyName(Constants.user.employee.company));
                            }
                            else return View(new Company());
                        else return View(new Company());
                    else return View(new Company());
                }
                else  return View(new Company()); 
            }
        }
        
        /// <summary>
        /// view for editing of company details
        /// </summary>
        public ActionResult Edit(int id = 0)
        {
            Constants.user.UserGroups = UserModel.GetGroupsAndPermissions(Constants.user.ID);
            bool allow = false;
            Company comp = new Company();

            if (Constants.user.newRoleMode)
            {
                if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Update")) && (Constants.user.tempUserGroup.company.CompanyID == id || CompanyModel.IsCompanyRegular(id) || UserGroupModel.AmAnIAJAdministrator(Constants.user) || UserGroupModel.AmAnAdministrator(Constants.user)) && id != 0)
                   allow = true;
            }

            else if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Update")) && (CompanyModel.GetMyCompany(Constants.user).CompanyID == id || CompanyModel.IsCompanyRegular(id) || UserGroupModel.AmAnIAJAdministrator(Constants.user) || UserGroupModel.AmAnAdministrator(Constants.user)) && id != 0)
                  allow = true;

            comp = CompanyModel.GetCompany(id);
            ViewData["TRNnotAvailable"] = false;
            if (comp.trn == "1000000000000") ViewData["TRNnotAvailable"] = true;

            if (allow) return View(comp);
            else return RedirectToAction("AccessDenied", "Error");
        }
        
        /// <summary>
        /// postback for edit view of Company data
        /// </summary>
        [HttpPost]
        public ActionResult Edit(Company company, int id = 0, string enable = null, int parishes = 0, string TRNnotAvailable = null)
        {
            Constants.user.UserGroups = UserModel.GetGroupsAndPermissions(Constants.user.ID);
            bool allow = false;

            ViewData["TRNnotAvailable"] = false;
            if (company.trn == "1000000000000") ViewData["TRNnotAvailable"] = true;
            if (TRNnotAvailable != null && TRNnotAvailable != "") company.trn = TRNnotAvailable;

            if (Constants.user.newRoleMode)
            {
                if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Update")) && (Constants.user.tempUserGroup.company.CompanyID == id || CompanyModel.IsCompanyRegular(id) || UserGroupModel.AmAnIAJAdministrator(Constants.user) || UserGroupModel.AmAnAdministrator(Constants.user)) && id != 0)
                    allow = true;
            }

            else if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Update")) && (CompanyModel.GetMyCompany(Constants.user).CompanyID == id || CompanyModel.IsCompanyRegular(id) || UserGroupModel.AmAnIAJAdministrator(Constants.user) || UserGroupModel.AmAnAdministrator(Constants.user)) && id != 0)
                allow = true;

            if (allow)
            {
                if (CompanyModel.GetCompanyByTRN(company.trn, company.CompanyID).CompanyID == 0)
                {

                    company.CompanyAddress = AddressModel.TrimAddress(company.CompanyAddress);
                    if (parishes != 0) company.CompanyAddress.parish = new Parish(parishes);
                    else if(!company.CompanyAddress.longAddressUsed)
                    {
                        ModelState.AddModelError("parish", "Please select a parish");
                        return View(company);
                    }

                    ModelState.Remove("CompanyAddress.parish.ID");

                    if (ModelState.IsValid)
                    {
                        switch (enable)
                        {
                            case "enabled":
                                company.Enabled = true;
                                break;
                            case "disabled":
                                company.Enabled = false;
                                break;
                            default:
                                company.Enabled = false;
                                break;
                        }

                        company.CompanyID = id;
                        CompanyModel.UpdateCompany(company);

                        TempData["EmailSent"] = "Company Modified!";
                        return RedirectToAction("Index", "Home");
                    }
                    else return View(company);
                }
               else
               {
                  ViewData["TRN Exists"] = "TRN already exists for another company. The Company TRN must be unique.";
                  return View(company);
               }
            }
            else
            {

                TempData["permissionMessage"] = "You do not have any permission to access this page";
                return RedirectToAction("Index", "Home");
            }
        }


        public ActionResult CompaniesList_old() {return View();}

        /// <summary>
        /// This function is used to display a list of companies
        /// </summary>
        public ActionResult CompaniesList()
        {
            
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read")))
            {
                if (Constants.user.ID != 0) return View(CompanyModel.MyCompanyList());
                else return new EmptyResult();
            }
            else return new EmptyResult();
        }

        /// <summary>
        /// Get Company Intermediaries
        /// </summary>
        /// <returns></returns>
        public JArray GetMyIntermediaries() 
        {
            JArray IM = new JArray();

           

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read")))
            {
                if (Constants.user.ID != 0) return JArray.Parse(JsonConvert.SerializeObject(CompanyModel.MyCompanyListMODIFIED()));
                else return IM;
            }
            else 
            { 
                return IM; 
            }

        }


        /// <summary>
        /// Gets default auth/permission values for home screen
        /// </summary>
        /// <returns></returns>
        public JObject HomeValues() {

            JObject response = new JObject();

            response["company_update"] = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Update"));
            response["company_delete"] = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Delete"));
            response["company_enable"] = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Enable"));
            if (Honeysuckle.Models.Constants.user.employee == null) Honeysuckle.Models.Constants.user.employee = new Honeysuckle.Models.Employee();
            Honeysuckle.Models.Constants.user.employee.company = Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user);
            response["am_administrator"] = Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user);
            response["am_iaj_administrator"] = Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user);

            response["CompanyID"] = Honeysuckle.Models.Constants.user.employee.company.CompanyID;
            response["success"] = true;


            return response;
        
        }


        /// <summary>
        /// Checks if the current user has permission to edit a company
        /// </summary>
        /// <param name="company_update"></param>
        /// <param name="am_administrator"></param>
        /// <param name="am_iaj_administrator"></param>
        /// <returns></returns>
        public JObject CheckIsAdmin(int CompanyID) {
            JObject response = new JObject();

            bool company_update = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Update"));
            bool company_delete = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Delete"));
            bool company_enable = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Enable"));
            if (Honeysuckle.Models.Constants.user.employee == null) Honeysuckle.Models.Constants.user.employee = new Honeysuckle.Models.Employee();
            Honeysuckle.Models.Constants.user.employee.company = Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user);
            bool am_administrator = Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user);
            bool am_iaj_administrator = Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user);

            response["success"] = false;
            if (company_update && Honeysuckle.Models.Constants.user.employee.company.CompanyID == CompanyID || am_administrator || am_iaj_administrator)
            {
                response["success"] = true;
            }
            return response;
        }


        public ActionResult Enable(int id = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Enable")))
            {
                CompanyModel.EnableCompany(new Company(id));
                return Json(new {result = 1}, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Disable(int id = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Enable")))
            {
                CompanyModel.DisableCompany(new Company(id));
                return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function is used to display a blank company form- to create a new company
        /// </summary>
        public ActionResult Create()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Create"))) return View();
            else
            {

                TempData["permissionMessage"] = "You do not have any permission to access this page";
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// This function is used to create a new company -postback
        /// </summary>
        /// <param name="company">Company object- with new company's information </param>
        [HttpPost]
        public ActionResult Create(Company company, string enable = null, int parishes = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Create")))
            { 
                switch (enable)
                {
                    case "enabled":
                        company.Enabled = true;
                        break;
                    case "disabled":
                        company.Enabled = false;
                        break;
                    default:
                        company.Enabled = false;
                        break;
                }

                ModelState.Remove("CompanyAddress.parish.ID");
                company.trn = company.trn.Trim();
                company.CompanyAddress = AddressModel.TrimAddress(company.CompanyAddress);

                if (parishes != 0) company.CompanyAddress.parish = new Parish(parishes);
                else
                {
                    ModelState.AddModelError("parish", "Please select a parish");
                    return View(company);
                }

                if (ModelState.IsValid)
                {
                    if (CompanyModel.AddCompany(company))
                    {
                        KPIModel.InitializeKPI(company, "enquiry");
                        KPIModel.InitializeKPI(company, "covernote");
                        KPIModel.InitializeKPI(company, "certificate");
                        KPIModel.InitializeKPI(company, "error");
                        KPIModel.InitializeKPI(company, "doubleInsured");

                        TempData["EmailSent"] = "Company Created!";
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        if(company.trn == "1000000000000")
                            ModelState.AddModelError("trn", "This TRN is not valid!");
                        else{
                            ModelState.AddModelError("trn", "That company already exists. The Company TRN and IAJID must be unique.");
                            ModelState.AddModelError("IAJID", " ");
                        }
                        return View(company);
                    }
                }
                else return View(company);
             }
            else
            {
                return RedirectToAction("Index","Home");
            }
        }


        /// <summary>
        /// This function is used to delete a company from the system
        /// </summary>
        /// <param name="id">Company Id</param>
        public ActionResult Delete(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Delete")) && id != 0)
            {
                if (CompanyModel.DeleteCompany(id)) return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function is used to display the details of a particular company
        /// </summary>
        /// <param name="id">Company Id</param>
        public ActionResult Details(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read")) && id != 0 || UserGroupModel.AmAnAdministrator(Constants.user))
            {
                Company company = CompanyModel.GetCompany(id);
                return View(company);
            }
            else return RedirectToAction("AccessDenied","Error");
        }


        /// <summary>
        /// This function is used to auto fill the company form with company types, based on what the user enters
        /// </summary>
        /// <param name="type">The data entered in the form by the user</param>
        public ActionResult GetCompanyTypes(string type = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read")))
            {
                List<string> result = new List<string>();
                CompanyModel cm = new CompanyModel();
                result = cm.GetCompanyTypes();

                var json = result.Where(x => x.StartsWith(type))
                                 .OrderBy(x => x)
                                 .Select(r => new { value = r });

                return Json(json, JsonRequestBehavior.AllowGet);
            }
            return Json(new { fail = 1, message = "You do not have permissions to use this function" }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function is used to auto fill the company form, based on what the user enters
        /// </summary>
        /// <param name="type">The data entered in the form by the user</param>
        public ActionResult GetCompanies(string type)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read")))
            {
                List<string> result = new List<string>();
                result = CompanyModel.GetCompanyNames();

                var json = result.Where(x => x.StartsWith(type))
                                 .OrderBy(x => x)
                                 .Select(r => new { value = r });

                return Json(json, JsonRequestBehavior.AllowGet);
            }
            return Json(new { fail = 1, message = "You do not have permissions to use this function" }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This function returns  a list of all companies in the system
        /// </summary>
        public ActionResult companyList(int id = 0, string compType = null, string Searchtext = "")
        {
            List<Company> companies = new List<Company>();
            ViewData["inter"] = false;
            return View(companies);

            //switch(compType)
            //{
            //    case "1":
            //        if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read")))
            //        {
            //            ViewData["inter"] = true;
            //            return View(CompanyModel.IntermediaryList());
            //        }
            //        else return Json(new { fail = 1 }, JsonRequestBehavior.AllowGet);
            //    default:
            //        if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read")))
            //        {
            //            ViewData["inter"] = false;
            //            if (id != 0)
            //            {
            //                if (Constants.user.newRoleMode) return View(CompanyModel.GetCompaniesIveInsuredBefore(Constants.user.tempUserGroup.company));
            //                else return View(CompanyModel.GetCompaniesIveInsuredBefore(new Company(id),Searchtext));
            //            }
            //            else return Json(new { fail = 1 }, JsonRequestBehavior.AllowGet);
            //        }
            //        else return Json(new { fail = 1 }, JsonRequestBehavior.AllowGet);
            //}
           
        }


        public JObject GetcompanyList(int id = 0, string compType = null, string Searchtext = "")
        {
            JObject Response = new JObject();
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read")))
            {
                ViewData["inter"] = false;
                if (id != 0)
                {
                    if (Constants.user.newRoleMode)
                    {
                        Response.Add("Comp", JArray.FromObject(CompanyModel.GetCompaniesIveInsuredBefore(Constants.user.CompanyID, Searchtext)));
                        return Response;
                    }
                    else 
                    {
                        Response.Add("Comp", JArray.FromObject(CompanyModel.GetCompaniesIveInsuredBefore(Constants.user.CompanyID, Searchtext)));
                        return Response;
                       
                    }
                }
                else return Response;
            }
            else return Response;

        }


        /// <summary>
        /// This function is used to diaplay a company's information
        /// </summary>
        /// <param name="id">Company Id</param>
        public ActionResult CompanyView(int CompanyIdNum = 0, string type = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read")))
            {
                if (type == "details") ViewData["compDetails"] = true;
                else ViewData["compDetails"] = false;

                Company company = new Company();
                if (CompanyIdNum != 0) company = CompanyModel.GetCompany(CompanyIdNum, true);

                ViewData["companyName"] = company.CompanyName;
              //  ViewData["inter"] = intermediaries;
                return View(company);
            }
            else return View(new Company());
        }

        /// <summary>
        /// DISPLAYS EMPTY PARTIAL
        /// </summary>
        /// <returns></returns>
        public ActionResult AddCompanies()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Create")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read"))) 
                return View();
            else 
                return Json(new { result = 0, fail = 1, message = "You do not have access to this function" }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function is used to add a new company -from jquery dialog
        /// </summary>
        public ActionResult AddNewCompany(string CompanyName, string TRN, string roadno, string aptNumber, string road,
                                       string roadType, string zipcode, string city, string parishes, string country)
       
        {
            
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Create"))) 
            {
                Company comp = new Company();

                comp.floginlimit = 0; //set to zero for now. 
                comp.CompanyAddress = new Address(0, roadno, aptNumber, new Road(road), new RoadType(roadType), new ZipCode(zipcode), new City(city), new Parish(int.Parse(parishes)), new Country(country));
                comp.CompanyName = CompanyName;
                comp.trn = TRN;

                comp.CompanyAddress = AddressModel.TrimAddress(comp.CompanyAddress);

                if (CompanyModel.AddCompany(comp, true)) return Json(new { result = 1, compId = comp.CompanyID }, JsonRequestBehavior.AllowGet);

                else return Json(new { result = 0}, JsonRequestBehavior.AllowGet);

            }
            else return Json(new { result = 0, fail = 1, message = "You do not have access to this function" }, JsonRequestBehavior.AllowGet);

        }


        /// <summary>
        /// This function is used to check if a company exists , given its IAJID
        /// </summary>
        /// <param name="id">Company IAJID</param>
        public ActionResult GetCompanyByIAJID(string id = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read")) && id != null)
            {
                //int iajid = int.Parse(id);
                if (CompanyModel.CheckCompanyByIAJID(id)) return Json(new { error = 0, result = 1 }, JsonRequestBehavior.AllowGet);
                else return Json(new { error = 0, result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { fail = 1, message = "You do not have access to this function"}, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ReturnCompanyByIAJID(string iajid = null)
        {
            Company comp = new Company();
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read")) && iajid != null)
            {
                comp = CompanyModel.GetCompanyByIAJID(iajid);
                if (comp.CompanyID != 0) return Json(new { error = 0, cid = comp.CompanyID, result = 1 }, JsonRequestBehavior.AllowGet);
                else return Json(new { error = 0, result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { fail = 1, message = "You do not have access to this function" }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function is used to retieves's the company setting, for the company of the current user
        /// </summary>
        /// <returns>Company Object</returns>
        public ActionResult Settings()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Settings")) && false) //locking out for now
            {
                 Company company= new Company();
                if (Constants.user.newRoleMode)
                     company = CompanyModel.GetCompany(UserModel.GetUser(Constants.user.tempUserGroup.company.CompanyID).employee.company.CompanyID);
                else company = CompanyModel.GetCompany(UserModel.GetUser(Constants.user.ID).employee.company.CompanyID);
                return View(company);
            }

            else
            {

                TempData["permissionMessage"] = "You do not have any permission to access this page";
                return RedirectToAction("Index", "Home");
            }
        }


        /// <summary>
        /// This function is used to update the company setting, for the company of the current user
        /// </summary>
        /// <returns>Company Object</returns>
        [HttpPost]
        public ActionResult Settings(Company company, IEnumerable<Address> address = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company","Settings")))
            {
                Company comp = new Company();
                company.CompanyAddress = address.ToList()[0];

                if (ModelState.IsValid)
                {
                    CompanyModel.UpdateCompany(company);
                    TempData["EmailSent"] = "Company setting saved!";
                    return RedirectToAction("Index", "Home");
                }
                else return View(company);
            }
            else
            {

                TempData["permissionMessage"] = "You do not have any permission to access this page";
                return RedirectToAction("Index", "Home");
            }
        }



        /// <summary>
        /// This function is used to retieves's a company's information, given the IAJID
        /// </summary>
        public ActionResult FullCompanyDetails(string id = null, bool create = false)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read")) && id != null)
            {
                if (id != null)
                {
                    if (create) ViewData["HeadingMessage"] = "Would you like to add this company: ";
                    else ViewData["HeadingMessage"] = "Company Details: ";

                    return View(CompanyModel.GetCompany(int.Parse(id), true));
                }
                else return new EmptyResult();
            }
            else return new EmptyResult();
        }


        /// <summary>
        /// This function is used to add contact-info for a company
        /// </summary>
        /// <param name="compId">Company Id</param>
        public ActionResult CompanyContact(int compId = 0)
        {
            if (UserGroupModel.TestUserGroup(Constants.user, (new UserGroup("Company Administrator"))) || (UserGroupModel.AmAnIAJAdministrator(Constants.user)))
            {
                Company company = new Company();
                if (compId != 0) company.CompanyID = compId;
                return View(company);
                
            }
            else return View(new Company());
        }


        /// <summary>
        /// This function is used to add contact info for a company - postback
        /// </summary>
        [HttpPost]
        public ActionResult CompanyContact(Company company, List<User> user = null)
        {
            if (UserGroupModel.TestUserGroup(Constants.user, (new UserGroup("Company Administrator"))) || (UserGroupModel.AmAnIAJAdministrator(Constants.user)))
            {
                ModelState.Remove("password");
                ModelState.Remove("confirm_password");
                PhoneModel.AddPhoneToCompUser(company, user);
                TempData["EmailSent"] = "Company contacts saved";
                return RedirectToAction("Index", "Home");
            }
            else return RedirectToAction ("Index", "Home");
        }


        /// <summary>
        /// This function is used to add contact info for a company - with data  sent from javascript dialog
        /// </summary>
        public ActionResult CompanyContactInfo(string compId, List<string> email = null, List<bool> primary = null, List<string> numbers = null,
                                                List<string> ext = null, List<bool> primaryPhone = null)
        {

            if (!UserGroupModel.TestUserGroup(Constants.user, (new UserGroup("Company Administrator"))) && (!UserGroupModel.AmAnIAJAdministrator(Constants.user)))
            {
                Company company = new Company();
                company.CompanyID = int.Parse(compId);

                // Adding emails to email model, and company model
                List<Email> emails = new List<Email>();

                if (email != null)
                    if (email.Count != 0)
                        for (int i = 0; i < primary.Count; i++)
                        {
                            emails.Add(new Email(email[i], primary[i]));
                        }
                company.emails = emails;


                // Adding phone numbers to phone model, and company model
                List<Phone> phones = new List<Phone>();

                if (numbers != null)
                    if (numbers.Count != 0)
                        for (int i = 0; i < numbers.Count; i++)
                        {
                            phones.Add(new Phone(numbers[i], ext[i], primaryPhone[i]));
                        }
                company.phoneNums = phones;


                EmailModel.AddEmailtoCompany(company);
                PhoneModel.AddPhoneNumToComp(company);

                return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
       
            }
            return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function deals with the editing of a company's contacts
        /// </summary>
        /// <param name="id">Vehicle's id </param>
        public ActionResult EditCompContactInfo(int id = 0)
        {
            if (!CompanyModel.IsCompanyRegular(id)) return RedirectToAction("Index", "Home");

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read")) || UserGroupModel.AmAnAdministrator(Constants.user))
            {
                Company comp = new Company();
                comp.CompanyID = id;
                CompanyModel.GetCompanyName(comp);
                //Gets the list of emails and phone numbers belonging to a company
                comp.emails = EmailModel.GetCompanyEmails(comp);
                comp.phoneNums = PhoneModel.GetCompanyPhones(comp);

                return View(comp);
            }
            else
            {

                TempData["permissionMessage"] = "You do not have any permission to access this page";
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// This function deals with the editing of a company's contacts
        /// </summary>
        [HttpPost]
        public ActionResult EditCompContactInfo(Company company)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read")) || UserGroupModel.AmAnIAJAdministrator(Constants.user))
            {
                ModelState.Remove("IAJID");
                //bool emptyPhoneNum = false;
                //bool PrimaryContactSet = false;

                if (ModelState.IsValid)
                {

                    //Removing all emails and phone-numbers associated with the company
                    EmailModel.DropEmailsFromCompany(company);
                    PhoneModel.DropPhonesFromCompany(company);

                    //adding the new/edited emails and phone numbers
                    EmailModel.AddEmailtoCompany(company);
                    PhoneModel.AddPhoneNumToComp(company);

                    TempData["EmailSent"] = "Company contacts saved";
                    return RedirectToAction("Index", "Home");
                }
                else return View(company);
            }
            else
            {

                TempData["permissionMessage"] = "You do not have any permission to access this page";
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// This function deals with the  adding of company contact-info to a vehicle
        /// </summary>
        public ActionResult compContacts(string id = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Create")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Create")))
            {
                Company company = new Company();
                if (id != null) company = CompanyModel.GetCompany(int.Parse(id), true); 
                return View(company);
            }
            else return View(new Company());
        }

        /// <summary>
        /// This function deals with the editing of a company's contacts
        /// </summary>
        /// <param name="id">Vehicle's id </param>
        public ActionResult ViewContactInfo(int id = 0)
        {
            if (!CompanyModel.IsCompanyRegular(id)) return RedirectToAction("Index", "Home");
            if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read"))) && id != 0)
            {
                Company comp = new Company();
                comp.CompanyID = id;

                //Gets the list of emails and phone numbers belonging to a company
                comp.emails = EmailModel.GetCompanyEmails(comp);
                comp.phoneNums = PhoneModel.GetCompanyPhones(comp);

                return View(comp);
            }
            else
            {

                TempData["permissionMessage"] = "You do not have any permission to access this page";
                return RedirectToAction("Index", "Home");
            }
        }


        /// <summary>
        /// This function deals with the editing of a company's contacts
        /// </summary>
        /// <param name="id">Vehicle's id </param>
        public ActionResult ViewCompContactInfo(int id = 0)
        {
            if (CompanyModel.IsCompanyRegular(id)) return RedirectToAction("Index", "Home");
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read")) && id != 0) //edit permission
            {
                Company comp = new Company();
                comp.CompanyID = id;

                //Gets the list of emails and phone numbers belonging to a company
                comp= CompanyModel.GetCompanyContact(id);

                return View(comp);
            }
            else
            {

                TempData["permissionMessage"] = "You do not have any permission to access this page";
                return RedirectToAction("Index", "Home");
            }
        }


        /// <summary>
        /// This function deals with the editing of a company's contacts
        /// </summary>
        /// <param name="id">Vehicle's id </param>
        public ActionResult EditCompContact(int id = 0)
        {
            if (CompanyModel.IsCompanyRegular(id)) return RedirectToAction("Index", "Home");
            bool allow = false;

            if (Constants.user.newRoleMode)
            {
                if ((Constants.user.tempUserGroup.company.CompanyID == id && UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "EditInfo"))) || UserGroupModel.AmAnAdministrator(Constants.user))
                    allow = true;
            }

            else if ((UserModel.GetUser(Constants.user.ID).employee.company.CompanyID == id && UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "EditInfo"))) || UserGroupModel.AmAnAdministrator(Constants.user))
                     allow = true;

            if (allow)
              {
                Company comp = new Company();

                //Gets the list of emails and phone numbers belonging to a company
                comp = CompanyModel.GetCompanyContact(id);
                comp.CompanyID = id;

                return View(comp);
              }
            else
            {

                TempData["permissionMessage"] = "You do not have any permission to access this page";
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// This function is used to add contact info for a company - postback
        /// </summary>
        [HttpPost]
        public ActionResult EditCompContact(Company company, List<User> user = null)
        {
           bool allow = false;
           bool emptyPhoneNum = false;
           bool PrimaryContactSet = false;
           List<Email> email = new List<Email>();

           if (Constants.user.newRoleMode)
           {
              if ((Constants.user.tempUserGroup.company.CompanyID == company.CompanyID && UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "EditInfo"))) || UserGroupModel.AmAnAdministrator(Constants.user))
                 allow = true;
           }

           else if ((UserModel.GetUser(Constants.user.ID).employee.company.CompanyID == company.CompanyID && UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "EditInfo"))) || UserGroupModel.AmAnAdministrator(Constants.user))
                 allow = true;

           if (allow)
           {
               //This part removes the model state errors for the users' password and password confirm fields
                foreach (var modelValue in ModelState.Values)
                {
                    if (modelValue.Value == null) modelValue.Errors.Clear();
                }

                if (user != null)
                {
                    for (int x = 0; x < user.Count(); x++)
                    {
                        email.Add(new Email(user[x].username));
                        company.emails = email;

                        if (company.phoneNums[x].PhoneNumber == "" || company.phoneNums[x].PhoneNumber == null ) emptyPhoneNum = true;
                        if (company.phoneNums[x].primary) PrimaryContactSet = true;
                    }

                    //Ensuring that no phone field is left blank
                    if (emptyPhoneNum)
                    {
                        ViewData["PhoneError"] = "Phone Number field can not be left empty!";
                        return View(company);
                    }

                    //Ensuring that a primary contact is selected
                    if (!PrimaryContactSet)
                    {
                        ViewData["PhoneError"] = "A primary contact must be selected!";
                        return View(company);
                    }
                }
              // ModelState.Remove("IAJID");

                if (ModelState.IsValid)
                {
                    
                    PhoneModel.DropPhonesFromCompany(company);
                    PhoneModel.AddPhoneToCompUser(company, user);

                    for(int i=0; i < user.Count(); i++){
                        if (user[i].employee.person.PersonID != 0)
                            PhoneModel.EditPersonPhone(user[i].employee.person, company.phoneNums[i]);
                    }

                    TempData["EmailSent"] = "Company contacts saved";
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewData["PhoneError"] = "Invalid data has been entered!";
                    return View(company);
                }
            }
           else
           {

               TempData["permissionMessage"] = "You do not have any permission to access this page";
               return RedirectToAction("Index", "Home");
           }
        }


        /// <summary>
        /// This function is used to test if a company's shortening code is set
        /// </summary>
        /// <param name="id">Company Id</param>
        public ActionResult IsCompShortSet(int id = 0)
        {
            if (id != 0)
            {
                if (CompanyModel.IsCompanyShortSet(new Company(id)))
                     return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult CompanyData()
        {
            if(UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read")) ||
                UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Read")) ||
                UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Read")) ||
                UserModel.TestUserPermissions(Constants.user, new UserPermission("People", "Read")) ||
                UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read")) ||
                UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Read")))
            {
            ViewData["CompanyName"] = CompanyModel.GetMyCompany(Constants.user).CompanyName;
            return View();
            }
            else
            {
               return RedirectToAction("Index","Home");
            }
        }

            private bool IsValidDate(DateTime start) {
            try
            {
                    DateTime D = new DateTime();
                    DateTime.TryParse(start.ToString(), out D);

                    if (D.Year <= 1969) return false; else return true;
            }
            catch (Exception)
            {
                return false;
            }
               
        }

        public ActionResult ShortCoverNote(DateTime start,DateTime end, string typeOfDate = null)
        {
            try 
            {
                if (IsValidDate(start)) { 
                     Company comp = CompanyModel.GetMyCompany(Constants.user);
                    if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Read"))) return View(VehicleCoverNoteModel.GetCompanyCoverNotes(comp.CompanyID, start, end, typeOfDate));
                    else return new EmptyResult();
                } else return new EmptyResult();
               
            }
            catch (Exception) { return new EmptyResult(); }

           
           
        }

        public ActionResult ShortCertificate(DateTime start, DateTime end, string typeOfDate = null)
        {
            try
            {
                if (IsValidDate(start))
                {
                    Company comp = CompanyModel.GetMyCompany(Constants.user);
                    if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Read"))) return View(VehicleCertificateModel.GetCompanyCertificates(comp.CompanyID, start, end, typeOfDate));
                    else return new EmptyResult();
                }
                else return new EmptyResult();

               
            }
            catch (Exception)
            {
                 return new EmptyResult();
               
            }
              

        }

        public ActionResult ShortVehicle(DateTime start, DateTime end, string typeOfDate = null)
        {
           
            try
            {
                if (IsValidDate(start))
                {
                    Company comp = CompanyModel.GetMyCompany(Constants.user);
                    if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read"))) return View(VehicleModel.GetCompanyVehicles(comp.CompanyID, start, end, typeOfDate));
                    else return new EmptyResult();
                }
                else return new EmptyResult();
                
            }
            catch (Exception)
            {

                return new EmptyResult();
            }
            
        }
        public ActionResult ShortPolicies(DateTime start, DateTime end, string typeOfDate = null)
        {
            try
            {
                if (IsValidDate(start))
                {
                    Company comp = CompanyModel.GetMyCompany(Constants.user);
                    if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Read"))) return View(PolicyModel.GetCompanyPolicies(comp.CompanyID, start, end, typeOfDate));
                    else return new EmptyResult();
                }
                else return new EmptyResult();
                
            }
            catch (Exception)
            {

                return new EmptyResult();
            }

           
        }
        public ActionResult ShortCompany(DateTime start, DateTime end, string typeOfDate = null)
        {
            try
            {
                if (IsValidDate(start))
                {
                     Company comp = CompanyModel.GetMyCompany(Constants.user);
                    if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read"))) return View(CompanyModel.GetCompanyCompanies(comp.CompanyID, start, end, typeOfDate));
                    else return new EmptyResult();
                }
                else return new EmptyResult();

                
            }
            catch (Exception)
            {

                return new EmptyResult();
            }

                        
        }

        public ActionResult ShortPeople(DateTime start, DateTime end, string typeOfDate = null)
        {
            try
            {
                if (IsValidDate(start))
                {
                    Company comp = CompanyModel.GetMyCompany(Constants.user);
                    if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read"))) return View(PersonModel.GetCompanyPeople(comp.CompanyID, start, end, typeOfDate));
                    else return new EmptyResult();

                }
                else return new EmptyResult();

                
            }
            catch (Exception)
            {

                return new EmptyResult();
            }

                        
        }

        public ActionResult GetCompanyDuplicates(string trn = null)
        {
            //PersonModel.getDuplicatePersons(trn);
            return View(CompanyModel.GetCompanyDuplicates(trn));
        }

        public ActionResult GetFullCompanyDetails(int compId)
        {
            Company comp = CompanyModel.GetCompany(compId, true);

            if (comp.CompanyAddress.longAddressUsed)
                return Json(new { result = 1, compId = comp.CompanyID, compname = comp.CompanyName, comptrn = comp.trn, longAddress = comp.CompanyAddress.longAddress, longAddressUsed = comp.CompanyAddress.longAddressUsed, duplicate = comp.duplicate }, JsonRequestBehavior.AllowGet);
            else
                return Json(new
                {
                    result = 1,
                    compId = comp.CompanyID,
                    compname = comp.CompanyName,
                    comptrn = comp.trn,
                    roadno = comp.CompanyAddress.roadnumber,
                    aptNumber = comp.CompanyAddress.ApartmentNumber,
                    roadname = comp.CompanyAddress.road.RoadName,
                    roadtype = comp.CompanyAddress.roadtype.RoadTypeName,
                    zipcode = comp.CompanyAddress.zipcode.ZipCodeName,
                    city = comp.CompanyAddress.city.CityName,
                    country = comp.CompanyAddress.country.CountryName,
                    parishid = comp.CompanyAddress.parish.ID,
                    parish = comp.CompanyAddress.parish.parish

                }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// New Company Data page
        /// </summary>
        /// <returns></returns>
        public ActionResult CompanyPolicies() { return View(); }


        public JArray GetPoliciesByRange(string start, string end, string SearchObject, string Searchvalue) 
        {
            Company comp = CompanyModel.GetMyCompany(Constants.user);
            JArray DataResponse = new JArray();

            if (start == "null" || end == "null") 
            {
                start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString();
                end = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).ToString();
            }

            DateTime T = DateTime.Now;
            DateTime F = DateTime.Now;

            TempData["FilterStart"] = start;
            TempData["FilterEnd"] = end;

            DateTime.TryParse(start, out F); DateTime.TryParse(end, out T); start = F.ToString("MM/dd/yyyy hh:mm tt"); end = T.ToString("MM/dd/yyyy hh:mm tt");

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read")))
            {
                try
                {
                    string Query = "";

                    try { if (Searchvalue.Trim() == "" || string.IsNullOrEmpty(Searchvalue) || Searchvalue == "'undefined'" || Searchvalue == "undefined") { Searchvalue = "null"; } else { Searchvalue = "'" + Searchvalue + "'"; } }
                    catch (Exception) { }

                    switch (SearchObject) 
                    { 
                        case "Policy":
                            Query = "EXEC GetCompanyData " + comp.CompanyID + ", '" + start + "','" + end + "'," + Searchvalue ;
                            break;
                        case "Certificate":
                            Query = "EXEC GetPolicyDetailsCert '" + start + "','" + end + "'," + Searchvalue + "," + Searchvalue + "," + Searchvalue + "," + comp.CompanyID;
                            break;
                        case "Cover Note":
                            Query = "EXEC GetPolicyDetailsCoverNotes '" + start + "','" + end + "'," + Searchvalue + "," + Searchvalue + "," + Searchvalue + "," + comp.CompanyID;
                            break;
                        default:
                            Query = "EXEC GetCompanyData " + comp.CompanyID + ", '" + start + "','" + end + "'";
                            break;
                    }
                                        
                    System.Data.DataTable dt = (System.Data.DataTable)OmaxFramework.Utilities.OmaxData.HandleQuery(Query, new sql().SqlConn(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.ScalarQuery);
                    DataResponse = JArray.Parse(JsonConvert.SerializeObject(dt));
                }
                catch (Exception)
                {

                }

            }
            return DataResponse;
        }


        /// <summary>
        /// Download Import Error Reports
        /// </summary>
        /// <param name="FromDate">Start Date Range</param>
        /// <param name="EndDate">End Date Range</param>
        /// <returns>FIle Names of error data to be downloaded</returns>
        public JObject ExportFailedReport(string FromDate, string EndDate, int ImportID = 0)
        {
            return new Import().DispatchErrorsFromWeb(Constants.user.employee.company.CompanyID, FromDate, EndDate, ImportID);
        }


        public ActionResult DataImports() { return View(); }

        public JObject GetDataImportsByRange(string start, string end, string Searchvalue = "")
        {
            Company comp = CompanyModel.GetMyCompany(Constants.user);
            JArray DataResponse = new JArray();
            JObject response = new JObject();
            response["success"] = false;
            bool InitialLoad = true;

            if (start == "null" || end == "null")
            {
                start = DateTime.Now.AddDays(-1).ToString();
                end = DateTime.Now.ToString();
            }
            else
            {
                InitialLoad = false;
            }

            DateTime T = DateTime.Now;
            DateTime F = DateTime.Now;

            TempData["FilterStart"] = start;
            TempData["FilterEnd"] = end;

            DateTime.TryParse(start, out F); DateTime.TryParse(end, out T); start = F.ToString("MM/dd/yyyy hh:mm tt"); end = T.ToString("MM/dd/yyyy hh:mm tt");

            try
            {
                try { if (Searchvalue.Trim() == "" || string.IsNullOrEmpty(Searchvalue) || Searchvalue == "'undefined'" || Searchvalue == "undefined") { Searchvalue = "null"; } else { Searchvalue = "'" + Searchvalue + "'"; } }
                catch (Exception) { }

                string QUERY = @"GetImportStatus " + comp.CompanyID + ",'" + start + "','" + end + "'," + InitialLoad;

                if (comp.CompanyID <= 0)
                {
                    response["success"] = false;
                    response["Imports"] = DataResponse;
                    return response;
                }
                   

                System.Data.DataTable dt = (System.Data.DataTable)OmaxFramework.Utilities.OmaxData.HandleQuery(QUERY, new sql().SqlConn(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.ScalarQuery);
                DataResponse = JArray.Parse(JsonConvert.SerializeObject(dt));

                response["success"] = true;
                response["Imports"] = DataResponse;
            }
            catch (Exception)
            {

            }

            return response;
        }


        public JArray GetPoliciesAssociatedCovers(int PolicyID)
        {
            Company comp = CompanyModel.GetMyCompany(Constants.user);
            JArray DataResponse = new JArray();

            string start = TempData["FilterStart"].ToString();
            string end = TempData["FilterEnd"].ToString();

            DateTime T = DateTime.Now;
            DateTime F = DateTime.Now;

            DateTime.TryParse(start, out F); DateTime.TryParse(end, out T); start = F.ToString(); end = T.ToString();

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read")))
            {
                try
                {
                    string Query = "EXEC GetCompanyData " + comp.CompanyID + ", '" + start + "','" + end + "'";
                    System.Data.DataTable dt = (System.Data.DataTable)OmaxFramework.Utilities.OmaxData.HandleQuery(Query, new sql().SqlConn(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.ScalarQuery);
                    DataResponse = JArray.Parse(JsonConvert.SerializeObject(dt));
                }
                catch (Exception)
                {

                }

            }
            return DataResponse;

        }



        /// <summary>
        /// Gets Policy View Details
        /// </summary>
        /// <param name="PolicyID"></param>
        /// <returns></returns>
        public ActionResult PolicyDetails(int PolicyID) 
        {
            TempData["PolicyID"] = PolicyID;
            return View();
        }


        public ActionResult PolicyDataDetails(int PolicyID)
        {
            TempData["PolicyID"] = PolicyID;
            var policy = PolicyModel.getPolicyFromID(PolicyID);
          

            ViewData["startDate"] = policy.startDateTime.ToString("d MMM, yyyy, hh:mm tt");
            ViewData["endDate"] = policy.endDateTime.ToString("d MMM, yyyy, hh:mm tt");
            ViewData["createdOn"] = policy.CreatedOn;
            ViewData["polCover"] = policy.policyCover.ID;
            ViewData["billCheck"] = policy.billCheck;

            ViewData["VehicleAlert"] = false;

            if (TempData["PolicySaved"] != null) ViewData["PolicySaved"] = TempData["PolicySaved"];

            //expire check
            if (DateTime.Now > policy.endDateTime) policy.expired = true;
            return View(policy);
        }


    }
}
