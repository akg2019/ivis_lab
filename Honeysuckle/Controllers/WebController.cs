﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Honeysuckle.Controllers
{
    public class WebController : Controller
    {
        //
        // GET: /Web/

        public ActionResult Index()
        {
            ViewData["searchDone"] = "";

            if (Request.Url.Scheme == "http")
            {
                //Response.Redirect("https://ivisja.com/web");//+ System.Web.HttpContext.Current.Request.Url.Host + (System.Web.HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + System.Web.HttpContext.Current.Request.Url.Port));
            }
           
            return View();
        }

        [HttpPost]
        public ActionResult Index(string chassis = null, string license = null)
        {
            if (chassis != null && license != null)
            {

                chassis = chassis.Replace(" ", "").Replace("-", "");//Cleans chassis of spaces and dashes
                license = license.Replace(" ", "").Replace("-", "");//Cleans License of spaces and dashes

                //Determines if full details must be shown in the result set. FUll details must be shown if a vehicle cover is determined by the chassis and license number
                if (license != "" && chassis != "") ViewData["FullDetails"] = true; else ViewData["FullDetails"] = false;

                ViewData["error"] = false;
                try { ViewData["model"] = PolicyModel.IsVehicleCoveredDetailsUpdated(chassis, license); }
                catch (Exception) { ViewData["model"] = ""; ViewData["error"] = true; }
                ViewData["searchDone"] = "Your search is finish. See results.";

                #region managing logging enquiries
                Enquiry enquiry = new Enquiry(license, chassis);
                enquiry = EnquiryModel.LogEnquiry(enquiry);
                EnquiryModel.WasEnquiryValid(enquiry);
                #endregion

                return View();

            }
            else return View();
        }



        public JObject CheckCoverage(string chassis = null, string license = null)
        {
            JObject DataResponse = new JObject();
            DataResponse.Add("error", false);
            if (chassis != null && license != null)
            {

                chassis = chassis.Trim().Replace(" ", "").Replace("-", "");//Cleans chassis of spaces and dashes
                license = license.Trim().Replace(" ", "").Replace("-", "");//Cleans License of spaces and dashes

                //Determines if full details must be shown in the result set. FUll details must be shown if a vehicle cover is determined by the chassis and license number
                if (license != "" && chassis != "") ViewData["FullDetails"] = true; else ViewData["FullDetails"] = false;


                try
                {

                    DataResponse.Add("VehicleCoverage", JArray.Parse(JsonConvert.SerializeObject(PolicyModel.IsVehicleCoveredDetails2(chassis, license))));
                    DataResponse.Add("FullDetails", string.IsNullOrWhiteSpace(license) ? true : false);
                }

                catch (Exception)
                {
                    DataResponse["error"] = true;
                }


                return DataResponse;
            }
            else return DataResponse;
        }


        //External Rest Service
        public JObject ExternalCheckCoverage(string chassis = null, string license = null)
        {
            JObject DataResponse = new JObject();
            DataResponse.Add("error", false);
            if (chassis != null && license != null)
            {

                chassis = chassis.Trim().Replace(" ", "").Replace("-", "");//Cleans chassis of spaces and dashes
                license = license.Trim().Replace(" ", "").Replace("-", "");//Cleans License of spaces and dashes

                //Determines if full details must be shown in the result set. FUll details must be shown if a vehicle cover is determined by the chassis and license number
                if (license != "" && chassis != "") ViewData["FullDetails"] = true; else ViewData["FullDetails"] = false;


                try
                {

                    DataResponse.Add("VehicleCoverage", JArray.Parse(JsonConvert.SerializeObject(PolicyModel.IsVehicleCoveredDetails2(license,chassis))));
                    DataResponse.Add("FullDetails", string.IsNullOrWhiteSpace(license) ? true : false);
                }

                catch (Exception)
                {
                    DataResponse["error"] = true;
                }


                return DataResponse;
            }
            else return DataResponse;
        }





    }
}
