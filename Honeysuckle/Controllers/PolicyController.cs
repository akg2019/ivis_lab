﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using System.Globalization;
using System.Data.SqlClient;
using System.IO;

namespace Honeysuckle.Controllers
{
    public class PolicyController : Controller
    {

        public ActionResult AuditPolicy(int id = 0)
        {
            if (id != 0) return View(PolicyModel.getPolicyFromID(id));
            else return View();
        }

        public ActionResult JsonPolicyData(int id = 0)
        {
            if (id != 0 && PolicyModel.IsRealPolicyID(id) && (PolicyModel.CanIViewPolicy(id) || UserGroupModel.AmAnAdministrator(Constants.user)))
            {
                Policy policy = PolicyModel.getPolicyFromID(id);
                return Json(new { result = true, policy_start_date = policy.startDateTime.ToString("MM/dd/yyyy HH:mm:ss"), policy_end_date = policy.endDateTime.ToString("MM/dd/yyyy HH:mm:ss") }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Vehicles(int id = 0)
        {
            if (id != 0) return View(PolicyModel.GetInsuredVehicles(id));
            else return new EmptyResult();
        }

        public ActionResult PolicyInsuredDropDown(int id = 0, string type = null, string selected = null)
        {
            List<pidrop> details = new List<pidrop>();
            if (type != null) ViewData["type"] = type;
            if (selected != null) ViewData["selected"] = selected;
            details.Add(new pidrop("0", "Leave Blank"));
            if (id != 0) details.AddRange(pidropmodel.GetPolicyInsuredsDropDown(id));
            return View(details);
        }

        /// <summary>
        /// this function renders the result of a search 
        /// </summary>
        /// <param name="id">string that is search term</param>
        public ActionResult PolGenSearch(string id = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Read")))
            {
                if (id != null) return View(PolicyModel.PolicySearch(Server.UrlDecode(id)));
                else return new EmptyResult();
            }
            else return new EmptyResult();
        }
        
        /// <summary>
        /// This function renders the email partial
        /// </summary>
        /// <param name="email">The email that is added</param>
        public ActionResult Email(Email email = null)
        {
            if (email != null) return RedirectToAction("NewEmail", "Email", new { id = email.emailID });
            else return RedirectToAction("NewEmail", "Email", new { id = 0 });
        }

        /// <summary>
        /// This function returns the list of vehicles in the system
        /// </summary>
        public ActionResult VehiclesList()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read"))) return RedirectToAction("VehiclesList", "Vehicle");
            else
            {

                TempData["permissionMessage"] = "You do not have any permission to access this page";
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// This function renders the index of the policy pages (showing a list of all policies in the system)
        /// </summary>
        //public ActionResult Index(int id = 0)
        //{
        //    if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Read")))
        //    {
        //        if (TempData["PolicyApproval"] != null)
        //        {
        //            ViewData["PolicyState"] = TempData["PolicyApproval"];
        //            TempData["PolicyApproval"] = null;
        //        }

        //        if (id!= 0)
        //        {
        //            if (id == UserModel.GetUser(Constants.user.ID).employee.company.CompanyID) return View(PolicyModel.GetMyPolicies(id)); //if the company Id belongs to the company of the current user
        //            else return View(PolicyModel.GetThisIntermediaryPolicies(id));
        //        }
        //        else 
        //        {
        //            // testing what kind of permissions the user has- in order to determine what is returned
        //            if (UserGroupModel.AmAnAdministrator(Constants.user)) return View(PolicyModel.GetPolicies());
        //            else return View(PolicyModel.GetMyCompPolicies());
        //        }
        //    }
        //    else return RedirectToAction("Index", "Home");
        //}

        /// <summary>
        /// This function returns the details about a particular policy
        /// </summary>
        /// <param name="id">Policy Id</param>
        public ActionResult Details(int id = 0, bool newPol = false)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Read")) && id != 0 && (PolicyModel.CanIViewPolicy(id) || UserGroupModel.AmAnAdministrator(Constants.user)) && PolicyModel.IsRealPolicyID(id))
            {
                Policy policy = PolicyModel.getPolicyFromID(id);

                ViewData["startDate"] = policy.startDateTime.ToString("d MMM, yyyy, hh:mm tt");
                ViewData["endDate"] = policy.endDateTime.ToString("d MMM, yyyy, hh:mm tt");
                ViewData["createdOn"] = policy.CreatedOn;
                ViewData["polCover"] = policy.policyCover.ID;
                ViewData["billCheck"] = policy.billCheck;

                ViewData["VehicleAlert"] = newPol;

                if (TempData["PolicySaved"] != null) ViewData["PolicySaved"] = TempData["PolicySaved"];

                //expire check
                if (DateTime.Now > policy.endDateTime) policy.expired = true;

                return View(policy);
            }
            else
            {
                TempData["EmailSent"] = "You cannot view this policy.";
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// This function deals with the creation of a policy
        /// </summary>
        public ActionResult Create(int vehID = 0,int id = 0, int page = 0, string submit = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Create")))
            {

                Policy pm = new Policy();
                DateTime dt = DateTime.Now;
                ViewData["startDate"] = dt;
                ViewData["endDate"] = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59).AddYears(1);
                ViewData["postBack"] = false;

                if(vehID !=0){
                   
                    Vehicle Risk = new Vehicle();
                    Risk = VehicleModel.GetVehicle(vehID);

                    if (Risk != null)
                    {
                        pm.vehicles = new List<Vehicle>();
                        pm.vehicles.Add(Risk);
                    }
                    Risk.usage = new Usage();
                    pm.policyCover = new PolicyCover();
                }
                
                return View(pm);
            }
            else{

                TempData["permissionMessage"] = "You do not have any permission to access this page";
                return RedirectToAction("Index", "Home");
            }
        }


        /// <summary>
        /// This function deals with the creation of a policy
        /// </summary>
        [HttpPost]
        public ActionResult Create(Policy policy, string startDateTime = null, string endDateTime = null, int companies = 0, int source_company = 0,
                                   int polCovers = 0, string submit = null, string proceed = null, bool billscheck = false)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Create")))
            {
                
                if (billscheck) policy.billingAddressShort = policy.mailingAddressShort;
                if (billscheck) policy.billingAddress = policy.mailingAddress;
                
                //Setting the dates selected to be returned- if necessary
                ViewData["startDate"] = policy.startDateTime.ToString("MM/dd/yyyy HH:mm:ss"); 
                ViewData["endDate"] = policy.endDateTime.ToString("MM/dd/yyyy HH:mm:ss"); 
                ViewData["PolCover"] = polCovers;
                ViewData["postBack"] = true;

                if (companies != 0) policy.insuredby = new Company(companies);
                if (source_company != 0) policy.compCreatedBy = new Company(source_company);
                else policy.compCreatedBy = new Company(); //so it will be defaulted to 0 and therefore set as the company that is creating the policy right now
                if (polCovers != 0) policy.policyCover = new PolicyCover(polCovers);
                policy.billCheck = billscheck;

                ModelState.Remove("insuredby.CompanyID");
                ModelState.Remove("compCreatedBy.CompanyID");
                ModelState.Remove("Usage");
                ModelState.Remove("vehicles.usage.ID");
                ModelState.Remove("insuredby.IAJID");
                ModelState.Remove("policyCover.ID");

                //Ensuring that the end date is later than start date
                int result = DateTime.Compare(policy.startDateTime, policy.endDateTime);
                if (result >= 0)
                {
                    ViewData["Date Error"] = "Policy END-DATE is either before or equal to start date!";
                    return View(policy);
                }


                //Ensuring that a policy has a policy cover
                if (polCovers == 0)
                {
                    ViewData["Date Error"] = "Policy Cover must be selected!";
                    return View(policy);
                }


                //Ensuring that at least one insured is added to the policy
                if (policy.insured == null && policy.company == null)
                {
                    ViewData["No Insured"] = "At least one INSURED must be attached to this policy ";
                    return View(policy);
                }


                //ensuring that at least one vehicle is added to the policy
                if (policy.vehicles == null)
                {
                    ViewData["No Vehicle"] = "At least one vehicle must be attached to this policy ";
                    return View(policy);
                }
                else
                {
                    for (int x = 0; x < policy.vehicles.Count(); x++)
                    {
                        if (policy.vehicles[x].usage.ID == 0) //A vehicle usage has to be added 
                        {
                            ViewData["No Vehicle"] = "One of the selected risks does not have a usage.";
                            return View(policy);
                        }
                    }
                }
                
              
                //Ensuring that the policy number is unique
                if(!policy.no_policy_no && policy.policyNumber != null)
                {
                    if (PolicyModel.PolicyNoExist(policy.policyNumber, policy.insuredby.CompanyID))
                    {
                        ViewData["Date Error"] = "The policy Number entered already exists in the system. Please enter a unique policy number.";
                        return View(policy);
                    }
                }

                if (!Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID))
                {
                    if (policy.brokerPolicyNumber != null && PolicyModel.BrokerNoExist(policy.brokerPolicyNumber, policy.insuredby.CompanyID) != 0)
                    {
                        ViewData["No Vehicle"] = "The Broker Number already exists!";
                        return View(policy);
                    }
                }
                
                if (ModelState.IsValidField("policyPrefix") && ModelState.IsValidField("policyNumber"))
                {
                    policy.ID = (PolicyModel.addPolicy(policy)); 
                    if  (policy.ID == 0)
                    {
                        ViewData["alreadyAdded"] = "That policy has already been created";
                        return View(policy);
                    }
                    else
                    {

                        if (proceed!="") //Sending messages to the admins of the insurance companies/intermediaries involved with the double insured risk
                        {
                            for (int x = 0; x < policy.vehicles.Count(); x++)
                            {
                                if ((VehicleModel.isVehUnderActivePolicy(policy.vehicles[x].ID, policy.startDateTime, policy.endDateTime)) &&
                                    !(CompanyModel.CanAutoCancelVehiclePolicy(PolicyModel.GetPolicyCoveredUnder(policy.vehicles[x].ID, policy.startDateTime, policy.endDateTime).insuredby.CompanyID)))
                                        PolicyModel.RiskDoubleInsuredEmail(policy.vehicles[x].ID, policy,false);
                            }
                        }


                   for (int x = 0; x < policy.vehicles.Count(); x++)
                       if ((VehicleModel.isVehUnderActivePolicy(policy.vehicles[x].ID, policy.startDateTime, policy.endDateTime)) &&
                          (CompanyModel.CanAutoCancelVehiclePolicy(PolicyModel.GetPolicyCoveredUnder(policy.vehicles[x].ID, policy.startDateTime, policy.endDateTime).insuredby.CompanyID)))
                       {
                           PolicyModel.RiskDoubleInsuredEmail(policy.vehicles[x].ID, policy, true);
                           VehicleModel.CancelVehicleUnderPolicy(policy.vehicles[x].ID, 0, policy.startDateTime, policy.endDateTime);
                           
                       }

                    //ADD VEHICLES AND INSURED TO THE POLICY BEING CREATED
                    PolicyModel.addPolicyInsured(policy);
                    return RedirectToAction("AddPolicyVehDrivers", new { id= policy.ID});
                    } 
                }
                else return View(policy);
            }
            else
            {

                TempData["permissionMessage"] = "You do not have any permission to access this page";
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// This function edits an existing policy in the system
        /// </summary>
        /// <param name="id">Policy Id</param>
        public ActionResult Edit(int id = 0)
        {
            //if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Update")) && id != 0 && (PolicyModel.CanIViewPolicy(id) || UserGroupModel.AmAnAdministrator(Constants.user)))
            //{
            //    Policy p = new Policy();

            //    p = PolicyModel.getPolicyFromID(id);

            //    if (p.endDateTime > DateTime.Now)
            //    {

            //        ViewData["startDate"] = p.startDateTime.ToString("MM/dd/yyyy HH:mm:ss");
            //        ViewData["endDate"] = p.endDateTime.ToString("MM/dd/yyyy HH:mm:ss"); 
            //        ViewData["polCover"] = p.policyCover.ID;
            //        ViewData["postback"] = false;
            //        ViewData["billCheck"] = p.billCheck;
 
            //        return View(p);
            //    }
            //    else return RedirectToAction("Details", new { id = id });
            //}
            //else return RedirectToAction("AccessDenied", "Error"); //return RedirectToAction("Index", "Home");
            Policy p = new Policy();

            p = PolicyModel.getPolicyFromID(id);
            
            //if (p.endDateTime > DateTime.Now)
            //{

            //    ViewData["startDate"] = p.startDateTime.ToString("MM/dd/yyyy HH:mm:ss");
            //    ViewData["endDate"] = p.endDateTime.ToString("MM/dd/yyyy HH:mm:ss");
            //    ViewData["polCover"] = p.policyCover.ID;
            //    ViewData["postback"] = false;
            //    ViewData["billCheck"] = p.billCheck;
            //    TempData["CurrentPolicyID"] = id;
            //    return View(p);
            //}
            //else return RedirectToAction("Details", new { id = id });

            ViewData["startDate"] = p.startDateTime.ToString("MM/dd/yyyy HH:mm:ss");
            ViewData["endDate"] = p.endDateTime.ToString("MM/dd/yyyy HH:mm:ss");
            ViewData["polCover"] = p.policyCover.ID;
            ViewData["postback"] = false;
            ViewData["billCheck"] = p.billCheck;
            TempData["CurrentPolicyID"] = id;
            return View(p);
        }


        
        public ActionResult UpdateVehicle(int vehicleID) 
        {
            int PolicyID = 0;
            int.TryParse(TempData["CurrentPolicyID"].ToString(), out PolicyID);
            Vehicle vehicle = VehicleModel.GetVehicle(vehicleID); 
            TempData["CurrentPolicyID"] = PolicyID;

       
            return View(vehicle);
        }

        [HttpPost]
        public ActionResult UpdateVehicle(Vehicle vehicle)
        {
            int PolicyID = 0;
            int.TryParse(TempData["CurrentPolicyID"].ToString(), out PolicyID);
            TempData["CurrentPolicyID"] = "";
            string Query = "Exec ModifyVehicleData " + vehicle.ID.ToString() + ",'" + vehicle.make + "','" + vehicle.VehicleModel  + "'," + vehicle.VehicleYear.ToString()  + ",'" + vehicle.chassisno  + "','" + vehicle.VehicleRegNumber + "'";
            OmaxFramework.Utilities.OmaxData.HandleQuery(Query, sql.GetCurrentCOnnectionString(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.NoneQuery);
            return RedirectToAction("Edit", new { id = PolicyID });
        }


        /// <summary>
        /// This function edits an existing policy in the system
        /// </summary>
        [HttpPost]
        public ActionResult Edit(Policy policy, string startDateTime = null, string endDateTime = null, int polCovers = 0, string proceed = null, bool billscheck = false)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Update")) && (PolicyModel.CanIViewPolicy(policy.ID) || UserGroupModel.AmAnAdministrator(Constants.user)))
            {
                PolicyModel pm = new PolicyModel();

                ModelState.Remove("startDateTime");
                ModelState.Remove("endDateTime");
                ModelState.Remove("Usage");
                ModelState.Remove("insuredby.IAJID");
                ViewData["postback"] = true;

                policy.policyCover = new PolicyCover(polCovers);

                ModelState.Remove("policyCover.ID");   


                //Storing values in case of page call-back
                ViewData["startDate"] = policy.startDateTime.ToString("MM/dd/yyyy HH:mm:ss");
                ViewData["endDate"] = policy.endDateTime.ToString("MM/dd/yyyy HH:mm:ss");
                ViewData["polCover"] = policy.policyCover.ID;
                policy.billCheck = billscheck;
                ViewData["billCheck"] = billscheck;

                //Ensuring that the end date is later than the start date
                int result = DateTime.Compare(policy.startDateTime, policy.endDateTime);
                if (result >= 0)
                {
                    ViewData["Date Error"] = "Policy END-DATE is either before or equal to start date!";
                    return View(policy);
                }

                //Ensuring that at least one insured is added to the policy
                if (policy.insured == null && policy.company == null)
                {
                    ViewData["No Insured"] = "At least one INSURED must be attached to this policy ";
                    return View(policy);
                }

                //Ensuring that at least one vehicle is added to the policy
                if (policy.vehicles == null)
                {
                    ViewData["No Vehicle"] = "At least one vehicle must be attached to this policy ";
                    return View(policy);
                }
                else
                {
                    for (int x = 0; x < policy.vehicles.Count(); x++)
                    {
                        if (policy.vehicles[x].usage.ID == 0) //A vehicle usage has to be added 
                        {
                            ViewData["No Vehicle"] = "One of the selected risks does not have a usage.";
                            return View(policy);
                        }
                    }

                }

                //Ensuring that the policy number is unique
                if (policy.brokerPolicyNumber != null && PolicyModel.BrokerNoExist(policy.brokerPolicyNumber, policy.insuredby.CompanyID, policy.ID) != 0)
                {
                    ViewData["Date Error"] = "The policy Number entered already exists in the system. Please enter a unique policy number.";
                    return View(policy);
                }


                if (ModelState.IsValidField("policyPrefix") && ModelState.IsValidField("policyNumber"))
                {
                    //Adding the edited list of insured and vehicles to the policy
                    PolicyModel.UpdatePolicyData(policy, Constants.user.ID);

                    //Removing all vehicles from the policy
                    pm.modifyPolicy(policy);

                  
                   
                    
                    TempData["PolicySaved"] = "The policy was successfully modified";
                    return RedirectToAction("Details", new { id = policy.ID });
                }
                else return View(policy);
            }
            else return RedirectToAction("AccessDenied", "Error");
        }

        /// <summary>
        /// This function retrieves all the information about a policy , based on the policy id
        /// </summary>
        /// <param name="id">Policy Id</param>
        public ActionResult GetPolicyByID(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Read")) && id != 0)
            {
                DateTime Start = new DateTime();
                DateTime End = new DateTime();

                sql sql = new sql();
                sql.ConnectSQL();

                SqlDataReader reader = sql.QuerySQL("EXECUTE GetPolicyFromId " + id);

                while (reader.Read())
                {
                    if (DBNull.Value != reader["StartDateTime"]) Start = DateTime.Parse(reader["StartDateTime"].ToString());
                    if (DBNull.Value != reader["EndDateTime"]) End = DateTime.Parse(reader["EndDateTime"].ToString());
                }

                //returning policy data
                return Json(new { StartDateTime = Start.ToString(), EndDateTime = End.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This function is used to view a policy
        /// </summary>
        /// <param name="id">Vehicle Id</param>
        public ActionResult ViewPolicy(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Read")) && id != 0)
            {
                Policy policy = new Policy();
                policy = PolicyModel.getPolicyFromID(id);
                return View(policy);
            }
            else
            {

                TempData["permissionMessage"] = "You do not have any permission to access this page";
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// This function is used to pull all emails of the company admins of a particular company (which has a particular vehicle under policy)
        /// </summary>
        /// <param name="id">Vehicle Id</param>
        public ActionResult CompanyAdmins(int id = 0, string start=null, string end= null)
        {
            if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read"))) && id != 0) //edit permission
            {
                List<string> Admins = new List<string>();
                Admins = PolicyModel.getAdmins(id, "", start, end);
                return Json(new { result = 1, admins = Admins }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This function cancels a policy in the system
        /// </summary>
        public ActionResult Cancel(int id) //can return index id if one is present too
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Cancel")) && id != 0)
            {
                int PolicyCreator = 0;
                Notification note = new Notification ();
                note.NoteTo = new List<User>();

                if (PolicyModel.CancelPolicy(id))
                {
                    //A notification is sent to the creator of the policy, upon cancellation
                    PolicyCreator = PolicyModel.GetPolicyCreator (id);

                    if (PolicyCreator != Constants.user.ID)
                    {
                        note.NoteTo.Add(new User(PolicyCreator));
                        note.NoteFrom = new User(Constants.user.ID);
                        note.heading = "Policy Cancelled";
                        note.content = "The Policy: PolicyId " + id + " was cancelled in the system";

                        NotificationModel.CreateNotification(note);
                    }

                    TempData["PolicySaved"] = "The policy has been cancelled";
                    return RedirectToAction("Details", new { id = id });
                    //return RedirectToAction("Index");
                }
                else
                {
                    ViewData["PolicyState"] = "The policy could not be cancelled";
                    return RedirectToAction("Details", new { id = id });
                    //return RedirectToAction("Index");
                }
            }
            ViewData["PolicyState"] = "You do not have the permissions to complete this action";
            return RedirectToAction("Details", new { id = id });
            //return RedirectToAction("Index");
        }

        /// <summary>
        /// This function adds drivers to vehicles in a policy
        /// </summary>
        public ActionResult AddPolicyVehDrivers(int id= 0, string Edit = null) 
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Create")))
            {
                Policy pol = new Policy();
                if (id != 0)
                {
                    if (Edit != null) ViewData["EditPolicy"] = true; else ViewData["EditPolicy"] = false;
                    pol = PolicyModel.getPolicyFromID(id);
                    
                    //Retriving the actual person ids (because the getPolicy function returns the people address ids)
                    if (pol.insured != null)
                    { 
                        for (int x = 0; x < pol.insured.Count; x++)
                        {
                            pol.insured[x].PersonID = PersonModel.getPersonFromAddressId(pol.insured[x].PersonID);
                        }
                    }

                }
                return View(pol);
            }
            else return RedirectToAction("Edit", new { id= id});
        }


        public ActionResult PoliciesToVehicle(int base_id, int compid = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Read")))
                return View(PolicyModel.GetPoliciesWithCompAndVehicle(compid, base_id));
            else
            {

                TempData["permissionMessage"] = "You do not have any permission to access this page";
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult get_last_billing_address(int id = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Update")))
            {
                Address address = PolicyModel.get_policy_billing_address(id);
                bool result = (address.ID != 0);
                return Json(new { result = result, address = address }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult activate_policy(int id = 0)
        {
            if (id != 0 && PolicyModel.CanIViewPolicy(id) && UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Activate"))) return Json(new { result = PolicyModel.activate_policy(PolicyModel.getPolicyFromID(id)) }, JsonRequestBehavior.AllowGet);
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

    }
}
