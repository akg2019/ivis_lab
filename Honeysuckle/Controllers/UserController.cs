﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Honeysuckle.Models;
using System.Diagnostics;
using System.IO;

namespace Honeysuckle.Controllers
{
    public class UserController : Controller
    {
        public UserController() { }

        public ActionResult my_name_json()
        {
            if (Constants.user.username != "not initialized")
            {
                User user = UserModel.GetUser(Constants.user.ID);
                return Json(new { result = 1, name = user.employee.person.fname + " " + user.employee.person.lname }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AuditUser()
        {
            return View();
        }

        public ActionResult ResendPassword(int id = 0,bool resend = false)
        {
            Mail mail = new Mail();
            if ((UserGroupModel.AmAnAdministrator(Constants.user) || UserGroupModel.AmAnIAJAdministrator(Constants.user) || UserModel.TestUserPermissions(Constants.user, new UserPermission("Users","ResetPassword"))) && id != 0)
            {
                mail = UserModel.ResentPasswordToUser(new User(id, UserModel.GetUserNameForMail(id)), resend);
                return Json(new { result = 1, mailSent = mail.sent, mailID = mail.outboundmailID }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ResetQuestions(int id = 0)
        {
            if ((UserGroupModel.AmAnAdministrator(Constants.user) || UserGroupModel.AmAnIAJAdministrator(Constants.user) || UserModel.TestUserPermissions(Constants.user, new UserPermission("Users","ResetQuestion"))) && id != 0)
            {
                UserModel.ResetQuestions(new User(id, UserModel.GetUserNameForMail(id)));
                return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This function sent the isLoggedIn flag on the user to 0
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ResetAccess(int id = 0)
        {
            if ((UserGroupModel.AmAnAdministrator(Constants.user) || UserGroupModel.AmAnIAJAdministrator(Constants.user) || UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "ResetQuestion"))) && id != 0)
            {
                UserModel.ResetAccess(id);
                return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This sends a new password to a selected user
        /// </summary>
        public ActionResult NewPassword(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Update")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Create")))
            {
                if (id != 0)
                {
                    //generates a password for the user
                    User user = new Models.User(id);
                    user.password = UserModel.PasswordGenerator();

                    UserModel.NewPassword(user); //sends username and generated password to database
                    UserModel.NewPasswordEmail(user); //send welcome email to user (with password)

                    TempData["EmailSent"] = "An email has been sent to the user, with login credentials";
                }
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// This function is used to test the existence of a user
        /// </summary>
        /// <param name="id">user id</param>
        public ActionResult UserExist(string id = null)
        {
            return Json(new { result = (UserModel.UserExist(id)) }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function is used to retrieve and return a user's username , given user Id
        /// </summary>
        /// <param name="id">user id</param>
        public ActionResult GetUserName(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Create")))
            {
                if (id != 0)
                {
                    string username = UserModel.GetUserName(id);
                    return Json(new { result = 1, username = username }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return new EmptyResult();
        }


        /// <summary>
        /// This function pulls the list of all users belonging to a particular company- given the company ID
        /// </summary>
        /// <param name="id">VIN of vehicle</param>
        public ActionResult UserList(string id = null)
        {
            int compID;

            if (Constants.user.newRoleMode) compID = Constants.user.tempUserGroup.company.CompanyID;
            else compID = CompanyModel.GetMyCompany(Constants.user).CompanyID;

            if (UserGroupModel.AmAnIAJAdministrator(Constants.user) || UserGroupModel.AmAnAdministrator(Constants.user) || (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Read")) && compID == int.Parse(id)))
            { 
                List<User> users = new List<User>();
                int compId = int.Parse(id);
                users = UserModel.getThisCompUsers(compId);
                return View(users);
            }
            return Json(new { fail = 1 }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This function retrieves a user
        /// </summary>
        /// <param name="id">User ID</param>
        public ActionResult UserListItemSelected(int id = 0)
        {
            if (id != 0) return View(UserModel.GetUser(id));
            return new EmptyResult();
        }

        /// <summary>
        /// This function is used to render an email form, for the adding /displying of a user's email
        /// </summary>
        /// <param name="id">usergroup id</param>
        public ActionResult Email(Email email = null)
        {
            if (email != null) return RedirectToAction("NewEmail", "Email", new { id = email.emailID });
            else return RedirectToAction("NewEmail", "Email", new { id = 0 });
        }


        
        /// <summary>
        /// This function is used to update a user's first-login time in the system
        /// </summary>
        /// <param name="id">user id</param>
        public ActionResult ResetFirstLoginTimer(int id)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Reset Login Timer")))
            {
                UserModel.ResetFirstLoginTimer(new User(id));
                return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function is used to render the creation form for users
        /// </summary>
        public ActionResult Create(int UserCompId = 0)
        {
            bool allow = false;

            if (Constants.user.newRoleMode) //If the current user has a temp role, then the user is created under the company that has the temp role
            {
                if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Create")) && (Constants.user.tempUserGroup.company.CompanyID == UserCompId || UserCompId == 0)) || (UserGroupModel.AmAnAdministrator(Constants.user) || (UserGroupModel.AmAnIAJAdministrator(Constants.user) && !UserModel.userExistsWithCompany(UserCompId))))
                    allow = true;
            }
            else if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Create")) && (CompanyModel.GetMyCompany(Constants.user).CompanyID == UserCompId || UserCompId == 0)) || (UserGroupModel.AmAnAdministrator(Constants.user) || (UserGroupModel.AmAnIAJAdministrator(Constants.user) && !UserModel.userExistsWithCompany(UserCompId))))
                allow = true;
            
            if(allow) 
            {
                User user = new User();
                user.employee = new Employee();
                user.employee.person = new Person();
                if (UserCompId != 0) user.employee.company = new Company(UserCompId);

                else
                {
                    if (Constants.user.newRoleMode) 
                        user.employee.company = CompanyModel.GetCompany(Constants.user.tempUserGroup.company.CompanyID);
                    else user.employee.company = CompanyModel.GetMyCompany(Constants.user);
                }
                
                return View(user);
            }
            else return RedirectToAction("AccessDenied", "Error");
        }

        /// <summary>
        /// This function is used to create a new user
        /// </summary>
        [HttpPost]
        public ActionResult Create(User user, IEnumerable<Email> emails = null, IEnumerable<Phone> phoneNums = null, int companies = 0, IEnumerable<Employee> employee = null, IEnumerable<Person> person = null, int parishes = 0, IEnumerable<Company> company = null, int compid = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users","Create")))
            {
                #region ModelState Remove
                ModelState.Remove("password"); //NOT VALIDATING PASSWORD HERE
                ModelState.Remove("confirm_password");
                ModelState.Remove("employee.person.address.parish.ID");
                #endregion
                ViewData["postback"] = true;
                if (compid != 0) user.employee.company = new Company(compid);
                
                if (emails != null) user.employee.person.emails = emails.ToList();
                else user.employee.person.emails = new List<Email>();
                user.employee.person.address = AddressModel.TrimAddress(user.employee.person.address);
                if (parishes != 0) user.employee.person.address.parish = new Parish(parishes);
                else 
                {
                    ModelState.AddModelError("parish", "Please select a parish");
                    return View(user);
                }
                
                if (ModelState.IsValid)
                {
                    if (!UserModel.UserExist(user.username))
                    {
                        //SET ALL RELEVANT DATA TO MODEL
                       
                        if (emails != null) user.employee.person.emails = EmailModel.AddUsernameEmailToPerson(emails.ToList(), user.username);
                        else user.employee.person.emails = EmailModel.AddUsernameEmailToPerson(new List<Email>(), user.username);

                        if (phoneNums != null) user.employee.person.phoneNums = phoneNums.ToList();

                        if (user.employee == null) user.employee = new Employee();
                        if (user.employee.company == null) user.employee.company = new Company();

                        //CHECKS IF THE COMPANY OF THE NEW USER WAS SELECTED BY THE SYSTEM ADMIN, ELSE, THE USER ASSUMES THE COMPANY OF THE CURRENT USER
                        if (companies == 0 && company == null) user.employee.company.CompanyID = UserModel.GetUser(Constants.user.ID).employee.company.CompanyID;
                        else if (companies == 0 && company != null) user.employee.company.CompanyID = company.ToList()[0].CompanyID;
                        else user.employee.company.CompanyID = companies;

                        //Generates a password for the user
                        string GeneratedPassword = UserModel.PasswordGenerator();
                        user.password = GeneratedPassword;

                        if (!UserGroupModel.AmAnIAJAdministrator(Constants.user)) UserModel.AddUser(user); //sends username and generated password to database
                        else UserModel.AddUser(user, 1); //Another parameter is added (because the user is being created by a system admin), so that the created user becomes an automatic company admin of the company added to

                        UserModel.WelcomeEmail(user, System.Web.HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + System.Web.HttpContext.Current.Request.Url.Host + (System.Web.HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + System.Web.HttpContext.Current.Request.Url.Port)); //send welcome email to user (with password)

                        TempData["EmailSent"] = "User Created! An email has been sent to the user, with login credentials";
                       
                        if(UserGroupModel.AmAnAdministrator(new User(Constants.user.ID))) return RedirectToAction("Index", "RoleManagement");
                        else return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("username","Unavailable username");
                        return View(user);
                    }
                }
                else return View(user);
            }
            else return RedirectToAction("AccessDenied", "Error");
        }


        /// <summary>
        /// This function is used to retrieve and display a user's information, for editing 
        /// </summary>
        /// <param name="id">user id</param>
        public ActionResult Modify(int id)
        {
            bool allow = false;

            if (Constants.user.newRoleMode)
            {
                if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Update")) && Constants.user.tempUserGroup.company.CompanyID == CompanyModel.GetMyCompany(new User(id)).CompanyID) || (UserGroupModel.AmAnIAJAdministrator(Constants.user))) allow = true;
            }
            else if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Update")) && CompanyModel.GetMyCompany(Constants.user).CompanyID == CompanyModel.GetMyCompany(new User(id)).CompanyID) || (UserGroupModel.AmAnAdministrator(new User(Constants.user.ID))) || (UserGroupModel.AmAnIAJAdministrator(Constants.user))) allow = true;

            if (allow)
            {
                User user = UserModel.GetUser(id);
                if(user.employee != null)
                    user.employee.person.emails = EmailModel.RmUsernameEmailToPerson(user.employee.person.emails, user.username);
                return View(user);
            }
            else return RedirectToAction("Index", "Home");
        }


        /// <summary>
        /// This function is used to edit a user's record in the system
        /// </summary>
        [HttpPost]
        public ActionResult Modify(User user, IEnumerable<Email> emails = null,IEnumerable<Phone> phoneNums = null, IEnumerable<Company> company = null, int companies = 0, IEnumerable<Employee> employee = null, IEnumerable<Person> person = null, int parishes = 0,string hiddenUsername = null)
        {
            bool allow = false;
            user.employee = employee.ToList()[0];
            //user.employee.company = company.ToList()[0];
            if (Constants.user.newRoleMode)
            {
                if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Update")) && Constants.user.tempUserGroup.company.CompanyID == user.employee.company.CompanyID) || (UserGroupModel.AmAnIAJAdministrator(Constants.user))) allow = true;
            }
            else if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Update")) && CompanyModel.GetMyCompany(Constants.user).CompanyID == user.employee.company.CompanyID) || (UserGroupModel.AmAnAdministrator(new User(Constants.user.ID))) || (UserGroupModel.AmAnIAJAdministrator(Constants.user))) allow = true;

            if (allow)
            {
                ModelState.Remove("password"); //NOT VALIDATING PASSWORD
                ModelState.Remove("confirm_password");
             
                if (companies != 0) user.employee.company = new Company(companies);
                else
                {
                    if (company != null) user.employee.company = company.ToList()[0];
                    else user.employee.company = CompanyModel.GetMyCompany(user);
                }
                user.employee.person = person.ToList()[0];
                user.employee.person.address = AddressModel.TrimAddress(user.employee.person.address);
                if (emails != null) user.employee.person.emails = EmailModel.AddUsernameEmailToPerson(emails.ToList(), user.username);
                else user.employee.person.emails = new List<Email>();
               
                if (parishes != 0) user.employee.person.address.parish = new Parish(parishes);
                else
                {
                    ModelState.AddModelError("parish", "Please select a parish");
                    return View(user);
                }
                
                if (UserModel.UserExist(user.username) && user.username != hiddenUsername)
                {
                    ViewData["usernameExists"] = "This username already exists";
                    ViewData["username"] = hiddenUsername;
                    user.employee.person.emails = emails.ToList();
                    return View(user);
                }

                ViewData["emailExists"] = "";
                if (emails != null)
                {
                    for (int i = 0; i < emails.Count(); i++)
                    {
                        if (UserModel.UserExist(emails.ElementAt(i).email))
                        {
                            ViewData["emailExists"] = "This email is already in use.";
                            user.employee.person.emails = emails.ToList();
                            return View(user);
                        }
                    }
                }
                UserModel.ModifyUser(user);

                PhoneModel.DropPhoneFromPerson(user.employee.person);
                if (phoneNums != null)
                {
                    user.employee.person.phoneNums = new List<Phone>();
                    foreach (Phone phone in phoneNums) user.employee.person.phoneNums.Add(phone);
                }
                PhoneModel.AddPhoneToPerson(user);

                TempData["EmailSent"] = "User modified!";
                return RedirectToAction("Index", "Home");
            }
            else return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// This function is used to retrieve and display a user's information, for display
        /// </summary>
        /// <param name="id">user id</param>
        public ActionResult Details(int id)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users","Read")))
            {
                User user = new User();
                user = UserModel.GetUser(id);
                user.employee.person.emails = EmailModel.RmUsernameEmailToPerson(user.employee.person.emails, user.username);
                ViewData["Details"] = true;
                return View(user);
            }
            else return RedirectToAction("Index", "Home");
        }


        /// <summary>
        /// This function is used to retrieve a user's information- for the sake of adding user roles
        /// </summary>
        /// <param name="id">user id</param>
        public ActionResult AddUserRoles(int id)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups","Create")))
            {
                User user = UserModel.GetUser(id);
                return View(user);
            }
            else return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// This function is used to activate a user
        /// </summary>
        /// <param name="id">user id</param>
        public ActionResult ActivateUser(int id)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Activate")) || UserGroupModel.AmAnAdministrator(Constants.user)) return Json( new { result = UserModel.ActivateUser(id) }, JsonRequestBehavior.AllowGet);
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This function is used to de-activate a user
        /// </summary>
        /// <param name="id">user id</param>
        public ActionResult DeactivateUser(int id)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Activate")) || UserGroupModel.AmAnAdministrator(Constants.user)) return Json(new { result = UserModel.DeactivateUser(id) }, JsonRequestBehavior.AllowGet);
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }



        /// <summary>
        /// This function is used to delete a user
        /// </summary>
        /// <param name="id">user id</param>
        public ActionResult Delete(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users","Delete")) && id != 0)
            {
              if ((UserModel.DeleteUser(id)))
                {
                    if (!(UserModel.DeleteUser(id))) return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
                    else return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }



        /// <summary>
        /// This function is used to retrieve all users that are apart of a group
        /// </summary>
        /// <param name="id">user id</param>
        public ActionResult UsersInRoleList(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups", "Read")) || UserGroupModel.AmAnAdministrator(Constants.user))
            {
                List<User> users = new List<User>();
                if (id != 0) users = UserModel.GetAllUsersForAGroup(new UserGroup(id));
                return View(users);
            }
            else return new EmptyResult();
        }



        /// <summary>
        /// This function is used to retrieve all users that are not apart of a group
        /// </summary>
        /// <param name="id">user id</param>
        public ActionResult UsersNotInRoleList(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups", "Read")) || UserGroupModel.AmAnAdministrator(Constants.user))
            {
                List<User> users = new List<User>();
                if (id != 0) users = UserModel.GetAllUsersNotForAGroup(new UserGroup(id));
                return View(users);
            }
           else return new EmptyResult();
        }


        /// <summary>
        /// This function is used to retrieve the list of companies from the system
        /// </summary>
        /// <param name="model">A list of companies</param>
        public ActionResult GetCompanies(List<Company> model)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read"))) return RedirectToAction("GetCompanies", "Company", model);
            else return new EmptyResult();
        }


        /// <summary>
        /// This function is used to retrieve a user's information, given the user's TRN
        /// </summary>
        /// <param name="id">User TRN</param>
        public ActionResult GetUserByTRN(string id,bool test = false)
        {

            #region accept certs
            //test code
            System.Net.ServicePointManager.ServerCertificateValidationCallback +=
            delegate(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                    System.Security.Cryptography.X509Certificates.X509Chain chain,
                                    System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                return true; // **** Always accept
            };
            #endregion

            bool result = false; bool personresult = false; bool userresult = false; bool fail = false; bool empresult = false;
            string trn = null;
            if (id == "10000000" && test) return Json(new { error = 0, result = 1,invalid = 1 }, JsonRequestBehavior.AllowGet);
            try
            {
                trn = id;
                result = true;
            }
            catch { fail = true; }
            if (!fail)
            {
                Person person = new Person();
                person.TRN = trn;
                User user = PersonModel.TestPersonWithTRN(person, false);
                if (user.ID != 0) userresult = true;
                if (!userresult) 
                { 
                    if (user.employee != null) 
                    {
                        if (user.employee.EmployeeID != 0) empresult = true; 
                        if (user.employee.person != null) 
                        { 
                            if (user.employee.person.PersonID != 0) personresult = true; 
                        } 
                    } 
                }
                if (result)
                {
                    if (!Constants.user.newRoleMode)
                    {
                        if (Constants.user.employee == null) EmployeeModel.GetMyEmployee(Constants.user); //get employee
                        if (Constants.user.employee.company == null) CompanyModel.GetMyCompany(Constants.user); //get company
                    }
                    if (userresult)
                    {
                        bool MyCompEmployee = false;

                        if (Constants.user.newRoleMode)
                        {
                            if (user.employee.company.CompanyID == Constants.user.tempUserGroup.company.CompanyID)
                                MyCompEmployee = true;
                        }

                        else
                        {
                           if (user.employee.company.CompanyID == Constants.user.employee.company.CompanyID)
                               MyCompEmployee = true;
                        }

                     if (MyCompEmployee)   {
                            //USER IS IN MY COMPANY DONT 
                            //CREATE A NEW USER/EMPLOYEE
                            return Json(new
                            {
                                error = 0,
                                result = 1,
                                user = 1,
                                person = 0,
                                exist = 1,
                                emp = 0,
                                empid = user.employee.EmployeeID,
                                uid = user.ID
                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {

                            //USER ALREADY EXISTS
                            return Json(new
                            {
                                error = 0,
                                result = 1,
                                user = 1,
                                person = 0,
                                exist = 0,
                                emp = 0,
                                companyID = user.employee.company.CompanyID,
                                personid = person.PersonID,
                                fname = person.fname,
                                mname = person.mname,
                                lname = person.lname,
                                roadno = person.address.roadnumber,
                                aptNumber = person.address.ApartmentNumber,
                                roadname = person.address.road.RoadName,
                                roadtype = person.address.roadtype.RoadTypeName,
                                zipcode = person.address.zipcode.ZipCodeName,
                                city = person.address.city.CityName,
                                country = person.address.country.CountryName
                            }, JsonRequestBehavior.AllowGet);
                        }
                        //return Json(new { error = 0, result = 1, user = 1, person = 0, uid = user.ID }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (empresult)
                        {

                            return Json(new
                            {
                                companyID = user.employee.company.CompanyID,
                                error = 0,
                                result = 1,
                                user = 0,
                                person = 1,
                                emp = 1,
                                personid = person.PersonID,
                                fname = person.fname,
                                mname = person.mname,
                                lname = person.lname,
                                roadno = person.address.roadnumber,
                                aptNumber = person.address.ApartmentNumber,
                                roadname = person.address.road.RoadName,
                                roadtype = person.address.roadtype.RoadTypeName,
                                zipcode = person.address.zipcode.ZipCodeName,
                                city = person.address.city.CityName,
                                country = person.address.country.CountryName
                            }, JsonRequestBehavior.AllowGet);
                            
                        }
                        else
                        {
                            //TAJ WORKING
                            bool taj = false;
                            if (person.PersonID == 0 && !personresult)
                            {
                                person = TAJ.GetPersonDetails(trn);
                                if (person.TRN != null && person.TRN != "")
                                {
                                    personresult = true;
                                    taj = true;
                                }
                            }
                            
                            if (personresult)
                            {

                                //PERSON ALREADY EXIST, RETURN DATA
                                return Json(new
                                {
                                    error = 0,
                                    result = 1,
                                    user = 0,
                                    person = 1,
                                    taj = taj,
                                    emp = 0,
                                    personid = person.PersonID,
                                    fname = person.fname,
                                    mname = person.mname,
                                    lname = person.lname,
                                    roadno = person.address.roadnumber,
                                    aptNumber = person.address.ApartmentNumber,
                                    roadname = person.address.road.RoadName,
                                    roadtype = person.address.roadtype.RoadTypeName,
                                    zipcode = person.address.zipcode.ZipCodeName,
                                    city = person.address.city.CityName,
                                    parish = person.address.parish.parish,
                                    parishid = person.address.parish.ID,
                                    country = person.address.country.CountryName
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else return Json(new { error = 0, message = "There was an error." }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    
                }
                else
                {
                    //TRN IS FREE DO NOTHING
                    return Json(new { error = 0, result = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                //fail in parsing text to int
                return Json(new { error = 1, message = "You must enter a number with 9 digits into the TRN field." }, JsonRequestBehavior.AllowGet);
            }
            
        }


        /// <summary>
        /// This function is used to display a users info-to add a contact for that person (as it relates to their company)
        /// </summary>
        /// <param name="model">A list of companies</param>
        public ActionResult UserContact(string userID=null)
         {
             if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Read")))
             {
                 User user = new User();
                 user = UserModel.GetUser(int.Parse(userID));
                 return View(user);
             }
             else return new EmptyResult();
         }

        /// <summary>
        /// This function is used to display a users info (partial)
        /// </summary>
        /// <param name="model">A list of companies</param>
        public ActionResult UserInfo(string username = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Read")))
            {
                User user = new User();
                user = UserModel.GetUserFromUsername(username);
                user.employee.person.emails = EmailModel.GetMyEmails(user.employee.person);
                return View(user);
            }
            else return new EmptyResult();
        }

        /// <summary>
        /// This function is used to retrieve and display a user's information, for editing (on their profile page)
        /// </summary>
        /// <param name="id">user id</param>
        public ActionResult Profile()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "EditProfile")))
            {
                User user = UserModel.GetUser(Constants.user.ID);
                user.employee.person.emails = EmailModel.RmUsernameEmailToPerson(user.employee.person.emails, user.username);

                ViewData["policyInsured"] = false;
                return View(user);
            }
            else return RedirectToAction("Index", "Home");
        }


        /// <summary>
        /// This function is used to edit a user's record (profile) in the system
        /// </summary>
        [HttpPost]
        public ActionResult Profile(User user, IEnumerable<Email> emails = null, IEnumerable<Person> person = null, int parishes = 0, IEnumerable<Company> company = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "EditProfile")) || (UserGroupModel.AmAnAdministrator(Constants.user)))
            {
                user.employee = new Employee (person.ToList()[0]);
                user.employee.company = company.ToList()[0];

                if(emails != null)
                user.employee.person.emails = emails.ToList();

                if (parishes != 0) user.employee.person.address.parish = new Parish(parishes);
                else
                {
                    ModelState.AddModelError("parish", "Please select a parish");
                    return View(user);
                }


                if (user.password == null && user.confirm_password == null) //If no new password was enetered, then the password doesn't need to be validated
                {
                    ModelState.Remove("password");
                    ModelState.Remove("confirm_password");
                }

                else
                {
                    if (user.password != user.confirm_password)
                       {
                         ViewData["error"] = "Your passwords must match";
                         user.password = null;
                         user.confirm_password = null;
                         return View(user);
                       }
                }

                if (ModelState.IsValid)
                {
                    PersonModel.ModifyPerson(user.employee.person);

                    if (user.password != null)
                    {
                        UserModel.SetResetPasswordData(user);
                        TempData["EmailSent"] = "Password and user information modified!";
                    }
                    else TempData["EmailSent"] = "Information modified!";

                    return RedirectToAction("Index", "Home");
                }
                else return View(user);
            }
            else return RedirectToAction("Index", "Home");
        }

        public ActionResult Upload()
        {
            if (false)
            {
                if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Upload"))) return View();
                else return RedirectToAction("AccessDenied", "Error");
            }
            else return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase UploadedFile = null)
        {
            if (false)
            {
                if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Upload")))
                {
                    List<response> responses = new List<response>();
                    string errors = "";
                    if (UploadedFile != null)
                    {
                        Stream stream = UploadedFile.InputStream; string line; string text = null;
                        using (StreamReader sr = new StreamReader(stream)) { while ((line = sr.ReadLine()) != null) { text += line; } }
                        responses = XML.UploadUsers(text);
                    }
                    else errors = "There was no file uploaded";
                    ViewData["error"] = errors;
                    return View(responses);
                }
                else return RedirectToAction("AccessDenied", "Error");
            }
            else return RedirectToAction("Index", "Home");
        }

        public ActionResult TestUsername(string username)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Users", "Update")))
            {
                return Json(new { result = UserModel.UserExist(username), error = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 , error = 1 },JsonRequestBehavior.AllowGet);
        }

    }
}
