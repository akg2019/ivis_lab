﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Honeysuckle.Models;
using System.Globalization;

namespace Honeysuckle.Controllers
{
    public class BIController : Controller
    {
        //
        // GET: /BI/

        public ActionResult Manage()
        {
            if (UserGroupModel.IsAdministrative(Constants.user) && UserModel.TestUserPermissions(Constants.user, new UserPermission("BI", "Update"))) return View(BusinessIntelligenceModel.GetAllBusinessIntelligence(CompanyModel.GetMyCompany(Constants.user)));
            else return RedirectToAction("AccessDenied", "Error");
        }

        [HttpPost]
        public ActionResult Manage(BusinessIntelligence bi, string submit = null)
        {
            if (UserGroupModel.IsAdministrative(Constants.user) && UserModel.TestUserPermissions(Constants.user, new UserPermission("BI", "Update")))
            {
                if (submit == "Save") BusinessIntelligenceModel.UpdateIntelligence(bi);
                return RedirectToAction("Index", "Home");
            }
            else return RedirectToAction("AccessDenied", "Error");
        }

        public ActionResult EnquiryByInsurer(DateTime start, DateTime end, int insurer = 0, bool days = false, bool months = false)
        {
            List<IntelCount> enquiries = IntelCountModel.EnquiryByInsurer(start, end, new Company(insurer), days, months);
            List<IntelCountCompany> enquiriesCompany = IntelCountCompany.Convert(enquiries);
            return Json(new { enq = JsonConvert.SerializeObject(enquiries), compenq = JsonConvert.SerializeObject(enquiriesCompany) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CertificateIntel(DateTime start, DateTime end)
        {
            return Json(new { enq = JsonConvert.SerializeObject(IntelCountModel.CertIntelCountForTime(start, end)) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Enquiries(DateTime start, DateTime end)
        {
            return Json(new { enq = JsonConvert.SerializeObject(EnquiryModel.GetEnquiryRange(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.AddDays(-1))) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Errors(DateTime start, DateTime end)
        {
            return Json(new { enq = 1 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CertificateSection()
        {
            return View();
        }

        public ActionResult EnquirySection()
        {
            return View();
        }

        public ActionResult EnquiryGauge(int id = 0, string datemode = null, string type = null)
        {
            if (id != 0 && datemode != null && type != null)
            {
                List<KPI> kpis = KPIModel.GetKPI(new Company(id), type, datemode);
                return View(kpis);
            }
            else return new EmptyResult();
        }

        public ActionResult AllGauges()
        {
            //if (Constants.user.username != "not initialized")return View();
            //else return new EmptyResult();

            return new EmptyResult();

        }

        public ActionResult Gauge(int id = 0, string datemode = null, string type = null, bool iaj = false)
        {
            if (id != 0 && datemode != null && type != null)
            {
                ViewData["type"] = type;
                ViewData["iaj"] = iaj;
                List<KPI> kpis = KPIModel.GetKPI(new Company(id), type, datemode);
                return View(kpis);
            }
            else return new EmptyResult();
        }

        public ActionResult GaugeCount(string datemode = null, string type = null)
        {
            if (datemode != null && type != null)
            {
                ViewData["type"] = type;
                return View(IntelCountModel.CountByMonthbyInsurer(CompanyModel.GetMyCompany(Constants.user), DateTime.Now.ToString("MMMM", CultureInfo.InvariantCulture), DateTime.Now.ToString("yyy", CultureInfo.InvariantCulture), type));
            }
            else return new EmptyResult();
        }

        public ActionResult EnquiryCount(int id = 0, string month = null, string year = null)
        {
            if (id != 0 && month != null && year != null) return Json(new { count = IntelCountModel.CountByMonthbyInsurer(new Company(id), month, year, "enquiry") }, JsonRequestBehavior.AllowGet);
            else return Json(new { count = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CompanyEnquiryCount()
        {
            return View();
        }

        public ActionResult ErrorSection()
        {
            return View();
        }

        public ActionResult IntermediaryCoverCount()
        {
            return View();
        }

    }
}
