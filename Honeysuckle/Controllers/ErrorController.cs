﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Honeysuckle.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        /// <summary>
        /// When URL does not exist in system
        /// </summary>
        public ActionResult NotFound(string aspxerrorpath = null)
        {
            bool isRelease = false;
            #if DEBUG
            isRelease = false;
            #else
            isRelease = true;
            #endif
            ViewData["release"] = isRelease;
            return View();
        }

        /// <summary>
        /// For when users are not given access to data they attempt to access
        /// </summary>
        public ActionResult AccessDenied()
        {
            bool isRelease = false;
            #if DEBUG
            isRelease = false;
            #else
            isRelease = true;
            #endif
            ViewData["release"] = isRelease;
            return View();
        }

    }
}
