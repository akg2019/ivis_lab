﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Honeysuckle.Controllers
{
    public class UsageController : Controller
    {

        /// <summary>
        /// This function is used to create a usage
        /// </summary>
        /// <param name="id">Usage Id</param>
        public ActionResult Create(int id = 0, int cid = 0, bool details = false, int UCID = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Usages", "Create")))
            {
                Usage usage = new Usage();
                ViewData["cid"] = cid;
                if (id != 0)
                {
                    usage = UsageModel.GetUsage(id, cid);
                    try
                    {
                        System.Data.DataTable dt = (System.Data.DataTable)OmaxFramework.Utilities.OmaxData.HandleQuery("SELECT TOP 1 CertCode,ID FROM UsagesToCovers WHERE ID = " + UCID, sql.GetCurrentCOnnectionString(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.ScalarQuery);
                        usage.CertCode = dt.Rows[0].ItemArray[0].ToString();
                        usage.UCID = int.Parse(dt.Rows[0].ItemArray[1].ToString());
                    }
                    catch (Exception)
                    {
                       usage.CertCode = "";
                    }
                   
                }
                return View(usage);
            }
            else return new EmptyResult();
        }

        /// <summary>
        /// This function is to view usages
        /// </summary>
        /// <param name="id">Usage Id</param>
        public ActionResult Details(int id = 0, int cid = 0,bool details = false, int UCID = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Usages", "Read")))
            {
                Usage usage = new Usage();
                ViewData["cid"] = cid;
                ViewData["Details"] = details;
               // ViewData["details"] = details;
                if (id != 0) usage = UsageModel.GetUsage(id, cid);
                try
                {
                    System.Data.DataTable dt = (System.Data.DataTable)OmaxFramework.Utilities.OmaxData.HandleQuery("SELECT CertCode FROM UsagesToCovers WHERE ID = " + UCID, sql.GetCurrentCOnnectionString(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.ScalarQuery);
                    usage.CertCode = dt.Rows[0].ItemArray[0].ToString();
                    usage.UCID = UCID;
                }
                catch (Exception)
                {
                   usage.CertCode = "";
                }
                return View(usage);
            }
            else return new EmptyResult();
        }

        public ActionResult Delete(int id = 0, int cid = 0, int UCID = 0)
        {
            if (id != 0 && cid != 0)
            {
                Usage u = new Usage(id);
                u.UCID = UCID;
                return Json(new { result = UsageModel.RmUsageFromCover(u, new PolicyCover(cid)) }, JsonRequestBehavior.AllowGet);
            }
            else 
            { 
                return Json(new { result = 0 }, JsonRequestBehavior.AllowGet); 
            }
        }


        public ActionResult Edit(int id = 0, string usage = null, string CertCode = "", int cid = 0, int UCID = 0)
        {
            if (id != 0 && !string.IsNullOrWhiteSpace(usage) && !string.IsNullOrWhiteSpace(CertCode))
            {
                Usage u = new Usage(id, usage);
                u.CertCode = CertCode;
                u.UCID = UCID;
                return Json(new { result = UsageModel.EditUsage(u,cid) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// This function deals with the dropdown of usages
        /// </summary>
        /// <param name="coverID">Policy Cover ID</param>
        public ActionResult UsageDropdown(int coverID = 0, bool All = false, int usageId = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Create"))|| UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Create")))
            {
                if (coverID != 0)
                {
                    List<Usage> usages = UsageModel.GetPolicyCoverUsages(new PolicyCover(coverID),All);
                   
                    int i, j;
                    Usage key = new Usage();
                    for (i = 1; i < usages.Count(); i++)
                    {
                        key = usages[i];
                        j = i - 1;


                        while (j >= 0 && usages[j].usage.CompareTo(key.usage) > 0)
                        {
                            usages[j + 1] = usages[j];
                            j = j - 1;
                        }
                        usages[j + 1] = key;
                    }
                    ViewData["usageID"] = usageId;
                    return View(usages.OrderBy(p => p.usage));
                }
                else if (All == true) {
                    List<Usage> usages = UsageModel.GetPolicyCoverUsages(new PolicyCover(coverID), All);
                    int i, j;
                    Usage key = new Usage();
                    for (i = 1; i < usages.Count(); i++)
                    {
                        key = usages[i];
                        j = i - 1;


                        while (j >= 0 && usages[j].usage.CompareTo(key.usage) > 0)
                        {
                            usages[j + 1] = usages[j];
                            j = j - 1;
                        }
                        usages[j + 1] = key;
                    }

                    return View(usages.OrderBy(p => p.usage));
                }
                else
                {
                    List<Usage> usages = new List<Usage>();
                    return View(usages);
                }
            }
            else return new EmptyResult();

        }


        public bool UpdateUsage(int UsageID, int VehicleID, int PolicyID){

            bool Updated = false;

            try
            {

            }
            catch (Exception)
            {
              Updated = false;
            }

            return Updated;
        }

        /// <summary>
        /// This function is used to retrieve all usages associated with a particular policy cover
        /// </summary>
        /// <param name="id">Policy Cover Id</param>
        public ActionResult CoverUsages(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Create")) && id != 0) return View(UsageModel.GetPolicyCoverUsages(new PolicyCover(id)));
            else return new EmptyResult();
        }


        /// <summary>
        /// This function deals with the dropdown of usages
        /// </summary>
        /// <param name="coverID">Policy Cover ID</param>
        public ActionResult UsageDropdownOptions(int coverID = 0, bool All= false)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Create")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Create")))
            {
                string [] UsageArray = {};
                JavaScriptSerializer jsonList = new JavaScriptSerializer();
                List<Usage> u = new List<Usage>();
                if (coverID != 0)
                {
                    u = UsageModel.GetPolicyCoverUsages(new PolicyCover(coverID),All);
                    string jsonFile = jsonList.Serialize(u);
                    return Json(new { result = jsonFile }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string jsonFile = jsonList.Serialize(u);
                    return Json(new { result = jsonFile }, JsonRequestBehavior.AllowGet);
                }
            }
            else return new EmptyResult();

        }

        public ActionResult CoverUsagesJson(int id = 0)
        {
            List<UserPermission> permissions = new List<UserPermission>();
            permissions.Add(new UserPermission("Wording","Create"));
            permissions.Add(new UserPermission("Policies", "Create"));
            
            if (UserModel.TestUserPermissions(Constants.user, permissions)) return Json(new { success = true, data = UsageModel.GetPolicyCoverUsages(new PolicyCover(id)) }, JsonRequestBehavior.AllowGet);
            else return Json(new { success = false }, JsonRequestBehavior.AllowGet);

        }


        //Gets Company Usage/Cert Type Dropdown Lists
        public JObject GetUsageCertTypesListDropDownData()
        {
            JObject response = new JObject();
            response["usage"] = JArray.FromObject(OmaxFramework.Utilities.OmaxData.HandleQuery("exec GetUsagesListDropDownData " + Constants.user.employee.company.CompanyID, sql.GetCurrentCOnnectionString(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.ScalarQuery));
            response["cert"] = JArray.FromObject(OmaxFramework.Utilities.OmaxData.HandleQuery("exec GetCertTemplateListDropDownData " + Constants.user.employee.company.CompanyID, sql.GetCurrentCOnnectionString(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.ScalarQuery));
            return response;
        }

        


    }
}
