﻿using System;   
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class KPIController : Controller
    {
        //
        // GET: /KPI/

        public ActionResult KPI(List<KPI> kpis, string type, int cid = 0)
        {
            ViewData["type"] = type;
            KPIModel.InitializeKPI(new Company(cid), type);
            return View(KPIModel.BunchKPIs(KPIModel.KPIsByType(kpis, type)));
        }

        [HttpPost]
        public ActionResult KPIData(string type, string start = null, string end = null, string ins = null, bool import = false)
        {
            //if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("BI", "Cover Chart")) && type == "cover") ||
            //    (UserModel.TestUserPermissions(Constants.user, new UserPermission("BI", "Enquiry Chart")) && type == "enquiry"))
            //{
            //    DateTime start_dt; DateTime end_dt;
            //    if (start != null) start_dt = DateTime.Parse(start);
            //    else start_dt = DateTime.Now.AddMonths(-1);
            //    if (end != null) end_dt = DateTime.Parse(end);
            //    else end_dt = DateTime.Now;

            //    switch(type){
            //        case "cover":
            //            return Json(new { result = 1, kpi = KPIModel.get_intermediary_cover_count(start_dt, end_dt, ins, import) }, JsonRequestBehavior.AllowGet);
            //        case "enquiry":
            //            return Json(new { result = 1, kpi = KPIModel.GetEnquiryCount(start_dt, end_dt, ins) }, JsonRequestBehavior.AllowGet);
            //        default:
            //            return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            //    }
            //}
            //else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);

            return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


    }
}
