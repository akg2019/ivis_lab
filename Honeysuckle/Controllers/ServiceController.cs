﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class ServiceController : Controller
    {
        public ActionResult vehicle_search(string type, string value)
        {
            WebController CoverageCheck = new WebController();
            Vehicle vehicle = new Vehicle();
            bool IsInsured = false;

            switch (type)
            {
                case "chs":
                    vehicle = VehicleModel.GetVehicleByChassis(value);
                    //IsInsured = PolicyModel.IsVehicleCovered(vehicle.chassisno, vehicle.VehicleRegNumber);
                    try { IsInsured = bool.Parse(CoverageCheck.CheckCoverage(vehicle.chassisno, vehicle.VehicleRegNumber)["VehicleCoverage"][0]["Vehicle_Covered"].ToString()); }
                    catch (Exception) { }
                    break;
                case "lcn":
                    vehicle = VehicleModel.GetVehicleByLicenseNo(value);
                    try { IsInsured = bool.Parse(CoverageCheck.CheckCoverage(vehicle.chassisno, vehicle.VehicleRegNumber)["VehicleCoverage"][0]["Vehicle_Covered"].ToString()); }
                    catch (Exception) { }
                    break;
                default:
                    break;
            }
            return Json(new { chassis_no = vehicle.chassisno, license_no = vehicle.VehicleRegNumber, insurance_status = IsInsured.ToString() }, JsonRequestBehavior.AllowGet);
        } 
    }
}
