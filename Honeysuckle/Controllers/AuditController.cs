﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class AuditController : Controller
    {
        //
        // GET: /Audit/

        public ActionResult Index()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("AuditLog", "Read")) && UserGroupModel.IsAdministrative(Honeysuckle.Models.Constants.user))
            {
                Audit audit = new Audit();
                //audit = AuditModel.GetAudit(id);
                return View(audit);
                
               
            }
            else
            {
                TempData["permissionMessage"] = "You need to have permission to read the Audit Log and be an administrator";
                return RedirectToAction("Index", "Home");
            }
            
        }


        public ActionResult View(int id = 0)
        {
            if ( id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("AuditLog", "Read")) && UserGroupModel.IsAdministrative(Honeysuckle.Models.Constants.user))
            {
                
                    Audit a = AuditModel.GetAudit(id);
                    ViewData["location"] = a.table.ToUpper();
                    return View(a);
                
            }
            else
            {
                TempData["permissionMessage"] = "You need to have permission to read the Audit Log and be an administrator";
                return RedirectToAction("Index", "Home");
            }

        }


        public ActionResult AuditLog(string location = null, int id = 0)
        {
            if (id != 0 && location != null) return View(AuditModel.GetAuditLog(location, id));
            else return new EmptyResult();
        }

        public ActionResult Audit(int id = 0)
        {
            return View(new Audit(id));
        }

        public ActionResult AuditShowData(int id = 0, bool orig = false)
        {
            if (id != 0)
            {
                ViewData["orig_data"] = orig;
                return View(AuditModel.GetAudit(id));
            }
            else return new EmptyResult();
        }

        public ActionResult ShortCertificate()
        {

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Read"))) return View(AuditModel.GetAuditByUser(Constants.user.ID, "vehiclecertificate"));
            else return new EmptyResult();

        }
        public ActionResult ShortCoverNote()
        {

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Read"))) return View(AuditModel.GetAuditByUser(Constants.user.ID, "vehiclecovernote"));
            else return new EmptyResult();

        }
        public ActionResult ShortPolicy()
        {

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Read"))) return View(AuditModel.GetAuditByUser(Constants.user.ID, "policy"));
            else return new EmptyResult();

        }
        public ActionResult ShortCompany()
        {

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Company", "Read"))) return View(AuditModel.GetAuditByUser(Constants.user.ID, "company"));
            else return new EmptyResult();

        }
        public ActionResult ShortPeople()
        {

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("People", "Read"))) return View(AuditModel.GetAuditByUser(Constants.user.ID, "person"));
            else return new EmptyResult();

        }
        public ActionResult ShortVehicle()
        {

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read"))) return View(AuditModel.GetAuditByUser(Constants.user.ID, "vehicle"));
            else return new EmptyResult();

        }

    }
}
