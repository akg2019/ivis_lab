﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Honeysuckle.Models;
using Newtonsoft.Json;
using System.Web.ClientServices;
//using OmaxFramework;
namespace Honeysuckle.Controllers
{
    public class ImportController : Controller
    {
        //
        // GET: /Import/

        [HttpPost]
        public ActionResult Webservice()
        {
            try
            {
                Import import = new Import();
                JSON json = new JSON();
                //string t = OmaxFramework.Utilities.FileHandler.ReadText("C:\\Users\\Joel\\Documents\\ivis-honeysuckle\\i.txt");
                try //test if we can read the input stream and it doesn't over load the system
                {
                    Stream req = Request.InputStream;
                    req.Seek(0, System.IO.SeekOrigin.Begin);
                    json.json = new StreamReader(req).ReadToEnd();
                }
                catch
                {
                    json.json = Request["policy_data"];
                    return Json(new { success = false, message = "Request string exceeds recommended length. Please resend with a smaller request string. We recommend less than 500 records at a time.", status = "maximum length exceeded" }, JsonRequestBehavior.DenyGet);
                }
                

                try //test if input stream is a valid json structure
                {
                    JSON js = new JSON();
                    string output = import.ParseJSONAuth(json, "import");
                    switch (output)
                    {
                        case "inprogress":
                            return Json(new { count = json.count, success = true, status = "inprogress", comment = "We are still processing your previous request. Please try back shortly" }, JsonRequestBehavior.DenyGet);
                        case "completed":
                            js = Constants.JsonResponse;
                            Constants.JsonResponse = new JSON();
                            return Json(new { success = true, status = "completed", errors = JsonConvert.DeserializeObject<List<error>>(js.processedjson), comment = "send more requests now if you want" }, JsonRequestBehavior.DenyGet);
                        case "fail":
                            js = Constants.JsonResponse;
                            Constants.JsonResponse = new JSON();
                            return Json(new { success = true, status = "completed", errors = JsonConvert.DeserializeObject<List<error>>(json.processedjson), comment = "send more requests now if you want" }, JsonRequestBehavior.DenyGet);
                        case "valid":
                            json = import.ParseJSONThreadHandler(json, Constants.user.ID);
                            return Json(new { success = true, status = "received", reqkey = RSA.Encrypt(json.reqkey, "request", Constants.user.ID, import.returnKid(json.json)) }, JsonRequestBehavior.DenyGet);
                        case "loggedin":
                            return Json(new { success = true, key = RSA.Encrypt(UserModel.GetSessionGUID(Constants.user), "session"), confirmation_code = "yes", comment = "logged in successfully", kid = Constants.rsa.id }, JsonRequestBehavior.DenyGet);
                        case "loggedout":
                            return Json(new { success = true, confirmation_code = "yes", comment = "logged out successfully" }, JsonRequestBehavior.DenyGet);
                        case "bad key":
                            return Json(new { success = false, confirmation_code = "yes", comment = "you did something wrong" }, JsonRequestBehavior.DenyGet); // for when request key is bad
                        default:
                            return Json(new { success = false, status = "bad session key", confirmation_code = "yes", comment = "failed authentication" }, JsonRequestBehavior.DenyGet); // for when session key is bad
                    }
                }
                catch (Exception)
                {
                    return Json(new { success = false, status = "invalid json", confirmation_code = "yes", comment = "didn't read parameter" }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "Request string exceeds recommended length. Please resend with a smaller request string. We recommend less than 500 records at a time.", status = "maximum length exceeded" }, JsonRequestBehavior.DenyGet);
            }
        }

    }
}
