﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel.DataAnnotations;


namespace Honeysuckle.Models
{
    public class Address
    {
        public int ID { get; set; }
        public string short_code { get; set; } //this is used as it relates to policy addresses, not for general usage

        [RegularExpression("(([a-zA-Z0-9])[ a-zA-Z0-9-,/& ]*[a-zA-Z0-9 ]*)", ErrorMessage = "Incorrect road-number format (numbers, letters and hyphens only)")]
        public string roadnumber { get; set; }
        [RegularExpression("(([a-zA-Z0-9&,#. '-])*)?", ErrorMessage = "Incorrect apartment number format (some symbols are not allowed)")]
        public string ApartmentNumber { get; set; }
        public Road road { get; set; }
        public RoadType roadtype { get; set; }
        public ZipCode zipcode { get; set; }
        public City city { get; set; }
        public Parish parish { get; set; }
        public Country country { get; set; }
        public string longAddress { get; set; }
        public bool longAddressUsed { get; set; }
        
        public Address() { }
        public Address(int ID)
        {
            this.ID = ID;
        }
        public Address(int ID, string roadnumber, string aptNumber, Road road, RoadType roadtype, ZipCode zipcode, City city, Parish parish, Country country)
        {
            this.ID = ID;
            this.roadnumber = roadnumber;
            this.road = road;
            this.roadtype = roadtype;
            this.zipcode = zipcode;
            this.city = city;
            this.parish = parish;
            this.country = country;
            this.ApartmentNumber = aptNumber;
        }
        public Address(string roadnumber, string road, string roadtype, string zipcode, string city, string country)
        {
            this.roadnumber = roadnumber;
            this.road = new Road(road);
            this.roadtype = new RoadType(roadtype);
            this.zipcode = new ZipCode(zipcode);
            this.city = new City(city);
            this.country = new Country(country);
        }
        public Address(string roadnumber, string road, string roadtype, string zipcode, string city, string parish, string country)
        {
            this.roadnumber = roadnumber;
            this.road = new Road(road);
            this.roadtype = new RoadType(roadtype);
            this.zipcode = new ZipCode(zipcode);
            this.city = new City(city);
            this.parish = new Parish(parish);
            this.country = new Country(country);
        }
        public Address(string longAddress,bool longAddressUsed)
        {
            this.longAddress = longAddress;
            this.longAddressUsed = longAddressUsed;
        }

    }
    
    public class AddressModel
    {
        /// <summary>
        /// this function uses an address id to retrieve a fully realized address
        /// </summary>
        /// <param name="address">an address object with a valid id</param>
        /// <returns>a fully realized address object</returns>
        public static Address GetAddress(Address address)
        {
            Road road = new Road();
            RoadType roadtype = new RoadType();
            ZipCode zipcode = new ZipCode();
            City city = new City();
            Parish parish = new Parish();
            Country country = new Country();
            string roadnumber = null;
            string aptNumber = null;
            string LongAddress = null;
            bool IsLongAddressUsed = false;

            if (address != null && address.ID != 0)
            {

                sql sql = new sql();

                try
                {
                    if (sql.ConnectSQL())
                {
                    SqlDataReader reader = sql.QuerySQL("EXEC GetAddress " + address.ID);
                    while (reader.Read())
                    {

                        if (DBNull.Value != reader["roadnumber"]) roadnumber = reader["roadnumber"].ToString();
                        if (DBNull.Value != reader["AptNumber"]) aptNumber = reader["AptNumber"].ToString();
                        if (DBNull.Value != reader["roadname"]) road.RoadName = reader["roadname"].ToString();
                        if (DBNull.Value != reader["roadtype"]) roadtype.RoadTypeName = reader["roadtype"].ToString();
                        if (DBNull.Value != reader["zipcode"]) zipcode.ZipCodeName = reader["zipcode"].ToString();
                        if (DBNull.Value != reader["city"]) city.CityName = reader["city"].ToString();
                        if (DBNull.Value != reader["Parish"]) { parish.parish = reader["Parish"].ToString(); parish.ID = int.Parse(reader["PID"].ToString()); }
                        if (DBNull.Value != reader["country"]) country.CountryName = reader["country"].ToString();
                        if (DBNull.Value != reader["LongAddress"]) LongAddress = reader["LongAddress"].ToString();
                        if (DBNull.Value != reader["LongAddressUsed"]) IsLongAddressUsed  = Convert.ToBoolean(reader["LongAddressUsed"].ToString());
                        
                        
                       address = new Address(address.ID, roadnumber, aptNumber, road, roadtype, zipcode, city, parish, country);
                       if (IsLongAddressUsed)
                       {
                           address.longAddress = LongAddress;
                           address.longAddressUsed = IsLongAddressUsed;
                       }
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
                }
                catch (Exception e) { }
               
            }
            else
            {
                roadnumber = "Not Available";
                aptNumber = "";
                road.RoadName = "";
                roadtype.RoadTypeName = "";
                zipcode.ZipCodeName = "";
                city.CityName = "";
                parish.parish = ""; 
                parish.ID = 0; 
                country.CountryName = "";

                address = new Address(0, roadnumber, aptNumber, road, roadtype, zipcode, city, parish, country);
               
            }

            return address;

        }

        /// <summary>
        /// This function is used to add an address to the database. 
        /// This version is used by logged in users of the website
        /// </summary>
        /// <param name="address">a complete address object</param>
        /// <returns>the id of the address that has been created or found in the IVIS database</returns>
        public static int AddAddress(Address address)
        {
           return AddAddress(address, Constants.user.ID);
        }
        
        /// <summary>
        /// this function adds an address to the database and returns the ID existing
        /// or generating for that now added address in the system
        /// </summary>
        /// <param name="address">an address object with all related data</param>
        /// <returns>the id of the address</returns>
        public static int AddAddress(Address address, int uid) 
        {
            string query = "";
            int returnAddressId = 0;
            MappingData parishmap = new MappingData();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                if (address.roadnumber != null) address.roadnumber = Constants.CleanString(address.roadnumber);
                if (address.ApartmentNumber != null) address.ApartmentNumber = Constants.CleanString(address.ApartmentNumber);
                if (address.road != null && address.road.RoadName != null) address.road.RoadName = Constants.CleanString(address.road.RoadName);
                if (address.roadtype != null && address.roadtype.RoadTypeName != null) address.roadtype.RoadTypeName = Constants.CleanString(address.roadtype.RoadTypeName);
                //if (address.zipcode.ZipCodeName != null) address.zipcode.ZipCodeName = Constants.CleanString(address.zipcode.ZipCodeName);
                if (address.city != null && address.city.CityName != null) address.city.CityName = Constants.CleanString(address.city.CityName);
                if (address.parish != null && address.parish.parish != null) address.parish.parish = Constants.CleanString(address.parish.parish);
                if (address.parish != null && address.parish.ID == 0)
                {
                    address.parish = ParishModel.GetParish(address.parish.parish);
                    if (address.parish.ID == 0) parishmap = MappingDataModel.AddMappingData(new MappingData("parish", address.parish.parish), uid);
                }
                if (address.country != null && address.country.CountryName != null) address.country.CountryName = Constants.CleanString(address.country.CountryName);

                if (address.longAddressUsed == null || !address.longAddressUsed)
                {
                    query = "EXECUTE AddAddressToSystem '" + null + "','" + null + "'," + uid + ",'" +
                                                                   address.road.RoadName + "','" +
                                                                   address.roadtype.RoadTypeName + "', '" +
                                                                   null + "','" +
                                                                   address.city.CityName + "'," +
                                                                   address.parish.ID + ",'" +
                                                                   address.country.CountryName + "','" +
                                                                   address.roadnumber + "','" +
                                                                   address.ApartmentNumber + "'," +
                                                                   parishmap.id;
                }
                else if (address.longAddressUsed != null && address.longAddressUsed)
                {

                    query = "EXECUTE AddAddressToSystem '" + Constants.CleanString(address.longAddress) + "'," +
                                                            address.longAddressUsed + "," + uid;
                }

                SqlDataReader reader = sql.QuerySQL(query, "create", "address", address, uid);
                while (reader.Read())
                {
                    returnAddressId = int.Parse(reader["AID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return returnAddressId; 
        }

        /// <summary>
        /// this function is to trim the elements of an address such that unneeded spaces are not stored in the database
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public static Address TrimAddress(Address address)
        {
            if (address.road != null && address.road.RoadName != null) address.road.RoadName = address.road.RoadName.Trim();
            if (address.city != null && address.city.CityName != null) address.city.CityName = address.city.CityName.Trim();
            if (address.roadtype != null && address.roadtype.RoadTypeName != null) address.roadtype.RoadTypeName = address.roadtype.RoadTypeName.Trim();
            if (address.roadnumber != null) address.roadnumber = address.roadnumber.Trim();
            if (address.ApartmentNumber != null) address.ApartmentNumber = address.ApartmentNumber.Trim();

            return address;
        }
       
    }

}
