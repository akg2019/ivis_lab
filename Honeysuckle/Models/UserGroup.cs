﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class UserGroup
    {
        public int UserGroupID { get; set; }
        public string UserGroupName { get; set; }
        public bool SuperGroup { get; set; }
        public List<UserGroup> children { get; set; }
        public List<UserPermission> Permissions { get; set; }
        public Company company { get; set; }
        public bool system { get; set; }
        public bool stock { get; set; }
        public bool administrative { get; set; }

        public UserGroup() { }
        public UserGroup(int UserGroupID)
        {
            this.UserGroupID = UserGroupID;
        }
        public UserGroup(string UserGroupName, List<UserPermission> Permissions) 
        {
            this.UserGroupName = UserGroupName;
            this.Permissions = Permissions;
        }
        public UserGroup(string UserGroupName)
        {
            this.UserGroupName = UserGroupName;
        }
        public UserGroup(int UserGroupID, string UserGroupName)
        {
            this.UserGroupID = UserGroupID;
            this.UserGroupName = UserGroupName;
        }
        public UserGroup(int UserGroupID, string UserGroupName, Company company, bool system)
        {
            this.UserGroupID = UserGroupID;
            this.UserGroupName = UserGroupName;
            this.company = company;
            this.system = system;
        }
        public UserGroup(int UserGroupID, string UserGroupName, bool SuperGroup)
        {
            this.UserGroupID = UserGroupID;
            this.UserGroupName = UserGroupName;
            this.SuperGroup = SuperGroup;
        }
        public UserGroup(int UserGroupID, string UserGroupName, List<UserGroup> children)
        {
            this.UserGroupID = UserGroupID;
            this.UserGroupName = UserGroupName;
            this.children = children;
        }
    }

    public class UserGroupModel
    {

        /// <summary>
        /// this function tests to make sure the group has a unique group name for that company
        /// </summary>
        /// <param name="usergroup"></param>
        /// <returns></returns>
        public static bool TestUserGroupUnique(UserGroup usergroup)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC TestUserGroupUnique " + usergroup.UserGroupID + ",'" + Constants.CleanString(usergroup.UserGroupName) + "'," + usergroup.company.CompanyID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "usergroup", new UserGroup());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }
        
        /// <summary>
        /// this function tests if a group is administrative
        /// </summary>
        /// <param name="usergroup"></param>
        /// <returns></returns>
        private static bool IsAdministrative(UserGroup usergroup)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsAdministrative " + usergroup.UserGroupID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "usergroup", new UserGroup());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function tests if a user possesses a group that is administrative
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static bool IsAdministrative(User user)
        {
            bool result = false;
            foreach (UserGroup usergroup in user.UserGroups)
            {
                result = IsAdministrative(usergroup);
                if (result) break;
            }
            return result;
        }

        /// <summary>
        /// this function deletes all the user groups from a company
        /// </summary>
        /// <param name="company"></param>
        public static void DeleteAllUserGroupsForCompany(Company company)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DeleteAllUserGroupsForCompany " + company.CompanyID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "company", company, "usergroup", true);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function automatically copies all 'stock' groups to a company;
        /// this is run when a new company is created
        /// </summary>
        /// <param name="cid"></param>
        public static void CopyStockGroups(int cid = 0)
        {
            sql sql = new sql();
            if (cid != 0)
            {
                if (sql.ConnectSQL())
                {
                    string query = "EXEC CopyStockGroups " + cid + "," + Constants.user.ID;
                    SqlDataReader reader = sql.QuerySQL(query, "read", "usergroup", new UserGroup());
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
        }

        ///<summary>
        /// Removes all users from a specific usergroup
        ///</summary>
        public static void PurgeUsersFromGroup(UserGroup usergroup)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC PurgeUserGroup " + usergroup.UserGroupID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "usergroup", new UserGroup());
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        ///<summary>
        ///Returns a List of All UserGroups in the database
        ///</summary>
        public static List<UserGroup> GetAllUserGroups(int compid = 0)
        {
            List<UserGroup> usergroups = new List<UserGroup>();
            sql sql = new sql();

            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetUserGroups " + compid, "read", "usergroup", new UserGroup());
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        try
                        { 
                        UserGroup usergroup = new UserGroup(int.Parse(reader["UGID"].ToString()), reader["UserGroup"].ToString());
                        usergroup.company = new Company(int.Parse(reader["cid"].ToString()), reader["cname"].ToString());
                        usergroup.stock = (bool)reader["stock"];
                        usergroup.system = (bool)reader["system"];

                        usergroups.Add(usergroup);
                        }
                        catch (Exception) { }
                        
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            for(int i = 0; i < usergroups.Count(); i++)
            {
                usergroups[i].Permissions = UserPermissionModel.GetAllPermissionsForGroup(usergroups[i]);
            }


            return usergroups;
        }

        /// <summary>
        /// this function returns all the user groups that a user who is not a system
        /// administrator can view
        /// </summary>
        /// <returns></returns>
        public static List<UserGroup> GetAllUserGroupsNotSA()
        {
            List<UserGroup> usergroups = new List<UserGroup>();
            sql sql = new sql();

            if (sql.ConnectSQL())
            {
                string query = "";
                if (Constants.user.newRoleMode) query = "EXEC GetAllUserGroupsNotSA " + Constants.user.tempUserGroup.company.CompanyID;
                else query = "EXEC GetAllUserGroupsNotSA " + CompanyModel.GetMyCompany(Constants.user).CompanyID;

                SqlDataReader reader = sql.QuerySQL(query, "read", "usergroup", new UserGroup());

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        UserGroup usergroup = new UserGroup(int.Parse(reader["UGID"].ToString()), reader["UserGroup"].ToString());
                        usergroup.company = new Company(int.Parse(reader["cid"].ToString()), reader["cname"].ToString());
                        usergroup.stock = (bool)reader["stock"];
                        usergroup.system = (bool)reader["system"];

                        usergroups.Add(usergroup);
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return usergroups;
        }

        ///<summary>
        ///Tests if the User is a "System Administrator"
        ///</summary>
        public static bool AmAnAdministrator(User user)
        {
            bool result = false;

            //If the user has a temporary role, then the user is an Admin (but the result is false, as the user is interfacing based on on the temp role)
            if (user.newRoleMode) return result; 

            user.UserGroups = GetMyUserGroups(user);
            foreach (UserGroup ug in user.UserGroups)
            {
                if (ug.UserGroupName == "System Administrator" && ug.system)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        ///<summary>
        ///Tests if the User is a "IAJ Administrator"
        ///</summary>
        public static bool AmAnIAJAdministrator(User user)
        {
            bool result = false;

            if (user.newRoleMode) //If the user is in a temp role, then, only the temp role is to be tested
            {
                if (user.tempUserGroup.UserGroupName == "IAJ Administrator" && user.tempUserGroup.system)
                {
                    result = true;
                }
            }

            else
            {
                user.UserGroups = GetMyUserGroups(user);
                foreach (UserGroup ug in user.UserGroups)
                {
                    if (ug.UserGroupName == "IAJ Administrator" && ug.system)
                    {
                        result = true;
                        break;
                    }
                }
            }
            return result;
        }

        ///<summary>
        ///ADDS A USER FROM THIS USERGROUP
        ///</summary>
        public static void AssociateUserGroup(UserGroup usergroup, User user)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC AssociateUserGroup " + usergroup.UserGroupID + "," + user.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "user", user);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        ///<summary>
        ///ADDS A USER FROM THIS LIST OF USERGROUPS
        ///</summary>
        public static void AssiociateUserGroups(List<UserGroup> usergroups, User user)
        {
            foreach(UserGroup usergroup in usergroups)
            {
                AssociateUserGroup(usergroup, user);
            }
        }

        ///<summary>
        ///REMOVES A USER FROM THIS USERGROUP
        ///</summary>
        public static void DisassociateUserGroup(UserGroup usergroup, User user)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DisassociateUserGroup " + usergroup.UserGroupID + "," + user.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "user", user);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        ///<summary>
        ///REMOVES A USER FROM THIS LIST OF USERGROUPS
        ///</summary>
        public static void DisassociateUserGroups(List<UserGroup> usergroups, User user)
        {
            foreach (UserGroup usergroup in usergroups)
            {
                DisassociateUserGroup(usergroup, user);
            }
        }

        ///<summary>
        ///THIS FUNCTION RETURNS A LIST OF GROUPS THAT 
        ///THE USER IN QUESTION IS NOT ASSOCIATED TO
        ///</summary>
        public static List<UserGroup> GetNotMyUserGroups(User user)
        {
            List<UserGroup> usergroups = new List<UserGroup>();
            sql sql = new sql();

            if (sql.ConnectSQL())
            {
                string query = "EXEC GetNotMyUserGroups " + user.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "usergroup", new UserGroup());
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        usergroups.Add(new UserGroup(int.Parse(reader["UGID"].ToString()), reader["UserGroup"].ToString(), new Company(int.Parse(reader["company"].ToString())), (bool)reader["system"]));
                    }
                    reader.Close();
                }

                sql.DisconnectSQL();
            }
            return usergroups;
        }
        
        ///<summary>
        ///THIS FUNCTION RETURNS A LIST OF GROUPS THAT 
        ///THE USER IN QUESTION IS ASSOCIATED TO
        ///</summary>
        public static List<UserGroup> GetMyUserGroups(User user)
        {
            List<UserGroup> usergroups = new List<UserGroup>();
            sql sql = new sql();

            if (sql.ConnectSQL())
            {
                string query = "EXEC GetMyUserGroups " + user.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "usergroup", new UserGroup());
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        usergroups.Add(new UserGroup(int.Parse(reader["UGID"].ToString()), reader["UserGroup"].ToString(), new Company(int.Parse(reader["company"].ToString())), (bool)reader["system"]));
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return usergroups;
        }

        ///<summary>
        ///Returns a boolean which represents if the User is a Member of the UserGroup
        ///</summary>
        public static bool TestUserGroup(User user, UserGroup usergroup)
        {
            bool result = false;

            if (user.newRoleMode) //If the user is in a temp role, then, only the temp role is to be tested
            {
                if (user.tempUserGroup.UserGroupName == usergroup.UserGroupName)
                {
                    result = true;
                }
            }

            else
            {
                user.UserGroups = GetMyUserGroups(user);
                foreach (UserGroup mygroup in user.UserGroups)
                {
                    if (mygroup.UserGroupName == usergroup.UserGroupName)
                    {
                        result = true;
                        break;
                    }
                }
            }
            return result;
        }

        ///<summary>
        ///Gets the list of child groups associated to a SuperGroup in question
        ///</summary>
        public static List<UserGroup> GetChildren(UserGroup usergroup)
        {
            List<UserGroup> children = new List<UserGroup>();
            if (usergroup.UserGroupID != 0)
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    string query = "EXEC GetGroupChildren " + usergroup.UserGroupID;
                    SqlDataReader reader = sql.QuerySQL(query, "read", "usergroup", new UserGroup());
                    while (reader.Read())
                    {
                        children.Add(new UserGroup(int.Parse(reader["UGID"].ToString()), reader["UserGroup"].ToString(), (bool)reader["SuperGroup"]));
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            return children;
        }

        ///<summary>
        ///Input allgroups and the list of groups (notgroups) that you wish to be removed from the allgroups list and be given the end product
        ///</summary>
        //public static List<UserGroup> NotTheseChildren(List<UserGroup> allgroups, List<UserGroup> notgroups)
        //{
            

        //    for (int i = allgroups.Count - 1; i >= 0; i--)
        //    {
        //        foreach (UserGroup ug in notgroups)
        //        {
        //            if (allgroups[i].UserGroupID == ug.UserGroupID)
        //            {
        //                allgroups.RemoveAt(i);
        //                break;
        //            }
        //        }
        //    }
        //    return allgroups;
        //}

        /// <summary>
        /// this returns a list of usergroups that are available and are not a part of the list of user groups provided
        /// </summary>
        /// <param name="usergroup">list of usergroups you don't want to return</param>
        /// <returns>list of rest of user groups</returns>
        public static List<UserGroup> GetNotTheseGroups(List<UserGroup> usergroups, int compid)
        {
            List<UserGroup> notgroups = GetAllUserGroups(compid);
            foreach (UserGroup group in usergroups)
            {
                for (int i = notgroups.Count - 1; i >= 0; i--)
                {
                    if (notgroups[i].UserGroupID == group.UserGroupID) notgroups.RemoveAt(i);
                }
            }
            return notgroups;
        }

        ///<summary>
        ///Gets the list of groups in the system that are not associated as children to the Usergroup in question
        ///</summary>
        public static List<UserGroup> GetNotChildren(UserGroup usergroup, int company_id = 0)
        {
            List<UserGroup> notchildren = new List<UserGroup>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetNotGroupChildren " + usergroup.UserGroupID;
                if (company_id != 0) query += "," + company_id;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    if (int.Parse(reader["UGID"].ToString()) != usergroup.UserGroupID) notchildren.Add(new UserGroup(int.Parse(reader["UGID"].ToString()), reader["UserGroup"].ToString(), (bool)reader["SuperGroup"]));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return notchildren;
        }

        ///<summary>
        ///Gets the remainder of the group details when you give it a UserGroup object with just a GroupName
        ///</summary>
        public static UserGroup GetUserGroup(UserGroup usergroup)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetUserGroupDetails '" + Constants.CleanString(usergroup.UserGroupName) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "usergroup", new UserGroup());
                while (reader.Read())
                {
                    usergroup.UserGroupID = int.Parse(reader["UGID"].ToString());
                    usergroup.SuperGroup = (bool)reader["SuperGroup"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return usergroup;
        }

        /// <summary>
        /// this function returns all usergroup data for a specified usergroup
        /// </summary>
        /// <param name="usergroup"></param>
        /// <param name="company"></param>
        /// <returns></returns>
        public static UserGroup GetUserGroup(UserGroup usergroup, Company company)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetUserGroupDetails '" + Constants.CleanString(usergroup.UserGroupName) + "'," + company.CompanyID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "usergroup", new UserGroup());
                while (reader.Read())
                {
                    usergroup.UserGroupID = int.Parse(reader["UGID"].ToString());
                    usergroup.SuperGroup = (bool)reader["SuperGroup"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return usergroup;
        }

        ///<summary>
        ///Creates a new UserGroup in the database
        ///</summary>
        public static bool CreateUserGroup(UserGroup usergroup)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC CreateNewUserGroup '" + Constants.CleanString(usergroup.UserGroupName) + "'," + usergroup.SuperGroup + "," + usergroup.administrative + "," + Constants.user.ID + "," + usergroup.company.CompanyID;
                SqlDataReader reader = sql.QuerySQL(query, "create", "usergroup", usergroup);
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        result = (bool)reader["result"];

                        if (result)
                            usergroup.UserGroupID = int.Parse(reader["GroupID"].ToString());
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        ///<summary>
        ///Assigns a list of "children" to a "parent" Group in the database
        ///</summary>
        public static void CreateSuperGroup(UserGroup parent, List<UserGroup> children)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                if (parent.UserGroupID == 0)
                {
                    UserGroupModel.GetUserGroup(parent);
                    parent.children = children;
                }
                foreach (UserGroup child in children)
                {
                    if (child.UserGroupID == 0) UserGroupModel.GetUserGroup(child);
                    string query = "EXEC AttachChildToSuperGroup " + parent.UserGroupID + "," + child.UserGroupID + "," + Constants.user.ID;
                    SqlDataReader reader = sql.QuerySQL(query, "update", "usergroup", parent);
                    reader.Close();
                }

                sql.DisconnectSQL();
            }
        }

        ///<summary>
        ///Given the UserGroupName in the UserGroup 
        ///</summary>
        public static void UpdateUserGroupPermissions(UserGroup usergroup)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                foreach (UserPermission userpermission in usergroup.Permissions)
                {
                    string query = "EXEC UpdateUserGroupPermissions " + usergroup.UserGroupID + ",'"
                                                                      + Constants.CleanString(userpermission.table) + "','"
                                                                      + Constants.CleanString(userpermission.action) + "',"
                                                                      + Constants.user.ID;
                    SqlDataReader reader = sql.QuerySQL(query, "update", "usergroup", usergroup);
                    reader.Close();
                }
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function determines which permissions need to be added and which to be removed from a user group
        /// </summary>
        /// <param name="usergroup"></param>
        public static void UpdatePermissions(UserGroup usergroup)
        {
            List<UserPermission> newpermissions = usergroup.Permissions;
            List<UserPermission> oldpermissions = UserPermissionModel.GetAllPermissionsForGroup(usergroup, usergroup.system);

            List<UserPermission> permissions_to_delete = new List<UserPermission>();
            List<UserPermission> permissions_to_add = new List<UserPermission>();

            //looking for permissions to delete
            for (int i = 0; i < oldpermissions.Count(); i++)
            {
                bool keep = false;
                for (int j = 0; j < newpermissions.Count(); j++)
                {
                    keep = (oldpermissions.ElementAt(i).id == newpermissions.ElementAt(j).id);
                    if (keep) break;
                }
                if (!keep) permissions_to_delete.Add(oldpermissions.ElementAt(i));
            }

            //looking for permissions to add
            for (int i = 0; i < newpermissions.Count(); i++)
            {
                bool keep = false;
                for (int j = 0; j < oldpermissions.Count(); j++)
                {
                    keep = (newpermissions.ElementAt(i).id == oldpermissions.ElementAt(j).id);
                    if (keep) break;
                }
                if (!keep) permissions_to_add.Add(newpermissions.ElementAt(i));
            }

            UserPermissionModel.RemoveUserPermissionsFromGroup(permissions_to_delete, usergroup);
            UserPermissionModel.AddUserPermissionsToGroup(permissions_to_add, usergroup);
        }

        ///<summary>
        ///Given an integer as the UserGroupID 
        ///This function will return all relevant data
        ///for the UserGroup, including the Permissions
        ///</summary>
        ///<param name="UGID">A valid UserGroup ID that exists in the database</param>
        public static UserGroup GetUserGroup(int UGID)
        {
            UserGroup usergroup = new UserGroup();
            List<UserPermission> permissions = new List<UserPermission>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                SqlDataReader reader = sql.QuerySQL("EXEC GetUserGroup " + UGID, "read", "usergroup", new UserGroup());
                while (reader.Read())
                {
                    usergroup.UserGroupName = reader["UserGroup"].ToString();
                    usergroup.SuperGroup = (bool)reader["SuperGroup"];
                    usergroup.administrative = (bool)reader["administrative"];
                    usergroup.stock = (bool)reader["stock"];
                    usergroup.system = (bool)reader["system"];
                    usergroup.company = new Company(int.Parse(reader["cid"].ToString()), reader["cname"].ToString());
                }
                usergroup.UserGroupID = UGID;
                reader.Close();
                sql.DisconnectSQL();
                usergroup.Permissions = UserPermissionModel.GetAllPermissionsForGroup(usergroup);
            }
            return usergroup;

        }

        ///<summary>
        ///Updates the UserGroup Name and Permissions associated to it
        ///</summary>
        //public UserGroup UpdateUserGroup(UserGroup usergroup)
        //{
            
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        SqlDataReader reader = sql.QuerySQL("EXEC UpdateUserGroupData " + usergroup.UserGroupID + ",'" + Constants.CleanString(usergroup.UserGroupName) + "'," + Constants.user.ID);
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    foreach (UserPermission up in usergroup.Permissions)
        //    {
        //        UserPermissionModel.UpdateUserGroupPermission(usergroup, up);
        //    }
        //    return usergroup;
        //}

        ///<summary>
        ///Removes a UserGroup from the database
        ///</summary>
        public static bool DeleteUserGroup(int UserGroupID)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DeleteUserGroup " + UserGroupID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "usergroup", new UserGroup(UserGroupID));
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// Adds a list of UserGroups with the List of ids handed
        /// </summary>
        /// <param name="groups">List of base UserGroups</param>
        /// <param name="ids">List of ids of UserGroups you would like added to the List of User Groups</param>
        /// <returns>Final List of UserGroups</returns>
        public static List<UserGroup> AddGroupToList(List<UserGroup> groups, List<int> ids)
        {
            foreach (int i in ids)
            {
                groups.Add(GetUserGroup(i));
            }
            return groups;
        }

        /// <summary>
        /// Remove a series of UserGroups with the ids in the list of ints
        /// </summary>
        /// <param name="groups">List of UserGroups you would like to parse through</param>
        /// <param name="ids">List of ids you would like removed from the List of UserGroups</param>
        /// <returns>List of Groups remaining</returns>
        //public static List<UserGroup> RemoveGroupFromList(List<UserGroup> groups, List<int> ids)
        //{
        //    foreach (int i in ids)
        //    {
        //        for (int j = groups.Count - 1; j >= 0; j--)
        //        {
        //            if (groups[j].UserGroupID == i) groups.RemoveAt(j);
        //        }
        //    }
        //    return groups;
        //}

        /// <summary>
        /// Add a single child to a supergroup
        /// </summary>
        /// <param name="supergroup">valid usergroup that is a supergroup</param>
        /// <param name="child">valid usergroup</param>
        public static void AddChildToSuperGroup(UserGroup supergroup, UserGroup child)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                //supergroup.children.Add(child);
                string query = "EXEC AddChildToSuperGroup " + supergroup.UserGroupID + "," + child.UserGroupID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "usergroup", supergroup);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function is about adding a list of ids to a supergroup
        /// </summary>
        /// <param name="supergroup">Super Group that children are being added to</param>
        /// <param name="children">List of ids of valid groups to be added to the SuperGroup</param>
        public static void AddChildrenToSuperGroup(UserGroup supergroup, List<int> children)
        {
            foreach (int child in children)
            {
                AddChildToSuperGroup(supergroup, new UserGroup(child));
            }
        }

        /// <summary>
        /// Remove a child from a supergroup
        /// </summary>
        /// <param name="supergroup">valid usergroup that is a supergroup</param>
        /// <param name="child">a valid usergroup that is a child of the supergroup</param>
        public static void RemoveChildFromSuperGroup(UserGroup supergroup, UserGroup child)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC RemoveChildFromSuperGroup " + supergroup.UserGroupID + "," + child.UserGroupID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "usergroup", child);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// Remove a list of ids from the supergroup as children
        /// </summary>
        /// <param name="supergroup">a valid usergroup that is a supergroup</param>
        /// <param name="children">a list of ids that are valid groups that are children of the supergroup</param>
        public static void RemoveChildrenFromSuperGroup(UserGroup supergroup, List<int> children)
        {
            foreach (int child in children)
            {
                RemoveChildFromSuperGroup(supergroup, new UserGroup(child));
            }
        }

        /// <summary>
        /// Editing the name of a user group
        /// </summary>
        /// <param name="userGroupName">User group name</param>
        public static void EditGroupName(int userGroupID, string userGroupName)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC EditGroupName " + userGroupID + ",'" + Constants.CleanString(userGroupName) + "'," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "usergroup", new UserGroup(userGroupID, userGroupName));
                reader.Close();
                sql.DisconnectSQL();
            }
           
        }

        /// <summary>
        /// test if group belongs to my company
        /// </summary>
        /// <param name="usergroup"></param>
        /// <returns></returns>
        public static bool is_my_group(UserGroup usergroup)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsMyGroup " + usergroup.UserGroupID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
            }
            return result;
        }
    }
}