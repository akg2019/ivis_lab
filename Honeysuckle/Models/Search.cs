﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class Search
    {
        public int ID { get; set; }
        public string table { get; set; }
        public string name { get; set; }
        public Policy policy { get; set; }
        public VehicleCertificate certificate { get; set; }
        public VehicleCoverNote covernote { get; set; }
        public Person person { get; set; }
        public Company company { get; set; }
        public Vehicle vehicle { get; set; }
        public User user { get; set; }


        // THIS IS USED FOR JSON PURPOSES
        public List<int> ids { get; set; }

        public Search() { }
        public Search(int ID, string table, string name)
        {
            this.ID = ID;
            this.table = table;
            this.name = name;
        }
        public Search(string table, List<int> ids)
        {
            this.table = table;
            this.ids = ids;
        }
        public Search(string table)
        {
            this.table = table;
        }
    }
    public class SearchModel
    {

        /// <summary>
        ///  this function returns the search that has table matching the specified table
        /// </summary>
        /// <param name="search"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        public static List<Search> GetSearch(List<Search> search, string table)
        {
            List<Search> sea = new List<Search>();
            sea = search.Where(x => x.table == table).ToList();
            return sea;
        }


        /// <summary>
        /// this function searches the database for any and all data
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public static List<Search> GeneralSearchNoLimit(string search_text, string Chassiss = "", string Reg = "", string Company = "", bool policysearch = false)
        {


            

            List<Search> results = new List<Search>();
            List<int> ids = new List<int>();
            List<string> vehicleChassis = new List<string>();
            List<string> companyTRN = new List<string>();
            List<string> peopleTRN = new List<string>();
            //int i = 0;
            //string type = null; string prevtype = null;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                if (Chassiss == "") Chassiss = Reg;
                if (Chassiss == "null") Chassiss = @"""";
                if (Reg == "null") Reg = @"""";
                if (Company == "null") Company = @"""";


                //Check for invalid characters in search text. Note: This will fail if the parameters are null? Null Reference Exception
                try
                {
                    Chassiss = Chassiss.Trim().Replace("-", "");
                    Reg = Reg.Trim().Replace("-", "");
                }
                catch (Exception)
                {
                    //Null Reference exception is thrown so by elimination we know that the search must be from the general box.
                    //Re-initialize variable so that they do not fail down the line
                    try
                    {
                        Chassiss = "";
                        Reg = "";

                    }
                    catch (Exception)
                    {

                    }
                }


                //Elimination and sorting
                if (Reg == "" && Chassiss == "" && Company != "")
                {
                    //If true then this is a company name search
                    if (search_text.Split(' ').Count() > 1)
                    {
                        Company = search_text;
                    }
                    //Reg search
                    else if (search_text.Length <= 7)
                    {
                        try { Reg = search_text.Trim().Replace("-", "");} // Company = ""; }
                        catch (Exception) { }
                    }
                    //Its a chassis number search
                    else
                    {
                        try { Chassiss = search_text.Trim().Replace("-", "");} // Company = ""; }
                        catch (Exception) { }
                    }

                }

                //OLD Query [HEAVY]
                //string q = @"exec GeneralSearchNoLimitReconStruct '""',1069,'""','""','Shevan Anderson'";

                string query = "";


                if (!string.IsNullOrEmpty(Chassiss))
                {
                    Chassiss = "'" + Chassiss + "'";
                }
                else
                {
                    Chassiss = @"'""""'";
                }


                if (!string.IsNullOrEmpty(Reg))
                {
                    Reg = "'" + Reg + "'";
                }
                else
                {
                    Reg = @"'""""'";
                }

                if (!string.IsNullOrEmpty(Company))
                {
                    // Company = "'" + Company + "'";
                }
                else
                {
                    // Company = @"'""""'";
                }


                //query = "EXEC GeneralSearchNoLimit '" + Constants.CleanString(search_text) + "'," + Constants.user.ID;

                query = @"exec GeneralSearchNoLimitReconStruct '""""'," + Constants.user.ID.ToString() + "," + Chassiss + "," + Reg + ", " + Company;
                //query = @"exec GeneralSearchNoLimitReconStructNew '""""'," + Constants.user.ID.ToString() + "," + Chassiss + "," + Reg + ", " + Company;

                if (Chassiss == "'undefined'" || Reg == "'undefined'")
                {
                    //query = "EXEC GeneralSearchNoLimit '" + Constants.CleanString(search_text) + "'," + Constants.user.ID;

                    query = @"exec GeneralSearchNoLimitReconStruct '""""'," + Constants.user.ID.ToString() + "," + search_text + "," + search_text + ", " + search_text;

                }

                if(policysearch)
                    query = @"exec GeneralSearchNoLimitFromPolicyNumber "+ search_text +"," + Constants.user.ID.ToString() + "," + Company;
                else                    
                    query = @"exec GeneralSearchNoLimitFromVehicleData "+ search_text +"," + Constants.user.ID.ToString() + "," + Company;
                    
                 
                SqlDataReader reader = sql.QuerySQL(query);
                if (reader != null)
                {
                    Company C = CompanyModel.GetCompany(Constants.user.employee.company.CompanyID);

                    while (reader.Read())
                    {
                        Search search = new Search();
                        switch (reader["table"].ToString())
                        {
                            case "certificate":
                                search.ID = int.Parse(reader["ID"].ToString());
                                search.table = reader["table"].ToString();
                                search.certificate = new VehicleCertificate();
                                search.certificate.VehicleCertificateID = int.Parse(reader["ID"].ToString());
                                search.certificate.CertificateNo = reader["certificate_no"].ToString();
                                search.certificate.CertificateType = reader["certificate_type"].ToString();
                                try { search.certificate.EffectiveDate = DateTime.Parse(reader["certificate_eff"].ToString()); }
                                catch (Exception)
                                {
                                    try
                                    {
                                        search.certificate.EffectiveDate = DateTime.Parse(reader["certificate_eff_date"].ToString());
                                    }
                                    catch (Exception)
                                    {


                                    }
                                }

                                //search.certificate.company = new Company(reader["certificate_company_name"].ToString());
                                search.certificate.company = C;
                                search.certificate.policy = new Policy();
                                //policyId is sometimes an empty string
                                var policyId = string.IsNullOrEmpty(reader["certificate_policy_id"].ToString()) ? "0" : reader["certificate_policy_id"].ToString();
                                search.certificate.policy.ID = int.Parse(policyId);
                                search.certificate.Risk = new Vehicle();
                                var riskId = 0;
                                int.TryParse(reader["certificate_risk_id"].ToString(), out riskId);
                                search.certificate.Risk.ID = riskId;
                                search.certificate.Risk.VehicleRegNumber = reader["risk_reg_no"].ToString();
                                break;
                            case "company":
                                if (!companyTRN.Contains(reader["company_trn"].ToString()))
                                {
                                    search.table = "company";
                                    search.ID = int.Parse(reader["ID"].ToString());
                                    search.company = new Company();
                                    search.company.CompanyID = int.Parse(reader["ID"].ToString());
                                    search.company.Enabled = (bool)reader["company_enable"];
                                    search.company.IAJID = reader["company_iajid"].ToString();
                                    search.company.CompanyName = reader["company_name"].ToString();
                                    search.company.CompanyType = reader["company_type"].ToString();
                                    search.company.trn = reader["company_trn"].ToString();
                                    search.company.createdBy = int.Parse(reader["CreatedBy"].ToString());
                                    companyTRN.Add(reader["company_trn"].ToString());
                                }
                                break;
                            case "covernote":
                                search.table = "covernote";
                                search.ID = int.Parse(reader["id"].ToString());
                                search.covernote = new VehicleCoverNote();
                                search.covernote.ID = int.Parse(reader["id"].ToString());
                                search.covernote.covernoteid = reader["covernote_no"].ToString();
                                search.covernote.effectivedate = DateTime.Parse(reader["covernote_eff"].ToString());
                                search.covernote.expirydate = DateTime.Parse(reader["covernote_expiry"].ToString());
                                search.covernote.Policy = new Policy(int.Parse(reader["covernote_policy_id"].ToString()));
                                //This does not seem to be relevant and cannot be returned from the improved search
                                //search.covernote.imported = (bool)reader["imported"];
                                search.covernote.risk = new Vehicle(int.Parse(reader["covernote_risk_id"].ToString()));
                                search.covernote.company = new Company(reader["covernote_company_name"].ToString());
                                search.covernote.Policy.insuredby = new Company(reader["covernote_company_name"].ToString());
                                break;
                            case "people":
                                if (!peopleTRN.Contains(reader["peo_trn"].ToString()))
                                {
                                    try
                                    {
                                        search.table = "people";
                                        search.ID = int.Parse(reader["id"].ToString());
                                        search.person = new Person(int.Parse(reader["person_id"].ToString()),
                                                                   reader["peo_fname"].ToString(),
                                                                   reader["peo_lname"].ToString(),
                                                                   reader["peo_trn"].ToString());
                                        search.person.mname = reader["peo_mname"].ToString();
                                        peopleTRN.Add(reader["peo_trn"].ToString());
                                    }
                                    catch (Exception)
                                    {

                                    }

                                }
                                break;
                            case "policy":
                                search.table = "policy";
                                search.ID = int.Parse(reader["id"].ToString());
                                search.policy = new Policy(int.Parse(reader["id"].ToString()));
                                search.policy.policyNumber = reader["policy_no"].ToString();
                                search.policy.startDateTime = DateTime.Parse(reader["policy_start"].ToString());
                                search.policy.endDateTime = DateTime.Parse(reader["policy_end"].ToString());
                                search.policy.insuredType = reader["policy_insured_type"].ToString();
                                search.policy.policyCover = new PolicyCover();
                                search.policy.policyCover.prefix = reader["policy_cover_prefix"].ToString();
                                break;
                            case "user":
                                search.table = "user";
                                search.ID = int.Parse(reader["id"].ToString());
                                search.user = new User(int.Parse(reader["id"].ToString()));
                                search.user.username = reader["user_name"].ToString();
                                search.user.employee = new Employee();
                                search.user.employee.company = new Company(reader["user_company_name"].ToString());
                                search.user.employee.person = new Person();
                                search.user.employee.person.fname = reader["people_fname"].ToString();
                                search.user.employee.person.lname = reader["people_lname"].ToString();
                                search.user.employee.person.TRN = reader["people_trn"].ToString();
                                search.user.active = (bool)reader["user_enabled"];
                                break;
                            case "vehicle":
                                if (!vehicleChassis.Contains(reader["vehicle_chassis_no"].ToString()))
                                {
                                    search.table = "vehicle";
                                    search.ID = int.Parse(reader["id"].ToString());
                                    search.vehicle = new Vehicle(int.Parse(reader["id"].ToString()));
                                    search.vehicle.VehicleRegNumber = reader["vehicle_reg_no"].ToString();
                                    search.vehicle.chassisno = reader["vehicle_chassis_no"].ToString();
                                    search.vehicle.make = reader["vehicle_make"].ToString();
                                    search.vehicle.VehicleModel = reader["vehicle_model"].ToString();
                                    search.vehicle.vehicle_under_pol = (bool)reader["vehicle_under_pol"];
                                    search.vehicle.vehicle_has_active_cover_note = (bool)reader["vehicle_has_active_cover_note"];
                                    search.vehicle.vehicle_has_cover_note_history = (bool)reader["vehicle_has_cover_note_history"];
                                    vehicleChassis.Add(reader["vehicle_chassis_no"].ToString());
                                }
                                break;
                            default:
                                break;
                        }
                        results.Add(search);
                    }

                    reader.Close();
                    sql.DisconnectSQL();
                }

            }
            return results;
        }

        /// <summary>
        /// this function grabs the result from the general search procedure on the database
        /// </summary>
        /// <param name="search">term you would like to search for</param>
        /// <returns>List of responses</returns>
        public static List<Search> GeneralSearch(string search, string type)
        {
            List<Search> results = new List<Search>();
            if (search != null) search = search.ToLower();
            if (type != null) type = type.ToLower();

            if (Constants.user.username != "not initialized")
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    string query = "EXEC GeneralSearch '" + Constants.CleanString(search) + "','" + type + "'," + Constants.user.ID;
                    SqlDataReader reader = sql.QuerySQL(query, "read", "search", new Search());
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            results.Add(new Search(int.Parse(reader["ID"].ToString()), reader["table"].ToString(), reader["name"].ToString()));
                        }
                        reader.Close();
                        sql.DisconnectSQL();
                    }
                }
            }
            return results;
        }

        /// <summary>
        /// in the javascript file (gensearch.js) I require for a URL link to be placed in 
        /// the autocomplete element. This takes a Search object and creates to appropriate
        /// link for the data to be viewed by the user
        /// </summary>
        /// <param name="search">a single search object with all data required</param>
        /// <returns>a string witha the appropriate link</returns>
        public static string GenerateLink(Search search)
        {
            string output = "";
            switch (search.table)
            {
                case "company":
                    output += "/Company/Details/" + search.ID.ToString();
                    break;
                case "user":
                    output += "/User/Details/" + search.ID.ToString();
                    break;
                case "vehicle":
                    output += "/Vehicle/Details/" + search.ID.ToString();
                    break;
                case "policy":
                    output += "/Policy/Details/" + search.ID.ToString();
                    break;
                case "covernote":
                    output += "/VehicleCoverNote/Details/" + search.ID.ToString();
                    break;
                case "certificate":
                    output += "/VehicleCertificate/Details/" + search.ID.ToString();
                    break;
                case "people":
                    output += "/People/Details/" + search.ID.ToString();
                    break;
                default:
                    break;
            }
            return output;
        }

        /// <summary>
        /// in the javascript file (gensearch.js) I require for a string describing the element 
        /// to be placed in the autocomplete element. This takes a Search object and creates 
        /// an appropriate description for the data to be viewed by the user
        /// </summary>
        /// <param name="search">a single search object with all data required</param>
        /// <returns>a string with the appropriate description</returns>
        public static string GenerateName(Search search)
        {
            string output = "";
            switch (search.table)
            {
                case "company":
                    //if (search.name != "1000000000000")
                    output += "Company: " + search.name;
                    // else
                    // output += "Person TRN: Not Avaialble";
                    break;
                case "user":
                    output += "Username: " + search.name;
                    break;
                case "vehicle":
                    output += "Reg No: " + search.name;
                    break;
                case "policy":
                    output += "Policy ID: " + search.name;
                    break;
                case "covernote":
                    output += "Cover Note ID: " + search.name;
                    break;
                case "certificate":
                    output += "Certificate No: " + search.name;
                    break;
                case "people":
                    if (search.name != "10000000")
                        output += "Person TRN: " + search.name;
                    else
                        output += "Person TRN: Not Avaialble";
                    break;
                default:
                    break;
            }
            return output;
        }

        /// <summary>
        /// this function merges two serach results based on the and/or logic
        /// </summary>
        /// <param name="previous"></param>
        /// <param name="current"></param>
        /// <param name="andor"></param>
        /// <returns></returns>
        public static List<Search> mergeResults(List<Search> previous, List<Search> current, string andor)
        {
            List<Search> results = new List<Search>();

            switch (andor)
            {
                case "and":
                    results = previous.Intersect(current).ToList();
                    break;
                case "or":
                    results = previous.Union(current).ToList();
                    break;
                default:
                    results = previous;
                    break;
            }

            return results;

        }

        /// <summary>
        /// this function filters results based on a table requirement
        /// </summary>
        /// <param name="list"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        public static List<Search> filterResults(List<Search> list, string table)
        {
            List<Search> result = new List<Search>();

            foreach (Search search in list)
            {
                if (search.table == table)
                {
                    result.Add(search);
                }
            }
            return result;
        }

        #region old
        //public static List<int> GetAndSearchIDs(List<int> current, List<int> previous)
        //{

        //    return previous.Intersect(current).ToList();
        //}


        //public static List<int> GetOrSearchIDs(List<int> current, List<int> previous)
        //{

        //    List<int> Ids = new List<int>();
        //    Ids.AddRange(previous);
        //    Ids.AddRange(current);


        //    return Ids.Distinct().ToList();
        //}

        #endregion

    }

}