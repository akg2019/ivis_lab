﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Honeysuckle.Controllers;

namespace Honeysuckle.Models
{
    public class User
    {
        public int ID { get; set; }

        [RegularExpression("([a-zA-Z0-9._-]+[@][a-zA-Z0-9]+([.][a-z]+)+)", ErrorMessage = "Invalid email address, please retry")]
        public string username { get; set; }

        [Required(ErrorMessage = "Password Required")]
        [RegularExpression("((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*()]))[a-zA-Z0-9!@#$%^&*()]{8,}", ErrorMessage="A password must have a number, capital letter, common letter, special character (!,@,#,$,%,^,&,*,(,)) and at least 8 characters")]
        public string password { get; set; }

        [Required(ErrorMessage = "Confirm password required")]
        [RegularExpression("((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*()]))[a-zA-Z0-9!@#$%^&*()]{8,}", ErrorMessage = "A password must have a number, capital letter, common letter, special character (!,@,#,$,%,^,&,*,(,)) and at least 8 characters")]
        public string confirm_password { get; set; }

        public Employee employee { get; set; }
        public bool reset { get; set; }
        public string question { get; set; }
        public string response { get; set; }
        public int faultylogins { get; set; }
        public DateTime lastloginattempt { get; set; }
        public bool passwordreset { get; set; }
        public DateTime passwordresetrequesttime { get; set; }
        public List<UserGroup> UserGroups { get; set; }
        public bool active { get; set; }
        public bool firstlogin { get; set; }
        public string sessionguid { get; set; }
        public bool newRoleMode { get; set; }
        public UserGroup tempUserGroup { get; set; }
        public bool IAJcreated { get; set; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string EmailAddress { set; get; }
        public int CompanyID { set; get; }
        public int CompanyType { set; get; }

        public User() { }

        public User(int ID)
        {
            this.ID = ID;
        }

        public User(string username)
        {
            this.username = username;
        }

        public User(int ID, string username)
        {
            this.ID = ID;
            this.username = username;
        }

        public User(string username, string password)
        {
            this.username = username;
            this.password = password;
        }

        public User(int ID, string username, bool reset, int faultylogins, DateTime  lastloginattempt,bool passwordResetForce, DateTime passwordresetrequesttime, bool active)
        {
            this.ID = ID;
            this.username = username;
            this.reset = reset;
            this.faultylogins = faultylogins;
            this.passwordreset = passwordResetForce;
            this.passwordresetrequesttime = passwordresetrequesttime;
            this.lastloginattempt = lastloginattempt;
            this.active = active;
            
        }

        public User(int ID, string username, Employee employee)
        {
            this.ID = ID;
            this.username = username;
            this.employee = employee;
        }

    }

    public class UserModel
    {
        /// <summary>
        /// this function tests if a trn is in use
        /// </summary>
        /// <param name="id"></param>
        /// <param name="trn"></param>
        /// <returns></returns>
        public static bool TestTRN(int id = 0, string trn = null, string type = null)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC TestTRN " + id + ",'" + Constants.CleanString(trn) + "','" + Constants.CleanString(type) + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function returns a user's security question from the database
        /// </summary>
        /// <param name="username">a valid username in the database</param>
        /// <returns>a User object with the user ID, username and question</returns>
        public static User GetQuestion(string username)
        {
            User user = new User(username);
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetQuestion '" + Constants.CleanString(username) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    user.ID = int.Parse(reader["ID"].ToString());
                    user.question = reader["question"].ToString();
                    user.username = username;
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return user;
        }

        /// <summary>
        /// This function sends back the username and response of the security question and 
        /// Tests if the data matches up
        /// </summary>
        /// <param name="user">a user object with a valid username in the system</param>
        /// <returns>boolean stating whether question was answered correctly or not</returns>
        public static bool AnswerQuestion(User user)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC AnswerQuestion '" + Constants.CleanString(user.username) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = EncryptText.TestPassword(user.response, reader["response"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }
        
        /// <summary>
        /// This function is used to attain the user data from the
        /// server based on the session guid currently obtained
        /// </summary>
        /// <param name="guid">a valid session guid on the server</param>
        /// <returns>user object</returns>
        public static User getUserFromGUID(string guid)
        {
            User user = new User();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC getUserFromGUID '" + guid + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                try
                {
                    while (reader.Read())
                    {
                        try{user.ID = int.Parse(reader["UID"].ToString());}catch(Exception){}
                        try{user.username = reader["username"].ToString();}catch(Exception){}
                        try{user.FirstName = reader["FirstName"].ToString();}catch(Exception){}
                        try{user.LastName = reader["LastName"].ToString();}catch(Exception){}
                        try{user.EmailAddress = reader["EmailAddress"].ToString();}catch(Exception){}
                        try{user.CompanyID = int.Parse(reader["CompanyID"].ToString());}catch(Exception){}
                        try{user.CompanyType = int.Parse(reader["CompanyType"].ToString());}catch(Exception){}
                        try{user.sessionguid = guid;}catch(Exception){}
                    }
                   
                }
                catch (Exception)
                {
                    
                  
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return user;
        }
        
        /// <summary>
        /// This function gets the current session guid for the user logged in
        /// </summary>
        public static string GetSessionGUID(User user)
        {
            string guid = null;
            sql sql = new sql();
            if (sql.ConnectSQL() && user.ID != 0)
            {
                string query = "EXEC GetSessionGUID " + user.ID + ",";
                if (user.username != null) query += "'" + Constants.CleanString(user.username) + "'";
                else query += "null";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    guid = reader["LoginGUID"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return guid;
        }

        /// <summary>
        /// This function sends the guid for the user currently logged into the system
        /// To continue his session
        /// </summary>
        /// <returns></returns>
        public static bool StillLoggedIn(string key, int kid = 0)
        {
            bool result = true;
            if (key != null)
            {
                sql sql = new sql();
                sql.ConnectSQL();
                string query = "EXEC StillLoggedIn '" + RSA.Decrypt(key, "session", kid) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);

              //Needs to catch erors

                try
                {
                    while (reader.Read())
                    {
                        result = (bool)reader["result"];
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
                catch
                {
                    if(Constants.user != null) Logout();
                }
            }
            return result;
        }

        /// <summary>
        /// This function overwrites all user data in sessions
        /// </summary>
        public static void Logout()
        {
            if (Constants.user.ID != 0)
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    string query = "EXEC Logout " + Constants.user.ID;
                    SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                    //SqlDataReader reader = sql.QuerySQL(query);
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            Constants.user = new User("not initialized");
            Constants.ClearBack();
            Constants.ClearData();
            RSA.CleanKeys();
        }

        /// <summary>
        /// This function overwrites all user data in sessions
        /// </summary>
        public static void ResetAccess(int id)
        {
            if (Constants.user.ID != 0)
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    string query = "EXEC Logout " + id;
                    SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                    //SqlDataReader reader = sql.QuerySQL(query);
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            Constants.user = new User("not initialized");
            Constants.ClearBack();
            Constants.ClearData();
            RSA.CleanKeys();
        }

        /// <summary>
        /// this function logs out a specific user
        /// </summary>
        /// <param name="username"></param>
        public static void Logout(string username)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC LogoutWithUsername '" + Constants.CleanString(username) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function manages the adding of a user to a group
        /// </summary>
        /// <param name="user">the user in question with a valid ID in the system</param>
        /// <param name="usergroup">a user group object with a valid ID in the system</param>
        public static void AddUserToUserGroup(User user, UserGroup usergroup)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC AssociateUserGroup " + usergroup.UserGroupID + "," + user.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "create", "usergroup", usergroup);
                //SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function takes a list of user ids to be added to a usergroup
        /// </summary>
        /// <param name="users"></param>
        /// <param name="usergroup"></param>
        public static void AddUsersToUserGroup(List<int> users, UserGroup usergroup)
        {
            foreach (int i in users)
            {
                AddUserToUserGroup(new User(i), usergroup);
            }
        }

        /// <summary>
        /// This removes a user/usergroup association
        /// </summary>
        /// <param name="user">valid user object with an id in the system</param>
        /// <param name="usergroup">valid usergroup object with an id in the system</param>
        private static void RemoveUserFromUserGroup(User user, UserGroup usergroup)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DisassociateUserGroup " + usergroup.UserGroupID + "," + user.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "usergroup", usergroup);
                //SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function takes a list of user ids that one would like to remove from a user group
        /// </summary>
        /// <param name="users">List of valid User IDs</param>
        /// <param name="usergroup">UserGroup that you would like to disassociate from</param>
        public static void RemoveUsersFromUserGroup(List<int> users, UserGroup usergroup)
        {
            foreach (int i in users)
            {
                RemoveUserFromUserGroup(new User(i), usergroup);
            }
        }

        /// <summary>
        /// This function returns a list of users who are associated to a group
        /// </summary>
        /// <param name="usergroup">a valid usergroup with a valid id</param>
        /// <returns>list of users</returns>
        public static List<User> GetAllUsersForAGroup(UserGroup usergroup)
        {
            List<User> users = new List<User>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetAllUsersForAGroup " + usergroup.UserGroupID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    users.Add(new User(int.Parse(reader["UID"].ToString()), reader["username"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return users;
        }

        /// <summary>
        /// This function gets a list of users who are not associated to a usergroup
        /// </summary>
        /// <param name="usergroup">a valid usergroup with a valid id</param>
        /// <returns>list of users</returns>
        public static List<User> GetAllUsersNotForAGroup(UserGroup usergroup)
        {
            List<User> users = new List<User>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetAllUsersNotForAGroup " + usergroup.UserGroupID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    users.Add(new User(int.Parse(reader["UID"].ToString()), reader["username"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return users;
        }

        /// <summary>
        /// This function gets a user based on the guid currently associated with
        /// </summary>
        /// <param name="guid">a valid RESET guid</param>
        /// <returns>User object</returns>
        public static User GetUserbyGUID(string guid)
        {
            User user = new User();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetUserbyGUID '" + new Guid(guid) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    user.ID = int.Parse(reader["UID"].ToString());
                    user.username = reader["username"].ToString();
                    user.sessionguid = guid;
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return user;
        }

        /// <summary>
        /// This function tests as to whether a user is able to reset their password
        /// </summary>
        /// <param name="guid">a valid guid associated with a user in the system</param>
        /// <returns>a boolean indicating as to whether the user can reset their password</returns>
        public static bool IsResetAvailable(string guid)
        {
            bool result = false;
            try
            {
                Guid real_guid = new Guid(guid);

                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    string query = "EXEC IsResetAccessible '" + real_guid + "'";
                    SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                    //SqlDataReader reader = sql.QuerySQL(query);
                    while (reader.Read())
                    {
                        result = (bool)reader["result"];
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            catch { }
            return result;
        }

        /// <summary>
        /// This function tests as to whether a user is able to reset their password
        /// </summary>
        /// <param name="user">a user object with a username</param>
        /// <returns>a boolean indicating as to whether the user can reset their password</returns>
        public static bool IsResetAvailable(User user)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsResetAccessibleByID '" + Constants.CleanString(user.username) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }
        
        /// <summary>
        /// This function returns a user's username based on an ID
        /// </summary>
        /// <param name="id">a valid ID in the system</param>
        /// <returns>the username</returns>
        public static string GetUserName(int id)
        {
            string result = "";
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetUserNameWithID " + id;
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = reader["username"].ToString();

                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        public static string GetUserNameForMail(int id)
        {
            string result = "";
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetUserNameWithID " + id;
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = reader["EmailAddress"].ToString();

                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function tests if the username provided exists in the system
        /// and isn't deleted or 'filtered' as per the database flags
        /// </summary>
        /// <param name="username">a string testing for user existence</param>
        /// <returns>boolean stating whether user exists or not</returns>
        public static bool IsValidUser(string username)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsValidUser '" + Constants.CleanString(username) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function removes a user from the system
        /// </summary>
        /// <param name="id">the id of the user you want deleted</param>
        /// <returns>a boolean indicating as to whether the user was deleted</returns>
        public static bool DeleteUser(int id)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DeleteUser " + id + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "user", new User(id));
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }
        
        /// <summary>
        /// This is the function that sets a user record as able to be reset and sends related details via notifications/email to the user in question
        /// </summary>
        /// <param name="user">the valid user object (with username) that we want to reset</param>
        /// <returns>boolean</returns>
        public static bool ResetPassword(User user)
        {
            bool result = false;
            Constants.tempuser = user;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC ResetPassword '" + Constants.CleanString(user.username) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "update", "user", user);
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                    if (result)
                    {
                        List<string> emails = new List<string>();
                        user.ID = int.Parse(reader["UID"].ToString());
                        emails.Add(reader["email"].ToString());
                        string fname = reader["fname"].ToString();
                        string lname = reader["lname"].ToString();
                        string resetguid = reader["resetguid"].ToString();

                        Regex url = new Regex("(.*/Login/)");
                        string strurl = "";
                        if (url.IsMatch(HttpContext.Current.Request.Url.AbsoluteUri))
                        {
                            MatchCollection matches = url.Matches(HttpContext.Current.Request.Url.AbsoluteUri);
                            foreach (Match m in matches)
                            {
                                strurl = m.Value;
                            }
                        }

                        //string emailBody = strurl + "ResetPassword/" + resetguid;
                        string emailBody = "A request has been made for your password to be reset. Please visit the following url: " + strurl + "ResetPassword/" + resetguid + " to reset your password. This link will no longer be accessible after 2 hours.";
                        MailModel.SendMailWithThisFunction(new List<User>() { user }, new Mail(emails, "Password Reset Link", emailBody));
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function updates user data due to reset 
        /// and sends appropriate notification/messages
        /// To any and all company administrators of
        /// The company that the user in question is 
        /// reseting
        /// </summary>
        /// <param name="user">user who is reseting a password</param>
        public static void SetResetPasswordData(User user)
        {
            List<User> users = new List<User>();
            List<string> to = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC SendResetPasswordData '" + Constants.CleanString(user.username) + "','" + EncryptText.Encrypt(user.password) + "'," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "user", user);
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    users.Add(new User(int.Parse(reader["UID"].ToString()), reader["username"].ToString()));
                    to.Add(reader["email"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            MailModel.SendMailWithThisFunction(users, new Mail(to, "Successful password reset" ,user.username + " has successfully reset his/her password."));
        }

        #region old_code
        /// <summary>
        /// This function takes in a regular string and encrypts it with a hash(static)
        /// </summary>
        /// <param name="text">string to be hashed</param>
        /// <returns>hashed string</returns>
        //public static string EncryptText.Encrypt(string text)
        //{
        //    string hash = BCrypt.GenerateSalt(); //"$2a$10$IWR1FBJZb9bmFHq7GZQuMO"; //generated hash stored to deal with encrypting data
        //    string hashText = BCrypt.HashPassword(text, hash);
        //    return hashText;
        //}

        

        //public static string GetHashedPassword(int id)
        //{
        //    string pwd = null;
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        SqlDataReader reader = sql.QuerySQL("EXEC GetHashedPassword " + id);
        //        while (reader.Read())
        //        {
        //            pwd = reader["password"].ToString();
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return pwd;
        //}
        #endregion

        /// <summary>
        /// This function adds a user to the database
        /// </summary>
        /// <param name="user">A valid user with all relevant data</param>
        public static void AddUser(User user, int SystemAdmin = 0)
        {
            bool isIAJAdmin = Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user);

            EmployeeModel emp = new EmployeeModel();
            Company comp = new Company();
            EmailModel email = new EmailModel();

            user.employee.person.address.ID = AddressModel.AddAddress(user.employee.person.address);
            PersonModel.AddPerson(user.employee.person);
            EmployeeModel.AddEmployee(user.employee);

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                
                string query = "EXECUTE AddUser '" + Constants.CleanString(user.username) + "','"
                                                   + EncryptText.Encrypt(user.password) + "',"
                                                   + user.employee.EmployeeID + ","
                                                   + Constants.user.ID + ","
                                                   + SystemAdmin + ","
                                                   + isIAJAdmin + ",'" + user.employee.person.fname + "','"
                                                    + user.employee.person.lname + "','"
                                                    + user.username + "'," + Constants.user.employee.company.CompanyID ;
                SqlDataReader reader = sql.QuerySQL(query, "create", "user", user);
                //SqlDataReader reader = sql.QuerySQL(query);

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        user.ID = int.Parse(reader["ID"].ToString());
                    }
                    reader.Close();
                }
                sql.DisconnectSQL();
            }
            
            user.employee.person.emails = new List<Email>();
            user.employee.person.emails.Add(new Email(user.username, true)); //adding an email that doesn't have an ID yet.
            EmailModel.AddEmailtoPerson(user.employee.person);
            PhoneModel.AddPhoneToPerson(user);

            MessageCompanyAdminsofCompanyForUsers(user); // sendsmessage to company admins (if we have any)

        }

        /// <summary>
        /// Gets companycode
        /// </summary>
        /// <param name="CompanyID"></param>
        /// <returns></returns>
        public static string GetCompanyCode(int CompanyID){
            string companycode = "";
            DataTable dt = (DataTable)OmaxFramework.Utilities.OmaxData.HandleQuery("select CompShortening from Company where CompanyID = " +  CompanyID, sql.GetCurrentCOnnectionString(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.ScalarQuery);
            try { companycode = dt.Rows[0].ItemArray[0].ToString() != "" ? dt.Rows[0].ItemArray[0].ToString() + "." : ""; }catch(Exception){}
            return companycode;
        }

        /// <summary>
        /// this function retrieves all the admins of a company of the user in question
        /// and sends a message to all the company admins that a new user has been created for
        /// their company
        /// </summary>
        /// <param name="user"></param>
        private static void MessageCompanyAdminsofCompanyForUsers(User user)
        {
            List<User> companyadmins = CompanyModel.GetMyCompanyAdmins(user);
            MailModel.SendMailWithThisFunction(companyadmins, new Mail(EmailModel.ConvertEmailtoString(EmailModel.GetPrimaryEmail(companyadmins)), "New user created", "The user '" + GetCompanyCode(user.employee.company.CompanyID) + user.username + "' has been created for your company."));
        }

        /// <summary>
        /// This function generates a welcome message for users
        /// </summary>
        /// <param name="user">A valid user in the system</param>
        public static void WelcomeEmail(User user, string url)
        {
            List<string> toList = new List<string>();
            toList.Add(EmailModel.GetPrimaryEmail(user.employee.person.emails).email);
           
            Mail mail = new Mail();
            mail.to = toList;
            mail.subject = "Welcome to IVIS";
            mail.body = "You are our newest member! Welcome to IVIS! Please use the following credentials at " +
                url + Environment.NewLine + "  Username:  " + GetCompanyCode(user.employee.company.CompanyID) + user.username + Environment.NewLine 
                + " , Password: " + user.password + Environment.NewLine + "You have 48 hours to take action. Failure to take action within 48 hours will result in your account being deactivated.";

            MailModel.SendMailWithThisFunction(new List<User>(){user}, mail);
        }

        /// <summary>
        /// The function modifies the personal data of a user
        /// </summary>
        /// <param name="user">a valid user with person data to be updated</param>
        public static void ModifyUser(User user)
        {
            #region audit_invoke
            Audit audit = new Audit();
            audit.ip_address = Constants.ipaddress;
            audit.note = "update user";
            audit.original_data = GetUser(user.ID);
            audit.new_data = user;
            audit.action = "update";
            audit.table = "user";
            audit.reference_id = user.ID;
            AuditModel.AuditLogThreadHandler(audit, Constants.user.ID);
            #endregion
            updateUsername(user);

            PersonModel.ModifyPerson(user.employee.person);
            EmployeeModel.UpdateEmployee(user.employee);
            

            string EmailIDQuery = @"SELECT PeopleToEmails.EmailID FROM PeopleToEmails INNER JOIN Emails ON PeopleToEmails.EmailID = Emails.EID WHERE (PeopleToEmails.PeopleID = " + user.employee.person.PersonID + ") and PeopleToEmails.[Primary] = 1";

            DataTable dt = (DataTable)OmaxFramework.Utilities.OmaxData.HandleQuery(EmailIDQuery, sql.GetCurrentCOnnectionString(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.ScalarQuery);

            int EmailID = 0;

            try
            {
                foreach(DataRow Ema in dt.Rows){
                   EmailID = int.Parse(Ema.ItemArray[0].ToString());
                }
            }
            catch (Exception)
            {
             
            }

            

            try
            {
                //Updates user's email address
                OmaxFramework.Utilities.OmaxData.HandleQuery("UPDATE USERS SET EMAILADDRESS = '" + user.EmailAddress + "' WHERE UID = " + user.ID, sql.GetCurrentCOnnectionString(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.NoneQuery);
           
            }
            catch (Exception)
            {
               
            }
            string UpdateEmail = "UPDATE Emails SET email = '" + user.EmailAddress + "' where EID = " + EmailID;

            try
            {
                //string RemoveQuery = "DELETE FROM Emails WHERE EID IN (SELECT PeopleToEmails.EmailID FROM PeopleToEmails INNER JOIN Emails ON PeopleToEmails.EmailID = Emails.EID WHERE (PeopleToEmails.PeopleID = " + user.employee.person.PersonID + ") and PeopleToEmails.[Primary] <> 1)";
                //OmaxFramework.Utilities.OmaxData.HandleQuery(RemoveQuery, sql.GetCurrentCOnnectionString(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.NoneQuery);
            
            }
            catch (Exception)
            {

            }


            try
            {
                OmaxFramework.Utilities.OmaxData.HandleQuery(UpdateEmail, sql.GetCurrentCOnnectionString(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.NoneQuery);
            
            }
            catch (Exception)
            {
              
            }
           

        }

        /// <summary>
        /// This function gets the user id and username of a user based on the username given
        /// </summary>
        /// <param name="username">valid username in the database</param>
        /// <returns>user object</returns>
        public static User GetUser(string username)
        {
            User user = new User();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetUserWithName '" + Constants.CleanString(username) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    try { user.ID = int.Parse(reader["UID"].ToString()); }catch(Exception){}
                    try { user.username = reader["username"].ToString();}catch(Exception){}
                    try { user.FirstName = reader["FirstName"].ToString();}catch(Exception){}
                    try { user.LastName = reader["LastName"].ToString();}catch(Exception){}
                    try { user.EmailAddress = reader["EmailAddress"].ToString();}catch(Exception){}
                    try { user.CompanyID = int.Parse(reader["CompanyID"].ToString());}catch(Exception){}
                    try { user.CompanyType = int.Parse(reader["CompanyType"].ToString());}catch(Exception){}

                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return user;
        }

        /// <summary>
        /// This function gets all relevant data for a user based on the id given
        /// </summary>
        /// <param name="ID">a valid id in the database</param>
        /// <returns>a filled user object</returns>
        public static User GetUser(int ID)
        {
            User u = new User();
            Employee emp = new Employee();
            Company comp = new Company();
            Person per = new Person();
            Address add = new Address();

            Road road = new Road();
            RoadType road1 = new RoadType();
            ZipCode zip = new ZipCode();
            City city = new City();
            Country country = new Country();
            Email email = new Email();
            
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE GetUserFromID " + ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);

                while (reader.Read())
                {
                    u.ID = ID;
                    u.username = reader["username"].ToString();
                    try {u.EmailAddress = reader["EmailAddress"].ToString(); }catch(Exception){}
                    try {u.CompanyID = int.Parse(reader["CompanyID"].ToString()); }catch(Exception){}
                    try {u.CompanyType = int.Parse(reader["CompanyType"].ToString()); }catch(Exception){}
                    u.reset = (bool)reader["reset"];
                    u.faultylogins = int.Parse(reader["faultylogins"].ToString());
                    u.active = (bool)reader["active"];
                    u.sessionguid = GetSessionGUID(u);
                    u.IAJcreated = (bool)reader["IAJcreated"];

                    if (DBNull.Value != reader["lastloginattempt"]) u.lastloginattempt = DateTime.Parse(reader["lastloginattempt"].ToString());
                    if (DBNull.Value != reader["passwordresetrequesttime"]) u.passwordresetrequesttime = DateTime.Parse(reader["passwordresetrequesttime"].ToString());
                    u.passwordreset = (bool)reader["password_reset_force"];

                    if (DBNull.Value != reader["companyid"]) comp.CompanyID = int.Parse(reader["companyid"].ToString());
                    if (DBNull.Value != reader["CompanyName"]) comp.CompanyName = reader["CompanyName"].ToString();
                    if (DBNull.Value != reader["IAJID"]) comp.IAJID = reader["IAJID"].ToString();
                    if (DBNull.Value != reader["floginlimit"]) comp.floginlimit = int.Parse(reader["floginlimit"].ToString());
                    if (DBNull.Value != reader["companytype"]) comp.CompanyType = reader["companytype"].ToString();

                    if (DBNull.Value != reader["PeopleID"]) per.PersonID = int.Parse(reader["PeopleID"].ToString());
                    if (DBNull.Value != reader["FName"]) per.fname = reader["FName"].ToString();
                    if (DBNull.Value != reader["MName"]) per.mname = reader["MName"].ToString();
                    if (DBNull.Value != reader["LName"]) per.lname = reader["LName"].ToString();
                    if (DBNull.Value != reader["TRN"]) per.TRN = reader["TRN"].ToString();

                    if (DBNull.Value != reader["RoadNumber"]) add.roadnumber = reader["RoadNumber"].ToString();
                    if (DBNull.Value != reader["AptNumber"]) add.ApartmentNumber = reader["AptNumber"].ToString();
                    if (DBNull.Value != reader["RoadName"]) road.RoadName = reader["RoadName"].ToString();
                    if (DBNull.Value != reader["RoadType"]) road1.RoadTypeName = reader["RoadType"].ToString();
                    if (DBNull.Value != reader["Zipcode"]) zip.ZipCodeName = reader["Zipcode"].ToString();
                    if (DBNull.Value != reader["City"]) city.CityName = reader["City"].ToString();
                    if (DBNull.Value != reader["CountryName"]) country.CountryName = reader["CountryName"].ToString();

                    if (DBNull.Value != reader["EmployeeID"]) emp.EmployeeID = int.Parse(reader["EmployeeID"].ToString());

                    add.road = road;
                    add.roadtype = road1;
                    add.zipcode = zip;
                    add.city = city;
                    if (DBNull.Value != reader["PID"])
                    {
                        add.parish = new Parish(int.Parse(reader["PID"].ToString()), reader["parish"].ToString());
                    }
                    add.country = country;

                    per.address = add;
                    emp.company = comp;
                    emp.person = per;
                    u.employee = emp;
                }
                reader.Close();
                sql.DisconnectSQL();
                if (u.employee != null)
                {
                    u.employee.person.emails = EmailModel.GetMyEmails(u.employee.person);
                    u.employee.person.phoneNums = PhoneModel.GetMyPhone(u.employee.person);
                }
                
            }
            return u;

        }

        /// <summary>
        /// This function authenticates a user login
        /// </summary>
        /// <param name="username">string</param>
        /// <param name="password">string</param>
        /// <returns>a boolean indicating with the username/password provided is valid</returns>
        public static user_login_object LoginUser(User user)
        {
            user_login_object output = new user_login_object();
            output.user = user;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC Login '" + Constants.CleanString(user.username) + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    output.active = (bool)reader["active"];
                    output.session_free = (bool)reader["session_free"];
                    output.valid_user = (bool)reader["valid_user"];
                    output.first_login = (bool)reader["first_login"];
                    output.force_reset = (bool)reader["force_reset"];
                    output.faulty_login_limit_passed = (bool)reader["faulty_login_limit_passed"];
                    output.first_login_timer_lapsed = (bool)reader["first_login_lapse"];
                    output.user_exist = (bool)reader["user_exist"];
                    output.company_active = (bool)reader["company_active"];

                    if (output.user_exist && output.session_free)
                    {
                        output.valid_user = EncryptText.TestPassword(user.password, reader["password"].ToString());
                    }

                }
                reader.Close();
                sql.DisconnectSQL();
            }
            else output.db_error = true;

            return output;
        }

        /// <summary>
        /// this function marks a user as having had a single fault login
        /// </summary>
        /// <param name="username"></param>
        public static void FaultyLogin(string username)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC FaultyLogin '" + Constants.CleanString(username) + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function resets a user's fault logins to 0 in the database
        /// </summary>
        /// <param name="username"></param>
        public static void ResetFaultyLogins(string username)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC ResetFaultyLogins '" + Constants.CleanString(username) + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function generates a password that is 'strong'. Password will contain a capital letter, a common letter, a numeric character and a 'special' character as well as at least 8 characters
        /// </summary>
        /// <returns>string</returns>
        public static string PasswordGenerator()
        {
            string GeneratedPassword = "";
            //change for release
            if (System.Configuration.ConfigurationSettings.AppSettings["test_mode"].ToString() == "false")
            {

                char[] letters = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x',
                               'y','z'};

                char[] CapitalLetters = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

                char[] numbers = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };

                char[] specialChars = { '!', '@', '#', '$', '%', '&', '?' };

                char[] allChars = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x',
                               'y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                               '1','2','3','4','5','6','7','8','9','0','!','@','#','$','%','&','?'};


                Random rand = new Random();



                GeneratedPassword += letters[rand.Next((letters.Length) - 1)];
                GeneratedPassword += numbers[rand.Next((numbers.Length) - 1)];
                GeneratedPassword += specialChars[rand.Next((specialChars.Length) - 1)];
                GeneratedPassword += CapitalLetters[rand.Next((CapitalLetters.Length) - 1)];

                for (int x = 0; x < 6; x++)
                {
                    GeneratedPassword += allChars[rand.Next((allChars.Length) - 1)];
                }
            }
            else GeneratedPassword = "P@$$w0rd"; //default for test mode

            return GeneratedPassword;
        }

        /// <summary>
        /// This function checks the system to see if a specific username is available
        /// </summary>
        /// <param name="username">string</param>
        /// <returns>boolean indicating that the username is available</returns>
        public static bool UserExist(string username)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC TestUsername '" + Constants.CleanString(username) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            
            return result;
        }

        /// <summary>
        /// this function tests if a guid is valid for the json webservice
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static bool TestWebGUID(string guid)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC TestWebGUID '" + guid + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function tests if a guid is valid for the system
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static bool TestGUID(User user)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL() && user != null)
            {
                if (user.sessionguid != null && user.sessionguid != "")
                {
                    string query = "EXEC TestGUID '" + user.sessionguid + "'";
                    SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                    //SqlDataReader reader = sql.QuerySQL(query);
                    while (reader.Read())
                    {
                        result = (bool)reader["result"];
                    }
                    reader.Close();
                }
                else
                {
                    Logout();
                }
                sql.DisconnectSQL();
            }
            else
            {
                Logout();
            }
            return result;
        }

        /// <summary>
        /// this function tests whether a user possesses any of a list of user permissions
        /// </summary>
        /// <param name="user"></param>
        /// <param name="userpermission"></param>
        /// <returns></returns>
        public static bool TestUserPermissions(User user, List<UserPermission> userpermission)
        {
            bool result = false;
            bool killit = true;
            //TESTING SESSIONS
            if (user != null)
            {
                if (user.ID == 0)
                {
                    user = GetUser(user.username);
                    //user.sessionguid = GetSessionGUID(user);
                }
                killit = TestGUID(user);
            }
            else killit = false;
            //TESTING PERMISSIONS
            if (user.username != "not initialized" && killit)
            {

                UserPermissionModel.GetPermissionID(userpermission);

                if (user.newRoleMode)
                { // If the system admin is temporarily using a new role
                    if (user.tempUserGroup.SuperGroup)
                    {
                        user.tempUserGroup.children = UserGroupModel.GetChildren(user.tempUserGroup);
                        foreach (UserGroup child in user.tempUserGroup.children)
                        {
                            child.Permissions = UserPermissionModel.GetAllPermissionsForGroup(child);
                            foreach (UserPermission up in child.Permissions)
                            {
                                foreach (UserPermission upin in userpermission)
                                {
                                    result = up.id == upin.id;
                                    if (result) break;
                                }
                                if (result) break;
                            } //end foreach
                            if (result) break;
                        } //end foreach
                    } //end if
                    else
                    {
                        if (user.tempUserGroup.Permissions == null) user.tempUserGroup.Permissions = UserPermissionModel.GetAllPermissionsForGroup(user.tempUserGroup);
                        foreach (UserPermission up in user.tempUserGroup.Permissions)
                        {
                            foreach (UserPermission upin in userpermission)
                            {
                                result = up.id == upin.id;
                                if (result) break;
                            }
                            if (result) break;
                        } //end foreach
                    } //end else
                }
                else //Regular test if the user does not have a temporary role
                {
                    if (user.UserGroups == null) user.UserGroups = UserGroupModel.GetMyUserGroups(user);
                    foreach (UserGroup ug in user.UserGroups)
                    {
                        if (result) break;
                        if (ug.UserGroupName == "System Administrator" && ug.system)
                        {
                            result = true;
                            break;
                        } //end if
                        else
                        {
                            if (ug.SuperGroup)
                            {
                                ug.children = UserGroupModel.GetChildren(ug);
                                foreach (UserGroup child in ug.children)
                                {
                                    child.Permissions = UserPermissionModel.GetAllPermissionsForGroup(child);
                                    foreach (UserPermission up in child.Permissions)
                                    {
                                        foreach (UserPermission upin in userpermission)
                                        {
                                            result = up.id == upin.id;
                                            if (result) break;
                                        }
                                        if (result) break;
                                    } //end foreach
                                    if (result) break;
                                } //end foreach
                            } //end if
                            else
                            {
                                if (ug.Permissions == null) ug.Permissions = UserPermissionModel.GetAllPermissionsForGroup(ug); //Once the permissions have been stored already, then another db call isn't needed

                                var PermissionExist =
                                from GroupPerm in ug.Permissions
                                join perm in userpermission on GroupPerm.id equals perm.id into PermGroups
                                from OutPerms in PermGroups
                                select OutPerms;

                                if (PermissionExist.ToList().Count > 0) 
                                { 
                                    result = true;
                                }

                               
                                //foreach (UserPermission up in ug.Permissions)
                                //{
                                //    foreach (UserPermission upin in userpermission)
                                //    {
                                //        result = up.id == upin.id;
                                //        if (result) break;
                                //    }
                                //    if (result) break;
                                //} //end foreach
                            } // end else
                        } //end else
                    } //end foreach
                }
            }

            return result;
        }

        /// <summary>
        /// THIS FUNCTION IS USED TO TEST IF A USER HAS PERMISSIONS TO CREATE/READ/UPDATE/DELETE
        /// ON ANY PARTICULAR TABLE. IT WILL BE CALLLED ON ALL FORMS BEFORE THEY ARE GENERATED
        /// TO ALLOW FOR PROPER SECURITY OF DATA.
        /// </summary>
        /// <param name="user">a valid user with a valid id in the system</param>
        /// <param name="userpermission">a userpermission object</param>
        /// <returns>a boolean indicating whether the user has the permission to complete the action requested</returns>
        public static bool TestUserPermissions(User user, UserPermission userpermission)
        {
            bool result = false;
            bool killit = true;
            //TESTING SESSIONS
            if (user != null)
            {
                if (user.ID == 0)
                {
                    user = GetUser(user.username);
                    user.sessionguid = GetSessionGUID(user);
                }
                killit = TestGUID(user);
            }
            else killit = false;
            //TESTING PERMISSIONS
            if (user.username != "not initialized" && killit)
            {
               
               UserPermissionModel.GetPermissionID(userpermission);

                if (user.newRoleMode) { // If the system admin is temporarily using a new role
                    if (user.tempUserGroup.SuperGroup)
                    {
                        user.tempUserGroup.children = UserGroupModel.GetChildren(user.tempUserGroup);
                        foreach (UserGroup child in user.tempUserGroup.children)
                        {
                            child.Permissions = UserPermissionModel.GetAllPermissionsForGroup(child, child.system);
                            result = child.Permissions.Select(x => x.id).ToList().Contains(userpermission.id);
                            //foreach (UserPermission up in child.Permissions)
                            //{
                            //    result = up.id == userpermission.id;
                            //    if (result) break;
                            //} //end foreach
                            if (result) break;
                        } //end foreach
                    } //end if
                    else
                    {
                        if (user.tempUserGroup.Permissions == null) user.tempUserGroup.Permissions = UserPermissionModel.GetAllPermissionsForGroup(user.tempUserGroup, user.tempUserGroup.system);
                        result = user.tempUserGroup.Permissions.Select(x => x.id).ToList().Contains(userpermission.id);
                        
                        //foreach (UserPermission up in user.tempUserGroup.Permissions)
                        //{
                        //    result = up.id == userpermission.id;
                        //    if (result) break;
                        //} //end foreach
                    } //end else
                }
                else //Regular test if the user does not have a temporary role
                {
                    if (user.UserGroups == null) user.UserGroups = UserGroupModel.GetMyUserGroups(user);
                    foreach (UserGroup ug in user.UserGroups)
                    {
                        if (result) break;
                        if (ug.UserGroupName == "System Administrator" && ug.system)
                        {
                            result = true;
                            break;
                        } //end if
                        else
                        {
                            if (ug.SuperGroup)
                            {
                                ug.children = UserGroupModel.GetChildren(ug);
                                //result = ug.children.Where(x => x.Permissions.Select(y => y.id).ToList().Contains(userpermission.id)).ToList().Count() > 0;
                                foreach (UserGroup child in ug.children)
                                {
                                    child.Permissions = UserPermissionModel.GetAllPermissionsForGroup(child, child.system);
                                    foreach (UserPermission up in child.Permissions)
                                    {
                                        result = up.id == userpermission.id;
                                        if (result) break;
                                    } //end foreach
                                    if (result) break;
                                } //end foreach
                            } //end if
                            else
                            {
                                if (ug.Permissions == null) ug.Permissions = UserPermissionModel.GetAllPermissionsForGroup(ug, ug.system); //Once the permissions have been stored already, then another db call isn't needed
                                result = ug.Permissions.Select(x => x.id).ToList().Contains(userpermission.id);
                                //foreach (UserPermission up in ug.Permissions)
                                //{
                                //    result = up.id == userpermission.id;
                                //    if (result) break;
                                //} //end foreach
                            } // end else
                        } //end else
                    } //end foreach
                }
            }

            return result;
        }


        /// <summary>
        /// This function returns all users in a particular company
        /// </summary>
        /// <returns>List of users</returns>
        public static List<User> GetCompUsers(int id = 0)
        {
            sql sql = new sql();
            List<User> users = new List<User>();

            if (sql.ConnectSQL())
            {
                string query = "";
                if (id == 0) query = "EXEC GetCompUsers " + UserModel.GetUser(Constants.user.ID).employee.company.CompanyID;
                else query = "EXEC GetCompUsers " + id;

                SqlDataReader reader = sql.QuerySQL(query);
                //SqlDataReader reader = sql.QuerySQL(query);

                if (reader != null)
                {
                    while (reader.Read())
                    {

                        try
                        {
                            int ID = int.Parse(reader["UID"].ToString());
                            string username = reader["username"].ToString();
                            bool reset = (bool)reader["reset"];
                            int flogin = int.Parse(reader["faultylogins"].ToString());
                            DateTime lla = new DateTime(); DateTime prrt = new DateTime();
                            if (DBNull.Value != reader["lastloginattempt"]) lla = DateTime.Parse(reader["lastloginattempt"].ToString());
                            if (DBNull.Value != reader["passwordresetrequesttime"]) prrt = DateTime.Parse(reader["passwordresetrequesttime"].ToString());
                            bool prf = (bool)reader["password_reset_force"];
                            bool active = (bool)reader["active"];
                            User user = new User(ID, username, reset, flogin, lla, prf, prrt, active);
                            user.employee = new Employee(int.Parse(reader["eid"].ToString()));
                            user.employee.person = new Person( reader["fname"].ToString(), reader["lname"].ToString(),string.Empty);
                            user.employee.person.address = new Address();
                 
                            user.IAJcreated = (bool)reader["IAJcreated"];
                            users.Add(user);
                        }
                        catch (Exception)
                        {
                          
                        }
                    }
                }
                reader.Close();

                sql.DisconnectSQL();
            }
            return users;
        }

        /// <summary>
        /// this function updates a particular user's
        /// security question in the database
        /// </summary>
        /// <param name="user">user object with valid ID in the database</param>
        public static void UpdateQuestionAnswer(User user)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC UpdateQuestionAnswer " + user.ID + ",'" + Constants.CleanString(user.question) + "','" + EncryptText.Encrypt(user.response) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "update", "user", user);
                //SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function sets a new password for a user in the database
        /// </summary>
        /// <param name="user">a valid user with a valid user id in the system</param>
        /// <returns>boolean</returns>
        public static bool NewPassword(User user)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC NewPassword " + user.ID + ",'" + EncryptText.Encrypt(user.password) + "'," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "user", user);
                //SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function send a message to users with the new password created
        /// </summary>
        /// <param name="user">a valid user with a valid user id in the system, along with password and user credentials</param>
        public static void NewPasswordEmail(User user)
        {
            List<string> toList = new List<string>();
            toList.Add(EmailModel.GetPrimaryEmail(user).email);
           
            Mail mail = new Mail();
            mail.to = toList;
            mail.subject = "New IVIS Password";
            mail.body = "Your password has been reset. Please login using the following credentials and immediately update your password and security information.<br />  Username:  " + GetCompanyCode(user.employee.company.CompanyID) + user.username + " , Password: " + user.password + user.password + Environment.NewLine + "You have 48 hours to take action. Failure to take action within 48 hours will result in your account being deactivated."; ;

            MailModel.SendMailWithThisFunction(new List<User>() { user }, mail);
        }

        /// <summary>
        /// This function sets a speicific user as 'active' in the database
        /// </summary>
        /// <param name="id">valid user id in the system</param>
        /// <returns>boolean indicating as to whether the user is now active</returns>
        public static bool ActivateUser(int id)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC ActivateUser " + id + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "user", new User(id));
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function sets a speicific user as 'inactive' in the database
        /// </summary>
        /// <param name="id">valid user id in the system</param>
        /// <returns>boolean indicating as to whether the user is now inactive</returns>
        public static bool DeactivateUser(int id)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DeactivateUser " + id + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "user", new User(id));
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
                
                
                List<Email> emails = CompanyModel.GetCompanyAdminEmails(CompanyModel.GetMyCompany(new User(id)));
                List<string> to = new List<string>();
                foreach (Email email in emails)
                {
                    to.Add(email.email);
                }
                MailModel.SendMailWithThisFunction(new List<User>() { new User(id) }, new Mail(to, "User Deactivated", "The user: " + UserModel.GetUser(id).username + " has been deactivated from the system."));
            }
            return result;
        }
        
        /// <summary>
        /// IsActive tests if a user (based on username) is set as active in the system
        /// </summary>
        /// <param name="username">string that is a valid username</param>
        /// <returns>boolean indication if the user is active</returns>
        public static bool IsActive(string username)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsActive '" + Constants.CleanString(username) + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["active"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }
        
        /// <summary>
        /// THIS FUNCTION RETURNS WHETHER A USER HAS EXCEEDED HIS/HER LIMIT OF BAD LOGINS
        /// </summary>
        /// <param name="user">A valid user object with a valid username in the database</param>
        /// <returns>a boolean indicating as to whether the user is over their faulty login limit</returns>
        public static bool FaultyLoginLimit(User user)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC FaultyLimit '" + Constants.CleanString(user.username) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function tests as to whether a user is required to perform a first login update
        /// </summary>
        /// <param name="username">a valid username on the system</param>
        /// <returns>a boolean</returns>
        public static bool FirstLogin(string username = null, int id = 0)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsFirstLogin ";
                
                if (username != null) query += "'" + Constants.CleanString(username) + "'";
                else query += "'null'";

                if (id != 0) query += "," + id;

                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["firstlogin"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function tests if the time has passed since the user was created for the user
        /// to login for the first time
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static bool TestFirstLoginTimer(string username)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC TestFirstLoginTimer '" + Constants.CleanString(username) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function updates relevant data on first login of new users
        /// </summary>
        /// <param name="user">user object with new data</param>
        public static void FirstLoginUpdateData(User user)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC FirstLoginUpdateData '" + Constants.CleanString(user.username) + "','" + EncryptText.Encrypt(user.password) + "','" + Constants.CleanString(user.question) + "','" + EncryptText.Encrypt(user.response) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "update", "user", user);
                //SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function resets the time the first login was set to allow for another 24 hours for the user to login and reset his details
        /// </summary>
        /// <param name="user">valid user object witha a valid id</param>
        public static void ResetFirstLoginTimer(User user)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC ResetFirstLoginTimer " + user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function gets the user's last login date
        /// </summary>
        /// <returns>Last Login date of user</returns>
        public static DateTime GetLastLogin()
        {
            DateTime lastLogin = new DateTime();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetLastLogin " + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    if (DBNull.Value != reader["LastSuccessful"]) lastLogin = DateTime.Parse(reader["LastSuccessful"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return lastLogin;
        }

        /// <summary>
        /// This function gets the users associated to a particular company
        /// </summary>
        /// <returns>Last Login date of user</returns>
        public static List<User> getThisCompUsers(int compId)
        {
            sql sql = new sql();
            List<User> users = new List<User>();

            if (sql.ConnectSQL())
            {
                string query = "EXECUTE GetCompanyUsers " + compId ;
                SqlDataReader reader = sql.QuerySQL(query);
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        int ID = int.Parse(reader["ID"].ToString());
                        string Username = reader["Username"].ToString();
                        string fname = reader["FName"].ToString();
                        string lname = reader["LName"].ToString();
                        string TRN = reader["TRN"].ToString();

                        users.Add(new User(ID, Username, new Employee(new Person (fname, lname, TRN))));
                    }
                    reader.Close();
                }
                sql.DisconnectSQL();
            }
            return users;
        }

        /// <summary>
        /// This function gets the user asoicated with the specified username
        /// </summary>
        /// <returns>User</returns>
        public static User GetUserFromUsername(string username)
        {
            sql sql = new sql();
            User u = new User();

            if (sql.ConnectSQL())
            {
                string query = "EXECUTE GetUserFromUsername '" + Constants.CleanString(username) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        int ID = int.Parse(reader["UID"].ToString());
                        string fname = reader["FName"].ToString();
                        string lname = reader["LName"].ToString();
                        string TRN = reader["TRN"].ToString();
                        int peopleID = int.Parse(reader["PeopleID"].ToString());

                        u = new User(ID, username, new Employee(new Person(peopleID, fname, lname, TRN)));
                    }
                    reader.Close();
                }
                sql.DisconnectSQL();
            }
            return u;
        }

        /// <summary>
        /// this generates a new password for a user and emails it to them
        /// </summary>
        /// <param name="user"></param>
        public static Mail ResentPasswordToUser(User user,bool resend = false)
        {
            Mail mail = new Mail();
            string pword = PasswordGenerator();
            string body = "Your password has been reset to \"" + pword + "\"." + user.password + Environment.NewLine + "You have 48 hours to take action. Failure to take action within 48 hours will result in your account being deactivated." +" Please go to the IVIS main page " + HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port) + " to login and use the system.";
            List<string> to = new List<string>(); List<User> users = new List<User>();

            Email primaryEmail = EmailModel.GetPrimaryEmail(user);
            if (primaryEmail != null && primaryEmail.email != null && primaryEmail.email != "" && primaryEmail.emailID != 0)
                to.Add(primaryEmail.email);
            else
                to.Add(user.username); 
            users.Add(user);

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                mail = new Mail(to, "Reset Password", body);
                mail = MailModel.SendMailWithThisFunction(users, mail, resend);

                if (mail.sent)
                {
                    string query = "EXEC ResentPasswordToUser " + user.ID + ",'" + EncryptText.Encrypt(pword) + "'," + Constants.user.ID;
                    SqlDataReader reader = sql.QuerySQL(query, "update", "user", user);
                    //SqlDataReader reader = sql.QuerySQL(query);
                    reader.Close();
                    sql.DisconnectSQL();
                }

                //mail = new Mail(to, "Reset Password", body);
                //mail = MailModel.SendMailWithThisFunction(users, mail);
            }

            return mail;
        }

        /// <summary>
        /// this function resets a user's password and mails the new password to the user
        /// </summary>
        /// <param name="user"></param>
        public static void ResetQuestions(User user)
        {
            string pword = PasswordGenerator();
            string body = "Your password has been reset to \"" + pword + Environment.NewLine + "You have 48 hours to take action. Failure to take action within 48 hours will result in your account being deactivated." + "\". Please go to the IVIS main page " + HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port) + " to login and use the system. You will be prompted to reset your security question and response as well.";
            List<string> to = new List<string>(); List<User> users = new List<User>();
            to.Add(user.username); users.Add(user);
        
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC ResetQuestions " + user.ID + ",'" + EncryptText.Encrypt(pword) + "'," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "user", user);
                //SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();

                Mail mail = new Mail(to, "Reset Password", body);
                MailModel.SendMailWithThisFunction(users, mail);
            }

        }

        /// <summary>
        /// this function returns all groups and permissions in each group for a specific user
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static List<UserGroup> GetGroupsAndPermissions(int Id)
        {
            User user = new User(Id);
            List<UserGroup> usergroups = new List<UserGroup>();
            usergroups = UserGroupModel.GetMyUserGroups(user);

            foreach (UserGroup ug in usergroups)
            {
                ug.Permissions = UserPermissionModel.GetAllPermissionsForGroup(ug, ug.system);
            }

            return usergroups;
        }

        /// <summary>
        /// this function tests whether this user account has it's record set to a 'force reset'
        /// which will make the user upon login have to reset their password before gaining access 
        /// to the system
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static bool ForceReset(User user)
        {
            bool reset = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC ForceReset '" + Constants.CleanString(user.username) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    reset = (bool)reader["reset"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return reset;
        }

        /// <summary>
        /// this function tests if a IAJ created user exists in a company
        /// </summary>
        /// <param name="compID">company id</param>
        /// <returns></returns>
        public static bool userExistsWithCompany(int compID)
        {

            sql sql = new sql();
            bool exists = false;
            if (sql.ConnectSQL())
            {

                string query = "EXECUTE UserExistsWithCompany " + compID;

                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                     exists = (bool)reader["NumOfRecords"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }


            return exists;
        }

        /// <summary>
        /// this function tests if a specified user was created by the IAJ admin
        /// </summary>
        /// <param name="userID">user id</param>
        /// <returns></returns>
        public static bool IsIAJcreated(int userID)
        {

            sql sql = new sql();
            bool exists = false;
            if (sql.ConnectSQL())
            {

                string query = "EXECUTE IsIAJcreated " + userID;

                SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    exists = (bool)reader["NumOfRecords"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }


            return exists;
        }

        /// <summary>
        /// this function updates the username of a specific user
        /// </summary>
        /// <param name="user"></param>
        public static void updateUsername(User user)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE updateUsername " + user.ID + ",'" + Constants.CleanString(user.username) + "'," + user.employee.company.CompanyID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "user", user);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        public static bool TestAdminCredentials( string username, string password )
        {
            sql sql = new sql();
            bool DBsuccess = false;
            bool Psuccess = false;
            string pass = "";

            if (sql.ConnectSQL())
            {
                string query = "EXEC testAdminCred '" + username + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    DBsuccess = (bool) reader["success"];
                    pass = reader["pass"].ToString();
                }

                reader.Close();
                sql.DisconnectSQL();
            }

            if (DBsuccess)
            {
                Psuccess = EncryptText.TestPassword(password, pass);
            }
            

            return Psuccess;
        }

    }

    public class user_login_object
    {
        public User user { get; set; }
        public bool active { get; set; }
        public bool session_free { get; set; }
        public bool valid_user { get; set; }
        public bool first_login { get; set; }
        public bool first_login_timer_lapsed { get; set; }
        public bool force_reset { get; set; }
        public bool user_exist { get; set; }
        public bool faulty_login_limit_passed { get; set; }
        public bool company_active { get; set; }
        public bool db_error { get; set; }

        /// <summary>
        /// this is for the login process
        /// </summary>
        public user_login_object() { }
        /// <summary>
        /// this is for the login process
        /// </summary>
        /// <param name="user">user who attempted to logged in</param>
        /// <param name="active">user is active in system</param>
        /// <param name="session_free">user's session is free</param>
        /// <param name="valid_user">user is valid login</param>
        /// <param name="first_login">user's logging in for the first time</param>
        /// <param name="first_login_timer_lapsed">the timer for the first login process has lapsed</param>
        /// <param name="force_reset">the user account requires data to be reset</param>
        /// <param name="user_exist">the user exists in the database</param>
        /// <param name="faulty_login_limit_passed">logged in poorly too many times for the company at hand</param>
        public user_login_object(User user, bool active, bool session_free, bool valid_user, bool first_login, bool first_login_timer_lapsed, bool force_reset, bool user_exist, bool faulty_login_limit_passed)
        {
            this.user = user;
            this.active = active;
            this.session_free = session_free;
            this.valid_user = valid_user;
            this.first_login = first_login;
            this.first_login_timer_lapsed = first_login_timer_lapsed;
            this.force_reset = force_reset;
            this.user_exist = user_exist;
            this.faulty_login_limit_passed = faulty_login_limit_passed;
        }
    }

}

