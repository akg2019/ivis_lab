﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Data;
using OmaxFramework;

namespace Honeysuckle.Models
{
    public class VehicleCoverData
    {
        public VehicleCoverData() { }
        //public  string jsonData { get; set; }
        public string chassisno { get; set; }
        public string vehregno { get; set; }
        public string make { get; set; }
        public string model { get; set; }
        public string colour { get; set; }
        public int year { get; set; }
        public string registeredOwner { get; set; }
        public bool insured { get; set; }
        public List<VehicleCoverDataPolicies> active_policies { get; set; }

        public static List<string> parseJsonLoginInfo(JSON json)
        {
            JObject obj = JObject.Parse(json.json);
            List<string> output = new List<string>();


            try
            {
                if ((obj.Property("username") != null && obj.Property("password") != null) && (obj["username"].ToString() != "" && obj["password"].ToString() != ""))
                {
                    if (obj.Property("create_user") != null && (bool)obj["create_user"])
                    {

                        if (obj.Property("admin_user") != null && obj.Property("admin_pass") != null && UserModel.TestAdminCredentials(obj["admin_user"].ToString(), obj["admin_pass"].ToString()))
                        {
                            bool taken = (bool)eGovUser.CreateUser(obj["username"].ToString(), obj["password"].ToString())[0]["Taken"];
                            if (taken) output.Insert(0, "userTaken"); else output.Insert(0, "userCreated");
                        }
                        else output.Insert(0, "no admin");

                    }
                    else if (obj.Property("change_password") != null && (bool)obj["change_password"] && obj.Property("new_password") != null && obj["new_password"].ToString() != "")
                    {
                        bool successful = (bool)eGovUser.ChangePassword(obj["username"].ToString(), obj["password"].ToString(), obj["new_password"].ToString())[0]["isSuccessful"];
                        if (successful) output.Insert(0, "passwordUpdated"); else output.Insert(0, "passNotUpdated");
                        output.Insert(1, "");
                        output.Insert(2, "");
                        output.Insert(3, obj["username"].ToString());
                    }
                    else
                    {
                        JArray j = eGovUser.LoginUser(obj["username"].ToString(), obj["password"].ToString());
                        bool success = (bool)j[0]["isSuccessful"];
                        string sessionkey = j[0]["sessionkey"].ToString();
                        if (success)
                        {
                            output.Insert(0, "loggedIn");
                            output.Insert(1, sessionkey);
                            output.Insert(2, success.ToString());
                            output.Insert(3, obj["username"].ToString());

                        }
                        else
                        {
                            output.Insert(0, "logIn failed");
                            output.Insert(1, "");
                            output.Insert(2, "");
                            output.Insert(3, obj["username"].ToString());
                        }


                    }


                    /*
                     user_login_object ulo = UserModel.LoginUser(new User(obj["username"].ToString(), obj["password"].ToString()));
                     if (ulo.session_free && ulo.valid_user)
                     {
                         Constants.user = UserModel.GetUser(obj["username"].ToString());
                         UserModel.ResetFaultyLogins(obj["username"].ToString());
                         Constants.user.sessionguid = UserModel.GetSessionGUID(Constants.user);
                         output = "loggedIn";
                     }
                     else output = "logIn failed";
                     * */
                }
                if (obj.Property("sessionkey") != null)
                {
                    if ((bool)eGovUser.CheckLogin(obj["sessionkey"].ToString())[0]["success"])
                    {

                        if (obj.Property("logout") != null)
                        {
                            if ((bool)obj["logout"])
                            {
                                bool l = (bool)eGovUser.Logout(obj["sessionkey"].ToString())[0]["success"];
                                output.Insert(0, "loggedOut");
                            }
                        }
                        else if (obj.Property("request_data") != null)
                        {

                            //what to show
                            //var t = obj["request_data"];
                            if ((bool)eGovUser.CheckSession(obj["sessionkey"].ToString())[0]["successful"])
                            { //Add function here to return data

                                //Response Object 
                                JObject Response = new JObject();

                                //Checks if its a regular TAJ search
                                bool IsReqular = false;
                                try
                                {
                                    string checks = obj["request_data"]["driver_trn"].ToString(); IsReqular = true;

                                }

                                catch (Exception) { IsReqular = false; }

                                //Checks if its an Epic Assure Search
                                bool IsEpicAssure = false; try { string checks = obj["request_data"]["risk_search"].ToString(); IsEpicAssure = true; }
                                catch (Exception) { }

                                if (IsReqular)
                                {
                                    output.Insert(0, parseRequestData(obj["request_data"]));
                                }
                                else
                                {
                                    if (IsEpicAssure)
                                    {
                                        try
                                        {
                                            string sqlConnectionStringAssure = "";
                                            #if DEBUG
                                            sqlConnectionStringAssure = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;//ApplicationServices
                                            #else
                                                sqlConnectionStringAssure = System.Configuration.ConfigurationManager.ConnectionStrings["ExternalConnection"].ConnectionString; 
                                            #endif

                                            JObject EpicAssureObject = new JObject();
                                            JArray PersonInfo = new JArray();
                                            JArray RiskInfo = new JArray();

                                            Response.Add("success", true);

                                            if (bool.Parse(obj["request_data"]["global_name_search"].ToString())) //Global Name Search
                                            {

                                                foreach (string trn in obj["request_data"]["trn"].Children())
                                                {
                                                    string Query = " exec EpicAssureGetGlobalName '" + trn + "'";

                                                    JObject P = new JObject();

                                                    DataTable dt = new DataTable();
                                                    dt = (DataTable)(OmaxFramework.Utilities.OmaxData.HandleQuery(Query, sqlConnectionStringAssure, Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery));
                                                    P.Add("person", JArray.Parse(JsonConvert.SerializeObject(dt)));
                                                    PersonInfo.Add(P);
                                                }


                                                Response.Add("global_name", PersonInfo);

                                               

                                            }

                                            if (bool.Parse(obj["request_data"]["risk_search"].ToString())) //Vehicle Data Search
                                            {

                                                foreach (string chassis in obj["request_data"]["chassis_number"].Children())
                                                {
                                                    string Query = "exec EpicAssureRiskSearch '" + chassis + "'";
                                                    JObject C = new JObject();

                                                    DataTable dt = new DataTable();
                                                    dt = (DataTable)(OmaxFramework.Utilities.OmaxData.HandleQuery(Query, sqlConnectionStringAssure, Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery));
                                                    C.Add("vehicle", JArray.Parse(JsonConvert.SerializeObject(dt)));
                                                    RiskInfo.Add(C);
                                                }


                                                foreach (string chassis in obj["request_data"]["chassis_number"].Children())
                                                {
                                                    string Query = "exec EpicAssureRiskSearch '" + chassis + "'";
                                                    JObject C = new JObject();

                                                    DataTable dt = new DataTable();
                                                    dt = (DataTable)(OmaxFramework.Utilities.OmaxData.HandleQuery(Query, sqlConnectionStringAssure, Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery));
                                                    C.Add("vehicle", JArray.Parse(JsonConvert.SerializeObject(dt)));
                                                    RiskInfo.Add(C);
                                                }


                                                Response.Add("risk", RiskInfo);
                                            }


                                            Response["success"] = true;
                                            output.Insert(0, JsonConvert.SerializeObject(Response));

                                        }
                                        catch (Exception) { Response["success"] = false; }
                                    }
                                    else
                                    {

                                        bool vehicleSearch = false;
                                        try { var v = obj["request_data"]["chassis_no"].ToString(); vehicleSearch = true; }
                                        catch (Exception) { vehicleSearch = false; }

                                        if (vehicleSearch)
                                        {
                                            JArray ChassisCovered = new JArray();
                                            JArray LicenseCovered = new JArray();

                                            foreach (var Chassis in obj["request_data"]["chassis_no"].Children())
                                            {
                                                JObject R = new JObject();
                                                R.Add("chassis", Chassis.ToString());
                                                R.Add("data", VehicleCoverData.GetCoveredVehicle(Chassis.ToString()));
                                                ChassisCovered.Add(R);
                                            }

                                            foreach (var license in obj["request_data"]["license_no"].Children())
                                            {
                                                JObject R = new JObject();
                                                R.Add("license", license.ToString());
                                                R.Add("data", VehicleCoverData.GetCoveredVehicle("", license.ToString()));
                                                LicenseCovered.Add(R);
                                            }



                                            Response.Add("success", true);
                                            Response.Add("chassis_covered", ChassisCovered);
                                            Response.Add("license_covered", LicenseCovered);
                                            output.Insert(0, JsonConvert.SerializeObject(Response));
                                        }
                                        else
                                        {
                                            output.Insert(0, parseRequestDataAMVS_Pull(obj["request_data"]));

                                        }

                                    }



                                }

                            }
                            else
                            {
                                output.Insert(0, "sessionend");
                            }


                            //JObject j = JObject.Parse(returnedData);
                            //returnedData = "";
                        }
                        else output.Insert(0, "no data requested");
                    }
                    else output.Insert(0, "failed");
                    
                        output.Insert(1, "");
                        output.Insert(2, "");
                        if (eGovUser.GetUserFromKey(obj["sessionkey"].ToString())[0]["username"] != null)
                            output.Insert(3, eGovUser.GetUserFromKey(obj["sessionkey"].ToString())[0]["username"].ToString());
                        else
                            output.Insert(3, "");

                    
                }
            }
            catch
            {
                output.Insert(0, "failed");
            }

            return output;
        }

        private static string parseRequestData(JToken search)
        {
            string driverTRN = "";
            string chassisNo = "";
            string vehRegNo = "";
            string comment = "";
            bool narrowSearch = false;
            bool success = true;
            string section = "";
            string code = "";
            List<VehicleCoverData> vcdList = new List<VehicleCoverData>();

            try
            {
                /*all records 
                bool all_records = (bool) requestData["all_records"];
                if (all_records)
                {
                    //get all vehicle info
                }
                else
                {*/

                //use serach criteria

                //JToken search = requestData["search_criteria"];

                comment = "Returned records relating to ";
                section = "chassis_no";
                if (search["chassis_no"] != null && search["chassis_no"].ToString() != "")
                {
                    chassisNo = search["chassis_no"].ToString();
                    comment += "chassis number: " + chassisNo + ", ";
                    narrowSearch = true;
                }
                section = "veh_reg_no";
                if (search["veh_reg_no"] != null && search["veh_reg_no"].ToString() != "")
                {
                    vehRegNo = search["veh_reg_no"].ToString();
                    comment += "vehicle reg number: " + vehRegNo + ", ";
                    narrowSearch = true;
                }
                section = "driver_trn";
                if (search["driver_trn"] != null && search["driver_trn"].ToString() != "")
                {
                    driverTRN = search["driver_trn"].ToString();
                    comment += "driver trn: " + driverTRN + " ";
                    narrowSearch = true;
                }

                comment += "specified";
                if (!narrowSearch)
                {
                    comment = "No search criterion defined. Please check your json string for correct tags";
                    success = false;
                }




                if (!narrowSearch) vcdList = new List<VehicleCoverData>();
                else
                    vcdList = getVehicleData(false, driverTRN, chassisNo, vehRegNo);


            }
            catch
            {
                comment = "No search criterion defined. Please check your json string for correct tags. Start at " + section;
                success = false;
            }

            if (success) code = "IVIS100"; else code = "IVIS500";

            return "{\"success\":" + JsonConvert.SerializeObject(success) + ",\"status\":\"Data received\",\"code\":\"" + code + "\",\"comment\":\"" + comment + "\"," + "\"vehicle_data\":" + JsonConvert.SerializeObject(vcdList) + "}";
        }


        /// <summary>
        /// AMVS/IVIS Data Exchange
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        private static string parseRequestDataAMVS_Pull(JToken search)
        {
            int number_of_policies = 1;
            int PageNumber = 1;
            int PageSize = 100;
            string comment = "";
            bool success = true;
            string section = "";
            string code = "";

            //int.TryParse(search["number_of_policies"].ToString(), out number_of_policies);
            
            string FromDate = "";
            string ToDate = "";

            try {FromDate = search["from_date"].ToString(); }catch(Exception){}
            try {ToDate = search["to_date"].ToString(); }catch(Exception){}
            int.TryParse(search["page_number"].ToString(), out PageNumber);
            int.TryParse(search["page_size"].ToString(), out PageSize);

            try
            {
                if (number_of_policies > 0)
                {
                    return "{\"success\":" + JsonConvert.SerializeObject(success) + ",\"status\":\"Data received\",\"code\":\"" + code + "\",\"comment\":\"" + comment + "\"," + "\"vehicle_data\":" + JsonConvert.SerializeObject(new eGovDataTransfer().GeteGovPolicyData(FromDate,ToDate,PageSize,PageNumber)) + "}";
                }
                else
                {
                    section = "number_of_policies";
                    comment = "State the number of policies to return. Start at " + section;
                    success = false;
                    return "{\"success\":" + JsonConvert.SerializeObject(success) + ",\"status\":\"Data received\",\"code\":\"" + code + "\",\"comment\":\"" + comment + "\"," + "\"vehicle_data\":" + JsonConvert.SerializeObject(new JObject()) + "}";
                }

            }
            catch
            {
                comment = "There was an error when trying to process data. Please try again later" + section;
                success = false;
            }

            if (success) code = "IVIS100"; else code = "IVIS500";

            return "{\"success\":" + JsonConvert.SerializeObject(success) + ",\"status\":\"Data received\",\"code\":\"" + code + "\",\"comment\":\"" + comment + "\"," + "\"vehicle_data\":" + JsonConvert.SerializeObject(new JObject()) + "}";
        }

        private static List<VehicleCoverData> getVehicleData(bool all_records, string driverTRN, string chassisNo, string vehRegNo)
        {
            VehicleCoverData vcd = new VehicleCoverData();
            List<VehicleCoverData> vcdList = new List<VehicleCoverData>();
            vcd.active_policies = new List<VehicleCoverDataPolicies>();
            VehicleCoverDataPolicies policy = new VehicleCoverDataPolicies();
            List<string> chassisList = new List<string>();
            string query = "";

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                query = "EXEC GetVehicleData " + all_records + ",'" + chassisNo + "','" + vehRegNo + "','" + driverTRN + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    if (!chassisList.Contains(reader["ChassisNo"].ToString()))
                    {
                        if (vcd.chassisno != null && vcd.chassisno != "")
                        {
                            vcdList.Add(vcd);
                            vcd = new VehicleCoverData();
                            vcd.active_policies = new List<VehicleCoverDataPolicies>();
                        }

                        if (DBNull.Value != reader["ChassisNo"]) vcd.chassisno = reader["ChassisNo"].ToString();
                        if (DBNull.Value != reader["VehicleRegistrationNo"]) vcd.vehregno = reader["VehicleRegistrationNo"].ToString();
                        if (DBNull.Value != reader["Make"]) vcd.make = reader["Make"].ToString();
                        if (DBNull.Value != reader["Model"]) vcd.model = reader["Model"].ToString();
                        if (DBNull.Value != reader["Colour"]) vcd.colour = reader["Colour"].ToString();
                        if (DBNull.Value != reader["VehicleYear"]) vcd.year = int.Parse(reader["VehicleYear"].ToString());
                        if (DBNull.Value != reader["RegisteredOwner"]) vcd.registeredOwner = reader["RegisteredOwner"].ToString();

                        vcd.insured = PolicyModel.IsVehicleCovered(vcd.chassisno);


                        chassisList.Add(vcd.chassisno);
                    }
                    policy = new VehicleCoverDataPolicies();
                    if (DBNull.Value != reader["CompanyName"]) policy.insurance_company = reader["CompanyName"].ToString();
                    if (DBNull.Value != reader["CertificateNo"]) policy.certificate_number = reader["CertificateNo"].ToString();
                    if (DBNull.Value != reader["CoverNoteID"]) policy.cover_note_id = reader["CoverNoteID"].ToString();
                    if (DBNull.Value != reader["Policyno"]) policy.policy_number = reader["Policyno"].ToString();
                    if (DBNull.Value != reader["Cover"]) policy.type_of_coverage = reader["Cover"].ToString();

                    try
                    {
                        if (DBNull.Value != reader["StartDateTime"]) policy.issue_date = DateTime.Parse(reader["StartDateTime"].ToString());
                        if (DBNull.Value != reader["EndDateTime"]) policy.expiry_date = DateTime.Parse(reader["EndDateTime"].ToString());
                    }
                    catch
                    {
                        //Dates failed
                    }

                    if (policy.policy_number != null && policy.policy_number != "") vcd.active_policies.Add(policy);





                    //vcdList.Add(vcd);
                    //vcd.active_policies = new List<VehicleCoverDataPolicies>();
                    //vcd = new VehicleCoverData();

                    //chassisList.Add(vcd.chassisno);

                }

                if (vcd.chassisno != null && vcd.chassisno != "")
                {
                    vcdList.Add(vcd);
                    vcd = new VehicleCoverData();
                    vcd.active_policies = new List<VehicleCoverDataPolicies>();
                }

                reader.Close();
                sql.DisconnectSQL();


            }

            return vcdList;
        }


        /// <summary>
        /// eGove cover vehicle search.
        /// </summary>
        /// <param name="chassis">Vehicle chassis number</param>
        /// <param name="license">Vehicle reg number</param>
        /// <returns></returns>
        public static JObject GetCoveredVehicle(string chassis = "", string license = "")
        {

            chassis = chassis.Replace(" ", "").Replace("-", "");
            license = license.Replace(" ", "").Replace("-", "");

            if (chassis.Trim() == string.Empty) chassis = null;
            if (license.Trim() == string.Empty) license = null;

            if (chassis == null && license == null) return new JObject("message", "failed");

            try
            {
                string Query = " Exec IsVehicleCoveredDETAILS ";
                if (chassis == null) Query += "null,'" + license + "'";
                if (license == null) Query += "'" + chassis + "',null";

                JObject JResponse = new JObject();
                JResponse.Add("vehicle", JArray.Parse(JsonConvert.SerializeObject(OmaxFramework.Utilities.OmaxData.HandleQuery(Query, Conn, Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery))));
                return JResponse;
            }
            catch (Exception)
            {
                return new JObject("message", "failed");
            }

        }


        public static bool isUriHttps(Uri url)
        {

            bool secure = false;
            if (url.Scheme == Uri.UriSchemeHttps) secure = true;

            return secure;
        }

        public static List<string> GetWhiteIPs()
        {
            List<string> ips = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                try
                {
                    SqlDataReader reader = sql.QuerySQL("EXEC GetWhiteIPs");
                    while (reader.Read())
                    {
                        ips.Add(reader["ip"].ToString());
                    }

                    reader.Close();
                    sql.DisconnectSQL();
                }
                catch
                {
                }
            }

            return ips;
        }


        /// <summary>
        /// Checks if this user is a client
        /// </summary>
        /// <returns></returns>
        public static bool IsClient(string Username)
        {
            bool IsClient = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                try
                {
                    SqlDataReader reader = sql.QuerySQL("EXEC IsClient '" + Username + "'");
                    while (reader.Read())
                    {
                       IsClient = bool.Parse(reader["IsClient"].ToString());
                    }

                    reader.Close();
                    sql.DisconnectSQL();
                }
                catch
                {
                }
            }

            return IsClient;
        }

        public class VehicleCoverDataPolicies
        {

            public string insurance_company { get; set; }
            public DateTime issue_date { get; set; }
            public DateTime expiry_date { get; set; }
            public string certificate_number { get; set; }
            public string cover_note_id { get; set; }
            public string type_of_coverage { get; set; }
            public string policy_number { get; set; }

        }

        public static readonly string Conn = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
        public class eGovUser
        {
            public static string username { get; set; }
            public static string password { get; set; }
            public static bool loggedIn { get; set; }
            public static bool active { get; set; }


            public static JArray CreateUser(string username, string password)
            {
                return JArray.Parse(JsonConvert.SerializeObject(
                        Utilities.OmaxData.HandleQuery("eGovCreateUser '" + username + "','" + Utilities.InformationHiding.MakeIllusion(password) + "'",
                        Conn,
                        Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery

                    )));
            }

            public static JArray LoginUser(string username, string password)
            {
                return JArray.Parse(JsonConvert.SerializeObject(
                        Utilities.OmaxData.HandleQuery("eGovSetSession '" + username + "','" + Utilities.InformationHiding.MakeIllusion(password) + "'",
                        Conn,
                        Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery

                    )));
            }

            public static JArray ChangePassword(string username, string password, string newPassword)
            {
                return JArray.Parse(JsonConvert.SerializeObject(
                        Utilities.OmaxData.HandleQuery("eGovChangePassword '" + username + "','" + Utilities.InformationHiding.MakeIllusion(password) + "','" + Utilities.InformationHiding.MakeIllusion(newPassword) + "'",
                        Conn,
                        Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery

                    )));
            }

            public static JArray CheckSession(string sessionkey)
            {
                return JArray.Parse(JsonConvert.SerializeObject(
                        Utilities.OmaxData.HandleQuery("eGovInvalidateSession '" + sessionkey + "'",
                        Conn,
                        Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery

                    )));
            }

            public static JArray CheckLogin(string sessionkey)
            {
                return JArray.Parse(JsonConvert.SerializeObject(
                        Utilities.OmaxData.HandleQuery("eGovCheckLogin '" + sessionkey + "'",
                        Conn,
                        Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery

                    )));
            }

            public static JArray Logout(string sessionkey)
            {
                return JArray.Parse(JsonConvert.SerializeObject(
                        Utilities.OmaxData.HandleQuery("eGovLogoutUser '" + sessionkey + "'",
                        Conn,
                        Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery

                    )));
            }

            public static JArray GetUserFromKey(string sessionkey)
            {
                return JArray.Parse(JsonConvert.SerializeObject(
                        Utilities.OmaxData.HandleQuery("eGovGetUserUsingSession '" + sessionkey + "'",
                        Conn,
                        Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery

                    )));
            }



            //Check if vehicle and or policy exist
            public JObject CheckIfDataExists(JArray ChasssisList)
            {
                string sqlConnectionString = "";
#if DEBUG
                sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;//ApplicationServices
#else
                  sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ExternalConnection"].ConnectionString; 
#endif

                JObject Response = new JObject();
                Response.Add("success", false);
                Response.Add("message", "");
                Response.Add("Users", "");
                string Query = "";

                try
                {
                    Response["success"] = true;
                    Response["message"] = "See user list";
                    Response["Users"] = JArray.Parse(JsonConvert.SerializeObject(Utilities.OmaxData.HandleQuery(Query, sqlConnectionString, Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery)));
                }
                catch (Exception e)
                {
                    Response["success"] = false;
                    Response["message"] = "Error getting users";
                    Response["Users"] = e.Message;
                }

                return Response;




            }




        }
    }


}