﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace Honeysuckle.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class VehicleCertificate
    {
        public int VehicleCertificateID { get; set; }
        public string EDIID { get; set; }
        public Company company { get; set; }
        public Policy policy { get; set; }
        public string CertificateNo { get; set; }

        [RegularExpression("[a-zA-Z0-9][a-zA-Z0-9-#&,:;/ ]*[a-zA-Z0-9]", ErrorMessage = "Incorrect Certificate-type format (only, letters, numbers and select symbols allowed) ")]
        public string CertificateType { get; set; }

        [RegularExpression("[a-zA-Z0-9][a-zA-Z0-9-#&,:;/ ]*[a-zA-Z0-9]*", ErrorMessage = "Incorrect Insured-code format (only, letters, numbers and select symbols allowed) ")]
        public string InsuredCode { get; set; }

        [RegularExpression("[0-9]+", ErrorMessage = "Incorrect ExtensionCode format (only numbers and select symbols allowed) ")]
        public int ExtensionCode { get; set; }

        public int UniqueNum { get; set; }

        [RegularExpression("[a-zA-Z0-9][a-zA-Z0-9-#&,:;/ ]*[a-zA-Z0-9]", ErrorMessage = "Incorrect usage format (only, letters, numbers and select symbols allowed) ")]
        public string Usage { get; set; }

        [RegularExpression("[a-zA-Z0-9][a-zA-Z0-9-#&,:;/ ]*[a-zA-Z0-9]", ErrorMessage = "Incorrect scheme format (only, letters, numbers and select symbols allowed) ")]
        public string Scheme { get; set; }

        public DateTime EffectiveDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime DateFirstPrinted { get; set; }
        public DateTime LastPrintedOn { get; set; }
        public Person LastPrintedBy { get; set; }
        public string last_printed_by_flat { get; set; }
        public string first_printed_by_flat { get; set; }
        public int printcount { get; set; }
        public int printcountInsurer { get; set; }

        [RegularExpression("[0-9-,]+", ErrorMessage = "Incorrect Endorsement-number format (only numbers and select symbols allowed) ")]
        public string EndorsementNo { get; set; }

        [RegularExpression("[0-9]+", ErrorMessage = "Incorrect Printed Paper Number format (only numbers and select symbols allowed) ")]
        public string PrintedPaperNo { get; set; }

        public Vehicle Risk { get; set; }
        public DateTime SignedAt { get; set; }
        public Person SignedBy { get; set; }

        [RegularExpression("[0-9]+[0-9-,]*[0-9]*", ErrorMessage = "Incorrect MarksRegNo format (only numbers and select symbols allowed) ")]
        public string MarksRegNo { get; set; }

        public bool Cancelled { get; set; }
        public string CancelReason { get; set; }
        public Person CancelledBy { get; set; }
        public DateTime CancelledOn { get; set; }
        public TemplateWording wording { get; set; }
        public VehicleCoverNoteGenerated generated_cert { get; set; }
        public bool imported { get; set; }
        public bool approved { get; set; }
        public DateTime createdOn { get; set; }
        public string PolicyHolders { get; set; }

        public string limitsofuse { get; set; }
        //public string Usage { get; set; }
        public string AuthorizedWording { get; set; }

        public string PolicyNo { get; set; }
        public string PolicyHoldersAddress { get; set; }
        public string CovernoteNo { get; set; }
        public string Period { get; set; }
        public string effectiveTime { get; set; }
        public string expiryTime { get; set; }
        //public string effectivedate { get; set; }
        //public string expirydate { get; set; }
        //public string endorsementNo { get; set; }
        public string bodyType { get; set; }
        public string seating { get; set; }
        public string chassisNo { get; set; }
        //public string certificateType { get; set; }
        //public string certitificateNo { get; set; }
        public string certificateCode { get; set; }
        public string engineNo { get; set; }
        public string VIN { get; set; }
        public string HPCC { get; set; }
        public string companyCreatedBy { get; set; }
        public string referenceNo { get; set; }
        public string estimatedValue { get; set; }

        public string HeaderImgLocation { get; set; }
        public string FooterImgLocation { get; set; }
        public string LogoLocation { get; set; }
        public string PolicyTypes { get; set; }
        public string Cover { get; set; }
        public int VehicleID { get; set; }
        public bool CancelledStatus { get; set; }
        //public bool Approved { get; set; }
        public int PolicyID { get; set; }
        public string CompanyName { get; set; }
        public int VehicleMetaDataID { set; get; }
        //public int PrintCount { set; get; }
        public string ManualCoverNoteNumber { get; set; }
        public int CompanyID { set; get; }
        public int IntermediaryID { set; get; }
       public int WordID { set; get; }
       public string vehyear { get; set; }
       public string vehmake { get; set; }
       public string vehregno { get; set; }
       public string vehExt { get; set; }
       public string drivers { get; set; }
       public string vehdesc { get; set; }
       public string vehmodeltype { get; set; }
       public string vehmodel { get; set; }
       //public string authorizedwording { get; set; }
       public string mortgagees { get; set; }
       public DateTime DateLastPrinted { set; get; }
       public string Usages { get; set; }

       public VehicleCertificate() { }
       public VehicleCertificate(int VehicleCertificateID)
        {
            this.VehicleCertificateID = VehicleCertificateID;
        }
        public VehicleCertificate
            (
                int VehicleCertificateID,
                Company company,
                Policy policy,
                string CertificateType,
                string InsuredCode,
                int ExtensionCode,
                int UniqueNum,
                DateTime EffectiveDate,
                DateTime ExpiryDate,
                DateTime DateFirstPrinted,
                DateTime LastPrintedOn,
                string EndorsementNo,
                string PrintedPaperNo,
                Vehicle Risk,
                DateTime SignedAt,
                Person SignedBy,
                string MarksRegNo,
                bool Cancelled,
                Person CancelledBy,
                DateTime CancelledOn
            )
        {
            this.VehicleCertificateID = VehicleCertificateID;
            this.company = company;
            this.policy = policy;
            this.CertificateType = CertificateType;
            this.InsuredCode = InsuredCode;
            this.ExtensionCode = ExtensionCode;
            this.UniqueNum = UniqueNum;
            this.EffectiveDate = EffectiveDate;
            this.ExpiryDate = ExpiryDate;
            this.DateFirstPrinted = DateFirstPrinted;
            this.LastPrintedOn = LastPrintedOn;
            this.EndorsementNo = EndorsementNo;
            this.PrintedPaperNo = PrintedPaperNo;
            this.Risk = Risk;
            this.SignedAt = SignedAt;
            this.SignedBy = SignedBy;
            this.MarksRegNo = MarksRegNo;
            this.Cancelled = Cancelled;
            this.CancelledBy = CancelledBy;
            this.CancelledOn = CancelledOn;
        }

        public VehicleCertificate(Company company, Vehicle Risk, Policy policy)
        {
            this.company = company;
            this.Risk = Risk;
            this.policy = policy;
        }

        public VehicleCertificate(Policy policy)
        {
            this.policy = policy;
        }

        public VehicleCertificate(int VehicleCertificateID, Company company, Policy policy, Vehicle Risk)
        {
            this.VehicleCertificateID = VehicleCertificateID;
            this.company = company;
            this.policy = policy;
            this.UniqueNum = UniqueNum;
            this.PrintedPaperNo = PrintedPaperNo;
            this.Risk = Risk;
            this.wording = wording;

        }

        public VehicleCertificate(int VehicleCertificateID, Company company, Policy policy, int UniqueNum, DateTime EffectiveDate, DateTime ExpiryDate)
        {
            this.VehicleCertificateID = VehicleCertificateID;
            this.company = company;
            this.policy = policy;
            this.UniqueNum = UniqueNum;
            this.EffectiveDate = EffectiveDate;
            this.ExpiryDate = ExpiryDate;
        }

        public VehicleCertificate(int VehicleCertificateID, Company company, Policy policy, Vehicle Risk, DateTime EffectiveDate, DateTime ExpiryDate, string CertificateType)
        {
            this.VehicleCertificateID = VehicleCertificateID;
            this.company = company;
            this.policy = policy;
            this.Risk = Risk;
            this.EffectiveDate = EffectiveDate;
            this.ExpiryDate = ExpiryDate;
            this.CertificateType = CertificateType;
        }


    }


    public class VehicleCertificateModel
    {

        /// <summary>
        /// this function returns all certs in a certain date range
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<VehicleCertificate> CoverSearchCerts(DateTime start, DateTime end, User user)
        {
            List<VehicleCertificate> certs = new List<VehicleCertificate>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC CoverSearchCerts '" + start.ToString("yyyy-MM-dd HH:mm:ss") + "','" + end.ToString("yyyy-MM-dd HH:mm:ss") + "'," + user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    VehicleCertificate cert = new VehicleCertificate();
                    cert.generated_cert = new VehicleCoverNoteGenerated();
                    cert.Risk = new Vehicle();
                    cert.policy = new Policy();

                    cert.EffectiveDate = DateTime.Parse(reader["effective"].ToString());
                    cert.ExpiryDate = DateTime.Parse(reader["expiry"].ToString());


                    cert.SignedAt = DateTime.Parse(reader["signon"].ToString());
                    cert.SignedBy = new Person(reader["signtrn"].ToString());

                    cert.Risk.ID = int.Parse(reader["risk_id"].ToString());
                    cert.Risk.chassisno = reader["chassis"].ToString();
                    cert.policy.ID = int.Parse(reader["polid"].ToString());

                    cert.Risk.AuthorizedDrivers = DriverModel.getAuthorizedDrivers(cert.Risk, cert.policy);

                    cert.Cancelled = (bool)reader["cancelled"];

                    #region optional data
                    if (DBNull.Value != reader["vin"]) cert.Risk.VIN = reader["vin"].ToString();
                    if (DBNull.Value != reader["ediid"]) cert.EDIID = reader["ediid"].ToString();
                    if (DBNull.Value != reader["certno"]) cert.CertificateNo = reader["certno"].ToString();
                    if (DBNull.Value != reader["firstprinted"]) cert.DateFirstPrinted = DateTime.Parse(reader["firstprinted"].ToString());
                    if (DBNull.Value != reader["lastprinted"]) cert.LastPrintedOn = DateTime.Parse(reader["lastprinted"].ToString());
                    if (DBNull.Value != reader["lastprintedbytrn"]) cert.LastPrintedBy = new Person(reader["lastprintedbytrn"].ToString());
                    if (DBNull.Value != reader["endorsementno"]) cert.EndorsementNo = reader["endorsementno"].ToString();
                    if (DBNull.Value != reader["printedpaperno"]) cert.PrintedPaperNo = reader["printedpaperno"].ToString();
                    if (DBNull.Value != reader["limits"]) cert.generated_cert.limitsofuse = reader["limits"].ToString();
                    if (DBNull.Value != reader["marksregno"]) cert.MarksRegNo = reader["marksregno"].ToString();
                    if (cert.Cancelled)
                    {
                        if (DBNull.Value != reader["cancelledtrn"]) cert.CancelledBy = new Person(reader["cancelledtrn"].ToString());
                        if (DBNull.Value != reader["cancelledreason"]) cert.CancelReason = reader["cancelledreason"].ToString();
                        if (DBNull.Value != reader["cancelledon"]) cert.CancelledOn = DateTime.Parse(reader["cancelledon"].ToString());
                    }
                    #endregion

                    certs.Add(cert);

                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return certs;
        }
        #region old
        /// <summary>
        /// This function searches the certificates table, given a particular search criteria
        /// </summary>
        /// <param name="search">Search criteria</param>
        /// <return>Risk certificate object lists - containing relevant Risk Certificate data, and matching the search criteria</returns>
        //public static List<VehicleCertificate> CertificateGenSearch(string search)
        //{
        //    List<VehicleCertificate> certs = new List<VehicleCertificate>();
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        string query = "EXEC CertificateGenSearch '" + Constants.CleanString(search) + "'";
        //        SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
        //        while (reader.Read())
        //        {
        //            certs.Add(new VehicleCertificate(int.Parse(reader["ID"].ToString()),
        //                                             new Company(reader["CompanyName"].ToString()),
        //                                             new Policy(int.Parse(reader["PID"].ToString()), reader["PolicyNumber"].ToString(), new PolicyCover(reader["cover"].ToString())),
        //                                             int.Parse(reader["unique"].ToString()),
        //                                             DateTime.Parse(reader["effective"].ToString()),
        //                                             DateTime.Parse(reader["expiry"].ToString())));
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return certs;
        //}
        #endregion
        /// <summary>
        /// This function generates the details about a Risk, in order to create its certificate
        /// </summary>
        /// <param name="vehicleId">Valid Risk Id in the system</param>
        /// <returns VehicleCertificate>Risk object - Filled with relevant Risk Certificate data for creating the certificate</returns>
        public static VehicleCertificate PullVehiclePolData(int vehicleId, int compID = 0)
        {
            VehicleCertificate cert = new VehicleCertificate();
            Vehicle veh = new Vehicle();
            Person per = new Person();
            Policy pol = new Policy();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC PullVehiclePolData " + vehicleId + "," + Constants.user.ID + "," + compID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if (DBNull.Value != reader["TRN"])
                        {
                            per.PersonID = int.Parse(reader["PeopleID"].ToString());
                            per.TRN = reader["TRN"].ToString();
                            if (DBNull.Value != reader["FName"]) per.fname = reader["FName"].ToString();
                            if (DBNull.Value != reader["LName"]) per.lname = reader["LName"].ToString();
                        }

                        veh.ID = vehicleId;
                        if (DBNull.Value != reader["VIN"]) veh.VIN = reader["VIN"].ToString();
                        veh.make = reader["Make"].ToString();
                        veh.VehicleModel = reader["Model"].ToString();
                        veh.mainDriver = new Driver(per);

                        pol.ID = int.Parse(reader["PolicyID"].ToString());
                        pol.insuredby = new Company(int.Parse(reader["CompanyID"].ToString()));
                        pol.policyCover = new PolicyCover(reader["Cover"].ToString());
                        pol.policyCover.ID = int.Parse(reader["ID"].ToString());
                        pol.policyNumber = reader["Policyno"].ToString();

                        veh.AuthorizedDrivers = VehicleModel.GetDriverList(vehicleId, pol.ID, 1);
                        veh.ExceptedDrivers = VehicleModel.GetDriverList(vehicleId, pol.ID, 2);
                        veh.ExcludedDrivers = VehicleModel.GetDriverList(vehicleId, pol.ID, 3);



                        cert = new VehicleCertificate(new Company(int.Parse(reader["CompanyID"].ToString())), veh, pol);

                    }
                }

                reader.Close();
                sql.DisconnectSQL();
            }
            return cert;
        }

        /// <summary>
        /// This function creates a new Risk certificate
        /// </summary>
        /// <param name="vehicleCert"> Risk Certificate object , storing all the certificate data</param>
        /// <returns>Integer- certificate Id </returns>
        public static int CreateCertificate(VehicleCertificate vehicleCert, int uid, bool import = false)
        {
            int certID = 0;
            int wid = 0;
            string compShort = CompanyModel.GetShortenedCode(vehicleCert.company.CompanyID);
            TemplateWording wording = new TemplateWording();
            int uniqueNumber = GenerateUniqueNum(vehicleCert.company.CompanyID);

            if (vehicleCert.PrintedPaperNo == null) vehicleCert.PrintedPaperNo = "0";

            if (vehicleCert.wording != null) wid = vehicleCert.wording.ID;
            wording = TemplateWordingModel.GetWording(wid);
            if (vehicleCert.CertificateNo == null || vehicleCert.CertificateNo == "") vehicleCert.CertificateNo = compShort + uniqueNumber.ToString();
           
                    try {vehicleCert.Risk = VehicleModel.GetVehicle(vehicleCert.Risk.ID); }catch(Exception){}

                    //Get Relevant information for to Generate Certificates
                    string NamesOnPolicy = "";
                    string DriverNames = "";
                    string AuthDrivers = "";
                    string ExcludedDrivers = "";
                    string ExceptedDrivers = "";
                    string PolicyHoldersAddress = "";
                    //Gets polcy holders address
                    try
                    {
                        if (vehicleCert.policy.mailingAddress != null)
                        {
                            if (vehicleCert.policy.mailingAddress.city != null)
                            {
                                PolicyHoldersAddress += (vehicleCert.policy.mailingAddress.ApartmentNumber + " " + vehicleCert.policy.mailingAddress.city.CityName);
                            }

                            if (vehicleCert.policy.mailingAddress.parish != null)
                            {
                                PolicyHoldersAddress += (" " + vehicleCert.policy.mailingAddress.parish.parish);
                            }

                            if (vehicleCert.policy.mailingAddress.country != null)
                            {
                                PolicyHoldersAddress += (" " + vehicleCert.policy.mailingAddress.country.CountryName);
                            }

                        }
                    }
                    catch (Exception)
                    {

                    }

                    
                  
                    try { DriverNames = vehicleCert.Risk.mainDriver.person.fname + " " + vehicleCert.Risk.mainDriver.person.mname + " " + vehicleCert.Risk.mainDriver.person.lname + " and "; }
                    catch (Exception) { }
                    string PolicyHolderNames = "";

                    string CompanyNames = "";
                    try { foreach (var N in vehicleCert.policy.company) { CompanyNames += N.CompanyName + " and "; } }
                    catch (Exception) { }
                    try { CompanyNames = CompanyNames.Substring(0, CompanyNames.Length - 4); }
                    catch (Exception) { }
                    PolicyHolderNames += CompanyNames;

                    string MainInsured = ""; try { var mainInsured = vehicleCert.policy.insured.OrderBy(p => p.PersonID).Take(1).FirstOrDefault(); MainInsured = mainInsured.fname + " " + mainInsured.lname; }
                    catch (Exception) { }
                    try { if (string.IsNullOrEmpty(MainInsured) && !string.IsNullOrEmpty(CompanyNames.Trim())) MainInsured = CompanyNames; }
                    catch (Exception) { }

                    try { foreach (var N in vehicleCert.policy.insured) { if(!PolicyHolderNames.Contains((N.fname + " " + N.mname + " " + N.lname))){PolicyHolderNames += N.fname + " " + N.mname + " " + N.lname + " and ";} } }
                    catch (Exception) { }

                    //try { foreach (var N in vehicleCert.policy.vehicles) { foreach (var d in N.AuthorizedDrivers) { if(!PolicyHolderNames.Contains((d.person.fname + " " + d.person.mname + " " + d.person.lname))){PolicyHolderNames += d.person.fname + " " + d.person.mname + " " + d.person.lname + " and "; }} } }
                    //catch (Exception) { }

                    try { foreach (var N in vehicleCert.policy.vehicles) { foreach (var d in N.AuthorizedDrivers) { if (!AuthDrivers.Contains((d.person.fname + " " + d.person.mname + " " + d.person.lname))) { AuthDrivers += d.person.fname + " " + d.person.mname + " " + d.person.lname + " and "; } } } }
                    catch (Exception) { }

                    try { DriverNames = vehicleCert.Risk.mainDriver.person.fname + " " + vehicleCert.Risk.mainDriver.person.mname + " " + vehicleCert.Risk.mainDriver.person.lname; }catch(Exception){}

                    try { foreach (var N in vehicleCert.Risk.ExceptedDrivers) { if (!ExceptedDrivers.Contains((N.person.fname + " " + N.person.mname + " " + N.person.lname))) { ExceptedDrivers += N.person.fname + " " + N.person.mname + " " + N.person.lname + " and "; } } }
                    catch (Exception) { }
                    try { foreach (var N in vehicleCert.policy.vehicles[0].ExcludedDrivers) { if (!ExcludedDrivers.Contains((N.person.fname + " " + N.person.mname + " " + N.person.lname))) { ExcludedDrivers += N.person.fname + " " + N.person.mname + " " + N.person.lname + " and "; } } }
                    catch (Exception) { }
                
                    try {if(PolicyHolderNames.Substring(PolicyHolderNames.Length - 4) == "and "){ PolicyHolderNames = PolicyHolderNames.Substring(0, PolicyHolderNames.Length - 4);} }catch (Exception) { }
                    
                    try {if(DriverNames.Substring(DriverNames.Length - 4) == "and "){ DriverNames = DriverNames.Substring(0, DriverNames.Length - 4);} }catch(Exception){}
                    try {if(AuthDrivers.Substring(AuthDrivers.Length - 4) == "and "){ AuthDrivers = AuthDrivers.Substring(0, AuthDrivers.Length - 4); }}catch(Exception){}
                    try {if(ExcludedDrivers.Substring(ExcludedDrivers.Length - 4) == "and "){ ExcludedDrivers = ExcludedDrivers.Substring(0, ExcludedDrivers.Length - 4);} }catch(Exception){}
                    try {if(ExceptedDrivers.Substring(ExceptedDrivers.Length - 4) == "and "){ ExceptedDrivers = ExceptedDrivers.Substring(0, ExceptedDrivers.Length - 4); }}catch(Exception){}

                    
                    try { PolicyHolderNames = PolicyHolderNames.Replace("'", ""); }catch(Exception){}
                    try { DriverNames = DriverNames.Replace("'", ""); }catch(Exception){}
                    if(string.IsNullOrEmpty(DriverNames.Trim())) DriverNames = PolicyHolderNames;
                
                            //Get Wording/Cert Template
                            TemplateWording Word = new TemplateWording();
                            try { Word = TemplateWordingModel.GetWording(vehicleCert.wording.ID); }catch(Exception){}

                            try { vehicleCert.wording = Word; }catch(Exception){}
                            
                            //Adds Generated CoverNote record here
                            VehicleCoverNoteGenerated vcng = new VehicleCoverNoteGenerated();
                                                        
                            vcng.Approved = true;
                            try {vcng.authorizedwording = Word.AuthorizedDrivers; }catch(Exception){}
                            try {vcng.authorizedwording = Word.AuthorizedDrivers; }catch(Exception){}
                            try {vcng.bodyType= vehicleCert.Risk.bodyType; }catch(Exception){}
                            try {vcng.cancelled = vehicleCert.Cancelled; }catch(Exception){}
                            try {vcng.CancelledStatus = vehicleCert.Cancelled; }catch(Exception){}
                            try {vcng.certificateCode = Word.CertificateInsuredCode + ":" + Word.ExtensionCode; }catch(Exception){}
                            try {vcng.certificateType = Word.CertificateType; }catch(Exception){}
                            try {vcng.chassisNo = vehicleCert.Risk.chassisno; }catch(Exception){}
                            try { vcng.certitificateNo = vehicleCert.CertificateNo; }catch(Exception){}
                            try {vcng.companyCreatedBy = vehicleCert.policy.compCreatedBy.CompanyName; }catch(Exception){}
                            try {vcng.CompanyName = vehicleCert.policy.insuredby.CompanyName; }catch(Exception){}
                            try {vcng.cover = vehicleCert.policy.policyCover.cover;}catch(Exception){}
                            try {vcng.CovernoteNo = vehicleCert.CertificateNo; }catch(Exception){}
                            try {vcng.drivers= DriverNames; }catch(Exception){}
                            try {vcng.effectivedate = vehicleCert.policy.startDateTime.ToString(); }catch(Exception){}
                            try {vcng.effectiveTime = vehicleCert.policy.startDateTime.ToShortTimeString(); }catch(Exception){}
                            try {vcng.endorsementNo= vehicleCert.EndorsementNo;}catch(Exception){}
                            try {vcng.engineNo = vehicleCert.Risk.engineNo; }catch(Exception){}
                            try {vcng.estimatedValue = vehicleCert.Risk.estimatedValue; }catch(Exception){}
                            try {vcng.expirydate = vehicleCert.policy.endDateTime.ToString(); }catch(Exception){}
                            try {vcng.expiryTime = vehicleCert.policy.endDateTime.ToShortTimeString(); }catch(Exception){}
                            try {vcng.HPCC = vehicleCert.Risk.HPCCUnitType; }catch(Exception){}
                            try {vcng.limitsofuse = vehicleCert.wording.LimitsOfUse; }catch(Exception){}
                            try {if(string.IsNullOrEmpty(vcng.limitsofuse)) vcng.limitsofuse = vehicleCert.generated_cert.limitsofuse; }catch(Exception){}
                            //try {vcng.Period = vehicleCert..ToString(); }catch(Exception){}
                            try {vcng.PolicyHolders = PolicyHolderNames; }catch(Exception){}
                            try {vcng.PolicyHoldersAddress = vehicleCert.policy.mailingAddressShort; }catch(Exception){}
                            try {vcng.PolicyID = vehicleCert.policy.ID; }catch(Exception){}
                            try {vcng.PolicyNo = vehicleCert.policy.policyNumber; }catch(Exception){}
                            try {vcng.PolicyTypes = vehicleCert.policy.insuredType; }catch(Exception){}
                            try {vcng.referenceNo = vehicleCert.Risk.referenceNo; }catch(Exception){}
                            try {vcng.seating = vehicleCert.Risk.seating.ToString(); }catch(Exception){}
                            try {vcng.Usage = vehicleCert.Risk.usage.usage; }catch(Exception){}
                            try { vcng.vehdesc = vehicleCert.policy.vehicles[0].VehicleYear + " " + vehicleCert.Risk.make + " " + vehicleCert.Risk.VehicleModel + " " + vehicleCert.Risk.extension; }
                            catch (Exception) { }
                            try { vcng.WordID = vehicleCert.wording.ID;}catch(Exception){}                         
                            try {vcng.vehExt = vehicleCert.Risk.extension; }catch(Exception){}
                            try {vcng.VehicleMetaDataID = vehicleCert.Risk.ID; }catch(Exception){}
                            try {vcng.VehicleID = vehicleCert.policy.vehicles[0].ID; }catch(Exception){}
                            try {vcng.vehmake = vehicleCert.Risk.make; }catch(Exception){}
                            try {vcng.vehmodel = vehicleCert.Risk.VehicleModel; }catch(Exception){}
                            try {vcng.vehmodeltype = vehicleCert.Risk.modelType; }catch(Exception){}
                            try {vcng.vehregno = vehicleCert.Risk.VehicleRegNumber; }catch(Exception){}
                            try {vcng.vehyear = vehicleCert.Risk.VehicleYear.ToString(); }catch(Exception){}
                            try {vcng.VIN = vehicleCert.Risk.VIN; }catch(Exception){} 
                            try {vcng.ManualCoverNoteNumber = vehicleCert.PrintedPaperNo; }catch(Exception){}
                            try {vcng.PrintCount = vehicleCert.printcount; }catch(Exception){}
                            try {vcng.LastPrintedBy = vehicleCert.last_printed_by_flat; }catch(Exception){}
                            try {vcng.DateLastPrinted = DateTime.Now; }catch(Exception){}
                            try {vcng.DateFirstPrinted = DateTime.Now; }catch(Exception){}
                            try {vcng.CompanyID = vehicleCert.policy.insuredby.CompanyID; }catch(Exception){}
                            try {vcng.IntermediaryID= vehicleCert.policy.compCreatedBy.CompanyID; }catch(Exception){}
                            try { vcng.limitsofuse = TemplateWordingModel.ReplaceBraceTags(vcng.limitsofuse,DriverNames,AuthDrivers,ExcludedDrivers,ExceptedDrivers,PolicyHolderNames, MainInsured).ToUpper(); }catch (Exception) { }
                            try { vcng.authorizedwording = TemplateWordingModel.ReplaceBraceTags(vcng.authorizedwording,DriverNames,AuthDrivers,ExcludedDrivers,ExceptedDrivers,PolicyHolderNames, MainInsured).ToUpper(); }catch (Exception) { }
                            //try { vcng.authorizedwording = TemplateWordingModel.ReplaceBraceTags(vcng.authorizedwording,DriverNames,AuthDrivers,ExcludedDrivers,ExceptedDrivers,PolicyHolderNames).ToUpper(); }catch (Exception) { }
                            try {vcng.PolicyHoldersAddress = PolicyHoldersAddress; }catch(Exception){}

                           
            
            certID =  VehicleCoverNoteGeneratedModel.GenerateVehicleCertGenerated(vcng,certID, uid);
            
            return certID;
        }
        #region old
        //public static List<VehicleCertificate> CreateManyCertificates(List<VehicleCertificate> certs)
        //{
        //    return CreateManyCertificates(certs, Constants.user.ID);
        //}
        #endregion
        /// <summary>
        /// this funcion adds many certificates to the system. it is also ONLY TO BE USED BY THE IMPORT MODEL/WEB SERVICE
        /// </summary>
        /// <param name="certs"></param>
        /// <param name="uid"></param>
        /// <param name="insuredid"></param>
        /// <returns></returns>
        public static List<VehicleCertificate> CreateManyCertificates(List<VehicleCertificate> certs, int uid, int insuredid = 0)
        {
            //foreach (VehicleCertificate cert in certs)b // Get changes
            int id = 0;
            for (int i = 0; i < certs.Count(); i++)
            {
                if (insuredid != 0) certs[i].company = new Company(insuredid);
                id = 1; // CreateCertificate(certs[i], uid, true);
                if (certs[i].Cancelled == true)
                {
                    VehicleCertificateModel.CancelCertificate(id, uid);
                }
                else
                {
                    try
                    {
                        VehicleCertificateModel.Uncancel(id);
                    }
                    catch { }
                }
                if (id != 0)
                {
                    certs[i].VehicleCertificateID = id;
                    //ApproveCert(id, uid);
                    certs[i].policy = PolicyModel.getPolicyFromID(certs[i].policy.ID);

                    
                    //Get Relevant information for to Generate Certificates
                    string NamesOnPolicy = "";
                    string DriverNames = "";
                    string AuthDrivers = "";
                    string ExcludedDrivers = "";
                    string ExceptedDrivers = "";

                    string PolicyHoldersAddress = "";
                    //Gets polcy holders address
                    try
                    {
                        if (certs[i].policy.mailingAddress != null)
                        {
                            if (certs[i].policy.mailingAddress.city != null)
                            {
                                PolicyHoldersAddress += (certs[i].policy.mailingAddress.ApartmentNumber + " " + certs[i].policy.mailingAddress.city.CityName);
                            }

                            if (certs[i].policy.mailingAddress.parish != null)
                            {
                                PolicyHoldersAddress += (" " + certs[i].policy.mailingAddress.parish.parish);
                            }

                            if (certs[i].policy.mailingAddress.country != null)
                            {
                                PolicyHoldersAddress += (" " + certs[i].policy.mailingAddress.country.CountryName);
                            }

                        }
                    }
                    catch (Exception)
                    {

                    }

                    try { DriverNames = certs[i].Risk.mainDriver.person.fname + " " + certs[i].Risk.mainDriver.person.mname + " " + certs[i].Risk.mainDriver.person.lname + " and "; }
                    catch (Exception) { }
                    string PolicyHolderNames = "";

                    string CompanyNames = "";
                    try { foreach (var N in certs[i].policy.company) { CompanyNames += N.CompanyName + " and "; } }
                    catch (Exception) { }
                    try { CompanyNames = CompanyNames.Substring(0, CompanyNames.Length - 4); }
                    catch (Exception) { }
                    PolicyHolderNames += CompanyNames;

                    try { foreach (var N in certs[i].policy.insured) { if (!PolicyHolderNames.Contains((N.fname + " " + N.mname + " " + N.lname))) { PolicyHolderNames += N.fname + " " + N.mname + " " + N.lname + " and "; } } }
                    catch (Exception) { }
                    try { foreach (var N in certs[i].policy.vehicles) { foreach (var d in N.AuthorizedDrivers) { if (!PolicyHolderNames.Contains((d.person.fname + " " + d.person.mname + " " + d.person.lname))) { PolicyHolderNames += d.person.fname + " " + d.person.mname + " " + d.person.lname + " and "; } } } }
                    catch (Exception) { }

                    try { foreach (var N in certs[i].policy.vehicles) { foreach (var d in N.AuthorizedDrivers) { if (!AuthDrivers.Contains((d.person.fname + " " + d.person.mname + " " + d.person.lname))) { AuthDrivers += d.person.fname + " " + d.person.mname + " " + d.person.lname + " and "; } } } }
                    catch (Exception) { }

                    try { DriverNames = certs[i].Risk.mainDriver.person.fname + " " + certs[i].Risk.mainDriver.person.mname + " " + certs[i].Risk.mainDriver.person.lname; }
                    catch (Exception) { }

                    try { foreach (var N in certs[i].Risk.ExceptedDrivers) { if (!ExceptedDrivers.Contains((N.person.fname + " " + N.person.mname + " " + N.person.lname))) { ExceptedDrivers += N.person.fname + " " + N.person.mname + " " + N.person.lname + " and "; } } }
                    catch (Exception) { }
                    try { foreach (var N in certs[i].Risk.ExcludedDrivers) { if (!ExcludedDrivers.Contains((N.person.fname + " " + N.person.mname + " " + N.person.lname))) { ExcludedDrivers += N.person.fname + " " + N.person.mname + " " + N.person.lname + " and "; } } }
                    catch (Exception) { }
                
                    try {if(PolicyHolderNames.Substring(PolicyHolderNames.Length - 4) == "and "){ PolicyHolderNames = PolicyHolderNames.Substring(0, PolicyHolderNames.Length - 4);} }catch (Exception) { }
                    
                    try {if(DriverNames.Substring(DriverNames.Length - 4) == "and "){ DriverNames = DriverNames.Substring(0, DriverNames.Length - 4);} }catch(Exception){}
                    try {if(AuthDrivers.Substring(AuthDrivers.Length - 4) == "and "){ AuthDrivers = AuthDrivers.Substring(0, AuthDrivers.Length - 4); }}catch(Exception){}
                    try {if(ExcludedDrivers.Substring(ExcludedDrivers.Length - 4) == "and "){ ExcludedDrivers = ExcludedDrivers.Substring(0, ExcludedDrivers.Length - 4);} }catch(Exception){}
                    try {if(ExceptedDrivers.Substring(ExceptedDrivers.Length - 4) == "and "){ ExceptedDrivers = ExceptedDrivers.Substring(0, ExceptedDrivers.Length - 4); }}catch(Exception){}
                    try { DriverNames = DriverNames.Substring(0, DriverNames.Length - 4); }catch(Exception){}

                        try { PolicyHolderNames = PolicyHolderNames.Replace("'", ""); }catch(Exception){}
                        try { DriverNames = DriverNames.Replace("'", ""); }catch(Exception){}

                        if (certs[i].Risk.mainDriver != null && NamesOnPolicy == "")
                        {
                            try
                            {
                                certs[i].PolicyHolders = certs[i].Risk.mainDriver.person.fname + " " + certs[i].Risk.mainDriver.person.mname + " " + certs[i].Risk.mainDriver.person.lname; ;
                            }
                            catch (Exception)
                            {
                                certs[i].PolicyHolders = "";
                            }

                        }
                
                            //Get Wording/Cert Template
                            TemplateWording Word = new TemplateWording();
                            try { Word = TemplateWordingModel.GetWording(certs[i].wording.ID); }catch(Exception){}
                            
                            //Adds Generated CoverNote record here
                            VehicleCoverNoteGenerated vcng = new VehicleCoverNoteGenerated();
                            vcng.Approved = true;
                            try {vcng.authorizedwording = certs[i].generated_cert.authorizedwording; }catch(Exception){}
                            try {vcng.authorizedwording = certs[i].generated_cert.authorizedwording; }catch(Exception){}
                            try {vcng.bodyType= certs[i].Risk.bodyType; }catch(Exception){}
                            try {vcng.cancelled = certs[i].Cancelled; }catch(Exception){}
                            try {vcng.CancelledStatus = certs[i].Cancelled; }catch(Exception){}
                            try {vcng.certificateCode = Word.CertificateInsuredCode; }catch(Exception){}
                            try {vcng.certificateType = certs[i].CertificateType; }catch(Exception){}
                            try {vcng.chassisNo = certs[i].Risk.chassisno; }catch(Exception){}
                            vcng.certitificateNo = certs[i].CertificateNo;
                            try {vcng.companyCreatedBy = certs[i].policy.compCreatedBy.CompanyName; }catch(Exception){}
                            try {vcng.CompanyName = certs[i].policy.insuredby.CompanyName; }catch(Exception){}
                            try {vcng.cover = certs[i].policy.policyCover.cover;}catch(Exception){}
                            try {vcng.CovernoteNo = certs[i].CertificateNo; }catch(Exception){}
                            try {vcng.drivers= DriverNames; }catch(Exception){}
                            try {vcng.effectivedate = certs[i].EffectiveDate.ToString(); }catch(Exception){}
                            try {vcng.effectiveTime = certs[i].EffectiveDate.ToShortTimeString(); }catch(Exception){}
                            try {vcng.endorsementNo= certs[i].EndorsementNo;}catch(Exception){}
                            try {vcng.engineNo = certs[i].Risk.engineNo; }catch(Exception){}
                            try {vcng.estimatedValue = certs[i].Risk.estimatedValue; }catch(Exception){}
                            try {vcng.expirydate = certs[i].ExpiryDate.ToString(); }catch(Exception){}
                            try {vcng.expiryTime = certs[i].ExpiryDate.ToShortTimeString(); }catch(Exception){}
                            try {vcng.HPCC = certs[i].Risk.HPCCUnitType; }catch(Exception){}
                            try {vcng.limitsofuse = certs[i].wording.LimitsOfUse; }catch(Exception){}
                            try {if(string.IsNullOrEmpty(vcng.limitsofuse)) vcng.limitsofuse = certs[i].generated_cert.limitsofuse; }catch(Exception){}
                            //try {vcng.Period = certs[i]..ToString(); }catch(Exception){}
                            try {vcng.PolicyHolders = PolicyHolderNames; }catch(Exception){}
                            try {vcng.PolicyHoldersAddress = certs[i].policy.mailingAddressShort; }catch(Exception){}
                            try {vcng.PolicyID = certs[i].policy.ID; }catch(Exception){}
                            try {vcng.PolicyNo = certs[i].policy.policyNumber; }catch(Exception){}
                            try {vcng.PolicyTypes = certs[i].policy.insuredType; }catch(Exception){}
                            try {vcng.referenceNo = certs[i].Risk.referenceNo; }catch(Exception){}
                            try {vcng.seating = certs[i].Risk.seating.ToString(); }catch(Exception){}
                            try {vcng.Usage = certs[i].Risk.usage.usage; }catch(Exception){}
                            try { vcng.vehdesc = certs[i].Risk.VehicleYear.ToString() + " " + certs[i].Risk.make + " " + certs[i].Risk.VehicleModel + " " + certs[i].Risk.extension; }
                            catch (Exception) { }
                            try { vcng.WordID = certs[i].wording.ID;}catch(Exception){}                         
                            try {vcng.vehExt = certs[i].Risk.extension; }catch(Exception){}
                            try {vcng.VehicleMetaDataID = certs[i].Risk.ID; }catch(Exception){}
                            try {vcng.VehicleID = certs[i].Risk.ID; }catch(Exception){}
                            try {vcng.vehmake = certs[i].Risk.make; }catch(Exception){}
                            try {vcng.vehmodel = certs[i].Risk.VehicleModel; }catch(Exception){}
                            try {vcng.vehmodeltype = certs[i].Risk.modelType; }catch(Exception){}
                            try {vcng.vehregno = certs[i].MarksRegNo; }catch(Exception){}
                            try {vcng.vehyear = certs[i].Risk.VehicleYear.ToString(); }catch(Exception){}
                            try {vcng.VIN = certs[i].Risk.VIN; }catch(Exception){} 
                            try {vcng.ManualCoverNoteNumber = certs[i].PrintedPaperNo; }catch(Exception){}
                            try {vcng.PrintCount = certs[i].printcount; }catch(Exception){}
                            try {vcng.LastPrintedBy = certs[i].last_printed_by_flat; }catch(Exception){}
                            try {vcng.DateLastPrinted = DateTime.Now; }catch(Exception){}
                            try {vcng.DateFirstPrinted = DateTime.Now; }catch(Exception){}
                            try {vcng.CompanyID = certs[i].policy.insuredby.CompanyID; }catch(Exception){}
                            try {vcng.IntermediaryID= certs[i].policy.compCreatedBy.CompanyID; }catch(Exception){}
                            try {vcng.PolicyHoldersAddress = PolicyHoldersAddress; }catch(Exception){}
                            certs[i].VehicleCertificateID = VehicleCoverNoteGeneratedModel.GenerateVehicleCertGenerated(vcng, id, uid);

                }
                else certs.RemoveAt(i);

            }
            return certs;
        }


        /// <summary>
        /// This function edits a Risk certificate
        /// </summary>
        /// <param name="vehicleCert"> Risk Certificate object , storing all the certificate data to be edited</param>
        public static int EditCertificate(VehicleCertificate vehicleCert)
        {
            int result = 0;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC UpdateVehicleCertificate " + vehicleCert.VehicleCertificateID + ","
                                                                + vehicleCert.Risk.ID + ","
                                                                + Constants.user.ID + ","
                                                                + vehicleCert.wording.ID;

                if (vehicleCert.PrintedPaperNo != null) query += "," + Constants.CleanString(vehicleCert.PrintedPaperNo);


                SqlDataReader reader = sql.QuerySQL(query, "update", "vehiclecertificate", vehicleCert);

                while (reader.Read())
                {
                    result = int.Parse(reader["result"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to approve a Risk certificate in the system
        /// </summary>
        /// <param name="VehCertId">Risk Certificate Id</param>
        public static void ApproveCert(int VehCertId)
        {
            ApproveCert(VehCertId, Constants.user.ID);
        }

        /// <summary>
        /// This function is used to approve a Risk certificate in the system
        /// </summary>
        /// <param name="VehCertId">Risk Certificate Id</param>
        /// <param name="uid">User Id who's approving the certificate</param>
        private static void ApproveCert(int VehCertId, int uid)
        {

            string query = "EXEC ApproveCertificate " + VehCertId + "," + uid;

            OmaxFramework.Utilities.OmaxData.HandleQuery("EXEC ApproveCertificate " + VehCertId + "," + uid, new sql().SqlConn(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.NoneQuery);


            //sql sql = new sql();
            //if (sql.ConnectSQL())
            //{
            //    string query = "EXEC ApproveCertificate " + VehCertId + "," + uid;

            //    OmaxFramework.Utilities.OmaxData.HandleQuery("EXEC ApproveCertificate " + VehCertId + "," + uid, new sql().SqlConn(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.NoneQuery);



            //    SqlDataReader reader = sql.QuerySQL(query, "update", "vehiclecertificate", new VehicleCertificate(VehCertId), uid, null, false);
            //    reader.Close();
            //    sql.DisconnectSQL();
            //}
        }

        /// <summary>
        /// This function deletes a Risk certificate from the system
        /// </summary>
        /// <param name="CertificateId">Risk Certificate Id</param>
        public static void DeleteCertificate(int CertificateId)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DeleteVehicleCertificate " + CertificateId + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "vehiclecertificate", new VehicleCertificate(CertificateId));
                reader.Close();
                sql.DisconnectSQL();
            }

        }

        /// <summary>
        /// Getting a Risk's active certificate, given the Risk object
        /// </summary>
        /// <param name="Risk">Risk object with Risk's information</param>
        /// <returns>The certificate information</returns>
        public static VehicleCertificate GetThisActiveCertificate(int vehicleId, string start, string end, int polId = 0)
        {
            VehicleCertificate cert = new VehicleCertificate();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetThisActiveCertificate '" + DateTime.Parse(start).ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                                 + DateTime.Parse(end).ToString("yyyy-MM-dd HH:mm:ss") + "',"
                                                                 + vehicleId + ","
                                                                 + polId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    cert = new VehicleCertificate(int.Parse(reader["CertID"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return cert;
        }

        /// <summary>
        /// Getting a Risk's inactive certificate, given the Risk object
        /// </summary>
        /// <param name="Risk">Risk object with Risk's information</param>
        /// <returns>The certificate information</returns>
        public static VehicleCertificate GetThisInactiveCertificate(int vehicleId, int polId)
        {
            VehicleCertificate cert = new VehicleCertificate();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetInactiveCertificate " + polId + "," + vehicleId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    cert = new VehicleCertificate(int.Parse(reader["CertID"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return cert;
        }

        /// <summary>
        /// Retrieving a certificate's data, given the certificate ID
        /// </summary>
        /// <param name="vehicleCert">Risk Certificate object- containing certificate data</param>
        /// <returns>Risk certificate object with certificate information</returns>
        public static VehicleCertificate GetThisCertificate(VehicleCertificate vehicleCert)
        {
            VehicleCertificate cert = new VehicleCertificate();
            Vehicle veh = new Vehicle();
            Person per = new Person();

            string Query = @"exec GetVehicleCert " + vehicleCert.VehicleCertificateID;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                SqlDataReader reader = sql.QuerySQL("EXEC GetThisCertificateGenerated " + vehicleCert.VehicleCertificateID, "read", "vehiclecertificate", new VehicleCertificate());

                


                while (reader.Read())
                {
                    try
                    {
                        int CompanyIDs = 0;
                        int PolicyIDs = 0;
                        int RiskItemNos = 0;

                        int.TryParse(reader["PolicyID"].ToString(), out PolicyIDs);
                        int.TryParse(reader["RiskItemNo"].ToString(), out RiskItemNos);
                        int.TryParse(reader["CompanyID"].ToString(), out CompanyIDs);


                        cert = new VehicleCertificate(int.Parse(reader["ID"].ToString()),
                                                                          new Company(CompanyIDs, reader["CompanyName"].ToString()),
                                                                          new Policy(PolicyIDs, new PolicyCover(reader["Cover"].ToString()), reader["Policyno"].ToString()),
                                                                          new Vehicle(RiskItemNos));
                    }
                    catch (Exception)
                    {

                    }

                   
                     //try {if (DBNull.Value != reader["PeopleID"]) per.PersonID = int.Parse(reader["PeopleID"].ToString()); }catch(Exception){}
                    try {if (DBNull.Value != reader["FName"]) per.fname = reader["FName"].ToString(); }catch(Exception){}
                    //try {if (DBNull.Value != reader["LName"]) per.lname = reader["LName"].ToString(); }catch(Exception){}
                    //try {if (DBNull.Value != reader["TRN"]) per.TRN = reader["TRN"].ToString(); }catch(Exception){}
                    try {cert.printcount = int.Parse(reader["PrintCount"].ToString()); }catch(Exception){}
                    //try {cert.printcountInsurer = int.Parse(reader["PrintCountInsurer"].ToString()); }catch(Exception){}
                    try {cert.Cancelled = (bool)reader["Cancelled"]; }catch(Exception){}
                    try {cert.EffectiveDate = DateTime.Parse(reader["StartDateTime"].ToString()); }catch(Exception){}
                    try {cert.ExpiryDate = DateTime.Parse(reader["EndDateTime"].ToString()); }catch(Exception){}
                    try {cert.policy.imported = (bool)reader["imported"]; }catch(Exception){}
                    try {cert.CertificateNo = reader["CertificateNo"].ToString(); }catch(Exception){}
                    try {cert.PrintedPaperNo = reader["PrintedPaperNo"].ToString(); }catch(Exception){}
                    try {cert.last_printed_by_flat = reader["LastPrintedByFlat"].ToString(); }catch(Exception){}
                    try {cert.first_printed_by_flat = reader["FirstPrintedByFlat"].ToString(); }catch(Exception){}
                    try {cert.Risk.VIN = reader["VIN"].ToString(); }catch(Exception){}
                    try {cert.Risk.chassisno = reader["ChassisNo"].ToString(); }catch(Exception){}
                    try {cert.Risk.make = reader["Make"].ToString(); }catch(Exception){}
                    try {cert.Risk.VehicleModel = reader["Model"].ToString(); }catch(Exception){}
                    try {cert.Risk.VehicleYear = int.Parse(reader["VehicleYear"].ToString()); }catch(Exception){}
                    try {cert.Risk.usage = new Usage(reader["usage"].ToString()); }catch(Exception){}
                    try {cert.Risk.VehicleRegNumber = reader["VehicleRegistrationNo"].ToString(); }catch(Exception){}
                    try {cert.Risk.engineNo = reader["EngineNo"].ToString(); }catch(Exception){}
                    try {cert.Risk.seating = int.Parse(reader["Seating"].ToString()); }catch(Exception){}
                    try {cert.Risk.HPCCUnitType = reader["HPCCUnitType"].ToString(); }catch(Exception){}
                    try {cert.wording = TemplateWordingModel.GetWording(int.Parse(reader["WordingTemplate"].ToString())); }catch(Exception){}
                    //try {cert.Risk.referenceNo = reader["ReferenceNo"].ToString(); }catch(Exception){}
                    try {cert.approved = (bool)reader["approved"]; }catch(Exception){}
                    try {cert.Risk.mainDriver = new Driver(per); }catch(Exception){}
                    try {cert.AuthorizedWording =  reader["authorizeddrivers"].ToString(); }catch(Exception){}
                   try {cert.AuthorizedWording =  reader["authorizeddrivers"].ToString(); }catch(Exception){}
                            try {cert.bodyType=  reader["bodyType"].ToString(); }catch(Exception){}
                            try {cert.CancelledStatus = (bool)reader["Cancelled"]; }catch(Exception){}
                            try {cert.certificateCode =  reader["ExtensionCode"].ToString(); }catch(Exception){}
                            try {cert.CertificateType =  reader["certificateType"].ToString(); }catch(Exception){}
                            try {cert.chassisNo =  reader["VIN"].ToString(); }catch(Exception){}
                            try { cert.CertificateNo = reader["CertificateNo"].ToString(); }catch(Exception){}
                            //try {cert.companyCreatedBy =  reader["LimitsOfUse"].ToString(); }catch(Exception){}
                            try {cert.CompanyName =  reader["CompanyName"].ToString(); }catch(Exception){}
                            try {cert.Cover =  reader["Cover"].ToString();}catch(Exception){}
                            try {cert.CovernoteNo =  reader["CovernoteNo"].ToString(); }catch(Exception){}
                            try {cert.drivers=  reader["VehDrivers"].ToString(); }catch(Exception){}
                            try {cert.EffectiveDate = DateTime.Parse(reader["effectivedate"].ToString()); }catch(Exception){}
                            try {cert.effectiveTime = reader["effectiveTime"].ToString(); }catch(Exception){}
                            try {cert.EndorsementNo =  reader["endorsementNo"].ToString();}catch(Exception){}
                            try {cert.engineNo =  reader["engineNo"].ToString(); }catch(Exception){}
                            //try {cert.estimatedValue =  reader["LimitsOfUse"].ToString(); }catch(Exception){}
                            try {cert.ExpiryDate = DateTime.Parse(reader["EndDateTime"].ToString()); }catch(Exception){}
                            try {cert.expiryTime =  reader["expiryTime"].ToString(); }catch(Exception){}
                            try {cert.HPCC = vehicleCert.Risk.HPCCUnitType; }catch(Exception){}
                            try {cert.limitsofuse =  reader["LimitsOfUse"].ToString(); }catch(Exception){}
                            
                            //try {cert.limitsofuse =  reader["LimitsOfUse"].ToString(); }catch(Exception){}
                            try {cert.PolicyHolders =  reader["FName"].ToString(); }catch(Exception){}
                            try {cert.PolicyHoldersAddress = reader["PolicyHolderAddress"].ToString(); }catch(Exception){}
                            try {cert.PolicyID = int.Parse(reader["PolicyID"].ToString()); }catch(Exception){}
                            try {cert.PolicyNo =  reader["PolicyNo"].ToString(); }catch(Exception){}
                            //try {cert.PolicyTypes = vehicleCert.policy.insuredType; }catch(Exception){}
                            try {cert.referenceNo =""; }catch(Exception){}
                            try {cert.seating =  reader["seating"].ToString(); }catch(Exception){}
                            try {cert.Usages =  reader["Usage"].ToString(); }catch(Exception){}
                            try { cert.vehdesc =  reader["VehicleDesc"].ToString(); }
                            catch (Exception) { }
                            try { cert.WordID = int.Parse(reader["WordID"].ToString());}catch(Exception){}                         
                            try {cert.vehExt =  reader["VehExtension"].ToString(); }catch(Exception){}
                            try {cert.VehicleMetaDataID = int.Parse( reader["VehicleMetaDataID"].ToString()); }catch(Exception){}
                            try {cert.VehicleID =  int.Parse(reader["VehicleID"].ToString()); }catch(Exception){}
                            try {cert.vehmake =  reader["Make"].ToString(); }catch(Exception){}
                            try {cert.vehmodel =  reader["Model"].ToString(); }catch(Exception){}
                            try {cert.vehmodeltype =  reader["VehModelType"].ToString(); }catch(Exception){}
                            try {cert.vehregno =  reader["VehicleRegistrationNo"].ToString(); }catch(Exception){}
                            try {cert.vehyear =  reader["VehicleYear"].ToString(); }catch(Exception){}
                            try {cert.VIN =  reader["chassisNo"].ToString(); }catch(Exception){} 
                            try {cert.ManualCoverNoteNumber =  reader["ManualCoverNoteNumber"].ToString(); }catch(Exception){}
                            try {cert.printcount = int.Parse(reader["PrintCount"].ToString()); }catch(Exception){}
                            //try {cert.LastPrintedBy =  reader["LimitsOfUse"].ToString(); }catch(Exception){}
                            try {cert.DateLastPrinted = DateTime.Now; }catch(Exception){}
                            try {cert.DateFirstPrinted = DateTime.Now; }catch(Exception){}
                            try {cert.CompanyID =  int.Parse(reader["CompanyId"].ToString()); }catch(Exception){}
                            try {cert.IntermediaryID= int.Parse(reader["IntermediaryId"].ToString()); }catch(Exception){}         
                                         
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return cert;
        }

        /// <summary>
        /// This function is used to generate a unique number -to be added to the Risk certificate
        /// </summary>
        /// <returns>integer- unique, generated number</returns>
        private static int GenerateUniqueNum(int compID)
        {
            Random random = new Random();
            int UniqueNum = random.Next(1, 1000000);

            bool result = TestUniqueNo(UniqueNum, compID);

            while (result == true)
            {
                UniqueNum = random.Next(1, 1000000);
                result = TestUniqueNo(UniqueNum, compID);

            }
            return UniqueNum;
        }

        /// <summary>
        /// This function is used to test if a particular printed paper number is already in the  system
        /// </summary>
        /// <param name="ppn">Printed Paper Number to be tested</param>
        /// <returns>boolean value</returns>
        public static bool TestPrintedPaperNo(int ppn)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC TestPrintedPaperNo " + ppn, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to test if a particular 'unique number' is already in the  system (apart of a Risk-certificate)
        /// </summary>
        /// <param name="UniqueNumber">Unique Number to be tested</param>
        /// <returns>boolean value</returns>
        private static bool TestUniqueNo(int UniqueNumber, int compID)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC testUniqueNumber " + compID + "," + UniqueNumber, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to cancel a particular Risk certificate
        /// </summary>
        /// <param name="CertId">Certificate Id- of certificate to be cancelled</param>
        /// <returns>boolean value</returns>
        public static bool CancelCertificate(int CertId = 0, int uid = 0)
        {
            bool result = false;
            int i = 0;

                if (uid != 0) i = uid; else i = Constants.user.ID;
                string query = "EXEC CancelCertificate " + CertId + "," + i;
                //SqlDataReader reader = sql.QuerySQL(query, "update", "vehiclecertificate", new VehicleCertificate(CertId));

                try
                {
                    bool.TryParse(JArray.Parse(JsonConvert.SerializeObject((System.Data.DataTable)OmaxFramework.Utilities.OmaxData.HandleQuery(query, sql.GetCurrentCOnnectionString(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.ScalarQuery)))[0]["result"].ToString(), out result);
              
                }
                catch (Exception)
                {
                 
                }
                try
                {
                    string QueryCancel = "CancelRiskOnCover " + CertId + ",0," + uid;
                    OmaxFramework.Utilities.OmaxData.HandleQuery(QueryCancel, sql.GetCurrentCOnnectionString(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.NoneQuery);
                }
                catch (Exception)
                {
                  
                }


           
            return result;

        }

        /// <summary>
        /// This function is used to test if a certificate is active
        /// </summary>
        /// <param name="CertId">Certificate Id- of certificate to be tested</param>
        /// <returns>boolean value</returns>
        public static bool IsCertitificateActive(int CertId)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC IsCertificateActive " + CertId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;

        }

        /// <summary>
        /// This function is used to test if a certificate is cancelled
        /// </summary>
        /// <param name="CertId">Certificate Id- of certificate to be tested</param>
        /// <returns>boolean value</returns>
        public static bool IsCertitificateCancelled(int CertId)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC IsCertificateCancelled " + CertId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;

        }

        /// <summary>
        /// This function is used to test if a Risk is under an active certificate
        /// </summary>
        /// <param name="vehId">Risk Id- of Risk to checked </param>
        /// <returns>boolean value</returns>
        public static bool IsVehicleUnderActiveCert(int vehId, DateTime startDate, DateTime endDate, int polId = 0)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC IsUnderactiveCertificate '" + startDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + endDate.ToString("yyyy-MM-dd HH:mm:ss") + "'," + vehId + "," + polId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to test if a Risk is under an inactive certificate (under a particular policy)
        /// </summary>
        /// <param name="vehId">Risk Id- of Risk to checked </param>
        /// <returns>boolean value</returns>
        public static bool IsPolicyCertMade(int vehID, int polID)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsInactiveCertificateMade " + polID + "," + vehID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }


        /// <summary>
        /// This function is used to update the print count of a particulat certificate
        /// </summary>
        /// <param name="VehCertID">Risk Certifivate Id</param>
        public static void Print(int VehCertID)
        {
            sql sql = new sql();
            string LastPrintedByName = Constants.user.employee.person.lname + ", " + Constants.user.employee.person.fname;

            if (sql.ConnectSQL())
            {
                try
                {
                    string query = "EXEC PrintCertificate " + VehCertID + "," + Constants.user.ID + ",'" + Constants.CleanString(LastPrintedByName) + "'";
                    SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
                    reader.Close();
                    sql.DisconnectSQL();
                }
                catch (Exception)
                {
                   
                }
                
            }
        }
        #region old
        /// <summary>
        /// this function returns a condensed form of the certificate data
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cmp"></param>
        /// <returns></returns>
        //private static VehicleCertificate GetShortCertificate(int id, int cmp = 0)
        //{
        //    VehicleCertificate vc = new VehicleCertificate(id);
        //    sql sql = new sql();
        //    if (sql.ConnectSQL() && id != 0)
        //    {
        //        string query = null;
        //        if (cmp != 0) query = "EXEC GetShortCertificate " + id + "," + cmp;
        //        else query = "EXEC GetShortCertificate " + id;
        //        SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
        //        while (reader.Read())
        //        {
        //            vc = new VehicleCertificate(id,
        //                                        new Company(reader["CompanyName"].ToString()),
        //                                        new Policy(int.Parse(reader["PolicyID"].ToString())),
        //                                        new Vehicle(reader["VehicleRegistrationNo"].ToString(), int.Parse(reader["VehicleID"].ToString())),
        //                                        DateTime.Parse(reader["EffectiveDate"].ToString()),
        //                                        DateTime.Parse(reader["ExpiryDate"].ToString()),
        //                                        reader["CertificateNo"].ToString());
        //            vc.imported = (bool)reader["imported"];
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return vc;
        //}

        /// <summary>
        /// this function returns a condensed for of the certificates in the system
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="cmp"></param>
        /// <returns></returns>
        //public static List<VehicleCertificate> GetShortCertificate(List<int> ids, int cmp = 0)
        //{
        //    List<VehicleCertificate> VehicleCertificates = new List<VehicleCertificate>();
        //    foreach (int i in ids)
        //    {
        //        VehicleCertificate v = GetShortCertificate(i, cmp);
        //        if (v.company != null) //If the company is null, then no data was pulled for that cert
        //        VehicleCertificates.Add(v);
        //    }

        //    return VehicleCertificates;
        //}
        #endregion
        /// <summary>
        /// this function determines if the certificate exsits in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool IsCertificateReal(int id)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC IsCertificateReal " + id, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to request a print for a particular certificate
        /// </summary>
        /// <param name="VehCertID">Risk Certifivate Id</param>
        public static bool PrintRequest(int VehCertID)
        {
            bool result = false;
            int compId;

            if (Constants.user.newRoleMode) compId = Constants.user.tempUserGroup.company.CompanyID;
            else compId = CompanyModel.GetMyCompany(Constants.user).CompanyID;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXECUTE PrintRequest " + VehCertID + ",'" + "Certificate" + "'," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());

                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to enable the printing for a particular certificate
        /// </summary>
        /// <param name="VehCertID">Risk Certifivate Id</param>
        public static bool EnablePrint(int VehCertID)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXECUTE MakePrintable '" + "Certificate" + "'," + VehCertID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function returns the amount of certs that are effective in a specified
        /// month and year for a specified company
        /// </summary>
        /// <param name="company">company</param>
        /// <param name="month">month</param>
        /// <param name="year">year</param>
        /// <returns></returns>
        public static int CertificateCountEffective(Company company, string month, string year)
        {
            int output = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC CertificateCountEffective " + company.CompanyID + ",'" + Constants.CleanString(month) + "','" + Constants.CleanString(year) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    output = int.Parse(reader["count"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return output;
        }

        /// <summary>
        /// This function is used to determine if the mass approval button should be shown on the policy page
        /// </summary>
        /// <param name="policy">Policy Model </param>
        /// <returns>bool</returns>
        public static bool MassApproveCertButton(Policy policy = null)
        {
            bool show = false;

            if (policy != null)
                foreach (Vehicle vehicle in policy.vehicles)
                {
                    if (!VehicleCertificateModel.IsVehicleUnderActiveCert(vehicle.ID, policy.startDateTime, policy.endDateTime, policy.ID) && VehicleCertificateModel.IsPolicyCertMade(vehicle.ID, policy.ID))
                    {
                        show = true;
                        return show;
                    }
                }
            return show;
        }

        /// <summary>
        /// This function is used to determine if the mass create cert button should be shown on the policy page
        /// </summary>
        /// <param name="policy">Policy Model </param>
        /// <returns>bool</returns>
        public static bool MassCreateCertButton(Policy policy = null)
        {
            bool show = false;

            if (policy != null)
                foreach (Vehicle vehicle in policy.vehicles)
                {
                    if (!VehicleCertificateModel.IsVehicleUnderActiveCert(vehicle.ID, policy.startDateTime, policy.endDateTime, policy.ID) && !VehicleCertificateModel.IsPolicyCertMade(vehicle.ID, policy.ID))
                    {
                        //There only needs to be a min of one vehicle that can potentially have a cert, to show the button
                        show = true;
                        return show;
                    }
                }
            return show;
        }

        /// <summary>
        /// This function is used to determine if the mass print cert button should be shown on the policy page
        /// </summary>
        /// <param name="policy">Policy Model </param>
        /// <returns>bool</returns>
        public static bool MassPrintCertButton(Policy policy = null)
        {
            bool show = false;

            if (policy != null)
                foreach (Vehicle vehicle in policy.vehicles)
                {
                    if (VehicleCertificateModel.IsVehicleUnderActiveCert(vehicle.ID, policy.startDateTime, policy.endDateTime, policy.ID))
                    {
                        //There only needs to be a min of one vehicle that can potentially have a cert, to show the button
                        show = true;
                        return show;
                    }
                }
            return show;
        }

        /// <summary>
        /// Get certificate ID using certificate number
        /// </summary>
        /// <param name="certNo"></param>
        /// <returns></returns>
        public static VehicleCertificate GetVehicleCertificateByCertificateNo(string certNo)
        {
            VehicleCertificate cert = new VehicleCertificate();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetVehicleCertificateByCertificateNo '" + certNo + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    cert = new VehicleCertificate(int.Parse(reader["VehicleCertificateID"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return cert;
        }

        public static List<VehicleCertificate> GetCompanyCertificates(int compID, DateTime start, DateTime end, string typeOfDate = null)
        {
            List<VehicleCertificate> certs = new List<VehicleCertificate>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                if (typeOfDate == null) typeOfDate = "created";
                //string query = "EXEC GetCompanyCertificates " + compID + ",'" + typeOfDate + "'";
                //string query = "EXEC GetCompanyCertificatesNew " + compID + ",'" + typeOfDate + "'";
                string query = "EXEC GetCompanyCertificates_Mod " + compID + ",'" + typeOfDate + "'";
                if ((start == DateTime.Parse("1969-12-31 00:00:00") || (start == DateTime.Parse("1969-12-31 19:00:00")) && (end == DateTime.Parse("1969-12-31 00:00:00") || end == DateTime.Parse("1969-12-31 19:00:00"))))
                {
                    query += ",'" + null + "','" + null + "'";
                }
                else
                    query += ",'" + start.ToString("yyyy-MM-dd HH:mm:ss") + "','" + end.ToString("yyyy-MM-dd HH:mm:ss") + "'";


                Company C = CompanyModel.GetCompany(compID);

                SqlDataReader reader = sql.QuerySQL(query);
                VehicleCertificate vc = new VehicleCertificate();
                while (reader.Read())
                {
                    bool Imported = false;
                    bool.TryParse(reader["imported"].ToString(), out Imported);

                    vc.VehicleCertificateID = int.Parse(reader["VehicleCertificateID"].ToString());
                    vc.Risk = new Vehicle(int.Parse(reader["vehID"].ToString()));
                    vc.policy = new Policy(int.Parse(reader["PolicyID"].ToString()));
                    vc.CertificateNo = reader["CertificateNo"].ToString();
                    vc.EffectiveDate = DateTime.Parse(reader["EffectiveDate"].ToString());
                    vc.Risk.VehicleRegNumber = reader["VehicleRegistrationNo"].ToString();
                    vc.company = C;
                    vc.imported = Imported;

                    certs.Add(vc);
                    vc = new VehicleCertificate();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return certs;
        }

        public static void MarkCertificateImported(VehicleCertificate vc)
        {
            sql sql = new sql();

            string query = "EXEC MarkCertificateImported " + vc.VehicleCertificateID;


            //if (sql.ConnectSQL())
            //{
            //    'string query = "EXEC MarkCertificateImported " + vc.VehicleCertificateID;
            //    SqlDataReader reader = sql.QuerySQL(query);
            //    reader.Close();
            //    sql.DisconnectSQL();
            //}


            OmaxFramework.Utilities.OmaxData.HandleQuery(query, new sql().SqlConn(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.NoneQuery);

        }

        public static void Uncancel(int id)
        {

            OmaxFramework.Utilities.OmaxData.HandleQuery("EXECUTE UnCancelCert " + id.ToString(), new sql().SqlConn(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.NoneQuery);


            //sql sql = new sql();

            //if (sql.ConnectSQL())
            //{
            //    SqlDataReader reader = sql.QuerySQL("EXECUTE UnCancelCert " + id.ToString(), "update", "vehiclecertificate", new VehicleCertificate(id));
            //    while (reader.Read())
            //    {
            //        bool result = (bool)reader["result"];
            //    }
            //    reader.Close();
            //    sql.DisconnectSQL();
            //}
        }

    }
}
