﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;

namespace Honeysuckle.Models
{
    public class ZipCode
    {
        public int ZCID { get; set; }

        [RegularExpression("[ a-zA-Z0-9]+", ErrorMessage = "Incorrect zipcode format- Only letters and numbers allowed")] 
        public string ZipCodeName { get; set; }

        public ZipCode() { }
        public ZipCode(string ZipCodeName)
        {
            this.ZipCodeName = ZipCodeName;
        }

    }

    public class ZipCodeModel 
    { 
    
        /// <summary>
        /// This function returns all zip codes in the system
        /// </summary>
        /// <returns>List of zip codes (strings)</returns>
        public static List<string> GetZipList()
        {
            List<string> result = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXECUTE GetZipList", "read", "zipcode", new ZipCode());
                while (reader.Read())
                {
                    result.Add(reader["Zip"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }
      }
    }
