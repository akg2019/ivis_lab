﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class Employee
    {
        public int EmployeeID { get; set; }
        public Person person { get; set; }
        public Company company { get; set; }
        public bool Active { get; set; }
        public User user { get; set; }
        
        public Employee() { }
        public Employee(int EmployeeID)
        {
            this.EmployeeID = EmployeeID;
        }
        public Employee(int EmployeeID, Company company)
        {
            this.EmployeeID = EmployeeID;
            this.company = company;
        }
        public Employee(int EmployeeID, Person person, Company company)
        {
            this.EmployeeID = EmployeeID;
            this.person = person;
            this.company = company;
        }
        public Employee(int EmployeeID, Person person, Company company, bool Active)
        {
            this.EmployeeID = EmployeeID;
            this.person = person;
            this.company = company;
            this.Active = Active;
        }

        public Employee(Person person)
        {
            this.person = person;
        }
    }
    public class EmployeeModel
    {
        /// <summary>
        /// This function updates an employee's information
        /// </summary>
        /// <param name="employee">Employee object- storing information about employee to be modified</param>
        public static void UpdateEmployee(Employee employee)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC UpdateEmployee " + employee.EmployeeID + ","
                                                      + employee.person.PersonID + ","
                                                      + employee.company.CompanyID + ","
                                                      + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "employee", employee);
                reader.Close();
                sql.DisconnectSQL();
            }
        }
        
        /// <summary>
        /// This function adds a new employee (of a company) to the system
        /// </summary>
        /// <param name="employee">Employee object- storing information about employee</param>
        public static void AddEmployee(Employee employee)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE AddEmployee " + employee.person.PersonID + "," + employee.company.CompanyID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "create", "employee", employee);
                while (reader.Read())
                {
                    employee.EmployeeID = int.Parse(reader["ID"].ToString());  //return value of the EmployeeId sent by stored Procedure
                }
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function retrieves an employee's information, given an employee object- fills the object with the information
        /// </summary>
        /// <param name="employee">Employee object- storing information about employee</param>
        public static void GetEmployee(Employee employee) //TO GET DATA, FILL INFO, THEN RETURN IT
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetEmployeeWithEmployeeID " + employee.EmployeeID, "read", "employee", new Employee());
                while (reader.Read())
                {
                    Address address = new Address(int.Parse(reader["AID"].ToString()),
                                                  reader["RoadNumber"].ToString(),
                                                  reader["AptNumber"].ToString(),
                                                  new Road(reader["RoadName"].ToString()),
                                                  new RoadType(reader["RoadType"].ToString()),
                                                  new ZipCode(reader["ZipCode"].ToString()),
                                                  new City(reader["City"].ToString()),
                                                  new Parish(reader["Parish"].ToString()),
                                                  new Country(reader["CountryName"].ToString()));
                    Person person = new Person(int.Parse(reader["PeopleID"].ToString()),
                                               reader["FName"].ToString(),
                                               reader["MName"].ToString(),
                                               reader["LName"].ToString(),
                                               address);
                    person.TRN = reader["TRN"].ToString();
                    Company company = new Company(int.Parse(reader["CompanyID"].ToString()), reader["CompanyName"].ToString());
                    employee.Active = (bool)reader["Active"];
                    employee.person = person;
                    employee.company = company;
                    employee.person.address = address;
                }
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// Gets a list of employees in a specific company
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public static List<Employee> GetEmployeesOfCompany(Company company)
        {
            List<Employee> employees = new List<Employee>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetEmployeesOfCompany " + company.CompanyID, "read", "employee", new Employee());
                while (reader.Read())
                {
                    Employee emp = new Employee(int.Parse(reader["EmployeeID"].ToString()), new Person(0, reader["fname"].ToString(), reader["mname"].ToString(), reader["lname"].ToString(), null), new Company(reader["CompanyName"].ToString()), (bool)reader["Active"]);
                    if (DBNull.Value != reader["uid"]) emp.user = new User(int.Parse(reader["uid"].ToString()));
                    else emp.user = new User();
                    employees.Add(emp);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return employees;
        }

        /// <summary>
        /// This function deactivates an employee in the system
        /// </summary>
        /// <param name="employee">Employee object- storing information about employee to be deactivated</param>
        /// <returns>boolean</returns>
        public static bool DeactivateEmployee(Employee employee)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC DeactivateEmployee " + employee.EmployeeID, "update", "employee", new Employee());
                while(reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function activates an employee
        /// </summary>
        /// <param name="employee">Employee object- storing information about employee to be activated</param>
        /// <returns>boolean</returns>
        public static bool ActivateEmployee(Employee employee)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC ActivateEmployee " + employee.EmployeeID, "update", "employee", employee);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function checks if an employee exists
        /// </summary>
        /// <param name="employee">Employee object- storing information about employee to be checked</param>
        /// <returns>boolean</returns>
        public static bool TestEmployee(Employee employee)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC TestEmployee " + employee.EmployeeID, "read", "employee", new Employee());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function retrieves the employees belonging to the company of the current user
        /// </summary>
        /// <param name="user">User currently using the system</param>
        public static void GetMyEmployee(User user)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetMyEmployee " + user.ID, "read", "employee", new Employee());
                while (reader.Read())
                {
                    if ((bool)reader["result"]) user.employee = new Employee(int.Parse(reader["EmployeeID"].ToString()), new Company(int.Parse(reader["CompanyID"].ToString()), reader["CompanyName"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            
        }

    }
}