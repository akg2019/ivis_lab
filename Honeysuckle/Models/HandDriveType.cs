﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;

namespace Honeysuckle.Models
{
    public class HandDriveType
    {
        public int ID { get; set; }
        [RegularExpression("([a-zA-Z]+[a-zA-Z-]*[a-zA-Z]+)", ErrorMessage = "Incorrect handrive format (Letters and hyphens only)")]
        public string handDriveType { get; set; }

        public HandDriveType() { }
        public HandDriveType(string handDriveType)
        {
            this.handDriveType = handDriveType;
        }
        public HandDriveType(int ID, string handDriveType)
        {
            this.ID = ID;
            this.handDriveType = handDriveType;
        }

    }

    public class HandDriveTypeModel
    {
        /// <summary>
        /// This function retrieves the list of all hand drive types in the system
        /// </summary>
        /// <returns>List of hand drives</returns>
        public List<string> GetHandDriveType()
        {
            List<string> result = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXECUTE GetHandDriveTypes", "read", "handdrivetype", new HandDriveType());
                while (reader.Read())
                {
                    result.Add(reader["HDType"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

    }

}
