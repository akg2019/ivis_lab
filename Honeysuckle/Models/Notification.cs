﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;

namespace Honeysuckle.Models
{
    public class Notification
    {
        public int ID { get; set; }
        public List<User> NoteTo { get; set; }
        public User NoteFrom { get; set; }
        public string heading { get; set; }
        [DataType(DataType.MultilineText)]
        public string content { get; set; }
        public bool seen { get; set; }
        public bool read { get; set; }
        public bool deleted { get; set; }
        public bool new_ { get; set; }
        public DateTime timestamp { get; set; }

        public Notification parent { get; set; }

        public Notification() { }
        public Notification(int ID)
        {
            this.ID = ID;
        }
        public Notification(int ID, List<User> NoteTo, User NoteFrom, string heading, string content)
        {
            this.ID = ID;
            this.NoteTo = NoteTo;
            this.NoteFrom = NoteFrom;
            this.heading = heading;
            this.content = content;
        }
        public Notification(int ID, User NoteFrom, string heading, string content)
        {
            this.ID = ID;
            this.NoteFrom = NoteFrom;
            this.heading = heading;
            this.content = content;
        }
        public Notification(int ID, User NoteFrom, string heading, string content, Notification parent)
        {
            this.ID = ID;
            this.NoteFrom = NoteFrom;
            this.heading = heading;
            this.content = content;
            this.parent = parent;
        }
        public Notification(int ID, DateTime timestamp, List<User> NoteTo, User NoteFrom, string heading, string content, Notification parent)
        {
            this.ID = ID;
            this.timestamp = timestamp;
            this.NoteTo = NoteTo;
            this.NoteFrom = NoteFrom;
            this.heading = heading;
            this.content = content;
            this.parent = parent;
        }
        public Notification(int ID, List<User> NoteTo, User NoteFrom, string heading, string content, bool seen, bool read, bool deleted)
        {
            this.ID = ID;
            this.NoteTo = NoteTo;
            this.NoteFrom = NoteFrom;
            this.heading = heading;
            this.content = content;
            this.seen = seen;
            this.read = read;
            this.deleted = deleted;
        }
        public Notification(int ID, DateTime timestamp, List<User> NoteTo, User NoteFrom, string heading, string content, bool seen, bool read, bool deleted)
        {
            this.ID = ID;
            this.timestamp = timestamp;
            this.NoteTo = NoteTo;
            this.NoteFrom = NoteFrom;
            this.heading = heading;
            this.content = content;
            this.seen = seen;
            this.read = read;
            this.deleted = deleted;
        }
        public Notification(int ID, User NoteFrom, string heading, string content, bool seen, bool read, bool deleted)
        {
            this.ID = ID;
            this.NoteFrom = NoteFrom;
            this.heading = heading;
            this.content = content;
            this.seen = seen;
            this.read = read;
            this.deleted = deleted;
        }
        public Notification(int ID, List<User> NoteTo, User NoteFrom, string heading, string content, bool seen, bool read, bool deleted, bool new_)
        {
            this.ID = ID;
            this.NoteTo = NoteTo;
            this.NoteFrom = NoteFrom;
            this.heading = heading;
            this.content = content;
            this.seen = seen;
            this.read = read;
            this.deleted = deleted;
            this.new_ = new_;
        }
        public Notification(int ID, List<User> NoteTo, User NoteFrom, string heading, string content, bool seen, bool read, bool deleted, bool new_, Notification parent)
        {
            this.ID = ID;
            this.NoteTo = NoteTo;
            this.NoteFrom = NoteFrom;
            this.heading = heading;
            this.content = content;
            this.seen = seen;
            this.read = read;
            this.deleted = deleted;
            this.new_ = new_;
            this.parent = parent;
        }
        public Notification(int ID, User NoteFrom, string heading, string content, bool seen, bool read, bool deleted, bool new_, Notification parent)
        {
            this.ID = ID;
            this.NoteFrom = NoteFrom;
            this.heading = heading;
            this.content = content;
            this.seen = seen;
            this.read = read;
            this.deleted = deleted;
            this.new_ = new_;
            this.parent = parent;
        }
    }
    public class NotificationModel
    {

        /// <summary>
        /// this function returns the latest notification
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pname"></param>
        /// <returns></returns>
        public static Notification GetTheNewLastNotification(int page = 0, string pname = null)
        {
            if (page != 0 && pname != null)
            {
                List<User> to = new List<User>();
                Notification notification = new Notification();
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    string query = "EXEC GetTheNewLastNotification " + Constants.user.ID + "," + page + ",'" + Constants.CleanString(pname) + "'";
                    SqlDataReader reader = sql.QuerySQL(query, "read", "notification", new Notification());
                    while (reader.Read())
                    {
                        string[] names = reader["NoteTo"].ToString().Split(';');
                        foreach (string name in names)
                        {
                            to.Add(UserModel.GetUser(name));
                        }
                        notification = new Notification(int.Parse(reader["NID"].ToString()),
                                                          DateTime.Parse(reader["timestamp"].ToString()),
                                                          to,
                                                          UserModel.GetUser(int.Parse(reader["NoteFrom"].ToString())),
                                                          reader["Heading"].ToString(),
                                                          reader["Content"].ToString(),
                                                          (bool)reader["seen"],
                                                          (bool)reader["read"],
                                                          (bool)reader["deleted"]);


                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
                return notification;
            }
            else return new Notification();
        }

        /// <summary>
        /// this function tests whether this notification should be viewable by you
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool IsMyNotification(int id)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL() && UserModel.TestGUID(Constants.user))
            {
                string query = "EXEC IsMyNotification " + id + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "notification", new Notification());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// Using the logged in user we return a list of all his notifications
        /// </summary>
        /// <returns>A list of notifications</returns>
        public static List<Notification> GetMyNotifications()
        {
            List<Notification> notifications = new List<Notification>();
            List<User> to = new List<User>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetMyNotifications " + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "notification", new Notification());
                while (reader.Read())
                {
                    string[] names = reader["NoteTo"].ToString().Split(';');
                    foreach (string name in names)
                    {
                        to.Add(UserModel.GetUser(name));
                    }
                    notifications.Add(new Notification(int.Parse(reader["NID"].ToString()),
                                                       DateTime.Parse(reader["timestamp"].ToString()),
                                                       to,
                                                       UserModel.GetUser(int.Parse(reader["NoteFrom"].ToString())),
                                                       reader["Heading"].ToString(),
                                                       reader["Content"].ToString(),
                                                       (bool)reader["seen"],
                                                       (bool)reader["read"],
                                                       (bool)reader["deleted"]));
                    to = new List<User>();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return notifications;
        }

        /// <summary>
        /// This function returns a notification based on a valid id request to the database
        /// </summary>
        /// <param name="id">a valid notification id in the database</param>
        /// <returns>a notification with all relevant data</returns>
        public static Notification GetThisNotification(int id)
        {
            Notification notification = new Notification();
            List<User> to = new List<User>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetThisNotification " + id;
                SqlDataReader reader = sql.QuerySQL(query, "read", "notification", new Notification());
                while (reader.Read())
                {
                    string[] names = reader["NoteTo"].ToString().Split(';');
                    foreach (string name in names)
                    {
                        to.Add(UserModel.GetUser(name));
                    }
                    notification = new Notification(int.Parse(reader["NID"].ToString()),
                                                      DateTime.Parse(reader["timestamp"].ToString()),
                                                      to,
                                                      new User(int.Parse(reader["NoteFrom"].ToString())),
                                                      reader["Heading"].ToString(),
                                                      reader["Content"].ToString(),
                                                      (bool)reader["seen"],
                                                      (bool)reader["read"],
                                                      (bool)reader["deleted"]);
                }
                reader.Close();
                sql.DisconnectSQL();
                notification.NoteFrom = UserModel.GetUser(notification.NoteFrom.ID);
            }
            return notification;
        }

        /// <summary>
        /// this function returns a list of all notifications based on the user who's logged in and is marked unseen in the database
        /// </summary>
        /// <returns>A list of notifications</returns>
        public static List<Notification> GetUnseenNotifications()
        {
            List<Notification> notifications = new List<Notification>();
            List<User> to = new List<User>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetUnseenNotifications " + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "notification", new Notification());
                while (reader.Read())
                {
                    string[] names = reader["NoteTo"].ToString().Split(';');
                    foreach (string name in names)
                    {
                        to.Add(UserModel.GetUser(name));
                    }
                    notifications.Add(new Notification(int.Parse(reader["NID"].ToString()),
                                                       DateTime.Parse(reader["timestamp"].ToString()),
                                                       to,
                                                       UserModel.GetUser(int.Parse(reader["NoteFrom"].ToString())),
                                                       reader["Heading"].ToString(),
                                                       reader["Content"].ToString(),
                                                       (bool)reader["seen"],
                                                       (bool)reader["read"],
                                                       (bool)reader["deleted"]));
                    to = new List<User>();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return notifications;
        }

        /// <summary>
        /// this function returns a list of all notifications based on the user who's logged in and is marked unread in the database
        /// </summary>
        /// <returns>A list of notifications</returns>
        public static List<Notification> GetUnreadNotifications(int page = 0)
        {
            List<Notification> notifications = new List<Notification>();
            List<User> to = new List<User>();
            Notification parent = new Notification();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                //string query = "EXEC GetUnreadNotifications " + Constants.user.ID;
                string query = "EXEC Epic_IVIS_GetUnreadNotifications " + Constants.user.ID;
                if (page != 0) query += "," + page;
                SqlDataReader reader = sql.QuerySQL(query, "read", "notifation", new Notification());

                if(reader != null)
                {
                    while (reader.Read())
                    {
                        string[] names = reader["NoteTo"].ToString().Split(';');
                        foreach (string name in names)
                        {
                            if (name != "") to.Add(UserModel.GetUser(name));
                        }


                        if (DBNull.Value != reader["ParentMessage"]) parent.ID = int.Parse(reader["ParentMessage"].ToString());
                        if (DBNull.Value != reader["NoteFrom"])
                        {
                            notifications.Add(new Notification(int.Parse(reader["NID"].ToString()),
                                                               DateTime.Parse(reader["timestamp"].ToString()),
                                                               to,
                                                               UserModel.GetUser(int.Parse(reader["NoteFrom"].ToString())),
                                                               reader["Heading"].ToString(),
                                                               reader["Content"].ToString(),
                                                               parent));
                            parent = new Notification();
                        }
                    }
                }

                if(reader != null)
                {
                    reader.Close();
                }
                
                sql.DisconnectSQL();
            }
            return notifications;
        }

        /// <summary>
        /// this function returns the amount of unread notifications there are for a certain user
        /// </summary>
        /// <returns></returns>
        public static int GetUnreadNotificationsCount()
        {
            int count = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                //string query = "EXEC GetUnreadNotificationsCount " + Constants.user.ID;
                string query = "EXEC Epic_IVIS_GetUnreadNotificationsCount " + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "notification", new Notification());
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        count = int.Parse(reader["count"].ToString());
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            return count;
        }

        /// <summary>
        /// this function returns a list of all notifcations that are not new to you
        /// </summary>
        /// <returns>list of notifications</returns>
        public static List<Notification> GetUnnewNotifications()
        {
            List<Notification> notifications = new List<Notification>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetUnnewNotifications " + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "notification", new Notification());
                while (reader.Read())
                {
                    notifications.Add(new Notification(int.Parse(reader["NID"].ToString()),
                                                       new User(int.Parse(reader["NoteFrom"].ToString())),
                                                       reader["Heading"].ToString(),
                                                       reader["Content"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
                foreach (Notification note in notifications)
                {
                    note.NoteFrom = UserModel.GetUser(note.NoteFrom.ID);
                }
            }
            return notifications;
        }

        /// <summary>
        /// this function marks a notification as seen
        /// </summary>
        /// <param name="notification">a valid notification in the system</param>
        public static void MarkNotifcationSeen(Notification notification)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC MarkNotifcationSeen " + notification.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "notification", notification);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function marks a notification as read
        /// </summary>
        /// <param name="notification">a valid notification in the system</param>
        public static void MarkNotifcationRead(Notification notification)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC MarkNotifcationRead " + notification.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "notification", notification);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function marks a notification as deleted
        /// </summary>
        /// <param name="notification">a valid notificiation in the system</param>
        public static void MarkNotifcationDeleted(Notification notification)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC MarkNotifcationDeleted " + notification.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "notification", notification);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function creates new notifications, for replies as well as new messages
        /// </summary>
        /// <param name="notification">a valid notification object</param>
        public static void CreateNotification(Notification notification)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = null;
                string query = "";
                notification.heading = Constants.CleanString(notification.heading);
                notification.content = Constants.CleanString(notification.content);

                if (notification.parent != null) query = "EXEC CreateNotification " + Constants.user.ID + "," + notification.NoteFrom.ID + ",'" + Constants.CleanString(notification.heading) + "','" + Constants.CleanString(notification.content) + "'," + notification.parent.ID;
                else query = "EXEC CreateNotification " + Constants.user.ID + "," + notification.NoteFrom.ID + ",'" + Constants.CleanString(notification.heading) + "','" + Constants.CleanString(notification.content) + "'";

                reader = sql.QuerySQL(query, "create", "notification", notification);

                while (reader.Read())
                {
                    notification.ID = int.Parse(reader["NID"].ToString());
                }
                reader.Close();
                foreach (User user in notification.NoteTo)
                {
                    query = "EXEC SendNotificationTo " + notification.ID + "," + user.ID + "," + Constants.user.ID;
                    reader = sql.QuerySQL(query, "read", "notification", new Notification());
                    reader.Close();
                }
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function returns all notifications that are children (i.e. replies) to a List of Notifications
        /// </summary>
        /// <param name="notifications">list of notifications</param>
        /// <returns>list of notifications</returns>
        public static List<Notification> GetChildNotifications(List<Notification> notifications)
        {
            List<Notification> children = new List<Notification>();
            List<User> to = new List<User>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                foreach (Notification note in notifications)
                {
                    string query = "EXEC GetNotificationChildren " + note.ID;
                    SqlDataReader reader = sql.QuerySQL(query, "read", "notification", new Notification());
                    while (reader.Read())
                    {
                        string[] names = reader["NoteTo"].ToString().Split(';');
                        foreach (string name in names)
                        {
                            if (name != "") to.Add(UserModel.GetUser(name));
                        }
                        children.Add(new Notification(int.Parse(reader["NID"].ToString()), to, UserModel.GetUser(int.Parse(reader["NoteFrom"].ToString())), reader["Heading"].ToString(), reader["Content"].ToString()));
                        to = new List<User>();
                    }
                    reader.Close();
                }
                sql.DisconnectSQL();
            }
            return children;
        }

        /// <summary>
        /// this function returns the parent (i.e. notification that spawned this reply) notification of a specific notification
        /// </summary>
        /// <param name="notification">a notification object</param>
        /// <returns>a notification object</returns>
        public static Notification GetParentNotification(Notification notification)
        {
            Notification parent = new Notification();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetNotificationParent " + notification.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "notification", new Notification());
                while (reader.Read())
                {
                    parent = new Notification(int.Parse(reader["NID"].ToString()), new User(int.Parse(reader["NoteFrom"].ToString())), reader["Heading"].ToString(), reader["Content"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return parent;
        }

        /// <summary>
        /// this function returns all notifications that have been 'deleted'
        /// </summary>
        /// <returns>list of notifications</returns>
        public static List<Notification> GetMyTrash(int page = 0)
        {
            List<Notification> trash = new List<Notification>();
            List<User> to = new List<User>();
            Notification parent = new Notification();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetMyTrash " + Constants.user.ID;
                if (page != 0) query += "," + page;
                SqlDataReader reader = sql.QuerySQL(query, "read", "notification", new Notification());
                while (reader.Read())
                {
                    string[] names = reader["NoteTo"].ToString().Split(';');
                    foreach (string name in names)
                    {
                        if (name != "") to.Add(UserModel.GetUser(name));
                    }
                    if (DBNull.Value != reader["ParentMessage"]) parent.ID = int.Parse(reader["ParentMessage"].ToString());
                    else
                    {
                        Notification note = new Notification(int.Parse(reader["NID"].ToString()),
                                                            DateTime.Parse(reader["timestamp"].ToString()),
                                                            to,
                                                            UserModel.GetUser(int.Parse(reader["NoteFrom"].ToString())),
                                                            reader["Heading"].ToString(),
                                                            reader["Content"].ToString(),
                                                            parent);
                        note.read = (bool)reader["Read"];
                        trash.Add(note);
                    }
                    parent = new Notification();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return trash;
        }

        /// <summary>
        /// this function returns the amount of messages in a user's trash
        /// </summary>
        /// <returns></returns>
        public static int GetMyTrashCount()
        {
            int count = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetMyTrashCount " + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "notification", new Notification());
                while (reader.Read())
                {
                    count = int.Parse(reader["count"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return count;
        }

        /// <summary>
        /// this function returns all top notifications of threads that are not deleted
        /// </summary>
        /// <returns>list of notifications</returns>
        public static List<Notification> GetAllMyNotes(int page = 0)
        {
            List<Notification> notifications = new List<Notification>();
            List<User> to = new List<User>();
            Notification parent = new Notification();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                //string query = "EXEC GetMyNotes " + Constants.user.ID;
                string query = "EXEC Epic_IVIS_GetAllMyNotes " + Constants.user.ID;
                if (page != 0) query += "," + page;
                SqlDataReader reader = sql.QuerySQL(query, "read", "notification", new Notification());
                while (reader.Read())
                {
                    string[] names = reader["NoteTo"].ToString().Split(';');
                    foreach (string name in names)
                    {
                        if (name != "") to.Add(UserModel.GetUser(name));
                    }
                    if (DBNull.Value != reader["ParentMessage"]) parent.ID = int.Parse(reader["ParentMessage"].ToString());
                    else
                    {
                        Notification note = new Notification(int.Parse(reader["NID"].ToString()),
                                                            DateTime.Parse(reader["timestamp"].ToString()),
                                                            to,
                                                            UserModel.GetUser(int.Parse(reader["NoteFrom"].ToString())),
                                                            reader["Heading"].ToString(),
                                                            reader["Content"].ToString(),
                                                            parent);
                        note.read = (bool)reader["Read"];
                        notifications.Add(note);
                    }
                    parent = new Notification();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return notifications;
        }

        /// <summary>
        /// this function is uses to return the count of all the messages assigned to me
        /// </summary>
        /// <returns></returns>
        public static int GetAllMyNotesCount()
        {
            int count = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                //string query = "EXEC GetAllMyNotesCount " + Constants.user.ID;
                string query = "EXEC Epic_IVIS_GetAllMyNotesCount " + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "notification", new Notification());

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        count = int.Parse(reader["count"].ToString());
                    }
                }
                else { count = 0; }

                reader.Close();
                sql.DisconnectSQL();
            }
            return count;
        }

    }
}