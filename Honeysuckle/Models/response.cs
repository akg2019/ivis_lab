﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Honeysuckle.Models
{
    public class response
    {
        /// <summary>
        /// this is either 'success' or 'error'
        /// </summary>
        public string type { get; set; }
        public string message { get; set; }

        /// <summary>
        /// this is the creation of a response object
        /// </summary>
        /// <param name="type">either 'success' or 'error'</param>
        /// <param name="message">message to be posted</param>
        public response(string type, string message)
        {
            this.type = type;
            this.message = message;
        }

        /// <summary>
        /// this function is used on view level (DONT REMOVE)
        /// </summary>
        /// <param name="responses"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<response> gettype(List<response> responses, string type)
        {
            List<response> output = new List<response>();
            for (int i = 0; i < responses.Count(); i++)
            {
                if (responses.ElementAt(i).type == type) output.Add(responses.ElementAt(i));
            }
            return output;
        }
    }
}