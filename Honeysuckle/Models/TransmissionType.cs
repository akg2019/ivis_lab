﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;

namespace Honeysuckle.Models
{
    public class TransmissionType 
    {
        public int ID { get; set; }
        [RegularExpression("([a-zA-Z]+[a-zA-Z-]*[a-zA-Z]+)", ErrorMessage = "Incorrect handrive format (Letters and hyphens only)")]
        public string transmissionType { get; set; }

        public TransmissionType () { }
        public TransmissionType(string transmissionsType)
        {
            this.transmissionType = transmissionType;
        }
        public TransmissionType(int ID, string transmissionType)
        {
            this.ID = ID;
            this.transmissionType = transmissionType;
            
        }

    }

    public class TransmissionTypeModel
    {
        /// <summary>
        /// This function retrieves the list of all transmission types in the system
        /// </summary>
        /// <returns>List of transmission types</returns>
        public List<string> GetTransmissionTypes()
        {
            List<string> result = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXECUTE GetTransmissionTypes", "read", "TransmissionType", new TransmissionType()); 
                while (reader.Read())
                {
                    result.Add(reader["TransmissionType"].ToString());
                }
                reader.Close();

                sql.DisconnectSQL();
            }
            return result;
        }
    }
}
