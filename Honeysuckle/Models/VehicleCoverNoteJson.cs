﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Honeysuckle.Models
{
    [DataContract]
    public class VehicleCoverNoteJson
    {
        public string edi_system_id { get; set; }
        public string alterations { get; set; }
        public bool cancelled { get; set; }
        public string cancelled_by_trn { get; set; }
        public string cancelled_reason { get; set; }
        public string cancelled_on_at { get; set; }
        public string date_first_printed { get; set; }
        public string last_printed_on_at { get; set; }
        public string last_printed_by_trn { get; set; }
        public string cover_note_id { get; set; }
        public string effective_date_time { get; set; }
        public string expiry_date { get; set; }
        public int days_effective { get; set; }
        public List<string> authorized_drivers { get; set; }
        public string endorsement_number { get; set; }
        public string limitations_of_use { get; set; }
        public string manual_cover_note_no { get; set; }
        public string mortgagees { get; set; }
        public int printed_count { get; set; }
        public string printed_paper_number { get; set; }
        public string risk_item_number { get; set; }
        public string risk_chassis_number { get; set; }

        public VehicleCoverNoteJson() { }

        public VehicleCoverNoteJson(VehicleCoverNote cover) 
        {
            this.edi_system_id = cover.EDIID;
            this.alterations = cover.alterations;
            this.cancelled = cover.cancelled;
            if (cover.cancelledby != null) this.cancelled_by_trn = cover.cancelledby.TRN;
            this.cancelled_reason = cover.cancelledreason;
            this.cancelled_on_at = cover.cancelledon.ToString("yyyy-MM-dd HH:mm:ss");
            this.date_first_printed = cover.firstprinted.ToString("yyyy-MM-dd HH:mm:ss");
            this.last_printed_on_at = cover.lastprinted.ToString("yyyy-MM-dd HH:mm:ss");
            if (cover.lastprintedby != null) this.last_printed_by_trn = cover.lastprintedby.TRN;
            this.cover_note_id = cover.covernoteid;
            this.effective_date_time = cover.effectivedate.ToString("yyyy-MM-dd HH:mm:ss");
            this.expiry_date = cover.expirydate.ToString("yyyy-MM-dd HH:mm:ss");
            this.days_effective = cover.period;
            this.authorized_drivers = new List<string>();
            foreach (Driver driver in cover.risk.AuthorizedDrivers)
            {
                this.authorized_drivers.Add(driver.person.TRN);
            }
            this.endorsement_number = cover.endorsementno;
            if (cover.gen != null)
            {
                this.limitations_of_use = cover.gen.limitsofuse;
                this.mortgagees = cover.gen.mortgagees;
            }
            this.manual_cover_note_no = cover.covernoteno;
            this.printed_count = cover.printcount;
            if (cover.risk != null)
            {
                this.risk_chassis_number = cover.risk.chassisno;
                this.risk_item_number = cover.risk.risk_item_no.ToString();
            }
            
        }
    }
    public class VehicleCoverNoteJsonModel
    {
        /// <summary>
        /// this function coverts the VehicleCoverNote object to a VehicleCoverNoteJson
        /// </summary>
        /// <param name="covers"></param>
        /// <returns></returns>
        public static List<VehicleCoverNoteJson> ParseFromCovers(List<VehicleCoverNote> covers)
        {
            List<VehicleCoverNoteJson> cover_jsons = new List<VehicleCoverNoteJson>();
            foreach (VehicleCoverNote cover in covers)
            {
                cover_jsons.Add(new VehicleCoverNoteJson(cover));
            }
            return cover_jsons;
        }
    }
}