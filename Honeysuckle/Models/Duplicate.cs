﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class Duplicate
    {
        public string reference_no { get; set; }
        public string type { get; set; }
        public int duplicate_count { get; set; }
        public object ref_obj { get; set; }
        //public int ref_id { get; set; }

        public Duplicate() { }
        public Duplicate(string reference_no, string type, int duplicate_count)
        {
            this.reference_no = reference_no;
            this.type = type;
            this.duplicate_count = duplicate_count;
        }
        //public Duplicate(int ref_id)
        //{
        //    this.ref_id = ref_id;
        //}
    }
    public class DuplicateModel
    {
        public static List<Duplicate> get_my_duplicates(Company company)
        {
            List<Duplicate> duplicates = new List<Duplicate>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC get_duplicate_records " + company.CompanyID);
                while (reader.Read())
                {
                    duplicates.Add(new Duplicate(reader["ref_no"].ToString(), 
                                                 reader["type"].ToString(), 
                                                 int.Parse(reader["count"].ToString())));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return duplicates;
        }

        public static List<Duplicate> get_duplicates(Duplicate duplicate, Company company)
        {
            List<Duplicate> duplicates = new List<Duplicate>();
            List<int> ids = new List<int>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC get_duplicates '" + duplicate.type + "','" + duplicate.reference_no + "'," + company.CompanyID);
                while (reader.Read())
                {
                    ids.Add(int.Parse(reader["id"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }

            foreach (int i in ids)
            {
                duplicates.Add(get_duplicate(i, duplicate));
            }

            return duplicates;
        }

        private static Duplicate get_duplicate(int id, Duplicate duplicate)
        {
            //Duplicate duplicate = new Duplicate();
            switch (duplicate.type)
            {
                case "policy":
                    duplicate.ref_obj = PolicyModel.getPolicyFromID(id);
                    break;
                default:
                    break;
            }
            return duplicate;
        }
        
    }
}