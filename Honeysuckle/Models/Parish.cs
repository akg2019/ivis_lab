﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class Parish
    {
        public int ID { get; set; }
        public string parish { get; set; }

        public Parish() { }
        public Parish(int ID)
        {
            this.ID = ID;
        }
        public Parish(string parish)
        {
            this.parish = parish;
        }
        public Parish(int ID, string parish)
        {
            this.ID = ID;
            this.parish = parish;
        }
    }
    public class ParishModel
    {
        /// <summary>
        /// this function returns all the parishes in the database
        /// </summary>
        /// <param name="test"></param>
        /// <returns></returns>
        public static List<Parish> ParishList(bool test = false)
        {
            List<Parish> parishes = new List<Parish>();
            if (test) parishes.Add(new Parish());
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC ParishList", "read", "parish", new Parish());
                while (reader.Read())
                {
                    parishes.Add(new Parish(int.Parse(reader["PID"].ToString()), reader["Parish"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return parishes;
        }

        /// <summary>
        /// this function returns the parish object based on parish name
        /// </summary>
        /// <param name="parishname"></param>
        /// <returns></returns>
        public static Parish GetParish(string parishname)
        {
            Parish parish = new Parish(parishname);
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetParish '" + Constants.CleanString(parish.parish) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "parish", new Parish());
                while (reader.Read())
                {
                    parish.ID = int.Parse(reader["PID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return parish;
        }
    }
}