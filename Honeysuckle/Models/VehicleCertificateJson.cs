﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Honeysuckle.Models
{
    public class VehicleCertificateJson
    {
        
        public string edi_system_id { get; set; }
        public string certificate_no { get; set; }
        public string effective_date { get; set; }
        public string expiry_date { get; set; }
        public string date_first_printed { get; set; }
        public string last_printed_on_at { get; set; }
        public string last_printed_by_trn { get; set; }
        public string endorsement_number { get; set; }
        public string printed_paper_number { get; set; }
        public string risk_chassis_number { get; set; }
        public string sign_on_at { get; set; }
        public string sign_by_trn { get; set; }
        public List<string> authorized_drivers { get; set; }
        public string limitations_of_use { get; set; }
        public string marks_and_registration_no { get; set; }
        public List<string> policy_holders { get; set; }
        public string vin { get; set; }
        public bool cancelled { get; set; }
        public string cancelled_by_trn { get; set; }
        public string cancelled_reason { get; set; }
        public string cancelled_on_at { get; set; }


        public VehicleCertificateJson() { }

        public VehicleCertificateJson(VehicleCertificate vc)
        {
            this.edi_system_id = vc.EDIID;
            this.certificate_no = vc.CertificateNo;
            this.effective_date = vc.EffectiveDate.ToString("yyyy-MM-dd HH:mm:ss");
            this.expiry_date = vc.ExpiryDate.ToString("yyyy-MM-dd HH:mm:ss");
            this.date_first_printed = vc.DateFirstPrinted.ToString("yyyy-MM-dd HH:mm:ss");
            this.last_printed_on_at = vc.LastPrintedOn.ToString("yyyy-MM-dd HH:mm:ss");
            if (vc.LastPrintedBy != null) this.last_printed_by_trn = vc.LastPrintedBy.TRN;
            this.endorsement_number = vc.EndorsementNo;
            this.printed_paper_number = vc.PrintedPaperNo;
            if (vc.Risk != null)
            {
                this.risk_chassis_number = vc.Risk.chassisno;
                this.vin = vc.Risk.VIN;
            }
            this.sign_on_at = vc.SignedAt.ToString("yyyy-MM-dd HH:mm:ss");
            if (vc.SignedBy != null) this.sign_by_trn = vc.SignedBy.TRN;
            this.authorized_drivers = new List<string>();
            foreach (Driver driver in vc.Risk.AuthorizedDrivers)
            {
                this.authorized_drivers.Add(driver.person.TRN);
            }
            if (vc.generated_cert != null) this.limitations_of_use = vc.generated_cert.limitsofuse;
            this.marks_and_registration_no = vc.MarksRegNo;
            this.policy_holders = PolicyModel.GetPolicyHolders(vc.policy);
            this.cancelled = vc.Cancelled;
            if (vc.CancelledBy != null) this.cancelled_by_trn = vc.CancelledBy.TRN;
            this.cancelled_on_at = vc.CancelledOn.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
    public class VehicleCertificateJsonModel
    {
        /// <summary>
        /// this function returns vehiclecertificatejsons from vehiclecertificates
        /// </summary>
        /// <param name="certs"></param>
        /// <returns></returns>
        public static List<VehicleCertificateJson> ParseCertsToJson(List<VehicleCertificate> certs)
        {
            List<VehicleCertificateJson> cert_jsons = new List<VehicleCertificateJson>();
            foreach (VehicleCertificate cert in certs)
            {
                cert_jsons.Add(new VehicleCertificateJson(cert));
            }
            return cert_jsons;
        }
    }
}