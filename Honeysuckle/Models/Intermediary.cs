﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;

namespace Honeysuckle.Models
{
    public class Intermediary
    {
        public int ID { get; set; }
        public Company company { get; set; }
        public bool PrintCertificate { get; set; }
        public bool PrintCoverNote { get; set; }
        public bool enabled { get; set; }

        [RegularExpression("([0-9]*)", ErrorMessage = "Incorrect value format for Print Limit (only numbers allowed) ")]
        public int PrintLimit { get; set; }

        public Intermediary() { }
        public Intermediary(int ID)
        {
            this.ID = ID;
        }
        public Intermediary(int ID, Company company)
        {
            this.ID = ID;
            this.company = company;
        }
        public Intermediary(Company company)
        {
            this.company = company;
        }
        public Intermediary(Company company, bool PrintCertificate, bool PrintCoverNote, int PrintLimit)
        {
            this.company = company;
            this.PrintCertificate = PrintCertificate;
            this.PrintCoverNote = PrintCoverNote;
            this.PrintLimit = PrintLimit;
        }
        public Intermediary(Company company, bool PrintCertificate, bool PrintCoverNote, bool enabled, int PrintLimit)
        {
            this.company = company;
            this.PrintCertificate = PrintCertificate;
            this.PrintCoverNote = PrintCoverNote;
            this.enabled = enabled;
            this.PrintLimit = PrintLimit;
        }
        public Intermediary(int ID, Company company, bool PrintCertificate, bool PrintCoverNote, int PrintLimit)
        {
            this.ID = ID;
            this.company = company;
            this.PrintCertificate = PrintCertificate;
            this.PrintCoverNote = PrintCoverNote;
            this.PrintLimit = PrintLimit;
        }
        public Intermediary(int ID, Company company, bool PrintCertificate, bool PrintCoverNote, bool enabled, int PrintLimit)
        {
            this.ID = ID;
            this.company = company;
            this.PrintCertificate = PrintCertificate;
            this.PrintCoverNote = PrintCoverNote;
            this.enabled = enabled;
            this.PrintLimit = PrintLimit;
        }
    }
    public class IntermediaryModel
    {
        /// <summary>
        /// This function is used to add an intermediary relationship to the system
        /// </summary>
        /// <param name="intermediary">Intermediary Relationship being added to the system</param>
        /// <param name="insurer"></param>
        /// <returns></returns>
        public static Intermediary AddIntermediary(Intermediary intermediary, Company insurer)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC AddIntermediary "  + insurer.CompanyID + ","
                                                        + intermediary.company.CompanyID + ",'"
                                                        + intermediary.PrintCoverNote + "','"
                                                        + intermediary.PrintCertificate + "','"
                                                        + intermediary.enabled + "',"
                                                        + intermediary.PrintLimit + ","
                                                        + Constants.user.ID + ","
                                                        + intermediary.ID;
                SqlDataReader reader = sql.QuerySQL(query, "create", "intermediary", intermediary);
                while (reader.Read())
                {
                    if (DBNull.Value != reader["ID"]) intermediary.ID = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return intermediary;
        }

        /// <summary>
        /// this function is used to associate many brokers/agents as intermediaries to an insurer
        /// </summary>
        /// <param name="intermediaries">List of Brokers/Agents being associated to insurer</param>
        /// <param name="insurer">Company that is an insurer</param>
        /// <returns></returns>
        //public static List<Intermediary> AddIntermediaries(List<Intermediary> intermediaries, Company insurer)
        //{
        //    for (int x = 0; x < intermediaries.Count; x++)
        //    {
        //        intermediaries[x] = AddIntermediary(intermediaries[x], insurer);
        //    }
        //    return intermediaries;
        //}

        /// <summary>
        /// this function removes the relationship between all brokers/agents that are currently
        /// set as the intermediaries to a single insurer in the system
        /// </summary>
        /// <param name="insurer"></param>
        //public static void RemovePreviousIntermediaries(Company insurer)
        //{
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        string query = "EXEC RemovePreviousIntermediaries " + insurer.CompanyID;
        //        SqlDataReader reader = sql.QuerySQL(query, "delete", "intermediary", insurer);
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        
        //}

        /// <summary>
        /// this function updates the particulars of a intermediary relationship between 
        /// a broker/agent and an insurer
        /// </summary>
        /// <param name="intermediary"></param>
        /// <param name="insurer"></param>
        public static void UpdateIntermediary(Intermediary intermediary, Company insurer)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC UpdateIntermediary " + insurer.CompanyID + ","
                                                          + intermediary.company.CompanyID + ",'"
                                                          + intermediary.PrintCoverNote + "','"
                                                          + intermediary.PrintCertificate + "','"
                                                          + intermediary.enabled + "',"
                                                          + intermediary.PrintLimit + ","
                                                          + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "intermediary", intermediary);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function processes the updating of many intermediaries particulars between 
        /// many brokers/agents associated to a single insurer
        /// </summary>
        /// <param name="intermediaries"></param>
        /// <param name="insurer"></param>
        //public static void UpdateIntermediaries(List<Intermediary> intermediaries, Company insurer)
        //{
        //    foreach (Intermediary intermediary in intermediaries)
        //    {
        //        if (intermediary.enabled) UpdateIntermediary(intermediary, insurer);
        //    }
        //}

        /// <summary>
        /// this function removes an intermediary relationship from the database
        /// </summary>
        /// <param name="intermediary"></param>
        /// <param name="compid"></param>
        public static void RemoveIntermediary(Intermediary intermediary)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DeleteIntermediary " + intermediary.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "intermediary", intermediary);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function returns all brokers/agents associated to an insurer as an assigned
        /// intermediary
        /// </summary>
        /// <param name="insurer"></param>
        /// <returns></returns>
        public static List<Intermediary> GetInsurerIntermediaries(Company insurer)
        {
            sql sql = new sql();
            List<Intermediary> intermediaries = new List<Intermediary>();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetInsurerIntermediaries " + insurer.CompanyID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "intermediary", new Intermediary());
                while (reader.Read())
                {
                    Intermediary intermediary = new Intermediary(int.Parse(reader["ID"].ToString()),
                                                                new Company(int.Parse(reader["CID"].ToString()), reader["cname"].ToString()),
                                                                (bool)reader["certificate"],
                                                                (bool)reader["covernote"],
                                                                int.Parse(reader["printlimit"].ToString()));
                    intermediary.enabled = (bool)reader["enabled"];
                    intermediaries.Add(intermediary);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return intermediaries;
        }

        /// <summary>
        /// this function returns all data related to a specific intermediary relationship
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Intermediary GetIntermediary(int id)
        {
            sql sql = new sql();
            Intermediary intermediary = new Intermediary(id);
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetIntermediary " + id;
                SqlDataReader reader = sql.QuerySQL(query, "read", "intermediary", new Intermediary());
                while (reader.Read())
                {
                    intermediary = new Intermediary(int.Parse(reader["ID"].ToString()), 
                                                    new Company(int.Parse(reader["CID"].ToString()), 
                                                    reader["cname"].ToString()), 
                                                    (bool)reader["certificate"], 
                                                    (bool)reader["covernote"], 
                                                    int.Parse(reader["printlimit"].ToString()));
                    intermediary.enabled = (bool)reader["enabled"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return intermediary;
        }

        public static int TestIntermediaryToInsurer(int compID, int insurer)
        {
            int result = 0;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC TestIntermediaryToInsurer " + compID + "," + insurer;
                SqlDataReader reader = sql.QuerySQL(query, "read", "intermediary", new Intermediary());
                while (reader.Read())
                {
                    result = int.Parse(reader["result"].ToString());
                }

                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function enables a intermediary relationship
        /// </summary>
        /// <param name="intermediary"></param>
        /// <returns></returns>
        public static bool EnableIntermediary(Intermediary intermediary)
        {
            sql sql = new sql();
            bool result = false;
            if (sql.ConnectSQL())
            {
                string query = "EXEC EnableIntermediary " + intermediary.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "intermediary", intermediary);
                reader.Close();
                sql.DisconnectSQL();
                result = true;
            }
            return result;
        }

        /// <summary>
        /// this function disables an intermediary relationship
        /// </summary>
        /// <param name="intermediary"></param>
        /// <returns></returns>
        public static bool DisableIntermediary(Intermediary intermediary)
        {
            sql sql = new sql();
            bool result = false;
            if (sql.ConnectSQL())
            {
                string query = "EXEC DisableIntermediary " + intermediary.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "intermediary", intermediary);
                reader.Close();
                sql.DisconnectSQL();
                result = true;
            }
            return result;
        }

        /// <summary>
        /// this function tests whether a specific broker/insurer are in an intermediary relationship currently
        /// </summary>
        /// <param name="insurer"></param>
        /// <param name="broker"></param>
        /// <returns></returns>
        public static bool IsBrokerIntermediary(Company insurer, Company broker)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsBrokerIntermediary " + insurer.CompanyID + "," + broker.CompanyID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "intermediary", new Intermediary());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }


            return result;
        }

    }
}