﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Honeysuckle.Models
{
    public class JSON
    {
        public int ID { get; set; }
        public string loginkey { get; set; }
        public string reqkey { get; set; }
        public string method { get; set; }
        public string json { get; set; }
        public bool fail { get; set; }
        public bool inprogress { get; set; }
        public bool completed { get; set; }
        public string processedjson { get; set; }
        public DateTime createdTime { get; set; }
        public DateTime completedTime { get; set; }
        public string title { get; set; }
        public int count { get; set; }
        
        public JSON() { }
        public JSON(int ID)
        {
            this.ID = ID;
        }
        public JSON(string json)
        {
            this.json = json;
        }
        public JSON(string method, string json)
        {
            this.method = method;
            this.json = json;
        }
        public JSON(string method, string json, bool fail)
        {
            this.method = method;
            this.json = json;
            this.fail = fail;
        }
        public JSON(int ID, string method, string json, bool fail)
        {
            this.ID = ID;
            this.method = method;
            this.json = json;
            this.fail = fail;
        }
    }

    public class JSONModel
    {

        /// <summary>
        /// mark a json process as failed
        /// </summary>
        /// <param name="json"></param>
        public static void JsonFailed(JSON json)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC JsonFailed " + json.ID;
                if (json.processedjson != null) query += ",'" + Constants.CleanString(json.processedjson) + "'";
                //not using update, because this already a loggable table
                SqlDataReader reader = sql.QuerySQL(query, "read", "json", json); 
                reader.Close();
                sql.DisconnectSQL();
            }
        }
        
        /// <summary>
        /// this function stores the successful response with relevant errors generated to the database
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static bool StoreResponse(JSON json)
        {
            bool result = false;
            try
            {
                    sql sql = new sql();
                    if (sql.ConnectSQL())
                    {
                        //not using update, because this already a loggable table
                        SqlDataReader reader = sql.QuerySQL("EXEC StoreResponse '" + json.reqkey + "','" + Constants.CleanString(json.processedjson) + "'," + json.count, "read", "json", json); 
                        reader.Close();
                        sql.DisconnectSQL();
                        result = true;
                    }
            }
            catch (Exception)
            {
            
            }
            return result;
        }

        /// <summary>
        /// this function updates the amount of records processed by the import parsing module
        /// </summary>
        /// <param name="json"></param>
        public static void StoreCount(JSON json)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                //not using update, because this already a loggable table
                SqlDataReader reader = sql.QuerySQL("EXEC StoreCount '" + json.reqkey + "'," + json.count, "read", "json", json);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function returns the response from the database from processed import module
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static JSON ReturnResponse(JSON json)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC ReturnResponse '" + json.reqkey + "'", "read", "json", json);
                while (reader.Read())
                {
                    json.inprogress = (bool)reader["inprogress"];
                    json.completed = (bool)reader["completed"];
                    json.fail = (bool)reader["fail"];
                    if (json.completed && !json.fail) 
                    {
                        try
                        {
                            //Remove fake/unwanted errors

                            JArray Errs = JArray.Parse(reader["processedjson"].ToString());
                            if (Errs.Count > 0)
                            {
                              
                               var FinalErros = Errs.Where(p => p["issues"].Count() > 0);

                               json.processedjson = JsonConvert.SerializeObject(FinalErros);
                            }
                            else {
                                json.processedjson = reader["processedjson"].ToString();
                            
                            }

                            
                        }
                        catch (Exception)
                        {
                         
                        }
                       
                    }

                    json.count = int.Parse(reader["count"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return json;
        }

        /// <summary>
        /// this function stores the main json received through the import webservice to the database
        /// for later reference and generates a GUID for the record that will be used by all users to 
        /// determine whether the record has been processed.
        /// </summary>
        /// <param name="json"></param>
        /// <param name="method"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static JSON StoreJSON(string json, string method, int uid = 0)
        {
            sql sql = new sql();
            JSON js = new JSON(method, json);
            if (sql.ConnectSQL())
            {
                string query = "EXEC StoreJson '" + Constants.CleanString(js.json) + "','" + Constants.CleanString(method) + "'";
                if (uid != 0) query += "," + uid;
                SqlDataReader reader = sql.QuerySQL(query, "read", "json", json);
                while (reader.Read())
                {
                    js.ID = int.Parse(reader["ID"].ToString());
                    js.reqkey = reader["key"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return js;
        }

        /// <summary>
        /// this function is used when policy update is being done internally.
        /// The contents of the file may not be able to hold in the database, so the file is already stored.
        /// This function only stores method (if any) and just simple creates a relevant record
        /// in JsonAudit table to continue processing and later storing of data
        /// </summary>
        /// <param name="json"></param>
        /// <param name="method"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static JSON StoreJsonRecord(string json, string method, int uid = 0)
        {
            sql sql = new sql();
            if (method == null) method = "";
            JSON js = new JSON(method, json);
            if (sql.ConnectSQL())
            {
                string query = "EXEC StoreJsonRecord '" +  Constants.CleanString(method) + "'";
                if (uid != 0) query += "," + uid;
                SqlDataReader reader = sql.QuerySQL(query, "read", "json", json);
                while (reader.Read())
                {
                    js.ID = int.Parse(reader["ID"].ToString());
                    js.reqkey = reader["key"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return js;
        }

        public static JSON CheckJsonProgress(int userID, int jsonID = 0)
        {
            JSON js = new JSON();
            sql sql = new sql();

            if (sql.ConnectSQL())
            {
                string query = "EXEC CheckJsonProgress " + userID + "," + jsonID;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    js.ID = int.Parse(reader["ID"].ToString());
                    js.inprogress = (bool)reader["inprogress"];
                    js.fail = (bool)reader["fail"];
                    js.completed = (bool)reader["completed"];
                    js.processedjson = (DBNull.Value != reader["processedjson"]) ? reader["processedjson"].ToString() : "";
                    js.count = int.Parse(reader["count"].ToString());
                    if (DBNull.Value != reader["CreatedTimeStamp"]) js.createdTime = DateTime.Parse(reader["CreatedTimeStamp"].ToString());
                    if (DBNull.Value != reader["CompletedTimeStamp"]) js.completedTime = DateTime.Parse(reader["CompletedTimeStamp"].ToString());
                    js.title =  (DBNull.Value != reader["JsonTitle"]) ? reader["JsonTitle"].ToString() : "";
                }

                reader.Close();
                sql.DisconnectSQL();
            }

            return js;

        }
    }
}