﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.User>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ResetPassword
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <div id="pass"></div>

        <div class="error"><%:ViewData["error"] %></div>

        <div id="reset-form" class="box-shadow tiny-box">
            <div id= "questionHeading"> 
                <div id="UserImg">
                    <img src="../../Content/more_images/User.png" alt=""/>
                </div>
                <div id="sQuestion"> 
                     Reset Password
                </div>
            </div>
             
             <div class="field-data">
                <div class="editor-label">
                    Username
                </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.username, new { id = "username", @readonly = "readonly", @required = "required", @Class= "read-only username-reset", @autocomplete = "off"})%>
                </div>
            
                <div class="passRules"> Password must contain: numbers, capital & common letters, special characters (!,@,#,$ ,%,^,&,*,(,)) and at least 8 characters</div>

                <div class="editor-label">
                    Password
                </div>
                <div class="editor-field">
                    <%: Html.PasswordFor(model => model.password, new { id = "password1", @required = "required", @autocomplete = "off", @class = "password" })%>
                </div>
                <div class="generateLink"> <a href="#" class="generate" onclick="generatePassword();"> Generate Password</a></div> 

                <div class="editor-label">
                    Confirm Password
                </div>
                <div class="editor-field">
                    <%: Html.PasswordFor(model => model.confirm_password, new { id = "confirm_password", @required = "required", @autocomplete = "off", @class = "password" })%>
                <div class="password-invalid"><%: Html.ValidationMessageFor(model => model.confirm_password)%></div> 
                </div><br /><br />
            </div>

            <div class="btnlinks flogin reset-btn">
                <input type="submit" value="Done" id= "Submit3" class = "reset-done" />
            </div>
            
        </div>

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/loginform.js" type="text/javascript"></script>
    <link href="../../Content/LoginPage.css" rel="stylesheet" type="text/css" />
</asp:Content>
