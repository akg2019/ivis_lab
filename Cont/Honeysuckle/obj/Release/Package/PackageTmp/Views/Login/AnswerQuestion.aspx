﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.User>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Answer Question
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
       
            

            <% if (ViewData["responsebad"] != null) { %> <div id="responsebad">bad</div> <% } %>
            <%: Html.HiddenFor(model => model.ID, new { @class = "dont-process" })%>
            <%: Html.HiddenFor(model => model.username, new { @class = "dont-process" })%>
            <div id="question-form" class="box-shadow tiny-box">
                <div class="heading"> <div id="UserImg"><img src="../../Content/more_images/User.png" alt=""/></div><div id="sQuestion"> Security Question</div></div>
                <div class="info question-content">
                    <div class="editor-label questions-label">
                       Question:
                    </div>
                    <div class="editor-field fields">
                        <%: Html.TextBoxFor(model => model.question, new { @readonly = "readonly", @class= "read-only" })%>
                    </div>
            
                    <div class="editor-label questions-label">
                       Response
                    </div>
                    <div class="editor-field fields">
                        <%: Html.TextBoxFor(model => model.response, new { @required = "required", @class = "anything" })%>
                        <%: Html.ValidationMessageFor(model => model.response) %>
                    </div>
                </div>
                <div class="btnlinks btn-questions">
                    <input type="submit" name="submit" value="Submit" id= "resetPass"/>
                    <input type="submit" name="submit" value="Cancel" id= "Cancel" />
                </div>
            </div>

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/security-question.js" type="text/javascript"></script>
    <link href="../../Content/LoginPage.css" rel="stylesheet" type="text/css" />
</asp:Content>


