﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.User>" %>

<% using(Html.BeginForm()){ %>
    <div id= "welcome">Welcome To IVIS</div>
    LOGIN  <br /><hr/><br />
        <div id ="username"> <%: Html.TextBoxFor(Model => Model.username, new { id = "usernamefield", @required = "required", placeholder = "User Name" })%></div><br />
        <div id="password"><%: Html.PasswordFor(Model => Model.password, new { id = "passwordfield", placeholder = "Password" })%></div><br />
        <div id="submit"><input type="submit" name="submit" value="Sign In" id="signInButton" /></div><br />
        <div id="reset"><input type="submit" name="submit" value="Forgot Password" /></div>
        <div id="clear"><input id="clearbtn" type="button" value="Clear" onclick="clear();" /></div>

<% } %>
