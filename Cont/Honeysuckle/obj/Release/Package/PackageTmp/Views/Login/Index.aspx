﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.User>" %>

<asp:Content ID="loginBackground" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
         <div class="error"><%:ViewData["error"] %></div>
        <% if (ViewData["reset"] != null)
           { %>
             <div class="error">The user: <%: ViewData["reset"] %> has been sent a reset link</div>
        <% } %>


        <div class="login-form">
        <div id= "welcome">Welcome To IVIS</div>
        LOGIN  <br /><hr/><br />
            <div id ="username"> <%: Html.TextBoxFor(Model => Model.username, new { id = "usernamefield", @required = "required", placeholder = "User Name", AutoComplete = "off",ClearCachedClientID = true })%></div><br />
            <div id="password"><%: Html.PasswordFor(Model => Model.password, new { id = "passwordfield", placeholder = "Password" })%></div><br />
            <div id="submit"><input type="submit" name="submit" value="Sign In" id="signInButton" /></div><br />
            <div id="reset"><input type="submit" name="submit" value="Forgot Password" /></div>
            <div id="clear"><input id="clearbtn" type="button" value="Clear" onclick="clear();" /></div>
            
        </div>
    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/loginform.js" type="text/javascript"></script>
    <link href="../../Content/LoginPage.css" rel="stylesheet" type="text/css" />
</asp:Content>

