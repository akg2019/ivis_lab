﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Vehicle>>" %>

    <div id="vehicles-search">
        <div class="row menu">
            <div class="item">
            </div>
            <div class="item">
                make
            </div>
            <div class="item">
                model
            </div>
            <div class="item">
                modelType
            </div>
            <div class="item">
                bodyType
            </div>
            <div class="item">
                extension
            </div>
            <div class="item">
                vehicleRegNumber
            </div>
            <div class="item">
                VIN
            </div>
            
        </div>
    <% int i = 0; %>
    <% foreach (var item in Model) { %>
        <% i++;
           if ((i % 2) == 1)
           { %>
        <div class="row odd">
        <% } %>
        <% else
           { %>
        <div class="row even">
        <% } %>
            <div class="item">
                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Vehicles", "Update")))
                  { %> 
                       <%: Html.ActionLink("Edit", "Edit", new {  id=item.ID  }) %> |
               <% } %>


               <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Vehicles", "Update")))
                  { %> 
                       <%: Html.ActionLink("Edit Drivers", "EditDrivers", new {  id=item.ID  }) %> |
               <% } %>


                <%: Html.ActionLink("Details", "Details", new { id=item.ID })%> |

                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Vehicles", "Delete")))
                  { %> 
                        <%: Html.ActionLink("Delete", "Delete", new {  id=item.ID})%> |
               <% } %>


                <% if (Honeysuckle.Models.VehicleCoverNoteModel.IsActiveCoverNoteExist(new Honeysuckle.Models.Vehicle(item.ID))) 
                   { %>
                    <%: Html.ActionLink("View Current Cover Note", "Details", "VehicleCoverNote", new { id = Honeysuckle.Models.VehicleCoverNoteModel.GetCurrentCoverNote(new Honeysuckle.Models.Vehicle(item.ID)).ID }, null)%> | 
                <% } %>
                <% else 
                   { %>
                    <%: Html.ActionLink("Create Cover Note", "Create", "VehicleCoverNote", new { id = item.ID }, null) %> | 
                <% } %>


                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Read")))
                   { %>
                        <% if (Honeysuckle.Models.VehicleCertificateModel.IsVehicleUnderActiveCert(item.ID)) 
                           { %>
                                 <%: Html.ActionLink("View Current Certificate", "Details", "VehicleCertificate", new { id = Honeysuckle.Models.VehicleCertificateModel.GetThisVehicleActiveCertificate(item.ID).VehicleCertificateID }, null)%> | 
                        <% } %>
                        <% else
                           { %>
                                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Create")))
                                   { %> 
                                     <%: Html.ActionLink("Create Certificate", "Create", "VehicleCertificate", new {  id=item.ID}, null) %> 
                                <% } %>
                        <% } %>
                   <% } %>

            </div>
            <div class="item">
                <%: item.make %>
            </div>
            <div class="item">
                <%: item.VehicleModel %>
            </div>
            <div class="item">
                <%: item.modelType %>
            </div>
            <div class="item">
                <%: item.bodyType %>
            </div>
            <div class="item">
                <%: item.extension %>
            </div>
            <div class="item">
                <%: item.VehicleRegNumber %>
            </div>
            <div class="item">
                <%: item.VIN %> 
            </div>
            
        </div>
    
    <% } %>

    </div>