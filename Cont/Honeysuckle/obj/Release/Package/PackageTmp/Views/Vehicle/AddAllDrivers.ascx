﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<h4> Add drivers to vehicle: </h4>
<br />
<h4> VIN</h4> <%: Html.TextBox("vehicleVIN", null, new { @readonly = "readonly", id = "vin", @Value = ViewData["vin"] })%>

<div class="row" style="clear:both;">
    <div id="authorized"> <h6> ADD Authorized drivers </h6></div>
    <div id="auth"></div>
    <div id="authorizedDriver"></div><br />

    <div id="excepted"><h6> ADD Excepted drivers</h6> </div>
    <div id="except"></div>
    <div id="exceptedDriver"></div> <br />

    <div id="excluded"><h6> ADD Excluded drivers </h6></div>
    <div id="excl"></div>
    <div id="excludedDriver"></div>
    <div id="createDriv"></div>
    <div id="createDriver"></div>
    <div id="vehicleVIN"></div>

</div>
<script src="../../Scripts/ApplicationJS/multipleVehicles.js" type="text/javascript"></script>
