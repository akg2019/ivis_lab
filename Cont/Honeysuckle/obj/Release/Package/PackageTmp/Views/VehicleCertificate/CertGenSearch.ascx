﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.VehicleCertificate>>" %>

    <div id = "certificates-search">
        <div class="menu row">
            <div class="item">
            </div>
            <div class="item">
                Company Name
            </div>
            <div class="item">
                Policy Number
            </div>
            <div class="item">
                UniqueNum
            </div>
            <div class="item">
                Effective Date
            </div>
            <div class="item">
                Exprity Date
            </div>
        </div>

    <% foreach (var item in Model) { %>
    
        <div class="row">
            <div class="item">
                <%: Html.ActionLink("Edit", "Edit", new { /* id=item.PrimaryKey */ }) %> |
                <%: Html.ActionLink("Details", "Details", new { /* id=item.PrimaryKey */ })%> |
                <%: Html.ActionLink("Delete", "Delete", new { /* id=item.PrimaryKey */ })%>
            </div>
            <div class="item">
                <%: item.company.CompanyName %>
            </div>
            <div class="item">
                <%: item.policy.policyNumber %>
            </div>
            <div class="item">
                <%: item.UniqueNum %>
            </div>
            <div class="item">
                <%: String.Format("{0:d}", item.EffectiveDate.Date)%>
            </div>
            <div class="item">
                <%: String.Format("{0:d}", item.ExpiryDate.Date)%>
            </div>
        </div>
    
    <% } %>

    </div>
