﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.VehicleCoverNote>>" %>

  <div class="approval-modal">
     
   <div class="ivis-table">
  
   <div class="vehicle-check item selectItem selectAll">
        <%: Html.CheckBox("selectAll", false, new { @class = "selectAll", @OnClick = "changeSelectAll(this)" })%>  Select All
    </div>

     <div class= "menu vehicle-row">
        <div class="item selectItem">Select</div> 
        <div class="element item"> CoverNote No. </div>
        <div class="make item"> Risk </div>
       
     </div>

        <div id="vehiclesDocList">
          <% if(Model.Count() == 0) 
          { %>
              <div class="rowNone">There are No Cover Notes at this time</div>
       <% } %>

        <% int i = 0; %>
        <% foreach (Honeysuckle.Models.VehicleCoverNote item in Model)
           { %>
            <% if (i % 2 == 0) { %><div class="vehicle-row row even"> <% } %>
            <% else { %> <div class="vehicle-row row odd"> <% } %>
                <% i++; %>

                <div class="vehicle-check item selectItem">
                    <%: Html.CheckBox("vehcheck", false, new { @class = "vehcheck", id = item.ID.ToString()})%>
                </div>
              
                <div class="item">
                    <% item.covernoteid.ToString(); %>
                </div>

                 <div class="item make">
                    <%: item.risk.VehicleYear %> , <%:item.risk.make  %>  <%:item.risk.VehicleModel  %>
                </div>

            </div>
        <% } %>
     </div>


        </div> 
   </div> 
</div>
