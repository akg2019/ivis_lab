﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.PolicyCover>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	AssociateUsages
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm()) { %>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>

            <div id="form-header">
                <img src="../../Content/more_images/UsageManager.png" />
                <div class="form-text-links">
                    <span class="form-header-text">
                        Associate Usages To Policy Covers
                    </span>
                   <div class="icon">
                        <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                   </div>
                </div>
            </div>
            <div class="usages-manage"></div>
            
            <div class="field-data">
                
                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right down"></div>
                        Main Cover Data
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <%: Html.HiddenFor(model => model.ID, new { id = "coverid", @class = "dont-process" })%>
                        <div class="editor-label">
                            Prefix:
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.prefix, new { @readonly = "readonly", @class = "read-only" })%>
                            <%: Html.ValidationMessageFor(model => model.prefix) %>
                        </div>

                        <div class="editor-label">
                            Policy Cover:
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.cover, new { @readonly = "readonly", @class = "read-only" })%>
                            <%: Html.ValidationMessageFor(model => model.cover) %>
                        </div>
                    </div>
                </div>
                <!-- ADD USAGES HERE -->
                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right"></div>
                        Usages
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div id="usages">
                            <div class="ivis-table">
		                        <div class="menu">
			                        <div class="item usages">
                                        Usages
                                    </div>
                                    <div class="item usageStat">
                                        Status
                                    </div>
		                        </div>
                                <div class="table-data-usages">
                                    <% if (Model.usages != null) {
                                           int i = 0; %>
                                        <% foreach(var item in Model.usages) { %>

                                             <% if (i%2 == 0) { %>
                                                <div class="usages row data odd"> <!-- begin div -->
                                            <% } %>
                                            <% else { %>
                                                <div class="usages row data even"> <!-- begin div -->
                                            <% } %>

                                            <% Html.RenderAction("Details", "Usage", new { id = item.ID, cid = Model.ID}); %>
                                            <% i++; %>
                                            </div> <!-- end div //usages -->
                                        <% } %>
                                    <% } %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="btnlinks">
                <% Html.RenderAction("BackBtn", "Back"); %>
                <input type="button" value="Add Usage" id="addusage" />
            </div>

        </fieldset>

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/covers.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/multipleUsages.js" type="text/javascript"></script>
    <link href="../../Content/cover-usages.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

