﻿<%@ Page Title="Policy Cover Details" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.PolicyCover>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <fieldset>

    <% bool manageUsage = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Usages", "Create")); %>
    <%: Html.Hidden("policyCoverID", Model.ID, new {id="policyCoverID", @class="dont-process" })%>
        
        <div id= "form-header">  <img src="../../Content/more_images/Policy.png" alt=""/>
            <div class="form-text-links">
                   <span class="form-header-text">Policy Cover Details</span>
                   <div class="icon">
                        <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                  </div>
            </div>
        </div>
        <div class="field-data">
            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right down"></div>
                    Main Cover Data
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div class="editor-label">
                        Prefix:
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.prefix, new { @readonly = "readonly", @class = "read-only" })%>
                        <%: Html.ValidationMessageFor(model => model.prefix) %>
                    </div>


                    <div class="editor-label">
                        Policy Cover:
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.cover, new { @readonly = "readonly", @class = "read-only" })%>
                        <%: Html.ValidationMessageFor(model => model.cover) %>
                    </div>
            
                    <div class="check-field">
                        <div class="editor-label">
                            Is Commercial Business:
                        </div>
                        <div class="editor-field">
                            <%: Html.CheckBoxFor(model => model.isCommercialBusiness, new { @readonly = "readonly", @class = "read-only", @disabled = "disabled" })%>
                            <%: Html.ValidationMessageFor(model => model.isCommercialBusiness) %>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ADD USAGES HERE -->
            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right"></div>
                    Usages
                    <div class="valid"></div>
                </div>
                <div id="usages" class="data">
                    <div class="ivis-table">
                        <div class="menu">
                            <div class="item usages">
                                Usages
                            </div>
                        </div>
                        <div class="table-data-usages">
                            <% if (Model.usages != null) { 
                                int i = 0; %>
                                <% foreach(var item in Model.usages) { %>
                                    <% if (i%2 == 0) { %>
                                        <div class="usages row data odd"> <!-- begin div -->
                                    <% } %>
                                    <% else { %>
                                        <div class="usages row data even"> <!-- begin div -->
                                    <% } %>
                                        <% Html.RenderAction("Details", "Usage", new { id = item.ID, cid = Model.ID, details = true }); %>
                                        <% i++; %>
                                    </div> <!-- end div //usages -->
                                <% } %>
                            <% } %>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="btnlinks">
                <% Html.RenderAction("BackBtn", "Back"); %>
                <% if (manageUsage)
                   { %>
                    <button class="manageUsages">Manage Usages</button>
                <% } %>
            </div>
    
        </div>
    </fieldset>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
  <link href="../../Content/cover-usages.css" rel="stylesheet" type="text/css" />
  <script src="../../Scripts/ApplicationJS/manageUsages.js" type="text/javascript"></script>
</asp:Content>


