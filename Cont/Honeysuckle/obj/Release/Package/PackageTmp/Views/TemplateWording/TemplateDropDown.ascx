﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.TemplateWording>>" %>

<%: Html.DropDownList("wording", new SelectList(Model.ToList(), "ID", "CertificateID"), new { @class = "formatDropdown", @required = "required" })%>