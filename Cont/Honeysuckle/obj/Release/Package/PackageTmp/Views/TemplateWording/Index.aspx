﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.TemplateWording>>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <%
        bool wording_update = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Wording","Update"));
        bool wording_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Wording", "Read"));
        bool wording_delete = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Wording", "Delete"));
        bool wording_create = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Wording", "Create"));
    %>
    
    <div id="wording-page">
        
        <div id="delete"><% if(ViewData["delete"] != null) ViewData["delete"].ToString(); %></div>
        <%: Html.Hidden("page", "index", new { @class = "dont-process" })%>
        
        <div id="form-header">
            <img src="../../Content/more_images/Wording_Header.png" />
            <div class="form-text-links">
                 <span class="form-header-text">Template Wording Manager</span>
            </div>
            <div id="wording-search-div" class="index-search search-box" >
                <div class="word-count"><span>Showing:</span><span class="count"></span></div>
                <%: Html.TextBox("search-wording", null, new { @placeholder = "Search [Wording]", @autocomplete="off", @class="quick-search wording" })%>
                <button type="button" class="quick-search-btn user-search-btn" onclick="clickSearchBtn()" ><img src="../../Content/more_images/Search_Icon.png" /></button>
            </div>
        </div>
        <div class="field-data">
            
            
    
            <div id="wording-list" class="ivis-table">
                <div class="wording menu">
                    <div class="word type item">
                        Certificate Type
                    </div>
                    <div class="word ext item">
                        Extension Code
                    </div>
                </div>
                <div id="wording-data-list" class="table-data">
                    <% int i = 0; %>
                    <% foreach (var item in Model) 
                        { %>
            
                        <% i++; %>
                        <% if (i % 2 == 0)
                            { %>
                            <div class="wording even data row">
                        <%  } %>
                        <% else
                            { %>
                            <div class="wording odd data row">   
                        <%  } %>
                            <%: Html.Hidden("hidid", item.ID, new { @class = "wordingid id dont-process" })%>
                            <div class="data type item">
                                <%: item.CertificateType %>
                            </div>
                            <div class="data ext item">
                                <%: item.ExtensionCode %>
                            </div>
                            <div class="functions item">
                                <% if (!item.imported && wording_update) 
                                    { %> 
                                    <div class="edit icon" title="Edit Wording">
                                        <img src="../../Content/more_images/edit_icon.png" class="sprite" />
                                    </div>
                                <%  } %>
                                <% if (wording_read) 
                                    { %> 
                                    <div class="view icon" title="View Wording Details">
                                        <img src="../../Content/more_images/view_icon.png" class="sprite" />    
                                    </div>
                                <%  } %>
                                <% if (!item.imported && wording_delete && item.can_delete) 
                                   { %> 
                                    <div class="delete icon" title="Remove Wording">
                                        <img src="../../Content/more_images/delete_icon.png" class="sprite" />
                                    </div>
                                <% } %>
                                <div class="cover_usage icon" title="Manage Cover/Usage Relationships with Wording">
                                    <img src="../../Content/more_images/wording-cover-icon.png" class="sprite" />
                                </div>
                            </div>
                        </div>
    
                    <% } %>
                
                </div>
            </div>
        
        
            <div class="btnlinks">
                <% if (wording_create) { %>
                    <button class="wordingcreate" onclick="CreateNewTemp()">New Template</button>
                <%  } %> 
                <% if (Honeysuckle.Models.UserGroupModel.IsAdministrative(Honeysuckle.Models.Constants.user)) { %>
                    <button class="wordingcreate" onclick="UploadTemp()">Upload Template</button>
                <%  } %> 
            </div>   
    </div>

    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/wording.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/wording.js" type="text/javascript"></script>
</asp:Content>


