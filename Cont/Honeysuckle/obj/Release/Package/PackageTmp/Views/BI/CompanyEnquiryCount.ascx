﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.KPI>>" %>

<%
    int company_id = Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID;
    bool is_iaj = Honeysuckle.Models.CompanyModel.is_company_iaj(company_id);
    bool is_insurer = Honeysuckle.Models.CompanyModel.IsCompanyInsurer(company_id);
    bool is_broker = Honeysuckle.Models.CompanyModel.IsCompanyBroker(company_id);
%>

<div class="container medium">
    <div class="stripes"></div>
    <div class="form-header">
        <div class="form-text-links">
            <span class="form-header-text">Enquiry Chart</span>
        </div>
        <div class="date-pickers">
            <%: Html.TextBox("enquiry-start", DateTime.Now.AddMonths(-1), new { @class = "datepicker enquiry start", @readonly = "readonly", @Value = DateTime.Now.AddMonths(-1).ToString("yyyy MM dd") })%>
            <strong>-</strong>
            <%: Html.TextBox("enquiry-end", DateTime.Now, new { @class = "datepicker enquiry end", @readonly = "readonly", @Value = DateTime.Now.ToString("yyyy MM dd") })%>
            <button class="date-change enquiry" onclick="update_enquiry_chart_data()">Update</button>
        </div>
        <div class="cover-filters">
            <span>Type:</span>
            <% if (is_iaj) { %>
                <%: Html.DropDownList(  "ins_brk_enq",
                                        new SelectList(new[] { new SelectListItem() { Text = "insurer", Value = "i", Selected = true }, new SelectListItem() { Text = "broker", Value = "b" }, new SelectListItem() { Text = "agent", Value = "a" } }, "Value", "Text"),
                                        new { @class = "ins_brk_enq" }
                                     ) %>
            <% } %>
            <% else if(is_broker){ %>
                <%: Html.DropDownList(  "ins_brk_enq",
                                        new SelectList(new[] { new SelectListItem() { Text = "broker", Value = "b", Selected = true }, new SelectListItem() { Text = "agent", Value = "a" } }, "Value", "Text"),
                                        new { @class = "ins_brk_enq" }
                                     ) %>
            <% } %>
               <% else if (is_insurer)
               { %>
                 <%: Html.DropDownList(  "ins_brk_enq",
                                        new SelectList(new[] { new SelectListItem() { Text = "insurer", Value = "i", Selected = true }, new SelectListItem() { Text = "broker", Value = "b" }, new SelectListItem() { Text = "agent", Value = "a" } }, "Value", "Text"),
                                        new { @class = "ins_brk_enq" }
                                     ) %>
            <% } %>
        </div>
        
    </div>
    <div class="chart-data">
        <div class="date-pickers">
        </div>
        <div id="enquiry-count" style="height:500px; width:100%;"></div>
    </div>
</div>