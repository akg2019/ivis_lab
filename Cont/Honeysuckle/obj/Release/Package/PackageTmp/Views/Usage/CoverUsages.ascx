﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Usage>>" %>

<div id="usages">
    <% foreach(var item in Model) {  %>
        <div class="usage data row">
            <%: Html.Hidden("usageid", item.ID, new { @class = "dont-process" })%>
            <div class="usage data text">
                <%: Html.TextBox("usage", item.usage, new { @readonly = "readonly", @class = "read-only" }) %>
            </div>
            <div class="usage action">
                Remove Usage
            </div>
        </div>
    <% } %>
    <div id="addusage">Add Usage</div>
</div>    


