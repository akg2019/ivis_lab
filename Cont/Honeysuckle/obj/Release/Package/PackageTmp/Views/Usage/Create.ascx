﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Usage>" %>

<div class="usage-row">
    <div class="editor-field">
        <%: Html.TextBoxFor(model => model.usage, new { @class = "usage-field" })%>
    </div>
</div>
  