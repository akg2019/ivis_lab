﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Email>" %>

    <div class="newemail">
        <% using (Html.BeginCollectionItem("emails")) 
           { %>
            <%: Html.ValidationSummary(true) %>
                <div class="email-data">
                    <div class="editor-label">
                        Email: 
                    </div>
                
                    <% if (Model.emailID != 0)
                       { %>
                        <div class="editor-field">
                            <%: Html.HiddenFor(model => model.emailID, new { @class = "dont-process" })%>
                            <%: Html.TextBoxFor(model => model.email, new { id = "email", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.email)%>
                        </div>
                    <% } %>
                    <% else
                       { %>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.email, new { @class = "email" })%>   
                            <%: Html.ValidationMessageFor(model => model.email)%>
                        </div>
                    <% } %> 
                </div>
                <div class="primaryLabel">
                   <%: Html.CheckBoxFor(model => model.primary, new { @class = "emailprimary", @title = "check this to set this email as the user's primary email" })%>
                </div>
              
                <a href="#" class="rmemail icon" title="Remove Email">
                    <img src="../../Content/more_images/delete_icon.png" class="sprite" />
                </a>

        <% } %> 
    </div>


