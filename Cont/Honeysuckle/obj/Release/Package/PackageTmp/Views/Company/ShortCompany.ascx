﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Company>>" %>

<%// Html.RenderPartial("~/Views/Company/ShowShortCompany.ascx", Model); %>

    <%: Html.Hidden("hiddencount", Model.Count(), new { @class = "dont-process" })%>

    <%
        bool company_update = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Update"));
        bool company_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Read"));
        bool company_delete = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Delete"));
        bool company_enable = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Enable"));
        bool amadmin = Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user);
     %>

    <div class="ivis-table">

        <div class="shortcomp menu">
            <div class="comprow iajid item">
                IAJID
            </div>
            <div class="comprow compname item">
                Name
            </div>
            <div class="comprow comptype item">
                Type
            </div>
            <div class="comprow trn item">
                TRN
            </div>
        </div>
        <div class="scroll-table">
        <% int i = 0; %>
        <% foreach (var item in Model) 
            { %>
            <% string TRN = "";
               if (item.trn == "1000000000000") TRN = "Not Available"; else TRN = item.trn;
             %>
            <% i++; %>
            <% if (i % 2 == 0)
                { %>
                <% if (item.Enabled) { %>
                    <div class="shortcomp data even row company">
                <% } %>
                <% else
                   { %>
                   <div class="shortcomp data even row company disabled">
                <% } %>
            <%  } %>
            <% else
                { %>
                <% if (item.Enabled) { %>
                    <div class="shortcomp data odd row company">
                <% } %>
                <% else
                   { %>
                   <div class="shortcomp data odd row company disabled">
                <% } %>
            <%  } %>
                <%: Html.Hidden("name", item.CompanyID, new { @class = "id dont-process" })%>
                 <%: Html.Hidden("hidimport", item.imported, new { @class = "hidimport dont-process" })%>
                <div class="comprow iajid item">
                    <%: item.IAJID %>
                </div>
                <div class="comprow compname item">
                    <%: item.CompanyName %>
                </div>
                <div class="comprow comptype item">
                    <%: item.CompanyType %>
                </div>
                <div class="comprow trn item">
                    <%: TRN  %>
                </div>
                <%: Html.Hidden("cid", item.CompanyID, new { @class = "dont-process" })%>
                <div class="comprow functions item">
                    <% if (company_update && (Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID == item.CompanyID || Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user) || Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user)))
                       { %>
                        <div id="editImage" class="icon edit company" title="Edit Company"  > 
                            <img src="../../Content/more_images/edit_icon.png" class="sprite" />
                        </div>
                    <% } %>
                    <% if (company_read) { %> 
                        <div class="icon view company" title="View Company">
                            <img src="../../Content/more_images/view_icon.png" class="sprite" />
                        </div> 
                    <% } %>
                    <% if (!item.has_data && company_delete && amadmin)  { %> 
                        <div id="deleteImage" class="icon delete company" title="Delete Company">
                            <img src="../../Content/more_images/delete_icon.png" class="sprite" />
                        </div>
                    <% } %>
                    <% if (company_enable) { %>
                        <% if (item.Enabled) { %>
                                <div id="disableImage" class="icon disable company" title="Deactivate Company">
                                    <img src="../../Content/more_images/disable_icon.png" class="sprite" />
                                </div> 
                        <% } %>
                        <% else { %>
                                <div id="enableImage" class="icon enable company" title="Activate Company">
                                    <img src="../../Content/more_images/enable_icon.png" class="sprite" />
                                </div> 
                        <% } %>
                    <% } %>
                    
                </div>
            </div>
    
        <% } %>
       </div>
    </div>