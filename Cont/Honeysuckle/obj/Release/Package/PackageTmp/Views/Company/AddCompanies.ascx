﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Company>" %>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
          
          
           <div class="subheader"> Company Information </div>
            <%: Html.Hidden("editingPolCompany", false, new { id = "editingPolCompany", @class = "dont-process" })%>
            <div class="editor-label">
                 TRN
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.trn, new { id = "trn", @required = "required", @PlaceHolder= " Eg. 088776655", @class="company-trn"})%>
                <%: Html.ValidationMessageFor(model => model.trn)%>
            </div>
            
            <div class="editor-label">
               Company Name
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.CompanyName, new { id = "CompanyName", @required = "required" })%>
                <%: Html.ValidationMessageFor(model => model.CompanyName) %>
            </div>
           
        <br />
           <div class="subheader"> Company Address </div>
            
            <% Html.RenderAction("Address", "Address"); %>
           
        <script src="../../Scripts/ApplicationJS/TestCompanyTRN.js" type="text/javascript"></script> 
        <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>

    <% } %>

   

