﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Company>" %>

<div class="editor-label">
    Policy Holder
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.CompanyName, new { @readonly = "readonly", @class = "read-only" }) %>
</div>