﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Company>" %>

<div class="company-row data selected row" >
   
    <%: Html.HiddenFor(model => model.base_id, new { @class = "baseid dont-process" })%>
    <% string TRN = "";
       if (Model.trn == "1000000000000") TRN = "Not Available"; else TRN = Model.trn;
      %>
    <div class="company-check select item" >
        <%: Html.CheckBox("companycheck", true, new { @class = "companycheck", id = Model.CompanyID.ToString(), @OnClick = "changeCompanyCheckBox(this)" })%>
    </div>
    <div class="company-IAJID iajid item" >
        <%: TRN %>
    </div>
    <div class="companyName item" >
        <%: Model.CompanyName %>
    </div>
    <div class="companyType item" >
        <%: Model.CompanyType %>
    </div>
</div>