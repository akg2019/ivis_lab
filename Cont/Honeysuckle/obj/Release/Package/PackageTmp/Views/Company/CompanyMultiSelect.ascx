﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Company>>" %>

<%: Html.ListBox("companies", new SelectList(Model.ToList(), "CompanyID", "CompanyName")) %>