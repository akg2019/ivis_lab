﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Policy>>" %>

    <div id="policies-search">
        <div class="menu row">
            <div class="item"></div>
            <div class="item">
                policyNumber
            </div>
            <div class="item">
                Company
            </div>
            <div class="item">
                Start Date
            </div>
            <div class="item">
                End Date
            </div>
    </div>
    <% foreach (var item in Model) { %>
    
        <div class="row">
            <div class="item">
                <%: Html.ActionLink("Edit", "Edit", new { /* id=item.PrimaryKey */ }) %> |
                <%: Html.ActionLink("Details", "Details", new { /* id=item.PrimaryKey */ })%> |
                <%: Html.ActionLink("Delete", "Delete", new { /* id=item.PrimaryKey */ })%>
            </div>
            <div class="item">
                <%: item.insuredby.CompanyName %>
            </div>
            <div class="item">
                <%: item.policyNumber %>
            </div>
            <div class="item">
                <%: String.Format("{0:d}", item.startDateTime.Date) %>
            </div>
            <div class="item">
                <%: String.Format("{0:d}", item.endDateTime.Date) %>
            </div>
            
        </div>
    
    <% } %>

    </div>


