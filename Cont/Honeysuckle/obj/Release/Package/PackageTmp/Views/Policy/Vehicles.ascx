﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Vehicle>>" %>

<div class="vehicles-modal">
    <div class="ivis-table modal-list">
        <div class= "menu" > 
            <div class="chassisno item">Chassis No</div>
            <div class="make item">Make</div>
            <div class="model item">Model</div>
            <div class="regno item">Reg. No.</div>
        </div>
        <div class="vehicle-list">
            <% for (int i = 0; i < Model.Count(); i++) { %>
                <% if (i % 2 == 0) { %><div class="vehicle-row row even"> <% } %>
                <% else { %> <div class="vehicle-row row odd"> <% } %>
                    <%: Html.HiddenFor(model => Model.ElementAt(i).ID, new { @class = "vehicle_Id dont-process" })%>
                    <div class="chassisno item">
                        <%: Model.ElementAt(i).chassisno %>
                    </div>
                    <div class="make item">
                        <%: Model.ElementAt(i).make %>
                    </div>
                    <div class="model item">
                        <%: Model.ElementAt(i).VehicleModel %>
                    </div>
                    <div class="regno item">
                        <%: Model.ElementAt(i).VehicleRegNumber %>
                    </div>
                </div>
            <% } %>
        </div>
    </div>
</div>