﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Search>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Search Results
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="page-content">
        <%: Html.Hidden("ASPage","advancedSearchPage", new {id="page"}) %>
        <div id="page-title"><img src="../../Content/more_images/Company.png" alt=""/>
             <div class="form-text-links">
                <span class="form-header-text">Search Results</span>
            </div>
            </div>
        <div class="field-data">
            <div id="vehicle-dialog"></div>
            <div id="filter-tools">
                <div id="companies-dd">
                    <span>Filter By Company:</span> <% Html.RenderAction("CompanyDropDown", "Company", new { type = "IntermediaryCompanies" }); %>
                </div>
                <div id="advanced-search">
                    <span>Search:</span>
                    <% if (ViewData["search"] != null) { %> <% Html.RenderAction("AdvancedSearchBar", "Search", new { search = ViewData["search"].ToString() }); %> <% } %>
                    <% else { %>  <% Html.RenderAction("AdvancedSearchBar", "Search"); %> <% } %>
                </div>
            </div>

            <div id="advanced-search-results">
                <div id="covernotes" class="search-result">
                    <div class="search-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Cover_Note.png" /><span class="category">Cover Notes:</span><span class="count"></span></div>
                    <div id="search-results-covernotes" class="results"></div>
                </div>
                <div id="certificates" class="search-result">
                    <div class="search-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Certificate.png" /><span class="category">Certificates:</span><span class="count"></span></div>
                    <div id="search-results-certificates" class="results"></div>
                </div>
                <div id="vehicles" class="search-result">
                    <div class="search-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Vehicle.png" /><span class="category">Vehicles:</span><span class="count"></span></div>
                    <div id="search-results-vehicles" class="results"></div>
                </div>
                <div id="policies" class="search-result">
                    <div class="search-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Policy.png" /><span class="category">Policies:</span><span class="count"></span></div>
                    <div id="search-results-policies" class="results"></div>
                </div>
                <div id="companies-search" class="search-result">
                    <div class="search-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Company.png" /><span class="category">Companies:</span><span class="count"></span></div>
                    <div id="search-results-companies" class="results"></div>
                </div>
                <div id="users" class="search-result">
                    <div class="search-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/User.png" /><span class="category">Users:</span><span class="count"></span></div>
                    <div id="search-results-users" class="results"></div>
                </div>
                <div id="people" class="search-result">
                    <div class="search-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/User.png" /><span class="category">Policy Holders (People):</span><span class="count"></span></div>
                    <div id="search-results-people" class="results"></div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/advanced-search.js" type="text/javascript"></script>
    <link href="../../Content/advanced-search.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/user-active-deactive.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/password.js" type="text/javascript"></script>
    <link href="../../Content/covernote-history.css" rel="stylesheet" type="text/css" />
    <!--<link href="../../Content/Policies.css" rel="stylesheet" type="text/css" />-->
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>



