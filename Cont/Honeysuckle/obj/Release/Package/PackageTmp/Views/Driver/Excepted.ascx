﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Driver>" %>

<div class="newdriverExcep driver-div">

    <% using (Html.BeginCollectionItem("Driver")) { %>
        <%: Html.ValidationSummary(true) %>

        <div class="insured-auth insured-data">
            <%: Html.HiddenFor(Model => Model.person.PersonID, new { @class = "exceptedId per dont-process" })%>
            
            <div class="line">
                <span class="label item">TRN:</span>
                <%: Html.TextBoxFor(model => model.person.TRN, new { @readonly = "readonly", @class = "perTRN item" })%>
            </div>
            
            <div class="line">
                <span class="driverName item"><%: ViewData["personName"] %></span>
            </div>
          
        </div>
        <div class="DriverFooter">
           <a href="#" class="remDriverExcep">Delete</a>  
        </div>
    <% } %>
</div>
