﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Driver>" %>

<div class="drivers">

    <% using (Html.BeginCollectionItem("insured")) {%>
        <%: Html.ValidationSummary(true) %>

        <%: Html.HiddenFor(Model => Model.person.PersonID, new { @class = "dont-process" })%>
        
        <div class="editor-label">
            <%: Html.LabelFor(model => model.person.TRN)%>
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => model.person.TRN, new { @readonly = "readonly", @class = "perTRN" })%>
            <%: Html.ValidationMessageFor(model => model.person.TRN)%>
        </div>

        <%: Html.ActionLink("View/Edit Driver Details", "GetDriverDetails", "Driver", new { id = Model.DriverId }, new { @class = "driverdetails" })  %>        
            
    <% } %>
    
</div>