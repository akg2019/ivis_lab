﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Phone>" %>

<div class="newphone">
    <% using (Html.BeginCollectionItem("phoneNums")) { %>
        <%: Html.ValidationSummary(true) %>

        <% if (Model.PhoneNumberId != 0 && (bool)ViewData["view"]) { %>
                 <div class="row-field">
                    <span class="pnum"> Phone #: </span> <%: Html.TextBoxFor(model => model.PhoneNumber, new { @required = "required", @readonly = "readonly", @class = "read-only telNum" })%> 
                    <span class="ext"> Ext: </span>  <%: Html.TextBoxFor(model => model.Extension, new { @readonly = "readonly", @class = "read-only extNum" })%> 
                        <%: Html.ValidationMessageFor(model => model.PhoneNumber)%> 
                        <%: Html.ValidationMessageFor(model => model.Extension)%>
                 </div>

                 <div class="primaryTelNum">
                    <span>Is Primary:</span> 
                    <%: Html.CheckBoxFor(model => model.primary, new { @class = "phonePrimary", @disabled = "disabled" })%>
                </div>
          <% } %>

          <% else { %>
                    <div class="row-field">
                        <span class="pnum"> Phone #: </span> <%: Html.TextBoxFor(model => model.PhoneNumber, new { @required = "required", @class = "telNum", @PlaceHolder = "(876)555-5555" })%> 
                        <span class="ext"> Ext: </span>  <%: Html.TextBoxFor(model => model.Extension, new { @class = "extNum" })%> 
                            <%: Html.ValidationMessageFor(model => model.PhoneNumber)%> 
                            <%: Html.ValidationMessageFor(model => model.Extension)%>
                    </div>

                    <div class="primaryTelNum">
                     <span>Is Primary:</span> 
                         <%: Html.CheckBoxFor(model => model.primary, new { @class = "phonePrimary"})%>
                    </div>

          <% } %>

    <% } %>
</div>
