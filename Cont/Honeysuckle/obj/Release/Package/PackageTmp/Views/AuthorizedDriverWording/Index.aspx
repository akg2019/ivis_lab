﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.AuthorizedDriverWording>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <%: Html.Hidden("company_id", int.Parse(ViewData["company_id"].ToString())) %>
    
    <div class="page">
        <div id="form-header">
            <img src="../../Content/more_images/Company.png"/>
            <div class="form-text-links">
                <span class="form-header-text">
                    Authorized Drivers Wordings
                </span>
                <div class="search-box inter-search">
                    <%= Html.TextBox("search-auth", null, new { @PlaceHolder = "Search [Wordings]", @autocomplete = "off", @class = "quick-search auth-wording" }) %>
                    <button type="button" class="quick-search-btn user-search-btn" onclick="clickSearchBtn()" ><img src="../../Content/more_images/Search_Icon.png" /></button>
                </div>
            </div>
        </div>
        <div class="field-data">
            <div class="ivis-table">
                <div class="menu">
                    <div class="item">
                        Wording
                    </div>
                </div>
                <div class="ivis-data">
                    <% for (int i = 0; i < Model.Count(); i++) { %>
                        <% if (i % 2 == 0) { %>
                            <div class="row data even auth">
                        <% } %>
                        <% else { %>
                            <div class="row data odd auth">
                        <% } %>
                            <%: Html.Hidden("h", Model.ElementAt(i).ID, new { @class = "word_id id" }) %>
                            <div class="item word">
                                <%: Model.ElementAt(i).wording %>
                            </div>
                            <div class="item functions">
                                <% if (Model.ElementAt(i).can_delete) { %>
                                    <div class="icon delete" title="Delete Authorized Driver Wording">
                                        <img src="../../Content/more_images/delete_icon.png" class="sprite" />        
                                    </div>
                                    <div class="icon edit" title="Edit Authorized Driver Wording">
                                        <img src="../../Content/more_images/edit_icon.png" class="sprite" />
                                    </div>
                                <% } %>
                            </div>
                        </div>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
    <div class="btnlinks">
        <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Authorized", "Create"))) { %>
            <button type="button" id="add_wording">New Authorized Driver Wording</button>
        <% } %>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/auth_wording_driving.js" type="text/javascript"></script>
    <link href="../../Content/auth_wording.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
