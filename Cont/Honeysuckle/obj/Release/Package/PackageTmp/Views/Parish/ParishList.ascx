﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Parish>>" %>

<% if((bool)ViewData["maplock"]){ %>
    <%: Html.DropDownList("parishes", new SelectList(Model.ToList(), "ID", "parish"), new { @class = "parishes", @disabled = "disabled" })%>
<% } %>
<% else if((bool)ViewData["mapping"]){ %>
    <%: Html.DropDownList("parishes", new SelectList(Model.ToList(), "ID", "parish"), new { @class = "parishes" })%>
<% } %>
<% else{ %>
    <%: Html.DropDownList("parishes", new SelectList(Model.ToList(), "ID", "parish"), new { @class = "parishes", @required = "required" })%>
<% } %>