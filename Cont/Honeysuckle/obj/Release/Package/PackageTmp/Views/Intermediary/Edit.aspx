﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Intermediary>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit Intermediary
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <%: Html.HiddenFor(model => model.ID, new { @class = "interid dont-process", @id = "intermediaryID" })%>

    <div id= "form-header"><img src="../../Content/more_images/Company.png" />
        <div class="form-text-links">
            <span class="form-header-text">Intermediary Edit</span>
            <div class="icon">
                <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
            </div>
        </div>
     </div>
    <div class="field-data">

        <% using (Html.BeginForm()) { %>
            <%: Html.ValidationSummary(true)%>
            <fieldset>
                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right down"></div>
                        Intermediary Company Details
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <%: Html.Hidden("ins", Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID, new { @id = "mycompid", @class = "dont-process" })%>
 
                        <div class="editor-label">
                            Company Name
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.company.CompanyName, new { @class = "read-only", @readonly = "readonly" })%>
                            <%: Html.HiddenFor(model => model.company.CompanyID, new { @class = "hidcid dont-process" })%>
                        </div>
                
                        <div class="check-div">
                            <div class="editor-label">
                                Enable
                            </div>
                            <div class="editor-field">
                                <%: Html.CheckBoxFor(model => model.enabled, new { @class = "int-check enablecheck" })%>
                                <%: Html.ValidationMessageFor(model => model.enabled)%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right"></div>
                        Certificates
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="check-div">
                            <div title="Check to give the intermediary the ability to print certificates" class="editor-label">
                                Can Print Certificates?
                            </div>
                            <div class="editor-field">
                                <%: Html.CheckBoxFor(model => model.PrintCertificate, new { @class = "int-check certcheck" })%>
                                <%: Html.ValidationMessageFor(model => model.PrintCertificate)%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="validate-section">
                    <div class="subheader"> 
                        <div class="arrow right"></div>
                        Cover Note Print Settings 
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="check-div">
                            <div title="Check to give the intermediary the ability to print cover notes" class="editor-label">
                                Can Print Cover Notes?
                            </div>
                            <div class="editor-field">
                                <%: Html.CheckBoxFor(model => model.PrintCoverNote, new { @class = "int-check cnotecheck" })%>
                                <%: Html.ValidationMessageFor(model => model.PrintCoverNote)%>
                            </div>
                        </div>
                
                        <div title="Set the the number of times cover notes can be printed" class="editor-label" id="print-label">
                            Print Limit
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.PrintLimit, new { @class = "printcount", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.PrintLimit)%>
                        </div>
                    </div>
                </div>
                
                <div class="btnlinks">
                    <% Html.RenderAction("BackBtn", "Back"); %>
                    <input type="submit" value="Save" class="savechanges inter-btn"/>
                </div>
            
            </fieldset>
        <% } %>
    </div>
        
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/Intermediaries.js" type="text/javascript"></script>
</asp:Content>

