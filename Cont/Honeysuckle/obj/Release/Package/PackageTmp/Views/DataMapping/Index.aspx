﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.MappingData>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    

    <%
        bool map_lock = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Mapping", "Lock"));
        //bool map_lock = true;
    %>

    <div id= "form-header">
        <img src="../../Content/more_images/User.png" alt=""/>
        <div class="form-text-links">
            <span class="form-header-text">
                Data Mapping Management
            </span>
        <div class="search-box index-search">
            <%= Html.TextBox("rm-search", null, new { @PlaceHolder = "Search [Data]", @class="quick-search mapping" })%>
            <button type="button" class="quick-search-btn" onclick="clickSearchBtn()" ><img src="../../Content/more_images/Search_Icon.png" /></button>
        </div>
    </div>
    </div>
          
    <div class="field-data">

        
        <div id="mapping-data" class="ivis-table">
            <div class="map menu">
                
                <div class="item tablename">
                    Table
                </div>
                <div class="item refid">
                    Correct Data <!-- to be renamed -->
                </div>
                <div class="item data">
                    Bad Data <!-- to be renamed -->
                </div>
            </div>
            <div class="mappingtabledata">
                <% int i = 0; %>
                <% foreach (var item in Model) { %>
                    <% i++; %>
                    <% if (i % 2 == 0) { %>
                        <div class="map row even data">
                    <% } %>
                    <% else { %>
                        <div class="map row odd data">
                    <% } %>
                        <%: Html.Hidden("mapid", item.id, new { @class = "hidemapid dont-process" })%>
                        <div class="item tablename">
                            <%: item.table %>
                        </div>
                        <div class="item refid">
                            <%: Html.Hidden("refid", item.refid, new { @class = "hiderefid dont-process" })%>
                            <%: Html.Hidden("correct", item.correct_data, new { @class = "dont-process hidden_cdata" }) %>
                            <% if (item.table == "parish"){ %>
                                <% Html.RenderAction("ParishList", "Parish", new { mapping = true, maplock = item.maplock }); %>
                            <% } %>
                            <% if (item.table == "wordings"){ %>
                                <% Html.RenderAction("GoodDataTags", "TemplateWording", new { maplock = item.maplock }); %>
                            <% } %>
                        </div>
                        <div class="item datamap">
                            <%: item.data %>
                        </div>
                        <div class="item lock">
                            <% if (map_lock){ %>
                                <% if (item.maplock)
                                   { %>
                                <button type="button" class="unlockbtn submitbutton" onclick="unlockData(this)"><span>Unlock Value</span></button>
                                <% } %>
                                <% else
                                   { %>
                                <button type="button" class="lockbtn submitbutton" onclick="lockData(this)"><span>Lock Value</span></button>
                                <% } %>
                            <% } %>
                        </div>
                    </div>
    
                <% } %>
            </div>
        </div>
       
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/mapping-index.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/mapping.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

