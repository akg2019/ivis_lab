﻿

//This function allows a user, to send a message to the company admnis of a company (under which a vehicle has a covernote)
function MessageCompAdmin(e) {

    var vehCoverNoteId = $('#coverNoteID').val();

    $("#SendMail").dialog({
        modal: true,
        autoOpen: false,
        width: 500,
        title: "Message Cover Note Approval Users",
        height: 450,
        buttons:
                   {
                       Send: function () {

                           var selected = [];
                           var sendto = document.getElementById("sendto");
                           for (var i = 0; i < sendto.options.length; i++) {
                               if (sendto.options[i].selected) {
                                   selected.push(sendto.options[i].value);
                               }
                           }

                           if (selected.length != 0) {

                                   var heading = $("#heading").val();
                                   var content = $("#content").val();

                                   var parms = { vehId: vehCoverNoteId, admins: selected, heading: heading, content: content };

                                   $.ajax({
                                       type: "POST",
                                       traditional: true,
                                       url: "/Notification/sendNoteToComp/",
                                       async: false,
                                       data: parms,
                                       dataType: "json",
                                       success: function (data) {

                                           if (data.result) {

                                               $("#SendMail").html("");
                                               $("#SendMail").dialog("close");
                                               $("<div>Message sent!</div>").dialog({
                                                   title: "Alert",
                                                   modal:true,
                                                   buttons:
                                               {
                                                   OK:
                                              function () {
                                                  $(this).dialog("close");
                                                  $(this).html("");
                                              }
                                               }

                                               });
                                           }
                                       }
                                   });

                                   //Removing the button
                                   $("#messageAdmins").remove();

                                   $.ajax({
                                       type: "POST",
                                       traditional: true,
                                       url: "/VehicleCovernote/PrintRequest/" + vehCoverNoteId,
                                       async: false,
                                       dataType: "json",
                                       success: function (data) { }
                                   });
                           } //END OF IF selected.length check-now to else
                               else {

                                   $("<div>You have not selected any recipients, so you cannot send this message. Please select at least one recipient</div>").dialog({
                                       title: "Alert",
                                       modal:true,
                                       buttons:
                                               {
                                                   OK:
                                              function () {
                                                  $(this).dialog("close");
                                                  $(this).html("");
                                              }
                                               }

                                   }); 

                           }//end of selected check

                       }, //END OF 'SEND' BUTTON

                       Cancel: function () {
                           $(this).dialog("close");
                           $(this).html("");
                       }

                   } //END OF BUTTONS

    });    // END OF JQUERY DIALOG

               $("#SendMail").load('/Notification/sendNote/' + vehCoverNoteId + '?type=' + 'CoverNote').dialog('open');

           }; //END OF 'sendTo' FUNCTION




           //This function allows a user, to send a message to the company admnis of a company (under which a vehicle has a covernote)
           function RequestCoverNoteApproval() {

               var vehCoverNoteId = $('#coverNoteID').val();

               $("#SendMail").dialog({
                   modal: true,
                   autoOpen: false,
                   width: 500,
                   height: 450,
                   title: 'Request Cover Note Approval',
                   buttons:
                   {
                       Send: function () {

                           var selected = [];
                           var sendto = document.getElementById("sendto");
                           for (var i = 0; i < sendto.options.length; i++) {
                               if (sendto.options[i].selected) {
                                   selected.push(sendto.options[i].value);
                               }
                           }

                           if (selected.length != 0) {

                           var heading = $("#heading").val();
                           var content = $("#content").val();

                           var parms = { vehId: vehCoverNoteId, admins: selected, heading: heading, content: content };

                           $.ajax({
                               type: "POST",
                               traditional: true,
                               url: "/Notification/sendNoteToComp/",
                               async: false,
                               data: parms,
                               dataType: "json",
                               success: function (data) {

                                   if (data.result) {

                                       $("#SendMail").html("");
                                       $("#SendMail").dialog("close");
                                       $("<div>Message sent!</div>").dialog(
                                       {
                                          title: "Alert",
                                          buttons:
                                          {
                                               OK:
                                          function () {
                                              $(this).dialog("close");
                                              $(this).html("");
                                             }
                                           }

                                       });
                                   }
                               }
                      });
                            } //END OF IF selected.length check-now to else
                               else {

                                   $("<div>You have not selected any recipients, so you cannot send this message. Please select at least one recipient</div>").dialog({
                                       title: "Alert",
                                       modal:true,
                                       buttons:
                                               {
                                                   OK: function () {
                                                  $(this).dialog("close");
                                                  $(this).html("");
                                              }
                                               }

                                   }); 

                           }//end of selected check

                       }, //END OF 'SEND' BUTTON

                       Cancel: function () {
                           $(this).dialog("close");
                           $(this).html("");
                       }

                   } //END OF BUTTONS

               });  // END OF JQUERY DIALOG

               $("#SendMail").load('/Notification/sendNote/' + vehCoverNoteId + '?type=' + 'ApproveCoverNote').dialog('open');

           }; //END OF 'sendTo' FUNCTION


           $(function () {
               $(".editCoverNote").on('click', function () {
                   var id = $("#coverNoteID").val();
                   window.location.replace('/VehicleCoverNote/Edit/' + id);
               });
           });