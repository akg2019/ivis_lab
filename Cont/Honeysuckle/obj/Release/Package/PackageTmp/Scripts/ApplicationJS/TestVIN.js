﻿var vinFocus = false;
var vinSet = false;

$(function () {
    if ($("#vin").val().trim().length > 0) vinSet = true;
});


$(function () {
    $('#editVin').click(function () {

        $('<div>You are about to edit this vehicle\'s VIN. Are you sure you want to do this?</div>').dialog({
            title: 'Edit VIN Dialog',
            resizable: false,
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    $("#vin").prop('readonly', false);
                    $("#vin").toggleClass('read-only');
                    $("#vin").focus();
                    vinFocus = true;

                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    });
});

$(function () {
    $("#vin").blur(function () {
        if (vinFocus && $("#vin").val().trim() != '') {
            var test = $("#vin").val();
            if (test.trim().length > 0) {
                var result = false;
                $.getJSON("/Vehicle/TestVin/?vin=" + test.trim() + "&id=" + $('#vehicle_base_id').val(), function (data) {
                    if (!data.error)
                    //something exists

                        if (data.vehicle) {
                            //the vehicle exists in the system already
                            /*
                            //the vehicle is already under a policy in the company
                            if (data.comp) {
                            //the vehicle exists in the system already
                            $("<div>The Vehicle you have entered already exists in the system, under a policy in your company. The existing policy has to be deleted to proceed.</div>").dialog({
                            modal: true,
                            buttons: {
                            Ok: function () {
                            window.location.replace("/Vehicle/Details/" + data.vid);
                            },
                            Cancel: function () {
                            $("#vin").val("");
                            $(this).dialog("close");
                            }
                            }
                            });
                            }

                            else {
                            $("<div>The Vehicle you have entered already exists in the system. Would you like to view that vehicle's details?</div>").dialog({
                            modal: true,
                            title: "Alert",
                            buttons: {
                            Ok: function () {
                            window.location.replace("/Vehicle/Details/" + data.vid);
                            },
                            Cancel: function () {
                            $("#vin").val("");
                            $(this).dialog("close");
                            }
                            }
                            });
                            }*/


                            $("<div>The VIN already exists for another vehicle. The Vehicle VIN must be unique.</div>").dialog({
                                modal: true,
                                title: "VIN Exists",
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog("close");

                                        $("#vin").val("");
                                        $("#vin").focus();
                                        $("#vin").trigger('keyup');
                                        //vinFocus = false;
                                    }
                                }
                            });



                        }


                        else {
                            //VIN FREE
                            if (test.trim().length != 0) vinSet = true;
                            vinFocus = false;
                            $("#vin").toggleClass('read-only');
                            $("#vin").prop('readonly', true);
                        }


                    else {
                        //ERROR
                        $("<div>" + data.message + "</div>").dialog({
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $("#vin").val("");
                                    $(this).dialog("close");
                                }
                            }
                        });

                    }
                });
                //vinFocus = false; //reset variable so that things dont go wonky
            }
            else {
                //if (vinSet) {
                  //  vinSet = false;
                //}

            }
        }
        else if ($('#vin').val().trim() == '') {
            vinFocus = false;
            $("#vin").addClass('read-only');
            $("#vin").prop('readonly', true);
        }

    });
});

/*
$(function () {
    $("#make,#vehiclemodel,#modeltype,#bodytype,#extension,#vehicleRegNum,#colour,#cylinders,#engineNo,#CengineModified,#engineType,#mileage,#fitnessExpiry,#hpccUnit,#refno,#regLocation,#roofType,#seating,#seatingCert,#tonnage,#vehicleYear,#handDriveType,#tranType,#username,#fname,#mname,#lname,#RoadNo,#RoadName,#RoadType,#ZipCode,#CityName,#CountryName").focus(function () {
        if (!vinSet) {
            $(this).blur();
            $("<div>Please fill the VIN first</div>").dialog({
                modal: true,
                buttons: {
                    Ok: function () {
                        $("#vin").val("");
                        $(this).dialog("close");
                    }
                }
            });
         }
    })
});
*/