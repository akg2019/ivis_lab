﻿var compIAJIDFocus = false;
var compIAJIDSet = false;
var IAJID1 = $("#compIAJID").val();

$(function () {
    if (typeof $("#compIAJID").val() != 'undefined')
    if ($("#compIAJID").val().trim().length > 0) {
        compIAJIDSet = true;
    }
});


$(function () {
    $("#compIAJID").focus(function () {
        compIAJIDFocus = true;
    })
}); 


$(function () {
    $("#compIAJID").blur(function () {
        if (compIAJIDFocus) {
            var result = false;
            var regexIAJID = /^[0-9]+$/; //regex to ensure that the IAJID is an integer before checking for its existence in the system

            if ($("#compIAJID").val() == '' || !regexIAJID.test($("#compIAJID").val())) {
                alert('Please enter a valid company IAJID- Numbers Only.');
                return false;
            }

            else
                $.getJSON("/Company/GetCompanyByIAJID/" + $("#compIAJID").val(), function (data) {
                    if (!data.error) {
                        if (data.result) {
                            //company already exists

                            $("<div>The IAJID you have entered already exists in the system. The company will be added to the selected list.</div>").dialog({
                                modal: true,
                                title: "Alert",
                                buttons: {
                                    Ok: function () {
                                        $(".ui-dialog-content").dialog("close");

                                        $.getJSON("/Company/ReturnCompanyByIAJID/?iajid=" + $("#compIAJID").val(), function (data) {
                                            if (!data.error) {
                                                if (data.result) {

                                                    $("#company-modal").load('/Company/companyList/' + $("#compid").val(), function () {

                                                      if ($(document).find(".company-row").length <= 50)
                                                          $(document).find(".company-row").show();  

                                                    }).dialog('open');
                                                    AddCompanySelected(data.cid, 1);

                                                }
                                            }
                                        });
                                    },
                                    Cancel: function () {
                                        $("#compIAJID").val("");
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        }

                        else {
                            //IAJID Not in the system yet
                            if (IAJID1.trim().length != 0) compIAJIDSet = true;

                        }
                    }
                    else {
                        //ERROR
                        $("<div>" + data.message + "</div>").dialog({
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $("#compIAJID").val("");
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }

                });
            compIAJIDFocus = false;
        }
    })
});


/*$(function () {
$("#username,#fname,#mname,#lname,#RoadNo,#RoadName,#RoadType,#ZipCode,#CityName,#CountryName").focus(function () {
if (!trnSet) {
$(this).blur();
$("<div>Please fill the Company IAJID first</div>").dialog();
}
})
}); */