﻿var chassisFocus = false;
var chassisSet = false;
var TRNmessage = "<div>You are about to change the TRN of the Main Driver of the Vehicle. Are you certain you want to do this?</div>";
var trnEdit = false;
var alreadyok = false;

function RemoveEmails() {
    $(".newemail").remove();
};

function GetPersonsEmails(id) {
    $.getJSON("/Email/GetPersonEmails/" + id, function (data) {
        var obj = jQuery.parseJSON(data);
        for (var i = 0; i < obj.length; i++) {
            $.ajax({
                url: '/Email/NewEmail/' + obj[i].emailID,
                cache: false,
                success: function (html) { $("#email-container").append(html); $(".emailprimary").attr("disabled", true); $(".rmemail").hide(); }
            });
            return false;
        }
    });

};

$(function () {

    if ($("#RegNoPrompt").text().trim().length != 0) {

        $("#RegNoPrompt").dialog({
            modal: true,
            autoOpen: false,
            title: "Alert!",
            buttons: {
                OK: function () {
                    $('#RegNoPrompt').dialog('close');
                    $('#RegNoPrompt').html("");
                }
            }
        });

        $('#RegNoPrompt').dialog('open');
    }
});

$(function () {
    if (typeof $('#trn').val() != 'undefined') {
        if ($("#trn").val().trim().length > 0) trnSet = true;
    }
    else {
        if ($('#person-trn-not-available').val().trim().length > 0) trnSet = true;
    }

});


$(function () {
    $("#trn").focus(function () {
        if(! $("#trn").hasClass('read-only'))
            trnFocus = true;
    })
});


$(function () {
    var usage = $("#usageID").val();
    if (usage != 0) {
        $(".usage_dropdown").children().each(function () {
            if ($(this).val() == usage) {
                $(this).prop('selected', true);
                return;
            }
        });
    }
});

$(function () {
    $("#trn").prop('readonly', true);
    $("#trn").addClass('read-only');
    $(".emailprimary").attr("disabled", true);
    $(".rmemail").hide();
    $(".addemail").hide();

    $("#fname").prop('readonly', true);
    $("#mname").prop('readonly', true);
    $("#lname").prop('readonly', true);


    $("#fname").addClass('read-only');
    $("#mname").addClass('read-only');
    $("#lname").addClass('read-only');

    $("#RoadNumber").prop('readonly', true);
    $("#roadname").prop('readonly', true);
    $("#aptNumber").prop('readonly', true);
    $("#roadtype").prop('readonly', true);
    $("#zipcode").prop('readonly', true);
    $("#parishes").prop('readonly', true);
    $(".parishes").prop('disabled', true);
    $("#city").prop('readonly', true);
    $("#country").prop('readonly', true);

    $("#editTRN").on('click', function () {

        if (trnEdit == false) //So that the modal doesn't keep popping up
            $(TRNmessage).dialog({
                title: 'Edit TRN',
                resizable: false,
                modal: true,
                buttons: {
                    Ok: function () {
                        $("#modify_btn").prop('disabled', true);
                        $("#trn").prop('readonly', false);
                        $("#trn").removeClass('read-only');
                        $("#trn").focus();
                        trnFocus = true;
                        origTRN = $("#trn").val();
                        trnEdit = true;
                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
    });
});

$(function () {

    var fitnessExpiry = new Date($("#hidden_fitnessExpiry").val());
    $('#edit_fitnessExpiry').val(fitnessExpiry.dateFormat('d M Y'));

});

function fitnessPicker() {
    $('#edit_fitnessExpiry').datetimepicker({
        id: 'edit_fitExpiry',
        format: 'd M Y',
        step: 5,
        lang: 'en-GB',
        timepicker: false,
        closeOnDateSelect: true,
        onSelectDate: function (ct) {
            $("#edit_fitnessExpiry").val(ct.dateFormat('d M Y'));
        }
    });
}

$(function () {
    $("#trn,#person-trn-not-available").blur(function () {
        if (trnEdit)
            if (trnFocus) {
                if (typeof $('#trn').val() != 'undefined')
                    var test = $("#trn").val();
                else
                    var test = $('#person-trn-not-available').val();
                var result = false;
                if (test.trim() == "" && (typeof $('#trn').val() != 'undefined')) {
                    $("<div>You are leaving the TRN field empty. This means no main driver will be assigned. Are sure you want to do this?</div>").dialog({
                        modal: true,
                        title: 'Alert!',
                        buttons: {
                            Yes: function () {
                                $(this).dialog("close");
                                $("#RoadNumber,#aptNumber, #roadname,#city,#roadtype,#country,#fname,#mname,#lname, #parish,#country").prop('readonly', true);
                                $("#RoadNumber, #aptNumber, #roadname,#city,#roadtype,#country,#fname,#mname,#lname, #parish,.parishes,#country").val("");

                                $("#fname").addClass('driver-fname');
                                $("#lname").addClass('driver-lname');
                                $("#city").addClass('vehicle-edit');
                                $("#country").addClass('vehicle-edit');
                                $('.parishes').parents('.editor-field').addClass('vehicle-edit');


                                $("#fname,#lname,#city,.parishes,#country").trigger('change');

                                $(".parishes").prop('disabled', true);
                                $("#mainDriverId").val(0);

                            },
                            No: function () {
                                $(this).dialog("close");
                                $("#trn").focus();
                            }
                        }
                    });
                }
                else {
                    $.getJSON("/Vehicle/GetUserByTRN/" + test.trim(), function (data) {
                        if (!data.error) {
                            if (data.result) {
                                //something exists
                                if (data.person) {
                                    //There is a person who's not a user yet
                                    alreadyok = false;
                                    $("<div>The TRN you have entered already exists in the system. Would you like to have the Person's Details populate the form now?</div>").dialog({
                                        modal: true,
                                        title: "Alert",
                                        buttons: {
                                            Ok: function () {
                                                $("#mainDriverId").val(data.personid);
                                                $("#fname").val(data.fname);
                                                $("#mname").val(data.mname);
                                                $("#lname").val(data.lname);
                                                $("#roadno").val(data.roadno);
                                                $("#aptNumber").val(data.aptNumber);
                                                $("#roadname").val(data.roadname);
                                                $("#roadtype").val(data.roadtype);
                                                $("#zipcode").val(data.zipcode);
                                                $("#city").val(data.city);
                                                $("#country").val(data.country);
                                                $("#personid").val(data.personid);
                                                $(".parishes").children().each(function () {
                                                    if ($(this).val() == data.parishid) {
                                                        $(this).prop('selected', true);
                                                        return;
                                                    }
                                                });
                                                trnSet = true;

                                                RemoveEmails();
                                                GetPersonsEmails(data.personid);
                                                $(this).dialog("close");
                                                $("#fname,#lname,#city,.parishes,#country").trigger('change');
                                            },
                                            Cancel: function () {
                                                $("#trn").val("");
                                                $(this).dialog("close");
                                            }
                                        }
                                    });
                                }
                                else {
                                    if (data.user) {
                                        //There is a user
                                        alreadyok = false;
                                        $("<div>The TRN you have entered is already listed to a current user in the system.  Would you like to have the Person's Details populate the form now?</div>").dialog({
                                            modal: true,
                                            buttons: {
                                                Ok: function () {
                                                    $("#mainDriverId").val(data.personid);
                                                    $("#fname").val(data.fname);
                                                    $("#mname").val(data.mname);
                                                    $("#lname").val(data.lname);
                                                    $("#roadno").val(data.roadno);
                                                    $("#aptNumber").val(data.aptNumber);
                                                    $("#roadname").val(data.roadname);
                                                    $("#roadtype").val(data.roadtype);
                                                    $("#zipcode").val(data.zipcode);
                                                    $("#city").val(data.city);
                                                    $("#country").val(data.country);
                                                    $("#personid").val(data.personid);
                                                    $(".parishes").children().each(function () {
                                                        if ($(this).val() == data.parishid) {
                                                            $(this).prop('selected', true);
                                                            return;
                                                        }
                                                    });
                                                    RemoveEmails();
                                                    GetPersonsEmails(data.personid);
                                                    trnSet = true;

                                                    $(this).dialog("close");
                                                    $("#fname,#lname,#city,.parishes,#country").trigger('change');
                                                },
                                                Cancel: function () {
                                                    $("#trn").val("");
                                                    $(this).dialog("close");
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                            else {
                                //TRN FREE
                                if (test.trim().length != 0) trnSet = true;

                                //The Data is only cleared out if the infromation currently in the form belongs to an existing person in the system
                                if (!alreadyok) {
                                    $("#RoadNumber,#aptNumber, #roadname,#city,#roadtype,#country,#fname,#mname,#lname, #parish,#country").prop('readonly', false);
                                    $("#RoadNumber, #aptNumber, #roadname,#city,#roadtype,#country,#fname,#mname,#lname, #parish,.parishes,#country").val("");

                                    $("#fname").removeClass('read-only');
                                    $("#mname").removeClass('read-only');
                                    $("#lname").removeClass('read-only');

                                    $("#fname").removeClass('driver-fname');
                                    $("#lname").removeClass('driver-lname');
                                    $("#city").removeClass('vehicle-edit');
                                    $("#country").removeClass('vehicle-edit');
                                    $('.parishes').parents('.editor-field').removeClass('vehicle-edit');


                                    $("#fname,#lname,#city,.parishes,#country").trigger('change');
                                }
                                $(".parishes").prop('disabled', false);
                                $(".addemail").show();
                                //$("#country").val("Jamaica");
                                $("#mainDriverId").val(0);
                                RemoveEmails();

                                alreadyok = true; //Set this to true, so that it doesn't clear if another available TRN is entered

                            }
                        }
                        else {
                            //ERROR
                            $("<div>" + data.message + "</div>").dialog({
                                modal: true,
                                buttons: {
                                    Ok: function () {
                                        $("#trn").val("");
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        }

                    });
                    trnFocus = false;
                } //end of trn empty check
            }
    });
});

$(function () {
    $('html').on('change', '#parishes', function () {
        $(this).find('option').each(function () {
            if ($(this).is(':selected')) {
                $('#parish').val($(this).val());
                return;
            }
        });
    });
});

$(function () {

    if ($("#chassis").val().trim().length > 0) chassisSet = true;


    $("#editChassis").on("click", function () {

        $('<div>You are about to edit this vehicle\'s chassis Number. Are you sure you want to do this?</div>').dialog({
            title: 'Edit Chassis Dialog',
            resizable: false,
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    $("#chassis").prop('readonly', false);
                    $("#chassis").toggleClass('read-only');
                    $("#chassis").focus();
                    chassisFocus = true;

                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });

    });

    $("#chassis").blur(function () {
        if (chassisFocus) {
            var test = $("#chassis").val();
            if (test.trim().length > 0) {
                var result = false;
                $.getJSON("/Vehicle/GetVehicleByChassis/?id=" + test.trim() + "&vehID=" + $('#vehicle_base_id').val(), function (data) {
                    if (!data.error) {
                        //something exists

                        if (data.vehicle) {
                            $("<div>The Chassis Number already exists for another vehicle. The vehicle chassis number must be unique.</div>").dialog({
                                modal: true,
                                title: "Chassis Exists",
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog("close");

                                        $("#chassis").val("");
                                        $("#chassis").focus();
                                        $("#chassis").trigger('keyup');
                                        //vinFocus = false;
                                    }
                                }
                            });

                        }
                        else {
                            if (test.trim().length != 0) vinSet = true;
                            chassisFocus = false;
                            $("#chassis").toggleClass('read-only');
                            $("#chassis").prop('readonly', true);
                        }
                    }
                    else {
                        //ERROR
                        $("<div>" + data.message + "</div>").dialog({
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $("#vin").val("");
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }
                });

            }
            else {
                // if (chassisSet)
                //chassisSet = false;
            }
        }
    });

});

$(function () {
    $('#editMake').click(function () {
        $('<div>You are about to edit this vehicle\'s make. Are you sure you want to do this?</div>').dialog({
            title: 'Edit Make Dialog',
            resizable: false,
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    $("#make").prop('readonly', false);
                    $("#make").toggleClass('read-only');
                    $("#make").focus();

                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    });

    $('#editModel').click(function () {
        $('<div>You are about to edit this vehicle\'s model. Are you sure you want to do this?</div>').dialog({
            title: 'Edit Model Dialog',
            resizable: false,
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    $("#vehiclemodel").prop('readonly', false);
                    $("#vehiclemodel").toggleClass('read-only');
                    $("#vehiclemodel").focus();

                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    });

    $('#editYear').click(function () {
        $('<div>You are about to edit this vehicle\'s year. Are you sure you want to do this?</div>').dialog({
            title: 'Edit Year Dialog',
            resizable: false,
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    $("#vehicleYear").prop('readonly', false);
                    $("#vehicleYear").toggleClass('read-only');
                    $("#vehicleYear").focus();

                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    });

    $('#editFitness').click(function () {
        $('<div>You are about to edit this vehicle\'s year. Are you sure you want to do this?</div>').dialog({
            title: 'Edit Year Dialog',
            resizable: false,
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    //$("#edit_fitnessExpiry").prop('readonly', false);
                    $("#edit_fitnessExpiry").removeClass('read-only');
                    fitnessPicker();
                    // $("#hidden_fitnessExpiry").focus();

                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    });


});



function build_edit_message(type) {
    return "Are you certain you want to edit " + type + "?";
}