﻿$(function () {
    $("html").on('click','.title', function () {
        if ($(this).parents(".responses").find(".data").hasClass("slidedown")) $(this).parents(".responses").find(".data").slideUp();
        else $(this).parents(".responses").find(".data").slideDown();
        $(this).parents(".responses").find(".data").toggleClass("slidedown");
    });
});

