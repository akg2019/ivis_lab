﻿$(function () {
    var is_safari = (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0);
    if (is_safari) {
        $('.submitbutton,.saveButton,#Submit2').click(function () {
            var required_filled = isFormValid();
            if (!required_filled) {
                alert("Please fill in all required fields.");
            }
            return required_filled;
        });
    }
});

function isFormValid() {
    var result = true;
    var count = $("input[required]").length;
    $("input[required]").each(function (index) {
        if (!$(this).val()) {
            result = false;
            return false;
        }
        if (index == count - 1) {
            result = true;
            return false;
        }
    });
    return result;
}