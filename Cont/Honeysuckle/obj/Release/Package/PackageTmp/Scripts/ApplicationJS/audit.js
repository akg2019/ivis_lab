﻿var processing = "<div class=\"dataprocessing\"><div class=\"rect1\"></div><div class=\"rect2\"></div><div class=\"rect3\"></div><div class=\"rect4\"></div><div class=\"rect5\"></div></div>";
$(function () {

    add_processing();
    $(".data-content").hide();
    $("#data-results-certificates").html("");

    $(".data-headers").on('click', function (e) {

        if ($(this).parent().children(".data-content").is(':visible')) {
            //$(this).parent().children(".data").css("margin-bottom", "20px");
            $(this).parent().children(".data-content").slideUp(400);
            $(this).parents(".data-result").find(".arrow").toggleClass('down');
            //$(this).parent().children(".data-content").find(".row").hide();
        }
        else {
            $(".data-content").each(function () {
                //$(this).parent().children(".data").css("margin-bottom", "20px");
                if ($(this).is(':visible')) $(this).slideUp(400);
                if ($(this).parents(".data-result").find(".arrow").hasClass("down")) $(this).parents(".data-result").find(".arrow").toggleClass('down');

            });
            // $(this).parent().children(".data").css("margin-bottom", "20px");
            $(this).parent().children(".data-content").slideDown(400);
            // $(this).parent().children(".data-content").find('.data.row').slideDown(400);
            $(this).parents(".data-result").find(".arrow").toggleClass('down');

            //Add the perfect scroll bar to the dropped down search element
            $(this).parent().find('.scroll-table').perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });
        }

    });

   $.ajax({
            url: '/Audit/ShortCertificate/',
            cache: false,
            async: false,
            success: function (html) {
                if (html == "") {
                    $("#certificates").children(".data-headers").children(".count").text("No Records Found");
                    $("#data-results-certificates").html("");

                }
                else {
                    $("#certificates").children(".data-headers").children(".count").text($($(html)[0]).val() + " Records");
                    //VCcountRecords = $("#certificates").children(".data-headers").children(".count").text();
                    $("#data-results-certificates").html(html);
                }

                if ($("#data-results-certificates").is(":visible")) $("#data-results-certificates").find('.scroll-table').perfectScrollbar({
                    wheelSpeed: 0.3,
                    wheelPropagation: true,
                    minScrollbarLength: 10,
                    suppressScrollX: true
                });
            },
            error: function () {
            }
        });

        $.ajax({
            url: '/Audit/ShortCoverNote/',
            cache: false,
            async: false,
            success: function (html) {
                if (html == "") {
                    $("#covernotes").children(".data-headers").children(".count").text("No Records Found");
                    $("#data-results-covernotes").html("");

                }
                else {
                    $("#covernotes").children(".data-headers").children(".count").text($($(html)[0]).val() + " Records");
                    //VCcountRecords = $("#certificates").children(".data-headers").children(".count").text();
                    $("#data-results-covernotes").html(html);
                }

                if ($("#data-results-covernotes").is(":visible")) $("#data-results-covernotes").find('.scroll-table').perfectScrollbar({
                    wheelSpeed: 0.3,
                    wheelPropagation: true,
                    minScrollbarLength: 10,
                    suppressScrollX: true
                });
            },
            error: function () {
            }
        });

        $.ajax({
            url: '/Audit/ShortPolicy/',
            cache: false,
            async: false,
            success: function (html) {
                if (html == "") {
                    $("#policies").children(".data-headers").children(".count").text("No Records Found");
                    $("#data-results-policies").html("");

                }
                else {
                    $("#policies").children(".data-headers").children(".count").text($($(html)[0]).val() + " Records");
                    //VCcountRecords = $("#certificates").children(".data-headers").children(".count").text();
                    $("#data-results-policies").html(html);
                }

                if ($("#data-results-policies").is(":visible")) $("#data-results-policies").find('.scroll-table').perfectScrollbar({
                    wheelSpeed: 0.3,
                    wheelPropagation: true,
                    minScrollbarLength: 10,
                    suppressScrollX: true
                });
            },
            error: function () {
            }
        });
        /*
        $.ajax({
            url: '/Audit/ShortCompany/',
            cache: false,
            async: false,
            success: function (html) {
                if (html == "") {
                    $("#companies").children(".data-headers").children(".count").text("No Records Found");
                    $("#data-results-companies").html("");

                }
                else {
                    $("#companies").children(".data-headers").children(".count").text($($(html)[0]).val() + " Records");
                    //VCcountRecords = $("#certificates").children(".data-headers").children(".count").text();
                    $("#data-results-companies").html(html);
                }

                if ($("#data-results-companies").is(":visible")) $("#data-results-companies").find('.scroll-table').perfectScrollbar({
                    wheelSpeed: 0.3,
                    wheelPropagation: true,
                    minScrollbarLength: 10,
                    suppressScrollX: true
                });
            },
            error: function () {
            }
        });

        $.ajax({
            url: '/Audit/ShortPeople/',
            cache: false,
            async: false,
            success: function (html) {
                if (html == "") {
                    $("#people").children(".data-headers").children(".count").text("No Records Found");
                    $("#data-results-people").html("");

                }
                else {
                    $("#people").children(".data-headers").children(".count").text($($(html)[0]).val() + " Records");
                    //VCcountRecords = $("#certificates").children(".data-headers").children(".count").text();
                    $("#data-results-people").html(html);
                }

                if ($("#data-results-people").is(":visible")) $("#data-results-people").find('.scroll-table').perfectScrollbar({
                    wheelSpeed: 0.3,
                    wheelPropagation: true,
                    minScrollbarLength: 10,
                    suppressScrollX: true
                });
            },
            error: function () {
            }
        });

        $.ajax({
            url: '/Audit/ShortVehicle/',
            cache: false,
            async: false,
            success: function (html) {
                if (html == "") {
                    $("#vehicles").children(".data-headers").children(".count").text("No Records Found");
                    $("#data-results-vehicles").html("");

                }
                else {
                    $("#vehicles").children(".data-headers").children(".count").text($($(html)[0]).val() + " Records");
                    //VCcountRecords = $("#certificates").children(".data-headers").children(".count").text();
                    $("#data-results-vehicles").html(html);
                }

                if ($("#data-results-vehicles").is(":visible")) $("#data-results-vehicles").find('.scroll-table').perfectScrollbar({
                    wheelSpeed: 0.3,
                    wheelPropagation: true,
                    minScrollbarLength: 10,
                    suppressScrollX: true
                });
            },
            error: function () {
            }
        });*/

    });

    
   

    function add_processing() {
        $(".data-result").find(".subheader").find(".count").html("");
        $(".data-result .subheader .count").append(processing);
    }
    