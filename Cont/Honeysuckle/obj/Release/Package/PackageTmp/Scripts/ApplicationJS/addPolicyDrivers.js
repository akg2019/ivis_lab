﻿var policyid = 0; var thisVeh = ""; var newChassis = ""; var polcyIdentity = ""; var DriverType = ""; var oldDialog = "";
var thisVehMainDriv;
var noInsured = "<div class=\"rowNone\">There Are No Drivers. Please add one. </div>";
var InsuredHidden = "<div class=\"rowNone\">Please search the system for existing drivers, or add a new insured. </div>";

$(function(){
    $(".PolVehicle").each(function(){
        updateCount($(this));
    });
    $(".vehicle-row").hide();
    clickme($(".PolVehicle:First").children(".vehicle-row"));
});

///////////////////////////////////////////////////////////////////////

var selected = [];
var notselected = [];
var idNumbers = [];
var shownPeople = [];
var checkAdded = 0;
var scrollHeight;


//Search when enter-button is hit
$(document).on("keyup", ".modalSearch", function (event) {
    if (event.which == 13) {
        var text = $(".modalSearch").val();
        searchData(text);
    }
});

    
function updateView() {
    for (var x = 0; x < selected.length; x++) {
        $("#selected-insured").append(selected[x]);
    }
    
    $("#insureds").children(".insured-row").each(function () {
        //If the person is already on display, in the select box, then it doesn't need to be shown in the search area again
        testIfAdded($($(this).children(".insured-check").children(".insuredcheck")).prop('id'));
        if (checkAdded == 1) $(this).hide();
        checkAdded = 0;
    });

    //Resizing the insured list div, so the scroll works
   // $("#insureds").height(scrollHeight);

    if ($("#insureds").height() + $("#selected-insured").height() < 380) 
        $("#insureds").height($("#insureds").height() + 20);

    else if ($("#insureds").height() + $("#selected-insured").height() >= 380) 
             $("#insureds").height($("#insureds").height() - 20);

}

function testIfAdded(id) {
    for (var z = 0; z < idNumbers.length; z++) {
        if (idNumbers[z] == id) checkAdded = 1;
    }
}

function removeELement(id) { //This function removes the element from the arrays
    for (var y = 0; y < selected.length; y++) {
        if (idNumbers[y] == id) {
            selected.splice(y, 1);
            idNumbers.splice(y, 1);
           // scrollHeight += 20;
        }
    }
    updateView();
}


function AddPersonSelected(id, addNew) {
    var err = "<div>There was an error with adding the person. Please try again. If the problem persists, please contact your administrator.</div>"

    //Removing the div that displayes the message (of either no driver being present, or driver count exceeding 50)
    if (typeof $(".rowNone").val() !== 'undefined') $(".rowNone").remove();

    $.ajax({
        type: "POST",
        url: '/People/PersonListItemSelected/' + id + '?insured=' + false,
        cache: false,
        success: function (data) {
            testIfAdded(id);
            if (checkAdded != 1) {
                selected.push($(data));
                idNumbers.push(id);
                //if (addNew != 1) scrollHeight -= 20;
            }
            updateView();
        },
        error: function () {
            $(err).dialog({
                title: "Error",
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog('close');
                    }
                }
            })
        }
    });   //END OF AJAX CALL
    checkAdded = 0; //Reset the variable
}

function changeCheckBox(e) {

    //Stores search value, as it breaks if the value is not stored before the div is manipulated
    var searchValue = $(e).parents("#insured").find(".modalSearch").val();

    $(e).parents(".insured-row").remove();

    if ($(e).parents(".insured-row").hasClass("selected")) {
        var alreadyAdded = false;

        $("#insureds").children(".insured-row").each(function () {
            if ($(this).find(".insuredcheck").prop('id') == e.id) {
                alreadyAdded = true;
                return false;
            }
        });

        if (!alreadyAdded)  $("#insureds").append($(e).parents(".insured-row").toggleClass("selected"));
        removeELement(e.id);
    }
    else {
        AddPersonSelected(e.id, 0);
    }
    searchData(searchValue);
}

function searchPeople(e) {
    var filter = $(e).parents("#insured").find(".modalSearch").val();
    searchData(filter);
}

function searchData(filter) {
  $(".rowNone").remove();
    if (filter.trim().length == 0) {

       //ONLY THE MAIN DRIVER AND THE POLICY INSUREDS ARE SHOWN 
            $("#insureds").children(".insured-row").each(function () {

               var ShowThisInsured = false;

               if (thisVehMainDriv !== "" &&  thisVehMainDriv === $(this).find(".insuredcheck").prop('id'))
                   ShowThisInsured = true;

               for (var z = 0; z < shownPeople.length; z++) {
                      if ($(this).find(".insuredcheck").prop('id') == shownPeople[z]) 
                      ShowThisInsured = true;
                   }   

                   if(ShowThisInsured=== true)
                   {
                      $(this).show();

                        if ($("#insureds").height() + $("#selected-insured").height() < 380) 
                            $("#insureds").height($("#insureds").height() + 20);
                    }
                   else 
                      $(this).hide();

                   //Reset the variable
                   ShowThisInsured = false;
            });
        }
    else {
        
        if (typeof $(".rowNone").val() !== 'undefined') $(".rowNone").remove();
        $("#insureds").children(".insured-row").each(function () {
            //If the person is already on display, in the select box, then it doesn't need to be shown in the search area again
            testIfAdded($($(this).children(".insured-check").children(".insuredcheck")).prop('id'));

            if (checkAdded != 1) {
                if (($(this).children(".insured-trn").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".insured-fname").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".insured-lname").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1)) {
                    $(this).show();

                   if ($("#insureds").height() + $("#selected-insured").height() < 380) 
                       $("#insureds").height($("#insureds").height() + 20);

                }
                else {
                    $(this).hide();
                }
            }
            checkAdded = 0;
        });

    }// End of else

   redrawList();

     if( $("#insureds").height() == 0 && !$($(".insured-row")[0]).is(':hidden') )
         $(document).find("#insureds").prepend(noInsured);

     else if(! $('.insured-row').is(':visible') )
         $(document).find("#insureds").prepend(InsuredHidden);
       
}


//////////////////////////////////////////////////////////////////////

$(function () {

   //STORING THE MAIN DRIVER ID, AND THE INSUREDS ON THE POLICY IN AN ARRAY
    $(".PeopleInsuredList").each(function () {
         shownPeople.push($(this).val());
    });

    $("#Done").click(function () {

      /*  policyid = $("#policyid").val();

        $(".insured-auth").each(function () {

            var PerId = $(this).children("#per").val();
            var Relation = $(this).find(".rel").val();
            var VehChassisNum = $(this).parents(".PolVehicle").find('.chassis').html().trim(); 

            var values = { chassis: VehChassisNum, per: PerId, rel: Relation, pol: policyid }

            $.ajax({
                type: "POST",
                async: false,
                url: "/Vehicle/AddRelation/",
                data: values,
                dataType: "json",
                success: function (data) { }
            });
        });


        $(".insured-excep").each(function () {

            var Per = $(this).children("#person").val();
            var Rel = $(this).find(".rel").val();
            var VehChassisNum = $(this).parents(".PolVehicle").find('.chassis').html().trim(); 

            var values1 = { chassis: VehChassisNum, per: Per, rel: Rel, pol: policyid }

            $.ajax({
                type: "POST",
                traditional: true,
                url: "/Vehicle/AddRelation/",
                async: false,
                data: values1,
                dataType: "json",
                success: function (data) { }
            });
        });


        $(".insured-exclu").each(function () {

            var Pers = $(this).children("#pers").val();
            var Rela = $(this).find(".rel").val();
            var VehChassisNum = $(this).parents(".PolVehicle").find('.chassis').html().trim(); 

            var values2 = { chassis: VehChassisNum, per: Pers, rel: Rela, pol: policyid }

            $.ajax({
                type: "POST",
                traditional: true,
                url: "/Vehicle/AddRelation/",
                async: false,
                data: values2,
                dataType: "json",
                success: function (data) { }
            });
        });

        */

        //TEST TO SEE IF THE POLICY WAS BEING CREATED OR EDITED
        if (typeof $("#backToEditPol").val() === 'undefined') window.location.replace("/Policy/Details/" + policyid +"?newPol=" + true ); 
        else window.location.replace("/Policy/Edit/" + $("#policyid").val());
    }); //// END OF $("#Done").click

    $(".authorized").on('click', function () {
        authClick($(this));
    });

    $(".excepted").on('click', function () {
        exceptClick($(this));
    });

    $(".excluded").on('click', function () {
        exclClick($(this));
    });

    $("#backToPol").on('click', function () {
        window.location.replace("/Policy/Details/" + $("#policyid").val());
    });
}); //// END OF MAIN FUNCTION


function authClick (e) {

    var chassisNum = $(e).parents(".PolVehicle").find('.chassis').val();
    var ThisVehicle = $(e).parents(".PolVehicle").find('#authorizedDriver');
    var ThisDialog = $(e).parents(".PolVehicle").find("#auth");
    var ThisPolVeh = $(e).parents(".PolVehicle");
    thisVehMainDriv = $(e).parents(".PolVehicle").find(".mainDriverID").text().trim();

    policyid = $("#policyid").val();

    ThisDialog.dialog({
        modal: true,
        autoOpen: false,
        title: "Drivers List",
        width: 700,
        height: 600,

        buttons:{

            'Add Selected': function () {
                var chass = $(this).data('chassis');

                if ( $(".insuredcheck:checked").length == 0) //No Driver was selected
                {
                    $('<div> You have not selected a driver to be added. Please do so, and try again. </div>').dialog({
                        title: 'Alert',
                        modal:true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                                return false;
                            }
                        }
                    });
                }

                else {

                    var alreadyAdded = false;
                    var driverAdded = false;

                    $(".insuredcheck:checked").each(function () {

                       var thisDriver = $(this).prop('id');

                        //adding the authorized driver to the vehicle
                        ThisPolVeh.find('.per').each(function () {

                            if( $(this).val() == thisDriver)
                            {
                                driverAdded = true;
                                alreadyAdded = true;
                            }

                        });

                        if(!driverAdded)
                          {
                            var vals = { chassis: chass, PersonId: thisDriver, type: 1, pol: policyid };

                            $.ajax({
                                type: "POST",
                                traditional: true,
                                url: "/Vehicle/addDrivers/",
                                async: false,
                                data: vals,
                                dataType: "json",
                                success: function (data) { }
                            });

                            var isMainDriver = $(e).parents(".PolVehicle").find(".mainDriverID").text().trim() == thisDriver;

                            $.ajax({
                                url: '/Driver/Drivers/' + thisDriver + '?isMainDriver=' + isMainDriver,
                                cache: false,
                                success: function (html) {
                                    ThisVehicle.append(html); 
                                    updateCount(ThisPolVeh);
                                }
                            }); //end of ajax
                        } //end of if

                        driverAdded = false;

                    }); //end function

                    selected = [];
                    idNumbers = [];
                    thisVehMainDriv = "";
                        
                    $(".ui-dialog-content").dialog("close");
                    $(".ui-dialog-content").html("");
                    $(".ui-dialog-content").dialog("destroy");

                    if(alreadyAdded)
                      {
                       alreadyAdded = false;

                        $('<div> One or more driver that you have selected has already been added to the vehicle. This driver(s) will not be added again. However, any other chosen one will be. </div>').dialog({
                            title: 'Alert',
                            modal:true,
                            buttons: {
                                       Ok: function () {
                                        $(this).dialog("close");
                                   }
                                }
                          });
                    
                      }    

                } //END OF ELSE

            }, //end 'add selected' button
            Create: function () { drivCreate(ThisVehicle, chassisNum, policyid, 1); },
            Cancel: function () {

                selected = [];
                idNumbers = [];
                thisVehMainDriv = "";
                $(".ui-dialog-content").dialog("close");
                $(".ui-dialog-content").html("");
                $(".ui-dialog-content").dialog("destroy")
            }

        } //end buttons
    });   //end dialog

    ThisDialog.data('chassis', chassisNum).load('/People/driverList/' + chassisNum + '?polId=' + policyid, function (value) {

        if (value.substring(2, 6) == 'fail') { //then user does not have the permissions
            $(this).dialog('close');
            $(this).html("");
            $(this).dialog().dialog("destroy");

            $("<div>You do not have the required permissions to complete this task. </div>").dialog({
                title: 'Alert!',
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                        $(this).dialog().dialog("destroy");
                    }
                }
            });
        }
        else {

           //ONLY THE MAIN DRIVER AND THE POLICY INSUREDS ARE SHOWN ON LIST LOAD
           $(".insured-row").each(function () {

                   var ShowThisInsured = false;

                   if($(e).parents(".PolVehicle").find(".mainDriverID").text().trim() !== "" &&  $(e).parents(".PolVehicle").find(".mainDriverID").text().trim() === $(this).find(".insuredcheck").prop('id'))
                     ShowThisInsured = true;

                  else 
                     {
                       for (var z = 0; z < shownPeople.length; z++) {
                              if ($(this).find(".insuredcheck").prop('id') == shownPeople[z] )
                              ShowThisInsured = true;
                         }   
                     }

                    if(ShowThisInsured=== true)
                        $(this).show();
                    else 
                        $(this).hide();

                    //Reset the variable
                    ShowThisInsured = false;
             });

             if( $("#insureds").height() == 0 && !$($(".insured-row")[0]).is(':hidden') )
               $(document).find("#insureds").prepend(noInsured);

             else if(! $('.insured-row').is(':visible') )
               $(document).find("#insureds").prepend(InsuredHidden);


            scrollHeight = $("#insureds").height();

            $("#insureds").perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });

            $("#selected-insured").perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });
        }

    }).dialog('open');
} //END OF MAIN FUNCTION


function exceptClick (e) {


    var chassisNum = $(e).parents(".PolVehicle").find('.chassis').val();
    var ThisVehicle = $(e).parents(".PolVehicle").find('#exceptedDriver');
    var ThisDialog = $(e).parents(".PolVehicle").find("#except");
    var ThisPolVeh = $(e).parents(".PolVehicle");
    thisVehMainDriv = $(e).parents(".PolVehicle").find(".mainDriverID").text().trim();
            
    policyid = $("#policyid").val();

    ThisDialog.dialog({
        modal: true,
        autoOpen: false,
        title: "Drivers List",
        width: 700,
        height: 600,

        buttons: {
            'Add Selected': function () {
                
                var chass = $(this).data('chassis');

                if ( $(".insuredcheck:checked").length == 0) { //No Driver was selected
                    $('<div> You have not selected a driver to be added. Please do so, and try again. </div>').dialog({
                        title: 'Alert',
                        modal:true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                                return false;
                            }
                        }
                    });
                }
                else {
                      
                    var alreadyAdded = false;
                    var driverAdded = false;

                    $(".insuredcheck:checked").each(function () {

                        //adding the excepted driver to the vehicle
                        var isMainDriver;

                        var thisDriver = $(this).prop('id');

                        //adding the excepted driver to the vehicle
                       ThisPolVeh.find('.per').each(function () {

                            if( $(this).val() == thisDriver)
                            {
                                driverAdded = true;
                                alreadyAdded = true;
                            }

                        });

                        if(!driverAdded)
                          {

                            var vals = { chassis: chass, PersonId: $(this).prop('id'), type: 2, pol: policyid };

                            $.ajax({
                                type: "POST",
                                traditional: true,
                                url: "/Vehicle/addDrivers/",
                                async: false,
                                data: vals,
                                dataType: "json",
                                success: function (data) { }
                            });

                            var isMainDriver = $(e).parents(".PolVehicle").find(".mainDriverID").text().trim() == $(this).prop('id');

                            $.ajax({
                                url: '/Driver/Excepted/' + $(this).prop('id') + '?isMainDriver=' + isMainDriver ,
                                cache: false,
                                success: function (html) { ThisVehicle.append(html); updateCount(ThisPolVeh); }
                            });

                        } //end of if

                      driverAdded = false;
                    }); //end function

                    selected= [];
                    idNumbers = [];
                    thisVehMainDriv = "";
                    $(".ui-dialog-content").dialog("close");
                    $(".ui-dialog-content").html("");
                    $(".ui-dialog-content").dialog("destroy");
                    
                    if(alreadyAdded)
                      {
                       alreadyAdded = false;

                        $('<div> One or more driver that you have selected has already been added to the vehicle. This driver(s) will not be added again. However, any other chosen one will be. </div>').dialog({
                            title: 'Alert',
                            modal:true,
                            buttons: {
                                       Ok: function () {
                                        $(this).dialog("close");
                                   }
                                }
                          });
                    
                      }    

                } //END OF ELSE

            }, //end 'add selected' button

            Create: function () { drivCreate(ThisVehicle, chassisNum, policyid, 2); },
            Cancel: function () {
                selected = [];
                idNumbers = [];
                thisVehMainDriv = "";
                $(".ui-dialog-content").dialog("close");
                $(".ui-dialog-content").html("");
                $(".ui-dialog-content").dialog("destroy");
            }

        } //End buttons
    }); //End dialog

    ThisDialog.data('chassis', chassisNum).load('/People/driverList/' + chassisNum + '?polId=' + policyid, function (value) {
        if (value.substring(2, 6) == 'fail') { //then user does not have the permissions
            $(this).dialog('close');
            $(this).html("");
            $(this).dialog().dialog("destroy");

            $("<div>You do not have the required permissions to complete this task. </div>").dialog({
                title: 'Alert!',
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                        $(this).dialog().dialog("destroy");
                    }
                }
            });

        }
        else {

             //ONLY THE MAIN DRIVER AND THE POLICY INSUREDS ARE SHOWN ON LIST LOAD
             $(".insured-row").each(function () {

                   var ShowThisInsured = false;

                   if($(e).parents(".PolVehicle").find(".mainDriverID").text().trim() !== "" &&  $(e).parents(".PolVehicle").find(".mainDriverID").text().trim() === $(this).find(".insuredcheck").prop('id'))
                     ShowThisInsured = true;

                   else 
                     {
                       for (var z = 0; z < shownPeople.length; z++) {
                              if ($(this).find(".insuredcheck").prop('id') == shownPeople[z] )
                              ShowThisInsured = true;
                         }   
                     }

                    if(ShowThisInsured=== true)
                        $(this).show();
                    else 
                        $(this).hide();

                    //Reset the variable
                    ShowThisInsured = false;
            });

             if( $("#insureds").height() == 0 && !$($(".insured-row")[0]).is(':hidden') )
               $(document).find("#insureds").prepend(noInsured);

             else if(! $('.insured-row').is(':visible') )
               $(document).find("#insureds").prepend(InsuredHidden);

            scrollHeight = $("#insureds").height();

            $("#insureds").perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });

            $("#selected-insured").perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });
        }
    }).dialog('open');

} //END of MAIN Function


function exclClick (e) {


    var chassisNum = $(e).parents(".PolVehicle").find('.chassis').val();
    var ThisVehicle = $(e).parents(".PolVehicle").find('#excludedDriver');
    var ThisDialog = $(e).parents(".PolVehicle").find("#excl");
    var ThisPolVeh = $(e).parents(".PolVehicle");
    thisVehMainDriv = $(e).parents(".PolVehicle").find(".mainDriverID").text().trim();

    policyid = $("#policyid").val();

    ThisDialog.dialog({
        modal: true,
        autoOpen: false,
        title: "Drivers List",
        width: 700,
        height: 600,

        buttons: {

            'Add Selected': function () {
                var chass = $(this).data('chassis');


                if ( $(".insuredcheck:checked").length == 0) { //No Driver was selected
                    $('<div> You have not selected a driver to be added. Please do so, and try again. </div>').dialog({
                        title: 'Alert',
                        modal:true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                                return false;
                            }
                        }
                    });
                }
                else {

                    var alreadyAdded = false;
                    var driverAdded = false;

                    $(".insuredcheck:checked").each(function () {

                        //adding the excluded driver to the vehicle

                         var thisDriver = $(this).prop('id');

                        //adding the excluded driver to the vehicle
                        ThisPolVeh.find('.per').each(function () {

                            if( $(this).val() == thisDriver)
                            {
                                driverAdded = true;
                                alreadyAdded = true;
                            }
                        });

                        if(!driverAdded)
                          {

                            var vals = { chassis: chass, PersonId: $(this).prop('id'), type: 3, pol: policyid };

                            $.ajax({
                                type: "POST",
                                traditional: true,
                                url: "/Vehicle/addDrivers/",
                                async: false,
                                data: vals,
                                dataType: "json",
                                success: function (data) { }
                            });

                            var isMainDriver = $(e).parents(".PolVehicle").find(".mainDriverID").text().trim() == $(this).prop('id');
                    
                            $.ajax({
                                url: '/Driver/Excluded/' + $(this).prop('id') + '?isMainDriver=' + isMainDriver,
                                cache: false,
                                success: function (html) { ThisVehicle.append(html); updateCount($(e).parents(".PolVehicle")); }
                            });

                        } //end of if

                      driverAdded = false;

                    });   //End function

                    selected = [];
                    idNumbers = [];
                    thisVehMainDriv = "";
                    $(".ui-dialog-content").dialog("close");
                    $(".ui-dialog-content").html("");
                    $(".ui-dialog-content").dialog("destroy")

                    if(alreadyAdded)
                      {
                       alreadyAdded = false;

                        $('<div> One or more driver that you have selected has already been added to the vehicle. This driver(s) will not be added again. However, any other chosen one will be. </div>').dialog({
                            title: 'Alert',
                            modal:true,
                            buttons: {
                                       Ok: function () {
                                        $(this).dialog("close");
                                   }
                                }
                          });
                      }   

                }// END OF ELSE
            },

            Create: function () { drivCreate(ThisVehicle, chassisNum, policyid, 3); },
            Cancel: function () {
                selected = [];
                idNumbers = [];
                thisVehMainDriv = "";
                $(".ui-dialog-content").dialog("close");
                $(".ui-dialog-content").html("");
                $(".ui-dialog-content").dialog("destroy")
            }

        } //end buttons 
    });   //end dialog

    ThisDialog.data('chassis', chassisNum).load('/People/driverList/' + chassisNum + '?polId=' + policyid, function (value) {
        if (value.substring(2, 6) == 'fail') { //then user does not have the permissions
            $(this).dialog('close');
            $(this).html("");
            $(this).dialog().dialog("destroy");

            $("<div>You do not have the required permissions to complete this task. </div>").dialog({
                title: 'Alert!',
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                        $(this).dialog().dialog("destroy");
                    }
                }
            });
        }
        else {

           //ONLY THE MAIN DRIVER AND THE POLICY INSUREDS ARE SHOWN ON LIST LOAD
           $(".insured-row").each(function () {

               var ShowThisInsured = false;

               if($(e).parents(".PolVehicle").find(".mainDriverID").text().trim() !== "" &&  $(e).parents(".PolVehicle").find(".mainDriverID").text().trim() === $(this).find(".insuredcheck").prop('id'))
                 ShowThisInsured = true;

               else 
                 {
                   for (var z = 0; z < shownPeople.length; z++) {
                          if ($(this).find(".insuredcheck").prop('id') == shownPeople[z] )
                          ShowThisInsured = true;
                     }   
                 }

                if(ShowThisInsured=== true)
                    $(this).show();
                else 
                    $(this).hide();

                //Reset the variable
                ShowThisInsured = false;
           });

            if( $("#insureds").height() == 0 && !$($(".insured-row")[0]).is(':hidden') )
             $(document).find("#insureds").prepend(noInsured);

            else if(! $('.insured-row').is(':visible') )
             $(document).find("#insureds").prepend(InsuredHidden);

            scrollHeight = $("#insureds").height();

            $("#insureds").perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });

            $("#selected-insured").perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });

        }

    }).dialog('open');
}  //END of MAIN Function



//Deleting an authorized driver
$(function () {
    $(document).on("click", "a.remDriver", function (e) {

        e.preventDefault();

        var deleteItem = $(this).parents("div.newAuthdriver:first");
        var id1 = $(this).parents(".newAuthdriver").find(".authorizedId").val(); 
        policyid = $("#policyid").val();
        var VehChassis = $(this).parents(".PolVehicle").find('.chassis').val();
        var ThisPolVeh = $(this).parents(".PolVehicle");
            
        $("<div>You are about to delete this driver. Are you sure you want to do this? </div>").dialog({
            modal: true,
            title: 'Alert!',
            buttons: {
                Ok: function () {

                    var parameters = { chassis: VehChassis, type: 1, perId1: id1, pol: policyid };
                    // console.log(parameters);

                    $.ajax({
                        type: "POST",
                        traditional: true,
                        url: "/Vehicle/deleteDrivers/",
                        async: false,
                        data: parameters,
                        dataType: "json",
                        success: function (data) { }
                    });

                    deleteItem.remove();
                    updateCount(ThisPolVeh);
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                },

                Cancel: function () {
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                }
            }
        });

    });



    $(document).on("click", "a.changeMain", function (e) {
        e.preventDefault();

        var thisCard = $(this);
       // var allDriverFooters = $(".newAuthdriver").find(".DriverFooter");

        var mainDriverImage = $(".mainImage");
        var mainImage = "<div class=\"mainImage\"><img src=\"../../Content/more_images/hat.png\"title=\"Main Driver\"/></div>";
        var mainDriverPicDIv = thisCard.parents(".newAuthdriver").find("#mainDriverImage");
        var previousMain = mainDriverImage.parents(".newAuthdriver").find(".DriverFooter");
        var vehID = $(".vehID").val();
        var TRN = thisCard.parents(".newAuthdriver").find(".perTRN").val();
        

        $("<div>You are about to make this driver the main one. Are you sure you want to do this? </div>").dialog({
            modal: true,
            title: 'Alert!',
            buttons: {
                Ok: function () {

                    var parameters = { vehID: vehID,trn: TRN};
                    
                    $.ajax({
                        type: "POST",
                        traditional: true,
                        url: "/Vehicle/changeMainDriver/",
                        async: false,
                        data: parameters,
                        dataType: "json",
                        success: function (data) { 
                            if(data.result){
                                previousMain.append("<a href='#' class='changeMain'>Change to Main</a>");
                                mainDriverImage.remove();
                                mainDriverPicDIv.html(mainImage);
                                //allDriverFooters.append("<a href='#' class='changeMain'>Change to Main</a>");
                                thisCard.parents(".DriverFooter").find(".changeMain").remove();
                               
                            }
                        }
                    });
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                },

                Cancel: function () {
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                }
            }
        });


        });
});

//Deleting an excepted driver
$(function () {
    $(document).on("click", "a.remDriverExcep", function (e) {

         e.preventDefault();

        var deleteItem = $(this).parents("div.newdriverExcep:first");
        var id1 = $(this).parents(".newdriverExcep").find(".exceptedId").val(); 
        policyid = $("#policyid").val();
        var VehChassis = $(this).parents(".PolVehicle").find('.chassis').val();
        var ThisPolVeh = $(this).parents(".PolVehicle");
        
        $("<div>You are about to delete this driver. Are you sure you want to do this? </div>").dialog({
            modal: true,
            title: 'Alert!',
            buttons: {
                Ok: function () {

                    var parameters = { chassis: VehChassis, type: 2, perId1: id1, pol: policyid };
                    // console.log(parameters);

                    $.ajax({
                        type: "POST",
                        traditional: true,
                        url: "/Vehicle/deleteDrivers/",
                        async: false,
                        data: parameters,
                        dataType: "json",
                        success: function (data) {  }
                    });
                    
                    deleteItem.remove();
                    updateCount(ThisPolVeh);
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                },
                Cancel: function () {
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                }
            }
        });
    });
});

//Deleting an excluded driver
$(function () {
    $(document).on("click", "a.remDriverExc", function (e) {

         e.preventDefault();

        var deleteItem = $(this).parents("div.newdriverExc:first");
        var id1 = $(this).parents(".newdriverExc").find(".excludedId").val(); 
        policyid = $("#policyid").val();
        var VehChassis = $(this).parents(".PolVehicle").find('.chassis').val();
        var ThisPolVeh = $(this).parents(".PolVehicle");

        $("<div>You are about to delete this driver. Are you sure you want to do this? </div>").dialog({
            modal: true,
            title: 'Alert!',
            buttons: {
                Ok: function () {
                    var parameters = { chassis: VehChassis, type: 3, perId1: id1, pol: policyid };
                    // console.log(parameters);

                    $.ajax({
                        type: "POST",
                        traditional: true,
                        url: "/Vehicle/deleteDrivers/",
                        async: false,
                        data: parameters,
                        dataType: "json",
                        success: function (data) { }
                    });

                    deleteItem.remove();
                    updateCount(ThisPolVeh);
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                },

                Cancel: function () {
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                }
            }
        });

    });
});

$(document).ready(function () {
    $("#createDriv").click(function () {
        drivCreate()
    });
});

function drivCreate(ThisVehicle, chassisNum, polId, type) {

    thisVeh = ThisVehicle; newChassis = chassisNum; polcyIdentity = polId; DriverType = type;
    var regexTRN = /^[01][0-9]{8}$/;
    var regex = /^[a-zA-Z][A-Za-z-.\' ]*[a-zA-Z]$/;
    var regex1 = /^[a-zA-Z][A-Za-z-\']*[a-zA-Z]*$/;
    var regex2 = /^[a-zA-Z0-9][a-zA-Z0-9#\-/&. ]*$/;
    var regex3 = /^[a-zA-Z][a-zA-Z0-9.&,#\-\' ]*[a-zA-Z]$/;
    var regex4 = /^[a-zA-Z][a-zA-Z.&,#\-\' ]*[a-zA-Z ]$/;
    var regexAptNum = /^[a-zA-Z0-9-\/#&.\' ]*$/;
    var regexEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9])+$/;

    var buttonClicked = $(ThisVehicle).parent().children("#authorized1").children(".authButton");
    var newDialog = $("#createDriver");

    newDialog.dialog({
        modal: true,
        autoOpen: false,
        title: "Create New Driver",
        width: 370,
        height: 580,

        buttons: {
            Create: function () {

                var trn = $("#trn").val();
                var fname = $("#fname").val();
                var mname = $("#mname").val();
                var lname = $("#lname").val();
                var roadno = $("#RoadNumber").val();
                var aptNumber = $("#aptNumber").val();
                var road = $("#roadname").val();
                var roadType = $("#roadtype").val();
                var zip = $("#zipcode").val();
                var city = $("#city").val();
                var country = $("#country").val();
                var parish = $(".parishes").val();
                var ret = 0;
                var count = 0;
                var perId = 0;

                var emails = [];
                var primary = [];
                var email_objects = [];
                var valid = true;

                $(".email").each(function () {
                    
                    emails[emails.length] = $(this).val();

                    if (!regexEmail.test(emails[count])) {
                        alert('An email address entered was invalid.');
                        valid = false;
                        return false;
                    }
                    count++;

                });

                if (valid) {
                    $(".emailprimary").each(function () {
                        primary[primary.length] = $(this).prop("checked");
                    });
                }

                if (valid) {
                    for (var i = 0; i < emails.length; i++) {
                        email_objects.push({ "email": emails[i], "primary": primary[i] });
                    }
                }

                // VALIDATION CHECKS
                if (!valid) { return false; }

                if (trn == '' || !regexTRN.test(trn)) {
                    alert('Please enter a valid TRN. TRN Must be a nine digit number, beginning with a 0 or 1');
                    return false;
                }

                if (fname == '' || !regex.test(fname)) {
                    alert('Please enter a valid first name.');
                    return false;
                }

                if (mname == '') { }
                else {
                    if (!regex1.test(mname)) {
                        alert('Please enter a valid middle name.');
                        return false;
                    }
                }

                if (lname == '' || !regex.test(lname)) {
                    alert('Please enter a valid last name.');
                    return false;
                }

                if (roadno == '') { }
                    else {
                    if (!regex2.test(roadno)) {
                        alert('Please enter a valid road number.');
                        return false;
                    }
                }

                if (aptNumber != '' && !regexAptNum.test(aptNumber)) {
                    alert('Please enter a valid apartment/building number.');
                    return false;
                }

//                if (road == '' || !regex3.test(road)) {
//                    alert('Please enter a valid road name.');
//                    return false;
//                }

                if (roadType == '') { }
                else {
                    if (!regex3.test(roadType)) {
                        alert('Please enter a valid road type.');
                        return false;
                    }
                }

                if (zip == '') { }
                else {
                    if (!regex2.test(zip)) {
                        alert('Please enter a valid zip.');
                        return false;
                    }
                }

                if (city == '') { }
                else {
                    if (!regex3.test(city)) {
                        alert('Please enter a valid city.');
                        return false;
                    }
                }

                if (country == '' || !regex4.test(country)) {
                    alert('Please enter a valid country name.');
                    return false;
                }

                var parms = {   
                                trn: trn, 
                                fname: fname, 
                                mname: mname, 
                                lname: lname, 
                                roadno: roadno, 
                                aptNumber: aptNumber, 
                                road: road, 
                                roadType: roadType, 
                                city: city, 
                                country: country, 
                                email: emails, 
                                primary: primary, 
                                parish: parish 
                            };


                $.ajax({
                    type: "POST",
                    traditional: true,
                    url: "/Vehicle/AddPersonDriver/",
                    async: false,
                    data: parms,
                    dataType: "json",
                    success: function (data) {
                        if (data.result) perId = data.perId;
                    }
                });

                /////////////////////////////////////////////////////////////////////////////
                // Adding the newly created driver to the drivers' list page
                  
                AddPersonSelected(perId, 1);
                newDialog.dialog("close");
                newDialog.html(""); 
                newDialog.dialog("destroy");

            }, 
            Cancel: function () {
                newDialog.dialog("close");
                newDialog.html("");
                newDialog.dialog("destroy"); 
            }
        }
    });

    newDialog.load('/Driver/Driver',function(){ $('#mname').trigger('keyup'); }).dialog('open');

} //End of main function

/*$(function(){

    $('.validate-section.policy-veh-drivers').click(function(){
        $('.validate-section.policy-veh-drivers .vehicleChassisHeader.subheader.arrow-click').each(function(){
            $(this).find(".arrow").removeClass('down');
        });
    }); 
});
*/

function clickme(e) {
    if ($(e).parent().children(".vehicle-row").is(':visible')) {
        $(e).parent().children(".vehicle-row").css("margin-bottom", "20px");
        $(e).parent().children(".vehicle-row").slideUp();
    }
    else {
        $(".vehicle-row").slideUp();
        $(".arrow-click").find(".arrow").removeClass("down");
        $(e).parent().children(".vehicle-row").css("margin-bottom", "20px");
        $(e).parent().children(".vehicle-row").slideDown();
    }
    $(e).parent().find(".arrow").toggleClass('down');
}

function updateCount(e){
    
    auth = $(e).find("#authorizedDriver").children().length;
    except = $(e).find("#exceptedDriver").children().length;
    exclude = $(e).find("#excludedDriver").children().length;

    $(e).find(".update-count").html("Drivers: Auth: " + auth + " / Except: " + except + " / Exclude: " + exclude);
    auth = 0; except = 0; exclude = 0;

}

$(function(){

    if($('span.description').html().length > 40){
        
        var descrip = $('span.description').html();
        $('span.description').html(descrip.substring(0,40) + "...");
    }


});

$(function(){
    
    var adwID, polID, vehID;


    $(".adwID").each(function(){
         var hiddenADW = $(this).val();
         $(this).parents(".adw-div").find(".ADW_dropdown").children().each(function () {
                    if ($(this).val() == hiddenADW) {
                        $(this).prop('selected', true);
                        return;
                    }
                });
    });
    
    $('html').on('change','.ADW_dropdown', function () {

        adwID = $(this).val();
        polID = $("#policyid").val();
        vehID = $(this).parents(".PolVehicle").find(".vehID").val();

        $.ajax({
            url: "/AuthorizedDriverWording/UpdateRiskADWording/" +'?adwID='+ adwID + "&polID=" + polID + "&vehID=" + vehID
        });
    });

});
    