﻿//THIS OBJECT IS USED TO STORE HTML AND IDS OF DATA GOTTEN FROM THE SYSTEM
//
function VehListObject(id, html) {
    this.id = id;
    this.html = html;
}


function clearExpiryFitness() {
    $("#fitnessExpiry").val("");
}

var defaultUsage = 0;
var checkIfVehAdded = 0;
//var vehIDNums = [];
var selectedVehs = [];
var vehicleSelected = [];
var scrollHeight;
var noInsuredVeh = "<div class=\"rowNone\">There are no vehicles currently available to you in the IVIS system. Please create a new one.</div>";
var InsuredHiddenVeh = "<div class=\"rowNone\">Please search the system for existing vehicles, or add a new vehicle. </div>";

function AdvancedCheckRun(flag) {
    //if flag true then just check, don't uncheck
    if ($(".advanced-check").is(":checked") || flag) $(".vehicle-advanced-data").show();
    else $(".vehicle-advanced-data").hide();
}

$(function () {
    $("body").on("change", ".advanced-check", function () {
        AdvancedCheckRun(false);
    });

    $("#create").click(function () {
        vehCreate();
    });

    $("#createDriv").click(function () {
        drivCreate();
    });
});

//Search when enter-button is hit
$(document).on("keyup", ".vehSearch", function (event) {
    var text = $(".vehSearch").val();
    if (event.which == 13) {
        searchVehicleData(text);
    }
});

function updateVehicleView() {
    for (var x = 0; x < selectedVehs.length; x++) {
        $("#vehiclelist-selected").append(selectedVehs[x].html);
    }
    setTimeout(function () {
        $("#vehiclesList").children(".vehicle-row").each(function () {
            //If the vehicle is already on display, in the select box, then it doesn't need to be shown in the vehicles list again
            testVehicleIfAdded($(this).find(".baseid").val());

            if (checkIfVehAdded == 1) $(this).hide();

            checkIfVehAdded = 0;

            if ($("#vehiclesList").height() + $("#vehiclelist-selected").height() < 380) $("#vehiclesList").height($("#vehiclesList").height() + 20);
            else if ($("#vehiclesList").height() + $("#vehiclelist-selected").height() >= 380) $("#vehiclesList").height($("#vehiclesList").height() - 20);


        });
    }, 1000);
    $("#vehiclelist-selected").perfectScrollbar('update');
    $("#vehiclesList").perfectScrollbar('update');

    //Resizing the vehicles list div, so the scroll works
}

function testVehicleIfAdded(id) {
    for (var y = 0; y < selectedVehs.length; y++) {
        if (selectedVehs[y].id == id) checkIfVehAdded = 1;
    }
}

function removeThisVeh(id) { //This function removes the element from the arrays
    for (var y = 0; y < selectedVehs.length; y++) {
        //if ($($(selectedVehs[y].html)).find(".baseid").val() == id) {
        if (selectedVehs[y].id == id) {
            selectedVehs.splice(y, 1);
            removeHTML(id);
        }
    }
    updateVehicleView();
}

function removeHTML(id) {
    $("#vehiclelist-selected .vehicle-row, #vehicleList .vehicle-row").each(function () {

        if ($(this).find(".baseid").val() == id) {
            $(this).remove();
            return false;
        }
    });
}

function testNewVehicleId(id) {
    var result = false;
    $("#vehiclelist-selected .vehicle-row, #vehicleList .vehicle-row").each(function () {
        if ($(this).find(".vehcheck").prop("id") == id) {
            result = true;
            return false;
        }
    });
    return result;
}

function replaceOldVehicle(html, baseid, id) {
    var result = false;
    $("#vehiclelist-selected .vehicle-row, #vehicleList .vehicle-row").each(function () {
        if ($(this).find(".baseid").val() == baseid) {
            removeThisVeh($(html).find(".vehcheck").prop("id"));
            return false;
        }
    });
    return result;
}

function AddVehicleSelected(id, addNew) {
    var err = "<div>There was an error with adding the vehicle. Please try again. If the problem persists, please contact your administrator.</div>";

    //Removing the div that displayes the message (of either no vehicles being present, or vehicles count exceeding 50)
    if (typeof $(".rowNone").val() !== 'undefined') $(".rowNone").remove();

    $.ajax({
        type: "POST",
        url: '/Vehicle/VehicleListItemSelected/' + id,
        cache: false,
        async: false,
        success: function (data) {

            var baseid = $(data).find(".baseid").val();
            testVehicleIfAdded(baseid);

            if (checkIfVehAdded != 1) {
                
                var vehobj = new VehListObject(baseid, $(data));
                selectedVehs.push(vehobj);
            }
            else {
                if (!testNewVehicleId(id)) {
                    removeThisVeh(baseid);

                    var vehobj = new VehListObject(baseid, $(data));
                    selectedVehs.push(vehobj);
                }
            }
            updateVehicleView();
        },
        error: function () {
            $(err).dialog({
                title: "Error",
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog('close');
                    }
                }
            })
        }
    });                 //END OF AJAX CALL
    
    checkIfVehAdded = 0; //Reset the variable
}


function searchOptionsDrivers() {
    var name = $(event.target).parent().children('#search').val().toLowerCase();
    $(event.target).parent().parent().children(".insured-row").filter(function (div) {
        for (var i = 0; i < $(this).children().length; i++) {
            if ($($(this).children()[i]).text().toLowerCase().trim().indexOf(name) > -1) {
                
                $(this).show();
                break;
            }
            else {
                //uncheck and hide
                if ($(this).children().length - 1 == i) {
                    $(this).children('.insured-check').children('input.driverCheck').prop('checked', false);
                    $(this).hide();
                }
            }
        }
    });
};

function searchOptionsVehicles() {
    var name = $(event.target).parent().children('#search').val();
    $(event.target).parent().parent().children("#vehiclesList").children('.vehicle-row').filter(function (div) {
        for (var i = 0; i < $(this).children().length; i++) {
            if ($($(this).children()[i]).text().trim().indexOf(name) > -1) {
                //show
                $(this).show();
                break;
            }
            else {
                //uncheck and hide
                if ($(this).children().length - 1 == i) {
                    $(this).children('.vehicle-check').children('input.vehcheck').prop('checked', false);
                    $(this).hide();
                }
            }
        }
    });
};


function changeVehicleCheckBox(e) {

    //Stores search value, as it breaks if the value is not stored before the div is manipulated
    var searchValue = $(e).parents(".insured-modal").find(".modalSearch").val();
    var thisRow = $(e).parents(".vehicle-row");
    $(e).parents(".vehicle-row").remove();
    var baseid = $(e).parents('.vehicle-row.selected').find('.baseid').val();

    if ($(e).parents(".vehicle-row").hasClass("selected")) {

        var vehAlreadyAdded = false;

        $("#vehiclesList").find(".vehicle-row").each(function () {
            if ($(this).find(".vehcheck").prop('id') == e.id) {
                vehAlreadyAdded = true;
                return false;
            }
        });

        if (!vehAlreadyAdded) $("#vehiclesList").append($(e).parents(".vehicle-row").toggleClass("selected"));
        removeThisVeh(baseid);
        $("#vehiclesList").prepend(thisRow);
        thisRow.hide();

    }
    else AddVehicleSelected(e.id, 0);
    searchVehicleData(searchValue,true);
}

function searchVehicle(e) {
    var filter = $(e).parents("#insured").find(".modalSearch").val();
    searchVehicleData(filter);
}

function searchVehicleData(filter, checkbox) {
    var checkbox = typeof checkbox == 'undefined' ? false : checkbox;
    $(".rowNone").remove();
    if (filter.trim().length == 0) {
        if ($("#vehiclesList").children(".vehicle-row").length <= 50) {
            $("#vehiclesList").children(".vehicle-row").show();
            if ($("#vehiclesList").height() + $("#vehiclelist-selected").height() < 380) $("#vehiclesList").height(380);
        }
        else {
            $("#vehiclesList").children(".vehicle-row").hide();

        }
    }
    else {
        //Removing the div that displayes the message (of either no vehicles being present, or vehicles count exceeding 50)
        if (typeof $(".rowNone").val() !== 'undefined') $(".rowNone").remove();
        $("#vehiclesList").children(".vehicle-row").each(function () {
            //If the vehicle is already on display, in the select box, then it doesn't need to be shown in the search area again
            testVehicleIfAdded($($(this).children(".vehicle-check").children(".vehcheck")).prop('id'));
            if (checkIfVehAdded != 1) {
                if (($(this).children(".chassisno").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".make").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".model").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".regno").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1)) {
                    $(this).show();
                    if ($("#vehiclesList").height() + $("#vehiclelist-selected").height() < 380) $("#vehiclesList").height($("#vehiclesList").height() + 20);
                }
                else $(this).hide();
            }
            checkIfVehAdded = 0;

            //Added to test if insured added already, then dont show

            for (var y = 0; y < vehicleSelected.length; y++) {

                if ($($(this)).find('.vehicle_Id').val() == vehicleSelected[y]) {
                    $(this).hide();
                }

            }

            /*****/
        });

    } //End of else

    redrawList();

    if ($("#vehiclesList").height() == 0 && !$($(".vehicle-row")[0]).is(':hidden'))
        $(document).find("#vehiclesList").prepend(noInsuredVeh);

    else if ( !$('.vehicle-row').is(':visible') )
        $(document).find("#vehiclesList").prepend(InsuredHiddenVeh);

    if (!checkbox) {
        $(".recordsFound").text("");
        if ($("#vehiclesList").children(".vehicle-row:visible").length <= 0) $(".rowNone").prepend("<div class=\"ResultModal\">No Vehicless found!</div>");
        else
            $(".recordsFound").text($("#vehiclesList").children(".vehicle-row:visible").length + " record(s) found");
    }

    $("#vehiclesList").perfectScrollbar('update');
    $("#vehiclelist-selected").perfectScrollbar('update');
}

//Closing the dialog window
function closeD() {
    $("#vehicle-modal").dialog("close");
    $("#vehicle-modal").html("");
    $("#vehicle-modal").dialog("destroy"); ;
    //$("#vehicle-modal").replaceWith('<div id=\"vehicle-modal\"></div>');
    vehIDNums = [];
    selectedVehs = [];
}

$(function () {
    $(document).on("click", "a.rmvehicle", function (e) {

        e.preventDefault();
        var deleteItem = $(this).parents("div.newvehicle:first");

        $("<div>You are about to delete this vehicle. Are you sure you want to do this? </div>").dialog({
            modal: true,
            title: 'Alert!',
            buttons: {
                Ok: function () {
                    //rm_vehicle_ajax($("#hiddenid").val(), $(deleteItem).find(".vehId").val());
                    deleteItem.remove();
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                },
                Cancel: function () {
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                }
            }
        });
    });

    $("#addvehicle").click(function () {
        var cid = $("#compid").val();
        $("#vehicle-modal").dialog({
            modal: true,
            autoOpen: false,
            width: 800,
            title: "Vehicles List",
            height: 600

        }); //END .DIALOG

        $("#vehicle-modal").load('/Vehicle/VehiclesList/' + cid, function (value) {

            if (value.substring(2, 6) == 'fail') { //then user does not have the permissions
                $(this).dialog('close');
                $(this).html("");
                $(this).dialog().dialog("destroy");

                $("<div>You do not have the required permissions to complete this task. </div>").dialog({
                    title: 'Alert!',
                    modal: true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                            $(this).dialog().dialog("destroy");
                        }
                    }
                });

            }

            else { //There were results returned

                vehicleSelected = [];
                var selectedInsureds = $('#vehicles').find(".newvehicle");
                if (selectedInsureds.length > 0) {

                    for (var i = 0; i < selectedInsureds.length; i++) {

                        vehicleSelected.push($(selectedInsureds[i]).find('.vehId').val());
                    }
                }

                if ($("#vehiclesList").children(".vehicle-row").length <= 50) {
                    $("#vehiclesList").children(".vehicle-row").show();

                    var vehicleRows = $(document).find(".vehicle-row");
                    for (var x = 0; x < vehicleSelected.length; x++) {
                        for (var i = 0; i < vehicleRows.length; i++) {


                            if ($(vehicleRows[i]).find('.vehicle_Id').val() == companySelected[x])
                                $(vehicleRows[i]).hide();

                        }
                    }
                }

                else $("#vehiclesList").children(".vehicle-row").hide();

                scrollHeight = $("#vehiclesList").height();

                $("#vehiclesList").perfectScrollbar({
                    wheelSpeed: 0.3,
                    wheelPropagation: true,
                    minScrollbarLength: 10,
                    suppressScrollX: true
                });

                $("#vehiclelist-selected").perfectScrollbar({
                    wheelSpeed: 0.3,
                    wheelPropagation: true,
                    minScrollbarLength: 10,
                    suppressScrollX: true
                });

            }

        }).dialog('open');

    }); //END CLICK

    $("#buttonAdd").click(function () {
        getValue();
    });
});

function getValue() {

    var chkArray = [];
    var vehIds = [];
    var checkAdded = 0;
	
    var vals = []; //store the values of the ids added already
    var count = 0;
    var count1 = 0;
    
    count = $(".vehId").length;             //counting the amount of vehicles already added to the policy
    count1 = $(".vehcheck:checked").length; //counting the amount of vehicles selected

    if (count1 == 0) //No vehicles have been selected 
    {
        $('<div> You have not selected a risk to be added. Please do so, and try again. </div>').dialog({
            title: 'Alert',
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    return false;
                }
            }
        });
    }
    else {
        if (count > 0) {

            var vehicleReAdd = false;

            //stroring all the already added Ids in an array
            for (var x = 0; x < count; x++) {
                vals[x] = $(".base_veh_id")[x].value; 
            }

            //checking each selected id against the ones already added
            for (var y = 0; y < count1; y++) 
            {
                for (var z = 0; z < count; z++) {
                    if ($($(".vehcheck:checked")[y]).parents(".vehicle-row").find(".baseid").val() == vals[z]) 
                    {
                        checkAdded = 1;
                        vehicleReAdd = true;
                    } 
                }

                if (checkAdded != 1) vehIds[vehIds.length] = $(".vehcheck:checked")[y].id;

                checkAdded = 0; //reset the variable

            } //END of outer for


            //IF AT LEAST ONE OF THE SELECTED VEHICLES WAS ALREADY ADDED TO THE PAGE
            if (vehicleReAdd) {
                
                $('<div> One or more risk(s) that you have selected has already been chosen to be added to this policy. This risk(s) will not be added again. However, any other chosen risk will be. </div>').dialog({
                    title: 'Alert',
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }

            var vehsUnderPol = false;
            var autoCancelled = false;  
            for (var x = 0; x < vehIds.length; x++)
            {
                var parameters = {
                    test: vehIds[x],
                    type: "Add",
                    startdate: $("#start").val(), 
                    endDate: $("#end").val(),
                    CoverID: $(".covers_dropdown").val()   
                };

                $.ajax({
                    type: "POST",
                    url: '/Vehicle/Vehicle/',
                    data: parameters,
                    cache: false,
                    async: false,
                    success: function (html) {
                        html = testVehiclePolicy($(html).find(".vehId").val(), $('#start').val(), $('#end').val(), $(html));
                        if ($(html).hasClass('underPolicy')) vehsUnderPol = true;
                        if ($(html).hasClass('autoCancelled')) autoCancelled = true;
                        $(html).insertBefore("#addvehicle");
                        $('.usage_dropdown').trigger('change');
                        //if ($("#page").val() == "edit") add_vehicle_ajax($("#hiddenid").val(), $(html).find(".vehId").val());
                    }
                });
            }

            if (vehsUnderPol) {
                $('<div>One or more of the vehicles selected are under a previous,active policy, during the time frame selected.</div>').dialog({
                    modal: true,
                    title: 'Alert!',
                    buttons: {
                        OK: function () {
                             $(this).dialog("close");
                        }
                    }
                });
             }

             if (autoCancelled) {
                 $('<div>The vehicle was under policy but the company\'s Auto Cancel feature is enabled. The vehicle was cancelled under that policy and added to this one.</div>').dialog({
                     modal: true,
                     title: 'Alert!',
                     buttons: {
                         OK: function () {
                             $(this).dialog("close");
                         }
                     }
                 });
             }
        }
         else {
             var vehsUnderPol = false;
             var autoCancelled = false;
             $(".vehcheck:checked").each(function () {

                 var parameters = {
                     test: $(this).prop('id'),
                     type: "Add",
                     startDate: $("#start").val(),
                     endDate: $("#end").val(),
                     CoverID: $(".covers_dropdown").val()
                 };

                 $.ajax({
                     type: "POST",
                     url: '/Vehicle/Vehicle/',
                     data: parameters,
                     cache: false,
                     async: false,
                     success: function (html) {
                         html = testVehiclePolicy($(html).find(".vehId").val(), $('#start').val(), $('#end').val(), $(html));
                         if ($(html).hasClass('underPolicy')) vehsUnderPol = true;
                         if ($(html).hasClass('autoCancelled')) autoCancelled = true;
                         $(html).insertBefore("#addvehicle");
                         $('.usage_dropdown').trigger('change');
                         
                         //if ($("#page").val() == "edit") add_vehicle_ajax($("#hiddenid").val(), $(html).find(".vehId").val());

                     }
                 });

             });

             if (vehsUnderPol) {
                 $('<div>One or more of the vehicles selected are under a previous,active policy, during the time frame selected.</div>').dialog({
                    modal: true,
                    title: 'Alert!',
                    buttons: {
                        OK: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }

            if (autoCancelled) {
                $('<div>The vehicle was under policy but the company\'s Auto Cancel feature is enabled. The vehicle was cancelled under that policy and added to this one.</div>').dialog({
                    modal: true,
                    title: 'Alert!',
                    buttons: {
                        OK: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }
        }

        $("#vehicle-modal").dialog('close');
        $("#vehicle-modal").html("");
        $("#vehicle-modal").dialog("destroy");
        //$("#vehicle-modal").replaceWith('<div id=\"vehicle-modal\"></div>');

    } //END OF ELSE 

    vehIDNums = [];
    selectedVehs = [];
    checkIfVehAdded = 0;

  
} // END OF FUNCTION 'getValue'


function vehCreate() {

    //regExpressions for validation
    var regexMake = /^[a-zA-Z0-9][a-zA-Z0-9-.,\'& ]*[a-zA-Z0-9]$/;
    var regexModel = /^[a-zA-Z0-9][a-zA-Z0-9-.&\', ]*[a-zA-Z0-9]*$/;
    var regexModelType = /^[a-zA-Z0-9][a-zA-Z0-9-,.\'& ]*[a-zA-Z0-9]*$/;
    var regexbodyType = /^[a-zA-Z0-9][a-zA-Z0-9-,\/.\'& ]*[a-zA-Z0-9]$/;
    var regexExtention = /^[a-zA-Z0-9][a-zA-Z0-9-,.\'& ]*[a-zA-Z0-9]$/;
    var regNumber = /^[a-zA-Z0-9]+$/;
    var regexVIN = /^[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]+$/;
    var regexColour = /^[a-zA-Z-.,\/& ]+$/;
    var regexChassis = /^[a-zA-Z0-9]+$/;
    var regexEngineNo = /^[a-zA-Z0-9][a-zA-Z0-9- ]*[a-zA-Z0-9]*$/;
    var regexEngineMod = /^[a-zA-Z][a-zA-Z0-9-]*$/;
    var regexengineType = /^[a-zA-Z0-9][a-zA-Z0-9-&.,\' ]*[a-zA-Z0-9]+$/;
    var regexHPCCUnitType = /^[a-zA-Z0-9][a-zA-Z0-9.,\' ]*[a-zA-Z0-9]+$/;
    var regexregLoc = /^[a-zA-Z0-9][a-zA-Z0-9-&\-,#.\' ]*[a-zA-Z0-9]+$/;
    var regexRoof = /^[a-zA-Z0-9][a-zA-Z0-9- ]*[a-zA-Z0-9]+$/;
    var regexTransType = /^[a-zA-Z]{1,}[a-zA-Z-]*[a-zA-Z]$/;
    var regexHDriveType = /^[a-zA-Z]{1,}[a-zA-Z-]*[a-zA-Z]$/;
    var regexNumbers = /^[0-9]+$/;
    var regexRegOwner = /^[a-zA-Z0-9][a-zA-Z0-9\-@!?\/#&$+,.\' ]*$/;
    var regexEstimatedValue = /^([$])?[1-9][0-9]{0,2}(([,][0-9]{3})*|([0-9]*))([.][0-9]{0,2})?$/;

    var regexTRN = /^[01][0-9]{8}$/;
    var regex = /^[a-zA-Z][A-Za-z-.\' ]*[a-zA-Z]$/;
    var regex1 = /^[a-zA-Z][A-Za-z-.\' ]*[a-zA-Z]*$/;
    var regex2 = /^[a-zA-Z0-9][a-zA-Z0-9-\/#&. ]*$/;
    var regex3 = /^[a-zA-Z][a-zA-Z0-9.&,#\-\' ]*$/;
    var regex4 = /^[a-zA-Z][a-zA-Z.&,#\-\' ]*[a-zA-Z]$/;
    var regexAptNum = /^[a-zA-Z0-9-\/#&.\' ]*$/;
    var regexEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9])+$/;
    var regexCity = /^(([a-zA-Z](([a-zA-Z0-9.& '-])*[a-zA-Z0-9])?)|([a-zA-Z](([a-zA-Z0-9.& '-])*([ ])?[(]([a-zA-Z0-9.& '-])+[a-zA-Z0-9][)])?))$/;


    $("#vehicle-modal1").load('/Vehicle/AddNewVehicle?PolCoverID=' + $(".covers_dropdown").val(), function (data) {
        // This part runs 'on-load' of the dialog 
        if (data != '') {
            var names = [];
            var shownNames = [];

            names = get_names();

            $("#vehicle-modal1").find("#regOwner").autocomplete({
                source: function (request, response) {
                    for (var i = 0; i < names.length; i++) {
                        if (names[i].toLowerCase().indexOf($("#regOwner").val().toLowerCase()) > -1) shownNames.push(names[i]);
                    }
                    response(shownNames);
                    shownNames = [];
                }
            });

            $(".insured .base_per_id").each(function () {
                $("#maindriver-dropdown").append($("<option></option>").val($(this).val()).text($(this).parents(".insured").find(".names").val()));
            });
        }
        else {
            $("#vehicle-modal1").html('<div>You do not have permissions to be running this procedure.</div>');
        }

    }).dialog({
        modal: true,
        //autoOpen: false,
        width: 400,
        title: "Vehicle Creation Form",
        height: 500,

        buttons: {
            Create:
            function () {

                var est_val = $("#estimated_value").val().trim();

                var vin = $("#vin").val().trim();
                var chassis = $("#chassis").val().trim();
                var make = $("#make").val().trim();
                var model = $("#vehiclemodel").val().trim();
                var modelType = $("#modeltype").val().trim();
                var bodyType = $("#bodytype").val().trim();
                var Extension = $("#extension").val().trim();
                var vehicleRegNumber = $("#vehicleRegNum").val().trim();
                var colour = $("#colour").val().trim();
                var cylinders = $("#cylinders").val().trim();
                var engineNo = $("#engineNo").val().trim();
                var engineModified = $("#engineModified").prop("checked");
                var engineType = $("#engineType").val().trim();
                var estAnnualMileage = $("#mileage").val().trim();
                var expiryDateOfFitness = $("#fitnessExpiry").val().trim();
                var HPCCUnitType = $("#hpccUnit").val().trim();

                var ReferenceNo = $("#refno").val().trim();
                var RegistrationLocation = $("#regLocation").val().trim();
                var RoofType = $("#roofType").val().trim();
                var seating = $("#seating").val().trim();
                var seatingCert = $("#seatingCert").val().trim();
                var tonnage = $("#tonnage").val().trim();
                var VehicleYear = $("#vehicleYear").val().trim();
                var handDriveType = $("#handDriveType").val().trim();
                var transtype = $("#tranType").val().trim();
                var regOwner = $("#regOwner").val().trim();
                var restrictedDriving = $("#restrictedDriver").prop("checked");

                var perId = $("#personid").val().trim();
                var trn = $("#trn").val().trim();
                var fname = $("#fname").val().trim();
                var mname = $("#mname").val().trim();
                var lname = $("#lname").val().trim();
                var roadno = $("#RoadNumber").val().trim();
                var road = $("#roadname").val().trim();
                var aptNumber = $("#aptNumber").val().trim();
                var roadType = $("#roadtype").val().trim();
                var zip = $("#zipcode").val();
                var city = $("#city").val().trim();
                var country = $("#country").val().trim();
                var parish = $(".vehicle-advanced-data .parishes").find('option:selected').val();
                var prevChassis = $("#prevChassis").val().trim();
                var prevRegNo = $("#prevRegNo").val().trim();

                var countEms = 0;

                var emails = [];
                var primary = [];
                var email_objects = [];
                var validation = true;

                var advanced = $(".advanced-check").is(":checked");

                //do we still need this???
                $(".email").each(function () {
                    emails[emails.length] = $(this).val();

                    if (!regexEmail.test(emails[countEms])) {

                        alert('An email address entered was invalid.');
                        validation = false;
                        return false;
                    }
                    countEms++;
                });

                if (validation) {
                    $(".emailprimary").each(function () {
                        primary[primary.length] = $(this).prop("checked");
                    });
                    for (var i = 0; i < emails.length; i++) {
                        email_objects.push({ "email": emails[i], "primary": primary[i] });
                    }
                }
                else { return false; }

                if (vin != '' && !regexVIN.test(vin)) {
                    alert('Please enter a valid VIN.');
                    return false;
                }

                if (chassis == '' || !regexChassis.test(chassis)) {
                    alert('Please enter a valid chassis (numbers and letters only) ');
                    return false;
                }

                if (make == '' || !regexMake.test(make)) {
                    alert('Please enter a valid make');
                    return false;
                }

                if (model != '' && !regexModel.test(model)) {
                    alert('Please enter a valid model');
                    return false;
                }

                if (VehicleYear == '' || !regexNumbers.test(VehicleYear)) {
                    alert('Please enter a valid Vehicle Year (numbers only) ');
                    return false;
                }

                if (vehicleRegNumber != '' && !regNumber.test(vehicleRegNumber)) {
                    alert('Please enter a valid  registration number');
                    return false;
                }

                if (modelType == '') { }
                else {
                    if (!regexModelType.test(modelType)) {
                        alert('Please enter a valid model type');
                        return false;
                    }
                }

                if (bodyType == '') { }
                else {
                    if (!regexbodyType.test(bodyType)) {
                        alert('Please enter a valid body type');
                        return false;
                    }
                }

                if (Extension == '') { }
                else {
                    if (!regexExtention.test(Extension)) {
                        alert('Please enter a valid  Extension');
                        return false;
                    }
                }

                if (colour == '') { }
                else {
                    if (!regexColour.test(colour)) {
                        alert('Please enter a valid colour');
                        return false;
                    }
                }

                if (cylinders == '') { }
                else {
                    if (!regexNumbers.test(cylinders)) {
                        alert('Please enter a valid cylinders (number) ');
                        return false;
                    }
                }

                if (engineNo == '') { }
                else {
                    if (!regexEngineNo.test(engineNo)) {
                        alert('Please enter a valid engine number ');
                        return false;
                    }
                }

                if (engineType == '') { }
                else {
                    if (!regexengineType.test(engineType)) {
                        alert('Please enter a valid engine type ');
                        return false;
                    }
                }

                if (estAnnualMileage == '') { }
                else {
                    if (!regexNumbers.test(estAnnualMileage)) {
                        alert('Please enter a valid annual mileage value (numbers only) ');
                        return false;
                    }
                }

                if (HPCCUnitType == '') { }
                else {
                    if (!regexHPCCUnitType.test(HPCCUnitType)) {
                        alert('Please enter a valid HPCCUnit value ');
                        return false;
                    }
                }


                if (ReferenceNo == '') { }
                else {
                    if (ReferenceNo == '' || !regexNumbers.test(ReferenceNo)) {
                        alert('Please enter a valid reference no (numbers only)');
                        return false;
                    }
                }

                if (RegistrationLocation == '') { }
                else {
                    if (!regexregLoc.test(RegistrationLocation)) {
                        alert('Please enter a valid registration location ');
                        return false;
                    }
                }

                if (RoofType == '') { }
                else {
                    if (!regexRoof.test(RoofType)) {
                        alert('Please enter a valid roof type ');
                        return false;
                    }
                }

                if (seating == '' || seating == '0') { seating = '' }
                else {
                    if (!regexNumbers.test(seating)) {
                        alert('Please enter a valid seating (number) ');
                        return false;
                    }
                }

                if (seatingCert == '' || seatingCert == '0') { seatingCert == '' }
                else {
                    if (!regexNumbers.test(seatingCert)) {
                        alert('Please enter a valid seating cert (number) ');
                        return false;
                    }
                }

                if (tonnage == '' || tonnage == '0') { tonnage == '' }
                else {
                    if (!regexNumbers.test(tonnage)) {
                        alert('Please enter a valid tonnage (number) ');
                        return false;
                    }
                }

                if (handDriveType == '') { }
                else {
                    if (handDriveType == '' || !regexTransType.test(handDriveType)) {
                        alert('Please enter a valid handDriveType ');
                        return false;
                    }
                }

                if (transtype == '') { }
                else {
                    if (!regexHDriveType.test(transtype)) {
                        alert('Please enter a valid transmission type ');
                        return false;
                    }
                }

                if (regOwner == '') { }
                else {
                    if (!regexRegOwner.test(regOwner)) {
                        alert('Please enter a valid registered owner name ');
                        return false;
                    }
                }

                if (perId == null) {
                    perId = 0;
                }

                var maindriver_test = false;
                if (trn == '') { }
                else {
                    if (!regexTRN.test(trn)) {
                        alert('Please enter a valid TRN. TRN Must be a nine digit number, beginning with a 0 or 1');
                        return false;
                    }
                    else maindriver_test = true;
                }

                if (maindriver_test) {
                    if (fname == '' || !regex.test(fname)) {
                        alert('Please enter a valid first name.');
                        return false;
                    }

                    if (mname == '') { }
                    else {
                        if (!regex1.test(mname)) {
                            alert('Please enter a valid middle name.');
                            return false;
                        }
                    }

                    if (lname == '' || !regex.test(lname)) {
                        alert('Please enter a valid last name.');
                        return false;
                    }

                    if (roadno == '') { }
                    else {
                        if (!regex2.test(roadno)) {
                            alert('Please enter a valid road number.');
                            return false;
                        }
                    }

                    if (aptNumber != '' && !regexAptNum.test(aptNumber)) {
                        alert('Please enter a valid apartment/building number.');
                        return false;
                    }

                    //                    if (road == '' || !regex3.test(road)) {
                    //                        alert('Please enter a valid road name.');
                    //                        return false;
                    //                    }

                    if (roadType == '') { }
                    else {
                        if (!regex.test(roadType)) {
                            alert('Please enter a valid road type.');
                            return false;
                        }
                    }

                    if (zip == '') { }
                    else {
                        if (!regex2.test(zip)) {
                            alert('Please enter a valid zip.');
                            return false;
                        }
                    }


                    if (city == '' && !regexCity.test(city)) {
                        alert('Please enter a valid city.');
                        return false;
                    }

                    if (parish == 0) {
                        alert('Please select a parish.');
                        return false;
                    }
                    if (est_val != '' && !regexEstimatedValue.test(est_val)) {
                        alert('Please enter a valid estimated value. Eg: $78 0r 78');
                        return false;
                    }

                    if (country == '' || !regex4.test(country)) {
                        alert('Please enter a valid country name.');
                        return false;
                    }
                }
                else {
                    /*
                    if (fname != '' || mname != '' || lname != '' || roadno != '' || aptNumber != '' || road != '' || roadType != '' || zip != '' || city != '' || country != '') {
                    alert('You have filled in a number elements of data for the Main Driver, but not his TRN. Please add a TRN to the Main Driver section, or clean out all the related data');
                    return false;
                    }
                    */
                }

                var parms =
                    {
                        vin: vin,
                        chassis: chassis,
                        make: make,
                        model: model,
                        modelType: modelType,
                        bodyType: bodyType,
                        Extension: Extension,
                        vehicleRegNumber: vehicleRegNumber,
                        colour: colour,
                        cylinders: cylinders,
                        engineNo: engineNo,
                        engineModified: engineModified,
                        engineType: engineType,
                        estAnnualMileage: estAnnualMileage,
                        estimatedvalue: est_val,
                        expiryDateOfFitness: expiryDateOfFitness,
                        HPCCUnitType: HPCCUnitType,
                        ReferenceNo: ReferenceNo,
                        RegistrationLocation: RegistrationLocation,
                        RoofType: RoofType,
                        seating: seating,
                        seatingCert: seatingCert,
                        tonnage: tonnage,
                        VehicleYear: VehicleYear,
                        handDriveType: handDriveType,
                        transtype: transtype,
                        regOwner: regOwner,
                        restrictedDriving: restrictedDriving,

                        perId: perId,
                        trn: trn,
                        fname: fname,
                        mname: mname,
                        lname: lname,
                        roadno: roadno,
                        aptNumber: aptNumber,
                        road: road,
                        roadType: roadType,
                        zipcode: zip,
                        city: city,
                        country: country,
                        email: emails,
                        primary: primary,
                        parish: parish,

                        advanced: advanced,

                        prevChassis: prevChassis,
                        prevRegNo: prevRegNo
                    };

                $.ajax({
                    type: "POST",
                    traditional: true,
                    url: "/Vehicle/AddVehicles/",
                    async: false,
                    data: parms,
                    dataType: "json",
                    success: function (data) {
                        if (!data.result && data.regExist) {

                            var prevRegNo = $("#prevRegNo").val();
                            var prevChassis = $("#prevChassis").val();
                            var currentChassis = $("#chassis").val();
                            var RegNoString = $("#vehicleRegNum").val();

                            if (prevChassis == "" ||
                            (prevRegNo.trim() != RegNoString.trim() && prevChassis.trim() != currentChassis.trim()) ||
                            (prevRegNo.trim() != RegNoString.trim() && prevChassis.trim() == currentChassis.trim()) ||
                            (prevRegNo.trim() == RegNoString.trim() && prevChassis.trim() != currentChassis.trim()))
                                $("<div> The Vehicle Registration Number is already in use.</div>").dialog({
                                    modal: true,
                                    title: "Alert",
                                    buttons: {
                                        Ok: function () {
                                            $(this).dialog("close");
                                        }
                                    }
                                });
                        }
                        else {
                            $("#vehicle-modal1").html("");
                            $("#vehicle-modal1").dialog("close");
                            AddVehicleSelected(data.RiskID, 1);
                        }
                    }
                });

            }, // END OF BUTTON 'CREATE'

            Cancel: function () {
                $("#vehicle-modal1").dialog("close");
                $("#vehicle-modal1").html("");
                updateVehicleView();
            }
        }   // END OF BUTTONS
    });                  // END CREATE VEHICLE DIALOG

    /*
    $("#vehicle-modal1").load('/Vehicle/AddNewVehicle?PolCoverID=' + $(".covers_dropdown").val(), function () {
        // This part runs 'on-load' of the dialog 
        var names = [];
        var shownNames = [];

        names = get_names();

        $("#vehicle-modal1").find("#regOwner").autocomplete({
            source: function (request, response) {
            for (var i = 0; i < names.length; i++) {
                if (names[i].toLowerCase().indexOf($("#regOwner").val().toLowerCase()) > -1) shownNames.push(names[i]);
            }
            response(shownNames);
            shownNames = [];
            }
        });

        $(".insured .base_per_id").each(function () {
            $("#maindriver-dropdown").append($("<option></option>").val($(this).val()).text($(this).parents(".insured").find(".names").val()));
        });

    }).dialog('open');
    $("#vehicle-modal1").html("");
    */
    /*
    $("#vehicle-model1").find("#maindriver-dropdown").append(function () {
        var options = "<option value\"test\">test</option>";
        $(".insured .perId").each(function () {
            
        });
        return options;
    });
    */
    
      

} // END OF FUNCTION 'vehCreate'


function get_names() {
    var values = []
    $(".insured").each(function () {
        values.push($(this).find(".names").val());
    });
    $(".company").each(function () {
        values.push($(this).find(".companyNames").val());
    });
    return values;
}
/*
function add_vehicle_ajax(policy_id, vehicle_id) {
    $.ajax({
        url: '/Policy/AddRiskToPolicy/' + policy_id + '?veh_id=' + vehicle_id,
        cache: false,
        async:false,
        success: function (json) {
            if (json.result) {
                //place bubble
                vehicle_success_save(json.chassis, 'add');
            }
            else {
                //error bubble
                scrub_vehicle(vehicle_id, 'add');
            }
        }
    });
}

function rm_vehicle_ajax(policy_id, vehicle_id) {
    $.ajax({
        url: '/Policy/RmRiskToPolicy/' + policy_id + '?veh_id=' + vehicle_id,
        cache: false,
        async:false,
        success: function (json) {
            if (json.result) {
                //place bubble
                vehicle_success_save(json.chassis, 'rm');
            }
            else {
                //error bubble
                scrub_vehicle(vehicle_id, 'rm');
            }
        }
    });
}

function scrub_vehicle(vehicle_id, type) {
    $(".newvehicle").each(function () {
        if ($(this).find(".vehId").val() == vehicle_id) {
            var message_type = "";
            if (type == 'add') message_type = 'add_err';
            if (type == 'rm') message_type = 'rm_err';
            vehicle_success_save($(this).find(".chassis").val(), message_type);
            $(this).remove();
            return false;
        }
    });
}


function vehicle_success_save(message, type) {
    var name = "";
    switch (type) {
        case 'add':
            message = "The risk \"" + message + "\" was added to the policy";
            name = "success-message";
            break;
        case 'rm':
            message = "The risk \"" + message + "\" was removed from the policy";
            name = "remove-message";
            break;
        case 'add_err':
            message = "There was an error in adding the risk \"" + message + "\" to the policy. Please try again.";
            name = "error-message";
            break;
        case 'rm_err':
            message = "There was an error in removing the risk \"" + message + "\" from the policy. Please try again.";
            name = "error-message";
            break;
        case 'upd_use':
            message = "This vehicle's usage has been updated";
            name = "success-message";
            break;
        case 'upd_use_err':
            message = "There was an error in saving the risk's usage. Please try again.";
            name = "error-message";
            break;
        case 'upd_mort':
            message = "This vehicle's mortgagee has been updated";
            name = "success-message";
            break;
        case 'upd_mort_err':
            message = "There was an error in saving the mortgagee. Please try again.";
            name = "error-message";
            break;
        default:
            break;
    }
    var success = "<div class=\"" + name + "\">" + message + "</div>";
    $(".policy-risk").find('.processed-responses').append(success);
    clean_up_server_responses_vehicles();
}

function clean_up_server_responses_vehicles() {
    $(".policy-risk .processed-responses").find(".success-message:hidden,.error-message:hidden").remove();
    $(".policy-risk .processed-responses").children().each(function () {
        var div = $(this);
        setTimeout(function () {
            vehicle_response_fade_out(div);
        }, 5000);
    });
}

function vehicle_response_fade_out(e) {
    $(e).fadeOut("slow");
}
*/
function testVehiclePolicy(vehID, start, end,e) {
    $.ajax({
        url: '/Vehicle/VehiclePolicyTest/?vehID=' + vehID + '&start=' + start + '&end=' + end,
        cache: false,
        async: false,
        success: function (json) {
            if (json.result && json.returnData) {
                $(e).addClass('underPolicy');
                $(e).find('.blurDiv').addClass('ui-widget-overlay');
                //$(e).addClass('ui-widget-overlay');

                $(e).find('.vup_mortgagee').prop('readonly', true);
                $(e).find('select').prop('disabled', true);
                $(e).find('#cancelPol').prop('disabled', true);
                if ($(e).find('.messageCompAdmins').length > 0) $(e).find('.messageCompAdmins').prop('disabled', true);
                /*
                $(e).find('input').each(function () {
                    $(this).prop('disabled', true);
                });
                $(e).find('input[type=text]').each(function () {
                    $(this).prop('readonly', true);
                });
                //$(e).find('inout').prop('disabled', true);
                $(e).find('select').each(function () {
                    $(this).prop('disabled', true);
                });
                */
                var proceedBtn = '<a href=\"#\" class=\"proceed\" onclick=\"proceed(this); return false;\">Proceed</a>';
                $(e).find('.vehicleFooter').append(proceedBtn);
                //$(e).find('select').prop('disabled', true);
                //$('.newvehicle input,.newvehicle select').prop('disabled', true)
            }
            else if (json.result && !json.returnData) {
                $(e).addClass('autoCancelled');
                if ($("#page").val() == "edit") {
                    //add_vehicle_ajax($("#hiddenid").val(), vehID);
                    /*
                    $.ajax({
                        url: '/Policy/SendDblRiskInsured/?vehID=' + vehID + '&polID=' + $("#hiddenid").val() + '&compID=' + $("#compid").val() + '&start=' + start + '&end=' + end + '&autoCancel=' + true,
                        cache: false
                    });
                    */
                }
            }
            else {
                //if ($("#page").val() == "edit") add_vehicle_ajax($("#hiddenid").val(), vehID);
            }
        }
    });
    return $(e);
}

function proceed(e) {
    $('#Continue').val("Proceed");
    $('<div>You are about to add this vehicle to the policy. Are you sure you want to do this?</div>').dialog({
        modal: true,
        title: 'Alert!',
        buttons: {
            Yes: function () {
                $(this).dialog('close');
                var vehicleCard = $(e).parents('.newvehicle');
                $(vehicleCard).removeClass('underPolicy');
                $(vehicleCard).find('.blurDiv').removeClass('ui-widget-overlay');

                $(vehicleCard).find('.vup_mortgagee').prop('readonly', false);
                $(vehicleCard).find('select').prop('disabled', false);
                $(vehicleCard).find('#cancelPol').prop('disabled', false);
                if ($(vehicleCard).find('.messageCompAdmins').length > 0) $(vehicleCard).find('.messageCompAdmins').prop('disabled', false);

                $(vehicleCard).find('.proceed').remove();
                /*
                if ($("#page").val() == "edit") {
                    add_vehicle_ajax($("#hiddenid").val(), $(vehicleCard).find('.vehId').val());

                    $.ajax({
                        url: '/Policy/SendDblRiskInsured/?vehID=' + $(vehicleCard).find('.vehId').val() + '&polID=' + $("#hiddenid").val() + '&compID=' + $("#compid").val() + '&start=' + $('#start').val() + '&end=' + $('#end').val() + '&autoCancel=' + false,
                        cache: false
                    });
                }
                */
            },
            No: function () {
                $(this).dialog('close');
            }

        }
    });

    
}
