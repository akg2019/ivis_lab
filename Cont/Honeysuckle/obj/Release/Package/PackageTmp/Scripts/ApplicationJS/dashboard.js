﻿$(function () {
    if ($(".companies-list")) {
        $.ajax({
            url: '/Company/CompaniesList/',
            cache: false,
            success: function (html) {
                $(".companies-list").html(html);
                update_scroll();
            },
            error: function () {
                $("<div></div>").text("There was an error in loading this partial").dialog({
                    modal: true,
                    title: "Error Loading Partial",
                    buttons: {
                        Ok: function () {
                            $(this).dialog('close');
                        }
                    }
                })
            }
        });
    }

    if ($(".cover-list")) {
        $.ajax({
            url: '/Vehicle/VehicleCertsNotesList/',
            cache: false,
            success: function (html) {
                $(".cover-list").html(html);
                update_scroll();
                if (window.location.href.includes('#container-covercerts')) window.location.replace("/#container-covercerts");
            },
            error: function () {
                $("<div></div>").text("There was an error in loading this partial").dialog({
                    modal: true,
                    title: "Error Loading Partial",
                    buttons: {
                        Ok: function () {
                            $(this).dialog('close');
                        }
                    }
                })
            }
        });
    }

    if ($(".business-intelligence")) {

        $.ajax({
            url: '/BI/AllGauges/',
            cache: false,
            success: function (html) {
                $(".business-intelligence").html(html);
                create_gauges();
            },
            error: function () {
                $("<div></div>").text("There was an error in loading this partial").dialog({
                    modal: true,
                    title: "Error Loading Partial",
                    buttons: {
                        Ok: function () {
                            $(this).dialog('close');
                        }
                    }
                })
            }
        });
    }

});

function update_scroll() {
    $('.table-data').perfectScrollbar({
        wheelSpeed: 0.3,
        wheelPropagation: true,
        minScrollbarLength: 10,
        suppressScrollX: true
    });
}