﻿$(function () {
    $('html').on('blur', '.newemail .email', function () {
        var email = $(this).val();
        var username = $('#username').val();
        var mail = $(this);
        $('.newemail .read-only').each(function () {
            if ($(this).val() == email) {
                EmailInUse(mail);
            }
        });
        if (username == email) {
            EmailInUse(mail);
        }
        else testEmailUsernames(email,mail);
    });
});


$(function () {
    if ($('#emailExists').html().length > 0) {
        $('#emailExists').dialog({
            modal: true,
            title: 'Alert!',
            buttons: {
                OK: function () {
                    $(this).dialog('close');
                }
            }
        });
    }
});

function EmailInUse(mail) {
    $('<div>You are already using this email.</div>').dialog({
        modal: true,
        title: 'Email in Use!',
        buttons: {
            OK: function () {
                $(this).dialog('close');
                mail.focus();

            }
        }
    });
}


function testEmailUsernames(email,mail) {
    $.ajax({
        url: '/User/TestUsername/?username=' + email,
        cache: false,
        success: function (data) {
            if (data.error == 0) {
                if (data.result == 1) {
                    $("<div>This email is already in use<div>").dialog({
                        modal: true,
                        title: 'Alert!',
                        buttons: {
                            OK: function () {
                                $(this).dialog("close");
                                $(this).dialog('close');
                                mail.focus();
                            }
                        }
                    });
                }
            }
        }
    });
}