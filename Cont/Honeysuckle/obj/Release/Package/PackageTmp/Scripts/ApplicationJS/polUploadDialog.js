﻿$(function () {
    $('.policyUpload').change(function () {
        var fileSize = $('.policyUpload')[0].files[0].size;
        //2453432
        //209715200
        //104857600
        // alert(fileSize);
        if (fileSize <= 104857600)
            $('#fileSubmit').prop('disabled', false);
        else {
            $('#fileSubmit').prop('disabled', true);
            $("<div>File size to be uploaded must be 100 MB or less</div>").dialog({
                modal: true,
                title: 'Alert!',
                buttons: {
                    OK: function () {
                        $(this).dialog('close');
                    }
                }
            });
        }
    });
});