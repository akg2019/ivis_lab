﻿$(function () {
    $("#add_wording").on('click', function () {
        $("<div></div>").load("/AuthorizedDriverWording/New/").dialog({
            title: 'New Wording',
            modal: true,
            buttons: {
                Create: function () {
                    // send to controller and show on page
                    var wording = $(".wording-textarea").val().trim();
                    var dia = $(this);
                    if (wording.length > 0) {
                        $.ajax({
                            method: "POST",
                            url: '/AuthorizedDriverWording/Create/',
                            data: { wording: wording },
                            success: function (json) {
                                if (json.result) {
                                    $(".ivis-data").append(build_wording_html(json.word_id, wording));
                                    $(dia).dialog('close');
                                    $(dia).dialog('destroy');
                                }
                                else {
                                    $(dia).dialog('close');
                                    $(dia).dialog('destroy');
                                }
                            },
                            error: function () {
                            }
                        });
                    }
                    else {
                        alert("Please enter some text into the dialog.");
                        return false;
                    }
                },
                Cancel: function () {
                    $(this).dialog('close');
                    $(this).dialog('destroy');
                }
            }
        });
    });
});

function build_wording_html(id, wording) {
    var html = "";
    if ($(".row:last").hasClass("even")) html += "<div class=\"data row odd auth\">";
    else html += "<div class=\"row even data auth\">";
    html += "<input class=\"word_id id\" type=\"hidden\" value=\"" + id +"\" />";
    html += "<div class=\"item word\">";
    html += wording;
    html += "</div>";
    html += "<div class=\"item functions\">";
    html += "<div class=\"icon delete\" title=\"Delete Authorized Driver Wording\">";
    html += "<img src=\"../../Content/more_images/delete_icon.png\" class=\"sprite\" />";
    html += "</div>";
    html += "</div>";
    html += "</div>";
    return html;
}