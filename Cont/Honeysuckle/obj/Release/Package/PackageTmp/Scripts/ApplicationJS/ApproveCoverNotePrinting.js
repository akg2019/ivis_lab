﻿document.getElementById("heading").style.width = "408px";
document.getElementById("sendto").style.width = "408px";

$("#sendto").prop('readonly', true);
$("#heading").val("Cover Note Print Approval Request");
$("#content").val("Requesting approval to print this cover note: " + window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '') + "/VehicleCoverNote/Details/" + $("#coverNoteID").val());
$("#heading").prop('readonly', true);

var vehicleCoverNoteID = $("#veh").val();

$("#veh").hide();
var admins = [];

$.getJSON("/VehicleCoverNote/CompanyAdmins/" + vehicleCoverNoteID, function (data) {
    if (data.result) {
        admins = data.admins;
        $("#sendto").val(admins);
    }
});