﻿$(function () {
    $(".fileupload").fileupload({
        url: "/TemplateWording/UploadJSON/",
        type: "POST",
        dataType: 'json',
        add: function (e, data) {
            if (data.files[0].size < 5120000) {
                data.formData = { type: get_type($(e.target)) };
                data.submit();
            }
            else {
                $("<div>Please pick a file that is less than 5MB</div>").dialog({
                    title: "File Size Error!",
                    modal: true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }
        },
        progressall: function (e, data) {
            StartSpinner();
        },
        done: function (e, data) {
            StopSpinner();
            if (data.result.result) {
                var html_responses_success = [];
                var html_responses_error = [];
                var responses = data.result.response;
                var fileInput = $(this);
                $.each(data.files, function (index, file) {
                    fileInput.parents('.file-info').find('.filename').text(file.name);
                });


                for (var i = 0; i < responses.length; i++) {
                    if (responses[i].type == "success") html_responses_success.push(responses[i])
                    if (responses[i].type == "error") html_responses_error.push(responses[i])
                }
                $(".response").remove();
                $(".response-div").html(convert_array_to_html_responses(html_responses_success, html_responses_error));
                $(".data").hide();
            }
            else {
                $("<div></div>").text("There was an error in uploading your file. Please reload the page and try again. If the proble persists please contact your administrator.").dialog({
                    title: "Error",
                    modal: true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog('close');
                        }
                    }
                });
            }
        }
    });
});

function get_type(e){
    return ($(e).hasClass('wording')? 'wording' : 'usage');
}

function convert_array_to_html_responses(success, error){
    var html = "";
    html += "<div id=\"response\">";
    html += "<div class=\"stripes\"></div>";
    if (success.length == 0 && error.length == 0) html += "<div class=\"message empty\">There was no response from the server. Maybe you had no content in the file you uploaded.</div>";
    else {
        html += "<div class=\"message empty\">Your file has been processed and here are the relevant responses from the server.</div>";
        html += "<div class=\"responses\">";
            html += "<div class=\"title\"><div>There were " + error.length + " </div><div class=\"error\">errors</div></div>";
            html += "<div class=\"data\">";
                for(var i = 0; i < error.length; i++){
                    html += "<div class=\"message error\">" + error[i].message + "</div>";
                }
            html += "</div>";
        html += "</div>";
        html += "<div class=\"responses\">";
            html += "<div class=\"title\"><div>There were " + success.length + " </div><div class=\"success\">successes</div></div>";
            html += "<div class=\"data\">";
                for(var j = 0; j < success.length; j++){
                    html += "<div class=\"message success\">" + success[j].message + "</div>";
                }
            html += "</div>";
        html += "</div>";
    }
    html += "</div>";

    return html;
}