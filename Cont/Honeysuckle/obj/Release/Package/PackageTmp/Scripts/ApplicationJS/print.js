﻿function update_last_printed_by() {
    $.ajax({
        url: '/User/my_name_json/',
        cache: false,
        success: function (json) {
            if (json.result) {
                if ($(".print-count").val() == 0 || $(".print-count").val() == "") {
                    $(".print-count").val(1);
                    $(".first-print").val(json.name);
                }
                else $(".print-count").val(parseInt($(".print-count").val()) + 1);
                $(".last-print").val(json.name);
            }
        }
    });
}