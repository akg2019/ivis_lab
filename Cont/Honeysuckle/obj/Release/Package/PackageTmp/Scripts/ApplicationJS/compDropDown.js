﻿var val;
var errormessage = "<div>There was an error upon updating the company's groups. Please try again. If the problem persists please contact your administrator</div>";

function changeComp(e) {
    $(e).children("option").each(function () {
        if ($(this).prop('selected')) {
            val = $(this).val();
            return;
        }
    });
};
function updateSuperGroupList(comp){
    $.ajax({
        url:'/UserGroup/SuperGroupListBoxUserGroups/?comp='+comp,
        cache:false,
        success: function(data){
            $("#allgroups").replaceWith(data);
        },
        error: function(){
            $("#allgroups").children().each(function(){
                $(this).hide();
            });
            $(errormessage).dialog({
                modal:true,
                buttons:{
                    Ok: function(){
                        $(this).dialog('close');
                    }
                }
            });
        } 
    });
    $.ajax({
        url:'/UserGroup/SuperGroupMyListBoxUserGroups/?comp='+comp,
        cache:false,
        success: function(data){
            $("#mygroups").replaceWith(data);
        },
        error: function(){
            $("#mygroups").children().each(function(){
                $(this).hide();
            });
            $(errormessage).dialog({
                modal:true,
                buttons:{
                    Ok: function(){
                        $(this).dialog('close');
                    }
                }
            })
        } 
    });
}

$(function () {
    $("#compDropDown").chosen();
    changeComp($("#compDropDown"));
    $("#compDropDown").on('change', function () {
        changeComp($(this));
        updateSuperGroupList(val);
    });
});