﻿

var trnFocus = false;
var trnSet = false;

$(function () {
    if (typeof $("#trn").val() != 'undefined')
    if( $("#trn").val().trim().length > 0 ) {
        trnSet = true;
    }
});


$(function () {
    $("#trn").focus(function () {
        trnFocus = true;
    })
});

$(function () {
    $("#trn").blur(function () {
        if (trnFocus) {
            var test = $("#trn").val();
            var result = false;
            $.getJSON("/Vehicle/GetUserByTRN/" + test.trim(), function (data) {
                if (!data.error) {
                    if (data.result) {
                        //something exists
                        if (data.person) {

                            //There is a person
                            $("#InformationDialog").dialog({
                                modal: true,
                                width: 430,
                                autoOpen: false,
                                title: "Person Exists",
                                height: 350,
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog("close");
                                        $(this).html("");
                                        $(this).dialog("destroy");
                                        $("#InformationDialog").html("");

                                        // Adding the selected driver to the drivers list
                                        AddPersonSelected(data.personid, 1);

                                        $(".ui-dialog-content").each(function () {

                                            if ($(this).attr('id') == "createDriver") {
                                                $(this).dialog("close");
                                                $(this).html("");
                                                $(this).dialog("destroy");
                                                $("#InformationDialog").html("");
                                            }

                                        });


                                        //$($(".ui-dialog-content")[1]).html("");
                                        // $($(".ui-dialog-content")[1]).dialog("destroy"); //Removing the driver create modal

                                    },

                                    Cancel: function () {
                                        $("#trn").val("");
                                        $(this).dialog("close");
                                        $(this).html("");
                                        $(this).dialog("destroy");
                                        $("#InformationDialog").html("");
                                    }
                                }
                            });
                            $("#InformationDialog").load('/People/FullDriverDetails/' + data.personid + "?create=" + true, function (value) { }).dialog('open');

                        }
                    }

                    else {
                        //TRN FREE
                        if (test.trim().length != 0) trnSet = true;
                        $("#country").val("Jamaica");
                        $("#country").trigger('change');
                    }
                }
                else {
                    //ERROR
                    $("<div>" + data.message + "</div>").dialog({
                        modal: true,
                        Title: "Alert",
                        buttons: {
                            Ok: function () {
                                $("#trn").val("");
                                $(this).dialog("close");
                            }
                        }
                    });
                }

            });
            trnFocus = false;
        }
    })
});

