﻿var activesuccessmessage = "<div>The user was successfully activated</div>";
var deactivesuccessmessage = "<div>The user was successfully deactivated</div>";
var activefailmessage = "<div>There was a problem with the activation of this user. Please check that there is no active user with the same TRN in the system</div>";
var deactivefailmessage = "<div>There was a problem with the deactivation of this user.</div>";
var deactivatebtn = "<button onclick=\"deactivateUser(this)\" class=\"activedeactivebtn\" >Deactivate User</button>";
var activebtn = "<button onclick=\"activateUser(this)\" class=\"activedeactivebtn\" >Activate User</button>";
var deactivatelink = "<li><a href=\"#\" class=\"reset\" onclick=\"deactivateUser(this);\">Deactivate User</a></li>";
var activelink = "<li><a href=\"#\" class=\"reset\" onclick=\"activateUser(this);\">Activate User </a></li>";


var deactivebtn_modifypage = "<input type=\"button\" onclick=\"deactivateUser(this)\" class=\"activedeactivebtn\" value=\"Deactivate User\" />";
var activebtn_modifypage = "<input type=\"button\" onclick=\"activateUser(this)\" class=\"activedeactivebtn\" value=\"Activate User\" />";

var resetPass = false;
var resetLogin = false;
var resetPassLink;
var resetLoginLink;

function deactivateUser(e, type) {
    var modify = false;
    var id = $(e).parents('fieldset').find('.userid').val();
    modify = true;

    var promptAdd = "";
    var IAJcreated = false;
    IAJcreated = $('.isIAJcreated').val() == "True";
    if (IAJcreated) promptAdd = "The user was created by an IAJ administrator and is a company administrator.";

    $("<div>You are about to deactivate this user." + promptAdd + " Are you sure you want to do this? </div>").dialog({
        title: 'Alert!',
        modal: true,
        buttons: {
            OK: function () {

                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
                StartSpinner();
                $.getJSON('/User/DeactivateUser/' + id, function (data) {
                    if (data.result) {
                        //worked
                        //switch buttons
                        if (modify) $(e).parent("li").remove();
                        $(".user-li-links").append(activelink);

                        if ($('.reset-question').length > 0) {
                            resetPass = true;
                            resetPassLink = $('.reset-question').parent();
                            $('.reset-question').parent().remove();
                        }
                        if ($('.reset-password').length > 0) {
                            resetLogin = true;
                            resetLoginLink = $('.reset-password').parent();
                            $('.reset-password').parent().remove();
                        }
                        StopSpinner();

                        //display dialog
                        $(deactivesuccessmessage).dialog({
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog("close");
                                    if (IAJcreated) {
                                        var url = '/User/Create/?UserCompId=' + $('.cid').val();
                                        $("<div>Do you want to create a new administrator for this company now?</div>").dialog({
                                            modal: true,
                                            title: 'Alert!',
                                            buttons: {
                                                Yes: function () {
                                                    window.location.replace('/User/Create/?UserCompId=' + $('.cid').val());
                                                    $(this).dialog("close");
                                                },
                                                No: function () {
                                                    $(this).dialog("close");
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                    else {
                        //ERROR
                        $(activefailmessage).dialog({
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }
                });
            },
            Cancel: function () {
                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
            }
        }
    });
}

function activateUser(e, type) {
    var id = $(e).parents('fieldset').find('.userid').val();

    $("<div>You are about to activate this user. Are you sure you want to do this? </div>").dialog({
        title: 'Alert!',
        modal: true,
        buttons: {
            OK: function () {
                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
                StartSpinner();
                $.getJSON('/User/ActivateUser/' + id, function (data) {
                    if (data.result) {
                        //worked
                        //switch buttons
                        $(e).parent("li").remove();

                        if (resetPass) $(".user-li-links").append(resetPassLink);
                        if (resetLogin) $(".user-li-links").append(resetLoginLink);

                        $(".user-li-links").append(deactivatelink);
                        StopSpinner();
                        //display dialog
                        $(activesuccessmessage).dialog({
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }
                    else {
                        //ERROR
                        $(deactivefailmessage).dialog({
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }
                });
            },
            Cancel: function () {
                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
            }
        }
    });
}


function resendpasword(e) {
    var id = $(e).parents("fieldset").children(".hiddenid").val(); 
    $("<div>You are about to reset this user's password. Are you sure you want to do this? </div>").dialog({
        title: 'Alert!',
        modal: true,
        buttons: {
            OK: function () {
                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
                $.ajax({
                    url: '/User/ResendPassword/' + id,
                    cache: false,
                    success: function (data) {
                        if (data.result) {
                            $("<div>A new password has been sent to the user.</div>").dialog({
                                title: 'Success',
                                modal: true,
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                        else {
                            $("<div>There was an error with your request. If the problem persists please contact your administrator.</div>").dialog({
                                title: 'Error',
                                modal: true,
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                    },
                    error: function () {
                        $("<div>There was an error with your request. If the problem persists please contact your administrator.</div>").dialog({
                            title: 'Error',
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                });
            },
            Cancel: function () {
                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
            }
        }
    });
}


function resetquestion(e) {
    var id = $(e).parents("fieldset").children(".hiddenid").val();
    $("<div>You are about to reset this user's questions. Are you sure you want to do this? </div>").dialog({
        modal: true,
        title: 'Alert!',
        buttons: {
            OK: function () {
                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
                $.ajax({
                    url: '/User/ResetQuestions/' + id,
                    cache: false,
                    success: function (data) {
                        if (data.result) {
                            $("<div>The user's security question has been reset.</div>").dialog({
                                modal: true,
                                title: 'Success',
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                        else {
                            $("<div>There was an error with your request. If the problem persists please contact your administrator.</div>").dialog({
                                modal: true,
                                title: 'Error',
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                    },
                    error: function () {
                        $("<div>There was an error with your request. If the problem persists please contact your administrator.</div>").dialog({
                            title: 'Error',
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                });
            },
            Cancel: function () {
                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
            }
        }
    });
}

function resetPassword(){
    var id = $(".hiddenid").val(); 
    $("<div>You are about to reset this user's password. Are you sure you want to do this? </div>").dialog({
        title: 'Alert!',
        modal: true,
        buttons: {
            OK: function () {
                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
                $.ajax({
                    url: '/User/ResendPassword/' + id,
                    cache: false,
                    success: function (data) {
                        if (data.result) {
                            $("<div>A new password has been sent to the user.</div>").dialog({
                                title: 'Success',
                                modal: true,
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                        else {
                            $("<div>There was an error with your request. If the problem persists please contact your administrator.</div>").dialog({
                                title: 'Error',
                                modal: true,
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                    },
                    error: function () {
                        $("<div>There was an error with your request. If the problem persists please contact your administrator.</div>").dialog({
                            title: 'Error',
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                });
            },
            Cancel: function () {
                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
            }
        }
    });
}

function resetQuest() {
    var id = $(".hiddenid").val();
    $("<div>You are about to reset this user's questions. Are you sure you want to do this? </div>").dialog({
        modal: true,
        title: 'Alert!',
        buttons: {
            OK: function () {
                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
                $.ajax({
                    url: '/User/ResetQuestions/' + id,
                    cache: false,
                    success: function (data) {
                        if (data.result) {
                            $("<div>The user's security question has been reset.</div>").dialog({
                                modal: true,
                                title: 'Success',
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                        else {
                            $("<div>There was an error with your request. If the problem persists please contact your administrator.</div>").dialog({
                                modal: true,
                                title: 'Error',
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                    },
                    error: function () {
                        $("<div>There was an error with your request. If the problem persists please contact your administrator.</div>").dialog({
                            modal: true,
                            title: 'Error',
                            buttons: {
                                Ok: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                });
            },
            Cancel: function () {
                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
            }
        }
    });
}