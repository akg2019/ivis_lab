﻿
document.getElementById("heading").style.width = "408px";
document.getElementById("sendto").style.width = "408px";

$("#sendto").prop('readonly', true);
$("#heading").val("Certificate Print Approval Request");
$("#content").val("Requesting approval to print this certificate: " + window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '') + "/VehicleCertificate/Details/" + $("#veh").val());
$("#heading").prop('readonly', true);

var vehicleCertID = $("#veh").val();

$("#veh").hide();
var admins = [];

$.getJSON("/VehicleCertificate/CompanyAdmins/" + vehicleCertID, function (data) {
    if (data.result) {
        admins = data.admins;
        $("#sendto").val(admins);
    }
});