﻿var successmessage = "<div>Role was successfully deleted</div>";
var errormessage = "<div>There was an error</div>";

$(function () {

    $("#user-groups").perfectScrollbar({
        wheelSpeed: 0.3,
        wheelPropagation: true,
        minScrollbarLength: 10,
        suppressScrollX: true
    });

    if ($('#newUser').text().trim().length != 0) {
        $("#newUser").dialog({
            modal: true,
            autoOpen: false,
            title: "Alert!",
            buttons: {
                OK: function () {
                    $('#newUser').dialog('close');
                    $('#newUser').html("");
                    $('#newUser').dialog('destroy');
                }
            }
        });
        $('#newUser').dialog('open');
    }


});

function NewRole() {
    window.location.replace('/RoleManagement/Create/');
}

function Upload() {
    window.location.replace('/RoleManagement/Upload/');
}
/*
function DeleteRole(id, e) {
  
   $("<div>You are about to delete this role. Are you sure you want to do this? </div>").dialog({
      title: 'Alert!',
      buttons: {
        OK: function () {
            $(this).dialog('close');
            $.ajax({
                url: '/RoleManagement/Delete/' + id,
                cache: false,
                success: function (data) {
                    if (data.result) {
                        $(e).remove();
                        $(successmessage).dialog({
                            title: 'Role Deleted',
                            buttons: {
                                Ok: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                    else {
                        $(errormessage).dialog({
                            title: 'Error!',
                            buttons: {
                                Ok: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                },
                error: function () {
                    $(errormessage).dialog({
                        title: 'Error!',
                        buttons: {
                            Ok: function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                }
            });
            },
            Cancel: function () {
                $(this).dialog('close');
                $(this).dialog('destroy');
           }
         }
     });
  }
  */