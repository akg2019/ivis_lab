﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class ResponseController : Controller
    {
        //
        // GET: /Response/

        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }

        public ActionResult ParseResponse(List<response> responses = null)
        {
            if (responses == null) responses = new List<response>();
            return View(responses);
        }

    }
}
