﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using System.Web.Script.Serialization;

namespace Honeysuckle.Controllers
{
    public class UsageController : Controller
    {

        /// <summary>
        /// This function is used to create a usage
        /// </summary>
        /// <param name="id">Usage Id</param>
        public ActionResult Create(int id = 0, int cid = 0, bool details = false)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Usages", "Create")))
            {
                Usage usage = new Usage();
                ViewData["cid"] = cid;
                if (id != 0) usage = UsageModel.GetUsage(id, cid);
                return View(usage);
            }
            else return new EmptyResult();
        }

        /// <summary>
        /// This function is to view usages
        /// </summary>
        /// <param name="id">Usage Id</param>
        public ActionResult Details(int id = 0, int cid = 0,bool details = false)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Usages", "Read")))
            {
                Usage usage = new Usage();
                ViewData["cid"] = cid;
                ViewData["Details"] = details;
               // ViewData["details"] = details;
                if (id != 0) usage = UsageModel.GetUsage(id, cid);
                return View(usage);
            }
            else return new EmptyResult();
        }

        public ActionResult Delete(int id = 0, int cid = 0)
        {
            if (id != 0 && cid != 0) return Json(new { result = UsageModel.RmUsageFromCover(new Usage(id), new PolicyCover(cid)) }, JsonRequestBehavior.AllowGet);
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0, string usage = null)
        {
            if (id != 0 && usage != null) return Json(new { result = UsageModel.EditUsage(new Usage(id,usage)) }, JsonRequestBehavior.AllowGet);
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// This function deals with the dropdown of usages
        /// </summary>
        /// <param name="coverID">Policy Cover ID</param>
        public ActionResult UsageDropdown(int coverID = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Create"))|| UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Create")))
            {
                if (coverID != 0)
                {
                    List<Usage> usages = UsageModel.GetPolicyCoverUsages(new PolicyCover(coverID));
                    int i, j;
                    Usage key = new Usage();
                    for (i = 1; i < usages.Count(); i++)
                    {
                        key = usages[i];
                        j = i - 1;


                        while (j >= 0 && usages[j].usage.CompareTo(key.usage) > 0)
                        {
                            usages[j + 1] = usages[j];
                            j = j - 1;
                        }
                        usages[j + 1] = key;
                    }

                    return View(usages);
                }
                else
                {
                    List<Usage> usages = new List<Usage>();
                    return View(usages);
                }
            }
            else return new EmptyResult();

        }

        /// <summary>
        /// This function is used to retrieve all usages associated with a particular policy cover
        /// </summary>
        /// <param name="id">Policy Cover Id</param>
        public ActionResult CoverUsages(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Create")) && id != 0) return View(UsageModel.GetPolicyCoverUsages(new PolicyCover(id)));
            else return new EmptyResult();
        }


        /// <summary>
        /// This function deals with the dropdown of usages
        /// </summary>
        /// <param name="coverID">Policy Cover ID</param>
        public ActionResult UsageDropdownOptions(int coverID = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Create")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Create")))
            {
                string [] UsageArray = {};
                JavaScriptSerializer jsonList = new JavaScriptSerializer();
                List<Usage> u = new List<Usage>();
                if (coverID != 0)
                {
                    u = UsageModel.GetPolicyCoverUsages(new PolicyCover(coverID));
                    string jsonFile = jsonList.Serialize(u);
                    return Json(new { result = jsonFile }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string jsonFile = jsonList.Serialize(u);
                    return Json(new { result = jsonFile }, JsonRequestBehavior.AllowGet);
                }
            }
            else return new EmptyResult();

        }

        public ActionResult CoverUsagesJson(int id = 0)
        {
            List<UserPermission> permissions = new List<UserPermission>();
            permissions.Add(new UserPermission("Wording","Create"));
            permissions.Add(new UserPermission("Policies", "Create"));
            
            if (UserModel.TestUserPermissions(Constants.user, permissions)) return Json(new { success = true, data = UsageModel.GetPolicyCoverUsages(new PolicyCover(id)) }, JsonRequestBehavior.AllowGet);
            else return Json(new { success = false }, JsonRequestBehavior.AllowGet);

        }


    }
}
