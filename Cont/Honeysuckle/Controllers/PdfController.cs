﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using PdfSharp.Pdf;

namespace Honeysuckle.Controllers
{
    public class PdfController : Controller
    {
        //
        // GET: /PDf/
        public ActionResult ViewPdf()
        {
            if (Constants.document != null) return File(PDF.ConvertToDataBytes(Constants.document), "application/pdf");
            else return new EmptyResult();
        }

        ////THIS TO BE REMOVED BEFORE RELEASE
        //public ActionResult TestPdf()
        //{
        //    #if DEBUG
        //        List<VehicleCoverNoteGenerated> g = new List<VehicleCoverNoteGenerated>();
        //        g.Add(VehicleCoverNoteGeneratedModel.GetGenCoverNote(9020));
        //        Constants.user = UserModel.GetUser(1);
        //        return File(PDF.ConvertToDataBytes(PDF.ICWIcovernote(g)), "application/pdf");
        //    #else
        //        return new EmptyResult();
        //    #endif
        //}
    }
}
