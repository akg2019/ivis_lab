﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class CookieController : Controller
    {
        //
        // GET: /Cookie/

        /// <summary>
        /// this creates the cookie for the security control of the user
        /// </summary>
        public ActionResult Create(string GUID = null)
        {
            if (Constants.user.ID != 0)
            {
                HttpCookie cookie = new HttpCookie("Cookie");
                if (GUID != null)
                {
                    cookie.Values.Add("sessions", RSA.Encrypt(GUID, "session"));
                    cookie.Values.Add("kid", Constants.rsa.id.ToString());

                    //cookie.Values.Add("gusession", GUID);
                    cookie.Expires = DateTime.Now.AddSeconds(315360000); //ten years in seconds
                    Response.Cookies.Add(cookie);
                }
            }
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// this destroys the cookie on the system as the user logs out
        /// </summary>
        public ActionResult Remove()
        {
            if (Request.Cookies.AllKeys.Contains("Cookie"))
            {
                HttpCookie cookie = Request.Cookies["Cookie"];
                cookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(cookie);
            }
            return RedirectToAction("Index", "Home");
        }

    }
}
