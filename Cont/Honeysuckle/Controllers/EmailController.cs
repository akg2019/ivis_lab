﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using System.Web.Script.Serialization;

namespace Honeysuckle.Controllers
{
    public class EmailController : Controller
    {

        public ActionResult AuditEmail()
        {
            return View();
        }

        /// <summary>
        /// This function is used to retrieve an existing email from the system, given email id (if email id is zero, empty email form is shown)
        /// </summary>
        /// <param name="id">Email id</param>
       public ActionResult NewEmail(int id = 0)
       {
           Email email = new Email();

           if (id != 0) 
           {
               email.emailID = id;
               email = EmailModel.GetEmail(email);
           }
           return View(email);
       }


       /// <summary>
       /// This function is used to retrieve and return a person's emails
       /// </summary>
       /// <param name="id">Person id</param>
       public ActionResult GetPersonEmails(int id = 0)
       {
           if (id != 0)
           {
               List<Email> emails = EmailModel.GetMyEmails(new Person(id));
               var jsonSerialiser = new JavaScriptSerializer();
               var json = jsonSerialiser.Serialize(EmailModel.GetMyEmails(new Person(id)));
               return Json(json, JsonRequestBehavior.AllowGet);
           }
           else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
       }
    }
}
