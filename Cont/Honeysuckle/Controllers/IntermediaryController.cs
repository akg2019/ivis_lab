﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class IntermediaryController : Controller
    {

        public ActionResult AuditIntermediary()
        {
            return View();
        }

        //
        // GET: /Intermediary/
        public ActionResult Intermediary(int id = 0)
        {
            //Intermediary intermediary = new Intermediary(CompanyModel.GetCompany(id));
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Intermediaries", "Read")) && id != 0) return View(IntermediaryModel.GetIntermediary(id));
            else return new EmptyResult();
        }


        /// <summary>
        /// This function is used to display the intermediary companies asociated with an insurance companies
        /// </summary>
        /// <param name="compId">Company Id</param>
        public ActionResult Index(int id = 0)
        {
            bool allow = false;

            if (Constants.user.newRoleMode) //Testing if the current user currently has a temp role
            {
                if (Constants.user.tempUserGroup.company.CompanyID == id && UserModel.TestUserPermissions(Constants.user, new UserPermission("Intermediaries", "Read")) && id != 0)
                    allow = true;
            }

            else if (UserModel.GetUser(Constants.user.ID).employee.company.CompanyID == id && UserModel.TestUserPermissions(Constants.user, new UserPermission("Intermediaries", "Read")) && id != 0)
                allow = true;

            if (allow)
            {
                ViewData["id"] = id;
                return View(IntermediaryModel.GetInsurerIntermediaries(new Company(id)));
            }
            else return RedirectToAction("AccessDenied", "Error");
        }


        public ActionResult Create()
        {
            ViewData["postBack"] = false;
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Intermediaries", "Create")))
            {
                Intermediary inter = new Intermediary();
                inter.enabled = true;
                return View(inter);
            }
            else return RedirectToAction("AccessDenied", "Error");
        }

        [HttpPost]
        public ActionResult Create(Intermediary intermediary,int ins = 0,int interCompanies = 0)
        {
            ViewData["postBack"] = true;
            if (interCompanies == 0)
            {
               
                ViewData["selectComp"] = "You need to select a Company";
                return View(intermediary);
            }

            if(!(CompanyModel.IsCompanyBroker(interCompanies) && CompanyModel.IsCompanyEnabled(new Company(interCompanies)))){

               
                ViewData["NotBroker"] = "The selected Company is not a broker nor is it enabled";
                return View(intermediary);

            }

            if (interCompanies != 0 && ins != 0 && ModelState.IsValid)
            {
                intermediary.company = new Company(interCompanies);
                IntermediaryModel.AddIntermediary(intermediary, new Company(ins));
                return RedirectToAction("Index", "Intermediary", new { id = Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID });
            }
            else
                return View(intermediary);
        }


        public ActionResult Edit(int id = 0)
        {
            ViewData["postBack"] = false;
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Intermediaries", "Update")) && id != 0) return View(IntermediaryModel.GetIntermediary(id));
            else return RedirectToAction("AccessDenied", "Error");
        }

        [HttpPost]
        public ActionResult Edit(Intermediary intermediary,int ins = 0)
        {
            ViewData["postBack"] = true;
            if (ins != 0 && ModelState.IsValid)
            {
                IntermediaryModel.UpdateIntermediary(intermediary, new Company(ins));
                return RedirectToAction("Index", "Intermediary", new { id = Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID });

            }
            else return View(intermediary);
        }


        public ActionResult InterDropDown(int compid = 0)
        {

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Intermediaries", "Create")) && compid != 0)
            {
                List<Company> CompList = new List<Company>();
                List<Company> TempList1 = new List<Company>();
                List<Intermediary> TempList2 = new List<Intermediary>();

                TempList1.AddRange(CompanyModel.IntermediaryList());
                TempList2.AddRange(IntermediaryModel.GetInsurerIntermediaries(new Company(compid)));

                for (int i = 0; i < TempList2.Count(); i++)
                {
                    for (int y = 0; y < TempList1.Count(); y++)
                    {   
                        if (TempList2.ElementAt(i).company.CompanyID == TempList1.ElementAt(y).CompanyID) TempList1.RemoveAt(y);
                    }
                }
               
                CompList.Add(new Company());
                CompList.AddRange(TempList1);
                return View(CompList);
            }
            else
                return new EmptyResult();
        }


        public ActionResult DisableIntermediary(int id = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("Intermediaries","Enable"))) return Json(new { result = IntermediaryModel.DisableIntermediary(new Intermediary(id)), fail = 0 }, JsonRequestBehavior.AllowGet);
            else return Json(new { fail = 1 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EnableIntermediary(int id = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("Intermediaries", "Enable"))) return Json(new { result = IntermediaryModel.EnableIntermediary(new Intermediary(id)), fail = 0 }, JsonRequestBehavior.AllowGet);
            else return Json(new { fail = 1 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveIntermediary(int id = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("Intermediaries", "Delete")))
            {

                Intermediary intermediary = new Intermediary(id);
                IntermediaryModel.RemoveIntermediary(intermediary);

                return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateNewIntermediary(int id = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("Intermediaries", "Create")))
            {
                Intermediary intermediary = new Intermediary(CompanyModel.GetCompany(id));
                intermediary.enabled = true;
                return View(intermediary);
            }
            else return new EmptyResult();
        }

        public ActionResult Update(int id = 0, int cid = 0, int ins = 0, bool cert = false, bool cnote = false, int pcount = 0)
        {
            if (ins != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("Intermediaries", "Update")))
            {
                Intermediary intermediary = new Intermediary();
                if (id != 0)
                {
                    intermediary = IntermediaryModel.GetIntermediary(id);
                    intermediary.PrintCertificate = cert;
                    intermediary.PrintCoverNote = cnote;
                    intermediary.PrintLimit = pcount;
                    IntermediaryModel.UpdateIntermediary(intermediary, new Company(ins));
                    return Json(new { result = 1, intid = 0 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    intermediary.company = new Company(cid);
                    intermediary.PrintCertificate = cert;
                    intermediary.PrintCoverNote = cnote;
                    intermediary.PrintLimit = pcount;
                    intermediary = IntermediaryModel.AddIntermediary(intermediary, new Company(ins));
                    return Json(new { result = 1, intid = intermediary.ID }, JsonRequestBehavior.AllowGet);
                }
                
            }
            return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

    }
}
