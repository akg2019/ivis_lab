﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class DriverController : Controller
    {

        public ActionResult AuditDriver()
        {
            return View();
        }

        public ActionResult SendDetails(int id = 0, string license = null, string dclass = null, string issued = null, string expiry = null)
        {
            return new EmptyResult();
        }

        public ActionResult GetDetails(int id = 0)
        {
            if (id != 0) return View(DriverModel.GetDriverDetails(new Driver(id)));
            else return new EmptyResult();
        }
        
        
        /// <summary>
        /// This function renders the email partial- to add a new email address to a person
        /// </summary>
        /// <param name="email">The email that is added</param>
        public ActionResult Email(Email email = null)
        {
            if (email != null) return RedirectToAction("NewEmail", "Email", new { id = email.emailID });
            else return RedirectToAction("NewEmail", "Email", new { id = 0 });
        }

        /// <summary>
        /// This function retrieves the index page for drivers
        /// </summary>
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// This function retrieves an authorized driver's information, given the person Id
        /// </summary>
        /// <param name="id">Person Id</param>
        /// <returns>Person object with person's information</returns>
        public ActionResult DriverView (int id=0)
        {
            Person person = new Person();
            if (id != 0) person = PersonModel.GetPerson(id);

            Driver driver = new Driver();
            //driver.DriverId = id;
            driver.person = person;
            return View(driver);
        }

        /// <summary>
        /// This function retrieves an authorized driver's information, given the person Id
        /// </summary>
        /// <param name="id">Person Id</param>
        /// <param name="vehId">vehicle Id</param>
        /// <param name="type">Type of Driver (authorized, excluded, excepted)</param>
        /// <param name="isMainDriver">Whether or not the person is the main driver of the vehicle</param>
        /// <returns>Driver object with driver's information</returns>
        public ActionResult Drivers(int id = 0, int vehId = 0, int polId = 0, int type = 0, bool isMainDriver = false) 
        {
            if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("People", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read"))) && id != 0)
            {
                Driver driver = new Driver();
                Person person = new Person();

                //Testing if the driver is being called for edit, or just be added
                if (vehId != 0)
                {
                    if (id != 0) driver = DriverModel.getDriverFromPersonId(id, type, vehId, polId); 
                    ViewData["relation"] = driver.relation;
                    if (driver.person.lname == "" && driver.person.fname == "")
                        ViewData["personName"] = "Name Not Available";
                    else
                        ViewData["personName"] = driver.person.lname + ", " + driver.person.fname;
                    ViewData["vehicleID"] = vehId;
                    ViewData["MainDriver"] = isMainDriver;

                    return View(driver);
                }

                else
                {
                    if (id != 0) driver.person = PersonModel.GetPerson(id);
                    ViewData["relation"] = "";
                    if (driver.person.lname == "" && driver.person.fname == "")
                        ViewData["personName"] = "Name Not Available";
                    else
                        ViewData["personName"] = driver.person.lname + ", " + driver.person.fname;

                    ViewData["MainDriver"] = isMainDriver;

                    return View(driver);
                }
             }

            else return new EmptyResult();
        }


        /// <summary>
        ///This function retrieves an excepted driver's information, given the person Id
        /// </summary>
        /// <param name="id">Person Id</param>
        /// <param name="vehId">vehicle Id</param>
        /// <param name="type">Type of Driver (authorized, excluded, excepted)</param>
        /// <param name="isMainDriver">Whether or not the person is the main driver of the vehicle</param>
        /// <returns>Driver object with driver's information</returns>
        public ActionResult Excepted(int id = 0, int vehId = 0, int polId = 0, int type = 0, bool isMainDriver = false)
        {
            if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("People", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read"))) && id != 0)
            {
                Driver driver = new Driver();
                Person person = new Person();

                if (vehId != 0)
                {
                    if (id != 0) driver = DriverModel.getDriverFromPersonId(id, type, vehId, polId);

                    ViewData["relation"] = driver.relation;
                    if (driver.person.lname == "" && driver.person.fname == "")
                        ViewData["personName"] = "Name Not Available";
                    else
                        ViewData["personName"] = driver.person.lname + ", " + driver.person.fname;

                    ViewData["MainDriver"] = isMainDriver;

                    return View(driver);
                }

                else
                {
                    if (id != 0) person = PersonModel.GetPerson(id);
                    driver.person = person;
                    ViewData["relation"] = "";
                    if (driver.person.lname == "" && driver.person.fname == "")
                        ViewData["personName"] = "Name Not Available";
                    else
                        ViewData["personName"] = driver.person.lname + ", " + driver.person.fname;

                    ViewData["MainDriver"] = isMainDriver;

                    return View(driver);
                }
            }

            else return new EmptyResult();
        }


        /// <summary>
        ///This function retrieves an excluded driver's information, given the person Id
        /// </summary>
        /// <param name="id">Person Id</param>
        /// <param name="vehId">vehicle Id</param>
        /// <param name="type">Type of Driver (authorized, excluded, excepted)</param>
        /// <param name="isMainDriver">Whether or not the person is the main driver of the vehicle</param>
        /// <returns>Driver object with driver's information</returns>
        public ActionResult Excluded(int id = 0, int vehId = 0, int polId = 0, int type = 0, bool isMainDriver = false)
        {
            if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("People", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read"))) && id != 0)
            {
                Driver driver = new Driver();
                Person person = new Person();

                if (vehId != 0)
                {
                    if (id != 0) driver = DriverModel.getDriverFromPersonId(id, type, vehId, polId); 
                    ViewData["relation"] = driver.relation;
                    if (driver.person.lname == "" && driver.person.fname == "")
                        ViewData["personName"] = "Name Not Available";
                    else
                        ViewData["personName"] = driver.person.lname + ", " + driver.person.fname;

                    ViewData["MainDriver"] = isMainDriver;

                    return View(driver);
                }

                else
                {
                    if (id != 0) person = PersonModel.GetPerson(id);
                    driver.person = person;

                    ViewData["relation"] = "";
                    if (driver.person.lname == "" && driver.person.fname == "")
                        ViewData["personName"] = "Name Not Available";
                    else
                        ViewData["personName"] = driver.person.lname + ", " + driver.person.fname;

                    ViewData["MainDriver"] = isMainDriver;

                    return View(driver);
                }
              }

            else return new EmptyResult();
         }

        /// <summary>
        /// This function retrieves information about a particular person, given the person ID
        /// </summary>
        /// <param name="id">Person Id</param>
        public ActionResult Driver(int id = 0)
        {
            if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("People", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read"))))
            {
                Driver person = new Driver();
                if (id != 0)
                {
                    person.person = PersonModel.GetPerson(id);
                    person.person.emails = EmailModel.GetMyEmails(person.person);
                }
                return View(person);
            }
            else
                return RedirectToAction("Index");
        }


    }
}
