﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class PhoneController : Controller
    {
        /// <summary>
        /// This function is used to retrieve an existing phoneNumber from the system, given phoneNum id 
        /// </summary>
        /// <param name="id">Email id</param>
        //public ActionResult NewPhone(int id = 0, string type = null, string edit= null, int compID = 0)
        public ActionResult NewPhone()
        {
            Phone phone = new Phone();
            
            return View(phone);
        }

        public ActionResult EditPhone(Phone phone =null, string type = null)
        {
            if(type!= null)
                ViewData["view"] = true;

            else ViewData["view"] = false;

            return View (phone);
        }

    }
}
