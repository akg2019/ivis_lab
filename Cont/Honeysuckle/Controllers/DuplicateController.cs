﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class DuplicateController : Controller
    {
        //
        // GET: /Duplicate/

        public ActionResult Index()
        {
            #if DEBUG
            Company company = new Company(1); 
            #else
            Company company = CompanyModel.GetMyCompany(Constants.user);
            #endif
            return View(DuplicateModel.get_my_duplicates(company));
        }

        public ActionResult CancelDuplicate(int id = 0, string type = null)
        {
            return new EmptyResult();
        }

        public ActionResult ShowDuplicates(string type = null, string refid = null)
        {
            Duplicate duplicate = new Duplicate();
            duplicate.reference_no = refid;
            duplicate.type = type;
            return View(DuplicateModel.get_duplicates(duplicate, new Company(1)));
        }

    }
}
