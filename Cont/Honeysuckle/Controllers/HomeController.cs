﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using System.Web.Routing;
using System.Text.RegularExpressions;
using OmaxFramework;

namespace Honeysuckle.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {

        /// <summary>
        /// This function displays the home page for the application
        /// </summary>
        public ActionResult Index(string message =null)
        {
            if (TempData["FileUpload"] != null)
            {
                ViewData["FileUpload"] = TempData["FileUpload"];
                TempData["FileUpload"] = null;
            }
            if (TempData["permissionMessage"] != null)
            {
                ViewData["permissionMessage"] = TempData["permissionMessage"];
                TempData["permissionMessage"] = null;
            }
            if (message != null)
            {
                ViewData["Message"] = "Policy Created!";
            }

            if (TempData["reset"] != null)
            {
                ViewData["reset"] = TempData["reset"];
                TempData["reset"] = null;
            }

            if (TempData["error"] != null)
            {
                ViewData["error"] = TempData["error"];
                TempData["error"] = null;
            }

            if (UserModel.TestGUID(Constants.user))
            {

                if (TempData["EmailSent"] != null)
                {
                    ViewData["Message"] = TempData["EmailSent"];
                    TempData["EmailSent"] = null;
                }
            }
            if (Honeysuckle.Models.Constants.user.username == "not initialized")
            {
                if (Request.Cookies.AllKeys.Contains("Cookie"))
                {
                    HttpCookie cookie = Request.Cookies["Cookie"];
                    //User u = UserModel.getUserFromGUID(cookie.Values.Get("gusession"));
                    try
                    {
                        Constants.user = UserModel.getUserFromGUID(Honeysuckle.Models.RSA.Decrypt(Request.Cookies["Cookie"]["sessions"], "session", int.Parse(Request.Cookies["Cookie"]["kid"])));
                    }
                    catch(Exception e){}
                    UserModel.Logout();
                    Constants.user = null;

               }
            }
            return View();
        }


        [HttpPost]
        public ActionResult Index(User user, string submit)
        {
            if (Constants.user.username == "not initialized")
            {
                switch (submit)
                {
                    case "Sign In":
                        if (user.username != null)
                        {
                            if (user.password != null)
                            {
                                user_login_object result = UserModel.LoginUser(user);

                                if (result.valid_user && !result.session_free && result.company_active)
                                {

                                    if (!result.active)
                                    {
                                        ViewData["error"] = "The user account you are trying to access is inactive, please contact your system administrator for assistance. ";
                                        return View(user);
                                    }

                                    //IF USER IS LOGGING IN FROM SECOND LOCATION
                                    ViewData["error"] = "This account is already authenticated. Please contact your administrator.";
                                    if (!result.session_free)
                                    {
                                        User tempuser = UserModel.GetUser(user.username);
                                        List<User> to = CompanyModel.GetMyCompanyAdmins(tempuser);
                                        to.Add(tempuser);
                                        List<string> toList = new List<string>();
                                        foreach (User u in to)
                                        {
                                            toList.Add(EmailModel.GetPrimaryEmail(u).email);
                                        }

                                        MailModel.SendMailWithThisFunction(to, new Mail(toList, "Double Login", "We detected a double login attempt into your account: " + user.username + " on " + System.DateTime.Now.ToString("dddd, MMMM dd, yyyy") + " at " + System.DateTime.Now.ToString("h:mm tt.") + " If this was you, please disregard this email. If this was not you, please secure your account here: " + System.Web.HttpContext.Current.Request.Url.AbsoluteUri + "  or contact your administrator, as someone else may be attempting to access it. \r\n \r\n  Thanks, IVIS Team."));
                                    }   
                                    return View(user);
                                }
                                else
                                {
                                    if (result.valid_user)
                                    {
                                        if (!result.company_active)
                                        {
                                            ViewData["error"] = "The company that this user is a part of has been disabled. Please contact the an administrator.";
                                            return View(user);
                                        }
                                        else if (result.first_login)
                                        {
                                            if (result.first_login_timer_lapsed)
                                            {
                                                //IF THIS IS THE FIRST TIME THIS USER IS LOGGING INTO THE SYSTEM

                                                RouteValueDictionary dict = new RouteValueDictionary();
                                                dict.Add("usernam", OmaxFramework.Utilities.InformationHiding.MakeIllusion(user.username));
                                                Models.UserValidator.CurrentUser = OmaxFramework.Utilities.InformationHiding.MakeIllusion(user.username).ToString();
                                                
                                                return RedirectToAction("FirstLogin", "Login", dict);
                                            }
                                            else
                                            {
                                                //timer times out show error message
                                                ViewData["error"] = "You have taken more than 24 hours to login in to the system. Please contact your administrator and have them renew your access.";
                                                return View(user);
                                            }
                                        }
                                        else
                                        {
                                            if (result.active)
                                            {
                                                if (!result.faulty_login_limit_passed)
                                                {
                                                    if (result.force_reset)
                                                    {
                                                        RouteValueDictionary dict = new RouteValueDictionary();
                                                        dict.Add("username", user.username);
                                                        dict.Add("reset", true);
                                                        //Constants.resetflag = true;
                                                        //Constants.user = user;
                                                        return RedirectToAction("ResetPassword", "Login", dict);
                                                    }
                                                    else
                                                    {
                                                        //SUCCESSFUL LOGIN
                                                        UserModel.ResetFaultyLogins(user.username); //reset fault login counter
                                                        User auth = UserModel.GetUser(user.username);
                                                        auth.sessionguid = UserModel.GetSessionGUID(auth);
                                                        Constants.user = auth;
                                                        Constants.user.UserGroups = UserModel.GetGroupsAndPermissions(Constants.user.ID);
                                                        
                                                        return RedirectToAction("Create", "Cookie", new { GUID = auth.sessionguid });
                                                    }
                                                }
                                                else
                                                {
                                                    //IF USER HAD PREVIOUSLY LOGGED IN ERRONEOUSLY MORE TIMES THAN ARE ALLOWED
                                                    ViewData["error"] = "You have made erroneous logins more than the system allows. Please contact the administrator to reactivate your account.";
                                                    return View(user);
                                                }
                                            }
                                            else
                                            {
                                                //ACCOUNT IS INACTIVE
                                                ViewData["error"] = "The user account you are trying to access is inactive, please contact your stystem administrator for assistance. ";
                                                UserModel.Logout(user.username);
                                                return View(user);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //BAD LOGIN INFORMATION
                                        if (result.user_exist)
                                        {
                                            if (CompanyModel.IsCompanyEnabled(CompanyModel.GetMyCompany(UserModel.GetUser(user.username))))
                                            {
                                                if (result.faulty_login_limit_passed)
                                                {
                                                    if (!result.active)
                                                    {
                                                        ViewData["error"] = "The user account you are trying to access is inactive, please contact your stystem administrator for assistance. ";
                                                        return View(user);
                                                    }

                                                    else
                                                    {
                                                        ViewData["error"] = "You have made erroneous logins more than the system allows. Please contact the administrator to reactivate your account.";
                                                        return View(user);
                                                    }
                                                }
                                                else
                                                {
                                                    UserModel.FaultyLogin(user.username); // increment fault login
                                                    ViewData["error"] = "The username or password you submitted is incorrect."; //bad password?
                                                }
                                            }
                                            else ViewData["error"] = "The company for your user is yet to be enabled. Please contact the Administrator to enable your company";
                                        }
                                        else ViewData["error"] = "The username or password you submitted is incorrect."; //user does not exist
                                        return View(user);
                                    }
                                }
                            }
                            else
                            {
                                //NO PASSWORD
                                ViewData["error"] = "Please enter a password when logging in";
                                return View(user);
                            }
                        }
                        else
                        {
                            //NO USERNAME
                            ViewData["error"] = "Please enter a username when logging in";
                            return View(user);
                        }
                    case "Forgot Password":
                        if (UserModel.IsActive(user.username))
                        {
                            if (!UserModel.FirstLogin(user.username))
                            {
                                if (UserModel.IsValidUser(user.username)) return RedirectToAction("AnswerQuestion", "Login", new { id = user.username });
                                else
                                {
                                    ViewData["error"] = "The username you have provided does not exist in the system.";
                                    return View(user);
                                }
                            }
                            else
                            {
                                ViewData["error"] = "This user is a new account. Please authenticate with the credentials that was sent to your email. If you did not receive any credentials please contact your administrator.";
                                return View(user);
                            }
                        }
                        else
                        {
                            ViewData["error"] = "This user has been deactivated from the system. Please contact your administrator.";
                            return View(user);
                        }

                    case "Clear":
                        return View();
                    default:
                        return View(user);

                }
            }
            else return new EmptyResult();
        }

        /// <summary>
        /// This function returns the ABOUT page of the application
        /// </summary>
        public ActionResult About()
        {
            return View();
        }

    }
}
