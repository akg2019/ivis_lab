﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class ParishController : Controller
    {
        //
        // GET: /Parish/

        public ActionResult ParishList(bool mapping = false, bool maplock = false)
        {
            if (Constants.user.username != "not initialized") 
            {
                ViewData["maplock"] = maplock;
                ViewData["mapping"] = mapping;
                List<Parish> parishes = new List<Parish>();
                if (mapping) parishes.Add(new Parish("None Selected"));
                parishes.AddRange(ParishModel.ParishList(!mapping));
                return View(parishes);
            }
            else return new EmptyResult();
        }
    }
}
