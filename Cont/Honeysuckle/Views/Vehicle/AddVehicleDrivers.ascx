﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Vehicle>" %>

<div class ="PolVehicle">
    <% using (Html.BeginCollectionItem("vehicle"))  { %>
        <%: Html.ValidationSummary(true) %>
        <div class="vehicleChassisHeader subheader arrow-click"  onclick="clickme(this)">
            <div class="arrow right veh"></div>
            <span class="description"><%: Model.chassisno %>, <%: Model.VehicleYear %> <%: Model.make %> <%: Model.VehicleModel %> </span>
            <%: Html.HiddenFor(model => model.chassisno, new { @Class = "chassis dont-process" })%>
            <%: Html.HiddenFor(model => model.ID, new { @class = "vehID dont-process" })%>
            <span class="update-count">Drivers: Auth: 0 / Except: 0 / Exclude: 0</span>
        </div>
        <div class="mainDriverID" style="visibility:hidden;"><%: Model.mainDriver.person.PersonID %></div>
         <div class = "adw-div">
            <span>Authorized Driver Wording:</span>
            <%: Html.Hidden("hiddenADW", int.Parse(ViewData["adw"].ToString()), new { @class = "adwID dont-process" })%>
            <% Html.RenderAction("AuthorizedDriverWordingDropDown", "AuthorizedDriverWording", new { id = int.Parse(ViewData["insurer"].ToString()) });  %>
        </div>
        <div class="vehicle-row">

            <div class="drivers">
                <div id="auth" class="authorizedDriv"></div>
                
                <div id="authorizedDriver">
                    <% if (Model.AuthorizedDrivers != null) { %>
                        <% foreach (Honeysuckle.Models.Driver AuthorizedDriver in Model.AuthorizedDrivers) { %>
                            <% Html.RenderAction("Drivers", "Driver", new { id = AuthorizedDriver.person.PersonID, vehId = Model.ID, polId = ViewData["policy"], type = 1, isMainDriver = (AuthorizedDriver.person.PersonID == Model.mainDriver.person.base_id) }); %>
                        <% } %>
                    <% }  %>
                </div>
                <div class="add-dynamic big authorized" title="Add a person authorised to drive this vehicle" ><img src="../../Content/more_images/ivis_add_icon.png" /><span>Add Authorized Driver(s)</span></div>
            </div>
            <div class="drivers">
                
                <div id="except" class="exceptedDriv"></div>
                
                <div id="exceptedDriver">
                    <% if (Model.ExceptedDrivers != null) { %>
                        <% foreach (Honeysuckle.Models.Driver ExceptedDriver in Model.ExceptedDrivers) { %>
                            <% Html.RenderAction("Excepted", "Driver", new { id = ExceptedDriver.person.PersonID, vehId = Model.ID, polId = ViewData["policy"], type = 2, isMainDriver = (ExceptedDriver.person.PersonID == Model.mainDriver.person.base_id) }); %>
                        <% } %>
                    <% } %>
                </div>
                <div class="add-dynamic big excepted" title="Add a person that is allowed to drive that is not within the limits of the authorized driver wording stated" ><img src="../../Content/more_images/ivis_add_icon.png" /><span>Add Excepted Driver(s)</span></div>
            </div>
            <div class="drivers">
                <div id="excl" class="excludedDriv"></div>
                
                <div id="excludedDriver">
                    <% if (Model.ExcludedDrivers != null) { %>
                        <% foreach (Honeysuckle.Models.Driver ExcludedDriver in Model.ExcludedDrivers) { %>
                            <% Html.RenderAction("Excluded", "Driver", new { id = ExcludedDriver.person.PersonID, vehId = Model.ID, polId = ViewData["policy"], type = 3, isMainDriver = (ExcludedDriver.person.PersonID == Model.mainDriver.person.base_id) }); %>
                        <% } %>
                    <% } %>
                </div>
                <div class="add-dynamic big excluded" title="Add a person that has been authorised to drive but should not drive at this time" ><img src="../../Content/more_images/ivis_add_icon.png" /><span>Add Excluded Driver(s)</span></div>
            </div>

            <div id="createDriv"></div>
            <div id="createDriver" class="createThisDriver"></div>
            <div id="vehicleVIN"></div>
        </div>

    <% } %>
</div>