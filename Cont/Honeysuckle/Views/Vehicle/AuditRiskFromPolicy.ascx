﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Vehicle>" %>

<div class="audit-risk">
    <div class="editor-label">
        Chassis Number
    </div>
    <div class="editor-field">
        <%: Html.TextBoxFor(model => model.chassisno, new { @class = "read-only", @readonly = "readonly" }) %>
    </div>

    <div class="editor-label">
        VIN 
    </div>
    <div class="editor-field">
        <%: Html.TextBoxFor(model => model.VIN, new { @class = "read-only", @readonly = "readonly" })%>
    </div>

    <div class="editor-label">
        Make
    </div>
    <div class="editor-field">
        <%: Html.TextBoxFor(model => model.make, new { @class = "read-only", @readonly = "readonly" })%>
    </div>
            
    <div class="editor-label">
        Model
    </div>
    <div class="editor-field">
        <%: Html.TextBoxFor(model => model.VehicleModel, new { @class = "read-only", @readonly = "readonly" })%>
    </div>
            
    <div class="editor-label">
        Vehicle Year
    </div>
    <div class="editor-field">
        <%: Html.TextBoxFor(model => model.VehicleYear, new { @class = "read-only", @readonly = "readonly" })%>
    </div>

    <div class="editor-label">
        Vehicle Registration Number
    </div>
    <div class="editor-field">
        <%: Html.TextBoxFor(model => model.VehicleRegNumber, new { @class = "read-only", @readonly = "readonly" })%>
    </div>
</div>