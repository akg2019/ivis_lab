﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Intermediary>" %>

<div class="editor-label">
    Printer Certificate?:
</div>
<div class="editor-field">
    <%: Html.CheckBoxFor(model => model.PrintCertificate) %>
</div>
            
<div class="editor-label">
    Print Cover Note?:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.PrintCoverNote) %>
</div>
            
<div class="editor-label">
    enabled?:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.enabled) %>
</div>
            
<div class="editor-label">
    Print Limit:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.PrintLimit) %>
</div>

<div class="subheader">Broker/Agent</div>
<% Html.RenderPartial("~/Company/AuditCompany.ascx", Model.company); %>