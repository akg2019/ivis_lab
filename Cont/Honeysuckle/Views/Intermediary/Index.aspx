﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.Intermediary>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Intermediaries
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="intermediary-page">
        <div id="form-header">
            <img src="../../Content/more_images/Company.png"/>
            <div class="form-text-links">
                <span class="form-header-text">
                    Intermediaries
                </span>
            
            <div  class="search-box inter-search">
                <%= Html.TextBox("search-inter", null, new { @PlaceHolder = "Search [Intermediaries]", @autocomplete = "off", @class = "quick-search intermediary-index" }) %>
                <button type="button" class="quick-search-btn user-search-btn" onclick="clickSearchBtn()" ><img src="../../Content/more_images/Search_Icon.png" /></button>
            </div>
            </div>
        </div>
        <!-- div id="below-header">-->
        <div class="field-data">
            <% 
            bool canCreate = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Intermediaries", "Create"));
            bool canEdit = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Intermediaries", "Update"));
            bool canDelete = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Intermediaries", "Delete"));
            bool canEnable = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Intermediaries", "Enable")); 
            %>
            <div id="company-modal"></div>
            <!--<div id="form">-->
                <%// using (Html.BeginForm()){ %>
                <%: Html.Hidden("compid", int.Parse(ViewData["ID"].ToString()), new { @class = "dont-process" })%>
                
                <div class="ivis-table">
                
            
                    <div class="menu">
                        <div class="inter-company item">
                             Company
                        </div>
                        <div class="print-cert item">
                             Print Certificates
                        </div>
                        <div class="print-note item">
                            Print CoverNotes
                        </div>
                        <div class="print-limit item">
                            Print Limit
                        </div>
                    </div>

                    <div id="inter-data">
                        <% int i = 0; %>
                        <% foreach (Honeysuckle.Models.Intermediary inter in Model)
                           {%>
                                <% if (i%2 == 1) { %>
                                    <% if (inter.enabled) { %>
                                        <div class="inters data odd row"> 
                                    <% } %>
                                    <% else { %>
                                        <div class="inters data odd row disabled"> 
                                    <% } %>
                                <% } %>
                                <% else { %> 
                                    <% if (inter.enabled) { %>
                                        <div class="inters data even row"> 
                                    <% } %>
                                    <% else { %>
                                        <div class="inters data even row disabled"> 
                                    <% } %>
                                <% } %>

                                <%: Html.Hidden("inter-company-id", inter.company.CompanyID, new { @class = "hidcid dont-process" })%>
                                <%: Html.HiddenFor(model => inter.ID, new { @class = "id dont-process" })%>
                                <%: Html.Hidden("ins", Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID, new { @id = "mycompid", @class = "dont-process" })%>
                                 
                                <div class="inter-company item">
                                    <%: inter.company.CompanyName%>
                                </div>
                                <div class="inter-print-certs check item">
                                    <%: Html.CheckBoxFor(model => inter.PrintCertificate, new { @disabled = "disabled" })%>
                                </div>
                                <div class="inter-print-cover check item">
                                    <%: Html.CheckBoxFor(model => inter.PrintCoverNote, new { @disabled = "disabled" })%>
                                </div>
                                <div class="inter-print-limit item">
                                    <%: inter.PrintLimit%>
                                </div>

                                <div class="item functions">
                
                                    <div class = "inter-row-edit inter-row-link">
                                         <% if (canEdit)
                                            { %>
                                                <div class="icon edit" title="Edit Intermediary"> 
                                                    <img src="../../Content/more_images/edit_icon.png" class="sprite" />
                                                </div>
                                            <% } %>
                                     </div>
                                     <div class = "inter-row-delete inter-row-link">
                                         <% if (canDelete)
                                            { %>
                                                <div class="icon delete" title="Delete Intermediary"> 
                                                    <img src="../../Content/more_images/delete_icon.png" class="sprite" />
                                                </div>
                                         <% } %>
                                    </div>
                                    <div class = "inter-row-enable inter-row-link">
                                     <% if (canEnable)
                                        { %>
                                             <% if (inter.enabled)
                                                { %>
                                                    <div class="icon disable" title="Disable Intermediary">
                                                         <img src="../../Content/more_images/disable_icon.png" class="sprite" />
                                                    </div>
                                            <% } %>
                                              <% else
                                                {%> 
                                                    <div class="icon enable" title="Enable Intermediary">
                                                        <img src="../../Content/more_images/enable_icon.png" class="sprite" />
                                                    </div>
                                               <% } %>
                                    <% } %>
                                     </div>
                                  </div>
                                </div>
                            <% i++; %> 
                        <%} %>

                <%// } %>
            <!--</div>-->
        </div>
    </div>
   </div>
    <div class="btnlinks">
        <% if (canCreate) { %>
            <button type="button" id="addIntermediary">Create Intermediary</button>
        <% } %>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/intermediaries.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/Intermediaries.js" type="text/javascript"></script>
    <link href="../../Content/Pages.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

