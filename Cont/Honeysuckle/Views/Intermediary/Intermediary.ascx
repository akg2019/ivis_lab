﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Intermediary>" %>

    <% using (Html.BeginCollectionItem("intermediaries")){%>
        <%: Html.ValidationSummary(true) %>
        <% 
            bool canEdit = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Intermediaries", "Update"));
            bool canDelete = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Intermediaries", "Delete"));
            bool canEnable = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Intermediaries", "Enable")); 
         %>
         <%: Html.Hidden("inter-company-id", Model.company.CompanyID, new { @class = "hidcid dont-process" })%>
         <%: Html.HiddenFor(model => model.ID, new { @class = "id dont-process" })%>
         <%: Html.Hidden("ins", Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID, new { @id = "mycompid", @class = "dont-process" })%>
         <div class="inter-company item">
              <%: Model.company.CompanyName %>
          </div>
          <div class="inter-print-certs check item">
              <%: Html.CheckBoxFor(model => model.PrintCertificate, new { @disabled = "disabled" })%>
          </div>
          <div class="inter-print-cover check item">
              <%: Html.CheckBoxFor(model => model.PrintCoverNote, new { @disabled = "disabled" })%>
          </div>
          <div class="inter-print-limit item">
              <%: Model.PrintLimit %>
          </div>
          <div class="item functions">
                
                 <div class = "inter-row-edit inter-row-link">
                        <% if (canEdit)
                           { %>
                           <div class="icon edit" title="Edit Intermediary"> 
                               <img src="../../Content/more_images/edit_icon.png" class="sprite" />
                           </div>
                        <% } %>
                </div>
                <div class = "inter-row-delete inter-row-link">
                        <% if (canDelete)
                           { %>
                           <div class="icon delete" title="Delete Intermediary"> 
                               <img src="../../Content/more_images/delete_icon.png" class="sprite" />
                           </div>
                        <% } %>
                </div>
                <div class = "inter-row-enable inter-row-link">
                        <% if (canEnable)
                           { %>
                                <% if (Model.enabled)
                                   { %>
                                       <div class="icon disable" title="Disable Intermediary">
                                           <img src="../../Content/more_images/disable_icon.png" class="sprite" />
                                       </div>
                                  <% } %>
                                 <% else
                                   {%> 
                                        <div class="icon enable" title="Enable Intermediary">
                                            <img src="../../Content/more_images/enable_icon.png" class="sprite" />
                                       </div>
                                    <% } %>
                        <% } %>
                </div>
          </div>

    <% } %>


