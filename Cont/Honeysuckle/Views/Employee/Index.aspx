﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.Employee>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="emperror" >
        <%: ViewData["empactive"] %>
    </div>

    <div id="deleteuser" >
        <%: ViewData["deleteuser"]%>
    </div>
    

    <h3><%: Model.ElementAt(Model.Count() - 1).company.CompanyName %></h3>
    <table>
        <tr>
            <th></th>
            <th>
                Employee Name
            </th>
            <% if(Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user))
               { %>
                <th>
                    Company Name
                </th>
            <% } %>
            
        </tr>

    <% foreach (var item in Model) { %>
    
        <tr>
            <td>
              <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Employees", "Update"))) 
                    { %>
                        <%: Html.ActionLink("Edit", "Edit", new { id = item.EmployeeID }) %>   |
                 <% } %>


                <%: Html.ActionLink("Details", "Details", new { id=item.EmployeeID })%> 


                 <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Employees", "Delete"))) 
                     { %>
                         | <%: Html.ActionLink("Delete", "Delete", new {  id=item.EmployeeID  })%> 
                  <% } %>


                   <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Employees", "Update"))) 
                      { %>
                            <% if (!item.Active)
                               { %>
                                 |  <%: Html.ActionLink("Activate", "Activate", new { id = item.EmployeeID })%> 
                            <% } %>

                            <% else
                               { %>
                                 |  <%: Html.ActionLink("Deactivate", "Deactivate", new { id = item.EmployeeID })%>
                            <% } %>
                   <% } %>
            </td>
            <td>
                <%: item.person.lname + ", " + item.person.fname + " " %>
                <% if (item.person.mname != "")
                   { %>
                     <%: item.person.mname.Substring(0,1) + "." %>
                <%  } %>
            </td>
            <% if(Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user))
               { %>
               <td>
                <%: item.company.CompanyName %>
               </td>
            <% } %>
            
        </tr>
    
    <% } %>

    </table>

    <p>
        <%: Html.ActionLink("Create New", "Create") %>
    </p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/employee.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftBar" runat="server">
</asp:Content>

