﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.VehicleCoverNote>>" %>

    <div id="covernotes-search">
        <div class="menu row">
            <div class="item"></div>
            <div class="item">
                cancelled
            </div>
            <div class="item">
                approved
            </div>
            <div class="item">
                covernoteid
            </div>
            <div class="item">
                effectivedate
            </div>
            <div class="item">
                period
            </div>
            <div class="item">
                covernoteno
            </div>
            
        </div>

    <% foreach (var item in Model) { %>
    
        <div class="row">
            <div class="item">
                <%: Html.ActionLink("Edit", "Edit", new { item.ID }) %> |
                <%: Html.ActionLink("Details", "Details", new { item.ID })%> |
                <%: Html.ActionLink("Delete", "Delete", new { item.ID })%>
            </div>
            <div class="item">
                <%: item.cancelled %>
            </div>
            <div class="item">
                <%: item.approved %>
            </div>
            <div class="item">
                <%: item.covernoteid %>
            </div>
            <div class="item">
                <%: String.Format("{0:d}", item.effectivedate) %>
            </div>
            <div class="item">
                <%: item.period %>
            </div>
            <div class="item">
                <%: item.covernoteno %>
            </div>
            
        </div>
    
    <% } %>

    </div>


