﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Address>" %>

    <% using (Html.BeginCollectionItem("address")) { %>
        <%: Html.ValidationSummary(true) %>
        <%: Html.HiddenFor(model => model.ID, new { @class = "dont-process" })%>
        
        <%if (Model.longAddressUsed)
          {  %>
                 <div class="editor-label">
                     <div>Combined Address:</div>
                </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.longAddress, new { id = "longAddress",@required="required", @autocomplete = "off", @class = "address-details" })%>
                    <%: Html.ValidationMessageFor(model => model.longAddress)%>
                    <%: Html.HiddenFor(model => model.longAddressUsed, new { @Value = true,@class="dont-process" })%>
                </div>
        <%}
          else
          {%>


                    <div class="editor-label">
                        Street
                    </div>
       
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.roadnumber, new { id = "RoadNumber", @PlaceHolder = " 12", @autocomplete = "off", @class = "address-details" })%>
                        <%: Html.TextBoxFor(model => model.road.RoadName, new { id = "roadname", @PlaceHolder = " Short Hill", @autocomplete = "off", @class = "address-details" })%>
                        <%: Html.TextBoxFor(model => model.roadtype.RoadTypeName, new { id = "roadtype", @PlaceHolder = " Avenue", @autocomplete = "off", @class = "address-details" })%>

                        <%: Html.ValidationMessageFor(model => model.roadnumber)%><br />
                        <%: Html.ValidationMessageFor(model => model.road.RoadName)%><br />
                        <%: Html.ValidationMessageFor(model => model.roadtype.RoadTypeName)%>
                    </div>

                     <div class="editor-label">
                         Building/Apartment No. 
                     </div>
                     <div class="editor-field">
                         <%: Html.TextBoxFor(model => model.ApartmentNumber, new { id = "aptNumber", @autocomplete = "off", @class = "address-details" })%>
                         <%: Html.ValidationMessageFor(model => model.ApartmentNumber)%>
                     </div>

                    <div class="editor-label">
                        City/Town
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.city.CityName, new { id = "city", @required = "required", @autocomplete = "off", @class = "address-details" })%>
                        <%: Html.ValidationMessageFor(model => model.city.CityName)%>
                    </div>
                    <div class="editor-label">
                         Parish
                    </div>
                    <div class="editor-field">
                        <%: Html.HiddenFor(model => model.parish.ID, new { id = "parish", @class = "address-details dont-process parish" })%>
                        <% Html.RenderAction("ParishList", "Parish"); %>
                    </div><br />
                    <div class="editor-label">
                        Country
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.country.CountryName, new { id = "country", @required = "required", @autocomplete = "off", @class = "address-details" })%>
                        <%: Html.ValidationMessageFor(model => model.country.CountryName)%>
                    </div>
                    <%: Html.HiddenFor(model => model.longAddressUsed, new { @Value = false,@class="dont-process" })%>
         <%} %>

    <% } %>

