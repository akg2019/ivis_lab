﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div id="dashboard">
    <% if (Honeysuckle.Models.Constants.user.username != "not initialized")
        { %>
        <div id="DashboardHeader">
            <img src="../../Content/more_images/Home_Icon.png" />
            <span>Dashboard</span>
        </div>
        <div id="DashboardBody">
            <%: Html.Hidden("pageIndex", "index", new { @id = "startpage", @class = "dont-process" })%>
            <!-- COMPANIES QUICK LIST -->

            <div class="companies-list">
                <div class="container big">
                    <div class="processing">
                        <span>Quick List Loading</span>
                        <img src="../../Content/more_images/loading.gif" />
                    </div>
                </div>
            </div>
            
            <% if (!(Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user))) { %>
                <!-- COVER LIST -->
                <div class="cover-list">
                    <div class="container big">
                        <div class="processing">
                            <span>Cover List Loading</span>
                            <img src="../../Content/more_images/loading.gif" />
                        </div>
                    </div>
                </div>
            <% } %>
            
            <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("BI", "Read"))) { %>
                <!-- BUSINESS INTELLIGENCE -->
                <div class="business-intelligence">
                    <div class="container big">
                        <div class="processing">
                            <span>Business Intelligence Loading</span>
                            <img src="../../Content/more_images/loading.gif" />
                        </div>
                    </div>
                </div>
            <% } %>
            
            
        </div>

    <% } %>
</div>
