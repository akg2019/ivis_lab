﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Audit>" %>


<%
    object data = new object();
    if ((bool)ViewData["orig_data"]) data = Model.original_data;
    else data = Model.new_data;
    if (data == null) data = new object();
    
    switch (Model.table)
    {
        case "company":
            Html.RenderPartial("~/Views/Company/AuditCompany.ascx", (Honeysuckle.Models.Company)data); 
            break;
        case "driver":
            Html.RenderPartial("~/Views/Driver/AuditDriver.ascx", (Honeysuckle.Models.Driver)data);
            break;
        case "email":
            Html.RenderPartial("~/Views/Email/AuditEmail.ascx", (Honeysuckle.Models.Email)data);
            break;
        case "employee":
            Html.RenderPartial("~/Views/Employee/AuditEmployee.ascx", (Honeysuckle.Models.Employee)data);
            break;
        case "person":
            Html.RenderPartial("~/Views/People/AuditPeople.ascx", (Honeysuckle.Models.Person)data);
            break;
        case "address":
            Html.RenderPartial("~/Views/Address/AuditAddress.ascx", (Honeysuckle.Models.Address)data);
            break;
        case "intermediary":
            Html.RenderPartial("~/Views/Intermediary/AuditIntermediary.ascx", (Honeysuckle.Models.Intermediary)data);
            break;
        case "mappingdata":
            Html.RenderPartial("~/Views/DataMapping/AuditMappingData.ascx", (Honeysuckle.Models.MappingData)data);
            break;
        case "mortgagee":
            Html.RenderPartial("~/Views/Mortgagee/AuditMortgagee.ascx", (Honeysuckle.Models.Mortgagee)data);
            break;
        case "user":
            Html.RenderPartial("~/Views/User/AuditUser.ascx", (Honeysuckle.Models.User)data);
            break;
        default:
            break;
    }
    
%>