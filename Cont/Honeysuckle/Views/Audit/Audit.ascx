﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Audit>" %>

<div class="log-partial">
    <div class="old">
        <% Html.RenderAction("AuditShowData", "Audit", new { id = Model.ID, orig = true }); %>
    </div>
    <div class="new">
        <% Html.RenderAction("AuditShowData", "Audit", new { id = Model.ID, orig = false }); %>
    </div>
</div>