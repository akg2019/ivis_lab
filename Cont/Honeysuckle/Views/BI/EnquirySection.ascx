﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div class="container big">
    <div class="stripes"></div>
    <div class="form-header">
        <img src="../../Content/more_images/Company.png" />
        <span class="form-header-text">Inquiries Data</span>
    </div>
    <div class="date changers">
        <%
            DateTime end = DateTime.Now;
            DateTime start = end.AddMonths(-1);
        %>
        <div class="dateselector">
            <span>Start Date:</span> <%: Html.TextBox("startDateEnquiry", start.ToShortDateString(), new { @class = "startdate enquiry date" }) %>
        </div>
        <div class="dateselector">
            <span>End Date:</span> <%: Html.TextBox("endDateEnquiry", end.ToShortDateString(), new { @class = "enddate enquiry date" })%>
        </div>
        <button class="datebutton" type="button" onclick="ChangeDate('company')">Update Data</button>
    </div>
    <!--<figure  id="enquiryChart"></figure>-->
    <div id="enquiryChart" style="width: 100%; height: 400px;" ></div>
</div>