﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.BusinessIntelligence>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Manage
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div id= "form-header"><img src="../../Content/more_images/BMI_SETTINGS_ICON.png" />
        <div class="form-text-links">
            <span class="form-header-text">Manage Business Intelligence KPIs</span>
        </div>
    </div>
    
    <div class="field-data">
        <% using (Html.BeginForm()) { %>
         
            <%: Html.ValidationSummary(true)%>
        
            <% Html.RenderAction("KPI", "KPI", new { kpis = Model.kpis, type = "enquiry", cid = Model.company.CompanyID }); %>
            <% Html.RenderAction("KPI", "KPI", new { kpis = Model.kpis, type = "covernote", cid = Model.company.CompanyID }); %>
            <% Html.RenderAction("KPI", "KPI", new { kpis = Model.kpis, type = "certificate", cid = Model.company.CompanyID }); %>
            <% Html.RenderAction("KPI", "KPI", new { kpis = Model.kpis, type = "error", cid = Model.company.CompanyID }); %>
            <% Html.RenderAction("KPI", "KPI", new { kpis = Model.kpis, type = "doubleInsured", cid = Model.company.CompanyID }); %>
        
            <div class="btnlinks kpi-page">
                <input type="submit" name="submit" value="Cancel" />
                <input type="submit" name="submit" value="Save" />
            </div>
        
        <% } %>

    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/businessintelligence-manage.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/businessintellligence-manage.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

