﻿<%@ Page Title="Edit Certificate" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.VehicleCertificate>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit Certificate
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <div id= "form-header">  <img src="../../Content/more_images/Certificate.png" alt=""/>
      <div class="form-text-links">
         <span class="form-header-text"> Edit Vehicle Certificate</span>
         <div class="icon">
                <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
         </div>
      </div>
  </div> 
    <div class="field-data">

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
     
            <div id= "Alert"> <%: ViewData["Message"]  %> <br />   <%: ViewData["Date Error"]  %> </div>

            <%: Html.HiddenFor(model => model.VehicleCertificateID, new { id = "certId", @class = "dont-process" })%>
            
            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right"></div>
                     Vehicle Information 
                    <div class="valid"></div>
                </div>
                <%: Html.HiddenFor(model => model.Risk.ID, new { id = "vehId", @class = "dont-process" })%>
                <div id = "company-display"></div>
                <div class="data">
                     <div class="editor-label">
                       Chassis No: 
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.Risk.chassisno, new { @readonly = "readonly", id = "chassisno", @class = "read-only" })%>
                        <%: Html.ValidationMessageFor(model => model.Risk.chassisno)%>
                    </div>

                    <div class="editor-label">
                       Make: 
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.Risk.make, new { @readonly = "readonly", id = "VehMake", @class = "read-only" })%>
                        <%: Html.ValidationMessageFor(model => model.Risk.make)%>
                    </div>
            
                    <div class="editor-label">
                       Model:
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.Risk.VehicleModel, new { @readonly = "readonly", id = "VehModel", @class = "read-only" })%>
                        <%: Html.ValidationMessageFor(model => model.Risk.VehicleModel)%>
                    </div>                                       

                 </div> <!-- end of data section -->
            </div> <!-- end of validate section -->  

            <div class="validate-section">
                <div class="subheader"> 
                    <div class="arrow right down"></div>
                    Certificate Information 
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div class="editor-label"><b> Vehicle Certificate Number:</b>  </div> 
                    <div class="editor-field">
                       <%: Html.TextBoxFor(model => model.CertificateNo, new { @readonly = "readonly", @class = "read-only" })%>
                    </div><br/>

                    <div class="editor-label">
                        Printed Paper Number
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.PrintedPaperNo, new { id = "ppn" })%>
                        <%: Html.ValidationMessageFor(model => model.PrintedPaperNo) %>
                    </div>
           
           
                    <div class="editor-label">
                        Certificate Wording Template:
                    </div>
                    <div class="editor-field">
                        <%: Html.HiddenFor(model => model.wording.ID, new { id = "wordingID", @class = "dont-process" })%>
                        <% Html.RenderAction("TemplateDropDown", "TemplateWording", new{id= Model.Risk.ID, polId = Model.policy.ID}); %> 
                    </div>
                </div> <!-- end of data section -->
            </div> <!-- end of validate section -->  
            
            

            <div class="validate-section">
                <div class="subheader"> 
                     <div class="arrow right"></div>
                     Main Driver 
                     <div class="valid"></div>
                </div>
                 <div class="data">
                    <div class="editor-label">
                        TRN
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.Risk.mainDriver.person.TRN, new { @readonly = "readonly", id = "maindriverTRN", @class = "read-only" })%>
                        <%: Html.ValidationMessageFor(model => model.Risk.mainDriver.person.TRN)%>
                    </div>

                    <div class="editor-label">
                        First Name
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.Risk.mainDriver.person.fname, new { @readonly = "readonly", id = "maindriverLname", @class = "read-only" })%>
                        <%: Html.ValidationMessageFor(model => model.Risk.mainDriver.person.fname)%>
                    </div>
            
                    <div class="editor-label">
                        Last Name
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.Risk.mainDriver.person.lname, new { @readonly = "readonly", id = "maindriverFname", @class = "read-only" })%>
                        <%: Html.ValidationMessageFor(model => model.Risk.mainDriver.person.lname)%>
                    </div>

                 </div> <!-- end of data section -->
             </div> <!-- end of validate section --> 

            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right"></div>
                    Insurance Company Information
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div id="company">
                        <% Html.RenderAction("CompanyRead", "Company", new { id = Model.company.CompanyID }); %>
                    </div>
                </div> <!-- end of data section -->
            </div> <!-- end of validate section -->

            <div class="validate-section">
                <div class="subheader"> 
                        <div class="arrow right"></div>
                        Policy Information
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div id="policy">
                        <%: Html.HiddenFor(model => model.policy.ID, new { id = "policyID", @class = "dont-process" })%>
                        <% Html.RenderAction("ViewPolicy", "Policy", new { id = Model.policy.ID }); %>
                    </div>

                        <div class="editor-label"> 
                        Multiple Risk: 
                        <%: Html.CheckBox("MultipleRisk", (bool)ViewData["multipleRisk"], new { id = "multipleRisk", @disabled = "disabled" })%>
                    </div>
                 </div> <!-- end of data section -->
            </div> <!-- end of validate section -->

            <div class="btnlinks">
                <% Html.RenderAction("BackBtn", "Back"); %>
                <input type="submit" value="Save Certificate" id="Submit2"/>
            </div>

        </fieldset>

    <% } %>

 </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
   <script src="../../Scripts/ApplicationJS/vehicleCertificatePPN.js" type="text/javascript"></script>
   <script src="../../Scripts/ApplicationJS/VehicleCertificateCreate.js" type="text/javascript"></script>
   <script src="../../Scripts/ApplicationJS/personDetails.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

