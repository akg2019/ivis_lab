﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Phone>" %>

    <div class="newphone">
        <% using (Html.BeginCollectionItem("phoneNums")) 
           { %>
            <%: Html.ValidationSummary(true) %>
                
                <div class="editor-label">
                    Phone Number: 
                </div>
                
                <% if (Model.PhoneNumberId != 0)
                   { %>
                        <div class="editor-field">

                         <h5> <%: Model.CountryCode %> <%: Model.PhoneNumber %>    (Ext: <%: Model.Extension %>)</h5>

                        </div>
                <% } %>
            

               <div class="editor-label">
                    Is Primary: 
                </div>
                <div class="editor-field">
                    <%: Html.CheckBoxFor(model => model.primary, new { @class = "phonePrimary"})%>
                    <%: Html.ValidationMessageFor(model => model.primary)%>
                </div>
            
            
        <% } %>
    </div>


<link href="../../Content/CompanyContact.css" rel="stylesheet" type="text/css" />
