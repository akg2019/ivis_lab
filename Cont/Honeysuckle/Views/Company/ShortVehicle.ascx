﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Vehicle>>" %>

<%: Html.Hidden("count", Model.Count(), new { @class = "dont-process" })%>

    <%
        bool vehicle_update = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Vehicles", "Update"));         
        bool vehicle_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Vehicles", "Read"));
        bool vehicle_delete = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Vehicles", "Delete"));
        bool covernote_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Read"));
        bool policy_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Policies", "Read"));
    %>

     <div class="ivis-table">
         <div class="shortveh menu">
            <div class="rowveh chassisno item">
                Chassis
            </div>
            <div class="rowveh regno item">
                Reg. No.
            </div>
            <div class="rowveh make item">
                Make
            </div>
            <div class="rowveh model item">
                Model
            </div>
         </div>
         <div class="scroll-table">
             <% int i = 0; %>
             <% foreach (var item in Model) { %>
                <% i++; %>
                <% if (i % 2 == 0){ %>
                <div class="shortveh data even row vehicle">
                <% } %>
                <% else{ %>
                <div class="shortveh data odd row vehicle">
                <% } %>
                    <%: Html.Hidden("hiddenid", item.ID, new { @class = "vehid id dont-process" })%>
                    <%: Html.Hidden("vid", item.ID, new { @class = "dont-process" })%>
                     <%: Html.Hidden("hidimport", item.imported, new { @class = "hidimport dont-process" })%>
                    <div class="rowveh chassisno item">
                        <%: item.chassisno %>
                    </div>
                    <div class="rowveh regno item">
                        <%: item.VehicleRegNumber %>
                    </div>
                    <div class="rowveh make item">
                        <%: item.make %>
                    </div>
                    <div class="rowveh model item">
                        <%: item.VehicleModel %>
                    </div>
                    <div class="rowveh functions item">
                        <% if (vehicle_update) { %> 
                            <div id="editImage" class="icon edit" title="Edit Vehicle"> 
                               <img src="../../Content/more_images/edit_icon.png" class="sprite" />
                            </div> 
                        <% } %>
                        <% if (vehicle_read) { %> 
                            <div class="icon view" title="View Vehicle Details">
                                <img src="../../Content/more_images/view_icon.png" class="sprite" />
                            </div>
                        <% } %>
                        <% if (policy_read) { %>
                            <% if (item.vehicle_under_pol) { %>
                            <div class="icon current-policy" title="View Current Policy">
                                <img src="../../Content/more_images/view-related-policy-icon.png" class="sprite"/>
                            </div>
                            <% } %>
                        <% } %>
                        <% if (covernote_read) { %>
                            <% if(item.vehicle_has_active_cover_note) { %>
                                <div class="icon current-cover" title="View Current Cover Note">
                                    <img src="../../Content/more_images/Cover_Note_Old.png" />
                                </div>
                            <% } %>
                        <% } %>
                        <% if (covernote_read) { %>
                            <% if (item.vehicle_has_cover_note_history) { %>
                                <div class="icon current-cover-history" title="View Cover Note History">
                                    <img src="../../Content/more_images/cover-note-history-icon.png" class="sprite" />
                                </div>
                            <% } %>
                        <% } %>
                    </div>
                </div>
    
            <% } %>
        </div>
    </div>
