﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Model.CompanyName %> Company Settings:
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <legend> </legend>
               <h2>CompanySettings</h2>
            
            <div class="editor-label">
                <%: Html.HiddenFor(model => model.CompanyID) %>
            </div>
           
           <div class="editor-label">
                <%: Html.LabelFor(model => model.IAJID) %> 
                <%: Html.TextBoxFor(model => model.IAJID, new { id = "IAJID", @readonly = "readonly", @required = "required" })%>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.CompanyName) %>
                <%: Html.TextBoxFor(model => model.CompanyName, new { id = "CompanyName", @required = "required", @readonly = "readonly" })%>
            </div>

             <div class="editor-label">
                <%: Html.LabelFor(model => model.trn) %>
                <%: Html.TextBoxFor(model => model.trn, new  {@readonly = "readonly" })%>
            </div>
            
             <div class="editor-label">
                <%: Html.LabelFor(model => model.CompanyType) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.CompanyType, new { @readonly = "readonly" })%>
                <%: Html.ValidationMessageFor(model => model.CompanyType) %>
            </div>

            <br /> 
            <div class="editor-label">
                <%: Html.LabelFor(model => model.floginlimit) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.floginlimit) %>
                <%: Html.ValidationMessageFor(model => model.floginlimit) %>
            </div>
            

             <!-- RENDER ADDRESS -->
            <% if (Model.CompanyAddress != null)
               { %>
                <div class="address">
                    <% Html.RenderPartial("~/Views/Address/Address.ascx", Model.CompanyAddress); %>
                </div>
            <% } %>
            <% else
               { %>
                <% Html.RenderAction("Address", "Address"); %>
            <% } %>
          

             <br /> <br />
             <h4> Certificate Wording: </h4>
             <% Html.RenderAction("TemplateDropDown", "TemplateWording"); %>

            <p>
                <input type="submit" value="Save" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Back", "Index", "Home") %> 

         <% if (Honeysuckle.Models.UserGroupModel.TestUserGroup(Honeysuckle.Models.Constants.user, (new Honeysuckle.Models.UserGroup("Company Administrator"))) || (Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user)))
            { %>
                 <%: Html.ActionLink("Edit Contact Info", "EditCompContactInfo", new { id = Model.CompanyID })%> |
         <% } %>

         <%: Html.ActionLink("View Contact Info", "ViewContactInfo", new { id = Model.CompanyID })%> 

         <% if (Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Model.CompanyID)) %>
         <% if (Honeysuckle.Models.UserGroupModel.TestUserGroup(Honeysuckle.Models.Constants.user, (new Honeysuckle.Models.UserGroup("Company Administrator"))) || (Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user)))
            { %>
                <%: Html.ActionLink("Edit Intermediaries", "Intermediaries", new { id = Model.CompanyID })%> |
         <% } %>

    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
 <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftBar" runat="server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ContactList" runat="server">
</asp:Content>

