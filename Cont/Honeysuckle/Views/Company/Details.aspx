﻿<%@ Page Title="Company Details" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: Model.CompanyName %> Company Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<% 
    bool allow = false;
    int MyCompId  = Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID;
    //Testing if the viewer of the company, is apart of the company that created the company (in the case of policy-holders)
    //bool PolicyHolder = (Honeysuckle.Models.CompanyModel.GetShortCompany(Model.CompanyID,MyCompId).CompanyName != null);
    if (Honeysuckle.Models.Constants.user.newRoleMode) allow = (Honeysuckle.Models.Constants.user.tempUserGroup.company.CompanyID == Model.CompanyID);
    bool is_company_regular = (bool)Honeysuckle.Models.CompanyModel.IsCompanyRegular(Model.CompanyID);
    bool company_settings = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Settings"));
    bool am_admin = Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user);
    bool is_insurer = Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Model.CompanyID);
%>

    <fieldset>
        <div id= "form-header"> <img src="../../Content/more_images/Company.png" alt=""/>
            <div class="form-text-links">
                <div class="form-header-text">Company Details</div> 
               
                 <div class="menu-click">
                     <div class="icon">
                        <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                    </div>
                    <ul id="menu-links">
                        <li><span class="menu-links-heading">Contacts</span>
                                <ul>
                                   <% if ((bool)Honeysuckle.Models.CompanyModel.IsCompanyRegular(Model.CompanyID)){
                                        if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("PolicyHolder", "Update")) && is_company_regular) { %> 
                                            <li><%: Html.ActionLink("Add Contacts", "EditCompContactInfo", new { id = Model.CompanyID })%></li>
                                        <%  } %>  
                                        <li><%: Html.ActionLink("View Contacts", "ViewContactInfo", new { id = Model.CompanyID })%></li> 
                                  <% } %>
                                    <% else { %>  
                                        <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company","EditInfo")) && (MyCompId == Model.CompanyID || allow)) { %> 
                                                <li><%: Html.ActionLink("Add Contacts", "EditCompContact", new { id = Model.CompanyID })%></li>
                                        <% } %>  
                                        <li><%: Html.ActionLink("View Contacts", "ViewCompContactInfo", new { id = Model.CompanyID })%></li> 
                                    <% } %>  
                                </ul>
                            </li>
                    </ul>
                </div>
            </div>
        </div>


        <div class="field-data">

            <%: Html.HiddenFor(model => model.CompanyID, new { @class = "hiddenid dont-process" }) %>
            
            <div class="validate-section">
                <div class="subheader"> 
                    <div class="arrow right down"></div>
                    Main Company Information
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div class ="editor-label">
                        Company Name
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.CompanyName, new { @class = "read-only", @readonly = "readonly" })%> 
                    </div>
                    <div class="editor-label">
                        TRN 
                    </div>
                    <% if (Model.trn == "1000000000000")
                       { %>
                     <div class ="editor-field">  
                        <%: Html.TextBox("defaultTRN", "Not Available", new { @class = "read-only", @readonly = "readonly" })%> 
                    </div> 
                    <% }
                       else
                       { %>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.trn, new { id = "trn", @class = "read-only", @readonly = "readonly", @PlaceHolder = " Eg. 0888776655" })%>
                    </div>
                    <%} %>

                    <% if (!is_company_regular){ %>
                        <div class ="editor-label">
                            IAJID
                        </div>
                        <div class ="editor-field">
                            <%: Html.TextBoxFor(model => model.IAJID, new { @class = "read-only", @readonly = "readonly" })%> 
                        </div>

                        <div class ="editor-label">
                            Company Type
                        </div>
                        <div class ="editor-field">
                            <%: Html.TextBoxFor(model => model.CompanyType, new { @class = "read-only", @readonly = "readonly" })%> 
                        </div>
                    <% } %>

                </div>
            </div>
            
            <% if (!is_company_regular && ((company_settings && Model.CompanyID == MyCompId) || am_admin)) { %>
                <div class="validate-section">
                    <div class="subheader"> 
                        <div class="arrow right"></div>
                        Shortenings 
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <% if (is_insurer) { %>
                            <div class="editor-label compCode">
                                Insurer Code
                            </div>
                            <div class = "editor-field">
                                <%: Html.TextBoxFor(model => model.insurercode, new { @class = "read-only compCode", @readonly = "readonly", id = "compInsCode"})%>
                            </div>
                        <% } %>

                        <div class="editor-label">
                            Company Shortening Code
                        </div>
                        <div class = "editor-field">
                            <%: Html.TextBoxFor(model => model.compshort, new { @class = "read-only", @readonly = "readonly" })%>
                        </div>
                    </div>
                </div>
                <div class="validate-section">
                    <div class="subheader"> 
                        <div class="arrow right"></div>
                        Admin Settings 
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="editor-label">
                            Password Retries
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.floginlimit, new { @class = "read-only", @readonly = "readonly" })%>
                            <%: Html.ValidationMessageFor(model => model.floginlimit)%>
                        </div>
                        <div class="check-div">
                            <div class="editor-label autoCanel">
                                Auto Cancel Policy Risk
                            </div>
                            <div class="editor-field">
                                <%: Html.CheckBoxFor(model => model.autoCancelVehicle, new { @id = "auto-cancel-vehicle", @Class = "autoCanel", @disabled = "disabled" })%>
                            </div>
                        </div>
                    </div>
                </div>
            <% } %>
            
            <div class="validate-section">                    
                <div class="subheader">
                    <div class="arrow right"></div>
                    Company Address
                    <div class="valid"></div>
                </div>
                <div class="data">

                <%if (Model.CompanyAddress.longAddressUsed)
                  {  %>
                         <div class="editor-label">
                             <div>Combined Address:</div>
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.CompanyAddress.longAddress, new { id = "longAddress", @readonly = "readonly", @autocomplete = "off", @class = "address-details read-only" })%>
                            <%: Html.ValidationMessageFor(model => model.CompanyAddress.longAddress)%>
                        </div>
                <%}
                  else
                  {%>

                        <div class ="editor-label">
                            Road:
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.CompanyAddress.roadnumber, new { @class = "read-only", @readonly = "readonly", id = "RoadNumber", })%>
                            <%: Html.TextBoxFor(model => model.CompanyAddress.road.RoadName, new { @class = "read-only", @readonly = "readonly", id = "roadname", })%>
                            <%: Html.TextBoxFor(model => model.CompanyAddress.roadtype.RoadTypeName, new { @class = "read-only", @readonly = "readonly", id = "roadtype" })%>
                        </div>
            
                        <div class ="editor-label">
                            Town/City:
                        </div>
                        <div class ="editor-field">
                            <%: Html.TextBoxFor(model => model.CompanyAddress.city.CityName, new { @class = "read-only", @readonly = "readonly" })%>
                        </div>
            
                        <div class ="editor-label">
                            Parish:
                        </div>
                        <div class ="editor-field">
                            <%: Html.TextBoxFor(model => model.CompanyAddress.parish.parish, new { @class = "read-only", @readonly = "readonly" })%>
                        </div>
            
                        <div class ="editor-label">
                            Country:
                        </div>
                        <div class ="editor-field">
                            <%: Html.TextBoxFor(model => model.CompanyAddress.country.CountryName, new { @class = "read-only", @readonly = "readonly" })%>
                        </div>
                    <% } %>
                </div>
            </div>
            
            <div class="btnlinks">
                <% Html.RenderAction("BackBtn", "Back"); %>
                
                <% if ((bool)Honeysuckle.Models.CompanyModel.IsCompanyRegular(Model.CompanyID)) { %>
                      <%  if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("PolicyHolder", "Update")) && (Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user) || Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user) || is_company_regular)) { %>
                             <button type="button" class="editbtn" onclick="editCompany()" >Edit</button>
                      <%  } %>  
                <%  } %>          
                       
                <% else if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Update")) && (MyCompId == Model.CompanyID || allow || Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user) || Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user)))
                    { %>
                        <button type="button" class="editbtn" onclick="editCompany()" >Edit</button>
                <%  } %>
            </div>
        </div>
        
    </fieldset>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
</asp:Content>



