﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Company>>" %>

<div id="insured" class="insured-modal">

    <div class="line">
        <span class="mess"> Please tick the appropriate companies:</span>
        <div id="searching" class="index-search">
            <%: Html.TextBox("search-modal", null, new { @placeholder = "Search Companies", @autocomplete = "off", @class = "modalSearch compSearch"})%> 
            <button type="button" class="quick-search-btn" onclick="searchCompanies(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
        </div>
    </div>

    <div class="ivis-table modal-list">
        <div class= "heading menu"> 
            <div class="select item" >Select</div>
            <div class="trn item" >TRN</div>
            <div class="companyName item" >Company Name</div>
        </div>
        <div id="selected-companies" class="selectedSection"></div>
        <div id="companiesList">
            <% if(Model.Count() == 0) {%>
                <div class="rowNone">There Are No Companies. Please add one.</div>
            <% } %>
            <% else { %>
                <% if(!(bool)ViewData["inter"] && Model.Count() > 50) { %>
                    <div class="rowNone">
                        Please search the system for existing companies, or add a new company. 
                    </div>
                <% } %>

                <% int i = 0; %>
                <% foreach (Honeysuckle.Models.Company item in Model){ %>
                    <% string TRN = "";
                         if (item.trn == "1000000000000") TRN = "Not Available"; else TRN = item.trn;
                            %>
                    <% if (i % 2 == 0) { %> <div class="company-row data even row"> <% } %>
                    <% else { %> <div class="company-row data odd row"> <% } %>
                    <% i++; %>
                        <div class="company-check select item">
                            <%: Html.CheckBox("companycheck", false, new { @class = "companycheck", id = item.CompanyID.ToString(), @OnClick = "changeCompanyCheckBox(this)" })%>
                        </div>
                        <div class="company-trn trn item">
                            <%: TRN %>
                        </div>
                        <div class="companyName item">
                            <%: item.CompanyName %>
                        </div>
                    </div>
                <% } %>
          <%  } %>
        </div>
    </div>
   
   </div>
</div>
<div class="recordsFound ResultModal green"></div>
<div class="btnlinks modal">
    <input type="button" value="Cancel" class="cancelButton" onclick="closeDiag()" />
    <% if (!(bool)ViewData["inter"]) { %>
        <input type="button" value="Create New" id="create" onclick="create()" /> 
    <% } %>
    <input type="button" value="Add Selected" id="buttonAdd" onclick="getValues()" />
</div>  
