﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.Company>>" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
           <fieldset>
              <legend></legend>

       <h2>Intermediaries</h2>

      <%: Html.Hidden("company", null, new { @required = "required", id = "compId", @Value = ViewData["compID"] })%>

       <div id="company-modal"></div>
        <div id = "addCompany1">Intermediary Companies  
           <% if (!(bool)ViewData["viewList"])
                  { %> 
                      <input type="button" value="Add New" id="addCompany"/></div>
               <% } %>

             <div id="company">
             <% foreach (var item in Model) 
                { %>

                 <!-- THIS IS SHOWN WHEN THE INTERMEDIARY COMPANIES ONLY ARE TO BE SHOWN, FOR ADDING, REMOVAL ETC-->
               
                 <% if (!(bool)ViewData["viewList"])
                        { %>
                            <% Html.RenderAction("CompanyView", "Company", new { compId = item.CompanyID});  %>
                     <% } %>


                     <!-- THIS IS SHOWN WHEN THE INTERMEDIARY COMPANIES AND THEIR POLICIES ETC ARE TO BE SHOWN-->
                     <% if ((bool)ViewData["viewList"])
                        { %>
                            <% Html.RenderAction("CompanyView", "Company", new { compId = item.CompanyID, type = "details"});  %>
                              <%: Html.ActionLink("View Policies", "Index","Policy", new {  id=item.CompanyID }, null) %> |
                                <%: Html.ActionLink("View Vehicles", "Index", "Vehicle", new {  id=item.CompanyID }, null) %> |
                            <%: Html.ActionLink("View Certificates", "Index", "VehicleCertificate", new {  id=item.CompanyID }, null) %> |
                         <%: Html.ActionLink("View Cover Notes", "Index", "VehicleCoverNote", new {  id=item.CompanyID }, null) %> |
                     <% } %>

                       
             <% } %>
        </div>

        <br /> <br /><br />

         <% if (!(bool)ViewData["viewList"])
             { %>
                   <input type="button"  id = "Done" value = "Done" style="clear:both;float:left;"  /> 
          <% } %>

     </fieldset>
    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/Intermediaries.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/personDetails.js" type="text/javascript"></script>
    <link href="../../Content/Pages.css" rel="stylesheet" type="text/css" />
</asp:Content>


