﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Company>>" %>

<% if (ViewData["source"] != null){ %>
    <%: Html.DropDownList("source_company", new SelectList(Model.ToList(), "CompanyID", "CompanyName"), new { @class = "company_dropdown", id = "compDropDown", @required = "required" })%>
<% } %>
<% else { %>
    <%: Html.DropDownList("companies", new SelectList(Model.ToList(), "CompanyID", "CompanyName"), new { @class = "company_dropdown", id="compDropDown",@required = "required" }) %>
<% } %>
