﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Policy>" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<br />

<% using (Html.BeginForm()) { %>
<%: Html.ValidationSummary(true) %>
 <fieldset> <legend>Add drivers to vehicle: </legend>
 <%: Html.HiddenFor(model => model.ID, new { id = "hiddenid" }) %>
 <% foreach (var item in Model.vehicles)
    { %>
 <div class="editor-field">
    <%: Html.TextBox(item.VIN, new { @readonly = "readonly", id = "vin" })%>
</div>

<div class="row" style="clear:both;">
<div id="authorized1"> <h6>ADD Authorized drivers </h6><input type="button" value="Add New" id="authorized"/></div>
<div id="auth"></div>
<div id="authorizedDriver"></div><br />

<div id="excepted1"><h6> ADD Excepted drivers</h6> <input type="button" value="Add New" id="excepted"/></div>

<div id="except"></div>
<div id="exceptedDriver"></div> <br />

<div id="excluded1"><h6> ADD Excluded drivers </h6><input type="button" value="Add New" id="excluded"/></div>
<div id="excl"></div>
<div id="excludedDriver"></div>
<div id="createDriv"></div>
<div id="createDriver"></div>
<div id="vehicleVIN"></div>

<br /> <br /><br />
  <p>
  <input type="button" value="Done" id="Done"/>
  <!--  <input type="submit" value="Done" /> -->
 </p>

</div>
<% } %>
</fieldset>
<% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/addDrivers.js" type="text/javascript"></script>
</asp:Content>