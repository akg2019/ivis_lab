﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Policy>" %>

<div class="editor-label">
    EDI System ID
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.EDIID, new { @readonly = "readonly", @class = "read-only" })%>
</div>
            
<div class="editor-label">
    Created On
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.CreatedOn, new { @readonly = "readonly", @class = "read-only" })%>
</div>
<div class="editor-label">
    Policy No.
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.policyNumber, new { @readonly = "readonly", @class = "read-only" })%>
</div>      
<div class="editor-label">
    Start Date Time
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.startDateTime, new { @readonly = "readonly", @class = "read-only" })%>
</div>
<div class="editor-label">
    End Date Time
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.endDateTime, new { @readonly = "readonly", @class = "read-only" })%>
</div>
<div class="editor-label">
    Cover:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.policyCover.cover, new { @readonly = "readonly", @class = "read-only"}) %>
</div>
<div class="editor-label">
    Is Scheme
</div>
<div class="editor-field">
    <%: Html.CheckBoxFor(model => model.Scheme, new { @readonly = "readonly", @class = "read-only", @disabled = "disabled" })%>
</div>
<div class="editor-label">
    Insured Type
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.insuredType, new { @readonly = "readonly", @class = "read-only" })%>
</div>
<% 
    foreach (var company in Model.company)
    {
        Html.RenderPartial("~/Views/Company/AuditCompanyPolicyHolder.ascx", company);
    }
    foreach (var person in Model.insured)
    {
        Html.RenderPartial("~/Views/People/AuditPersonPolicyHolder.ascx", person);
    }
    foreach (var risk in Model.vehicles)
    {
        Html.RenderPartial("~/Views/Vehicle/AuditRiskFromPolicy.ascx", risk);
    }
       
%>


            
<div class="editor-label">
    Imported
</div>
<div class="editor-field">
    <%: Html.CheckBoxFor(model => model.imported, new { @readonly = "readonly", @class = "read-only", @disabled = "disabled" })%>
</div>
