﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Policy>>" %>

<%
    bool policy_update = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Policies", "Update"));
    bool policy_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Policies", "Read"));
    bool policy_delete = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Policies", "Delete"));
%>
<%: Html.Hidden("count", Model.Count(), new { @class = "dont-process" })%>

<div class="ivis-table">
    <div class="shortpol menu">
        <div class="rowpol start item">
            Start Date
        </div>
        <div class="rowpol end item">
            End Date
        </div>
        <div class="rowpol instype item">
            Insured Type
        </div>
        <div class="rowpol polpref item">
            Policy Prefix
        </div>
        <div class="rowpol polnum item">
            Policy Number
        </div>
    </div>
    <div class="scroll-table">
        <% int i = 0; %>
        <% foreach (var item in Model) { %>
            <% i++; %>
            <% if (i%2 == 0){ %>
            <div class="policy data even row">
            <% } %>
            <% else{ %>
            <div class="policy data odd row">
            <% } %>
                <%: Html.Hidden("did", item.ID, new { @class = "hiddenid id dont-process" })%>
                <div class="rowpol start item">
                    <%: String.Format("{0:dd MMM yyyy}", item.startDateTime) %>
                </div>
                <div class="rowpol end item">
                    <%: String.Format("{0:dd MMM yyyy}", item.endDateTime)%>
                </div>
                <div class="rowpol instype item">
                    <%: item.insuredType %>
                </div>
                <div class="rowpol polpref item">
                    <%: item.policyCover.prefix %>
                </div>
                <div class="rowpol polnum item">
                    <%: item.policyNumber %>
                </div>
                    <%: Html.Hidden("pid", item.ID, new { @class = "dont-process" })%>
                <div class="rowpol functions item">
                    <% if (policy_update) { %> 
                        <div class="icon edit" title="Edit Policy"> 
                            <img src="../../Content/more_images/Edit_icon.png" class="sprite"/> 
                        </div>
                    <% } %>
                    <% if (policy_read) { %>
                        <div class="icon view" title="View Policy"> 
                            <img src="../../Content/more_images/View_icon.png" class="sprite"/>
                        </div>
                    <% } %>
                    <% if(policy_read) { %>
                        <div class="icon vehicles" title="View Vehicles on Policy">
                            <!--<img src="../../Content/more_images/Vehicle_Old.png" />-->
                            <img src="../../Content/more_images/view-policy-risk-icon.png" class="sprite" />
                        </div>
                    <% } %>
                </div>
            </div>
    
        <% } %>
    
    </div>
</div>



