﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.UserGroup>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	AssignToUsers
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    
        <div id="form-header"><img src="../../Content/more_images/User.png" />
            <div class="form-text-links">
                <span class="form-header-text">Assign Roles to Users</span>
                <div class="icon">
                    <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                </div>
            </div>
        </div>
        <div id="noDeleteUser"><%: ViewData["users"] %></div>
        <% using (Html.BeginForm("AssignToUsers", "RoleManagement", FormMethod.Post)) { %>
            <fieldset>
                <%: Html.HiddenFor(m => Model.UserGroupID, new { @class = "dont-process hidid" })%>
                <div class="field-data">
                    <div class="validate-section">
                        <div class="subheader">
                            <div class="arrow right down"></div>
                            Main User Group Data
                            <div class="valid"></div>
                        </div>
                        <div class="data">
                            <div class="editor-label userGroupName">
                                User Group Name 
                            </div>
                            <div class="editor-field">
                                <%: Html.HiddenFor(model => model.UserGroupID, new { @class = "dont-process" })%>
                                <%: Html.TextBoxFor(model => model.UserGroupName, new { @class = "read-only", @readonly = "readonly" })%>
                            </div>
                            <div class="company">
                                <div class="editor-label">
                                    Company Group is Assigned To:
                                </div>
                                <div class="editor-field">
                                    <%: Html.TextBoxFor(model => model.company.CompanyName, new { @class = "read-only", @readonly = "readonly" }) %>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="validate-section permission-header">
                        <div class="subheader">
                            <div class="arrow right"></div>
                            Assign to Users
                            <div class="valid"></div>
                            <div class="server-responses"></div>
                        </div>
                        <div class="data">
                            <div class="modify">
                                <div class="not-in-group group">
                                    <div class="heading">Users Not Assigned to this Role</div>
                                    <div class="search-section">
                                        <input placeholder="eg. group" class="searching out quick-search rolemanagement-assign ignore" autocomplete="off" />
                                        <button type="button" class="quick-search-btn" onclick="clickSearchBtn(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
                                    </div>
                                    <% Html.RenderAction("UsersNotInRoleList", "User", new { id = Model.UserGroupID }); %>
                                </div>
                                <div class="move-buttons">
                                    <button type="button" class="add-button perm move btnclr" onclick="addUser()" >>></button>
                                    <button type="button" class="rm-button perm move btnclr" onclick="rmUser()" ><<</button>
                                    <!--
                                    <input value=">>" name="submit" type="submit" class="add-button" />
                                    <input value="<<" name="submit" type="submit" class="rm-button" />
                                    -->
                                </div>
                                <div class="in-group group">
                                    <div class="heading">Users Assigned to this Role</div>
                                    <div class="search-section">
                                        <input placeholder="eg. group" class="searching in quick-search rolemanagement-assign ignore" autocomplete="off" />
                                        <button type="button" class="quick-search-btn" onclick="clickSearchBtn(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
                                    </div>
                                    <% Html.RenderAction("UsersInRoleList", "User", new { id = Model.UserGroupID }); %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btnlinks">
                    <% Html.RenderAction("BackBtn", "Back"); %>
                    <button type="button" id="assign-done">Done</button>
                </div>
            </fieldset>    
        <% } %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/rolemanagement.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/rolemanagement.js" type="text/javascript"></script>
</asp:Content>

