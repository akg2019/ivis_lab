﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Notification>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Send
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="newnote">
        <% using(Html.BeginForm("send"))
           { %>

           <fieldset> <h2>Send</h2>

            <div id="to">
                <div class="label">
                    Send To: 
                </div>
                <div class="field">
                    <%: Html.TextBox("send",null ,  new { id = "sendto", @readonly = "readonly"}) %>
                </div>
            </div>
            <div id="subject">
                <div class="label">
                    Subject: 
                </div>
                <div class="field">
                    <%: Html.TextBoxFor(model => Model.heading, new {id ="heading", @readonly = "readonly"}) %>
                </div>
            </div>
            <div id="content">
                <div class="label">
                    Content: 
                </div>
                <div class="field">
                    <%: Html.EditorFor(model => Model.content, new {id = "content"}) %>
                </div>
            </div>
            <div id="buttons">
                <input type="button" name="submit" value="Clear" id="clear" onclick="clearForm()" />
                <input type="submit" name="submit" id="send" value="Send" />
            </div>

          </fieldset>
        <% } %>
     </div>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/CLEditor/jquery.cleditor.js" type="text/javascript"></script>
    <link href="../../Content/Notification-Send.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/contact-list.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/contacts.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/notification-send.js" type="text/javascript"></script>
</asp:Content>


