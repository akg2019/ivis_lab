﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.Notification>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Trash
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="notifications-display">
        <div class="form-header"><img src="../../Content/more_images/Notification.png"  class="mailboxicon" />
              <div class="form-text-links">
                    <span class="form-header-text">Deleted Notifications</span>
               </div>
        </div>
        <div class="field-data">
            <% if (Model != null)
               { %>
                <% Html.RenderPartial("~/Views/Notification/Notifications.ascx", Model); %>
            <% } %>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/notification.js" type="text/javascript"></script>
    <link href="../../Content/NotificationView.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/contact-list.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/notificationlist.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>


