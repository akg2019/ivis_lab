﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Notification>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Reply
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Reply</h2>

    <div id="newnote">
        <% using(Html.BeginForm("send"))
           { %>
            <%: Html.HiddenFor(parent => Model.parent.ID, new { @class = "dont-process" })%>
            <div id="to">
                <div class="label">
                    Send To: 
                </div>
                <div class="field">
                    <% string usernames = null;
                       foreach (Honeysuckle.Models.User u in Model.NoteTo)
                       {
                           if (usernames != null) usernames = usernames + ';' + u.username;
                           else usernames = u.username + ';';
                       }   
                     %>
                    <%: Html.TextBox("send",null ,  new { id = "sendto", @Value = usernames}) %>
                </div>
            </div>
            <div id="subject">
                <div class="label">
                    Subject: 
                </div>
                <div class="field">
                    <%: Html.TextBoxFor(model => Model.heading, new {id ="heading", @readonly = "readonly"}) %>
                </div>
            </div>
            <div id="content">
                <div class="label">
                    Content: 
                </div>
                <div class="field">
                    <%: Html.TextBoxFor(model => Model.content, new {id = "contents"}) %>
                </div>
            </div>
            <div id="buttons">
                <input type="submit" name="submit" value="Clear" id="clear" />
                <input type="submit" name="submit" value="Send" id="send" />
            </div>
        <% } %>
     </div>

    

    <div>
        <%: Html.ActionLink("Back to List", "Index") %>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/Notification-Send.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/contact-list.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/contacts.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/notification-send.js" type="text/javascript"></script>
</asp:Content>



