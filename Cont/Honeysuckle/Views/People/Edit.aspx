﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Person>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm()) { %>
        <%: Html.ValidationSummary(true)%>

        <fieldset> 
            <div id= "form-header"><img src="../../Content/more_images/User.png" alt=""/>
                <div class="form-text-links">
                    <span class="form-header-text">Person Edit</span>
                    <div class="icon">
                        <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                    </div>
                </div>
            </div>
    
            <div class="field-data">
                <div class="validate-section">
                    <div class="subheader"> 
                        <div class="arrow right down"></div>
                        Main Information
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <%: Html.HiddenFor(model => model.PersonID, new { @class = "dont-process hidden_person_id" })%>
                        <% bool editTRN = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("TRN", "Edit")); %>
                        <div class ="editor-label">
                            TRN: 
                        </div>
                        <div class ="editor-field">
                        <% if ((bool)ViewData["TRNnotAvailable"])
                           {%>
                                <%: Html.HiddenFor(model => model.TRN, new { @class = "dont-process" })%>
                                <%: Html.TextBox("TRNnotAvailable", "Not Available", new { id = "person-trn-not-available", @required = "required", @class = "person-trn-not-available" })%>
                            <% }
                           else
                           {%>
                            <%: Html.TextBoxFor(model => model.TRN, new { @required = "required", id = "trn", @PlaceHolder = " Eg. 154785623", @class = "person-trn read-only", @readonly = "readonly" })%> 
                            <%: Html.ValidationMessageFor(model => model.TRN)%>
                            <% if (editTRN)
                               { %>
                                     <button id="edit-trn" class ="editButton" type="button">Edit</button>
                            <% } else { %>
                                    <span style="color:Red;font-size:0.5em">*No Permission to Edit TRN</span>
                            <% } %>
                          <% } %>
                        </div> 
            
                        <div class ="editor-label">
                            First Name: 
                        </div>
                        <div class ="editor-field">  
                            <%: Html.TextBoxFor(model => model.fname, new { @required = "required" })%> 
                            <%: Html.ValidationMessageFor(model => model.fname)%>
                        </div> 
            
                        <div class ="editor-label">
                            Middle Name:  
                        </div>
                        <div class ="editor-field">  
                            <%: Html.TextBoxFor(model => model.mname, new { })%> 
                            <%: Html.ValidationMessageFor(model => model.mname)%>
                        </div> 
       
                        <div class ="editor-label">
                            Last Name: 
                        </div> 
                        <div class ="editor-field">  
                            <%: Html.TextBoxFor(model => model.lname, new { @required = "required" })%> 
                            <%: Html.ValidationMessageFor(model => model.lname)%>
                        </div> 
                    </div>
            </div>

            <div class="validate-section">
                <div class="subheader"> 
                        <div class="arrow right"></div>
                        Address 
                        <div class="valid"></div>
                </div>
                <div class="data">
                      <% if (Model.address != null)
                       { %>
                    <% Html.RenderAction("Address", "Address", new { id = Model.address.ID }); %>
                    <% }
                       else
                       {%>
                    <% Html.RenderAction("Address", "Address", new { id = 0 }); %>
                    <% } %>

                </div>
            </div>

                <div class="btnlinks">
                    <% Html.RenderAction("BackBtn", "Back"); %>
                    <input type="submit" value="Save Changes" name="submit" id="Submit2" class="saveButton" />
                </div>
             </div>
        </fieldset>

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>
    <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/person-trn-edit.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
