﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Person>" %>

    <% using (Html.BeginCollectionItem("person")) {%>
        <%: Html.ValidationSummary(true) %>

        <%: Html.HiddenFor(model => model.PersonID, new { id = "personid", @class = "dont-process" })%>
        <% Html.RenderAction("PolicyInsuredDropDown", "Policy", new { type = "maindriver-dropdown" }); %>
            
        <div class="editor-label">
            TRN <span class="red">*</span>
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => model.TRN, new { @required = "required", id = "trn" , @PlaceHolder = " Eg. 0888776655", @class = "driver-details" })%>
            <%: Html.ValidationMessageFor(model => model.TRN)%>
        </div>

        <div class="editor-label">
            First Name <span class="red">*</span>
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => model.fname, new { @required = "required", id = "fname", @class = "driver-details" })%>
            <%: Html.ValidationMessageFor(model => model.fname)%>
        </div>
            
        <div class="editor-label">
            Middle Name 
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => model.mname, new { id = "mname", @class = "driver-details" })%>
            <%: Html.ValidationMessageFor(model => model.mname)%>
        </div>
            
        <div class="editor-label">
            Last Name <span class="red">*</span>
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => model.lname, new { @required = "required", id = "lname", @class = "driver-details" })%>
            <%: Html.ValidationMessageFor(model => model.lname)%>
        </div>

        <!-- RENDER ADDRESS -->
        <div class="subheader">Address</div>
        <div class="driver-details">
            <% if (Model.address != null) { %>
                <div class="address">
                    <% Html.RenderPartial("~/Views/Address/Address.ascx", Model.address); %>
                </div>
            <% } %>
            <% else
                { %>
                <% Html.RenderAction("Address", "Address"); %>
            <% } %>
        </div>

        <div class="emails">
            <div class="email-container">
                <% if (Model.emails != null)
                    { %>
                    <% foreach (Honeysuckle.Models.Email email in Model.emails)
                        { %>
                        <% Html.RenderPartial("~/Views/Email/NewEmail.ascx", email); %>
                    <% } %>
                <% } %>
            </div>
        </div>
   
       <script src="../../Scripts/ApplicationJS/Vehiclemanagement.js" type="text/javascript"></script> 
       <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>

    <% } %>

    

