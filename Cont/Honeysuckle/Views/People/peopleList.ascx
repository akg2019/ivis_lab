﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Person>>" %>

<div id="insured" class="insured-modal">

    <div class="line">
        <span class="mess">Please tick the appropriate person(s):</span>
        <div id="searching" class="index-search">
         <!-- <input type="text" name="search"  placeholder="Search People" class="people-search" onkeyup="searchPeople(this)" autocomplete="off" /> -->
            <%: Html.TextBox("search-modal", null, new { @placeholder = "Search People", @autocomplete = "off", @class = "modalSearch peopleSearch" })%> 
            <button type="button" class="quick-search-btn" onclick="searchPeople(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
        </div>
    </div>
    <div class="ivis-table modal-list">
        <div class="menu"> 
            <div class="select item">Select</div>
            <div class="trn item" >TRN</div>
            <div class="fname item" >First Name</div>
            <div class="fname item" >Last Name</div>
        </div>

        <div id="selected-insured" class="static selectedSection"></div>

        <div id="insureds" class="dynamic">
            <% if(Model.Count() == 0) { %>
                    <div class="rowNone">There Are No Insured. Please add one.</div>
            <% } %>
            <% else { %>
                <% if(Model.Count() > 50) { %>
                    <div class="rowNone">
                        Please search the system for existing insureds, or add a new insured. 
                    </div>
                <% } %>

                <% int i = 0; %>
                <% foreach (Honeysuckle.Models.Person item in Model) { %>
                    <% string TRN = "";
                       if (item.TRN == "10000000") TRN = "Not Available"; else TRN = item.TRN;
                        %>
                    <% if (i%2 == 0) { %> <div class="insured-row row even" > <%} %>
                    <% else { %> <div class="insured-row row odd" > <%} %>
                    <% i++; %>
                        <div class="insured-check select item" >
                            <%: Html.CheckBox("insuredcheck", false, new { @class = "insuredcheck", id = item.PersonID.ToString(), @OnClick="changeCheckBox(this)" })%>
                        </div>
                        <div class="insured-trn trn item" >
                            <%: TRN %>
                        </div>
                        <div class="insured-fname fname item" >
                            <%: item.fname %> 
                        </div>
                        <div class="insured-lname lname item" >
                            <%: item.lname %>
                        </div>
                     </div>
                  <% } %>
                <% } %>
            </div>
         </div>
     </div>
 </div>
 <div class="recordsFound ResultModal green"></div>
<div class="btnlinks">
   
    
    <input type="button" value="Cancel" class="cancelButton" onclick="closeDialog()" />
    <input type="button" value="Create New" id="create" onclick="loadCreate()" />
    <input type="button" value="Add Selected" id="buttonAdd" onclick="getValueUsingClass()" />
</div>