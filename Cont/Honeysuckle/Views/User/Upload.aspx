﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.response>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Upload
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id= "form-header"><img src="../../Content/more_images/User.png" alt=""/> 
        <div class="form-text-links">
            <span class="form-header-text">User Upload</span>
        </div>
    </div>

    <fieldset>
        <div class="field-data">
            <% using (Html.BeginForm("Upload", "User", FormMethod.Post, new { enctype = "multipart/form-data" }))
               { %>
                <label for="file"> Select File :</label>
                <input type="file" name="UploadedFile" value="" accept="text/plain" class="fileUpload" />
                <input id="btnAdd" type="submit" class="button" name="Add" value="Upload"/>
            <% } %>
            <% if (ViewData["error"] != null) ViewData["error"].ToString(); %>
            <% Html.RenderAction("ParseResponse", "Response", new { responses = Model }); %>
        </div>
    </fieldset>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/responses.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/responses.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
