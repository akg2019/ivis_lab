﻿<%@ Page Title="Modify User" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.User>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Modify
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <%
        bool reset_password = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "ResetPassword"));
        bool reset_login_timer = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "Reset Login Timer"));
        bool activate = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "Activate"));
        bool an_admin = Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user);
        bool iaj_admin = Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user);
    %>
    <% using (Html.BeginForm()) { %>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <div id= "form-header"><img src="../../Content/more_images/User.png" alt=""/>
                <div class="form-text-links">
                    <div class="form-header-text">User Edit</div>
                    <div class="menu-click">
                        <div class="icon">
                             <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                        </div>
                        <% if (an_admin || iaj_admin ||  reset_password ||  reset_login_timer ||  activate) { %> 
                            <ul id = "menu-links">
                                <li>
                                    <span class="menu-links-heading">User Links</span>
                                    <ul class="user-li-links">
                                        <% if (reset_password && Model.active)
                                           { %>  
                                            <li>
                                                <a href="#" class="reset reset-password" onclick="resetPassword();">Reset Password To User</a> <br />
                                            </li>
                                        <% } %>
                                        <% if (reset_login_timer && Model.active)
                                           { %>
                                            <li>
                                                <a href="#" class="reset reset-question" onclick="resetQuest();">Reset Password and Question</a>
                                            </li>
                                        <% } %>
                                        <% if (activate) { %>
                                            <% if(Model.active) { %>
                                                <li>
                                                    <a href="#" class="reset" onclick="deactivateUser(this);">Deactivate User</a> 
                                                </li>
                                            <%  }  %>
                                            <% else { %>
                                                <li>
                                                    <a href="#" class="reset" onclick="activateUser(this);">Activate User </a> 
                                                </li>
                                            <%  } %>
                                        <%  } %>
                                    </ul>
                                </li>
                            </ul>
                        <% } %>
                    </div>
                </div>
            </div>
            
            <div class="field-data">
                <% if (ViewData["usernameExists"] != null)
                   { %>
                    <div id="userExists"><%: ViewData["usernameExists"]%></div>
                <%} %>
                <div id="emailExists"><%: ViewData["emailExists"] %></div>
                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right down"></div>
                        Main User Data
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <%: Html.HiddenFor(model => model.ID, new { id = "hiddenid", @class = "userid hiddenid dont-process" })%>
                        <%: Html.HiddenFor(model => model.IAJcreated, new { id = "hiddenIAJcreated", @class = "isIAJcreated dont-process" })%>
                        <%: Html.HiddenFor(model => model.employee.company.CompanyID, new { id = "cid", @class = "cid dont-process" })%>
                        <div class="editor-label">
                            username
                        </div>
                        <div class="editor-field">
                        <% if (ViewData["usernameExists"] != null && ViewData["username"] != null)
                           { %>
                                <%: Html.Hidden("hiddenUsername", ViewData["username"].ToString(), new { @class = "usernameHidden dont-process" })%>
                           <%}
                           else
                           { %>
                           <%: Html.Hidden("hiddenUsername", Model.username, new { @class = "usernameHidden dont-process" })%>
                                <% } %>
                            <%: Html.TextBoxFor(model => model.username, new { @readonly = "readonly", id = "username", @class = "read-only email" })%>
                            <%: Html.ValidationMessageFor(model => model.username)%>
                            <button id="edit-username" class ="editButton" type="button">Edit</button>
                        </div>
                    </div>
                </div>
            
                <% if (Model.employee != null) { %>
                    <% Html.RenderPartial("~/Views/Employee/Employee.ascx", Model.employee, new ViewDataDictionary{{"edit", true}}); %>
                <% } %>
                <% else { %>
                    <% Html.RenderAction("Employee", "Employee", new { id = 0 }); %>
                <% } %>
            
                <div class="btnlinks">
                    <% Html.RenderAction("BackBtn", "Back"); %>
                    <input type="submit" value="Save" id="modify_btn" class="submitbutton" />
                </div>
            </div>
        </fieldset>

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/person-trn-edit.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/user-active-deactive.js" type="text/javascript"></script>
    <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/usernameEdit.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/company-dropdown-update.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/parishUpdate.js" type="text/javascript"></script>
    <link href="../../Content/links.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/email.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/userModify.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/TestEmail.js" type="text/javascript"></script>
</asp:Content>


