﻿<%@ Page Title="User Create" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.User>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <%: Html.Hidden("page", "create", new { @class = "dont-process" }) %>
    <%: Html.Hidden("postback", (ViewData["postback"] != null && (bool)ViewData["postback"]), new { @class = "dont-process" })%>
    
    <% using (Html.BeginForm("Create","User",FormMethod.Post)) { %>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <div id= "form-header">
                <img src="../../Content/more_images/User.png" alt=""/>
                <div class="form-text-links">
                    <span class="form-header-text">
                        Create User
                    </span>
                    <div class="icon">
                        <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                    </div>
                </div> 
            </div>
            
            <div class="error"><%:ViewData["takenUser"]%></div>

            <div class="field-data">
                <div class="validate-section">
                    <div class="subheader"> 
                        <div class="arrow right down"></div>
                        User Information
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        
                        <% if (Model.employee.company != null) { %> 
                            <% Html.RenderAction("EmployeeCompany", "Company", new { compId = Model.employee.company.CompanyID }); %>
                            <%: Html.Hidden("compid", Model.employee.company.CompanyID, new { @class = "dont-process" })%>
                            <%: Html.Hidden("companies", Model.employee.company.CompanyID, new { @class = "dont-disable dont-process" })%>
                        <% } %>
            
                        <div class="editor-label">
                            TRN
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.employee.person.TRN, new { @required = "required", id = "trn", @PlaceHolder = " Eg. 123456789", @class = "person-trn" })%> <br />
                            <%: Html.ValidationMessageFor(model => model.employee.person.TRN)%>
                        </div>
          
                        <div class="editor-label">
                            Primary Email/Login Name
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.username, new { @required = "required", id = "username", @PlaceHolder = " username@email.com", @class = "email" })%><br />
                            <%: Html.ValidationMessageFor(model => model.username)%>
                        </div>
             
                        <div class="editor-label">
                            First Name
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.employee.person.fname, new { @required = "required", id = "fname", @class = "name" })%><br />
                            <%: Html.ValidationMessageFor(model => model.employee.person.fname)%>
                        </div>
            
                        <div class="editor-label">
                            Middle Name
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.employee.person.mname, new { id = "mname", @class = "name" })%>
                            <%: Html.ValidationMessageFor(model => model.employee.person.mname)%>
                        </div>
           
                        <div class="editor-label">
                            Last Name
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.employee.person.lname, new { @required = "required", id = "lname", @class = "name" }) %>
                            <%: Html.ValidationMessageFor(model => model.employee.person.lname) %>
                        </div>
                    </div> <!-- end of data section -->
                </div> <!-- end of validate section --> 
          
                <!--////////////////// -->
                <div class="validate-section">
                    <div class="subheader"> 
                        <div class="arrow right"></div>
                        Address 
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="editor-label">
                            Road 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.employee.person.address.roadnumber, new { id = "RoadNumber", @class="padding", @PlaceHolder = " 12", @autocomplete = "off" }) %>
                            <%: Html.TextBoxFor(model => model.employee.person.address.road.RoadName, new { id = "roadname", @class = "padding", @PlaceHolder = " Short Hill", @autocomplete = "off" })%>
                            <%: Html.TextBoxFor(model => model.employee.person.address.roadtype.RoadTypeName, new { id = "roadtype", @class = "padding", @PlaceHolder = " Avenue", @autocomplete = "off" })%>
          
                            <%: Html.ValidationMessageFor(model => model.employee.person.address.roadnumber) %>
                            <%: Html.ValidationMessageFor(model => model.employee.person.address.road.RoadName) %>
                            <%: Html.ValidationMessageFor(model => model.employee.person.address.roadtype.RoadTypeName) %>
                        </div>

                        <div class="editor-label">
                            Building/Apartment No. 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.employee.person.address.ApartmentNumber, new { id = "aptNumber", @autocomplete = "off" })%>
                            <%: Html.ValidationMessageFor(model => model.employee.person.address.ApartmentNumber)%>
                        </div>

                        <div class="editor-label">
                            City/Town
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.employee.person.address.city.CityName, new { id = "city", @required = "required", @autocomplete = "off", @class = "city" })%>
                            <%: Html.ValidationMessageFor(model => model.employee.person.address.city.CityName)%>
                        </div>

                        <div class="editor-label">
                            Parish
                        </div>
                        <div class="editor-field">
                            <%: Html.HiddenFor(model => model.employee.person.address.parish.ID, new { id = "parish", @class = "dont-process" })%>
                            <% Html.RenderAction("ParishList", "Parish"); %>
                            <%: Html.ValidationMessage("parish") %>
                        </div>

                        <div class="editor-label">
                            Country Name
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.employee.person.address.country.CountryName, new { id = "country", @required = "required", @Value = "Jamaica", @autocomplete = "off", @class = "country" })%>
                            <%: Html.ValidationMessageFor(model => model.employee.person.address.country.CountryName)%>
                        </div>

                        
                    </div> <!-- end of data section -->
                </div> <!-- end of validate section -->  

                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right"></div>
                        Additional Emails
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="emails">
                            <div class="email-container">
                                <% if (Model.employee.person.emails != null) { %>
                                    <% foreach (Honeysuckle.Models.Email email in Model.employee.person.emails) { %>
                                        <% Html.RenderPartial("~/Views/Email/NewEmail.ascx", email); %> 
                                    <% } %>
                                <% } %>
                            </div>
                            <div class="addemail">
                                <div id="addNewEmail" class="add-dynamic small">
                                    <img src="../../Content/more_images/ivis_add_icon.png" />
                                    <span>Add Email</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right"></div>
                        Phone Numbers
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="phones">
                            <div class="phone-container">
                                       <% if (Model.employee.person.phoneNums != null) { %>
                                    <% foreach (Honeysuckle.Models.Phone phone in Model.employee.person.phoneNums) { %>
                                        <% Html.RenderPartial("~/Views/Phone/NewPhone.ascx", phone); %> 
                                    <% } %>
                                <% } %>
                            </div>
                            <div class="addNewphone">
                                <div id="addphone" class="add-dynamic small">
                                    <img src="../../Content/more_images/ivis_add_icon.png" />
                                    <span>Add Phone Number</span>
                                </div>
                           </div>
                        </div>
                    </div>
                </div>

                <% if (Model.employee.company == null) { %>
                    <% if (Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user) || Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user)) { %>  
                        <div class="editor-label"> 
                            Company
                        </div>
                        <div class="editor-field"> 
                            <% Html.RenderAction("CompanyDropDown", "Company", new { type = "InsureAndInterComp" });  %> 
                        </div>
                    <% } %>
                <% } %>
            </div>
            
            <div class="btnlinks">
                <% Html.RenderAction("BackBtn", "Back"); %>
                <input type="submit" value="Create" id="usr_crt_btn" class="submitbutton" />
            </div>

        </fieldset>

    <% } %>

    <div id="spinner"></div>
    <!--<div><input type =text onmouseout</div>--!-->
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/spin.min.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/multiple-emails.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/primary-email.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/userManagement.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/person-trn.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/multiplePhoneNums.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/parishUpdate.js" type="text/javascript"></script>
    <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/spinner.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/email.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

</asp:Content>


