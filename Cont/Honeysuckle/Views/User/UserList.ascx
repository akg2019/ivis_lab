﻿    <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.User>>" %>

<div id="insured" class="insured-modal">
   
    <div class="line">
        <span class="mess">Please tick the appropriate user (s):</span>
        <div id="searching" class="index-search">
            <%: Html.TextBox("search-modal", null, new { @placeholder = "Search Users", @autocomplete = "off", @class="modalSearch" }) %> 
            <button type="button" class="quick-search-btn" onclick="searchOptionsUsers(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
        </div>
    </div>
    <div class="ivis-table">
        <div class="menu"> 
            <div class="select item">Select</div>
            <div class="trn item" >Username</div>
            <div class="fname item" >TRN</div>
            <div class="fname item" >Name</div>
        </div>

        <div id="selected-user" class="static selectedSection"></div>
        <div id="user">
            <% if(Model.Count() == 0) { %>
                    <div class="rowNone">There Are No Users. Please add one.</div>
            <% } %>
            <% else { %>
                <% if(Model.Count() > 50) { %>
                     <div class="rowNone">
                         Please search the system for existing users, belonging to this company.
                     </div>
                <% } %>

                <% int i = 0; %>
                <% foreach (Honeysuckle.Models.User item in Model){ %>
                    <% if (i%2 == 0) { %> <div class="user-row row even" > <%} %>
                        <% else { %> <div class="user-row row odd" > <%} %>
                        <% i++; %>
                        <div class="user-check select item">
                            <%: Html.CheckBox("usercheck", false, new { @class = "usercheck", id = item.ID.ToString(), @OnClick = "changeCheckBox(this)" })%>
                        </div>
                        <div class="username item">
                            <%: item.username %>
                        </div>
                            <div class="trn item">
                                <%: item.employee.person.TRN %>
                        </div>
                        <div class="name item">
                                <%: item.employee.person.lname %>, <%: item.employee.person.fname %>
                        </div>
                     </div>
                  <% } %>
                <% } %>
             </div>
          </div>
      </div>
</div>