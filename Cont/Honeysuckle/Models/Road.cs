﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;

namespace Honeysuckle.Models
{
    public class Road
    {
        public int RoadID { get; set; }

        [RegularExpression("[a-zA-Z](([a-zA-Z0-9&,#. '-])*)?", ErrorMessage = "Incorrect road name format (some symbols are not allowed)")]
        public string RoadName { get; set; }

        public Road() { }
        public Road(int RoadID1, string RoadName1)
        {
            this.RoadID = RoadID1;
            this.RoadName = RoadName1;
            
        }
        public Road(string RoadName)
        {
            this.RoadName = RoadName;
        }
    }

    public class RoadModel
    {
        
        /// <summary>
        /// This function returns a list of roads in the system
        /// </summary>
        /// <returns>List of Road names (strings)</returns>
        public static List<string> GetRoadList()
        {
            List<string> result = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXECUTE GetRoadList", "read", "road", new Road());
                while (reader.Read())
                {
                    result.Add(reader["RoadName"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }
    }
}