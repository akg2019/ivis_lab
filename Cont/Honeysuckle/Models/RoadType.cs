﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;

namespace Honeysuckle.Models
{
    public class RoadType
    {
        public int RTID { get; set; }

        [RegularExpression("[ a-zA-Z ]+", ErrorMessage = "Incorrect road-type format (No numbers or symbols allowed)")]
        public string RoadTypeName { get; set; }

        public RoadType() { }

        public RoadType(string RoadTypeName)
        {
            this.RoadTypeName = RoadTypeName;
        }
    }

    public class RoadTypeModel
    {
        /// <summary>
        /// This function returns all the roadtypes in the system
        /// </summary>
        /// <returns>List of road types (strings)</returns>
        public static List<string> GetRoadType()
        {
            List<string> result = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXECUTE GetRoadTypeList", "read", "roadtype", new RoadType());
                while (reader.Read())
                {
                    result.Add(reader["RoadType"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
         }
      }
   }
