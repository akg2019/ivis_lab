﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Honeysuckle.Models
{
    public class UserPermission
    {
        public int id { get; set; }
        public string table { get; set; }
        public string action { get; set; }
        public string actiontable { get; set; }

        public UserPermission() { }
        public UserPermission(int id)
        {
            this.id = id;
        }
        /// <summary>
        /// Create new UserPermission Object
        /// </summary>
        /// <param name="table">Valid string that is a table in the database</param>
        /// <param name="action">'Create','Read','Update','Delete' are valid</param>
        public UserPermission(string table, string action)
        {
            this.table = table;
            this.action = action;
            this.actiontable = action + " " + table;
        }
        /// <summary>
        /// Create new UserPermission Object
        /// </summary>
        /// <param name="id">Valid ID integer that represents the User Permission record on the database</param>
        /// <param name="table">Valid string that is a table in the database</param>
        /// <param name="action">'Create','Read','Update','Delete' are valid</param>
        public UserPermission(int id, string table, string action)
        {
            this.id = id;
            this.table = table;
            this.action = action;
            this.actiontable = action + " " + table;
        }
    }
    
    public class UserPermissionModel
    {
        //public List<string> List;

        /// <summary>
        /// this places the correct ID of the permission in the userpermission object provided
        /// </summary>
        /// <param name="userpermission">a user permission object with an action and table</param>
        public static void GetPermissionID(UserPermission userpermission)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetPermissionID '" + Constants.CleanString(userpermission.action) + "','" + Constants.CleanString(userpermission.table) + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    userpermission.id = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function returns the IDS of man permissions
        /// </summary>
        /// <param name="userpermissions"></param>
        public static void GetPermissionID(List<UserPermission> userpermissions)
        {
            foreach (UserPermission per in userpermissions)
            {
                GetPermissionID(per);
            }
        }
        
        /// <summary>
        /// This function returns all posible permissions in the database
        /// </summary>
        /// <returns>List of UserPermissions</returns>
        public static List<UserPermission> GetAllUserPermissions(bool system = false)
        {
            sql sql = new sql();
            List<UserPermission> result = new List<UserPermission>();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetAllPermissions " + "'" + system + "'";
                SqlDataReader reader = sql.QuerySQL(query);

                while (reader.Read())
                {
                    result.Add(new UserPermission(int.Parse(reader["UPID"].ToString()), reader["Table"].ToString(), reader["Action"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function returns all the permissions currently associated to specified group
        /// </summary>
        /// <param name="usergroup">The usergroup you are attempting to query on</param>
        /// <returns>List of User Permissions associated to that group</returns>
        public static List<UserPermission> GetAllPermissionsForGroup(UserGroup usergroup, bool system = false)
        {
            List<UserPermission> result = new List<UserPermission>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GroupPermissions " + usergroup.UserGroupID + ",'" + system + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result.Add(new UserPermission(int.Parse(reader["UPID"].ToString()), reader["Table"].ToString(), reader["Action"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function returns all the permissions in the database that currently are NOT associated to the specified group
        /// </summary>
        /// <param name="usergroup">The usergroup you are attempting to query on</param>
        /// <returns>List of User Permissions associated to that group</returns>
        public static List<UserPermission> GetNotPermissionsForGroup(UserGroup usergroup, bool system = false)
        {
            List<UserPermission> result = new List<UserPermission>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC NotGroupPermissions " + usergroup.UserGroupID + ",'" + system + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result.Add(new UserPermission(int.Parse(reader["UPID"].ToString()), reader["Table"].ToString(), reader["Action"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function takes a list of defined permissions and returns the rest of available permissions in the database
        /// </summary>
        /// <param name="myuserpermissions">List of permissions you have</param>
        /// <returns>List of User Permissions you don't have</returns>
        public static List<UserPermission> GetRestOfUserPermissions(List<UserPermission> myuserpermissions, bool system = false)
        {
            List<UserPermission> restpermissions = GetAllUserPermissions(system);
            foreach (UserPermission mypermission in myuserpermissions)
            {
                for (int i = restpermissions.Count - 1; i >= 0; i--)
                {

                    if (restpermissions[i].id == mypermission.id)
                    {
                        restpermissions.RemoveAt(i);
                        break;
                    }
                }
            }
            return restpermissions;
        }

        /// <summary>
        /// This function attaches a specific userpermission to a usergroup
        /// </summary>
        /// <param name="usergroup">A UserGroup with a valid ID</param>
        /// <param name="userpermission">A UserPermission with a valid ID</param>
        public static void UpdateUserGroupPermission(UserGroup usergroup, UserPermission userpermission)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC UpdateUserGroupPermission " + usergroup.UserGroupID + "," + userpermission.id + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "usergroup", usergroup);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function attaches a series of UserPermissions to a specified UserGroup
        /// </summary>
        /// <param name="permissions">A List of UserPermissions with valid IDs</param>
        /// <param name="usergroup">A UserGroup with a valid ID</param>
        public static void AddUserPermissionsToGroup(List<UserPermission> permissions, UserGroup usergroup)
        {
            foreach (UserPermission permission in permissions)
            {
                UpdateUserGroupPermission(usergroup, permission);
            }
        }

        /// <summary>
        /// This takes a permissions that are to be removed from a UserGroup
        /// </summary>
        /// <param name="permission">Permission with valid ids</param>
        /// <param name="usergroup">UserGroup with valid id</param>
        public static void RemoveUserPermissionFromGroup(UserPermission permission, UserGroup usergroup)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC RemoveUserPermissionFromGroup " + usergroup.UserGroupID + "," + permission.id + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "usergroup", usergroup);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This takes a list of permissions that are to be removed from a UserGroup
        /// </summary>
        /// <param name="permissions">List of permissions with valid ids</param>
        /// <param name="usergroup">UserGroup with valid id</param>
        public static void RemoveUserPermissionsFromGroup(List<UserPermission> permissions, UserGroup usergroup)
        {
            foreach (UserPermission permission in permissions)
            {
                RemoveUserPermissionFromGroup(permission, usergroup);
            }
        }

        /// <summary>
        /// GetUserPermission takes a valid id for a User Permission on the database and returns all relevant data
        /// </summary>
        /// <param name="id">integer</param>
        /// <returns>User Permission object</returns>
        private static UserPermission GetUserPermission(int id)
        {
            UserPermission output = new UserPermission(id);
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetUserPermission " + id, "read", "userpermission", new UserPermission());
                while (reader.Read())
                {
                    output = new UserPermission(id, reader["table"].ToString(), reader["action"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return output;
        }

        /// <summary>
        /// This function takes a list of permission ids and gets the relevant data
        /// then adds it to the predefined list of userpermissions
        /// </summary>
        /// <param name="list">list of user permissions you want to have new data added to</param>
        /// <param name="allpermissions">list of ids of userpermissions you want to get data for and add to the list</param>
        /// <returns>returns all user permissions</returns>
        public static List<UserPermission> AddPermissionsToList(List<UserPermission> list, List<int> allpermissions)
        {
            foreach (int permission in allpermissions)
            {
                list.Add(GetUserPermission(permission));
            }
            return list;
        }

    }
}