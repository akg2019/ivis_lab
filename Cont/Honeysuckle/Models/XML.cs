﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Honeysuckle.Models
{
    public class XML
    {

        /// <summary>
        /// this function creates user groups via an xml upload
        /// </summary>
        /// <param name="xmlstring"></param>
        /// <returns></returns>
        public static List<response> UploadUserGroups(string xmlstring)
        {
            List<response> response = new List<response>();
            
            List<UserGroup> usergroups = new List<UserGroup>();
            List<string> errors = new List<string>();

            Company company = CompanyModel.GetMyCompany(Constants.user);
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xmlstring);
            XmlNodeList xnList = xmldoc.SelectNodes("/usergroups/usergroup");

            foreach (XmlNode node in xnList)
            {
                UserGroup usergroup = new UserGroup();
                usergroup.company = company;
                usergroup.UserGroupName = node["usergroup_name"].InnerText.Trim();
                usergroup.Permissions = new List<UserPermission>();

                foreach (XmlNode permission_node in node["userpermissions"])
                {
                    UserPermission permission = new UserPermission(permission_node["table"].InnerText.Trim(), permission_node["action"].InnerText.Trim());
                    UserPermissionModel.GetPermissionID(permission);
                    if (permission.id != 0) usergroup.Permissions.Add(permission);
                    else response.Add(new response("error", "The permission '" + permission.table + " " + permission.action + "' does not match up with what we have in the database. This permission was skipped in the creation of this usergroup."));
                    //else errors.Add("The permission '" + permission.table + " " + permission.action + "' does not match up with what we have in the database. This permission was skipped in the creation of this usergroup.");
                }

                if (UserGroupModel.TestUserGroupUnique(usergroup))
                {
                    UserGroupModel.CreateUserGroup(usergroup);
                    response.Add(new response("success", "The user group '" + usergroup.UserGroupName + "' was created."));
                }
                usergroup = UserGroupModel.GetUserGroup(usergroup);
                UserGroupModel.UpdatePermissions(usergroup);
            }
            return response;
        }

        /// <summary>
        /// this function creates users based on xml sent to system
        /// </summary>
        /// <param name="xmlstring"></param>
        public static List<response> UploadUsers(string xmlstring)
        {
            List<response> response = new List<response>();

            List<string> errors = new List<string>();
            List<User> users = new List<User>();

            Company company = CompanyModel.GetMyCompany(Constants.user);
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xmlstring);
            XmlNodeList xnList = xmldoc.SelectNodes("/users/user");

            foreach (XmlNode node in xnList)
            {
                bool valid = false;
                User user = new User(); 
                user.username = node["username"].InnerText.Trim();

                Regex r = new Regex("([a-zA-Z0-9.-_]+[@][a-zA-Z0-9]+([.][a-z]+)+)");
                valid = r.IsMatch(user.username);

                if (valid)
                {
                    if (!(UserModel.UserExist(user.username)))
                    {
                        user.employee = new Employee();
                        if (UserGroupModel.AmAnAdministrator(Constants.user))
                        {
                            //ONLY SYSTEM ADMIN CAN SPECIFY THE COMPANY FOR THE USERS
                            try
                            {
                                user.employee.company = new Company();
                                user.employee.company.CompanyName = node["company"]["company_name"].InnerText.Trim();
                                user.employee.company.trn = node["company"]["trn"].InnerText.Trim();
                                user.employee.company = CompanyModel.GetCompanyByTRN(user.employee.company.trn);
                                if (user.employee.company.CompanyID == 0) throw new Exception(); //go to catch
                            }
                            catch
                            {
                                valid = false;
                                response.Add(new response("error", "The user '" + user.username + "' will not be added to the company '" + user.employee.company.CompanyName + "'  because it does not exist in the system. Please use a different company or create a company with that name."));
                            }
                        }
                        else user.employee.company = company; //EVERYONE ELSE CAN ONLY CREATE USERS OF THEIR COMPANY
                        
                        if (valid) 
                        {
                            
                            user.employee.person = new Person();
                            user.employee.person.fname = node["first_name"].InnerText.Trim();
                            user.employee.person.mname = node["middle_name"].InnerText.Trim();
                            user.employee.person.lname = node["last_name"].InnerText.Trim();
                            user.employee.person.TRN = node["trn"].InnerText.Trim();

                            if (!(PersonModel.TestTRNAvailable(user.employee.person.TRN)))
                            {
                                user.employee.person.address = new Address(node["address"]["road_number"].InnerText.Trim(),
                                                                            node["address"]["road_name"].InnerText.Trim(),
                                                                            node["address"]["road_type"].InnerText.Trim(),
                                                                            null,  //zip code
                                                                            node["address"]["city"].InnerText.Trim(),
                                                                            node["address"]["parish"].InnerText.Trim(),
                                                                            node["address"]["country"].InnerText.Trim());

                                user.password = UserModel.PasswordGenerator(); //generate password
                                UserModel.AddUser(user); //create user
                                UserModel.WelcomeEmail(user, System.Web.HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + System.Web.HttpContext.Current.Request.Url.Host + (System.Web.HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + System.Web.HttpContext.Current.Request.Url.Port)); //send email to new user that was created

                                response.Add(new response("success", "The user '" + user.username + "' was created."));

                                users.Add(user);

                                //adding user groups to users
                                foreach (XmlNode group_node in node["usergroups"])
                                {
                                    string ug_name = group_node.InnerText.Trim();
                                    UserGroup usergroup = UserGroupModel.GetUserGroup(new UserGroup(ug_name), user.employee.company);
                                    if (usergroup.UserGroupID != 0)
                                    {
                                        UserModel.AddUserToUserGroup(user, usergroup);
                                        response.Add(new response("success", "The user '" + user.username + "' has been assigned to the group '" + usergroup.UserGroupName + "' successfully."));
                                    }
                                    else response.Add(new response("error", "The user group '" + ug_name + "' does not exist in the system. Please use a different user group or create a user group with that name."));
                                    //else errors.Add("The user group '" + ug_name + "' does not exist in the system. Please use a different user group or create a user group with that name.");
                                }
                            }
                        }
                        else response.Add(new response("error", "The TRN '" + user.employee.person.TRN + "' already exists in the system. Please use a different TRN for that user."));
                        //else errors.Add("The TRN '" + user.employee.person.TRN + "' already exists in the system. Please use a different TRN for that user.");
                    }
                    else response.Add(new response("error", "The user '" + user.username + "' already exists in the system. Please use a different username for that user."));
                    //else errors.Add("The user '" + user.username + "' already exists in the system. Please use a different username for that user.");
                }
                else response.Add(new response("error", "The user '" + user.username + "' is not an email. All user names must be emails. Please enter an email for the username and try again."));
                //else errors.Add("The user '" + user.username + "' is not an email. All user names must be emails. Please enter an email for the username and try again.");
            }

            return response;
        }
        

    }
    
}

