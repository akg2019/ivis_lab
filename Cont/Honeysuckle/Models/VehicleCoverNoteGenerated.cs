﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class VehicleCoverNoteGenerated
    {
        public int ID { get; set; }
        public string vehyear { get; set; }
        public string vehmake { get; set; }
        public string vehregno { get; set; }
        public string vehExt { get; set; }
        public string PolicyHolders { get; set; }
        public string drivers { get; set; }
        public string vehdesc { get; set; }
        public string cover { get; set; }
        public string vehmodeltype { get; set; }
        public string vehmodel { get; set; }
        public string authorizedwording { get; set; }
        public string mortgagees { get; set; }
        public string limitsofuse { get; set; }
        public string Usage { get; set; }
        public string AuthorizedWording { get; set; }

        public string PolicyNo { get; set; }
        public string PolicyHoldersAddress { get; set; }
        public string CovernoteNo { get; set; }
        public string Period { get; set; }
        public string effectiveTime { get; set; }
        public string expiryTime { get; set; }
        public string effectivedate { get; set; }
        public string expirydate { get; set; }
        public string endorsementNo { get; set; }
        public string bodyType { get; set; }
        public string seating { get; set; }
        public string chassisNo { get; set; }
        public string certificateType { get; set; }
        public string certitificateNo { get; set; }
        public string certificateCode { get; set; }
        public string engineNo { get; set; }
        public string VIN { get; set; }
        public string HPCC { get; set; }
        public string companyCreatedBy { get; set; }
        public string referenceNo { get; set; }
        public bool cancelled { get; set; }
        public string estimatedValue { get; set; }

        public string HeaderImgLocation { get; set; }
        public string FooterImgLocation { get; set; }
        public string LogoLocation { get; set; }


        public VehicleCoverNoteGenerated() { }
        public VehicleCoverNoteGenerated(int ID) 
        {
            this.ID = ID;
        }
        public VehicleCoverNoteGenerated(int ID, string authorizedwording, string mortgagees, string limitsofuse)
        {
            this.ID = ID;
            this.authorizedwording = authorizedwording;
            this.mortgagees = mortgagees;
            this.limitsofuse = limitsofuse;
        }
        public VehicleCoverNoteGenerated(int ID, string vehyear, string vehmake, string vehregno, string vehext, string maininsured,
                                         string drivers, string vehdesc, string cover, string vehmodeltype, string vehmodel,
                                         string authorizedwording, string mortgagees, string limitsofuse, string Usage, string PolicyNo, string PolicyHoldersAddress, 
                                         string CovernoteNo, string Period, string effectiveTime, string expiryTime, string effectivedate , string expirydate ,
                                         string endorsementNo, string bodyType, string seating, string chassisNo, string certificateType, string certitificateNo,
                                         string certCode, string engineNo, string HPCC)

                                                         
        {
            this.ID = ID;
            this.vehyear = vehyear;
            this.vehmake = vehmake;
            this.vehregno = vehregno;
            this.vehExt = vehext;
            this.PolicyHolders = maininsured;
            this.drivers = drivers;
            this.vehdesc = vehdesc;
            this.cover = cover;
            this.vehmodel = vehmodel;
            this.vehmodeltype = vehmodeltype;
            this.authorizedwording = authorizedwording;
            this.mortgagees = mortgagees;
            this.limitsofuse = limitsofuse;
            this.Usage = Usage;
            this.PolicyNo = PolicyNo;
            this.PolicyHoldersAddress = PolicyHoldersAddress;
            this.CovernoteNo = CovernoteNo;
            this.Period = Period;
            this.effectiveTime = effectiveTime;
            this.expiryTime = expiryTime;
            this.effectivedate = effectivedate;
            this.expirydate = expirydate;
            this.endorsementNo = endorsementNo;
            this.bodyType = bodyType;
            this.seating = seating;
            this.chassisNo = chassisNo;
            this.certificateType = certificateType;
            this.certitificateNo = certitificateNo;
            this.certificateCode = certCode;
            this.engineNo = engineNo;
            this.HPCC = HPCC;

        }

        public VehicleCoverNoteGenerated(int ID, string vehyear, string vehmake, string vehregno, string vehext, string maininsured,
                                        string drivers, string vehdesc, string cover, string vehmodeltype, string vehmodel,
                                        string authorizedwording, string mortgagees, string limitsofuse, string Usage, string PolicyNo, string PolicyHoldersAddress,
                                        string CovernoteNo, string Period, string effectiveTime, string expiryTime, string effectivedate, string expirydate,
                                        string endorsementNo, string bodyType, string seating, string chassisNo, string certificateType, string certitificateNo,
                                        string certCode, string engineNo, string compCreatedBy, string HPCC)
        {
            this.ID = ID;
            this.vehyear = vehyear;
            this.vehmake = vehmake;
            this.vehregno = vehregno;
            this.vehExt = vehext;
            this.PolicyHolders = maininsured;
            this.drivers = drivers;
            this.vehdesc = vehdesc;
            this.cover = cover;
            this.vehmodel = vehmodel;
            this.vehmodeltype = vehmodeltype;
            this.authorizedwording = authorizedwording;
            this.mortgagees = mortgagees;
            this.limitsofuse = limitsofuse;
            this.Usage = Usage;
            this.PolicyNo = PolicyNo;
            this.PolicyHoldersAddress = PolicyHoldersAddress;
            this.CovernoteNo = CovernoteNo;
            this.Period = Period;
            this.effectiveTime = effectiveTime;
            this.expiryTime = expiryTime;
            this.effectivedate = effectivedate;
            this.expirydate = expirydate;
            this.endorsementNo = endorsementNo;
            this.bodyType = bodyType;
            this.seating = seating;
            this.chassisNo = chassisNo;
            this.certificateType = certificateType;
            this.certitificateNo = certitificateNo;
            this.certificateCode = certCode;
            this.engineNo = engineNo;
            this.companyCreatedBy = compCreatedBy;
            this.engineNo = engineNo;
            this.HPCC = HPCC;

        }


        public VehicleCoverNoteGenerated(int ID, string vehyear, string vehmake, string vehregno, string vehext, string maininsured,
                                        string drivers, string vehdesc, string cover, string vehmodeltype, string vehmodel,
                                        string authorizedwording, string mortgagees, string limitsofuse)
        {
            this.ID = ID;
            this.vehyear = vehyear;
            this.vehmake = vehmake;
            this.vehregno = vehregno;
            this.vehExt = vehext;
            this.PolicyHolders = maininsured;
            this.drivers = drivers;
            this.vehdesc = vehdesc;
            this.cover = cover;
            this.vehmodel = vehmodel;
            this.vehmodeltype = vehmodeltype;
            this.authorizedwording = authorizedwording;
            this.mortgagees = mortgagees;
            this.limitsofuse = limitsofuse;
        }

        //For the import of certificates
         public VehicleCoverNoteGenerated(string vehyear, string vehmake, string vehregno, string vehext, string maininsured,
                                         string drivers, string vehdesc, string cover, string vehmodeltype, string vehmodel,
                                         string authorizedwording, string mortgagees, string limitsofuse, string Usage, string PolicyNo, string PolicyHoldersAddress, 
                                         string CovernoteNo, string Period, string effectiveTime, string expiryTime, string effectivedate , string expirydate ,
                                         string endorsementNo, string bodyType, string seating, string chassisNo, string certificateType, string certitificateNo,
                                         string certCode, string engineNo, string HPCC)
          {
            this.ID = ID;
            this.vehyear = vehyear;
            this.vehmake = vehmake;
            this.vehregno = vehregno;
            this.vehExt = vehext;
            this.PolicyHolders = maininsured;
            this.drivers = drivers;
            this.vehdesc = vehdesc;
            this.cover = cover;
            this.vehmodel = vehmodel;
            this.vehmodeltype = vehmodeltype;
            this.authorizedwording = authorizedwording;
            this.mortgagees = mortgagees;
            this.limitsofuse = limitsofuse;
            this.Usage = Usage;
            this.PolicyNo = PolicyNo;
            this.PolicyHoldersAddress = PolicyHoldersAddress;
            this.CovernoteNo = CovernoteNo;
            this.Period = Period;
            this.effectiveTime = effectiveTime;
            this.expiryTime = expiryTime;
            this.effectivedate = effectivedate;
            this.expirydate = expirydate;
            this.endorsementNo = endorsementNo;
            this.bodyType = bodyType;
            this.seating = seating;
            this.chassisNo = chassisNo;
            this.certificateType = certificateType;
            this.certitificateNo = certitificateNo;
            this.certificateCode = certCode;
            this.engineNo = engineNo;
            this.HPCC = HPCC;

        }

    }
    public class VehicleCoverNoteGeneratedModel
    {
        /// <summary>
        /// this function retrieves the cover note id based on the generated id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private static VehicleCoverNote get_cover_note_from_gen_id(int id)
        {
            VehicleCoverNote vcn = new VehicleCoverNote();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC get_cover_note_from_gen_id " + id);
                while (reader.Read())
                {
                    vcn.ID = int.Parse(reader["id"].ToString());
                    vcn.Policy = new Policy();
                    vcn.Policy.ID = int.Parse(reader["policy_id"].ToString());
                    vcn.Policy.insuredby = new Company();
                    vcn.Policy.insuredby.CompanyID = int.Parse(reader["comp_id"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return vcn;
        }
        
        /// <summary>
        /// this functions retrieves generated cover note data
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static VehicleCoverNoteGenerated GetGenCoverNote(int ID)
        {
            VehicleCoverNoteGenerated vcng = new VehicleCoverNoteGenerated(ID);
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetGenCoverNote " + ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernotegenerated", new VehicleCoverNoteGenerated());
                while (reader.Read())
                {
                    vcng = new VehicleCoverNoteGenerated(ID, reader["vehyear"].ToString(),
                                                         reader["vehmake"].ToString(), reader["vehregno"].ToString(),
                                                         reader["vehext"].ToString(), reader["maininsured"].ToString(),
                                                         reader["drivers"].ToString(), reader["vehdesc"].ToString(),
                                                         reader["cover"].ToString(), reader["vehmodeltype"].ToString(),
                                                         reader["vehmodel"].ToString(), reader["authorized"].ToString(),
                                                         reader["mortgagees"].ToString(), reader["limitsofuse"].ToString(),
                                                         reader["Usage"].ToString(), reader["PolicyNo"].ToString(),
                                                         reader["PolicyHolderAddress"].ToString(), reader["CovernoteNo"].ToString(),
                                                         reader["Period"].ToString(), reader["effectiveTime"].ToString(),
                                                         reader["expiryTime"].ToString(), reader["effectivedate"].ToString(),
                                                         reader["expirydate"].ToString(), reader["endorsementNo"].ToString(),
                                                         reader["bodyType"].ToString(), reader["seating"].ToString(),
                                                         reader["chassisNo"].ToString(), reader["certificateType"].ToString(),
                                                         reader["certitificateNo"].ToString(), reader["CertificateCode"].ToString(),
                                                         reader["engineNo"].ToString(),
                                                         reader["companyCreatedBy"].ToString(),reader["HPCC"].ToString());
                    VehicleCoverNote vcn = get_cover_note_from_gen_id(ID);
                    vcng.HeaderImgLocation = TemplateImageModel.get_image_location(vcn.Policy.insuredby, "header", "cover note");
                    vcng.FooterImgLocation = TemplateImageModel.get_image_location(vcn.Policy.insuredby, "footer", "cover note");
                    vcng.LogoLocation = TemplateImageModel.get_image_location(vcn.Policy.insuredby, "logo", "cover note");
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return vcng;
        }


        /// <summary>
        /// this function retrieves all generated certificate data
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static VehicleCoverNoteGenerated GetGenCertificate(int ID)
        {
            VehicleCoverNoteGenerated vcng = new VehicleCoverNoteGenerated(ID);
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetThisGeneratedCert " + ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernotegenerated", new VehicleCoverNoteGenerated());
                while (reader.Read())
                {
                    vcng = new VehicleCoverNoteGenerated(ID, reader["vehyear"].ToString(),
                                                         reader["vehmake"].ToString(), reader["vehregno"].ToString(),
                                                         reader["vehext"].ToString(), reader["maininsured"].ToString(),
                                                         reader["drivers"].ToString(), reader["vehdesc"].ToString(),
                                                         reader["cover"].ToString(), reader["vehmodeltype"].ToString(),
                                                         reader["vehmodel"].ToString(), reader["authorized"].ToString(),
                                                         reader["mortgagees"].ToString(), reader["limitsofuse"].ToString(),
                                                         reader["Usage"].ToString(), reader["PolicyNo"].ToString(),
                                                         reader["PolicyHolderAddress"].ToString(), reader["CovernoteNo"].ToString(),
                                                         reader["Period"].ToString(), reader["effectiveTime"].ToString(),
                                                         reader["expiryTime"].ToString(), reader["effectivedate"].ToString(),
                                                         reader["expirydate"].ToString(), reader["endorsementNo"].ToString(),
                                                         reader["bodyType"].ToString(), reader["seating"].ToString(),
                                                         reader["chassisNo"].ToString(), reader["certificateType"].ToString(),
                                                         reader["certitificateNo"].ToString(), reader["CertificateCode"].ToString(),
                                                         reader["engineNo"].ToString(),
                                                         reader["HPCC"].ToString());
                    
                    int ins_comp_id = int.Parse(reader["ins_comp_id"].ToString());
                    vcng.HeaderImgLocation = TemplateImageModel.get_image_location(new Company(ins_comp_id), "header", "certificate");
                    vcng.FooterImgLocation = TemplateImageModel.get_image_location(new Company(ins_comp_id), "footer", "certificate");
                    vcng.LogoLocation = TemplateImageModel.get_image_location(new Company(ins_comp_id), "logo", "certificate");
                }
                reader.Close();
                sql.DisconnectSQL();
            }

            return vcng;
        }

        /// <summary>
        /// this function tests if a cover note/certificate has been generated or not
        /// </summary>
        /// <param name="CoverNoteID"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsGenerated(int CoverNoteID, string type)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "IsGenerated " + CoverNoteID + ",'" + Constants.CleanString(type) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernote", new VehicleCoverNote());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function inserts generated data into the IVIS database for cover notes
        /// </summary>
        /// <param name="vcng"></param>
        /// <param name="vcnid"></param>
        public static void InsertGenCoverNote(VehicleCoverNoteGenerated vcng, int vcnid)
        {
            InsertGenCoverNote(vcng, vcnid, Constants.user.ID);
        }

        /// <summary>
        /// this function inserts generated data into the IVIS database for cover notes
        /// </summary>
        /// <param name="vcng"></param>
        /// <param name="vcnid"></param>
        /// <param name="uid"></param>
        public static void InsertGenCoverNote(VehicleCoverNoteGenerated vcng, int vcnid, int uid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                String query = "EXEC InsertGenCoverNote '" + Constants.CleanString(vcng.vehyear) + "','"
                                                           + Constants.CleanString(vcng.vehmake) + "','"
                                                           + Constants.CleanString(vcng.vehregno) + "','"
                                                           + Constants.CleanString(vcng.vehExt) + "','"
                                                           + Constants.CleanString(vcng.PolicyHolders) + "','"
                                                           + Constants.CleanString(vcng.drivers) + "','"
                                                           + Constants.CleanString(vcng.vehdesc) + "','"
                                                           + Constants.CleanString(vcng.cover) + "','"
                                                           + Constants.CleanString(vcng.vehmodeltype) + "','"
                                                           + Constants.CleanString(vcng.vehmodel) + "','"
                                                           + Constants.CleanString(vcng.authorizedwording) + "','"
                                                           + Constants.CleanString(vcng.mortgagees) + "','"
                                                           + Constants.CleanString(vcng.limitsofuse) + "','"
                                                           + Constants.CleanString(vcng.Usage) + "','"
                                                           + Constants.CleanString(vcng.PolicyNo) + "','"
                                                           + Constants.CleanString(vcng.PolicyHoldersAddress) + "','"
                                                           + Constants.CleanString(vcng.CovernoteNo) + "','"
                                                           + Constants.CleanString(vcng.Period) + "','"
                                                           + Constants.CleanString(vcng.effectiveTime) + "','" 
                                                           + Constants.CleanString(vcng.expiryTime) + "','"
                                                           + Constants.CleanString(vcng.effectivedate) + "','"
                                                           + Constants.CleanString(vcng.expirydate) + "','"
                                                           + Constants.CleanString(vcng.endorsementNo) + "','"
                                                           + Constants.CleanString(vcng.bodyType) + "','"
                                                           + Constants.CleanString(vcng.seating) + "','"
                                                           + Constants.CleanString(vcng.chassisNo) + "','"
                                                           + Constants.CleanString(vcng.certificateType) + "','"
                                                           + Constants.CleanString(vcng.certitificateNo) + "','"
                                                           + Constants.CleanString(vcng.engineNo) + "','"
                                                           + Constants.CleanString(vcng.companyCreatedBy) + "','"
                                                           + Constants.CleanString(vcng.certificateCode) + "','"
                                                           + Constants.CleanString(vcng.HPCC) + "',"
                                                           + vcnid + ","
                                                           + uid;
                SqlDataReader reader = sql.QuerySQL(query, "create", "vehiclecovernotegenerated", vcng);
                reader.Close();
                sql.DisconnectSQL();
            }
        }


        /// <summary>
        /// this function inserts generated data into the IVIS database for certificates
        /// </summary>
        /// <param name="certificate">Generated Certidficate</param>
        public static void GenerateVehicleCert(VehicleCoverNoteGenerated certificate, int certId)
        {
            GenerateVehicleCert(certificate, certId, Constants.user.ID);
        }
        
        /// <summary>
        /// this function inserts generated data into the IVIS database for certificates
        /// </summary>
        /// <param name="certificate"></param>
        /// <param name="certId"></param>
        /// <param name="uid"></param>
        public static void GenerateVehicleCert(VehicleCoverNoteGenerated certificate, int certId, int uid)
        {

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GenerateVehicleCertificate  '" + Constants.CleanString(certificate.vehyear) + "','"
                                                                    + Constants.CleanString(certificate.vehmake) + "','"
                                                                    + Constants.CleanString(certificate.vehregno) + "','"
                                                                    + Constants.CleanString(certificate.vehExt) + "','"
                                                                    + Constants.CleanString(certificate.PolicyHolders) + "','"
                                                                    + Constants.CleanString(certificate.drivers) + "','"
                                                                    + Constants.CleanString(certificate.vehdesc) + "','"
                                                                    + Constants.CleanString(certificate.cover) + "','"
                                                                    + Constants.CleanString(certificate.vehmodeltype) + "','"
                                                                    + Constants.CleanString(certificate.vehmodel) + "','"
                                                                    + Constants.CleanString(certificate.authorizedwording) + "','"
                                                                    + Constants.CleanString(certificate.mortgagees) + "','"
                                                                    + Constants.CleanString(certificate.limitsofuse) + "','"
                                                                    + Constants.CleanString(certificate.Usage) + "','"
                                                                    + Constants.CleanString(certificate.PolicyNo) + "','"
                                                                    + Constants.CleanString(certificate.PolicyHoldersAddress) + "','"
                                                                    + Constants.CleanString(certificate.CovernoteNo) + "','"
                                                                    + Constants.CleanString(certificate.Period) + "','"
                                                                    + Constants.CleanString(certificate.effectiveTime) + "','"
                                                                    + Constants.CleanString(certificate.expiryTime) + "','"
                                                                    + Constants.CleanString(certificate.effectivedate) + "','"
                                                                    + Constants.CleanString(certificate.expirydate) + "','"
                                                                    + Constants.CleanString(certificate.endorsementNo) + "','"
                                                                    + Constants.CleanString(certificate.bodyType) + "','"
                                                                    + Constants.CleanString(certificate.seating) + "','"
                                                                    + Constants.CleanString(certificate.chassisNo) + "','"
                                                                    + Constants.CleanString(certificate.certificateType) + "','"
                                                                    + Constants.CleanString(certificate.certitificateNo) + "','"
                                                                    + Constants.CleanString(certificate.certificateCode) + "','"
                                                                    + Constants.CleanString(certificate.engineNo) + "','"
                                                                    + Constants.CleanString(certificate.HPCC) + "',"
                                                                    + certId + ","
                                                                    + uid;

                SqlDataReader reader = sql.QuerySQL(query, "create", "vehiclecovernotegenerated", certificate);
                reader.Close();
                sql.DisconnectSQL();
            }
         }

    }
}
