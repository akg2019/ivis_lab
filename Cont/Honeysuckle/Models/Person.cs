﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel.DataAnnotations;

namespace Honeysuckle.Models
{
    public class Person
    {
        public int PersonID { get; set; }
        public int base_id { get; set; }
        public DateTime DOB { get; set; }
        public string EDIID { get; set; }
        //used for special import cases, where TRN is not used
        public string ID { get; set; }

        public bool imported { get; set; }

        [RegularExpression("(([a-zA-Z])[a-zA-Z-'. ]*)", ErrorMessage = "Incorrect first-name format (No numbers allowed)")]
        public string fname { get; set; }

        [RegularExpression("(([a-zA-Z])[a-zA-Z-'. ]*)", ErrorMessage = "Incorrect middle-name format (No numbers allowed)")]
        public string mname { get; set; }

        [RegularExpression("(([a-zA-Z])[a-zA-Z-'. ]*)", ErrorMessage = "Incorrect surname format (No numbers allowed)")]
        public string lname { get; set; }
        public Address address { get; set; }
        public List<Email> emails { get; set; }
        public List<Phone> phoneNums { get; set; }

        [RegularExpression("([1][0-9]{8})", ErrorMessage = "TRN Must be 9 digits and start with a 1")]
        public string TRN { get; set; }

        public Person() { }
        public Person(Address address) { this.address = address; }
      
        public Person(int PersonID)
        {
            this.PersonID = PersonID;
        }
        public Person(int PersonID, string fname, string mname, string lname, Address address)
        {
            this.PersonID = PersonID;
            this.fname = fname;
            this.mname = mname;
            this.lname = lname;
            this.address = address;
        }
        public Person(int PersonID, string fname, string lname)
        {
            this.PersonID = PersonID;
            this.fname = fname;
            this.lname = lname;
        }
        public Person(string fname, string lname, string TRN)
        {
            this.fname = fname; 
            this.lname = lname;
            this.TRN = TRN;
            
        }

        public Person(int PersonID, string fname, string mname, string lname, Address address, List<Email> emails)
        {
            this.PersonID = PersonID;
            this.fname = fname;
            this.mname = mname;
            this.lname = lname;
            this.address = address;
            this.emails = emails;
        }
        public Person(int PersonID, string fname, string mname, string lname, Address address, List<Email> emails, string TRN)
        {
            this.PersonID = PersonID;
            this.fname = fname;
            this.mname = mname;
            this.lname = lname;
            this.address = address;
            this.emails = emails;
            this.TRN = TRN;
        }

        public Person(int PersonID, string fname, string lname, string TRN)
        {
            this.PersonID = PersonID;
            this.fname = fname;
            this.lname = lname;
            this.TRN = TRN;
        }
        public Person(string TRN)
        {
            this.TRN = TRN;
        }
        public Person(List<Email> emails)
        {
            this.emails = emails;
        }
        public Person(string fname, string lname, List<Email> emails)
        {
            this.fname = fname;
            this.lname = lname;
            this.emails = emails;
        }

        public static List<int> toInt(List<Person> people)
        {
            List<int> result = new List<int>();
            foreach (Person person in people)
            {
                result.Add(person.PersonID);
            }
            return result;
        }
    }

    public class PersonModel
    {

        /// <summary>
        /// this function lets the user know if this is a person's information that they are able to view
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        public static bool CanISeeThisPerson(Person person)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC CanISeeThisPerson " + person.PersonID + "," + Constants.user.ID, "read", "person", person);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function retrieves the current address of the person spitulated
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        public static Address GetPersonAddress(Person person)
        {
            Address address = new Address();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetPersonAddress " + person.PersonID, "read", "address", address);
                while (reader.Read())
                {

                    address.ID = int.Parse(reader["AID"].ToString());
                    if (DBNull.Value != reader["City"]) address.city = new City(reader["City"].ToString());
                    if (DBNull.Value != reader["RoadNumber"]) address.roadnumber = reader["RoadNumber"].ToString();
                    if (DBNull.Value != reader["RoadName"]) address.road = new Road(reader["RoadName"].ToString());
                    if (DBNull.Value != reader["CountryName"]) address.country = new Country(reader["CountryName"].ToString());
                    if (DBNull.Value != reader["PID"]) address.parish =new Parish(int.Parse(reader["PID"].ToString()), reader["Parish"].ToString());
                    if (DBNull.Value != reader["ZipCode"]) address.zipcode = new ZipCode(reader["ZipCode"].ToString());
                    if (DBNull.Value != reader["AptNumber"]) address.ApartmentNumber = reader["AptNumber"].ToString();
                    if (DBNull.Value != reader["RoadType"]) address.roadtype = new RoadType(reader["RoadType"].ToString());
                    if (DBNull.Value != reader["LongAddressUsed"]) address.longAddressUsed = (bool)reader["LongAddressUsed"];
                    if (DBNull.Value != reader["LongAddress"]) address.longAddress = reader["LongAddress"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return address;
        }
        
        /// <summary>
        /// this function returns all the people that this company has insured in the past and present
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public static List<Person> GetAllPeopleIveInsured(Company company)
        {
            List<Person> people = new List<Person>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetAllPeopleIveInsured " + company.CompanyID, "read", "person", company);
                while (reader.Read())
                {
                    people.Add(new Person(int.Parse(reader["ID"].ToString()), reader["fname"].ToString(), reader["lname"].ToString(), reader["trn"].ToString()));
                }
                sql.DisconnectSQL();
            }
            return people;
        }
                
        /// <summary>
        /// This function retrieves a person's information, given the person Id
        /// </summary>
        /// <param name="id">Person Id</param>
        /// <returns>Person object with person's information</returns>
        public static Person GetPerson(int id, int cmp = 0, bool PolicyHolder = false)
        {
            Person person = new Person();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query =  "EXEC GetPerson " + id + "," + cmp + "," + PolicyHolder;
                
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                   if (DBNull.Value != reader["ID"]) person.PersonID = int.Parse(reader["ID"].ToString());
                   if (DBNull.Value != reader["fname"]) person.fname = reader["fname"].ToString();
                   if (DBNull.Value != reader["mname"])  person.mname = reader["mname"].ToString();
                   if (DBNull.Value != reader["lname"])  person.lname = reader["lname"].ToString();
                   if (DBNull.Value != reader["AID"])  person.address = new Address(int.Parse(reader["AID"].ToString()));
                   if (DBNull.Value != reader["TRN"]) person.TRN = reader["TRN"].ToString();
                   if (DBNull.Value != reader["base_id"]) person.base_id = int.Parse(reader["base_id"].ToString());
                   if (DBNull.Value != reader["ImportID"]) person.ID = reader["ImportID"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return person;
        }

        /// <summary>
        /// This function adds a person to the database.
        /// This version of the function is for the web interface, with the user logged in already.
        /// </summary>
        /// <param name="person">Personal data being added to the database</param>
        /// <param name="policyHolder">this flag let's the database know this is for a policyholder</param>
        public static void AddPerson(Person person, bool policyHolder = false, bool importing = false)
        {
            AddPerson(person, Constants.user.ID, policyHolder,importing);
        }

        /// <summary>
        /// This function adds a new person to the system
        /// </summary>
        /// <param name="person">Person object with person's information</param>
        public static void AddPerson(Person person, int uid, bool policyHolder = false, bool importing = false)
        {
            //add emails belonging to the person to list
            sql sql = new sql();
            if (person.address == null) person.address = new Address();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE AddPerson '" + Constants.CleanString(person.fname)
                                                     + "','" + Constants.CleanString(person.mname)
                                                     + "', '" + Constants.CleanString(person.lname)
                                                     + "'," + person.address.ID
                                                     + ",'" + person.TRN
                                                     + "'," + uid
                                                     + ","  + policyHolder;

                if (person.DOB != null && person.DOB != new DateTime())
                {
                    query += ",'" + person.DOB.ToString("yyyy-MM-dd") + "'"; // add date of birth if sent in model
                    if (person.ID != null && person.ID != "") query += ",'" + Constants.CleanString(person.ID) + "'";
                }
                else
                {
                    if (person.ID != null && person.ID != "") query += ",'" + null + "','" + Constants.CleanString(person.ID) + "'";
                }
                

                SqlDataReader reader = sql.QuerySQL(query, "create", "person", person, uid);

                while (reader.Read())
                {
                    person.PersonID = int.Parse(reader["ID"].ToString()); 
                }
                reader.Close();
                sql.DisconnectSQL();
                if (person.emails != null) EmailModel.AddEmailtoPerson(person, uid, policyHolder);
                if (importing)
                {
                    if (policyHolder) MarkPeopleImported(person, true);
                    else MarkPeopleImported(person, false);
                }
            }
        }

        /// <summary>
        /// This function edits an existing person's information in the system
        /// </summary>
        /// <param name="person">Person object with person's information</param>
        public static void ModifyPerson(Person person)
        {
            person.address.ID = AddressModel.AddAddress(person.address);

            //add person to database 
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE ModifyPerson "  + person.PersonID + ",'"
                                                        + Constants.CleanString(person.fname) + "','"
                                                        + Constants.CleanString(person.mname) + "','"
                                                        + Constants.CleanString(person.lname) + "',"
                                                        + person.address.ID + ",'"
                                                        + Constants.CleanString(person.TRN) + "',"
                                                        + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "person", person);
                reader.Close();
                sql.DisconnectSQL();
                EmailModel.UpdateEmails(person); 
                
            }
        }

        /// <summary>
        /// This function tests if a TRN is currently in the system
        /// </summary>
        /// <param name="person">Person object with person's TRN</param>
        /// <returns>User -Person's information (in a user object)</returns>
        public static User TestPersonWithTRN(Person person, bool insured)     
        
        {
            User user = new User();

            Address add = new Address();
            Road road = new Road();
            RoadType road1 = new RoadType();
            ZipCode zip = new ZipCode();
            City city = new City();
            Country country = new Country();
            Parish parish = new Parish();



            try
            { 
            
             if (person != null)
            {            

                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    string query = "EXEC GetPersonWithTRN '" + Constants.CleanString(person.TRN) + "'," + insured;
                    SqlDataReader reader = sql.QuerySQL(query, "read", "person", person);
                    while (reader.Read())
                    {
                        if ((bool)reader["result"])
                        {
                            //THERE IS A RESULT
                            if ((bool)reader["user"])
                            {
                                //THE TRN IS ASSOCIATED TO A USER
                                user.ID = int.Parse(reader["UID"].ToString()); 
                                person.PersonID = int.Parse(reader["PeopleID"].ToString());
                                person.fname = reader["fname"].ToString();
                                person.mname = reader["mname"].ToString();
                                person.lname = reader["lname"].ToString();

                                if (DBNull.Value != reader["RoadNumber"]) add.roadnumber = reader["RoadNumber"].ToString();
                                if (DBNull.Value != reader["AptNumber"]) add.ApartmentNumber = reader["AptNumber"].ToString();

                                if (DBNull.Value != reader["RoadName"]) road.RoadName = reader["RoadName"].ToString();
                                else road.RoadName = "";

                                if (DBNull.Value != reader["RoadType"]) road1.RoadTypeName = reader["RoadType"].ToString();

                                if (DBNull.Value != reader["Zipcode"]) zip.ZipCodeName = reader["Zipcode"].ToString();
                                if (DBNull.Value != reader["City"]) city.CityName = reader["City"].ToString();

                                if (DBNull.Value != reader["CountryName"]) country.CountryName = reader["CountryName"].ToString();

                                if (DBNull.Value != reader["PID"])
                                {
                                    parish.ID = int.Parse(reader["PID"].ToString());
                                    parish.parish = reader["parish"].ToString();
                                }

                                add.road = road;
                                add.roadtype = road1;
                                add.zipcode = zip;
                                add.city = city;
                                add.country = country;
                                add.parish = parish;

                                person.address = add;
                                Employee employee = new Employee();
                                employee.company = new Company(int.Parse(reader["CompanyID"].ToString()));
                                employee.person = person;
                                user.employee = employee;
                            }
                            else
                            {
                                if ((bool)reader["person"])
                                {
                                    //THE TRN BELONGS TO A PERSON THAT IS NOT A USER
                                    person.PersonID = int.Parse(reader["PeopleID"].ToString());
                                    person.fname = reader["fname"].ToString();
                                    person.mname = reader["mname"].ToString();
                                    person.lname = reader["lname"].ToString();

                                    if (DBNull.Value != reader["RoadNumber"]) add.roadnumber = reader["RoadNumber"].ToString();
                                    if (DBNull.Value != reader["AptNumber"]) add.ApartmentNumber = reader["AptNumber"].ToString();

                                    if (DBNull.Value != reader["RoadName"]) road.RoadName = reader["RoadName"].ToString();
                                    if (DBNull.Value != reader["RoadType"]) road1.RoadTypeName = reader["RoadType"].ToString();

                                    if (DBNull.Value != reader["Zipcode"]) zip.ZipCodeName = reader["Zipcode"].ToString();
                                    if (DBNull.Value != reader["City"]) city.CityName = reader["City"].ToString();
                                    if (DBNull.Value != reader["CountryName"]) country.CountryName = reader["CountryName"].ToString();

                                    if (DBNull.Value != reader["PID"])
                                    {
                                        parish.ID = int.Parse(reader["PID"].ToString());
                                        parish.parish = reader["parish"].ToString();
                                    }

                                    add.road = road;
                                    add.roadtype = road1;
                                    add.zipcode = zip;
                                    add.city = city;
                                    add.country = country;
                                    add.parish = parish;

                                    person.address = add;
                                    Employee employee = new Employee();
                                    employee.person = person;
                                    user.employee = employee;
                               
                                }
                                else
                                {
                                    if ((bool)reader["emp"])
                                    {
                                        //THE TRN IS ASSOCIATED TO AN EMPLOYEE THAT ISN'T A USER
                                        person.PersonID = int.Parse(reader["PeopleID"].ToString());
                                        person.fname = reader["fname"].ToString();
                                        person.mname = reader["mname"].ToString();
                                        person.lname = reader["lname"].ToString();

                                        if (DBNull.Value != reader["RoadNumber"]) add.roadnumber = reader["RoadNumber"].ToString();
                                        if (DBNull.Value != reader["AptNumber"]) add.ApartmentNumber = reader["AptNumber"].ToString();

                                        if (DBNull.Value != reader["RoadName"]) road.RoadName = reader["RoadName"].ToString();
                                        if (DBNull.Value != reader["RoadType"]) road1.RoadTypeName = reader["RoadType"].ToString();

                                        if (DBNull.Value != reader["Zipcode"]) zip.ZipCodeName = reader["Zipcode"].ToString();
                                        if (DBNull.Value != reader["City"]) city.CityName = reader["City"].ToString();
                                        if (DBNull.Value != reader["CountryName"]) country.CountryName = reader["CountryName"].ToString();

                                        if (DBNull.Value != reader["PID"])
                                        {
                                            parish.ID = int.Parse(reader["PID"].ToString());
                                            parish.parish = reader["parish"].ToString();
                                        }


                                        add.road = road;
                                        add.roadtype = road1;
                                        add.zipcode = zip;
                                        add.city = city;
                                        add.country = country;
                                        add.parish = parish;

                                        person.address = add;
                                        Employee employee = new Employee(int.Parse(reader["EmployeeID"].ToString()));
                                        employee.company = new Company(int.Parse(reader["CompanyID"].ToString()));
                                        employee.person = person;
                                        user.employee = employee;
                                    }
                                }
                            }
                        }
                        
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            
            }


            catch (Exception Lm)
            {


                string HttMessage = "<SCRIPT LANGUAGE=\"JavaScript\"> alert(" + Lm.StackTrace + "); </SCRIPT>";

                System.Web.HttpContext.Current.Response.Write(HttMessage);

            }




           
            return user;
        }

        /// <summary>
        /// this function tests if this trn is in use in the system
        /// </summary>
        /// <param name="trn"></param>
        /// <returns></returns>
        public static bool TestTRNAvailable(string trn)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC TestTRNAvailable '" + Constants.CleanString(trn) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "person", new Person());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }


        /// <summary>
        /// This function retrieves a person's information, given the person's TRN
        /// </summary>
        /// <param name="TRN">Person TRN</param>
        /// <returns>Person object with person's information</returns>
        public static Person GetPersonByTRN(string TRN)
        {
            //bool result = false;
            Person person = new Person();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetPersonByTRN '" + Constants.CleanString(TRN) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "person", person);
                while (reader.Read())
                {
                    person = new Person(int.Parse(reader["ID"].ToString()), reader["fname"].ToString(), reader["mname"].ToString(), reader["lname"].ToString(), null, null, reader["TRN"].ToString());
                    if(DBNull.Value != reader["base_id"]) person.base_id = int.Parse(reader["base_id"].ToString());
                    if (DBNull.Value != reader["AID"]) person.address = new Address(int.Parse(reader["AID"].ToString()));
                    if (DBNull.Value != reader["ImportID"]) person.ID = reader["ImportID"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return person;
        }

        /// <summary>
        /// This function retrieves a person's information, given the person's TRN
        /// </summary>
        /// <param name="person">Person object</param>
        /// <returns>Person object with person's information</returns>
        public static bool getPersonWithTRN(Person person)
        {
            bool found = true;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE GetPersonWithTRN '" + Constants.CleanString(person.TRN) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "person", person);
                while (reader.Read())
                {
                    found = (bool)(reader["result"]);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return found;
        }

        /// <summary>
        /// This function pulls the list of all persons yet to be added to to a particular vehicle as a driver
        /// </summary>
        /// <param name="vin">VIN of vehicle</param>
        /// <returns>List of persons</returns>
        public static List<Person> GetNonDriver(string chassis, int polId)
        {
            //authorized
            sql sql = new sql();
            List<Person> persons = new List<Person>();
            int MyCompID;
            
            if (Constants.user.newRoleMode) MyCompID = Constants.user.tempUserGroup.company.CompanyID;
            else MyCompID = CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID;

            if (sql.ConnectSQL())
            {
                string query = "EXECUTE GetPossibleDrivers '" + Constants.CleanString(chassis) + "'," + polId + "," + MyCompID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "person", new Person());

                while (reader.Read())
                {
                    persons.Add(new Person(int.Parse(reader["PeopleId"].ToString()), reader["Fname"].ToString(), reader["Lname"].ToString(), reader["TRN"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return persons;
        }
        /// <summary>
        /// This function retrieves a person Id of a person, given person address ID
        /// </summary>
        ///<param name="personAddId"> Person address ID </param>
        /// <returns>integer- Person Id</returns>
        public static int getPersonFromAddressId(int personAddId)
        {
            sql sql = new sql();
            int returnPersonId = 0;
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXECUTE getPersonId " + personAddId, "read","person", new Person());
                while (reader.Read())
                {
                    returnPersonId = int.Parse(reader["PersonID"].ToString());  
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return returnPersonId; 
        }

        public static int GetPersonByImportID(string IDs)
        {
            int personID = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetPersonByImportID '" + Constants.CleanString(IDs) + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    personID = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return personID;
        }

        public static List<Person> GetCompanyPeople(int compID, DateTime start, DateTime end, string typeOfDate = null)
        {
            List<Person> people = new List<Person>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                if (typeOfDate == null) typeOfDate = "created";
                string query = "EXEC GetCompanyPeople " + compID + ",'" + typeOfDate + "'";
                if ((start == DateTime.Parse("1969-12-31 00:00:00") || (start == DateTime.Parse("1969-12-31 19:00:00")) && (end == DateTime.Parse("1969-12-31 00:00:00") || end == DateTime.Parse("1969-12-31 19:00:00"))))
                {
                    query += ",'" + null + "','" + null + "'";
                }
                else
                    query += ",'" + start.ToString("yyyy-MM-dd HH:mm:ss") + "','" + end.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                
                SqlDataReader reader = sql.QuerySQL(query);
                Person person = new Person();
                while (reader.Read())
                {
                    person.PersonID = int.Parse(reader["PeopleID"].ToString());
                    person.TRN = reader["TRN"].ToString() == "10000000" ? "Not Available" : reader["TRN"].ToString();
                    person.fname = reader["FName"].ToString();
                    person.mname = reader["MName"].ToString();
                    person.lname = reader["LName"].ToString();
                    person.imported = (bool)reader["imported"];

                    people.Add(person);
                    person = new Person();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return people;
        }

        public static void MarkPeopleImported(Person per,bool policyHolder = false)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC MarkPeopleImported " + per.PersonID + "," + policyHolder;
                SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        public static int GetPersonCurrentAddress(int personID)
        {
            int currentID = personID;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCurrentPersonID " + personID;
                SqlDataReader reader = sql.QuerySQL(query);

                while(reader.Read())
                {
                    currentID = int.Parse(reader["ID"].ToString());
                }

                reader.Close();
                sql.DisconnectSQL();
            }

            return currentID;
        }

    }

}
 
 
    
