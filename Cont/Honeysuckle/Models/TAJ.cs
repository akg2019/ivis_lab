﻿using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Text; 
using System.Web.Services.Description; 
using System.Web.Services;
using Microsoft.Web.Services3;
using System.Web;
using System.Web.Services.Protocols;
using System.Net; 
using System.CodeDom; 
using System.CodeDom.Compiler; 
using System.Security.Permissions; 
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class TAJ
    {
        private static string username  = "iajadmin";
        private static string password  = "iaj3678";

        public int id { get; set; }
        public Company Company { get; set; }
        public string CompUserName { get; set; }
        public string CompPassowrd { get; set; }
        public bool DefaultOption { get; set; }


        public TAJ(){ }
        public TAJ(Company company, string UserName, string Password, bool Default ) 
        {
            this.Company = company;
            this.CompUserName = UserName;
            this.CompPassowrd = Password;
            this.DefaultOption = Default;
        }

        /// <summary>
        /// this function generates a user token for SOAP purposes
        /// </summary>
        /// <returns></returns>
        public static Microsoft.Web.Services3.Security.Tokens.UsernameToken GenerateUserToken()
        {
            return new Microsoft.Web.Services3.Security.Tokens.UsernameToken(TAJ.username, TAJ.password, Microsoft.Web.Services3.Security.Tokens.PasswordOption.SendPlainText);
        }

        /// <summary>
        /// this function gets a driver model from TAJ service
        /// </summary>
        /// <param name="trn"></param>
        /// <returns></returns>
        public static Driver GetDriversOnTRN(string trn)
        {
            Driver driver = new Driver();
            if (Constants.EnableTAJ)
            {
                try
                {
                    FSL.FslDriversLicenceService fsl = new FSL.FslDriversLicenceService();
                    FSL.DriverLicenceDetails details = fsl.getDriversLicenceI(trn);

                    driver.dateissued = DateTime.Parse(details.issueDate.Trim());
                    driver.expirydate = DateTime.Parse(details.expiryDate.Trim());
                    driver.licenseclass = details.dlClassDesc.Trim();
                    driver.licenseno = details.controlNo.Trim();

                    driver.person = new Person();
                    driver.person.fname = details.firstName.Trim();
                    driver.person.mname = details.middleName.Trim();
                    driver.person.lname = details.lastName.Trim();
                    driver.person.TRN = trn;

                    driver.person.address = AddressManagementFSL.ParseAddressFromService(details.address);

                }
                catch (SoapException)
                {
                    //to write special code for soap exception
                    driver = new Driver();
                }
                catch
                {
                    driver = new Driver();
                }
            }
            return driver;
        }
        /// <summary>
        /// this function returns personal details based on trn
        /// </summary>
        /// <param name="trn"></param>
        /// <returns></returns>
        public static Person GetPersonDetails(string trn)
        {
            Person person = new Person();
            if (Constants.EnableTAJ)
            {
                try
                {
                    FSL.TrnValidatorService trnservice = new FSL.TrnValidatorService();

                    FSL.IndividualExtendedDTO trnout = trnservice.getIndividualExtended(trn);

                    person.fname = trnout.currentName.firstName.Trim();
                    person.mname = trnout.currentName.middleName.Trim();
                    person.lname = trnout.currentName.lastName.Trim();
                    person.TRN = trn;
                    if (trnout.birthDate != null) person.DOB = (DateTime)trnout.birthDate; //to be tested
                    person.address = AddressManagementFSLDTO.ParseAddressFromService(trnout.residentAddress);

                }
                catch (SoapException)
                {
                    //to write special code for soap exception
                    person = new Person();
                }
                catch
                {
                    person = new Person();
                }
            }
            return person;
        }
        /// <summary>
        /// this function returns vehicle details based on chassisno, plateno or vehicle_id
        /// </summary>
        /// <param name="chassisno"></param>
        /// <param name="vehid"></param>
        /// <param name="plateno"></param>
        /// <returns></returns>
        public static Vehicle GetVehicleDetails(string chassisno, string vehid, string plateno)
        {
            Vehicle vehicle = new Vehicle();
            if (Constants.EnableTAJ)
            {
                #region null cleanup
                if (chassisno == null) chassisno = "";
                if (vehid == null) vehid = "";
                if (plateno == null) plateno = "";
                #endregion

                try
                {
                    FSL.FslMotorVehicleService vehservice = new FSL.FslMotorVehicleService();

                    FSL.Vehicle vehreturn = vehservice.getVehicleDetails(plateno, vehid, chassisno);

                    vehicle.chassisno = vehreturn.vehInfo.chassisNo.Trim();
                    vehicle.engineNo = vehreturn.vehInfo.engineNo.Trim();
                    vehicle.VehicleRegNumber = vehreturn.vehInfo.plateRegistrationNo.Trim();
                    vehicle.VehicleYear = int.Parse(vehreturn.vehInfo.vehicleYear.Trim());
                    vehicle.VehicleModel = vehreturn.vehInfo.vehicleModel.Trim();
                    vehicle.colour = vehreturn.vehInfo.vehicleColour.Trim();

                    /*Adding this for more info to vehicle- 20/5/2016*/

                    vehicle.make = vehreturn.vehInfo.vehicleMakeDesc.Trim();
                    vehicle.bodyType = vehreturn.vehInfo.vehicleBodyTypeDesc.Trim();

                    /*Ending extra info*/

                    vehicle.expiryDateOfFitness = DateTime.Parse(vehreturn.vehFitness.expDate.Trim());

                    int last = vehreturn.vehOwners.Count(); int i = 0; //for test purposes
                    if (vehreturn.vehOwners.Count() > 0) vehicle.RegisteredOwner = "";
                    foreach (var own in vehreturn.vehOwners)
                    {
                        i++;
                        vehicle.RegisteredOwner += own.firstName.Trim() + " " + own.lastName.Trim();
                        if (i != last) vehicle.RegisteredOwner += ", ";
                    }

                }
                catch (SoapException)
                {
                    //to write special code for soap exception
                    vehicle = new Vehicle();
                }
                catch (Exception)
                {
                    vehicle = new Vehicle();
                }
            }
            return vehicle;
        }
    }

    class AddressManagementFSL : AddressManagement
    {
        public static Address ParseAddressFromService(FSL.Address add)
        {
            Address address = new Address();

            address.roadnumber = add.streetNumber.Trim();
            address.road = new Road(add.streetName.Trim());
            address.roadtype = new RoadType();
            address.zipcode = new ZipCode();
            address.city = new City(add.mark.Trim()); //is the mark the city
            address.parish = new Parish(add.parish.Trim());
            address.country = new Country("Jamaica"); //defaulted for now

            return address;
        }

        public override Address ParseAddressFromService() { return new Address(); }
    }
    class AddressManagementFSLDTO : AddressManagement
    {
        public static Address ParseAddressFromService(FSL.AddressDTO add)
        {
            Address address = new Address();

            address.roadnumber = add.streetNumber.Trim();
            address.road = new Road(add.streetName.Trim());
            address.city = new City(add.mark.Trim()); //is the mark the city
            address.parish = new Parish(add.parishName.Trim());
            address.country = new Country(add.countryName.Trim());
            address.roadtype = new RoadType();
            address.zipcode = new ZipCode();

            return address;
        }

        public override Address ParseAddressFromService() { return new Address(); }
    }
    abstract class AddressManagement
    {
        public abstract Address ParseAddressFromService();
    }

    public class TAJModel
    {

        /// <summary>
        /// this function retreives the TAJ credentials of a specific company
        /// </summary>
        /// <param name="compId"></param>
        /// <returns></returns>
        public static TAJ GetCompanyTAJCredentials(int compId)
        {
            TAJ TAJCredentials = new TAJ ();
            TAJCredentials.Company = new Company(compId);

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCompanyTAJCredentials " + compId;

                SqlDataReader reader = sql.QuerySQL(query, "read", "taj", new TAJ());

                while (reader.Read())
                {
                    if (DBNull.Value != reader["UserName"]) TAJCredentials.CompUserName = reader["UserName"].ToString();
                    if (DBNull.Value != reader["Password"]) TAJCredentials.CompPassowrd = reader["Password"].ToString();
                    if (DBNull.Value != reader["DefaultOption"]) TAJCredentials.DefaultOption = (bool) reader["DefaultOption"];
                }

                reader.Close();
                sql.DisconnectSQL();
            }
            return TAJCredentials;
        }

        /// <summary>
        /// this function updates a company's TAJ credentials
        /// </summary>
        /// <param name="compTRNCredentials"></param>
        /// <returns></returns>
        public static bool EditCompTAJCredentials(TAJ compTRNCredentials)
        {
            TAJ TAJCredentials = new TAJ();

            sql sql = new sql();
            bool result = false;

            if (sql.ConnectSQL())
            {
                string query = "EXEC EditCompanyTAJCredentials " + compTRNCredentials.Company.CompanyID + ",'" 
                                                                 + compTRNCredentials.CompUserName +"','" 
                                                                 + compTRNCredentials.CompPassowrd +"'," 
                                                                 + compTRNCredentials.DefaultOption ;

                SqlDataReader reader = sql.QuerySQL(query, "update", "taj", compTRNCredentials);

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }

                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        public static TAJ GetCompanyTAJCredentials(TAJ taj)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCompanyTAJCredentials " + 0 + "," + taj.id;

                SqlDataReader reader = sql.QuerySQL(query, "read", "taj", new TAJ());

                while (reader.Read())
                {
                    if (DBNull.Value != reader["UserName"]) taj.CompUserName = reader["UserName"].ToString();
                    if (DBNull.Value != reader["Password"]) taj.CompPassowrd = reader["Password"].ToString();
                    if (DBNull.Value != reader["DefaultOption"]) taj.DefaultOption = (bool)reader["DefaultOption"];
                }

                reader.Close();
                sql.DisconnectSQL();
            }
            return taj;
        }


    }

}