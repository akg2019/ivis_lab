﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Threading;

namespace Honeysuckle.Models
{
    public class Audit
    {

        public int ID { get; set; }
        public string action { get; set; }
        public DateTime timestamp { get; set; }
        public string note { get; set; }
        public object original_data { get; set; }
        public object new_data { get; set; }
        public Person person { get; set; }
        public string ip_address { get; set; }
        public string table { get; set; }
        public int reference_id { get; set; }
        public bool multiples { get; set; }
        public object newObjectData { get; set; }
        public object oldObjectData { get; set; }
        public string specifcRecordUniqueID { get; set; }
        
        public Audit() { }
        public Audit(int ID)
        {
            this.ID = ID;
        }
        public Audit(int ID, string action, DateTime timestamp, string original_data, string new_data, Person person)
        {
            this.ID = ID;
            this.action = action;
            this.timestamp = timestamp;
            this.original_data = original_data;
            this.new_data = new_data;
            this.person = person;
        }
        public Audit(string action, string note, DateTime timestamp, object original_data, object new_data, Person person, string ip_address, string table, int reference_id, bool multiples)
        {
            this.action = action;
            this.note = note;
            this.timestamp = timestamp;
            this.original_data = original_data;
            this.new_data = new_data;
            this.person = person;
            this.ip_address = ip_address;
            this.table = table;
            this.reference_id = reference_id;
            this.multiples = multiples;
        }
        public Audit(string action, string note, DateTime timestamp, object original_data, object new_data, Person person, string ip_address, string table, int reference_id, bool multiples,object newObjectData)
        {
            this.action = action;
            this.note = note;
            this.timestamp = timestamp;
            this.original_data = original_data;
            this.new_data = new_data;
            this.person = person;
            this.ip_address = ip_address;
            this.table = table;
            this.reference_id = reference_id;
            this.multiples = multiples;
            this.newObjectData = newObjectData;
        }
    }
    public class AuditModel
    {
        /// <summary>
        /// this is the function that sql class calls to log a change
        /// it spawns a new thread where the log is managed so that 
        /// the process of logging data doesn't affect the user's experience too much
        /// </summary>
        /// <param name="audit"></param>
        /// <param name="uid"></param>
        public static void AuditLogThreadHandler(Audit audit, int uid)
        {
            if (ShouldILog(audit))
            {
                Thread thread = new Thread(() => LogAudit(audit, uid));
                thread.Start();
            }
        }

        /// <summary>
        /// test function to decide whether logging is required
        /// </summary>
        /// <param name="audit"></param>
        /// <returns></returns>
        private static bool ShouldILog(Audit audit)
        {
            bool result = true;
            switch (audit.table)
            {
                case "address":
                case "mortgagee":
                case "mappingdata":
                case "word_usage_cover":
                case "suberror":
                    result = false;
                    break;
                default:
                    break;
            }
            return result;
        }

        /// <summary>
        /// this function retrieves the ID of the of the record that we are logging
        /// this function is only called when the id is 0 and we are creating a record
        /// </summary>
        /// <param name="audit"></param>
        /// <returns></returns>
        private static int get_reference_id(Audit audit)
        {
            int reference_id = 0;
            switch (audit.table)
            {
                case "company":
                    reference_id = CompanyModel.GetCompanyByTRN(((Company)audit.newObjectData).trn).CompanyID;
                    break;
                case "person":
                    reference_id = PersonModel.GetPersonByTRN(((Person)audit.newObjectData).TRN).PersonID;
                    break;
                case "driver":
                    reference_id = DriverModel.GetDriverFromPersonID(((Driver)audit.newObjectData).person).DriverId;
                    break;
                case "usergroup":
                    reference_id = UserGroupModel.GetUserGroup((UserGroup)audit.newObjectData).UserGroupID;
                    break;
                case "policy":
                    //DO I REALLY WANT TO DO THIS???????
                    if (((Policy)audit.newObjectData).EDIID != null && ((Policy)audit.newObjectData).EDIID != "") reference_id = PolicyModel.get_policy_by_edi(((Policy)audit.newObjectData).EDIID);
                    else if (((Policy)audit.newObjectData).policyNumber != null && ((Policy)audit.newObjectData).policyNumber != "")
                        reference_id = PolicyModel.GetPolicyByNo(((Policy)audit.newObjectData).policyNumber).ID;
                    break;
                case "vehicle":
                    reference_id = VehicleModel.GetVehicle(((Vehicle)audit.newObjectData).ID).base_id;
                    break;
                case "vehiclecovernote":
                    reference_id = VehicleCoverNoteModel.GetCoverNoteByCoverNoteID(((VehicleCoverNote)audit.newObjectData).covernoteid).ID;
                    break;
                case "vehiclecertificate":
                    reference_id = VehicleCertificateModel.GetVehicleCertificateByCertificateNo(((VehicleCertificate)audit.newObjectData).CertificateNo).VehicleCertificateID;
                    break;
                default:
                    break;
            }
            return reference_id;
        }

        private static int get_reference_id(Object obj)
        {
            Audit audit = new Audit();
            audit.newObjectData = obj;
            audit.table = get_type_in_text(obj);
            return get_reference_id(audit);
        }

        /// <summary>
        /// this function retrieves the logs for a specific record
        /// </summary>
        /// <param name="location"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<Audit> GetAuditLog(string location, int id)
        {
            List<Audit> log = new List<Audit>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetAuditLog '" + Constants.CleanString(location) + "'," + id);
                while (reader.Read())
                {
                    log.Add(new Audit(int.Parse(reader["ID"].ToString()), 
                                      reader["AuditType"].ToString(), 
                                      DateTime.Parse(reader["datetime"].ToString()), 
                                      reader["orig"].ToString(),
                                      reader["new"].ToString(),
                                      new Person(int.Parse(reader["pid"].ToString()), reader["fname"].ToString(), reader["lname"].ToString())));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            //log = MakeNotes(log);
            return log;
        }

        /// <summary>
        /// this function logs a change in data to the database
        /// </summary>
        /// <param name="audit"></param>
        /// <param name="uid"></param>
        private static void LogAudit(Audit audit, int uid)
        {
            try
            {
                sql sql = new sql();
                SqlDataReader reader;
                if (sql.ConnectSQL())
                {
                    if (audit.multiples)
                    {
                        string orig ="";
                        List<object> orig_data_list = (audit.original_data as IEnumerable<object>).ToList();
                        for (int i = 0; i < orig_data_list.Count(); i++)
                        {
                            if (audit.action == "create")
                            {
                                orig = "";
                                audit.new_data = "";
                            }
                            else orig = (string) orig_data_list.ElementAt(i);
                            //if (audit.reference_id == 0 && audit.action == "create") audit.reference_id = get_reference_id(audit);
                            reader = sql.QuerySQL("EXEC LogAudit '" + Constants.CleanString(audit.action) + "',"
                                                                    + uid + ",'"
                                                                    + Constants.CleanString(audit.note) + "','"
                                                                    + Constants.CleanString(get_type_in_text(orig_data_list.ElementAt(i))) + "',"
                                                                    + get_reference_id(orig_data_list.ElementAt(i)) + ",'"
                                                                    + Constants.CleanString(orig) + "','"
                                                                    + Constants.CleanString((string)audit.new_data) + "','"
                                                                    + Constants.CleanString(audit.action) + "','"
                                                                    + Constants.CleanString(audit.ip_address) + "'");
                            reader.Close();
                        }
                        sql.DisconnectSQL();
                    }
                    else
                    {
                        //if (audit.reference_id == 0 && audit.action == "create") audit.reference_id = get_reference_id(audit);
                        if (audit.action == "create")
                        {
                            audit.original_data = "";
                            audit.new_data = "";
                        }
                        if (audit.reference_id == 0) audit.reference_id = get_reference_id(audit);
                        reader = sql.QuerySQL("EXEC LogAudit '" + Constants.CleanString(audit.action) + "',"
                                                                + uid + ",'"
                                                                + Constants.CleanString(audit.note) + "','"
                                                                + Constants.CleanString(audit.table) + "',"
                                                                + audit.reference_id + ",'"
                                                                + Constants.CleanString((string)audit.original_data) + "','"
                                                                + Constants.CleanString((string)audit.new_data) + "','"
                                                                + Constants.CleanString(audit.action) + "','"
                                                                + Constants.CleanString(audit.ip_address) + "'");
                        reader.Close();
                        sql.DisconnectSQL();
                    }
                }
                else
                {
                    //failed to log
                    Log.LogRecord("audit", audit.ToString());
                }
            }
            catch (Exception e)
            {
                Log.LogRecord("exception", e.Message);
                Log.LogRecord("audit", audit.ToString());
            }
        }

        /// <summary>
        /// this function determines the object type of a generic object so that
        /// later we can cast/serialize it accordingly for the audit log
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private static string get_type_in_text(Object obj)
        {
            string result = "";
            
            string re1 = ".*?";	// Non-greedy match on filler
            string re2 = "(?:[a-z][a-z]+)";	// Uninteresting: word
            string re3 = ".*?";	// Non-greedy match on filler
            string re4 = "(?:[a-z][a-z]+)";	// Uninteresting: word
            string re5 = ".*?";	// Non-greedy match on filler
            string re6 = "((?:[a-z][a-z]+))";	// Word 1

            Regex r = new Regex(re1 + re2 + re3 + re4 + re5 + re6, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match m = r.Match(obj.GetType().ToString().ToLower());
            if (m.Success)
            {
                result = m.Groups[1].ToString();
            }
            return result;
        }

        /// <summary>
        /// this function makes a JSON string of an object
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string SerializeObject(Object obj, string type)
        {
            bool run = true;
            
            MemoryStream ms = new MemoryStream();
            DataContractJsonSerializer ser;
            switch (type.ToLower())
            {
                #region switch code
                case "honeysuckle.models.address":
                    ser = new DataContractJsonSerializer(typeof(Address));
                    break;
                case "honeysuckle.models.businessintelligence":
                    ser = new DataContractJsonSerializer(typeof(BusinessIntelligence));
                    break;
                case "honeysuckle.models.certcover":
                    ser = new DataContractJsonSerializer(typeof(CertCover));
                    break;
                case "honeysuckle.models.city":
                    ser = new DataContractJsonSerializer(typeof(City));
                    break;
                case "honeysuckle.models.company":
                    ser = new DataContractJsonSerializer(typeof(Company));
                    break;
                case "honeysuckle.models.country":
                    ser = new DataContractJsonSerializer(typeof(Country));
                    break;
                case "honeysuckle.models.driver":
                    ser = new DataContractJsonSerializer(typeof(Driver));
                    break;
                case "honeysuckle.models.email":
                    ser = new DataContractJsonSerializer(typeof(Email));
                    break;
                case "honeysuckle.models.employee":
                    ser = new DataContractJsonSerializer(typeof(Employee));
                    break;
                case "honeysuckle.models.enquiry":
                    ser = new DataContractJsonSerializer(typeof(Enquiry));
                    break;
                case "honeysuckle.models.error":
                    ser = new DataContractJsonSerializer(typeof(error));
                    break;
                case "honeysuckle.models.handdrivetype":
                    ser = new DataContractJsonSerializer(typeof(HandDriveType));
                    break;
                case "honeysuckle.models.import":
                    ser = new DataContractJsonSerializer(typeof(Import));
                    break;
                case "honeysuckle.models.intelcount":
                    ser = new DataContractJsonSerializer(typeof(IntelCount));
                    break;
                case "honeysuckle.models.intelcountcompany":
                    ser = new DataContractJsonSerializer(typeof(IntelCountCompany));
                    break;
                case "honeysuckle.models.intermediary":
                    ser = new DataContractJsonSerializer(typeof(Intermediary));
                    break;
                case "honeysuckle.models.json":
                    ser = new DataContractJsonSerializer(typeof(JSON));
                    break;
                case "honeysuckle.models.kpi":
                    ser = new DataContractJsonSerializer(typeof(KPI));
                    break;
                case "honeysuckle.models.mappingdata":
                    ser = new DataContractJsonSerializer(typeof(MappingData));
                    break;
                case "honeysuckle.models.mortgagee":
                    ser = new DataContractJsonSerializer(typeof(Mortgagee));
                    break;
                case "honeysuckle.models.notification":
                    ser = new DataContractJsonSerializer(typeof(Notification));
                    break;
                case "honeysuckle.models.parish":
                    ser = new DataContractJsonSerializer(typeof(Parish));
                    break;
                case "honeysuckle.models.person":
                    ser = new DataContractJsonSerializer(typeof(Person));
                    break;
                case "honeysuckle.models.phone":
                    ser = new DataContractJsonSerializer(typeof(Phone));
                    break;
                case "honeysuckle.models.policy":
                    ser = new DataContractJsonSerializer(typeof(Policy));
                    break;
                case "honeysuckle.models.policycover":
                    ser = new DataContractJsonSerializer(typeof(PolicyCover));
                    break;
                case "honeysuckle.models.road":
                    ser = new DataContractJsonSerializer(typeof(Road));
                    break;
                case "honeysuckle.models.roadtype":
                    ser = new DataContractJsonSerializer(typeof(RoadType));
                    break;
                case "honeysuckle.models.rsa":
                    ser = new DataContractJsonSerializer(typeof(RSA));
                    break;
                case "honeysuckle.models.suberror":
                    ser = new DataContractJsonSerializer(typeof(suberror));
                    break;
                case "honeysuckle.models.taj":
                    ser = new DataContractJsonSerializer(typeof(TAJ));
                    break;
                case "honeysuckle.models.templateimage":
                    ser = new DataContractJsonSerializer(typeof(TemplateImage));
                    break;
                case "honeysuckle.models.templatewording":
                    ser = new DataContractJsonSerializer(typeof(TemplateWording));
                    break;
                case "honeysuckle.models.usage":
                    ser = new DataContractJsonSerializer(typeof(Usage));
                    break;
                case "honeysuckle.models.user":
                    ser = new DataContractJsonSerializer(typeof(User));
                    break;
                case "honeysuckle.models.usergroup":
                    ser = new DataContractJsonSerializer(typeof(UserGroup));
                    break;
                case "honeysuckle.models.userpermission":
                    ser = new DataContractJsonSerializer(typeof(UserPermission));
                    break;
                case "honeysuckle.models.vehicle":
                    ser = new DataContractJsonSerializer(typeof(Vehicle));
                    break;
                case "honeysuckle.models.vehiclecertificate":
                    ser = new DataContractJsonSerializer(typeof(VehicleCertificate));
                    break;
                case "honeysuckle.models.vehiclecovernote":
                    ser = new DataContractJsonSerializer(typeof(VehicleCoverNote));
                    break;
                case "honeysuckle.models.vehiclecovernotegenerated":
                    ser = new DataContractJsonSerializer(typeof(VehicleCoverNoteGenerated));
                    break;
                case "honeysuckle.models.zipcode":
                    ser = new DataContractJsonSerializer(typeof(ZipCode));
                    break;
                default:
                    ser = new DataContractJsonSerializer(typeof(object));
                    run = false;
                    break;
                #endregion
            }
            if (run)
            {
                ser.WriteObject(ms, obj);

                ms.Position = 0;
                StreamReader reader = new StreamReader(ms);
                return reader.ReadToEnd();
            }
            else return null;
        }

        /// <summary>
        /// this function returns the contents of a single audit record
        /// </summary>
        /// <param name="id">database unique identifier of record</param>
        /// <returns>completely realized audit object</returns>
        public static Audit GetAudit(int id, bool objectCast = false)
        {
            Audit audit = new Audit();
            audit.ID = id;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetAudit " + id);
                while (reader.Read())
                {
                    audit.timestamp = DateTime.Parse(reader["timestamp"].ToString());
                    audit.action = reader["audit_type"].ToString();
                    //audit.person 
                    audit.table = reader["location"].ToString();

                    audit.oldObjectData = reader["orig_data"].ToString();
                    audit.newObjectData = reader["new_data"].ToString();

                    if (objectCast)
                    {
                        audit.oldObjectData = CastObject(audit.oldObjectData.ToString(), audit.table);
                        audit.newObjectData = CastObject(audit.newObjectData.ToString(), audit.table);
                    }

                    audit.reference_id = int.Parse(reader["record_id"].ToString());
                }
                sql.DisconnectSQL();
            }
            return audit;
        }

        /// <summary>
        /// this function changes a JSON to a object
        /// </summary>
        /// <param name="json">json to be converted</param>
        /// <param name="type">object identifier</param>
        /// <returns>object having been converted from json</returns>
        private static object CastObject(string json, string type)
        {
            object obj = new object();
            try
            {
                switch (type)
                {
                    case "company":
                        obj = JsonConvert.DeserializeObject<Company>(json);
                        break;
                    case "driver":
                        obj = JsonConvert.DeserializeObject<Driver>(json);
                        break;
                    case "email":
                        obj = JsonConvert.DeserializeObject<Email>(json);
                        break;
                    case "employee":
                        obj = JsonConvert.DeserializeObject<Employee>(json);
                        break;
                    case "intermediary":
                        obj = JsonConvert.DeserializeObject<Intermediary>(json);
                        break;
                    case "mappingdata":
                        obj = JsonConvert.DeserializeObject<MappingData>(json);
                        break;
                    case "mortgagee":
                        obj = JsonConvert.DeserializeObject<Mortgagee>(json);
                        break;
                    case "address":
                        obj = JsonConvert.DeserializeObject<Address>(json);
                        break;
                    case "policy":
                        obj = JsonConvert.DeserializeObject<Policy>(json);
                        break;
                    case "user":
                        obj = JsonConvert.DeserializeObject<User>(json);
                        break;
                    case "vehiclecovernote":
                        obj = JsonConvert.DeserializeObject<VehicleCoverNote>(json);
                        break;
                    case "vehiclecertificate":
                        obj = JsonConvert.DeserializeObject<VehicleCoverNote>(json);
                        break;
                    default:
                        break;
                }
            }
            catch
            {
                //nothing yet
            }
            return obj;
        }


        public static int getLastInsertedID(string location)
        {
            sql sql = new sql();
            int id = 0;
            string query = "";

            if (sql.ConnectSQL())
            {
                query = "EXEC GetLastInsertedID '" + location + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    id = int.Parse(reader["ID"].ToString());
                }

                reader.Close();
                sql.DisconnectSQL();
            }

            return id;
        }

        public static List<Audit> GetAuditByUser(int userID = 0, string location = "")
        {
            
            Audit audit = new Audit();
            List<Audit> audits = new List<Audit>();
            string query = "";
            sql sql = new sql();
            if(userID != 0 && location != "")
            {
                if (sql.ConnectSQL())
                {
                    query = "EXEC GetAuditByUser " + userID + ",'" + Constants.CleanString(location) + "'";
                    SqlDataReader reader = sql.QuerySQL(query);
                    while (reader.Read())
                    {
                        audit = new Audit();
                        audit.timestamp = DateTime.Parse(reader["timestamp"].ToString());
                        audit.action = reader["audit_type"].ToString();
                        audit.table = reader["location"].ToString();
                        audit.oldObjectData = CastObject(reader["orig_data"].ToString(), audit.table);
                        audit.newObjectData = CastObject(reader["new_data"].ToString(), audit.table);
                        audit.reference_id = int.Parse(reader["record_id"].ToString());
                        audit.ID = int.Parse(reader["id"].ToString());
                        audit.ip_address = reader["ipaddress"].ToString();

                        audit.specifcRecordUniqueID = GetRecordsUniqueID(audit.reference_id, location);
                        if (audit.specifcRecordUniqueID == null || audit.specifcRecordUniqueID == "") audit.specifcRecordUniqueID = "NOT AVAILABLE";

                        audits.Add(audit);
                    }

                    reader.Close();
                    sql.DisconnectSQL();
                }
            }

            return audits;
        }

        public static string GetRecordsUniqueID(int recordID = 0, string location = "")
        {
            string uniqueID = "";
            string query = "";
            sql sql = new sql();
            if (recordID != 0 && location != "")
            {
                if (sql.ConnectSQL())
                {
                    query = "EXEC GetRecordsUniqueID " + recordID + ",'" + location + "'";
                    SqlDataReader reader = sql.QuerySQL(query);
                    while (reader.Read())
                    {
                        uniqueID = reader["uniqueID"].ToString();
                    }
                }
            }

            return uniqueID;
        }
    }
}