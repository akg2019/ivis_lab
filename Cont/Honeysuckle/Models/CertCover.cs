﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class CertCover
    {
        public VehicleCoverNote vCoverNote { set; get; }
        public VehicleCertificate vCertificate { set; get; }
        public String Type { set; get; }

        public CertCover() { }
        public CertCover(String type, VehicleCoverNote vcn, VehicleCertificate vc)
        {
            this.Type = type;
            this.vCertificate = vc;
            this.vCoverNote = vcn;
        }

    }

    public class CertCoverModel {

        /// <summary>
        /// This function is used to return a list of cover notes and certificates currently pending approval for 
        /// the user currently logged in to the system
        /// </summary>
        /// <returns></returns>
        public static List<CertCover> GetCertCovers() {

            List<CertCover> certCovers = new List<CertCover>();
            sql sql = new sql();
            if(sql.ConnectSQL()) {
                string query = "EXEC GetVehicleCertsAndCover " + CompanyModel.GetMyCompany(Constants.user).CompanyID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "certcover", new CertCover());
                while (reader.Read())
                {
                    VehicleCoverNote vcn = new VehicleCoverNote();
                    vcn.Policy = new Policy();
                    VehicleCertificate vc = new VehicleCertificate();
                    vc.policy = new Policy();
                    switch (reader["Type"].ToString())
                    {
                        case "Certificate":
                            vc.VehicleCertificateID = Int32.Parse(reader["ID"].ToString());
                            if (DBNull.Value != reader["EDIID"]) vc.EDIID = reader["EDIID"].ToString();
                            else vc.EDIID = "None";

                            if (DBNull.Value != reader["Number"]) vc.CertificateNo = reader["Number"].ToString();
                            else vc.CertificateNo = "None";
                            if(DBNull.Value != reader["EffectiveDate"]) vc.EffectiveDate = DateTime.Parse(reader["EffectiveDate"].ToString());
                            if (DBNull.Value != reader["ExpiryDate"]) vc.ExpiryDate = DateTime.Parse(reader["ExpiryDate"].ToString());
                            if (DBNull.Value != reader["CreatedOn"]) vc.createdOn = DateTime.Parse(reader["CreatedOn"].ToString());
                            if (DBNull.Value != reader["PolicyNo"]) vc.policy.policyNumber = reader["PolicyNo"].ToString();
                            break;
                        case "Cover":
                            vcn.ID = Int32.Parse(reader["ID"].ToString());
                            if (DBNull.Value != reader["EDIID"]) vcn.EDIID = reader["EDIID"].ToString();
                            else vcn.EDIID = "None";

                            if (DBNull.Value != reader["Number"]) vcn.covernoteid = reader["Number"].ToString();
                            else vcn.covernoteid = "None";
                            if(DBNull.Value != reader["EffectiveDate"]) vcn.effectivedate = DateTime.Parse(reader["EffectiveDate"].ToString());
                            if(DBNull.Value != reader["ExpiryDate"]) vcn.expirydate = DateTime.Parse(reader["ExpiryDate"].ToString());
                            if (DBNull.Value != reader["CreatedOn"]) vcn.createdOn = DateTime.Parse(reader["CreatedOn"].ToString());
                            if (DBNull.Value != reader["PolicyNo"]) vcn.Policy.policyNumber = reader["PolicyNo"].ToString();
                            break;
                        default:
                            break;
                    }
                    certCovers.Add(new CertCover(reader["Type"].ToString(), vcn, vc));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return certCovers;
        }
    }
}