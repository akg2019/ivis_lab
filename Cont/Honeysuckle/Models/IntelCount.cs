﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class IntelCount
    {

        public Company company { get; set; }
        //x and y axis
        public string field { get; set; }
        public string yaxis { get; set; }
        //date field
        public string day { get; set; }
        public string month { get; set; }
        public string year { get; set; }
        //date type return
        public bool days { get; set; }
        public bool months { get; set; }
        public string years { get; set; }
        //count
        public int count { get; set; }

        public IntelCount( ) { }
        public IntelCount(Company company, int count)
        {
            this.company = company;
            this.count = count;
        }
        public IntelCount(Company company, string field, int count)
        {
            this.company = company;
            this.field = field;
            this.count = count;
        }
        public IntelCount(string month, int count)
        {
            this.month = month;
            this.count = count;
        }
    }
    public class IntelCountModel
    {
        /// <summary>
        /// this function returns the amount of certificates generated in the time period specified
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static List<IntelCount> CertIntelCountForTime(DateTime start, DateTime end)
        {
            List<IntelCount> counts = new List<IntelCount>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC CertIntelCountForTime '" + start.ToString("yyyy-MM-dd hh:mm:ss") + "','" + end.ToString("yyyy-MM-dd hh:mm:ss") + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "intelcount", new IntelCount());
                while (reader.Read())
                {
                    counts.Add(new IntelCount(new Company(int.Parse(reader["cid"].ToString()), reader["cname"].ToString()), reader["cname"].ToString(), int.Parse(reader["count"].ToString())));
                }
                sql.DisconnectSQL();
            }
            return counts;
        }

        //public static List<IntelCount> CertIntelCountForTime(DateTime start, DateTime end, Company insurer = null, bool month = false, bool day = false)
        //{
        //    List<IntelCount> counts = new List<IntelCount>();
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        string query = "EXEC CertIntelCountForTime '" + start.ToString("yyyy-MM-dd hh:mm:ss") + "','" + end.ToString("yyyy-MM-dd hh:mm:ss") + "'";
        //        if ((end - start).Days <= 30 && !day) month = true;

        //        if (insurer != null) query += "," + insurer.CompanyID;
        //        SqlDataReader reader = sql.QuerySQL(query, "read", "intelcount", new IntelCount());
        //        while (reader.Read())
        //        {
        //            counts.Add(new IntelCount(new Company(int.Parse(reader["cid"].ToString()), reader["cname"].ToString()), reader["cname"].ToString(), int.Parse(reader["count"].ToString())));
        //        }
        //        sql.DisconnectSQL();
        //    }
        //    return counts;
        //}

        /// <summary>
        /// this function manages the collection of business intelligence data for gauges on the front end
        /// </summary>
        /// <param name="company">company who's data we're looking at (by their insurer's in intermediaries)</param>
        /// <param name="month">month's data we're looking at</param>
        /// <param name="year">year's data we're looking at</param>
        /// <param name="type">type of data (i.e. "enquiry","error","covernote","certificate","doubleInsured"</param>
        /// <returns></returns>
        public static int CountByMonthbyInsurer(Company company, string month, string year, string type)
        {
            int output = 0;
            switch (type) 
            {
                case "enquiry":
                    output = EnquiryCountByMonthbyInsurer(company, month, year);
                    break;
                case "error":
                    output = errorModel.ErrorCount(company, month, year);
                    break;
                case "covernote":
                    output = VehicleCoverNoteModel.CoverNoteCountEffective(company, month, year);
                    break;
                case "certificate":
                    output = VehicleCertificateModel.CertificateCountEffective(company, month, year);
                    break;
                case "doubleInsured":
                    output = VehicleModel.GetNumberOfDoubleInsured(company);
                    break;
                default:
                    break;
            }
            return output;
        }

        /// <summary>
        /// this function returns the amount of enquiries (external searches) that match the a specific company in a specified month and a year in question.
        /// </summary>
        /// <param name="company">company who's insurers we should look at (via intermediary relationship)</param>
        /// <param name="month">month of data looked at</param>
        /// <param name="year">year of data looked at</param>
        /// <returns></returns>
        private static int EnquiryCountByMonthbyInsurer(Company company, string month, string year)
        {
            int output = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC EnquiryCount " + company.CompanyID + ",'" + Constants.CleanString(month) + "','" + Constants.CleanString(year) + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    output = int.Parse(reader["count"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return output;
        }

        /// <summary>
        /// this function returns a list of enquiries by insurer throughout a time period, this is used for the charting tool on the front end
        /// </summary>
        /// <param name="start">start date time</param>
        /// <param name="end">end date time</param>
        /// <param name="insurer">company object with valid id, optional.</param>
        /// <param name="days">boolean stating whether data should be daily, optional</param>
        /// <param name="months">boolean stating whether data will be monthly, optional</param>
        /// <returns></returns>
        public static List<IntelCount> EnquiryByInsurer(DateTime start, DateTime end, Company insurer = null, bool days = false, bool months = false)
        {
            List<IntelCount> enquiries = new List<IntelCount>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                if (insurer != null) insurer = new Company();
                string query = "EXEC EnquiryByInsurer '" + start.ToString("yyyy-MM-dd") + "','" + end.ToString("yyyy-MM-dd") + "'," + insurer.CompanyID + ",'" + days + "','" + months + "'";
                
                SqlDataReader reader = sql.QuerySQL(query, "read", "intelcount", new IntelCount());
                while (reader.Read())
                {
                    IntelCount intel = new IntelCount();
                    if (insurer.CompanyID == 0){
                        intel.company = new Company(int.Parse(reader["cid"].ToString()), reader["cname"].ToString());
                        intel.field = reader["fieldname"].ToString();
                        intel.yaxis = reader["yaxis"].ToString();
                        intel.days = false;
                        intel.months = false;
                        intel.count = int.Parse(reader["count"].ToString());

                        enquiries.Add(intel);   
                    }
                    else if (DBNull.Value == reader["day"])
                    {
                        //this is months
                        intel.month = reader["month"].ToString();
                        intel.year = reader["year"].ToString();
                        intel.months = true;
                        intel.company = insurer;
                        intel.count = int.Parse(reader["count"].ToString());
                        intel.field = reader["fieldname"].ToString();
                        intel.yaxis = reader["yaxis"].ToString();

                        enquiries.Add(intel);

                    }
                    else
                    {
                        //this is days
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return enquiries;
        }
    }
}