﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;

namespace Honeysuckle.Models
{
    public class City 
    {
       public int CityID { get; set; }

       [RegularExpression("[a-zA-Z](([a-zA-Z0-9&. '-])*[a-zA-Z0-9 ])?", ErrorMessage = "Incorrect City name format (some symbols are not allowed). The City/Town field must begin with a alpha character and end with an alphanumeric character")]
       public string CityName { get; set; }

       public City() { }       
       public City(int CityID1, string CityName1)
       {
           this.CityID = CityID1;
           this.CityName = CityName1;
       }
       public City(string CityName)
       {
           this.CityName = CityName;
       }

    }

    public class CityModel
    {
        /// <summary>
        /// This function gets all the cities stored in the database
        /// </summary>
        /// <returns>List of city names (strings)</returns>
        public static List<string> GetCityList()
        {
            List<string> result = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXECUTE GetCities", "read", "city", new City());
                while (reader.Read())
                {
                    result.Add(reader["CityName"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }
    }
}
