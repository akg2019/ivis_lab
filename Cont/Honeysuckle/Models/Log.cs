﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;


namespace Honeysuckle.Models
{
    public class Log
    {
        private static string logfile = "log/logfile.txt";

        /// <summary>
        /// this function logs error data for when the application fails to a static logfile
        /// </summary>
        /// <param name="errortype"></param>
        /// <param name="text"></param>
        public static void LogRecord(string errortype, string text)
        {
            try
            {
                //string baseUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/";
                string path = AppDomain.CurrentDomain.BaseDirectory;
                if (!(File.Exists(path + Log.logfile))) File.Create(path + Log.logfile);
                string line = "";
                switch (errortype)
                {
                    case "sql":
                        line = DateTime.Now.ToString() + " ==> SQL Connection Error ==> ";
                        File.AppendAllText(path + Log.logfile, line + text + Environment.NewLine);
                        break;
                    case "print":
                        line = DateTime.Now.ToString() + " ==> Print Error ==> ";
                        File.AppendAllText(path + Log.logfile, line + text + Environment.NewLine);
                        break;
                    case "cookie":
                        line = DateTime.Now.ToString() + " ==> Session Error ==> ";
                        File.AppendAllText(path + Log.logfile, line + text + Environment.NewLine);
                        break;
                    case "user":
                        line = DateTime.Now.ToString() + " ==> User Error ==> ";
                        File.AppendAllText(path + Log.logfile, line + text + Environment.NewLine);
                        break;
                    case "rsa":
                        line = DateTime.Now.ToString() + " ==> RSA Error ==> ";
                        File.AppendAllText(path + Log.logfile, line + text + Environment.NewLine);
                        break;
                    case "audit":
                        line = DateTime.Now.ToString() + " ==> AUDIT ERROR ==> ";
                        File.AppendAllText(path + Log.logfile, line + text + Environment.NewLine);
                        break;
                    case "back":
                        line = DateTime.Now.ToString() + " ==> BACK BTN ERROR ==> ";
                        File.AppendAllText(path + Log.logfile, line + text + Environment.NewLine);
                        break;
                    default:
                        line = DateTime.Now.ToString() + " ==> General Exception Error ==> ";
                        File.AppendAllText(path + Log.logfile, line + text + Environment.NewLine);
                        break;
                }
            }
            catch (Exception)
            {

            }
        }
    }
}