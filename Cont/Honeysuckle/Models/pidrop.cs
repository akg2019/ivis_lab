﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class pidrop
    {
        public string id { get; set; }
        public string name { get; set; }

        public pidrop() { }
        public pidrop(string id, string name)
        {
            this.id = id;
            this.name = name;
        }
    }
    public class pidropmodel
    {
        /// <summary>
        /// this function returns the ids of all the policy holders of a policy for
        /// the auto complete and drop down of the mailing/billing address system
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<pidrop> GetPolicyInsuredsDropDown(int id)
        {
            List<pidrop> output = new List<pidrop>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetPolicyInsuredsDropDown " + id, "read", "pidrop", new pidrop());
                while (reader.Read())
                {
                    output.Add(new pidrop(reader["id"].ToString(), reader["name"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return output;
        }
    
    }
}