﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Web.Routing;
using System.Data.SqlClient;
using System.Collections;

namespace Honeysuckle.Models
{
    public class Constants
    {

        //STORES THE AUTHENTICATED USER
        public static User user 
        {
            get
            {
                try
                {
                    if (HttpContext.Current.Session["User"] != null)
                    {
                        if (((User)HttpContext.Current.Session["User"]).ID != 0)
                        {
                            User user = HttpContext.Current.Session["User"] as User;
                            return user;
                        }
                        else return new User("not initialized");
                    }
                    else return new User("not initialized");
                }
                catch (Exception)
                {
                    //Log.LogRecord("user", "Catch exception " + e.Message);
                    return new User("not initialized");
                }
            }
            set
            {
                HttpContext.Current.Session["User"] = value;
            }
        }

        public static string ipaddress
        {
            get
            {
                try
                {
                    if (HttpContext.Current.Session["ip"] != null)
                    {
                        return HttpContext.Current.Session["ip"].ToString();
                    }
                    else return null;
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["ip"] = value;
            }
        }

        public static User web_import_user { get; set; }
        
        //this is a flag to enable/disable the TAJ query system
        public static bool EnableTAJ = false;
       
        private static string currentpage { get; set; }
        private static Stack<string> backto
        {
            get
            {
                try
                {
                    if (HttpContext.Current.Session["back"] != null)
                    {
                        Stack<string> back = HttpContext.Current.Session["back"] as Stack<string>;
                        return back;
                    }
                    else return new Stack<string>();
                }
                catch (Exception e)
                {
                    Log.LogRecord("back", "Catch exception " + e.Message);
                    return new Stack<string>();
                }
            }
            set
            {
                HttpContext.Current.Session["back"] = value;
            }
        }


        private static bool backpress = false;

        /// <summary>
        /// this function updates the pages that you have visited
        /// </summary>
        public static void updatePages()
        {
            string current = HttpContext.Current.Request.Url.AbsoluteUri;
            current = current.Substring(HttpContext.Current.Request.Url.Authority.Length + current.IndexOf(HttpContext.Current.Request.Url.Authority), current.Length - (HttpContext.Current.Request.Url.Authority.Length + current.IndexOf(HttpContext.Current.Request.Url.Authority)));
            if (currentpage != current && !current.Contains("/Error/NotFound?aspxerrorpath="))
            {
                if (backto != null && currentpage != null && !backpress)
                {
                    if (backto.Count > 0)
                    {
                        if (backto.Peek() != currentpage) backto.Push(currentpage);
                    }
                    else backto.Push(currentpage);
                }
                backpress = false;
                //if (currentpage != null) backto.Push(currentpage);
                currentpage = current;
            }
            if (current == "/") backto = new Stack<string>();
        }
        /// <summary>
        /// this function checks if the backto stack has been initialized.
        /// if initialized it returns the page at the top of the stack
        /// if not it returns a link to the home page
        /// </summary>
        /// <returns></returns>
        public static string GenerateBack()
        {
            if (backto == null || backto.Count == 0 || backto.Peek() == "/") return "/Home/Index";
            else return backto.Peek();
        }
        /// <summary>
        /// this function is called when a user clicks a 'back' button and pops a page off of the stack
        /// </summary>
        public static void BackClicked()
        {
            string current = HttpContext.Current.Request.Url.AbsolutePath;
            backpress = true;
            if (backto.Count > 0) backto.Pop();
        }
        /// <summary>
        /// this function clears out the stack for the back btn
        /// </summary>
        public static void ClearBack()
        {
            backto = new Stack<string>();
        }

        //THIS IS USED FOR MAIL, FOR WHEN A USER HASN'T BEEN AUTHENTICATED
        public static User tempuser { get; set; } 


        //USED FOR JSON REQUEST ERRORS
        public static List<error> parseerrors { get; set; }
        public static JSON JsonResponse { get; set; }
        
        //ON THE BUBBLE
        public static List<VehicleCertificate> certificateerrors { get; set; }
        public static List<VehicleCoverNote> covernoteerrors { get; set; }

        ///THIS SECTION IS FOR THE PDF PRINTING
        public static PdfSharp.Pdf.PdfDocument document 
        {
            get
            {
                if (HttpContext.Current.Session["pdf"] != null) return HttpContext.Current.Session["pdf"] as PdfSharp.Pdf.PdfDocument;
                else return null;
            }
            set
            {
                HttpContext.Current.Session["pdf"] = value;
            }
        }
        
        //advanced search uses this
        public static List<Search> searchresults 
        {
            get
            {
                if (HttpContext.Current.Session["searchresults"] != null)
                {
                    return HttpContext.Current.Session["searchresults"] as List<Search>;
                }
                else return null;
            }
            set
            {
                HttpContext.Current.Session["searchresults"] = value;
            }
        }

        //THIS SECTION IS TO STORE THE RSA KEY BEING CURRENTLY USED BY THIS LOGIN SESSION
        public static RSA rsa 
        {
            get
            {
                try
                {
                    if (HttpContext.Current.Session["rsa"] != null)
                    {
                        RSA rsa = HttpContext.Current.Session["rsa"] as RSA;
                        return rsa;
                    }
                    else return null;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["rsa"] = value;
            }
        }
        
        /// <summary>
        /// This function adds a ' to a string so that SQL queries don't break when ' is in a string
        /// </summary>
        /// <param name="input">input string</param>
        /// <returns>fixed for sql run string</returns>
        public static string CleanString(string input)
        {
            if (input != null)
            {
                string reg = "[']";
                Regex r = new Regex(reg, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                input = r.Replace(input, "''");
            }
            return input;
        }

        /// <summary>
        /// this function clears all session data from the application
        /// </summary>
        public static void ClearData()
        {
            user = new User();
            web_import_user = new User();
            tempuser = new User();
            currentpage = null;
            backto = null;
            backpress = false;
            parseerrors = null;
            JsonResponse = null;
            certificateerrors = null;
            covernoteerrors = null;
            document = null;
            searchresults = null;
            rsa = null;
        }


    }
}