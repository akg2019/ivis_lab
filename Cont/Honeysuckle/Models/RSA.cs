﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class RSA
    {
        public int id { get; set; }
        private string publickey { get; set; }
        private string session_private { get; set; }
        private string request_private { get; set; }
        private User user { get; set; }

        private RSA() { }
        private RSA(int id)
        {
            this.id = id;
        }

        #region RSA DB WORK
        /// <summary>
        /// this function stores the private sessions key in the database
        /// </summary>
        /// <param name="rsa"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        private static RSA StoreSessionKey(RSA rsa, int uid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC StoreSessionKey " + rsa.id + ",'" + rsa.session_private + "'," + uid;
                SqlDataReader reader = sql.QuerySQL(query, "read", "rsa", rsa);
                while (reader.Read())
                {
                    rsa.id = int.Parse(reader["id"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return rsa;
        }
        /// <summary>
        /// this stores the private request key in the database
        /// </summary>
        /// <param name="rsa"></param>
        /// <param name="uid"></param>
        /// <param name="kid"></param>
        /// <returns></returns>
        private static RSA StoreRequestKey(RSA rsa, int uid, int kid = 0)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query =  "";
                if (rsa.id != 0) query = "EXEC StoreRequestKey " + rsa.id + ",'" + rsa.request_private + "'," + uid;
                if (kid != 0) query = "EXEC StoreRequestKey " + kid + ",'" + rsa.request_private + "'," + uid;
                if (query != "")
                {
                    SqlDataReader reader = sql.QuerySQL(query, "read", "rsa", rsa);
                    while (reader.Read())
                    {
                        rsa.id = int.Parse(reader["id"].ToString());
                    }
                    reader.Close();
                }
                sql.DisconnectSQL();
            }
            return rsa;
        }
        /// <summary>
        /// this returns the decryption key for session keys based on a database id
        /// </summary>
        /// <param name="kid"></param>
        /// <returns></returns>
        private static RSA GetSessionKey(int kid)
        {
            RSA rsa = new RSA(kid);
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetSessionKey " + kid, "read", "rsa", new RSA());
                while(reader.Read())
                {
                    if ((bool)reader["exist"])
                    {
                        rsa.session_private = reader["session"].ToString();
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return rsa;
        }
        /// <summary>
        /// this function returns the decrypt key based on a database record id
        /// </summary>
        /// <param name="kid"></param>
        /// <returns></returns>
        private static RSA GetRequestKey(int kid)
        {
            RSA rsa = new RSA(kid);
            sql sql = new sql();
            bool exist = false;
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetRequestKey " + kid, "read", "rsa", new RSA());
                while (reader.Read())
                {
                    exist = (bool)reader["exist"];
                    if (exist) rsa.request_private = reader["request"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            if (!exist)
            {
                rsa = GenerateNewRSAKey("request", GetKidUID(kid));
            }
            return rsa;
        }
        /// <summary>
        /// this returns the user who's key is associated to
        /// </summary>
        /// <param name="kid"></param>
        /// <returns></returns>
        private static int GetKidUID(int kid)
        {
            int uid = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetKidUID " + kid, "read", "rsa", new RSA());
                while (reader.Read())
                {
                    uid = int.Parse(reader["uid"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return uid;
        }
        #endregion

        /// <summary>
        /// this function generates a new rsa key
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static RSA GenerateNewRSAKey(string type)
        {
            return GenerateNewRSAKey(type, Constants.user.ID);
        }

        /// <summary>
        /// this function generates a new rsa key
        /// </summary>
        /// <param name="type"></param>
        /// <param name="uid"></param>
        /// <param name="kid"></param>
        /// <returns></returns>
        public static RSA GenerateNewRSAKey(string type, int uid, int kid = 0)
        {
            RSA rsa_result = new RSA();
            if (kid != 0) rsa_result.id = kid;
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                switch(type)
                {
                    case "session":
                        rsa_result.session_private = rsa.ToXmlString(true);
                        rsa_result = StoreSessionKey(rsa_result, uid);
                        break;
                    case "request":
                        rsa_result.request_private = rsa.ToXmlString(true);
                        rsa_result = StoreRequestKey(rsa_result, uid, kid);
                        break;
                }
                rsa_result.publickey = rsa.ToXmlString(false);
            }
            return rsa_result;
        }

        /// <summary>
        /// this function removes all rsa keys from the session data
        /// </summary>
        public static void CleanKeys()
        {
            Constants.rsa = null;
        }

        /// <summary>
        /// this function ecrypts data given via the rsa method
        /// </summary>
        /// <param name="text"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string Encrypt(string text, string type)
        {
            return Encrypt(text, type, Constants.user.ID);
        }

        /// <summary>
        /// this function encrypts data via the rsa method
        /// </summary>
        /// <param name="text"></param>
        /// <param name="type"></param>
        /// <param name="uid"></param>
        /// <param name="kid"></param>
        /// <returns></returns>
        public static string Encrypt(string text, string type, int uid, int kid = 0)
        {
            Constants.rsa = GenerateNewRSAKey(type, uid, kid);

            var testData = Encoding.UTF8.GetBytes(text);
            using (var rsa = new RSACryptoServiceProvider(2048))
            {

                try
                {
                    // client encrypting data with public key issued by server
                    
                    string publicPrivateKeyXML;
                    switch (type)
                    {
                        case "session":
                            publicPrivateKeyXML = rsa.ToXmlString(true);
                            StoreSessionKey(Constants.rsa, uid);
                            break;
                        case "request":
                            publicPrivateKeyXML = Constants.rsa.request_private;
                            StoreRequestKey(Constants.rsa, uid, kid);
                            break;
                        default:
                            throw new Exception("bad type");
                    }
                    string publicOnlyKeyXML = Constants.rsa.publickey;
                    
                    rsa.FromXmlString(publicOnlyKeyXML);

                    var encryptedData = rsa.Encrypt(testData, true);
                    var base64Encrypted = Convert.ToBase64String(encryptedData);

                    return base64Encrypted;
                }
                catch (Exception e)
                {
                    Log.LogRecord("rsa", e.Message);
                    return "";
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }

            }
        }

        /// <summary>
        /// this function decrypts data via the rsa method
        /// </summary>
        /// <param name="text"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string Decrypt(string text, string type)
        {
            return Decrypt(text, type, Constants.rsa.id);
        }

        /// <summary>
        /// this function decrypts data via the rsa method
        /// </summary>
        /// <param name="text"></param>
        /// <param name="type"></param>
        /// <param name="kid"></param>
        /// <returns></returns>
        public static string Decrypt(string text, string type, int kid)
        {
            var testData = Encoding.UTF8.GetBytes(text);
            using (var rsa = new RSACryptoServiceProvider(2048))
            {

                try
                {
                    // client encrypting data with public key issued by server
                    string publicPrivateKeyXML;
                    switch (type)
                    {
                        case "session":
                            Constants.rsa = GetSessionKey(kid);
                            publicPrivateKeyXML = Constants.rsa.session_private;
                            break;
                        case "request":
                            Constants.rsa = GetRequestKey(kid);
                            publicPrivateKeyXML = Constants.rsa.request_private;
                            break;
                        default:
                            throw new Exception("bad type");
                    }
                    
                    // server decrypting data with private key
                    rsa.FromXmlString(publicPrivateKeyXML);

                    var resultBytes = Convert.FromBase64String(text);
                    var decryptedBytes = rsa.Decrypt(resultBytes, true);
                    var decryptedData = Encoding.UTF8.GetString(decryptedBytes);
                    return decryptedData;

                }
                catch (Exception e)
                {
                    Log.LogRecord("rsa", e.Message);
                    return "";
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }

            }
        }
    }
}
