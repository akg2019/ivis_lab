﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;

namespace Honeysuckle.Models
{
    public class Driver
    {
        
        public int DriverId { get; set; }
        public string ediid { get; set; }
        public Person person { get; set; }
        public string licenseno { get; set; }
        public DateTime dateissued { get; set; }
        public DateTime expirydate { get; set; }
        public string licenseclass { get; set; }

        public string relation { get; set; } //??? why here???


        public Driver(){}

        public Driver(int DriverId)
        {
            this.DriverId = DriverId;
        }

        public Driver (int Id, Person person)
        {
            this.DriverId = Id;
            this.person = person;
        }

        public Driver(Person person)
        {
            this.person = person;
        }

        public Driver(Person person, string relation)
        {
            this.person = person;
            this.relation = relation;
        }

        public Driver(Person person, string licenseno, DateTime dateissued, DateTime expirydate, string licenseclass)
        {
            this.person = person;
            this.licenseno = licenseno;
            this.dateissued = dateissued;
            this.expirydate = expirydate;
            this.licenseclass = licenseclass;
        }

        public Driver(int DriverId, Person person, string licenseno, DateTime dateissued, DateTime expirydate, string licenseclass)
        {
            this.DriverId = DriverId;
            this.person = person;
            this.licenseno = licenseno;
            this.dateissued = dateissued;
            this.expirydate = expirydate;
            this.licenseclass = licenseclass;
        }

    }

    public class DriverModel
    {
        /// <summary>
        /// this function returns the details of a driver
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static Driver GetDriverDetails(Driver driver)
        {
            sql sql = new sql();
            if (sql.ConnectSQL() && driver.DriverId != 0)
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetDriverDetails " + driver.DriverId, "read", "driver", driver);
                while (reader.Read())
                {

                    DateTime issue = new DateTime(); DateTime expiry = new DateTime();
                    if (DBNull.Value != reader["issued"]) issue = DateTime.Parse(reader["issued"].ToString());
                    if (DBNull.Value != reader["expiry"]) expiry = DateTime.Parse(reader["expiry"].ToString());

                    driver = new Driver(driver.DriverId,
                                        new Person(int.Parse(reader["PeopleID"].ToString()), reader["fname"].ToString(), reader["lname"].ToString(), reader["trn"].ToString()),
                                        reader["license"].ToString(),
                                        issue,
                                        expiry,
                                        reader["class"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return driver;
        }

        /// <summary>
        /// This function adds a new driver to the system, and returns the driver Id
        /// </summary>
        /// <param name="PersonId">Employee Id</param>
        /// <returns>Driver Id</returns>
        public static int addDriver(int PersonId)
        {
            return addDriver(PersonId, Constants.user.ID);
        }

        /// <summary>
        /// This function adds a new driver to the system. This version is for users not logged in
        /// through the web interface (i.e. JSON)
        /// </summary>
        /// <param name="PersonId"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static int addDriver(int PersonId, int uid)
        {
            int driverId = 0;
            Driver driver = new Driver();
            driver.person = PersonModel.GetPerson(PersonId);
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                //adding a driver
                SqlDataReader reader = sql.QuerySQL("EXECUTE AddDriver " + PersonId + "," + uid, "create", "driver", driver);

                while (reader.Read())
                {
                    driverId = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return driverId;
        }

        /// <summary>
        /// This function adds a driver to the system that is used by the web services import module.
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="uid"></param>
        public static void addDriver(Driver driver, int uid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE AddDriver " + driver.person.PersonID + ","
                                                    + uid + ",'"
                                                    + driver.dateissued.ToString("yyyy-MM-dd hh:mm:ss") + "','"
                                                    + driver.expirydate.ToString("yyyy-MM-dd hh:mm:ss") + "','"
                                                    + Constants.CleanString(driver.licenseclass) + "','"
                                                    + Constants.CleanString(driver.licenseno) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "create", "driver", driver, uid);
                while (reader.Read())
                {
                    driver.DriverId = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function checks if a driver exists in the system, given the person's TRN
        /// </summary>
        /// <param name="Risk">Risk object</param>
        /// <returns>Driver Id</returns>
        public static int getDriver(Vehicle Risk)
        {
            bool driverExists = false;
            Driver d = new Driver();
            sql sql = new sql();
            if (sql.ConnectSQL() && Risk.mainDriver != null && Risk.mainDriver.person != null && Risk.mainDriver.person.TRN != null && Risk.mainDriver.person.TRN != "")
            {
                //testing to see if the person is a driver
                SqlDataReader reader = sql.QuerySQL("EXECUTE GetDriver '" + Constants.CleanString(Risk.mainDriver.person.TRN) + "'", "read", "driver", new Driver());
                while (reader.Read())
                {
                    driverExists = (bool)reader["result"];
                    if (driverExists) d.DriverId = int.Parse(reader["driverID"].ToString());
                    else d.DriverId = 0; //driver Id is set to 0 if the person is not a driver in the system
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return d.DriverId;
        }

        /// <summary>
        /// This function checks if a driver exists in the system, given the person Id 
        /// </summary>
        /// <param name="perId">Person Id</param>
        /// <param name="type">Driver type (authorized, excepted or excluded)</param>
        /// <param name="polId">Policy Id</param>
        /// <param name="vehId">Risk Id</param>
        /// <returns>Driver Id</returns>
        public static Driver getDriverFromPersonId(int perId, int type, int vehId, int polId)
        {
            Driver driver = new Driver();
            Person person = new Person();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetDriverFromPerId " + perId + "," + type + "," + vehId + "," + polId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "driver", new Driver());
                while (reader.Read())
                {
                    //result = true;
                    driver = new Driver(new Person(perId, reader["fname"].ToString(), reader["mname"].ToString(), reader["lname"].ToString(),
                                          null, null, reader["TRN"].ToString()), reader["relation"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return driver;
        }

        /// <summary>
        /// this function retrieves a vehicle's authorized drivers under a policy
        /// </summary>
        /// <param name="risk"></param>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static List<Driver> getAuthorizedDrivers(Vehicle risk, Policy policy)
        {
            List<Driver> drivers = new List<Driver>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetAuthorizedDrivers " + policy.ID + "," + risk.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "driver", new Driver());
                while (reader.Read())
                {
                    drivers.Add(new Driver(new Person(reader["trn"].ToString())));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return drivers;
        }

        /// <summary>
        /// This function returns a driver based ona  driver id
        /// </summary>
        /// <param name="driver_id"></param>
        /// <returns></returns>
        public static Driver getDriver(int driver_id)
        {
            Driver driver = new Driver();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetDriverByID " + driver_id;
                SqlDataReader reader = sql.QuerySQL(query, "read", "driver", new Driver());
                while (reader.Read())
                {
                    driver = new Driver(driver_id, new Person(int.Parse(reader["pid"].ToString()), reader["fname"].ToString(), reader["lname"].ToString(), reader["trn"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return driver;
        }


        /// <summary>
        /// this function retreives a driver from a person id; used in the audit log
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        public static Driver GetDriverFromPersonID(Person person)
        {
            Driver driver = new Driver();
            driver.person = person;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetDriverFromPersonID " + person.PersonID);
                while (reader.Read())
                {
                    driver.DriverId = int.Parse(reader["did"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return driver;
        }


    }

}



 
    


    
