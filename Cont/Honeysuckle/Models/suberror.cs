﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class suberror
    {
        public string type { get; set; }
        public string id { get; set; }
        public string comment { get; set; }
        
        public suberror() { }
        public suberror(string type, string id)
        {
            this.type = type;
            this.id = id;
        }
        public suberror(string type, string id, string comment)
        {
            this.type = type;
            this.id = id;
            this.comment = comment;
        }
        
    }
    public class suberrorModel
    {
        
        /// <summary>
        /// this function stores errors for data analytic purposes in the database
        /// </summary>
        /// <param name="error"></param>
        /// <param name="reqkey"></param>
        /// <param name="id"></param>
        public static void StoreError(suberror error, string reqkey, string id = null)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC StoreError '" + Constants.CleanString(error.type) + "','" + Constants.CleanString(error.id) + "','" + reqkey + "'";
                if (id != null) query += ",'" + id + "'";
                SqlDataReader reader = sql.QuerySQL(query, "create", "suberror", error);
                reader.Close();
                sql.DisconnectSQL();
            }
        }
    }
}