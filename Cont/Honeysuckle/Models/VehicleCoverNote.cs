﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
//using Newtonsoft.Json;

namespace Honeysuckle.Models
{
    public class VehicleCoverNote
    {
        public int ID { get; set; }
        public string EDIID { get; set; }
        public Company company { get; set; }
        public string alterations { get; set; }
        public bool cancelled { get; set; }
        public DateTime cancelledon { get; set; }
        public Person cancelledby { get; set; }
        public string cancelledbyflat { get; set; }
        public string cancelledreason { get; set; }
        public bool approved { get; set; }
        public DateTime approvedon { get; set; }
        public User approvedby { get; set; }
        public DateTime firstprinted { get; set; }
        public DateTime lastprinted { get; set; }
        public Person lastprintedby { get; set; }
        public string lastprintflat { get; set; }
        public string firstprintflat { get; set; }
        public string covernoteid { get; set; }
        public DateTime effectivedate { get; set; }
        public DateTime expirydate { get; set; }
        public int period { get; set; }
        public string endorsementno { get; set; }
        public string covernoteno { get; set; }
        public int printcount { get; set; }
        public int printcountInsurer { get; set; }
        public Vehicle risk { get; set; }
        public int? printlimit { get; set; }
        public string printpaperno { get; set; }
        public int serial { get; set; }
        public bool printable { get; set; }
        public string printedpaperno { get; set; }
        public string limitsofuse { get; set; }
        public TemplateWording wording { get; set; }
        public VehicleCoverNoteGenerated gen { get; set; }
        public Policy Policy { get; set; }
        public Mortgagee mortgagee { get; set; }
        public bool imported { get; set; }
        public DateTime createdOn { get; set; }
        
        public VehicleCoverNote() { }
        public VehicleCoverNote(int ID)
        {
            this.ID = ID;
        }
        public VehicleCoverNote(int ID, DateTime effectivedate, int period, bool cancelled, bool approved, Company company)
        {
            this.ID = ID;
            this.effectivedate = effectivedate;
            this.period = period;
            this.cancelled = cancelled;
            this.approved = approved;
            this.company = company;
        }
        public VehicleCoverNote(int ID, DateTime effectivedate, int period, DateTime expirydate, bool cancelled, bool approved, Company company)
        {
            this.ID = ID;
            this.effectivedate = effectivedate;
            this.expirydate = expirydate;
            this.period = period;
            this.cancelled = cancelled;
            this.approved = approved;
            this.company = company;
        }
        public VehicleCoverNote(int ID, DateTime effectivedate, int period, bool cancelled, bool approved, Company company, string covernoteno)
        {
            this.ID = ID;
            this.effectivedate = effectivedate;
            this.period = period;
            this.cancelled = cancelled;
            this.approved = approved;
            this.company = company;
            this.covernoteno = covernoteno;
        }
        public VehicleCoverNote(int ID, string alterations, DateTime effectivedate, DateTime expirydate, int period, string covernoteno, bool approved, Company company)
        {
            this.ID = ID;
            this.alterations = alterations;
            this.effectivedate = effectivedate;
            this.expirydate = expirydate;
            this.period = period;
            this.covernoteno = covernoteno;
            this.approved = approved;
            this.company = company;
        }
        public VehicleCoverNote(int ID, DateTime effectivedate, DateTime expirydate, string covernoteno, Policy policy)
        {
            this.ID = ID;
            this.effectivedate = effectivedate;
            this.expirydate = expirydate;
            this.covernoteno = covernoteno;
            this.Policy = policy;
        }


    }
    public class VehicleCoverNoteModel
    {

        /// <summary>
        /// this function is used on the view to determine if a cover note can be generated
        /// for a risk on a specific policy
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static bool CanIGenerateCoverNote(Vehicle vehicle, Policy policy)
        {
            bool result = false;
            if (vehicle.ID != 0 && policy.ID != 0)
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    string query = "EXEC CanIGenerateCoverNote " + vehicle.ID + "," + policy.ID;
                    SqlDataReader reader = sql.QuerySQL(query);
                    while (reader.Read())
                    {
                        result = (bool)reader["result"];
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            else result = true;
            return result;
        }

        /// <summary>
        /// this function returns the amount of cover notes that are effective
        /// for a company in a specified month and year.
        /// </summary>
        /// <param name="company"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static int CoverNoteCountEffective(Company company, string month, string year)
        {
            int output = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC CoverNoteCountEffective " + company.CompanyID + ",'" + Constants.CleanString(month) + "','" + Constants.CleanString(year) + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    output = int.Parse(reader["count"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return output;
        }

        /// <summary>
        /// this function returns a list of all the cover notes that are currently effective for a specified company
        /// </summary>
        /// <param name="company">Company in question</param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static List<VehicleCoverNote> CoverNoteEffectiveList(Company company, string month, string year)
        {
            List<VehicleCoverNote> covernotes = new List<VehicleCoverNote>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC CoverNoteCountEffectiveRecords " + company.CompanyID + ",'" + Constants.CleanString(month) + "','" + Constants.CleanString(year) + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    Policy policy = new Policy();
                    policy.insuredby = new Company(int.Parse(reader["ins_comp_id"].ToString()), reader["ins_comp_name"].ToString());
                    policy.compCreatedBy = new Company(int.Parse(reader["bk_comp_id"].ToString()), reader["bk_comp_name"].ToString());
                    covernotes.Add(new VehicleCoverNote(int.Parse(reader["id"].ToString()),
                                                        DateTime.Parse(reader["effective"].ToString()),
                                                        DateTime.Parse(reader["expiry"].ToString()),
                                                        reader["covernoteno"].ToString(),
                                                        policy));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return covernotes;
        }

        #region old
        //public static bool CoverNoteHistoryExist(Policy pol, Vehicle veh)
        //{
        //    bool result = false;
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        SqlDataReader reader = sql.QuerySQL("EXEC CoverNoteHistoryExist " + pol.ID + "," + veh.ID);
        //        while (reader.Read())
        //        {
        //            result = (bool)reader["result"];
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return result;
        //}
        #endregion

        /// <summary>
        /// this function retrieves  vehicle's cover note history 
        /// under a specified policy or for any policy
        /// </summary>
        /// <param name="veh">vehicle object</param>
        ///  /// <param name="pol">policy object (default:null)</param>
        /// <returns></returns>
        public static List<VehicleCoverNote> CoverNoteHistory(Vehicle veh,Policy pol = null)
        {
            List<VehicleCoverNote> covers = new List<VehicleCoverNote>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                if (pol == null) { pol = new Policy(); pol.ID = 0; }
                string query = "EXEC CoverNoteHistory " + veh.ID + "," + pol.ID;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    VehicleCoverNote vcn = new VehicleCoverNote(int.Parse(reader["id"].ToString()),
                                                                 DateTime.Parse(reader["effective"].ToString()),
                                                                 int.Parse(reader["period"].ToString()),
                                                                 DateTime.Parse(reader["expiry"].ToString()),
                                                                 (bool)reader["cancel"],
                                                                 (bool)reader["approve"],
                                                                 new Company(int.Parse(reader["cid"].ToString()), reader["cname"].ToString()));
                    vcn.covernoteid = reader["covernoteid"].ToString();
                    if (DBNull.Value != reader["first"]) vcn.firstprinted = DateTime.Parse(reader["first"].ToString());
                    covers.Add(vcn);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return covers;
        }
        
        /// <summary>
        /// this function returns all the cover notes effective in a time range
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<VehicleCoverNote> CoverSearchCoverNotes(DateTime start, DateTime end, User user)
        {
            List<VehicleCoverNote> covers = new List<VehicleCoverNote>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC CoverSearchCoverNotes '" + start.ToString("yyyy-MM-dd HH:mm:ss") + "','" + end.ToString("yyyy-MM-dd HH:mm:ss") + "'," + user.ID;
                SqlDataReader reader = sql.QuerySQL(query);
                while(reader.Read())
                {
                    VehicleCoverNote cover = new VehicleCoverNote();
                    cover.risk = new Vehicle();
                    cover.gen = new VehicleCoverNoteGenerated();

                    cover.EDIID = reader["ediid"].ToString(); 
                    cover.alterations = reader["alterations"].ToString();
                    cover.effectivedate = DateTime.Parse(reader["effective"].ToString());
                    cover.expirydate = DateTime.Parse(reader["expiry"].ToString());
                    cover.period = int.Parse(reader["period"].ToString());
                    cover.endorsementno = reader["endorsementno"].ToString();
                    cover.limitsofuse = reader["limits"].ToString();
                    cover.covernoteno = reader["covernoteno"].ToString();
                    
                    cover.risk.ID = int.Parse(reader["risk_id"].ToString());
                    cover.risk.chassisno = reader["chassis"].ToString();
                    
                    cover.Policy = new Policy(int.Parse(reader["polid"].ToString()));

                    #region optional stuff
                    if (!(DBNull.Value == reader["covernoteid"])) cover.covernoteid = reader["covernoteid"].ToString(); 
                    if (!(DBNull.Value == reader["cancelled"]))  cover.cancelled = (bool)reader["cancelled"]; 
                    if (!(DBNull.Value == reader["cancelledbyfname"]) && !(DBNull.Value == reader["cancelledbylname"])) cover.cancelledby = new Person(reader["cancelledbyfname"].ToString(), reader["cancelledbylname"].ToString(), "");
                    if (!(DBNull.Value == reader["cancelledreason"])) cover.cancelledreason = reader["cancelledreason"].ToString();
                    if (!(DBNull.Value == reader["cancelledon"])) cover.cancelledon = DateTime.Parse(reader["cancelledon"].ToString());
                    if (!(DBNull.Value == reader["firstprinted"])) cover.firstprinted = DateTime.Parse(reader["firstprinted"].ToString());
                    if (!(DBNull.Value == reader["lastprinted"])) cover.lastprinted = DateTime.Parse(reader["lastprinted"].ToString());
                    if (!(DBNull.Value == reader["lastprintedbytrn"])) cover.lastprintedby = new Person(reader["lastprintedbytrn"].ToString());
                    if (!(DBNull.Value == reader["printcount"])) cover.printcount = int.Parse(reader["printcount"].ToString());
                    if (!(DBNull.Value == reader["printedpaperno"])) cover.printedpaperno = reader["printedpaperno"].ToString();
                    if (!(DBNull.Value == reader["risk_item_no"])) cover.risk.risk_item_no = int.Parse(reader["risk_item_no"].ToString());
                    if (!(DBNull.Value == reader["mortgagees"])) cover.gen.mortgagees = reader["mortgagees"].ToString();
                    if (!(DBNull.Value == reader["limitsofuse"])) cover.gen.limitsofuse = reader["limitsofuse"].ToString();
                    #endregion
                    
                    covers.Add(cover);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            foreach(VehicleCoverNote cover in covers)
            {
                cover.risk.AuthorizedDrivers = DriverModel.getAuthorizedDrivers(cover.risk, cover.Policy);
            }
            return covers;
        }

        /// <summary>
        /// this function generates a cover note no for the cover note
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public static string GenerateCoverNoteNo(Company company)
        {
            string result = null;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GenerateCoverNoteNo " + company.CompanyID;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = reader["result"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }
        #region old
        /// <summary>
        /// this function returns an abridged cover note
        /// </summary>
        /// <param name="i"></param>
        /// <param name="cmp"></param>
        /// <returns></returns>
        //private static VehicleCoverNote GetShortCoverNote(int i, int cmp = 0)
        //{
        //    VehicleCoverNote vcn = new VehicleCoverNote(i);
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        string query = "EXEC GetShortCoverNote " + i;
        //        if (cmp != 0) query = query + "," + cmp;
        //        SqlDataReader reader = sql.QuerySQL(query);
        //        while (reader.Read())
        //        {
        //            vcn = new VehicleCoverNote(i,
        //                                       reader["alterations"].ToString(),
        //                                       DateTime.Parse(reader["effective"].ToString()),
        //                                       DateTime.Parse(reader["expiry"].ToString()),
        //                                       int.Parse(reader["period"].ToString()),
        //                                       reader["covernoteno"].ToString(),
        //                                       (bool)reader["approved"],
        //                                       new Company(reader["CompanyName"].ToString()));
        //            vcn.Policy = new Policy(int.Parse(reader["policy_id"].ToString()));
        //            vcn.Policy.insuredby = new Company(reader["insured_by_comp_name"].ToString());
        //            vcn.risk = new Vehicle(int.Parse(reader["risk_id"].ToString()));
        //            vcn.imported = (bool)reader["imported"];
        //            vcn.approved = (bool)reader["approved"];
        //            vcn.cancelled = (bool)reader["cancelled"];
        //            vcn.covernoteid = reader["covernoteid"].ToString();
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return vcn;
        //}

        /// <summary>
        /// this function returns a list of abridged cover notes
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="cmp"></param>
        /// <returns></returns>
        //public static List<VehicleCoverNote> GetShortCoverNote(List<int> ids, int cmp = 0)
        //{
        //    List<VehicleCoverNote> VehicleCoverNotes = new List<VehicleCoverNote>();
        //    foreach (int i in ids)
        //    {
        //        VehicleCoverNote v = GetShortCoverNote(i, cmp);
        //        if (v.company != null)
        //        VehicleCoverNotes.Add(v);
        //    }

        //    return VehicleCoverNotes;
        //}

        /// <summary>
        /// this function queries the database to find if a PPN is unique
        /// </summary>
        /// <param name="ppn">PPN to be tested</param>
        /// <returns>true if the PPN doesn't exist in the system</returns>
        //public static bool IsPrintedPaperNoUnique(string ppn)
        //{
        //    bool result = false;
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        SqlDataReader reader = sql.QuerySQL("EXEC IsPrintedPaperNoUnique '" + Constants.CleanString(ppn) + "'");
        //        while (reader.Read())
        //        {
        //            result = (bool)reader["result"];
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return result;
        //}
        
        /// <summary>
        /// this functions sets the state of all active 
        /// </summary>
        /// <param name="Risk">Risk object with valid ID in the database</param>
        //public static void CancelActiveCoverNotes(Vehicle Risk)
        //{
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        SqlDataReader reader = sql.QuerySQL("EXEC CancelActiveCoverNotes " + Risk.ID + "," + Constants.user.ID);
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    else
        //    {
        //        //throw event error
        //    }
        //}

        /// <summary>
        /// returns all Cover Notes in the system
        /// </summary>
        /// <returns></returns>
        //public static List<VehicleCoverNote> GetAllVehicleCoverNotes()
        //{
        //    List<VehicleCoverNote> covernotes = new List<VehicleCoverNote>();
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        string query = "EXEC GetAllVehicleCoverNotes";
        //        SqlDataReader reader = sql.QuerySQL(query);
        //        VehicleCoverNote cn = new VehicleCoverNote();
        //        while (reader.Read())
        //        {
        //            cn.ID = int.Parse(reader["ID"].ToString());
        //            cn.risk = new Vehicle(int.Parse(reader["vehID"].ToString()), reader["VIN"].ToString());
        //            cn.effectivedate = DateTime.Parse(reader["effectivedate"].ToString());
        //            cn.period = int.Parse(reader["period"].ToString());
        //            cn.serial = int.Parse(reader["serial"].ToString());

        //            covernotes.Add(cn);
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return covernotes;
        //}

        /// <summary>
        /// Returns all Cover Notes in the system, belonging to a particular company
        /// </summary>
        /// <returns></returns>
        //public static List<VehicleCoverNote> GetCompVehicleCoverNotes()
        //{
        //    List<VehicleCoverNote> covernotes = new List<VehicleCoverNote>();
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {

        //        SqlDataReader reader = sql.QuerySQL("EXEC GetAllCompVehicleCoverNotes " + UserModel.GetUser(Constants.user.ID).employee.company.CompanyID);

        //        VehicleCoverNote cn = new VehicleCoverNote();
        //        while (reader.Read())
        //        {
        //            cn.ID = int.Parse(reader["ID"].ToString());
        //            cn.risk = new Vehicle(int.Parse(reader["vehID"].ToString()), reader["VIN"].ToString());
        //            cn.effectivedate = DateTime.Parse(reader["effectivedate"].ToString());
        //            cn.period = int.Parse(reader["period"].ToString());
        //            cn.serial = int.Parse(reader["serial"].ToString());

        //            covernotes.Add(cn);
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return covernotes;
        //}
        #endregion
        /// <summary>
        /// Returns all Cover Notes in the system, belonging to a particular company only -not made by intermediary
        /// </summary>
        /// <returns></returns>
        public static List<VehicleCoverNote> GetMyCoverNotes(int id =0)
        {
            List<VehicleCoverNote> covernotes = new List<VehicleCoverNote>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCompCoverNotesOnly " + id;
                SqlDataReader reader = sql.QuerySQL(query);
                VehicleCoverNote cn = new VehicleCoverNote();
                while (reader.Read())
                {
                    cn.ID = int.Parse(reader["ID"].ToString());
                    cn.risk = new Vehicle(int.Parse(reader["vehID"].ToString()), reader["VIN"].ToString());
                    cn.effectivedate = DateTime.Parse(reader["effectivedate"].ToString());
                    cn.period = int.Parse(reader["period"].ToString());
                    //cn.serial = int.Parse(reader["serial"].ToString());

                    cn.covernoteid = reader["CoverNoteID"].ToString();
                    cn.expirydate = DateTime.Parse(reader["ExpiryDateTime"].ToString());
                    cn.Policy = new Policy(int.Parse(reader["PolicyID"].ToString()));
                   // cn.approved = (bool)reader["approved"];
                    //cn.cancelled = (bool)reader["Cancelled"];
                    cn.imported = (bool)reader["imported"];
                    cn.Policy.insuredby = CompanyModel.GetCompany(int.Parse(reader["CompanyID"].ToString()));

                    covernotes.Add(cn);
                    cn = new VehicleCoverNote();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return covernotes;
        }

        /// <summary>
        /// Returns all Cover Notes in the system, belonging to a particular company, and created by a particular intermediate company
        /// </summary>
        /// <returns></returns>
        public static List<VehicleCoverNote> GetIntermediaryCoverNotes(int id)
        {
            List<VehicleCoverNote> covernotes = new List<VehicleCoverNote>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCompIntermediaryCoverNotes " + UserModel.GetUser(Constants.user.ID).employee.company.CompanyID + "," + id;
                SqlDataReader reader = sql.QuerySQL(query);

                VehicleCoverNote cn = new VehicleCoverNote();
                while (reader.Read())
                {
                    cn.ID = int.Parse(reader["ID"].ToString());
                    cn.risk = new Vehicle(int.Parse(reader["vehID"].ToString()), reader["VIN"].ToString());
                    cn.effectivedate = DateTime.Parse(reader["effectivedate"].ToString());
                    cn.period = int.Parse(reader["period"].ToString());
                    cn.serial = int.Parse(reader["serial"].ToString());

                    covernotes.Add(cn);
                    cn = new VehicleCoverNote();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return covernotes;
        }
        #region old
        /// <summary>
        /// this function tests as to whether a cover note can be printed
        /// </summary>
        /// <param name="CovernoteID">A cover note object with a valid ID in the database</param>
        /// <returns>boolean</returns>
        //public static bool IsCoverNotePrintable(int CovernoteID, int InsurerID)
        //{
        //    bool result = false;
        //    int compID;

        //    if (Constants.user.newRoleMode) compID = Constants.user.tempUserGroup.company.CompanyID;
        //    else compID = CompanyModel.GetMyCompany(Constants.user).CompanyID;

        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        SqlDataReader reader = sql.QuerySQL("EXEC IsCoverNotePrintable " + CovernoteID +"," + compID +"," + InsurerID);
        //        while (reader.Read())
        //        {
        //            result = (bool)reader["result"];
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return result;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="covernote"></param>
        //public static void TogglePrintable(VehicleCoverNote covernote)
        //{
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        SqlDataReader reader = sql.QuerySQL("EXEC TogglePrintable " + covernote.ID + "," + Constants.user.ID);
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //}

        /// <summary>
        /// this returns a list of cover notes that are currently
        /// pending approval in the system
        /// </summary>
        /// <returns></returns>
        //public static List<VehicleCoverNote> GetAllPendingCoverNotes()
        //{
        //    List<VehicleCoverNote> covernotes = new List<VehicleCoverNote>();
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        SqlDataReader reader = sql.QuerySQL("EXEC GetAllPendingCoverNotes " + Constants.user.ID);
        //        VehicleCoverNote cn = new VehicleCoverNote();
        //        while (reader.Read())
        //        {
        //            cn.ID = int.Parse(reader["ID"].ToString());
        //            cn.risk = new Vehicle(int.Parse(reader["vehID"].ToString()), reader["VIN"].ToString());
        //            cn.effectivedate = DateTime.Parse(reader["effectivedate"].ToString());
        //            cn.period = int.Parse(reader["period"].ToString());

        //            covernotes.Add(cn);
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return covernotes;
        //}
        #endregion
        /// <summary>
        /// This function tests as to whether there is an active cover note currently existing
        /// </summary>
        /// <param name="Risk">A Risk object with a valid ID</param>
        /// <returns>cover note id of current active cover note of Risk</returns>
        public static bool IsActiveCoverNoteExist(int RiskId, int PolId)
        {
            bool result = false;
            if (RiskId != 0)
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    string query = "EXEC IsActiveCoverNoteExist " + RiskId + "," + PolId;
                    SqlDataReader reader = sql.QuerySQL(query);
                    while (reader.Read())
                    {
                        result = (bool)reader["result"];
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            return result;
        }

        /// <summary>
        /// this function marks a cover note as 'pending' in the database awaiting for it to be approved on effectivedate
        /// </summary>
        /// <param name="covernote">a cover note object with a valid cover note id</param>
        public static void ApproveCoverNote(VehicleCoverNote covernote)
        {
            ApproveCoverNote(covernote, Constants.user.ID);
        }

        /// <summary>
        /// this function marks a cover note as approved
        /// </summary>
        /// <param name="covernote"></param>
        /// <param name="uid"></param>
        public static void ApproveCoverNote(VehicleCoverNote covernote, int uid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "ApproveCoverNote " + covernote.ID + "," + uid;
                SqlDataReader reader = sql.QuerySQL(query, "update", "vehiclecovernote", covernote);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function marks a cover note as 'cancelled' in the database
        /// </summary>
        /// <param name="covernote">cover note</param>
        public static void CancelCoverNote(VehicleCoverNote covernote)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "CancelCoverNote " + covernote.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "vehiclecovernote", covernote);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function is used to test if a certificate is active
        /// </summary>
        /// <param name="CertId">Certificate Id- of certificate to be tested</param>
        /// <returns>boolean value</returns>
        public static bool IsCoverNoteActive(int CovernoteID)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC IsCoverNoteActive " + CovernoteID;
                SqlDataReader reader = sql.QuerySQL(query);

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;

        }

        /// <summary>
        /// this function adds a cover note to a vehicle on a policy
        /// </summary>
        /// <param name="covernote"></param>
        /// <param name="polid"></param>
        /// <param name="compid"></param>
        /// <param name="newitem"></param>
        /// <param name="import"></param>
        /// <returns></returns>
        public static int AddVehicleCoverNote(VehicleCoverNote covernote, int polid = 0, int compid = 0, bool newitem= false, bool import = false)
        {
            return AddVehicleCoverNote(covernote, Constants.user.ID, polid, compid, newitem, import);
        }

        /// <summary>
        /// this function adds a cover note to the system
        /// </summary>
        /// <param name="covernote"></param>
        /// <returns></returns>
        public static int AddVehicleCoverNote(VehicleCoverNote covernote, int uid, int polid = 0, int compid = 0, bool newitem = false, bool import = false)
        {
            int vcnid = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                int wordid = 0;
                if (covernote.wording != null) wordid = covernote.wording.ID;
                
                string con = null;
                if (covernote.cancelledon != null && covernote.cancelledon != new DateTime()) con = covernote.cancelledon.ToString("yyyy-MM-dd HH:mm:ss");
                if (covernote.cancelledby == null) covernote.cancelledby = new Person();
                if (covernote.lastprintedby == null) covernote.lastprintedby = new Person();
                if (covernote.mortgagee == null) covernote.mortgagee = new Mortgagee();
                if (covernote.covernoteid == null || covernote.covernoteid == "")
                {
                    if (Constants.user.newRoleMode) covernote.covernoteid = VehicleCoverNoteModel.GenerateCoverNoteNo(Constants.user.tempUserGroup.company);
                    else covernote.covernoteid = VehicleCoverNoteModel.GenerateCoverNoteNo(CompanyModel.GetMyCompany(Constants.user));
                }

                string query=  "EXEC AddVehicleCoverNote '"  + Constants.CleanString(covernote.EDIID) + "','"
                                                             + Constants.CleanString(covernote.alterations) + "','"
                                                             + Constants.CleanString(covernote.covernoteid) + "','"
                                                             + covernote.effectivedate.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                             + covernote.expirydate.ToString("yyyy-MM-dd HH:mm:ss") + "',"
                                                             + covernote.period + ",'"
                                                             + Constants.CleanString(covernote.endorsementno) + "','"
                                                             + Constants.CleanString(covernote.covernoteno) + "',"
                                                             + covernote.risk.ID + ",'"
                                                             + covernote.cancelled + "','"
                                                             + Constants.CleanString(covernote.cancelledbyflat) + "',"
                                                             + covernote.cancelledby.PersonID + ",'"
                                                             + Constants.CleanString(covernote.cancelledreason) + "','"
                                                             + Constants.CleanString(con) + "','"
                                                             + Constants.CleanString(covernote.printedpaperno) + "',"
                                                             + covernote.lastprintedby.PersonID + ",'"
                                                             + Constants.CleanString(covernote.lastprintflat) + "',"
                                                             + covernote.printcount + ","
                                                             + wordid + ","
                                                             + polid + ","
                                                             + compid + ","
                                                             + uid + ",'" 
                                                             + newitem + "','"
                                                             + import + "','"
                                                             + Constants.CleanString(covernote.mortgagee.mortgagee) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "create", "vehiclecovernote", covernote);
                while (reader.Read())
                {
                    vcnid = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return vcnid;
        }

        /// <summary>
        /// DONT USE THIS
        /// </summary>
        /// <param name="covernotes"></param>
        /// <param name="polid"></param>
        /// <param name="compid"></param>
        /// <param name="gen"></param>
        public static void AddManyCoverNotes(List<VehicleCoverNote> covernotes, int polid = 0, int compid = 0, bool gen = false)
        {
            AddManyCoverNotes(covernotes, Constants.user.ID, polid, compid, gen);
        }

        /// <summary>
        /// this version is only used for IMPORT
        /// </summary>
        /// <param name="covernotes"></param>
        /// <param name="uid"></param>
        /// <param name="polid"></param>
        /// <param name="compid"></param>
        /// <param name="gen"></param>
        public static void AddManyCoverNotes(List<VehicleCoverNote> covernotes, int uid, int polid = 0, int compid = 0, bool gen = false)
        {
            foreach (VehicleCoverNote vcn in covernotes)
            {
                vcn.ID = AddVehicleCoverNote(vcn, uid, polid, compid, false, true);
                ApproveCoverNote(vcn, uid);
                vcn.Policy = PolicyModel.getPolicyFromID(polid);
                VehicleCoverNoteGenerated generatedCert = new VehicleCoverNoteGenerated(vcn.risk.VehicleYear.ToString(), vcn.risk.make,
                                                          vcn.risk.VehicleRegNumber, vcn.risk.extension, "", "",
                                                          vcn.risk.make + " " + vcn.risk.VehicleModel + " " + vcn.risk.modelType,
                                                          vcn.Policy.policyCover.cover, vcn.risk.modelType, vcn.risk.VehicleModel,
                                                          vcn.gen.authorizedwording, vcn.gen.mortgagees, vcn.limitsofuse, vcn.risk.usage.usage, vcn.Policy.policyNumber,
                                                          vcn.Policy.mailingAddress.roadnumber + " " + vcn.Policy.mailingAddress.road.RoadName + " " + vcn.Policy.mailingAddress.roadtype.RoadTypeName 
                                                          + ", " +vcn.Policy.mailingAddress.city.CityName + ", "  + vcn.Policy.mailingAddress.parish.parish + ", " + vcn.Policy.mailingAddress.country.CountryName, 
                                                          vcn.covernoteid, vcn.period.ToString(), vcn.Policy.startDateTime.ToString("H:mm tt"), vcn.Policy.endDateTime.ToString("H:mm tt"),
                                                          vcn.Policy.startDateTime.ToString("MMMM dd, yyyy"), vcn.Policy.endDateTime.ToString("MMMM dd, yyyy"),
                                                          "", vcn.risk.bodyType, vcn.risk.seating.ToString(), vcn.risk.chassisno,
                                                          "", "", "",vcn.risk.engineNo, vcn.risk.HPCCUnitType);


                VehicleCoverNoteGeneratedModel.InsertGenCoverNote(generatedCert, vcn.ID, uid);
            }
        }

        /// <summary>
        /// this function tests if the manual cover note no has been previously used in the system
        /// </summary>
        /// <param name="cn"></param>
        /// <returns></returns>
        public static bool TestManualCoverNoteNo(VehicleCoverNote cn)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "TestManualCoverNoteNo '" + Constants.CleanString(cn.covernoteno) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernote", new VehicleCoverNote());
                if (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        
        /// <summary>
        /// this function tests if the risk in question is already 
        /// under certificate, cover note or exceeded the amount of
        /// cover notes that can be generated
        /// </summary>
        /// <param name="covernotes"></param>
        /// <returns></returns>
        public static List<Vehicle> TestCoverNotesPossible(List<VehicleCoverNote> covernotes)
        {
            List<Vehicle> badvehs = new List<Vehicle>();
            for (int i = 0; i < covernotes.Count(); i++ )
            {
                if (!(PolicyModel.IsVehicleCovered(covernotes[i].risk.ID, covernotes[i].effectivedate, covernotes[i].effectivedate.AddDays((double)covernotes[i].period))
                    && IsCoverNoteExistNow(covernotes[i].risk.ID, covernotes[i].effectivedate, covernotes[i].effectivedate.AddDays((double)covernotes[i].period))))
                {
                    covernotes.RemoveAt(i);
                    badvehs.Add(covernotes[i].risk);
                }
            }
            //return covernotes;
            return badvehs;
        }

        /// <summary>
        /// this function tests whether a cover note exists in a specific time range
        /// </summary>
        /// <param name="vehicleid"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static bool IsCoverNoteExistNow(int vehicleid, DateTime start, DateTime end)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsCoverNoteExistNow " + vehicleid + ",'" + start.ToString("yyyy-MM-dd HH:mm:ss") + "','" + end.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernote", new VehicleCoverNote());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function tests if vehicle is under an active note now
        /// </summary>
        /// <param name="vehicleid"></param>
        /// <returns></returns>
        public static bool IsVehUnderActiveNote(int vehicleid, int PolId = 0) 
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsVehUnderActiveNote " + vehicleid + "," + PolId ;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernote", new VehicleCoverNote());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function tests if creating a cover note now is possible
        /// </summary>
        /// <param name="vcn"></param>
        /// <returns></returns>
        public static bool TestCoverNotePossible(VehicleCoverNote vcn)
        {
            return (PolicyModel.IsVehicleCovered(vcn.risk.ID, vcn.effectivedate, vcn.expirydate) && !IsCoverNoteExistNow(vcn.risk.ID, vcn.effectivedate, vcn.expirydate));
        }

        /// <summary>
        /// this tests if you can print a cover note
        /// </summary>
        /// <param name="vcn">a cover note with a valid id</param>
        /// <returns></returns>
        public static bool CanIPrintCoverNote(VehicleCoverNote vcn)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC CanIPrintCoverNote " + vcn.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernote", new VehicleCoverNote());
                while (reader.Read())
                {
                    int companyPrintLimit = int.Parse(reader["CoverNotePrintLimit"].ToString());
                    int printLimitOverride = int.Parse(reader["PrintLimitOverride"].ToString());
                    int printCount = int.Parse(reader["PrintCount"].ToString());

                    if (companyPrintLimit == 0) result = true;
                    else
                    {
                        if (printLimitOverride == 0) result = printCount < companyPrintLimit;
                        else result = printCount < printLimitOverride;
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this is a function that registers a print by a user in the system
        /// </summary>
        /// <param name="vcn">a cover note object with a valid id</param>
        /// <returns></returns>
        public static bool PrintCoverNote(VehicleCoverNote vcn)
        {
            bool result = false;
            string LastPrintedByName = Constants.user.employee.person.lname + ", " + Constants.user.employee.person.fname;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC PrintCoverNote " + vcn.ID + "," + Constants.user.ID + ",'" + Constants.CleanString(LastPrintedByName) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "update", "vehiclecovernote", vcn);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function returns a Risk id if the manually entered Cover Note No already exists
        /// </summary>
        /// <param name="covernoteno">integer</param>
        /// <returns>Risk id</returns>
        public static VehicleCoverNote GetVehicleCoverNoteByNumber(int id)
        {
            VehicleCoverNote vcn = new VehicleCoverNote();
            Person per = new Person();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "GetVehicleCoverNoteByNumber " + id;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernote", new VehicleCoverNote());
                while (reader.Read())
                {
                    vcn.alterations = reader["Alterations"].ToString();
                    vcn.company = new Company(int.Parse(reader["CompanyID"].ToString()), reader["CompanyName"].ToString());
                    vcn.cancelled = (bool)reader["Cancelled"];
                    //if (DBNull.Value != reader["CancelledBy"]) vcn.cancelledby = new Person(int.Parse(reader["CancelledBy"].ToString()));
                    if (DBNull.Value != reader["CancelledOn"]) vcn.cancelledon = DateTime.Parse(reader["CancelledOn"].ToString());
                    if (DBNull.Value != reader["CoverNoteID"]) vcn.covernoteid = reader["CoverNoteID"].ToString();
                    vcn.effectivedate = DateTime.Parse(reader["EffectiveDate"].ToString());
                    vcn.expirydate = DateTime.Parse(reader["ExpiryDateTime"].ToString());
                    vcn.period = int.Parse(reader["period"].ToString());
                    vcn.endorsementno = reader["EndorsementNo"].ToString();
                    vcn.printcount = int.Parse(reader["PrintCount"].ToString());
                    vcn.printcountInsurer = int.Parse(reader["PrintCountInsurer"].ToString());
                    
                    vcn.approved = (bool)reader["approved"];
                    vcn.cancelled = (bool)reader["cancelled"];
                    vcn.covernoteno = reader["covernoteno"].ToString();
                    if (DBNull.Value != reader["LastPrinted"]) vcn.lastprinted = DateTime.Parse(reader["LastPrinted"].ToString());
                    if (DBNull.Value != reader["DateFirstPrinted"]) vcn.firstprinted = DateTime.Parse(reader["DateFirstPrinted"].ToString());
                    vcn.risk = new Vehicle(int.Parse(reader["VehicleID"].ToString()), reader["VIN"].ToString());
                    vcn.Policy = new Policy(int.Parse(reader["PolicyID"].ToString()), new PolicyCover(reader["Cover"].ToString()), reader["Policyno"].ToString());
                    vcn.Policy.imported = (bool)reader["imported"];

                    vcn.risk = new Vehicle();
                    vcn.risk.ID = int.Parse(reader["VehicleID"].ToString());
                    if (DBNull.Value != reader["VIN"]) vcn.risk.VIN = reader["VIN"].ToString();
                    if (DBNull.Value != reader["ChassisNo"])  vcn.risk.chassisno = reader["ChassisNo"].ToString();
                    if (DBNull.Value != reader["Make"]) vcn.risk.make = reader["Make"].ToString();
                    if (DBNull.Value != reader["Model"]) vcn.risk.VehicleModel = reader["Model"].ToString();
                    if (DBNull.Value != reader["VehicleYear"]) vcn.risk.VehicleYear = int.Parse(reader["VehicleYear"].ToString());
                    if (DBNull.Value != reader["usage"]) vcn.risk.usage = new Usage (reader["usage"].ToString());
                    if (DBNull.Value != reader["usage"]) vcn.risk.VehicleRegNumber = reader["VehicleRegistrationNo"].ToString();
                    if (DBNull.Value != reader["EngineNo"]) vcn.risk.engineNo = reader["EngineNo"].ToString();
                    if (DBNull.Value != reader["Seating"]) vcn.risk.seating = int.Parse(reader["Seating"].ToString());
                    if (DBNull.Value != reader["HPCCUnitType"]) vcn.risk.HPCCUnitType = reader["HPCCUnitType"].ToString();
                    if (DBNull.Value != reader["mortgagee"]) vcn.mortgagee = new Mortgagee(reader["mortgagee"].ToString());
                    if (DBNull.Value != reader["LastPrintedByFlat"]) vcn.lastprintflat = reader["LastPrintedByFlat"].ToString();
                    if (DBNull.Value != reader["FirstPrintedByFlat"]) vcn.firstprintflat = reader["FirstPrintedByFlat"].ToString();
                    if (DBNull.Value != reader["EstimatedValue"]) vcn.risk.estimatedValue = reader["EstimatedValue"].ToString();
                    if (DBNull.Value != reader["BodyType"]) vcn.risk.bodyType = reader["BodyType"].ToString();

                    if (DBNull.Value != reader["ReferenceNo"]) vcn.risk.referenceNo = reader["ReferenceNo"].ToString();

                    //if (DBNull.Value != reader["WordingTemplate"]) vcn.wording = new TemplateWording(int.Parse(reader["WordingTemplate"].ToString()));
                    if (DBNull.Value != reader["WordingTemplate"]) vcn.wording = TemplateWordingModel.GetWording(int.Parse(reader["WordingTemplate"].ToString()));
                    if (DBNull.Value != reader["PeopleID"]) per.PersonID = int.Parse(reader["PeopleID"].ToString());
                    if (DBNull.Value != reader["FName"]) per.fname = reader["FName"].ToString();
                    if (DBNull.Value != reader["LName"]) per.lname = reader["LName"].ToString();
                    if (DBNull.Value != reader["TRN"]) per.TRN = reader["TRN"].ToString();
                    

                    vcn.risk.mainDriver = new Driver(per);

                    vcn.ID = id;
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return vcn;
        }

        /// <summary>
        /// this function updates the vehicle cover note data in the database
        /// </summary>
        /// <param name="vcn"></param>
        /// <returns></returns>
        public static bool UpdateVehicleCoverNote(VehicleCoverNote vcn)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC UpdateVehicleCoverNote '"  + Constants.CleanString(vcn.alterations) + "','"
                                                                + vcn.effectivedate.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                                + vcn.expirydate.ToString("yyyy-MM-dd HH:mm:ss") + "',"
                                                                + vcn.period + ",'"
                                                                + Constants.CleanString(vcn.endorsementno) + "','"
                                                                + Constants.CleanString(vcn.covernoteno) + "','"
                                                                + Constants.CleanString(vcn.covernoteid) + "'," 
                                                                + vcn.wording.ID + ","
                                                                + vcn.ID +","+
                                                                + Constants.user.ID;

                SqlDataReader reader = sql.QuerySQL(query, "udpate", "vehiclecovernote", vcn);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;

        }

        /// <summary>
        /// this returns the id of the latest cover note that is currently active for the Risk in the system
        /// </summary>
        /// <param name="Risk">a valid Risk with an ID</param>
        /// <returns>cover note object</returns>
        public static VehicleCoverNote GetCurrentCoverNote(Vehicle Risk)
        {
            VehicleCoverNote covernote = new VehicleCoverNote();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "GetCurrentCoverNote " + Risk.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernote", new VehicleCoverNote());
                while (reader.Read())
                {
                    covernote.ID = int.Parse(reader["VehicleCoverNoteID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return covernote;
        }

        /// <summary>
        /// This function retrieves the vehicle cover note of the vehicle, under a given time frame
        /// </summary>
        public static VehicleCoverNote GetActiveCoverNote(int vehId = 0, string startDate = null, string endDate = null, int polId = 0)
        {
            VehicleCoverNote coverNote = new VehicleCoverNote();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC GetThisActiveCoverNote '" + DateTime.Parse(startDate).ToString("yyyy-MM-dd HH:mm:ss") + "','" + DateTime.Parse(endDate).ToString("yyyy-MM-dd HH:mm:ss") + "'," + vehId + ","+ polId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernote", new VehicleCoverNote());

                while (reader.Read())
                {
                    coverNote = new VehicleCoverNote(int.Parse(reader["CoverID"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return coverNote;
        }

        /// <summary>
        /// this function returns all cover notes in relation to a specified Risk
        /// </summary>
        /// <param name="Risk">a Risk object with a valid ID in the system</param>
        /// <returns>List of Cover Notes</returns>
        public static List<VehicleCoverNote> GetVehiclesCoverNotesHistory(Vehicle Risk)
        {
            List<VehicleCoverNote> covernotes = new List<VehicleCoverNote>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCoverNotesForVehicle " + Risk.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernote", new VehicleCoverNote());
                while (reader.Read())
                {
                    covernotes.Add(new VehicleCoverNote(int.Parse(reader["VehicleCoverNoteID"].ToString()),
                                                        DateTime.Parse(reader["EffectiveDate"].ToString()),
                                                        int.Parse(reader["period"].ToString()),
                                                        (bool)reader["Cancelled"],
                                                        (bool)reader["approved"],
                                                        new Company(int.Parse(reader["CompanyID"].ToString()), reader["CompanyName"].ToString())));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return covernotes;
        }
        
        #region old
        /// <summary>
        /// this fucntion retrieves the current state of a particular cover note
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //public static string GetCoverNoteState(int id)
        //{
        //    string state = "draft";
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        SqlDataReader reader = sql.QuerySQL("GetCoverNoteState " + id);
        //        while (reader.Read())
        //        {
        //            state = reader["state"].ToString();
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return state;
        //}

        
        //public static string MakeCoverNote(Vehicle Risk)
        //{
        //    /*
        //    <[Risk Items]Veh Year> 
        //    <[Risk Items]Veh Make> 
        //    <[Risk Items]Veh Model> 
        //    <[Risk Items]Veh Model Type> 
        //    <[Risk Items]Veh Extension>
        //    <[Risk Items]Veh Reg No>
        //    <[Policy Info]Main Insured> ( indicate all the insureds)
        //    <[Risk Items]Veh Main Driver Name> (indicate who is the main driver)
        //    <[Risk Items]Veh Driver Names> (List all the drivers)
        //    */

        //    Regex vehyr = new Regex("<[Risk Items]Veh Year>");
        //    Regex vehmake = new Regex("<[Risk Items]Veh Make>");
        //    Regex vehmodel = new Regex("<[Risk Items]Veh Model>");
        //    Regex vehmodtype = new Regex("<[Risk Items]Veh Model Type>");
        //    Regex vehext = new Regex("<[Risk Items]Veh Extension>");
        //    Regex insureds = new Regex("<[Policy Info]Main Insured>");
        //    Regex mdriver = new Regex("<[Risk Items]Veh Main Driver Name>");
        //    Regex drivers = new Regex("<[Risk Items]Veh Driver Names>");

        //    Policy p = PolicyModel.GetListofPoliciesWithVehicle(Risk).Where(x => x.endDateTime >= DateTime.Now && x.startDateTime <= DateTime.Now).ElementAt(0);
        //    string names = "";
        //    foreach (Person person in p.insured)
        //    {
        //        names += person.lname + ", " + person.fname + System.Environment.NewLine;
        //    }

        //    string driverlist = "";

        //    foreach (Driver driver in Risk.AuthorizedDrivers)
        //    {
        //        driverlist += driver.person.lname + ", " + driver.person.fname + System.Environment.NewLine;
        //    }

        //    if (Constants.user.employee == null) Constants.user = UserModel.GetUser(Constants.user.ID);
        //    else if (Constants.user.employee.company == null) Constants.user.employee.company = CompanyModel.GetMyCompany(Constants.user);

        //    TemplateWording covernotewording = CompanyModel.GetCoverNoteWordingForCompany(Constants.user.employee.company);


        //    if (covernotewording.ID != 0)
        //    {
        //        //MAKE COVER NOTE
        //    }
        //    else
        //    {
        //        //THROW ERROR THAT ADMIN SHOULD SET TEMPLATE/WORDING
        //    }

           
        //    return "";
        //}

        ///// <summary>
        ///// this function calls function to print cover notes to PDF
        ///// </summary>
        ///// <param name="company">company</param>
        ///// <param name="Risk">Risk</param>
        //public static void PrintCoverNote(Company company, Vehicle Risk)
        //{
        //    switch (company.CompanyName)
        //    {
        //        //TO DEFINE COMPANIES
        //        case "Andrew Made A Company": //TEST DATA
        //            break;
        //        default:
        //            break;
        //    }
        //}

        /// <summary>
        /// this function searches cover notes 
        /// </summary>
        /// <param name="search">search</param>
        /// <returns>list of cover notes</returns>
        //public static List<VehicleCoverNote> CoverNoteGenSearch(string search)
        //{
        //    List<VehicleCoverNote> notes = new List<VehicleCoverNote>();
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        SqlDataReader reader = sql.QuerySQL("EXEC CoverNoteGenSearch '" + Constants.CleanString(search) + "'");
        //        while (reader.Read())
        //        {
        //            notes.Add(new VehicleCoverNote(int.Parse(reader["ID"].ToString()), 
        //                                           DateTime.Parse(reader["start"].ToString()), 
        //                                           int.Parse(reader["period"].ToString()), 
        //                                           (bool)reader["cancelled"], 
        //                                           (bool)reader["approved"], 
        //                                           new Company(reader["companyname"].ToString()),
        //                                           reader["covernoteno"].ToString()));
        //        }
        //        reader.Close();
        //    }
        //    return notes;
        //}
        #endregion

        /// <summary>
        /// This function is used to enable the printing for a particular cover note
        /// </summary>
        /// <param name="covernote_id">Risk Cover note Id</param>
        public static bool EnablePrint(int covernote_id)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXECUTE MakePrintable '" + "CoverNote" + "'," + covernote_id + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "vehiclecovernote", new VehicleCoverNote(covernote_id));

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to request a print for a particular cover note
        /// </summary>
        /// <param name="covernote_id">Risk Cover note Id</param>
        public static bool PrintRequest(int covernote_id)
        {
            bool result = false;
            int compId;

            if (Constants.user.newRoleMode) compId = Constants.user.tempUserGroup.company.CompanyID;
            else compId = CompanyModel.GetMyCompany(Constants.user).CompanyID;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXECUTE PrintRequest " + covernote_id + ",'" + "Covernote" + "'," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "vehiclecovernote", new VehicleCoverNote(covernote_id));

                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to test if a cover note is cancelled
        /// </summary>
        /// <param name="CertId">Cover note Id- of cover note to be tested</param>
        /// <returns>boolean value</returns>
        public static bool IsCoverNoteCancelled(int CovernoteID)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC IsCoverNoteCancelled " + CovernoteID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernote", new VehicleCoverNote());

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;

        }

        /// <summary>
        /// this function retrieves base cover note information for a cover note (ID, Effective Date & Expiry Date)
        /// </summary>
        /// <param name="coverID"></param>
        /// <returns></returns>
        public static VehicleCoverNote GetCoverNo(int coverID)
        {
            VehicleCoverNote vcn = new VehicleCoverNote();
            
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCoverNo " + coverID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernote", new VehicleCoverNote());

                while (reader.Read())
                {
                    vcn.covernoteno = reader["CoverNoteID"].ToString();
                    vcn.effectivedate = DateTime.Parse(reader["EffectiveDate"].ToString());
                    vcn.expirydate = DateTime.Parse(reader["ExpiryDateTime"].ToString());
                }

                reader.Close();
                sql.DisconnectSQL();
            }
            return vcn;

        }

        /// <summary>
        /// This function is used to determine if the mass approval button should be shown on the policy page
        /// </summary>
        /// <param name="policy">Policy Model </param>
        public static bool MassApproveNotesButton(Policy policy = null)
        {
            bool show = false;

            if (policy != null)
                foreach (Vehicle vehicle in policy.vehicles)
                {
                    if (VehicleCoverNoteModel.IsInactiveNoteExists(vehicle.ID, policy.ID))
                    {
                        show = true;
                        return show;
                    }
                }

            return show;
        }

        /// <summary>
        /// this function tests whether a cover note exists in a specific time range
        /// </summary>
        /// <returns> boolean </returns>
        public static bool IsInactiveNoteExists(int vehicleid, int PolId)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsInactiveNoteMade " + vehicleid + ","+ PolId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernote", new VehicleCoverNote());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to determine if the mass create note button should be shown on the policy page
        /// </summary>
        /// <param name="policy">Policy Model </param>
        public static bool MassCreateCoverButton(Policy policy = null)
        {
            bool show = false;

            if (policy != null)
            foreach (Vehicle vehicle in policy.vehicles)
            {
                if (!VehicleCertificateModel.IsVehicleUnderActiveCert(vehicle.ID, policy.startDateTime, policy.endDateTime, policy.ID) && !VehicleCertificateModel.IsPolicyCertMade(vehicle.ID, policy.ID) && VehicleCoverNoteModel.CanIGenerateCoverNote(vehicle, policy))
                {
                    //There only needs to be a min of one vehicle that can potentially have a cover note, to show the button
                    show = true;
                    return show;
                }
            }
            return show;
        }

        /// <summary>
        /// This function is used to determine if the mass print note button should be shown on the policy page
        /// </summary>
        /// <param name="policy">Policy Model </param>
        public static bool MassPrintNotesButton(Policy policy = null)
        {
            bool show = false;

            if (policy != null)
                foreach (Vehicle vehicle in policy.vehicles)
                {
                    if (VehicleCoverNoteModel.IsActiveCoverNoteExist(vehicle.ID, policy.ID))
                    {
                        //There only needs to be a min of one vehicle that can potentially have a cover note, to show the button
                        show = true;
                        return show;
                    }
                }
            return show;
        }

        /// <summary>
        /// Getting a Risk's inactive cover note, given the Risk id and policy id
        /// </summary>
        /// <param name="Risk">Risk object with Risk's information</param>
        /// <returns>The certificate information</returns>
        public static VehicleCoverNote GetThisInactiveNote(int vehicleId, int polId)
        {
            VehicleCoverNote note = new VehicleCoverNote();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetInactiveCoverNote " + polId + "," + vehicleId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernote", new VehicleCertificate());
                while (reader.Read())
                {
                    note = new VehicleCoverNote(int.Parse(reader["NoteID"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return note;
        }

        /// <summary>
        /// Gets the VehicleCovernoteID by using coverID (string)
        /// </summary>
        /// <param name="covernoteid"></param>
        /// <returns></returns>
        public static VehicleCoverNote GetCoverNoteByCoverNoteID(string covernoteid)
        {
            VehicleCoverNote note = new VehicleCoverNote();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCoverNoteByCoverNoteID '" + covernoteid + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecovernote", new VehicleCoverNote());
                while (reader.Read())
                {
                    note = new VehicleCoverNote(int.Parse(reader["VehicleCoverNoteID"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }


            return note;
        }

        public static List<VehicleCoverNote> GetCompanyCoverNotes(int compID, DateTime start, DateTime end, string typeOfDate = null)
        {
            List<VehicleCoverNote> covernotes = new List<VehicleCoverNote>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                if (typeOfDate == null) typeOfDate = "created";
                string query = "EXEC GetCompanyCoverNotes " + compID + ",'" + typeOfDate + "'";
                if ((start == DateTime.Parse("1969-12-31 00:00:00") || (start == DateTime.Parse("1969-12-31 19:00:00")) && (end == DateTime.Parse("1969-12-31 00:00:00") || end == DateTime.Parse("1969-12-31 19:00:00"))))
                {
                    query += ",'" + null + "','" + null + "'";
                }
                else
                    query += ",'" + start.ToString("yyyy-MM-dd HH:mm:ss") + "','" + end.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                
                SqlDataReader reader = sql.QuerySQL(query);
                VehicleCoverNote cn = new VehicleCoverNote();
                while (reader.Read())
                {
                    cn.imported = (bool)reader["ImportedCover"];
                    cn.ID = int.Parse(reader["ID"].ToString());
                    cn.risk = new Vehicle(int.Parse(reader["vehID"].ToString()), reader["VIN"].ToString());
                    cn.risk.VehicleRegNumber = reader["VehicleRegistrationNo"].ToString();
                    cn.effectivedate = DateTime.Parse(reader["effectivedate"].ToString());
                    cn.period = int.Parse(reader["period"].ToString());

                    cn.covernoteid = reader["CoverNoteID"].ToString();
                    cn.expirydate = DateTime.Parse(reader["ExpiryDateTime"].ToString());
                    cn.Policy = new Policy(int.Parse(reader["PolicyID"].ToString()));
                    cn.Policy.insuredby = CompanyModel.GetCompany(int.Parse(reader["CompanyID"].ToString()));

                    covernotes.Add(cn);
                    cn = new VehicleCoverNote();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return covernotes;
        }


        public static void MarkCoverNoteImported(VehicleCoverNote vcn)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC MarkCoverNoteImported " + vcn.ID;
                SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }
    }
}
