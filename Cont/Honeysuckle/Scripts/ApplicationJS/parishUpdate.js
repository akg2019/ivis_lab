﻿$(function () {
    var parish = $("#parish").val();
    if (parish != 0) {
        $("#parish").parents('.editor-field').find('.parishes').children().each(function () {
            if ($(this).val() == parish) {
                $(this).prop('selected', true);
                return;
            }
        });
        $(".parishes").trigger("change");
    }

    $('.parishes').on('change', function () {
        var parish = $(this).find('option:selected').val();
        $('#parish').val(parish);
    });

});