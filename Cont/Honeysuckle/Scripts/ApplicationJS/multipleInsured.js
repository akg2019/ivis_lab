﻿var cid;
var notselected = [];
var selected = [];
var idNumbers = [];
var peopleSelected = [];
var checkIfAdded = 0;
var scrollHeight;
var noInsured = "<div class=\"rowNone\">There Are No Insured. Please add one. </div>";
var InsuredHidden = "<div class=\"rowNone\">Please search the system for existing insureds, or add a new insured. </div>";

//Search when enter-button is hit
$(document).on("keyup", ".peopleSearch", function (event) {
    var text = $(".peopleSearch").val();
    if (event.which == 13) {
        searchData(text);
    }
});

function updateView() {
    for (var x = 0; x < selected.length; x++)  {
        $("#selected-insured").append(selected[x]);
    }
    $("#insureds").children(".insured-row").each(function () {
        //If the person is already on display, in the select box, then it doesn't need to be shown in the search area again 
        // (this is needed for when the modal is called back, and all the insured are called from the database (to not include duplicates)
        testIfAdded($($(this).children(".insured-check").children(".insuredcheck")).prop('id'));
        if (checkIfAdded == 1) $(this).hide();
        checkIfAdded = 0;
    });
    if ($("#insureds").height() + $("#selected-insured").height() < 380) $("#insureds").height($("#insureds").height() + 20);
    else if ($("#insureds").height() + $("#selected-insured").height() >= 380) $("#insureds").height($("#insureds").height() - 20);

    $("#insureds").perfectScrollbar('update');
    $("#selected-insured").perfectScrollbar('update');
}

function testIfAdded(id) {
    for(var z= 0; z< idNumbers.length; z++) {
        if (idNumbers[z] == id) checkIfAdded = 1;
    }
}

function testBasePerson(id) {
    var result = false;
    for (var y = 0; y < selected.length; y++) {
        result = (selected[y].find(".baseid").val() == id);
    }
    return result;
}


function removeELement(id) { //This function removes the element from the arrays
    for (var y = 0; y < selected.length; y++) {
        if (idNumbers[y] == id) {
            selected.splice(y, 1);
            idNumbers.splice(y, 1);
        }
    }
    updateView();
}


function AddPersonSelected(id, addNew) {
    var err = "<div>There was an error with adding the person. Please try again. If the problem persists, please contact your administrator.</div>"
    
    //Removing the div that displays the message (of either no insured being present, or insured count exceeding 50)
    if (typeof $(".rowNone").val() !== 'undefined') $(".rowNone").remove();

    $.ajax({
        type: "POST",
        url: '/People/PersonListItemSelected/' + id,
        cache: false,
        success: function (data) {
            var baseid = $(data).find(".baseid").val();
            if(testBasePerson(baseid)) {
                $("#selected-insured .insured-row, #insureds .insured-row").each(function () {
                    if ( $(this).find(".baseid").val() == baseid ) 
                    $(this).remove();
                });

                for (var y = 0; y < selected.length; y++) {
                    if (selected[y].find(".baseid").val() == baseid) {
                        selected.splice(y, 1);
                        idNumbers.splice(y, 1);
                    }
                }    
            }

            selected.push($(data));
            idNumbers.push(id);
            updateView();
        },
        error: function () {
            $(err).dialog({
                title: "Error",
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog('close');
                    }
                }
            })
        }
    });   //END OF AJAX CALL
    checkIfAdded = 0; //Reset the variable
}


$(function () {
    cid = $("#compid").val();

    $("#addInsured").click(function () {
        $("#insured-modal").load('/People/peopleList/' + cid, function (value) {

            if (value.substring(2, 6) == 'fail') { //then user does not have the permissions
                $(this).dialog('close');
                $(this).html("");
                $(this).dialog().dialog("destroy");

                $("<div>You do not have the required permissions to complete this task. </div>").dialog({
                    title: 'Alert!',
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                            $(this).dialog().dialog("destroy");
                        }
                    }
                });

            }
            else {
                
                peopleSelected = [];
                var selectedInsureds = $('#insured').find(".insured");
                   if( selectedInsureds.length > 0 ){
                        
                         for( var i = 0; i < selectedInsureds.length; i++){
                            
                            peopleSelected.push($(selectedInsureds[i]).find('.perId').val());
                         }
                     }


                if ($(document).find(".insured-row").length <= 50){

                     $(document).find(".insured-row").show();
//Added to prevent insured on page to be shown in modal again
                     
                
                    
                    // $(document).find(".insured-row").each({

                    for( var x = 0; x < peopleSelected.length; x++){
                            for( var i = 0; i < $(document).find(".insured-row").length; i++){
                        
                            
                            if( $(document).find(".insured-row")[i].find('.insuredcheck').prop('id') == peopleSelected[x])
                                 $(document).find(".insured-row")[i].hide();

                        }
                    }

                   //  });
/********/
                  }
                else $(document).find(".insured-row").hide();



                scrollHeight = $("#insureds").height();

                $("#insureds").perfectScrollbar({
                    wheelSpeed: 0.3,
                    wheelPropagation: true,
                    minScrollbarLength: 10,
                    suppressScrollX: true
                });

                $("#selected-insured").perfectScrollbar({
                    wheelSpeed: 0.3,
                    wheelPropagation: true,
                    minScrollbarLength: 10,
                    suppressScrollX: true
                });
            }
        }).dialog({
            modal: true,
            //autoOpen: false,
            width: 700,
            height: 620,
            title: "People List",
            dialogClass: 'peopleList'
        });
    });
});

function changeCheckBox(e) {

    //Stores search value, as it breaks if the value is not stored before the div is manipulated
    var searchValue = $(e).parents("#insured").find(".modalSearch").val();  
    $(e).parents(".insured-row").remove();

    if ($(e).parents(".insured-row").hasClass("selected")) {
        var alreadyAdded = false;

        $("#insureds").children(".insured-row").each(function () {
            if ($(this).children(".insured-check").children(".insuredcheck").prop('id') == e.id) {
                alreadyAdded = true;
                return false;
            } 
        });
                
        if (!alreadyAdded) {
            $("#insureds").append($(e).parent().parent().toggleClass("selected"));
        }
        removeELement(e.id);
    }
    else AddPersonSelected(e.id, 0);

    searchData(searchValue,true);
}


function searchPeople(e) {
    var filter = $(e).parents("#insured").find(".modalSearch").val();
    searchData(filter);
}

function searchData(filter,checkbox) {
    var checkbox = typeof checkbox == 'undefined' ? false : checkbox;

    $(".rowNone").remove();
    if (filter.trim().length == 0) {
        if ($(document).find(".insured-row").length <= 50){
            $(document).find(".insured-row").show();
            if ($("#insureds").height() + $("#selected-insured").height() < 380)  $("#insureds").height(380); 
         }
        else  $("#insureds").children(".insured-row").hide(); 
    }
    else {
        //var t =0;
        $("#insureds").children(".insured-row").each(function () {
            //If the person is already on display, in the select box, then it doesn't need to be shown in the search area again 
            // (this is needed for when the modal is called back, and all the insured are called from the database (to not include duplicates)
            testIfAdded($($(this).children(".insured-check").children(".insuredcheck")).prop('id'));

            if (checkIfAdded != 1) 
            if (($(this).children(".insured-trn").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".insured-fname").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".insured-lname").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1)) {
                $(this).show();
                if ($("#insureds").height() + $("#selected-insured").height() < 380)  $("#insureds").height($("#insureds").height() + 20);
            }
            else $(this).hide();

            checkIfAdded = 0;

            //Added to test if insured added already, then dont show

            for( var y = 0; y < peopleSelected.length; y++ ){
                
                if( $($(this).children(".insured-check").children(".insuredcheck")).prop('id') == peopleSelected[y]){
                $(this).hide();
                 }

            }

           
            //t++;
            /*****/
        });

        
            

    } //End of Else

    redrawList();

    if($("#insureds").height() == 0 && !$($(".insured-row")[0]).is(':hidden')) $(document).find("#insureds").prepend(noInsured);
    else if (! $('.insured-row').is(':visible')) $(document).find("#insureds").prepend(InsuredHidden);

    if(!checkbox){
        $(".recordsFound").text("");
        if($("#insureds").children(".insured-row:visible").length <= 0)  $(".rowNone").prepend("<div class=\"ResultModal\">No Persons found!</div>");
        else
        $(".recordsFound").text($("#insureds").children(".insured-row:visible").length + " record(s) found");
    }


    $("#insureds").perfectScrollbar('update');
    $("#selected-insured").perfectScrollbar('update');
}

function searchOptionsPeople() {
    var name = $(event.target).parent().children('#searchModal').val().toLowerCase();
    $(event.target).parent().parent().children("#insureds").children(".insured-row").filter(function (div) {
        for (var i = 0; i < $(this).children().length; i++) {
            if ($($(this).children()[i]).text().toLowerCase().trim().indexOf(name) > -1) {
                //show
                $(this).show();
                break;
            }
            else {
                //uncheck and hide
                if ($(this).children().length - 1 == i) {
                    $(this).children('.insured-check').children('input.insuredcheck').prop('checked', false);
                    $(this).hide();
                }
            }
        }
    });
};


$(function () {
    $(document).on("click", ".remInsured", function (e) {
          
        e.preventDefault();
        var deleteItem = $(this).parents("div.insured:first");
            

        $("<div>You are about to delete this person. Are you sure you want to do this? </div>").dialog({
            modal: true,
            title: 'Alert!',
            buttons: {
                Ok: function () {
                    deleteItem.remove();
                    //if ($("#page").val() == "edit") rm_person_policyholder_ajax($("#hiddenid").val(), $(deleteItem).find(".perId").val());
                    RemoveHolder((deleteItem).find(".perId").val());
                    update_type_all();
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                },
                Cancel: function () {
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                }
            }
        });
    });
 
    $("#add").click(function () {
        getValueUsingClass();
    });
 
    $("#create").click(function () {
        loadCreate();
    });

});

//closing the dialog window
function closeDialog() {
    $("#insured-modal").dialog("close");
    $("#insured-modal").html("");
    selected = [];
    idNumbers = [];
    checkIfAdded = 0;
}

function loadCreate() {

    var regexTRN = /^[01][0-9]{8}$/;
    var regexTRNPerson = /^[1][0-9]{8}$/;
    var regex = /^[a-zA-Z][A-Za-z-.\' ]*[a-zA-Z]$/;
    var regex1 = /^[a-zA-Z][A-Za-z-.\' ]*[a-zA-Z]*$/;
    var regex2 = /^[a-zA-Z0-9][a-zA-Z0-9#\-/&. ]*$/;
    var regex3 = /^[a-zA-Z][a-zA-Z0-9.&,#\-\' ]*$/;
    var regex4 = /^[a-zA-Z][a-zA-Z.&,#\-\' ]*[a-zA-Z ]$/;
    var regexAptNum = /^[a-zA-Z0-9-\/#&.\' ]*$/;
    var regexEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9])+$/;
    var regexCity = /^(([a-zA-Z](([a-zA-Z0-9.& '-])*[a-zA-Z0-9])?)|([a-zA-Z](([a-zA-Z0-9.& '-])*([ ])?[(]([a-zA-Z0-9.& '-])+[a-zA-Z0-9][)])?))$/;

    $("#insured-modal1").load('/People/Person/?type=' + 1,function(){ $('input').trigger('change'); }).dialog({
        modal: true,
        width: 370,
        height: 690,
        title: "Policy Holder Create",

        buttons: {

            Create: function () {

                var trn = $("#insured-modal1").find("#trn-editPolHol").val().trim();
                var fname = $("#insured-modal1").find("#fname").val().trim();
                var mname = $("#insured-modal1").find("#mname").val().trim();
                var lname = $("#insured-modal1").find("#lname").val().trim();
                var roadno = $("#insured-modal1").find("#RoadNumber").val().trim();
                var aptNumber = $("#insured-modal1").find("#aptNumber").val().trim();
                var road = $("#insured-modal1").find("#roadname").val().trim();
                var roadType = $("#insured-modal1").find("#roadtype").val().trim();
                var zip = $("#insured-modal1").find("#zipcode").val();
                var parish = $("#insured-modal1").find(".parishes").val();
                var city = $("#insured-modal1").find("#city").val().trim();
                var country = $("#insured-modal1").find("#country").val().trim();
                var ret = 0;
                var count = 0; 
                var validated = true;


                var emails = [];
                var primary = [];
                var email_objects = [];

                $(".email").each(function () {
                    emails[emails.length] = $(this).val();

                    if (!regexEmail.test(emails[count])) {

                        alert('An email address entered was invalid.');
                        validated = false;
                        return false;
                    }
                    count++;
                });


                if (validated)
                    $(".emailprimary").each(function () {
                        primary[primary.length] = $(this).prop("checked");
                    });


                if (validated)
                    for (var i = 0; i < emails.length; i++) {
                        email_objects.push({ "email": emails[i], "primary": primary[i] });
                    }


                // VALIDATION CHECKS
                if (!validated) { return false; }
                if (trn == '' || !regexTRNPerson.test(trn)) {
                    alert('Please enter a valid TRN. TRN Must be a nine digit number, beginning with a 1');
                    return false;
                }

                if (fname == '' || !regex.test(fname)) {
                    alert('Please enter a valid first name. ');
                    return false;
                }

                if (mname == '') { }
                else {
                    if (!regex1.test(mname)) {
                        alert('Please enter a valid middle name.');
                        return false;
                    }
                }

                if (lname == '' || !regex.test(lname)) {
                    alert('Please enter a valid last name.');
                    return false;
                }
                /*
                if (roadno == '') { }
                else {
                    if (!regex2.test(roadno)) {
                        alert('Please enter a valid road number.');
                        return false;
                    }
                }

                if (aptNumber != '' && !regexAptNum.test(aptNumber))
                {
                    alert('Please enter a valid apartment/building number.');
                    return false;
                }

                if (road == '' || !regex3.test(road)) {
                    alert('Please enter a valid road name.');
                    return false;
                }

                if (roadType == '') { }
                else {
                    if (!regex3.test(roadType)) {
                        alert('Please enter a valid road type.');
                        return false;
                    }
                }

                if (zip == '') { }
                else {
                    if (!regex2.test(zip)) {
                        alert('Please enter a valid zip.');
                        return false;
                    }
                }
                */
                if(parish == 0){
                    alert('Please select a parish.');
                    return false;
                }
                  

                if (city == '' || !regexCity.test(city)) {
                    alert('Please enter a valid city.');
                    return false;
                }
                  

                if (country == '' || !regex4.test(country)) {
                    alert('Please enter a valid country name.');
                    return false;
                }

                var parms =
                { 
                    trn: trn, 
                    fname: fname, 
                    mname: mname, 
                    lname: lname, 
                    roadno: roadno, 
                    aptNumber: aptNumber,
                    road: road, 
                    roadType: roadType, 
                    zipcode: zip, 
                    city: city, 
                    country: country, 
                    email: emails, 
                    primary: primary, 
                    parish: parish
                };


                $.ajax({
                    type: "POST",
                    traditional: true,
                    url: "/People/AddPerson/",
                    async: false,
                    data: parms,
                    dataType: "json",
                    success: function (data) {
                        if($("#editingPolHolder").val() == true || $("#editingPolHolder").val() == "true"){
                            
                            $('<div>The policy holder\'s record has been saved</div>').dialog({
                                modal:true,
                                title: 'Policy Holder Saved',
                                buttons:{
                                    OK: function(){
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                        $("#insured-modal1").html("");
                        $("#insured-modal1").dialog("close");

                        AddPersonSelected(data.per, 1);
                    }
                });

            }, // END OF 'Create' BUTTON Function

            Cancel: function () {
                $("#insured-modal1").dialog("close");
                $("#insured-modal1").html("");
                updateView(); //Load back the selected items
            }

        } // END OF BUTTONS
    }); // END OF 'loadCreate' FUNCTION

} // END OF '#insured-modal1' DIALOG


function getValueUsingClass() {

    var chkArray = [];
    var peopleIds = [];
    var checkIfAdded = 0;
   
    var val = []; //stores the values of the ids added already
    var count = 0;
    var count1 = 0;

    //counting the amount of persons already added to the policy
    $(".perId").each(function () {
        count++;
    });

    //counting the amount of persons selected
    $(".insuredcheck:checked").each(function () {
        count1++;
    });


    if (count1 == 0) //No insured has been selected 
    {
         
        $('<div> You have not selected an insured to be added. Please do so, and try again. </div>').dialog({
            title: 'Alert',
            modal:true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    return false;
                }
            }
        });
    }
    else {
        if (count > 0) {
            var policyHolderReAdd = false;

            //stroring all the already added Ids in an array
            for (var x = 0; x < count; x++) {
               val[x] = $(".base_per_id")[x].value;
            }

            //checking each selected ids against the ones already added
            for (var y = 0; y < count1; y++) {
                for (var z = 0; z < count; z++) {
                    if ($($(".insuredcheck:checked")[y]).parents(".insured-row").find(".baseid").val() == val[z]) {
                        checkIfAdded = 1;
                        policyHolderReAdd = true;
                    }
                } 

                if (checkIfAdded != 1) peopleIds[peopleIds.length] = $(".insuredcheck:checked")[y].id;
                checkIfAdded = 0; //variable is reset
            } //END OF OUTER FOR


            //IF AT LEAST ONE OF THE POLICY HOLDERS WAS ALREADY ADDED TO THE PAGE
            if (policyHolderReAdd) {
                $('<div> One or more policyholder that you have selected has already been chosen to be added to this policy. This policyholder(s) will not be added again. However, any other chosen one will be. </div>').dialog({
                    title: 'Alert',
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }

            for (var x= 0; x < peopleIds.length; x++) {
                $.ajax({
                    type: "POST",
                    url: '/People/personView/?perId=' + peopleIds [x],
                    cache: false,
                    success: function (html) { 
                        $("#insured").append(html); 
                        update_type_all();
                        AddPolicyHolderToDropDown($(html).find(".perId").val(),$(html).find(".names").val());
                       // if ($("#page").val() == "edit") add_person_policyholder_ajax($("#hiddenid").val(), $(html).find(".perId").val());
                    }
                });
            } 
        } //END IF
        else {
            $(".insuredcheck:checked").each(function () {
                $.ajax({
                    type: "POST",
                    url: '/People/personView/?perId=' + $(this).prop('id'),
                    cache: false,
                    success: function (html) { 
                        $("#insured").append(html); 
                        AddPolicyHolderToDropDown($(html).find(".perId").val(),$(html).find(".names").val());
                        update_type_all();
                        //if ($("#page").val() == "edit") add_person_policyholder_ajax($("#hiddenid").val(), $(html).find(".perId").val());
                    }
                });
            });
        }

        $("#insured-modal").dialog('close');
        $("#insured-modal").html("");
    } //END OF ELSE

    selected = [];
    idNumbers = [];
    checkIfAdded = 0;
} // END OF 'getValueUsingClass' FUNCTION

function AddPolicyHolderToDropDown(id, name){
    var len = $("#insuredsdropdown option").length;
    var goahead = true;
    if (len == 0) {
        ActuallyAddHolder('p' + id, name);
    }
    else {
        $("#insuredsdropdown option").each(function(index, element){
            if ($(this).val() == 'p' + id && goahead) {
                goahead = false;
            }
            if (index == len -1) {
                if (goahead){
                    ActuallyAddHolder('p' + id, name);
                }
            }
        });
    }
}

function ActuallyAddHolder(id, name){
    $(".pidrop").append("<option value=\""+ id + "\">" + name + "</option>");
}

function RemoveHolder(id){
    $(".pidrop option").each(function(index, element){
        if ($(this).val() == 'p' + id) {
            if ($(this).prop('selected') == true) DefaultInsuredsDropDown();
            $(this).remove();
            return false;
        }
    });
}

/*
function add_person_policyholder_ajax(policy_id, person_id){
    $.ajax({
        url:'/Policy/AddPolicyHolderToPolicy/' + policy_id + '?pid=' + person_id,
        cache: false,
        success: function(json){
            if (json.result){
                policy_holder_message(json.name, 'add');
            }
            else {
                policy_holder_message("", 'err');
            }
        },
        error: function(){
            policy_holder_message("", 'err');
        }
    });
}

function rm_person_policyholder_ajax(policy_id, person_id) {
    $.ajax({
        url:'/Policy/RmPolicyHolderToPolicy/' + policy_id + '?pid=' + person_id,
        cache: false,
        success: function(json){
            if (json.result){
                policy_holder_message_person(json.name, 'rm');
            }
            else {
                policy_holder_message_person("", 'err');
            }
        },
        error: function(){
            policy_holder_message_person("", 'err');
        }
    });
}

function policy_holder_message_person(message, type){
    var _class = "";
    switch(type){
        case 'add':
            _class = "success-message";
            message = "The policy holder " + message + " has been added to the policy.";
            break;
        case 'rm':
            _class = "remove-message";
            message = "The policy holder " + message + " has been removed from the policy.";
            break;
        case 'err':
            _class = "error-message";
            message = "There was an error in adding the policy holder " + message + ". Please try again.";
            break;
        default:
            break;
    }
    $(".policyholders .processed-responses").append("<div class=\"" + _class + "\">" + message + "</div>");
    clean_up_server_responses_persons();
}

function clean_up_server_responses_persons() {
    $(".policyholders .processed-responses").find(".success-message:hidden,.error-message:hidden").remove();
    $(".policyholders .processed-responses").children().each(function () {
        var div = $(this);
        setTimeout(function () {
            person_response_fade_out(div);
        }, 5000);
    });
}

function person_response_fade_out(e) {
    $(e).fadeOut("slow");
}
*/