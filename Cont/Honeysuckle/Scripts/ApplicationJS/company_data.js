﻿var processing = "<div class=\"dataprocessing\"><div class=\"rect1\"></div><div class=\"rect2\"></div><div class=\"rect3\"></div><div class=\"rect4\"></div><div class=\"rect5\"></div></div>";
var interval;
var PolcountRecords = "";
var CompcountRecords = "";
var VehcountRecords = "";

var VCcountRecords = "";
var VCNcountRecords = "";
var PercountRecords = "";

var selected_start;
var selected_end;


$(function () {


    /*$('.scroll-table').perfectScrollbar({
    wheelSpeed: 0.3,
    wheelPropagation: true,
    minScrollbarLength: 10,
    suppressScrollX: true
    });*/
    DisplayData();
    $(".data-headers").on('click', function (e) {
        if (e.target.id != "import-dropdown") {
            if ($(this).parent().children(".data-content").is(':visible')) {
                //$(this).parent().children(".data").css("margin-bottom", "20px");
                $(this).parent().children(".data-content").slideUp(400);
                $(this).parents(".data-result").find(".arrow").toggleClass('down');
                //$(this).parent().children(".data-content").find(".row").hide();
            }
            else {
                $(".data-content").each(function () {
                    //$(this).parent().children(".data").css("margin-bottom", "20px");
                    if ($(this).is(':visible')) $(this).slideUp(400);
                    if ($(this).parents(".data-result").find(".arrow").hasClass("down")) $(this).parents(".data-result").find(".arrow").toggleClass('down');

                });
                // $(this).parent().children(".data").css("margin-bottom", "20px");
                $(this).parent().children(".data-content").slideDown(400);
                // $(this).parent().children(".data-content").find('.data.row').slideDown(400);
                $(this).parents(".data-result").find(".arrow").toggleClass('down');

                //Add the perfect scroll bar to the dropped down search element
                $(this).parent().find('.scroll-table').perfectScrollbar({
                    wheelSpeed: 0.3,
                    wheelPropagation: true,
                    minScrollbarLength: 10,
                    suppressScrollX: true
                });
            }
        }
    });
});

$(function () {
    //selected_start = new Date($(".datepicker.created_data.start").val()).dateFormat('Y m d H:i');
    //selected_end = new Date($(".datepicker.created_data.end").val()).dateFormat('Y m d H:i');

    selected_start = $(".datepicker.created_data.start").val();
    selected_end = $(".datepicker.created_data.end").val();

    $('.datepicker.created_data').prop('disbaled', true);
    $('.update-data').prop('disabled', true);
    $('.datepicker.created_data').addClass('read-only');
    $('#createdORmodified').prop('disabled', true);


    $("input[name='filter']").on('change', function () {
        var checked = $(this).val();
        switch (checked) {
            case "all":
                DisplayData();
                $('.datepicker.created_data').prop('disbaled', true);
                $('.update-data').prop('disabled', true);
                $('.datepicker.created_data').addClass('read-only');
                $('#createdORmodified').prop('disabled', true);
                break;
            case "date-range":
                $('.datepicker.created_data').prop('disbaled', false);
                $('.update-data').prop('disabled', false);
                $('.datepicker.created_data').removeClass('read-only');
                $('#createdORmodified').prop('disabled', false);
                break;
        }
    });


    var start = new Date($(".datepicker.created_data.start").val());
    var end = new Date($(".datepicker.created_data.end").val());

    /*  $(".start").datetimepicker({
    id: 'effect',
        
    format: 'd M Y h:i A',
    formatTime: 'h:i A',
    step: 5,
    lang: 'en-GB',
    closeOnDateSelect: true,
    onShow: function (ct) {
    effective = ct;
    },
        
    });*/


    $(".datepicker.created_data.start").datetimepicker({
        id: 'created_start',
        format: 'd M Y H:i',
        formatTime: 'H:i',
        step: 5,
        lang: 'en-GB',
        closeOnDateSelect: true,
        closeOnWithoutSelect: true,
        // value: start.dateFormat('d M Y H:i'),
        onShow: function (ct) {

        },
        onSelectDate: function (ct) {
            selected_start = new Date(ct).dateFormat('Y m d H:i');
        },
        onSelectTime: function (ct) {
            selected_start = new Date(ct).dateFormat('Y m d H:i');
        }
    });

    $(".datepicker.created_data.end").datetimepicker({
        id: 'created_end',
        format: 'd M Y H:i',
        formatTime: 'H:i',
        step: 5,
        lang: 'en-GB',
        closeOnDateSelect: true,
        closeOnWithoutSelect: true,
        // value: end.dateFormat('d M Y H:i'),
        onShow: function (ct) {

        },
        onSelectDate: function (ct) {
            //selected_end = new Date(ct.setDate(ct.getDate() + 1)).dateFormat('Y m d H:i');
            selected_end = new Date(ct).dateFormat('Y m d H:i');
        }
    ,
        onSelectTime: function (ct) {
            // selected_end = new Date(ct.setDate(ct.getDate() + 1)).dateFormat('Y m d H:i');
            selected_end = new Date(ct).dateFormat('Y m d H:i');
        }
    });

    $('.update-data').click(function () {
        if (!$('.update-data').is(':disabled')) {
            $('#import-dropdown').val('all');
            $(".data-content").each(function () {
                //$(this).parent().children(".data").css("margin-bottom", "20px");
                if ($(this).is(':visible')) $(this).slideUp(400);
                if ($(this).parents(".data-result").find(".arrow").hasClass("down")) $(this).parents(".data-result").find(".arrow").toggleClass('down');

            });
            var typeOfDate = $('#createdORmodified').val();
            DisplayData(selected_start, selected_end, typeOfDate);
        }
    });

});

function DisplayData(start, end, typeOfDate) {
    startInterval();

    if (typeof typeOfDate == "undefined") typeOfDate = null;

    if (typeof start == "undefined" && typeof end == "undefined") {
        start = new Date(0).dateFormat('Y m d H:i');
        end = new Date(0).dateFormat('Y m d H:i');
    }
    $(".data-content").hide();
    $("#data-results-covernotes").html("");
    $("#data-results-certificates").html("");
    $("#data-results-vehicles").html("");
    $("#data-results-policies").html("");
    $("#data-results-companies").html("");
    $("#data-results-people").html("");
    add_processing();
    
    $.ajax({
        url: '/Company/ShortCoverNote/?start=' + start + '&end=' + end + '&typeOfDate=' + typeOfDate,
        cache: false,
        success: function (html) {
            if (html == "") {
                $("#covernotes").children(".data-headers").children(".count").text("No Records or Not Allowed");
                $("#data-results-covernotes").html("");
            }
            else {
                $("#covernotes").children(".data-headers").children(".count").text($($(html)[0]).val() + " Records");
                VCNcountRecords = $("#covernotes").children(".data-headers").children(".count").text();
                $("#data-results-covernotes").html(html);
            }

            if ($("#data-results-covernotes").is(":visible")) $("#data-results-covernotes").find('.scroll-table').perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });
        },
        error: function () {
        }
    });

    $.ajax({
        url: '/Company/ShortCertificate/?start=' + start + '&end=' + end + '&typeOfDate=' + typeOfDate,
        cache: false,
        success: function (html) {
            if (html == "") {
                $("#certificates").children(".data-headers").children(".count").text("No Records or Not Allowed");
                $("#data-results-certificates").html("");
                
            }
            else {
                $("#certificates").children(".data-headers").children(".count").text($($(html)[0]).val() + " Records");
                VCcountRecords = $("#certificates").children(".data-headers").children(".count").text();
                $("#data-results-certificates").html(html);
            }

            if ($("#data-results-certificates").is(":visible")) $("#data-results-certificates").find('.scroll-table').perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });
        },
        error: function () {
        }
    });

    $.ajax({
        url: '/Company/ShortVehicle/?start=' + start + '&end=' + end + '&typeOfDate=' + typeOfDate,
        cache: false,
        success: function (html) {
            if (html == "") {
                $("#vehicles").children(".data-headers").children(".count").text("No Records or Not Allowed");
                $("#data-results-vehicles").html("");
            }
            else {
                $("#vehicles").children(".data-headers").children(".count").text($($(html)[0]).val() + " Records");
                VehcountRecords = $("#vehicles").children(".data-headers").children(".count").text();
                $("#data-results-vehicles").html(html);
            }

            if ($("#data-results-vehicles").is(":visible")) $("#data-results-vehicles").find('.scroll-table').perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });
        },
        error: function () {
        }
    });

    $.ajax({
        url: '/Company/ShortPolicies/?start=' + start + '&end=' + end + '&typeOfDate=' + typeOfDate,
        cache: false,
        success: function (html) {
            if (html == "") {
                $("#policies").children(".data-headers").children(".count").text("No Records or Not Allowed");
                $("#data-results-policies").html("");
            }
            else {
                $("#policies").children(".data-headers").children(".count").text($($(html)[0]).val() + " Records");
                PolcountRecords = $("#policies").children(".data-headers").children(".count").text();
                $("#data-results-policies").html(html);
            }

            if ($("#data-results-policies").is(":visible")) $("#data-results-policies").find('.scroll-table').perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });
        },
        error: function () {
        }
    });

    $.ajax({
        url: '/Company/ShortCompany/?start=' + start + '&end=' + end + '&typeOfDate=' + typeOfDate,
        cache: false,
        success: function (html) {
            if (html == "") {
                $("#companies").children(".data-headers").children(".count").text("No Records or Not Allowed");
                $("#data-results-companies").html("");
            }
            else {
                $("#companies").children(".data-headers").children(".count").text($($(html)[0]).val() + " Records");
                CompcountRecords = $("#companies").children(".data-headers").children(".count").text();
                $("#data-results-companies").html(html);
            }

            if ($("#data-results-companies").is(":visible")) $("#data-results-companies").find('.scroll-table').perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });
        },
        error: function () {
        }
    });

    $.ajax({
        url: '/Company/ShortPeople/?start=' + start + '&end=' + end + '&typeOfDate=' + typeOfDate,
        cache: false,
        success: function (html) {
            if (html == "") {
            }
            else {
                $("#people").children(".data-headers").children(".count").text($($(html)[0]).val() + " Records");
                PercountRecords = $("#people").children(".data-headers").children(".count").text();
                $("#data-results-people").html(html);
            }

            if ($("#data-results-people").is(":visible")) $("#data-results-people").find('.scroll-table').perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });
        },
        error: function () {
        }
    });
}

$(function () {

    $('#import-dropdown').on('change', function () {
        var polCount = 0, vehCount = 0, certCount = 0, coverCount = 0, perCount = 0, compCount = 0;
        var val = $(this).val();

        switch (val) {
            case "all":
                //$("#data-results-policies").find('.data.row').show();
                //$("#policies").children(".data-headers").children(".count").text(countRecords);
                $('.data.row').show();
                $("#covernotes").children(".data-headers").children(".count").text(VCNcountRecords);
                $("#certificates").children(".data-headers").children(".count").text(VCcountRecords);
                $("#policies").children(".data-headers").children(".count").text(PolcountRecords);

                $("#vehicles").children(".data-headers").children(".count").text(VehcountRecords);
                $("#companies").children(".data-headers").children(".count").text(CompcountRecords);
                $("#people").children(".data-headers").children(".count").text(PercountRecords);
                break;

            case "imported":
                $("#data-results-policies").find('.hidimport').each(function () {
                    if ($(this).val() == "False")
                        $(this).parents('.data.row').hide();
                    if ($(this).val() == "True") {
                        polCount++;
                        $(this).parents('.data.row').show();
                    }
                });
                $("#policies").children(".data-headers").children(".count").text(polCount + " Records");
                $("#data-results-policies").find('.scroll-table').perfectScrollbar('update');

                $("#data-results-covernotes").find('.hidimport').each(function () {
                    if ($(this).val() == "False")
                        $(this).parents('.data.row').hide();
                    if ($(this).val() == "True") {
                        coverCount++;
                        $(this).parents('.data.row').show();
                    }
                });
                $("#covernotes").children(".data-headers").children(".count").text(coverCount + " Records");
                $("#data-results-covernotes").find('.scroll-table').perfectScrollbar('update');

                $("#data-results-certificates").find('.hidimport').each(function () {
                    if ($(this).val() == "False")
                        $(this).parents('.data.row').hide();
                    if ($(this).val() == "True") {
                        certCount++;
                        $(this).parents('.data.row').show();
                    }
                });
                $("#certificates").children(".data-headers").children(".count").text(certCount + " Records");
                $("#data-results-certificates").find('.scroll-table').perfectScrollbar('update');

                $("#data-results-vehicles").find('.hidimport').each(function () {
                    if ($(this).val() == "False")
                        $(this).parents('.data.row').hide();
                    if ($(this).val() == "True") {
                        vehCount++;
                        $(this).parents('.data.row').show();
                    }
                });
                $("#vehicles").children(".data-headers").children(".count").text(vehCount + " Records");
                $("#data-results-vehicles").find('.scroll-table').perfectScrollbar('update');

                $("#data-results-companies").find('.hidimport').each(function () {
                    if ($(this).val() == "False")
                        $(this).parents('.data.row').hide();
                    if ($(this).val() == "True") {
                        compCount++;
                        $(this).parents('.data.row').show();
                    }
                });
                $("#companies").children(".data-headers").children(".count").text(compCount + " Records");
                $("#data-results-companies").find('.scroll-table').perfectScrollbar('update');

                $("#data-results-people").find('.hidimport').each(function () {
                    if ($(this).val() == "False")
                        $(this).parents('.data.row').hide();
                    if ($(this).val() == "True") {
                        perCount++;
                        $(this).parents('.data.row').show();
                    }
                });
                $("#people").children(".data-headers").children(".count").text(perCount + " Records");
                $("#data-results-people").find('.scroll-table').perfectScrollbar('update');
                break;

            case "ximported":
                //                $("#data-results-vehicles").find('.hidimport').each(function () {
                //                    if ($(this).val() == "True")
                //                        $(this).parents('.data.row').hide();

                //                    if ($(this).val() == "False") {
                //                        c++;
                //                        $(this).parents('.data.row').show();
                //                    }
                //                });
                //                $("#policies").children(".data-headers").children(".count").text(c + " Records");
                //                $("#data-results-policies").find('.scroll-table').perfectScrollbar('update');


                $("#data-results-policies").find('.hidimport').each(function () {
                    if ($(this).val() == "True")
                        $(this).parents('.data.row').hide();
                    if ($(this).val() == "False") {
                        polCount++;
                        $(this).parents('.data.row').show();
                    }
                });
                $("#policies").children(".data-headers").children(".count").text(polCount + " Records");
                $("#data-results-policies").find('.scroll-table').perfectScrollbar('update');

                $("#data-results-covernotes").find('.hidimport').each(function () {
                    if ($(this).val() == "True")
                        $(this).parents('.data.row').hide();
                    if ($(this).val() == "False") {
                        coverCount++;
                        $(this).parents('.data.row').show();
                    }
                });
                $("#covernotes").children(".data-headers").children(".count").text(coverCount + " Records");
                $("#data-results-covernotes").find('.scroll-table').perfectScrollbar('update');

                $("#data-results-certificates").find('.hidimport').each(function () {
                    if ($(this).val() == "True")
                        $(this).parents('.data.row').hide();
                    if ($(this).val() == "False") {
                        certCount++;
                        $(this).parents('.data.row').show();
                    }
                });
                $("#certificates").children(".data-headers").children(".count").text(certCount + " Records");
                $("#data-results-certificates").find('.scroll-table').perfectScrollbar('update');

                $("#data-results-vehicles").find('.hidimport').each(function () {
                    if ($(this).val() == "True")
                        $(this).parents('.data.row').hide();
                    if ($(this).val() == "False") {
                        vehCount++;
                        $(this).parents('.data.row').show();
                    }
                });
                $("#vehicles").children(".data-headers").children(".count").text(vehCount + " Records");
                $("#data-results-vehicles").find('.scroll-table').perfectScrollbar('update');

                $("#data-results-companies").find('.hidimport').each(function () {
                    if ($(this).val() == "True")
                        $(this).parents('.data.row').hide();
                    if ($(this).val() == "False") {
                        compCount++;
                        $(this).parents('.data.row').show();
                    }
                });
                $("#companies").children(".data-headers").children(".count").text(compCount + " Records");
                $("#data-results-companies").find('.scroll-table').perfectScrollbar('update');

                $("#data-results-people").find('.hidimport').each(function () {
                    if ($(this).val() == "True")
                        $(this).parents('.data.row').hide();
                    if ($(this).val() == "False") {
                        perCount++;
                        $(this).parents('.data.row').show();
                    }
                });
                $("#people").children(".data-headers").children(".count").text(perCount + " Records");
                $("#data-results-people").find('.scroll-table').perfectScrollbar('update');
                break;
        }


    });
});



function startInterval() {
    $('#import-dropdown').prop('disabled', true);
    interval = setInterval(checkProcessing, 1000);
}

function add_processing() {
    $(".data-result").find(".subheader").find(".count").html("");
    $(".data-result .subheader .count").append(processing);
}



function checkProcessing() {
    if ($(".data-result .subheader .count .dataprocessing").length <= 0) {
        $('#import-dropdown').prop('disabled', false);
        stopInterval();
    }
}
function stopInterval() {
    clearInterval(interval);
}
