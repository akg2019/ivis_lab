﻿var errormessage  = "<div>There was a problem with connecting to the server. Please try again and if the problem persists please contanct your administrator.</div>"
var lockedmessage = "<div>The record you are attempted to modify is currently locked. Please refresh the page and/or unlock the record before attempting to modify the record again.</div>"
var lockbtn   = "<button type=\"button\" class=\"lockbtn submitbutton\" onclick=\"lockData(this)\">Lock Value</button>";
var unlockbtn = "<button type=\"button\" class=\"unlockbtn submitbutton\" onclick=\"unlockData(this)\">Unlock Value</button>";

$(function () {

    UpdateParishSelect();

    $(".parishes").on('change', function () {
        var id = $(this).parents('.row').children(".hidemapid").val();
        var parid = $(this).val();
        $.ajax({
            url: '/DataMapping/UpdateMapDataParish/' + id + '?parish=' + parid,
            cache: false,
            success: function (data) {
                if (!data.result) {
                    $(lockedmessage).dialog({
                        title: "Record Locked!",
                        modal:true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                                $(this).parent().each("option", function () {
                                    if ($(this).val() == 0) {
                                        $(this).prop('selected', true);
                                        return;
                                    }
                                });
                            }
                        }
                    });
                }
            },
            error: function () {
                //show modal and kick the selection
                $(errormessage).dialog({
                    title: "Error!",
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                            $(this).parent().each("option", function () {
                                if ($(this).val() == 0) {
                                    $(this).prop('selected', true);
                                    return;
                                }
                            });
                        }
                    }
                });
            }
        });
    });


    $(".wording_tags").on('change', function () {
        var id = $(this).parents('.row').children(".hidemapid").val();
        var cdata = $(this).val();
        $.ajax({
            url: '/DataMapping/UpdateMapDataParish/' + id + '?cdata=' + cdata,
            cache: false,
            success: function (data) {
                if (!data.result) {
                    $(lockedmessage).dialog({
                        title: "Record Locked!",
                        modal:true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                                $(this).parent().each("option", function () {
                                    if ($(this).val() == 0) {
                                        $(this).prop('selected', true);
                                        return;
                                    }
                                });
                            }
                        }
                    });
                }
            },
            error: function () {
                //show modal and kick the selection
                $(errormessage).dialog({
                    title: "Error!",
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                            $(this).parent().each("option", function () {
                                if ($(this).val() == 0) {
                                    $(this).prop('selected', true);
                                    return;
                                }
                            });
                        }
                    }
                });
            }
        });
    });

    $('.mappingtabledata').perfectScrollbar({
        wheelSpeed: 0.3,
        wheelPropagation: true,
        minScrollbarLength: 10,
        suppressScrollX: true
    });


});

function lockData(e) {
    var id = $(e).parents(".map").find(".hidemapid").val();
    $.ajax({
        url: '/DataMapping/ToggleLock/' + id + '?maplock=true',
        cache: false,
        success: function () {
            $(e).parents('.row').find(".parishes").prop("disabled", true);
            $(e).parents('.row').find(".wording_tags").prop("disabled", true);
            $(e).parent().append(unlockbtn);
            $(e).parent().find(".lockbtn").remove();
        },
        error: function () {
            //show modal and kick the selection
            $(errormessage).dialog({
                title: "Error!",
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                        $(this).parent().each("option", function () {
                            if ($(this).val() == 0) {
                                $(this).prop('selected', true);
                                return;
                            }
                        });
                    }
                }
            });
        }
    });
    return false;
}

function unlockData(e) {
    var id = $(e).parents(".map").find(".hidemapid").val();
    $.ajax({
        url: '/DataMapping/ToggleLock/' + id + '?maplock=false',
        cache: false,
        success: function () {
            $(e).parents('.row').find(".parishes").prop("disabled", false);
            $(e).parents('.row').find(".wording_tags").prop("disabled", false);
            $(e).parent().append(lockbtn);
            $(e).parent().find(".unlockbtn").remove();
        },
        error: function () {
            //show modal and kick the selection
            $(errormessage).dialog({
                title: "Error!",
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                        $(this).parent().each("option", function () {
                            if ($(this).val() == 0) {
                                $(this).prop('selected', true);
                                return;
                            }
                        });
                    }
                }
            });
        }
    });
    return false;
}

function UpdateParishSelect() {
    $(".row").each(function (index, element) {
        var rid = $(element).find(".hiderefid").val();
        if (rid != 0) {
            $(this).find(".parishes").children("option").each(function (index, element) {
                if ($(element).val() == rid) {
                    $(this).prop('selected', true);
                    return;
                }
            });
        }
        var cdata = $(element).find(".hidden_cdata").val();
        if (cdata != "") {
            $(this).find(".wording_tags").children("option").each(function (index, element) {
                if ($(element).val() == cdata) {
                    $(this).prop('selected', true);
                    return;
                }
            });
        }
    });
}