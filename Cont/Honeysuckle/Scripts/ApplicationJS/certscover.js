﻿$(function () {

    $('#covercerts').perfectScrollbar({
        wheelSpeed: 0.3,
        wheelPropagation: true,
        minScrollbarLength: 10,
        suppressScrollX: true
    });


    $("body").on("click", ".covercert.menu .item", function () {
        var type = "null";
        if ($(this).hasClass("type")) type = "type";
        else $(".covercert.menu .type").removeClass("ordered");

        if ($(this).hasClass("number")) type = "number";
        else $(".covercert.menu .number").removeClass("ordered");

        if ($(this).hasClass("effective")) type = "effective";
        else $(".covercert.menu .effective").removeClass("ordered");

        if ($(this).hasClass("expiry")) type = "expiry";
        else $(".covercert.menu .expiry").removeClass("ordered");

        if ($(this).hasClass("created")) type = "created";
        else $(".covercert.menu .created").removeClass("ordered");

        if ($(this).hasClass("policy")) type = "policy";
        else $(".covercert.menu .policy").removeClass("ordered");


        if (type == "null") alert("error");
        else CertCoverSortData(type);

        $(this).toggleClass("ordered");
    });

});


function CertCoverSortData(type) {
    var covercerts = $(".covercert.row");
    
    switch (type) {
        case "type":
            if ($(".covercert.menu .type").hasClass("ordered")) {
                covercerts = $(covercerts).sort(function (a, b) {
                    if ($(a).find(".type").html().toLowerCase() < $(b).find(".type").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".type").html().toLowerCase() > $(b).find(".type").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            else {
                covercerts = $(covercerts).sort(function (a, b) {
                    if ($(a).find(".type").html().toLowerCase() > $(b).find(".type").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".type").html().toLowerCase() < $(b).find(".type").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            break;
        case "number":
            if ($(".covercert.menu .number").hasClass("ordered")) {
                covercerts = $(covercerts).sort(function (a, b) {
                    if ($(a).find(".number").html().toLowerCase() < $(b).find(".number").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".number").html().toLowerCase() > $(b).find(".number").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            else {
                covercerts = $(covercerts).sort(function (a, b) {
                    if ($(a).find(".number").html().toLowerCase() > $(b).find(".number").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".number").html().toLowerCase() < $(b).find(".number").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            break;
        case "effective":
            if ($(".covercert.menu .effective").hasClass("ordered")) {
                covercerts = $(covercerts).sort(function (a, b) {
                    var effectiveDate1 = new Date($(a).find(".effective").html());
                    var effectiveDate2 = new Date($(b).find(".effective").html());

                    if (effectiveDate1 < effectiveDate2) {
                        return 1;
                    }
                    if (effectiveDate1 > effectiveDate2) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            else {
                covercerts = $(covercerts).sort(function (a, b) {
                    var effectiveDate1 = new Date($(a).find(".effective").html());
                    var effectiveDate2 = new Date($(b).find(".effective").html());

                    if (effectiveDate1 > effectiveDate2) {
                        return 1;
                    }
                    if (effectiveDate1 < effectiveDate2) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            break;
        case "expiry":
            if ($(".covercert.menu .expiry").hasClass("ordered")) {
                covercerts = $(covercerts).sort(function (a, b) {

                    var effectiveDate1 = new Date($(a).find(".expiry").html());
                    var effectiveDate2 = new Date($(b).find(".expiry").html());

                    if (effectiveDate1 < effectiveDate2) {
                        return 1;
                    }
                    if (effectiveDate1 > effectiveDate2) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            else {
                covercerts = $(covercerts).sort(function (a, b) {

                    var effectiveDate1 = new Date($(a).find(".expiry").html());
                    var effectiveDate2 = new Date($(b).find(".expiry").html());

                    if (effectiveDate1 > effectiveDate2) {
                        return 1;
                    }
                    if (effectiveDate1 < effectiveDate2) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            break;
        case "created":
            if ($(".covercert.menu .created").hasClass("ordered")) {
                covercerts = $(covercerts).sort(function (a, b) {
                    var createdDate1 = new Date($(a).find(".created").html());
                    var createdDate2 = new Date($(b).find(".created").html());

                    if (createdDate1 < createdDate2) {
                        return 1;
                    }
                    if (createdDate1 > createdDate2) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            else {
                covercerts = $(covercerts).sort(function (a, b) {
                    var createdDate1 = new Date($(a).find(".created").html());
                    var createdDate2 = new Date($(b).find(".created").html());

                    if (createdDate1 > createdDate2) {
                        return 1;
                    }
                    if (createdDate1 < createdDate2) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            break;
        case "policy":
            if ($(".covercert.menu .policy").hasClass("ordered")) {
                covercerts = $(covercerts).sort(function (a, b) {
                    if ($(a).find(".policy").html().toLowerCase() < $(b).find(".policy").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".policy").html().toLowerCase() > $(b).find(".policy").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            else {
                covercerts = $(covercerts).sort(function (a, b) {
                    if ($(a).find(".policy").html().toLowerCase() > $(b).find(".policy").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".policy").html().toLowerCase() < $(b).find(".policy").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            break;
        default:
            break;
    }


    rebuild_coverlist(covercerts);

 }

function rePrintcovercertDivs() {
    var i = 0;
    $(".covercert.row").each(function () {
        $(this).removeClass('odd').removeClass('even');
        if ($(this).is(":visible")) {
            if (i % 2 == 0) $(this).addClass('odd');
            else $(this).addClass('even');
        }
        i++;
    });
}

function rebuild_coverlist(covers) {
    var cover_list = "";
    for (var x = 0; x < covers.length; x++) {
        var cov = covers[x];
        if ($(covers[x]).is(':hidden')) cover_list += cov.outerHTML;
        if (($(covers[x]).hasClass("odd") && x % 2 == 1) || ($(covers[x]).hasClass("even") && x % 2 == 0)) cover_list += cov.outerHTML;
        if (($(covers[x]).hasClass("odd") && x % 2 == 0) || ($(covers[x]).hasClass("even") && x % 2 == 1)) {
            cov = $(cov.outerHTML).toggleClass("odd").toggleClass("even")[0];
            cover_list += cov.outerHTML;
        }
    }
    $("#covercerts").html(cover_list);
}
