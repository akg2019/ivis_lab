﻿$(function () {


    if ($("#CoverID").val() != undefined) {
        if ($("#CoverID").val().length > 0) {
            $(".covers_dropdown").children("option").each(function () {
                if ($(this).val() == $("#CoverID").val()) {
                    $(this).prop('selected', true);
                    return false;
                }
            });
        }
        else updateUsage( $(".covers_dropdown").val() );
    }

    if ($("#useid").val() != undefined) {
        if ($("#useid").val().length > 0) {
            $(".usage_dropdown").children("option").each(function () {
                if ($(this).val() == $("#useid").val()) {
                    $(this).prop('selected', true);
                    return false;
                }
            });
        }
    }

    $(document).on("change", ".covers_dropdown", function () {
        var id = $(this).val();
        updateUsage(id);
    });
});

function updateUsage(i) {
    $.ajax({
        type: "POST",
        url: '/Usage/UsageDropdownOptions/?coverID=' + i,
        cache: false,
        success: function (data) {

            $.each(data, function (i, v) {

                v = JSON.parse(v);

                var elems = document.getElementsByClassName('usage_dropdown');
                for (var i = 0; i < elems.length; i++) {

                    //clearing the data from the dropdowns

                    if (typeof elems[i].options !== 'undefined')
                        for (p = elems[i].options.length - 1; p >= 0; p--) {
                            elems[i].remove(p);
                        }

                    // Placing the new options in the dropdown (if any)
                    for (c = 0; c < v.length; c++) {
                        var option = document.createElement("option");
                        option.text = v[c].usage;
                        option.value = v[c].ID;

                        elems[i].add(option);
                    }
                    $('.usage_dropdown').trigger('change');
                    if (elems[i].value != "")
                        $(".usageIdentity").val(elems[i].value);
                    else $(".usageIdentity").val("");
                } //END OF DROPDOWN FOR LOOP
            })
        } //END OF AJAX SUCCESS FUNCTION
    });
}