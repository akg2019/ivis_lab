﻿var prefixnone = "<div>There is no prefix for the policy cover you've selected. Please go to the Policy Cover Management Module and Update the data set.</div>";
var prefixerror = "<div>There was a networking error of some kind. Please contact your administrator if this problem persists.</div>";

$(function () {

    if ($("#polCoverBox option:selected").val() != undefined) getPrefix($("#polCoverBox option:selected").val());
    $("#polCoverBox").on('change', function (index) {
        getPrefix($("#polCoverBox option:selected").val());
    });

    if ($("#insurer").length == 0) {
        var id = 0;
        $(".company_dropdown option").each(function () {
            if ($(this).prop('selected') == true) {
                getCovers($(this).val());
                return false;
            }
        });
    }
});

function getCovers(id) {
    var firstcover = 0;
    $.ajax({
        url: '/PolicyCover/CoversDropDownJSON/?Compid=' + id,
        cache: false,
        success: function (data) {
            var obj = JSON.parse(data.result);
            if (obj.length > 0) firstcover = obj[0].ID;
            var options = buildOptionsForDropDown(obj);
            $(".covers_dropdown").html(options);
            getPrefix(firstcover);

            // Pulling the policy cover already chosen (if one was chosen).. ie. on postback
            var Polcover = $("#CoverID").val();
            if (Polcover != 0) {
                $(".covers_dropdown").children().each(function () {
                    if ($(this).val() == Polcover) {
                        $(this).prop('selected', true);
                        return;
                    }
                });
            }

        }
    });
}

function buildOptionsForDropDown(json) {
    var output = "";
    for (var i = 0; i < json.length; i++) {
        output += "<option value=\"" + json[i].ID + "\">" + json[i].cover + "</option>";
    }
    return output;
}

function getPrefix(id) {
    //var id = $("#polCoverBox option:selected").val();
    if (id != 0) {
        $.ajax({
            url: '/PolicyCover/Prefix/' + id,
            cache: false,
            success: function (data) {
                if (data.result) {
                    $("#policyprefix").val(data.prefix);
                    $("#policyprefix").trigger("change");
                }
                else {
                    $(prefixnone).dialog({
                        title: 'No Prefix Exists',
                        modal:true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                }
            },
            error: function () {
                $(prefixerror).dialog({
                    title: 'Error!',
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog('close');
                        }
                    }
                });
            }
        });
    }
}