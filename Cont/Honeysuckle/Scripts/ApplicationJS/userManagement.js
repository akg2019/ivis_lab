
$(function () {
    $("#CompanyName").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Company/GetCompanies?type=' + req.term, function (data) {
                var values = []
                $.each(data, function (i, v) {
                    values.push(v.value);
                })
                resp(values);
            });
        },

        select: function (event, ui) {
            return false;
        },

        select: function (event, ui) {
            $(this).val(ui.item ? ui.item : " ");
        },
        change: function (event, ui) {
            if (!ui.label) { this.value = ''; }
        }
    })


    $(".editButton").on('click', function () {
        var id = $(".hiddenid").val();
        window.location.replace('/User/Modify/' + id);
    });
    
});

    