﻿var successful = "<div>You have successfull reset this user's password</div>";
var failed = "<div>Something went wrong. Please try again. If the problem persists please contact your administrator.</div>";
var cant = "<div>You do not have the permissions to perform this action. Please contact your administrator.</div>";

$(function () {

    $('body').on('click', '.reset', function () {
        $.ajax({
            url: this.href,
            cache: false,
            success: function (data) {
                if (data.result) {
                    $(successful).dialog({
                        title: 'Success!',
                        resizable: false,
                        modal: true,
                        buttons:{
                            Ok: function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }
                else {
                    $(cant).dialog({
                        title: 'Permission Error!',
                        resizable: false,
                        modal: true,
                        buttons:{
                            Ok: function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            },
            error: function () {
                $(failed).dialog({
                    title: 'Network Error!',
                    resizable: false,
                    modal: true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }
        });
        return false;
    });

});