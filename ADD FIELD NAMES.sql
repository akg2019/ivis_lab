
---ADDING COLUMNS

ALTER TABLE UsagesToCovers ADD CertType varchar(50) null
ALTER TABLE UsagesToCovers ADD CertCode varchar(50) null
ALTER TABLE UsagesToCovers ADD CompanyID int null
ALTER TABLE POLICIES ADD Prefix varchar(50) null
ALTER TABLE VehiclesUnderPolicy ADD CertType varchar(100)
ALTER TABLE VehiclesUnderPolicy ADD CertCode varchar(100)
ALTER TABLE VehiclesUnderPolicy ADD CertExt varchar(100)
ALTER TABLE VehiclesUnderPolicy ADD CompanyID varchar(100)


---REMOVING DATA

DELETE FROM OutboundMail
DELETE FROM EmailFailures
DELETE FROM SystemAuditLog
DELETE FROM JsonAudit










