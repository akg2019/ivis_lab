﻿var effective;
var expiry;
var period;
var effective_static;
var expiryChange;
var expiryShow;
var InvalidDates;
var InvalidDatesEnd;
var ApprovedList;

vehiclesList = [];
templatewordings = [];
EffectiveDates = [];
ExpiryDates = [];

$(function () {

    update_policy_no_required();
    $(".no_policy_no_check").on("change", function () {
        update_policy_no_required();
    });

    $(".cancel_policy").click(function () {
        $('<div>You are about to cancel this Policy. Are sure you want to do this?</div>').dialog({
            modal: true,
            title: 'Alert!',
            buttons: {
                OK: function () {
                    var id = $("#hiddenid").val();
                    $(this).dialog('destroy');
                    window.location.replace('/Policy/Cancel/' + id);
                },
                Cancel: function () {
                    $(this).dialog('close');
                }
            }
        });

    });

    $(".editButton").on('click', function () {
        var id = $("#hiddenid").val();
        window.location.replace('/Policy/Edit/' + id);
    });

    //THE END DATE IS ONLY CLEARED OUT ON POLICY CREATE 
    if (typeof $("#hiddenid").val() === 'undefined' && typeof $('#postBackVals').val() === 'undefined') {
        // $("#end").val("");
    }

    effective = new Date($("#sysdate").val());
    expiry = new Date($("#sysdate").val());
    effective_static = new Date($("#sysdate").val());

    //expiry.dateFormat('d M Y h:i A'),
    expiry.setHours(23);
    expiry.setMinutes(59);
    expiry.setYear(effective.getFullYear() + 1);
    expiry.setDate(effective.getDate() - 1);
    expiry.setMonth(effective.getMonth());


    //POLICY CREATE  PAGE
    if ($("#page").val() === "create") {


        if (typeof $('#isInsurer').val() !== 'undefined') {
            //This section is run only on a policy create 'postback'
            if (typeof $('#postBackVals').val() !== 'undefined') {


                effective = new Date($("#hidden_start").val());
                expiry = new Date($("#hidden_end").val());


            }

            $("#start").datetimepicker({
                id: 'effect',
                value: effective.dateFormat('d M Y h:i A'),
                format: 'd M Y h:i A',
                formatTime: 'h:i A',
                step: 5,
                lang: 'en-GB',
                closeOnDateSelect: true,
                onShow: function (ct) {
                    effective = ct;
                },
                onSelectDate: function (ct) {
                    effective = ct;
                    $("#start").val(effective.dateFormat('d M Y h:i A'));
                    $("#start").trigger("change");
                    if (expiry != undefined) {

                        var newStart = new Date();

                        newStart.setDate(effective.getDate() + 1);
                        $("#end").val(newStart.dateFormat('d M Y h:i A'));
                        $("#end").trigger("change");

                        if (expiry < effective) {
                            expiry = new Date();
                            $("#end").val($("#start").val());
                            $("#end").trigger("change");
                        }
                    }
                }
            });

            $("#end").datetimepicker({
                id: 'expiry',
                format: 'd M Y h:i A',
                formatTime: 'h:i A',
                step: 5,
                lang: 'en-GB',

                value: expiry.dateFormat('d M Y h:i A'),
                formatTime: 'h:i A',
                allowTimes: ['11:59 PM'],
                closeOnDateSelect: true,
                onShow: function (ct) {
                    expiry = ct;
                },
                onSelectDate: function (ct) {
                    expiry = ct;
                    expiry.setHours(23); expiry.setMinutes(59);
                    $("#end").val(expiry.dateFormat('d M Y h:i A'));
                    $("#end").trigger("change");
                    effective = new Date($("#start").val());
                    if (expiry < effective) {
                        var newStart = new Date();
                        newStart.setDate(expiry.getDate() - 1);
                        $("#start").val(newStart.dateFormat('d M Y h:i A'));
                        $("#start").trigger("change");
                    }

                }
            });
        }
        else {
            //This section is run only on a policy create 'postback'
            if (typeof $('#postBackVals').val() !== 'undefined') {

                effective = new Date($("#hidden_start").val());
                expiry = $("#hidden_end").val();
                expiry = new Date(expiry);

            }

            $("#start").datetimepicker({
                id: 'effect',
                value: effective.dateFormat('d M Y h:i A'),
                //minDate: effective,
                //minTime: effective_static.toLocaleTimeString(),
                format: 'd M Y h:i A',
                formatTime: 'h:i A',
                step: 5,
                lang: 'en-GB',
                closeOnDateSelect: true,
                onShow: function (ct) {
                    effective = ct;

                },
                onSelectDate: function (ct) {
                    effective = ct;
                    $("#start").val(effective.dateFormat('d M Y h:i A'));
                    if (expiry != undefined) {

                        $("#end").val(effective.dateFormat('d M Y h:i A'));
                        $("#end").trigger("change");

                        if (expiry < effective) {
                            expiry = new Date();
                            $("#end").val("");
                        }
                    }
                }
            });

            $("#end").datetimepicker({
                id: 'expiry',
                format: 'd M Y h:i A',

                minDate: effective,
                formatTime: 'h:i A',
                step: 5,
                lang: 'en-GB',
                closeOnDateSelect: true,
                value: expiry.dateFormat('d M Y h:i A'),
                formatTime: 'h:i A',
                allowTimes: ['11:59 PM'],
                onShow: function (ct) {
                    expiry = ct;
                    //expiry.setHours(23); expiry.setMinutes(59);

                },
                onSelectDate: function (ct) {
                    expiry = ct;
                    expiry.setHours(23); expiry.setMinutes(59);
                    $("#end").val(expiry.dateFormat('d M Y h:i A'));
                    $("#end").trigger("change");
                }
            });

        }
    } // END OF IF (POLICY CREATE)


    //POLICY EDIT PAGE
    if ($("#page").val() === "edit") {

        effective = $("#edit_hidden_start").val();
        effective = new Date(effective);
        expiry = $("#edit_hidden_end").val();
        expiry = new Date(expiry);

        $("#start").datetimepicker({
            id: 'effect',
            value: effective.dateFormat('d M Y h:i A'),
            format: 'd M Y h:i A',
            formatTime: 'h:i A',
            step: 5,
            lang: 'en-GB',
            closeOnDateSelect: true,
            onShow: function (ct) {
                effective = ct;
            }
        });

        $("#end").datetimepicker({
            id: 'expiry',
            format: 'd M Y h:i A',
            minDate: effective,
            formatTime: 'h:i A',
            allowTimes: ['11:59 PM'],
            value: expiry.dateFormat('d M Y h:i A'),
            closeOnDateSelect: true,
            onShow: function (ct) {
                expiry = ct;
                //expiry.setHours(23); expiry.setMinutes(59);
            },
            onSelectDate: function (ct) {
                expiry = ct;
                expiry.setHours(23); expiry.setMinutes(59);
                $("#end").val(expiry.dateFormat('d M Y h:i A'));
                $("#end").trigger("change");
            }
        });
    }

    $("#start,#end").trigger("change");
});


//Cancels a vehicle (when the vehicle is under a policy at the company of the current user) from the policy that it is currently under
function CancelRiskFromPolicy() {

    var start = $("#start").val();
    var end = $("#end").val();
    var vehid_div = $(event.target).parents(".newvehicle");
    var vehid = vehid_div.find('.vehId').val();
    var polid = $(event.target).parents("fieldset").find("#policyId").val();
    if (typeof polid == "undefined") polid = 0;
    vehid_div.children(".cancelbtn").toggleClass("hideit");

    var parameters = { vehId: vehid, polid: polid, startDate: start, endDate: end };

    $.ajax({
        type: "POST",
        traditional: true,
        url: "/Vehicle/CancelRiskToPolicy/",
        async: false,
        data: parameters,
        dataType: "json",
        success: function (data) {

            if (data.result) {
                $(".hideit").hide();
                $('#addVehicle').html("");
                $("<div>All other active policies with this vehicle (during the time-frame) have been cancelled</div>").dialog({
                    title: "Alert!",
                    modal:true,
                    buttons:
                        {
                            OK: function () {
                                $(this).dialog("close");
                                $(this).html("");
                                $("#cancelPol").remove();
                            }
                        }
                });
            }

            else {
                $("<div>There was an error in cancelling the policies</div>").dialog({
                    modal: true,
                    autoOpen: false,
                    width: 400,
                    title: "Error!",
                    height: 400,
                    buttons: {
                        OK: function () {
                            $(this).dialog('close');
                            $(this).html("");
                        }
                    }
                });
            }
        }
    });
    return false;
}

$(function () {
    if ($('#Alert').text().trim().length != 0) {

        $("#Alert").dialog({
            modal: true,
            autoOpen: false,
            title: "Alert!",
            buttons:
                {
                OK:
                function () {

                    $('#Alert').dialog('close');
                    $('#Alert').html("");

                }
            }
            });

        $('#Alert').dialog('open');
    }

    if ($('#VehiclePrompt').text().trim().length != 0) {

        $("#VehiclePrompt").dialog({
            modal: true,
            autoOpen: false,
            title: "Alert!",
            buttons: {
                Proceed: function () {
                    $("#Continue").val("true");
                    $("#Submit2").click();
                },
                Cancel: function () {
                    $("#Continue").val("");
                    $('#VehiclePrompt').dialog('close');
                    $('#VehiclePrompt').html("");
                }
            }
        });

        $('#VehiclePrompt').dialog('open');
    }
});


function isEmpty(el) {
    return !$.trim(el.html())
}


//This function allows a user, trying to add a vehicle that is under a policy (at another company), to email the company Admins of the company with the vehicle under policy
function MessageAdmin() {

    var start = $("#start").val();
    var end = $("#end").val();
    var vehid_div = $(event.target).parents(".newvehicle");
    var vehId = vehid_div.find('.vehId').val();

    $("#SendMail").dialog({
        modal: true,
        autoOpen: false,
        width: 500,
        height: 450,
        title: "Message Company Admins.",
        buttons: {
            Send: function () {

                var vehicleID = $("#veh").val();
                var heading = $("#heading").val();
                var content = $("#content").val();
                var recipients = [];
                $('#sendto option:selected').each(function () {
                    recipients.push($(this).val());
                });

                if (recipients.length > 0) {

                    var parms = { vehId: vehicleID, admins: recipients, heading: heading, content: content };


                    $.ajax({
                        type: "POST",
                        traditional: true,
                        url: "/Notification/sendNoteToComp/",
                        async: false,
                        data: parms,
                        dataType: "json",
                        success: function (data) {

                            if (data.result) {

                                $("#SendMail").html("");
                                $("#SendMail").dialog("close");
                                $("<div>A message was sent to Administrator(s) of the Company</div>").dialog({
                                    title: "Alert!",
                                    modal: true,
                                    buttons:
                                    {
                                        OK: function () {
                                            $(this).dialog("close");
                                            $(this).html("");
                                        }
                                    }
                                });
                            }
                        }
                    });
                } //checking if recipients > 0
                else {
                    $("<div>Please add a recipient</div>").dialog({
                        title: "Alert!",
                        modal: true,
                        buttons:
                                {
                                    OK: function () {
                                        $(this).dialog("close");
                                        $(this).html("");
                                    }
                                }
                    });
                } //end recipients check

            }, //END OF 'SEND' BUTTON

            Cancel: function () {
                $("#SendMail").dialog("close");
                $("#SendMail").html("");
            }

        } //END OF BUTTONS

    });          // END OF JQUERY DIALOG
        
    $("#SendMail").load('/Notification/sendNote?id=' + vehId).dialog('open');

}; //END OF 'sendTo' FUNCTION

$(function () {
    $("#editDrivers").click(function () {
        editdrivers();
    });
});

//////////////////// CREATING ALL CERTIFICATES  ////////////////////////////////////

$(function () {
    $("#allCerts").click(function () {
        var pid = $("#hiddenid").val();
        var ShortSet = 0;
        /* var vehiclesList = [];
        var templatewordings = [];
        var EffectiveDates = [];
        */

        $.ajax({
            type: "POST",
            url: "/Company/IsCompShortSet/" + $('#insID').val(),
            cache: false,
            async: false,
            success: function (data) {
                if (data.result == 1)
                    ShortSet = 1;
                else
                    $("<div>The insurance company for this policy does not have a company Shortening code. Please contact your admin for assistance. </div>").dialog({
                        title: "Alert!",
                        modal:true,
                        buttons:
                        {
                            OK: function () {
                                $(this).dialog("close");
                                $(this).html("");
                            }
                        }
                    });
            }
        });


        if (ShortSet == 1) {

            $("#createCerts").dialog({
                modal: true,
                autoOpen: false,
                width: 800,
                title: "Mass Create Certificates",
                height: 600,
                buttons: {
                    Create: function () {
                        massCreateCertificates();
                    }, //END OF 'CREATE' BUTTON FUNCTION    

                    Cancel: function () {
                        $("#createCerts").dialog("close");
                        $("#createCerts").html("");
                        vehiclesList = [];
                        templatewordings = [];
                        EffectiveDates = [];
                    }
                } //END OF BUTTONS
            }); //END OF Vehicles List DIALOG

            $("#createCerts").load('/Vehicle/PolicyVehiclesList/' + pid + '?type=' + 'certificate', function (value) {

                if (value != null && value.substring(2, 6) == 'fail') { //then user does not have the permissions
                    $("#createCerts").dialog('close');
                    $("#createCerts").html("");
                    $("#createCerts").dialog().dialog("destroy");

                    $("<div>You do not have the required permissions to complete this task. </div>").dialog({
                        title: 'Alert!',
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                                $(this).dialog().dialog("destroy");
                            }
                        }
                    });
                }

                else {
                    ////// Limiting the date pickers to dates within the Policy period

                    var now = new Date($("#sysdate").val());

                    $(".effective").each(function () {
                        $(this).datetimepicker({
                            value: now.dateFormat('d M, Y h:i A'),
                            format: 'd M, Y h:i A',
                            closeOnDateSelect: true,
                            onShow: function (ct) {
                                this.setOptions({
                                    minDate: get_effective_date(),
                                    maxDate: get_expiry_date()
                                });
                            }
                        });
                    });

                    //////////////******//////////////////

                    $(".expiryPicker").remove(); //Remove the expiry-date date pickers as those aren't needed 
                    //remove effective for now upon request of app support
                    $(".effectiveDate").remove();

                    $(".Vehicle_ID").each(function () {
                        var ThisVehicle = $(this);
                        $.ajax({
                            type: "POST",
                            url: "/TemplateWording/TemplateWordingFilter/" + ThisVehicle.val() + '?polId=' + $("#hiddenid").val(),
                            cache: false,
                            success: function (data) {

                                $.each(data, function (i, v) {

                                    v = JSON.parse(v);

                                    var elems = ThisVehicle.parent().find('.formatDropdown');
                                    for (var i = 0; i < elems.length; i++) {

                                        //clearing the data from the dropdowns

                                        if (typeof elems[i].options !== 'undefined')
                                            for (p = elems[i].options.length - 1; p >= 0; p--) {
                                                elems[i].remove(p);
                                            }

                                        // Placing the new options in the dropdown (if any)
                                        for (c = 0; c < v.length; c++) {
                                            var option = document.createElement("option");
                                            option.text = v[c].CertificateType;
                                            option.value = v[c].ID;

                                            elems[i].add(option);
                                        }

                                        if (elems[i].value != "")
                                            $("#wordingID").val(elems[i].value);
                                        else $("#wordingID").val("");
                                    } //END OF DROPDOWN FOR LOOP

                                }) //END OF FOREACH USAGE ITEM

                            } // END OF SUCCESS FUNCTION
                        }); //End of third AJAX call

                    }); //END OF 'VEHICLE EACH' IN THE DISPLAY LIST ON THE MODAL


                    //////////////

                    $("#vehiclesListSelect").perfectScrollbar({
                        wheelSpeed: 0.3,
                        wheelPropagation: true,
                        minScrollbarLength: 10,
                        suppressScrollX: true
                    });
                }

                ///////////


            }).dialog('open');

        } // END OF ID TEST (CHECKING IF THE COMP SHORTENING CODE IS SET)

    }); //END CLICK
});


//////////////////// CREATING ALL COVERNOTES  ////////////////////////////////////
$(function () {
    $("#allCoverNotes").click(function () {

        var pid = $("#hiddenid").val();
        var ShortSet = 0;
        var vehiclesList = [];
        var templatewordings = [];
        var EffectiveDates = [];
        var ExpiryDates = [];

        $.ajax({
            type: "POST",
            url: "/Company/IsCompShortSet/" + $('#insID').val(),
            cache: false,
            async: false,
            success: function (data) {
                if (data.result == 1)
                    ShortSet = 1;
                else
                    $("<div>The insurance company for this policy does not have a Company Shortening code. Please contact your admin for assistance! </div>").dialog({
                        title: "Alert!",
                        modal: true,
                        buttons:
                        {
                            OK: function () {
                                $(this).dialog("close");
                                $(this).html("");
                            }
                        }
                    });
            }
        });

        if (ShortSet == 1) {

            $("#createCerts").dialog({
                modal: true,
                autoOpen: false,
                //width: 850,
                width: $(window).width(),
                title: "Mass Create Cover Notes",
                height: 600,
                buttons: {
                    Create: function () {
                        if ($(".vehcheck:checked").length > 0)
                            massCreateCoverNotes();
                        else
                            $('<div>No vehicle was selected</div>').dialog({
                                modal: true,
                                title: 'Alert!',
                                buttons: {
                                    OK: function () {
                                        $(this).dialog('close');
                                    }
                                }
                            });
                    }, //END OF 'DONE' BUTTON FUNCTION
                    Cancel: function () {
                        $("#createCerts").dialog("close");
                        $("#createCerts").html("");
                        vehiclesList = [];
                        templatewordings = [];
                        EffectiveDates = [];
                        ExpiryDates = [];
                        InvalidDates = []; InvalidDatesEnd = [];
                    }
                } //END OF BUTTONS
            }); //END OF Vehicles List DIALOG

            $("#createCerts").load('/Vehicle/PolicyVehiclesList/' + pid + '?type=' + 'coverNote', function (value) {

                if (value.substring(2, 6) == 'fail') { //then user does not have the permissions
                    $("#createCerts").dialog('close');
                    $("#createCerts").html("");
                    $("#createCerts").dialog().dialog("destroy");

                    $("<div>You do not have the required permissions to complete this task. </div>").dialog({
                        title: 'Alert!',
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                                $(this).dialog().dialog("destroy");
                            }
                        }
                    });
                }
                else {
                    $(".Vehicle_ID").each(function () {
                        var ThisVehicle = $(this);
                        $.ajax({
                            type: "POST",
                            url: "/TemplateWording/TemplateWordingFilter/" + ThisVehicle.val() + '?polId=' + $("#hiddenid").val(),
                            cache: false,
                            success: function (data) {

                                $.each(data, function (i, v) {

                                    v = JSON.parse(v);

                                    var elems = ThisVehicle.parent().find('.formatDropdown');
                                    for (var i = 0; i < elems.length; i++) {

                                        //clearing the data from the dropdowns

                                        if (typeof elems[i].options !== 'undefined')
                                            for (p = elems[i].options.length - 1; p >= 0; p--) {
                                                elems[i].remove(p);
                                            }

                                        // Placing the new options in the dropdown (if any)
                                        for (c = 0; c < v.length; c++) {
                                            var option = document.createElement("option");
                                            option.text = v[c].CertificateType;
                                            option.value = v[c].ID;

                                            elems[i].add(option);
                                        }

                                        if (elems[i].value != "")
                                            $("#wordingID").val(elems[i].value);
                                        else $("#wordingID").val("");
                                    } //END OF DROPDOWN FOR LOOP

                                }) //END OF FOREACH USAGE ITEM

                            } // END OF SUCCESS FUNCTION
                        }); //End of third AJAX call

                    }); //END OF 'VEHICLE EACH' IN THE DISPLAY LIST ON THE MODAL




                    //////////// Limiting the date pickers to dates within the Policy period//////////
                    var now = new Date($("#sysdate").val());

                    $(".Vehicle_ID").each(function () {
                        var ThisVehicle = $(this);
                        InvalidDates = []; InvalidDatesEnd = [];
                        //Getting the dates of cover notes already made
                        $.ajax({
                            type: "POST",
                            url: "/VehicleCoverNote/GetcoverNoteDates/" + ThisVehicle.val() + '?pol=' + $("#hiddenid").val(),
                            cache: false,
                            async: false,
                            success: function (data) {

                                $.each(data, function (i, v) {
                                    v = JSON.parse(v);

                                    for (var c = 0; c < v.length; c++) {

                                        var a; var value = v[c].effectivedate; var b;  //expirydate
                                        if (typeof value === 'string') {
                                            a = /\/Date\((\d*)\)\//.exec(value);
                                            if (a) {
                                                b = new Date(+a[1]);
                                            }

                                            var m = b.getMonth();
                                            var d = b.getDate();
                                            var y = b.getFullYear();

                                            // First convert the date in to the mm-dd-yyyy format 
                                            var currentdate = (m + 1) + '-' + d + '-' + y;

                                            InvalidDates.push(currentdate);
                                        }

                                        //////Expiry Dates

                                        value = v[c].expirydate;
                                        if (typeof value === 'string') {
                                            a = /\/Date\((\d*)\)\//.exec(value);
                                            if (a) {
                                                b = new Date(+a[1]);
                                            }

                                            m = b.getMonth();
                                            d = b.getDate();
                                            y = b.getFullYear();

                                            // First convert the date in to the mm-dd-yyyy format 
                                            currentdate = (m + 1) + '-' + d + '-' + y;

                                            InvalidDatesEnd.push(currentdate);
                                        }

                                    } //END OF FOR

                                })
                            }
                        });
                    }); //END OF 'VEHICLE EACH' IN THE DISPLAY LIST ON THE MODAL


                    $(".effective").each(function () {
                        $(this).datetimepicker({
                            value: now.dateFormat('d M, Y h:i A'),
                            format: 'd M, Y h:i A',
                            closeOnDateSelect:true,
                            //  beforeShowDay: DisableSpecificDates,
                            onShow: function (ct) {
                                this.setOptions({
                                    beforeShowDay: DisableSpecificDates,
                                    minDate: get_effective_date(),
                                    maxDate: get_expiry_date()

                                });
                            }
                        });
                    });

                    $(".expiryPicker").each(function () {
                        $(this).datetimepicker({
                            value: now.dateFormat('d M, Y h:i A'),
                            format: 'd M, Y h:i A',
                            closeOnDateSelect: true,
                            onShow: function (ct) {
                                this.setOptions({
                                    beforeShowDay: DisableSpecificDates,
                                    minDate: get_effective_date(),
                                    maxDate: get_expiry_date()
                                });
                            }
                        });
                    });


                    $("#vehiclesListSelect").perfectScrollbar({
                        wheelSpeed: 0.3,
                        wheelPropagation: true,
                        minScrollbarLength: 10,
                        suppressScrollX: true
                    });
                }
                //////////////******/////////////////////////////////////////


            }).dialog('open');
        } //  END OF ID TEST (CHECKING IF THE COMP SHORTENING CODE IS SET)

    }); //END CLICK
});

function editdrivers() {
    window.location.replace("/Policy/AddPolicyVehDrivers/" + $("#hiddenid").val() + "?Edit=" + 1);
}

function CoverNoteHistory(polid, vehid) {
    window.location.replace('/VehicleCoverNote/History/?id=' + vehid+'&polid='+polid);
}

$(function () {
    $(".quickperiod").on('click', function () {
        period = $(this).val();
        UpdatePeriod();
    });
});

function UpdatePeriod() {
      
    expiry.setDate(effective.getDate()-1);
    expiry.setYear(effective.getFullYear());
    expiry.setMonth(effective.getMonth() + parseInt(period));
    expiry.setHours(23); expiry.setMinutes(59);
    $("#end").val(expiry.dateFormat('d M Y h:i A'));
}


function get_effective_date() {
    var effective;
    if ($("#start").val() != "") effective = new Date($("#start").val());
    else effective = new Date($("#sysdate").val());
    return effective;
}

function get_expiry_date() {
    var expiry;
    if ($("#end").val() != "") expiry = new Date($("#end").val());
    else expiry = new Date($("#sysdate").val());
    return expiry;
}

function DisableSpecificDates(date) {

    var m = date.getMonth();
    var d = date.getDate();
    var y = date.getFullYear();

    // First convert the date in to the mm-dd-yyyy format 
    // Increment the month count by 1 (necessary)
    var currentdate = (m+1) + '-' + d + '-' + y;

    // We will now check if the date belongs to disableddates array 
    for (var i = 0; i < InvalidDates.length; i++) {

        var thisDate = new Date(currentdate);

        if ( thisDate >= new Date(InvalidDates[i]) && thisDate <= new Date(InvalidDatesEnd[i])) {
            return [false];
        }
    }
}


/////Mass approval
function massApproval(certs, typeofDoc, canPrint) {

    var ThisUrlCall ;var UrlCall;var title;

    if (typeofDoc == 'VehicleCertificate') 
    {
        ThisUrlCall = '/VehicleCertificate/ApproveCert/' + '?certificateId=';
        UrlCall = '/VehicleCertificate/MassApproval' + '?certs=';
    }
    else
    {
        ThisUrlCall ='/VehicleCoverNote/Approve/';
        UrlCall = '/VehicleCoverNote/MassApproval' + '?coverNotes=';
    }

    switch (typeofDoc) {
        case 'VehicleCertificate':
            title = 'Certificate';
            break;
        case 'VehicleCoverNote':
            title = 'Cover Note';
            break;
        default:
            break;
    }

    $("#vehicle-modal").dialog({
        modal: true,
        autoOpen: false,
        width: 800,
        title: "Mass " + title + " Approval",
        height: 600,
        buttons: {
            Approve: function () {
                ApprovedList = [];
                if ($(".vehcheck:checked").length > 0) {
                    $(".vehcheck:checked").each(function () { //Loading the data into array for sending
                        ApprovedList[ApprovedList.length] = $(this).prop('id');
                        $.ajax({
                            type: "POST",
                            url: ThisUrlCall + ApprovedList[ApprovedList.length - 1],
                            cache: false,
                            success: function (html) { }
                        });
                    });

                    $("#vehicle-modal").dialog("close");
                    $("#vehicle-modal").html("");

                    if (canPrint) massPrint(ApprovedList, typeofDoc);
                    else {
                        $("<div>Approvals Successfull!</div>").dialog({
                            title: "Success",
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    window.location.replace("/Policy/Details/" + $("#hiddenid").val());
                                }
                            }
                        });
                    }
                }
                else {
                    $('<div>No vehicle was selected</div>').dialog({
                        modal:true,
                        title: 'Alert!',
                        buttons:{
                            OK:function(){
                                $(this).dialog('close');
                            }
                        }
                    });
                }

            },
            Cancel: function () {
                $("#vehicle-modal").dialog("close");
                $("#vehicle-modal").html("");
                window.location.replace("/Policy/Details/" + $("#hiddenid").val());
            }
        }
    });

    $("#vehicle-modal").load(UrlCall + certs, function () { }).dialog('open');
}


/////Mass approval
function massPrint(certs, typeofDoc) {
    var PrintLists = [];
    var ThisUrlCall;var UrlCall;
    if (typeofDoc == 'VehicleCertificate') {
        ThisUrlCall = '/VehicleCertificate/PrintCertificate/' + '?certs=';
        UrlCall = "/VehicleCertificate/MassPrint" + '?certs=';
    }
    else {
        ThisUrlCall = '/VehicleCoverNote/PrintCoverNote/' + '?coverNotes=';
        UrlCall = "/VehicleCoverNote/MassApproval" + '?coverNotes=';
    }

    var title = '';
    switch (typeofDoc) {
        case 'VehicleCertificate':
            title = 'Certificate';
            break;
        case 'VehicleCoverNote':
            title = 'Cover Note';
            break;
        default:
            break;
    }

    $("#vehicle-modal").dialog({
        modal: true,
        autoOpen: false,
        width: 800,
        title: "Mass " + title + " Print",
        height: 600,
        buttons: {
            Print: function () {
                if ($(".vehcheck:checked").length > 0) {
                    $(".vehcheck:checked").each(function () { //Loading the data into array for sending
                        PrintLists[PrintLists.length] = $(this).prop('id');
                    });
                    $.ajax({
                        type: "POST",
                        traditional: true,
                        url: ThisUrlCall + PrintLists,
                        async: false,
                        dataType: "json",
                        success: function (data) {
                            if (data.result) {
                                window.open("/Pdf/ViewPdf/");
                                window.location.replace("/Policy/Details/" + $("#hiddenid").val());
                            }
                        }
                    });
                    $("#vehicle-modal").dialog("close");
                    $("#vehicle-modal").html("");
                }
                else {
                    $('<div>No vehicle was selected</div>').dialog({
                        modal: true,
                        title: 'Alert!',
                        buttons: {
                            OK: function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                }
            },
            Cancel: function () {
                $("#vehicle-modal").dialog("close");
                $("#vehicle-modal").html("");
                window.location.replace("/Policy/Details/" + $("#hiddenid").val());
            }
        }
    });
    $("#vehicle-modal").load(UrlCall + certs, function () { }).dialog('open');
}

function changeSelectAll(e){
    $(".vehcheck").each(function () {
        $(this).prop("checked", $(e).is(":checked"));
    });
}

function update_policy_no_required() {
    if ($(".no_policy_no_check").is(":checked")) {
        $("#policyNum").prop('required', false);
        $("#policyNum").addClass('read-only');
        $("#policyNum").prop('readonly', true);
        $("#policyNum").val("");

        if (typeof $('.brokerPolicyCreate').val() != 'undefined') {
            $('.brokerPolicyCreate').show();
            $('#BrokerPolicyNum').prop('required', true);
            $('#BrokerPolicyNum').trigger('keyup');
        }
        if (typeof $('.brokerPolicyEdit').val() != 'undefined') {
            $('.brokerPolicyEdit').show();
            $('#BrokerPolicyNum').prop('required', true);
            $('#BrokerPolicyNum').trigger('keyup');
        }

    }
    else {
        $("#policyNum").prop('required', true);
        $("#policyNum").removeClass('read-only');
        $("#policyNum").prop('readonly', false);

        if (typeof $('.brokerPolicyCreate').val() != 'undefined') {
            $('.brokerPolicyCreate').hide();
            $('#BrokerPolicyNum').val("");
            $('#BrokerPolicyNum').prop('required', false);
            $('#BrokerPolicyNum').trigger('keyup');
        }
        if (typeof $('.brokerPolicyEdit').val() != 'undefined') {
            $('.brokerPolicyEdit').hide();
            $('#BrokerPolicyNum').val("");
            $('#BrokerPolicyNum').prop('required', false);
            $('#BrokerPolicyNum').trigger('keyup');
        }
    }

    $("#policyNum").trigger("change");
}

$(function () {
    $(".activate_policy").on('click', function () {
        $.ajax({
            url: '/Policy/activate_policy/' + $("#hiddenid").val(),
            cache: false,
            success: function(json){
                if (json.result){
                    $("<div></div>").text("This policy has been reactivated. Press OK to proceed.").dialog({
                        modal: true,
                        title: "Activate",
                        buttons:{
                            Ok: function(){
                                $(this).dialog("close");
                                window.location.reload();
                            }
                        }
                    }); 
                }
            }
        });
    });
});

$(function () {

    if (typeof $('.brokerPolicyCreate').val() != 'undefined') {
        $('.brokerPolicyCreate').hide();
    }
    if (typeof $('.brokerPolicyEdit').val() != 'undefined') {
       if($('#BrokerPolicyNum').val() != '' && $('#BrokerPolicyNum').val() != null)
           $('.brokerPolicyEdit').show();
        else
            $('.brokerPolicyEdit').hide();
    }

});