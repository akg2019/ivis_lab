﻿$(function () {


    $('#fitnessExpiry').datetimepicker({
        id: 'fitExpiry',
        value: "",
        format: 'd M Y',
        step: 5,
        lang: 'en-GB',
        timepicker: false,
        closeOnDateSelect:true,
        onSelectDate: function (ct) {
            $("#fitnessExpiry").val(ct.dateFormat('d M Y'));
        }
    });

});

