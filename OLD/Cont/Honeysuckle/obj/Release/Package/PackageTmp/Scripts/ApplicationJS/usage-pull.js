﻿/*$(function () {
    $(document).on('focus', '.usage_dropdown', function () {
        loadDropDown($(this));
    });
});*/

function loadDropDown(e){
var polcovid = getPolicyCoverId();
        var $usage_dropdown = $(e);
        //$($usage_dropdown).html(""); //dunno
        $.ajax({
            url: '/Usage/CoverUsagesJson/' + polcovid,
            cache: false,
            async:false,
            success: function (data) {
                if (data.success) {
                    $($usage_dropdown).html(updateUsageDropDown(data.data));
                }
                else {
                    alert("bad permissions");
                }
            },
            error: function () {
                alert("failed");
            }
        });
}

function getPolicyCoverId() {
    var id = 0;
    $(".covers_dropdown").children("option").each(function () {
        if ($(this).prop('selected')) {
            id = $(this).val();
            return id;
        }
    });
    return id;
}

function updateUsageDropDown(obj){
    var html = "";
    //var obj = JSON.parse(json);
    for(i = 0; i < obj.length; i++){
        html += "<option value=\"" + obj[i].ID + "\">" + obj[i].usage + "</option>";
    }
    return html;
}

$(function () {
    $('.usage_dropdown').each(function () {
        loadDropDown($(this));
        var usageID = $(this).parents('.useDropDown').find('.vehicle-usage-id').val();
        if (usageID != 0) {
            $(this).children('option').each(function () {
                if (usageID == $(this).val()) {
                    $(this).prop('selected', true);
                    return;
                }
            });
        }
    });
    $('.usage_dropdown').trigger("change");
});

$(function () {
    $('html').on('change', '.usage_dropdown', function () {
        var usageID;
        $(this).children('option').each(function () {
            if ($(this).prop('selected')) {
                usageID = $(this).val();
                return;
            }
        });
        $('.vehicle-usage-id').val(usageID);
    });
});
