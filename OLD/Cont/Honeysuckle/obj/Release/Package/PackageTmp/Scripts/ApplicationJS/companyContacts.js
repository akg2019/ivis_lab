﻿
        var selected = [];
        var idNumbers = [];
        var checkIfAdded = 0;
        var scrollHeight;
        var noUsers = "<div class=\"rowNone\">There Are No Users. Please add one. </div>";
        var UsersHidden = "<div class=\"rowNone\"> Please search the system for existing users, belonging to this company. </div>";

        $(function () {
            $("#addUserContact").click(function () {

                $("#user-modal").dialog({
                    modal: true,
                    autoOpen: false,
                    width: 700,
                    height: 600,
                    title: "User List",
                    buttons:
                     {
                         'Add Selected':
                      function () { getUsers(); },

                         Cancel:
                      function () {

                          $('#user-modal').dialog('close');
                          $('#user-modal').html("");
                          clearDialog();
                      }

                     } //END OF BUTTONS
                });

                $("#user-modal").load('/User/UserList?Id=' + $("#compID").val(), function (value) {

                    if (value.substring(2, 6) == 'fail') { //then user does not have the permissions
                        $(this).dialog('close');
                        $(this).html("");
                        $(this).dialog().dialog("destroy");

                        $("<div>You do not have the required permissions to complete this task. </div>").dialog({
                            title: 'Alert!',
                            modal:true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog("close");
                                    $(this).dialog().dialog("destroy");
                                }
                            }
                        });
                    }

                    else {
                        if ($(document).find(".user-row").length <= 50)
                            $(document).find(".user-row").show();

                        else $(document).find(".user-row").hide();

                        scrollHeight = $("#user").height();

                        $("#user").perfectScrollbar({
                            wheelSpeed: 0.3,
                            wheelPropagation: true,
                            minScrollbarLength: 10,
                            suppressScrollX: true
                        });

                        $("#selected-user").perfectScrollbar({
                            wheelSpeed: 0.3,
                            wheelPropagation: true,
                            minScrollbarLength: 10,
                            suppressScrollX: true
                        });
                    }

                }).dialog('open');
            });
        });

        $(function () {
            $(document).on("click", ".remove", function () {

                var deleteItem = $(this).parents("div.items:first");

                $("<div>You are about to delete this contact. Are you sure you want to do this? </div>").dialog({
                    title: 'Alert!',
                    modal:true,
                    buttons: {
                        Ok: function () {
                            deleteItem.remove();
                            $(this).dialog("close");
                            $(this).dialog().dialog("destroy");
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                            $(this).dialog().dialog("destroy");
                        }
                    }
                });

            });
        });


        $(document).on("keyup", ".modalSearch", function (event) {

            var text = $(".modalSearch").val().toLowerCase().trim();
            if (event.which == 13) {
                searchData(text);
            }
        });


      // Clearing the arrays
        function clearDialog() {
          
            selected = [];
            idNumbers = [];
            checkIfAdded = 0;
        }

        function updateView() {
            for (var x = 0; x < selected.length; x++) {
                $("#selected-user").append(selected[x]);
            }

            $("#user").children(".user-row").each(function () {

                //If the user is already on display, in the select box, then it doesn't need to be shown in the search area again 
                // (this is needed for when the modal is called back, and all the insured are called from the database (to not include duplicates)
                testIfAdded($($(this).children(".user-check").children(".usercheck")).prop('id'));

                if (checkIfAdded == 1)
                    $(this).hide();

                checkIfAdded = 0;

            });

            //Resizing the user list div, so the scroll works
            //$("#user").height(scrollHeight);

            if ($("#user").height() + $("#selected-user").height() < 380)
                $("#user").height($("#user").height() + 20);

            else if ($("#user").height() + $("#selected-user").height() >= 380)
                $("#user").height($("#user").height() - 20);

        }


        function testIfAdded(id) {
            for (var z = 0; z < idNumbers.length; z++) {
                if (idNumbers[z] == id)
                    checkIfAdded = 1;
            }
        }

        function removeELement(id) { //This function removes the element from the arrays
            for (var y = 0; y < selected.length; y++) {
                if (idNumbers[y] == id) {
                    selected.splice(y, 1);
                    idNumbers.splice(y, 1);
                  //  scrollHeight += 20; //Re-adjusting the height needed for the scroll to work nicely
                }
            }
            updateView();
        }


        function AddPersonSelected(id) {
            var err = "<div>There was an error with adding the user. Please try again. If the problem persists, please contact your administrator.</div>"

            $.ajax({
                type: "POST",
                url: '/User/UserListItemSelected/' + id,
                cache: false,
                success: function (data) {

                    testIfAdded(id);

                    if (checkIfAdded != 1) {
                        selected.push($(data));
                        idNumbers.push(id);
                       // scrollHeight -= 20; //Re-adjusting the height needed for the scroll to work nicely
                    }
                    updateView();
                },
                error: function () {
                    $(err).dialog({
                        title: "Error",
                        modal:true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog('close');
                            }
                        }
                    })
                }
            });   //END OF AJAX CALL
            checkIfAdded = 0; //Reset the variable
        }



    function changeCheckBox(e) {

        //Stores search value, as it breaks if the value is not stored before the div is manipulated
        var searchValue = $(e).parents(".insured-modal").find(".modalSearch").val();

        $(e).parent().parent().remove();

        if ($(e).parent().parent().hasClass("selected")) {
           
            var alreadyAdded = false;

            $("#user").children(".user-row").each(function () {
                if ($($(this).children(".user-check").children(".usercheck")).prop('id') == e.id) {
                    alreadyAdded = true;
                    return false;
                }
            });

            if (alreadyAdded == false) {
                $("#user").append($(e).parent().parent().toggleClass("selected"));
            }
            removeELement(e.id);
          }
        else {
            AddPersonSelected(e.id);
        }
        searchData(searchValue);
      }


      function searchOptionsUsers(e) {
          var filter = $(e).parents(".insured-modal").find(".modalSearch").val();
          searchData(filter);
      }

      function searchData(filter) {
          $(".rowNone").remove();
          if (filter.trim().length == 0) {

              if ($(document).find(".user-row").length <= 50)
                  $(document).find(".user-row").show();

              else $("#user").children(".user-row").hide();
          }
          else {

             // if (typeof $(".rowNone").val() !== 'undefined') $(".rowNone").remove();

                $("#user").children(".user-row").each(function () {

                  //If the person is already on display, in the select box, then it doesn't need to be shown in the search area again 
                  // (this is needed for when the modal is called back, and all the insured are called from the database (to not include duplicates)
                  testIfAdded($($(this).children(".user-check").children(".usercheck")).prop('id'));

                  if (checkIfAdded != 1)
                      if (($(this).children(".username").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".trn").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".name").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1)) {
                          $(this).show();

                          if ($("#user").height() + $("#selected-user").height() < 380)
                              $("#user").height($("#user").height() + 20);

                      }
                      else {
                          $(this).hide();
                      }

                  checkIfAdded = 0;
              });

          } //End of else

          redrawList();

          if ($("#user").height() == 0 && !$($(".user-row")[0]).is(':hidden'))
              $(document).find("#user").prepend(noUsers);

          else if (!$('.user-row').is(':visible'))
              $(document).find("#user").prepend(UsersHidden);
          

      }


      function getUsers() {

            var chkArray = [];
            var checkAdd = 0;
            var UserIds = [];

            var valIds = []; //Store the values of the ids added already
            var count = 0;
            var count1 = 0;

            //Counting the amount of users already added to the view
            $(".userId").each(function () {
                count++;
            });

            //Counting the amount of companies selected
            $(".usercheck:checked").each(function () {
                count1++;
            });

            if (count1 == 0) //No users have been selected 
            {
              $('<div> You have not selected a user to be added. Please do so, and try again. </div>').dialog({
                  title: 'Alert',
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                            return false;
                        }
                    }
                });
            }

            else {
                if (count > 0) {

                    var Contact = false;

                    //Stroring all the already added Ids in an array
                    for (var x = 0; x < count; x++) {
                        valIds[x] = $(".userId")[x].value;
                    }

                    //Checking each selected id against the ones already added
                    for (var y = 0; y < count1; y++) {
                        for (var z = 0; z < count; z++)

                            if ($(".usercheck:checked")[y].id == valIds[z]) {
                                checkAdd = 1;
                                Contact = true;
                            }

                        if (checkAdd != 1) {
                            UserIds[UserIds.length] = $(".usercheck:checked")[y].id;
                        }

                        checkAdd = 0; //Variable is reset

                    } //END OF outer FOR


                    //IF AT LEAST ONE OF THE POLICY HOLDERS WAS ALREADY ADDED TO THE PAGE
                    if (Contact == true) {

                        $('<div> One or more contact that you have selected has already been added to the company contact list. This contact(s) will not be added again. However, any other chosen one will be. </div>').dialog({
                            title: 'Alert',
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }

                    for (var x = 0; x < UserIds.length; x++) {
                        $.ajax({
                            type: "POST",
                            url: '/User/UserContact/?userID=' + UserIds[x],
                            cache: false,
                            success: function (html) { $("#contact").append(html); $('.extNum,.telNum').trigger('keyup'); }
                        });
                    }
                }


                else {

                    var countUser = 1; //This is used to identify the first user added to the contact page

                    $(".usercheck:checked").each(function () {

                        $.ajax({
                            type: "POST",
                            url: '/User/UserContact/?userID=' + $(this).prop('id'),
                            cache: false,
                            success: function (html) {

                                $("#contact").append(html);
                                if (countUser == 1) {

                                    $(".phonePrimary").prop('checked', true);
                                    countUser = countUser +1;
                                }

                            }
                        });

                    });
                }

                $("#user-modal").html("");
                $("#user-modal").dialog('close');

            } //END OF ELSE

            selected = [];
            idNumbers = [];
            checkIfAdded = 0;

        } // END OF 'getValues' function


        ///Primary contact stuff
        $(function () {
            $("body").on("click", ".phonePrimary", function (event) {
                if ($("#" + event.target.id).is(':checked')) {
                    checked = true;
                }
                else {
                    checked = false;
                }
                $(".phonePrimary").prop('checked', false);
                $("#" + event.target.id).prop('checked', checked);
            });
        });


        $(function () {
            if ($('.error').text().trim().length != 0) {

                $(".error").dialog({
                    modal: true,
                    autoOpen: false,
                    title: "Alert!",
                    buttons:
                 {
                     OK:
                  function () {

                      $('.error').dialog('close');
                      $('.error').html("");

                  }
                 }
                });

                $('.error').dialog('open');
            }
        });

        $(function () {

            //if (typeof $('.telNum').val() != 'undefined') {

                $('html').on('keyup', '.telNum', function (e) {
                    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
                        $(this).val($(this).val().replace(/^[(]?(\d{3})[)]?/g, '($1)'));
                        $(this).val($(this).val().replace(/[)](\d{3})\-?/g, ')$1-'));

                        return true;
                    }
                    $(this).val($(this).val().replace(/[^\-()0-9]/g, ''));
                });
           // }
        });


     