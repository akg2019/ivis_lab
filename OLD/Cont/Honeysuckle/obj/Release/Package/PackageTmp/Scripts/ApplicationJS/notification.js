﻿var show = false;
var addcontact = false;

function ToggleNotificationClass() {

    $("#notificationtoggle").toggleClass("show");
    $("#notificationbar").toggleClass("show");
    $(".note").toggleClass("show");
    $("#notes").toggleClass("show");
    $("#notification-section").toggleClass("show");
    $("#seefullnotify").toggleClass("show");
    show = !show;

    $("#covercertstab ").removeClass("show");
    $("#covercertnote").removeClass("show");
};

function ToggleCoverCertClass() {

    $("#notificationtoggle").removeClass("show");
    $("#notificationbar").removeClass("show");
    $(".note").removeClass("show");
    $("#notes").removeClass("show");
    $("#notification-section").removeClass("show");
    $("#seefullnotify").removeClass("show");
    show = !show;


    $("#covercertstab ").toggleClass("show");
    $("#covercertnote").toggleClass("show");
}

$(function () {
    $('html').click(function (e) {
        if(e.target.id != "noting1" && e.target.id != "noting2" && ($("#covercertstab ").hasClass('show') || $(".note").hasClass("show"))){
             $("#notificationtoggle").removeClass("show");
            $("#notificationbar").removeClass("show");
            $(".note").removeClass("show");
            $("#notes").removeClass("show");
            $("#notification-section").removeClass("show");
            $("#seefullnotify").removeClass("show");
            $("#covercertstab ").removeClass("show");
            $("#covercertnote").removeClass("show");
            show = !show;
        }
    });
});

function GetUnseenNotifications() {
    $.ajax({
        url: "/Notification/GetUnnewNotifications/",
        cache: false,
        success: function (data) {
            var jsonData = JSON.parse(data);
            for (var i = 0; i < jsonData.length; i++) {
                var jsonDataItem = jsonData.pop();
                $.ajax({
                    url: "/Notification/ShowNotification/" + jsonDataItem.ID + "?show=" + show.toString(),
                    cache: false,
                    success: function (d) {
                        
                        var $response = $(d);
                        var heading = $(d).children(".note-meat").children(".note_heading").html();
                        var content = $(d).children(".note-meat").children(".note_content").html();

                        if (heading.length > 20) {
                            $response.children(".note-meat").children(".note_heading").html($.trim(heading.substr(0, 20)) + "...");
                        }
                        if (content.length > 20) {
                            $response.children(".note-meat").children(".note_content").html($.trim(content.substr(0, 20)) + "...");
                        }

                        $("#notesscroll").prepend($response.hide().slideDown(500));
                        
                        UpdateMessageCount();
                    }
                });
                return false;
            }
        }
    });
    return false;
};

var exists = false;



$(document).ready(function () {
    setInterval(function () {
        if (!exists) if ($(".notification-section").children().length > 1) exists = true;
        if (exists) GetUnseenNotifications();
    }, 60000);
});

function UpdateMessageCount() {
    $(".notifications .note-count").find(".count").html("");
    if ($(".note").length > 0) {
        $(".note-count").show();
        $(".notifications .note-count").find(".count").html($(".note").length);
    }
    else $(".note-count").hide();
}

function CoverCertCount() {

    $("#covercert-notification .note-count").find(".count").html("");
    if ($(".covercertnote").length > 0) {
        $("#covercert-count").show();
        $("#covercert-notification .note-count").find(".count").html($(".covercertnote").length);
    }
    else $("#covercert-notification .note-count").hide();

}

$(function () {
    UpdateMessageCount();
    CoverCertCount();

    $(".note_heading, .note_content").each(function () {
        if ($(this).html().length > 20) {
            $(this).html($.trim($(this).html().substr(0, 20)) + "...");
        }
    });
    $('body').on('click', '.note_remove', function (event) {
        $.ajax({
            url: this.href,
            cache: false,
            success: function (html) {
                $(event.target).closest(".note").toggle('slide', { direction: 'right' }, 500);
                setInterval(function () {
                    $(event.target).closest(".note").remove();
                    UpdateMessageCount();
                }, 600);
            },
            error: function () {
                $("<div>There was an error in your request. Please try again. If the error persists please contact your administrator</div>").dialog({
                    title: "Alert!",
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }
        });
        return false;
    });


    $('body').on('click', '.covercert_remove', function (event) {
        $.ajax({
            url: this.href,
            cache: false,
            success: function (html) {
                $(event.target).closest(".covercertnote").toggle('slide', { direction: 'right' }, 500);
                setInterval(function () {
                    $(event.target).closest(".covercertnote").remove();
                    CoverCertCount();
                }, 600);

                if (typeof $('#startpage').val() !== 'undefined') {
                    var coverCertType = $(event.target).parents(".covercertnote").find("#note-hidden-type").val();
                    var coverCertID = $(event.target).parents(".covercertnote").find("#note-covercert-id").val();

                    $(".covercert.row").each(function () {
                        if (($(this).find(".id").val() == coverCertID) && ($(this).find(".CoverCertHidType").val() == coverCertType)) {
                            $(this).remove();
                        }

                    });
                    rePrintRows($(".covercert.row"));
                }
            },
            error: function () {
                $("<div>There was an error in your request. Please try again. If the error persists please contact your administrator</div>").dialog({
                    title: "Alert!",
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }
        });
        return false;
    });

    $("body").on('click', '.note', function (e) {
        if ($(e.target).hasClass("note_remove")) return; //stops the JS from dropping down/sliding up when user clicks links/buttons
        else {
            var id = $(this).find(".NID").val();
            window.location.replace("/Notification/ViewMessage/" + id);
        }
    });
});

function rePrintRows(rows) {

    var rows_list_html = "";
    for (var x = 0; x < rows.length; x++) {
        var comp = rows[x];
        if (($(rows[x]).hasClass("odd") && x % 2 == 1) || ($(rows[x]).hasClass("even") && x % 2 == 0)) rows_list_html += comp.outerHTML;
        if (($(rows[x]).hasClass("odd") && x % 2 == 0) || ($(rows[x]).hasClass("even") && x % 2 == 1)) {
            comp = $(comp.outerHTML).toggleClass("odd").toggleClass("even")[0];
            rows_list_html += comp.outerHTML;
        }
    }
    $("#covercerts").html(rows_list_html);
}
