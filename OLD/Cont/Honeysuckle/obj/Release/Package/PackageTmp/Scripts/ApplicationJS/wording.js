﻿$(function () {

    $(".action").hide();

    $(".word-check").on('change', function () {
        UpdateExtensionCode();
    });

    if ($("#page").val() != undefined) {
        if ($("#page").val() == "edit") {
            var code = $("#realccode").val();
            $("#certinscodedropdown").children("option").each(function () {
                if ($(this).val() == code) {
                    $(this).prop('selected', true);
                    return;
                }
            });
        }
    }

    $("#certtype").on('keyup', function () {
        UpdateCertificateID();
    });

    $("#certinscodedropdown").on('change', function () {
        UpdateCertificateID();
    });

    $(".rmwording").on('click', function () {
        RemoveWording($(this));
    });

    $(".editwording").on('click', function () {
        UpdateWording($(this));
    });

    $('.table-data').perfectScrollbar({
        wheelSpeed: 0.3,
        wheelPropagation: true,
        minScrollbarLength: 10,
        suppressScrollX: true
    });

    $(".word-count .count").html(count_words());

});

function count_words() {
    return $(".wording.data:visible").length;
}

function UpdateCertificateID() {
    
    var cid = "";

    cid += $("#certtype").val() + ":";
    cid += $("#certinscodedropdown").val() + ":";
    cid += $("#extcode").val();

    $("#certid").val(cid);
    $("#certid").trigger("change");

}

function UpdateExtensionCode() {
    var ext = "";

    if ($("#excdri").is(":checked")) ext += "x";
    if ($("#regdri").is(":checked")) ext += "r";
    if ($("#mulveh").is(":checked")) ext += "m";
    if ($("#excins").is(":checked")) ext += "e";
    if ($("#regown").is(":checked")) ext += "o";
    if ($("#sch").is(":checked")) ext += "s";

    $("#extcode").val(ext);
    $("#extcode").trigger("change");

    UpdateCertificateID();

}



var successdelete = "<div>You have successfully deleted this wording.</div>";
var deleteprompt = "<div>Are you certain you want to delete this wording?</div>";
var deleteerror = "<div>There was an error. Please try again. If this error persists please contact your administrator</div>";
function RemoveWording(e) {
    var id = $(e).parents(".row").find(".wordingid").val();
    $(deleteprompt).dialog({
        title: "Delete",
        modal:true,
        buttons: {
            Ok: function () {
                $(this).dialog('close');
                $.ajax({
                    url: '/TemplateWording/Delete/' + id,
                    cache: false,
                    success: function (data) {
                        if (data.result) {
                            $(successdelete).dialog({
                                title: 'Success!',
                                modal:true,
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog('close');
                                        $(e).parents(".row").remove();
                                    }
                                }
                            });
                        }
                        else {
                            $(deleteerror).dialog({
                                title: 'Error!',
                                modal: true,
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                    },
                    error: function () {
                        $(deleteerror).dialog({
                            title: 'Error!',
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                });
             },
        Cancel: function () {
            $(this).dialog('close');
            $(this).dialog('destroy');
        }
      }
   });
}

function UpdateWording(e) {
    var id = $(e).parents(".row").find(".wordingid").val();
    window.location.replace('/TemplateWording/Edit/' + id);
}

function CreateNewTemp() {
    window.location.replace('/TemplateWording/Create/');
}

function UploadTemp() {
    window.location.replace('/TemplateWording/Upload/');
}

function TemplateTags(type, e) {
    var text = "";
    /*
    <[Risk Items]Veh Year> 
    <[Risk Items]Veh Make> 
    <[Risk Items]Veh Model> 
    <[Risk Items]Veh Model Type> 
    <[Risk Items]Veh Extension>
    <[Risk Items]Veh Reg No>
    <[Policy Info]Main Insured> ( indicate all the insureds)
    <[Risk Items]Veh Main Driver Name> (indicate who is the main driver)
    <[Risk Items]Veh Driver Names> (List all the drivers)
    */
    switch (type) {
        case "vehyear":
            text = "<[Risk Items]Veh Year>";
            break;
        case "vehmake":
            text = "<[Risk Items]Veh Make>";
            break;
        case "vehmod":
            text = "<[Risk Items]Veh Model>";
            break;
        case "vehmodtype":
            text = "<[Risk Items]Veh Model Type>";
            break;
        case "vehext":
            text = "<[Risk Items]Veh Extension>";
            break;
        case "vehregno":
            text = "<[Risk Items]Veh Reg No>";
            break;
        case "ins":
            text = "<[Policy Info]Main Insured>";
            break;
        case "main":
            text = "<[Risk Items]Veh Main Driver Name>";
            break;
        case "names":
            text = "<[Risk Items]Veh Driver Names>";
            break;
        case "auth":
            text = "<[Risk Items]Veh Authorized Drivers>";
            break;
        default:
            text = "fail";
            break;
    }
    if (text == "fail") {
        $("<div>There was an error</div>").dialog({
            title: "Error!",
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog('close');
                }
            }
        })
    }
    else {
        e = $(e).parents(".text-area-section").find("textarea");
        $(e).val(text + "\n" + $(e).val());
    }
}

