﻿
var compTRNFocus = false;
var compTRNSet = false;
var TRN1 = $("#trn").val();

$(function () {
    if (typeof $("#trn").val() != 'undefined')
        if ($("#trn").val().trim().length > 0) {
            compTRNSet = true;
        }
});


$(function () {
    $("#edit-trn").on('click', function () {
        $('<div>You are about to edit this company\'s TRN. Are you sure you want to do this?</div>').dialog({
            title: 'Edit TRN Dialog',
            resizable: false,
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    $("#trn").prop('readonly', false);
                    $("#trn").toggleClass('read-only');
                    $("#trn").focus();
                    compTRNFocus = true;
                    
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    });
});



$(function () {
    $("#trn").blur(function () {
        if (compTRNFocus) {
            var result = false;
            var regexTRN = /^(([ ]*[01][0-9]{12}[ ]*)|([ ]*[0][0-9]{8}[ ]*))$/;
            var regexComTRN1 = /^[ ]*[0][0-9]{8}[ ]*$/;  //regex to ensure that the TRN is an integer, that starts with a 0 before checking for its existence in the system

            if ($("#trn").val() == '' || (!regexTRN.test($("#trn").val()) /*&& !regexComTRN1.test($("#trn").val())*/)) {
                alert('Please enter a valid Company TRN. The Company TRN must be 13 digits long when \'Trading As\' applies, or begin with a 0 when 9 digits long');
                return false;
            }

            else
                $.getJSON("/Company/GetCompanyTRN/?TRN=" + $("#trn").val() + '&compID=' + $('#compID').val() + '&insured=' + false, function (data) {
                    if (!data.error) {
                        if (data.result) {
                            //company already exists

                            $("<div>TRN already exists for another company. The Company TRN must be unique.</div>").dialog({
                                modal: true,
                                title: "Company Exists",
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog("close");

                                        $("#trn").val("");
                                        $("#trn").focus();
                                        $("#trn").trigger('keyup');
                                    }
                                }
                            });
                        }

                        else {
                            //TRN Not in the system yet
                            if ($("#trn").val().trim().length != 0) compTRNSet = true;
                            compTRNFocus = false;
                            $("#trn").toggleClass('read-only');
                            $("#trn").prop('readonly', true);

                        }
                    }
                    else {
                        //ERROR
                        $("<div>" + data.message + "</div>").dialog({
                            modal: true,
                            title: "Alert",
                            buttons: {
                                Ok: function () {
                                    $("#trn").val("");
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }

                });
        }
    })
});
