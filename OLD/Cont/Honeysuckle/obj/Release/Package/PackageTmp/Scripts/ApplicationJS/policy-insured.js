﻿function update_type_all(){
    var people_count = 0; var company_count = 0;
    people_count = $(".insured").length;
    company_count = $(".company").length;
    update_type(people_count, company_count);
};

function update_type(people, companies) {

    if (people == 1 && companies == 0)
        $("#insuredType").val("Individual");
    else if (people == 0 && companies == 1)
        $("#insuredType").val("Company");
    else if((people > 1 && companies == 0) || (people == 0 && companies > 1))
        $("#insuredType").val("Joint Insured");
    else if(people == 1 && companies == 1)
        $("#insuredType").val("Individual/Company");
    else if ((people > 1 && companies > 0) || (people > 0 && companies > 1))
        $("#insuredType").val("Blanket");
    else
        $("#insuredType").val("");


}