﻿

//This function allows a user, to send a message to the company admnis of a company (under which a vehicle has a certificate)
function MessageCompAdmin() {

    var vehCertId = $("#certId").val();

    $("#SendMail").dialog({
        title:"Message Certificate Approval Users",
        modal: true,
        autoOpen: false,
        width: 500,
        height: 450,
        buttons:
                   {
                       Send: function () {

                       var selected = [];
                           var sendto = document.getElementById("sendto");
                           for (var i = 0; i < sendto.options.length; i++) {
                               if (sendto.options[i].selected) {
                                   selected.push(sendto.options[i].value);
                               }
                           }

                        if (selected.length != 0) {

                        //var vehCertId = $("#vehCertID").val();
                        var heading = $("#heading").val();
                        var content = $("#content").val();

                        var parms = { vehId: vehCertId, admins: selected, heading: heading, content: content };

                        $.ajax({
                            type: "POST",
                            traditional: true,
                            url: "/Notification/sendNoteToComp/",
                            async: false,
                            data: parms,
                            dataType: "json",
                            success: function (data) {

                                if (data.result) {

                                    $("#SendMail").html("");
                                    $("#SendMail").dialog("close");
                                    $("<div>Message sent!</div>").dialog({
                                        title: "Alert",
                                        modal:true,
                                        buttons:
                                        {
                                            OK:
                                        function () {
                                            $(this).dialog("close");
                                            $(this).html("");
                                        }
                                        }

                                    });
                                }
                            }
                        });

                      //Removing the button
                      $("#messageAdmins").remove();
                   
                      $.ajax({
                            type: "POST",
                            traditional: true,
                            url: "/VehicleCertificate/PrintRequest/"+vehCertId,
                            async: false,
                            dataType: "json",
                            success: function (data) {}
                       });

                    } //END OF IF selected.length check-now to else
                    else {

                        $("<div>You have not selected any recipients, so you cannot send this message. Please select at least one recipient</div>").dialog({
                            title: "Alert",
                            modal:true,
                            buttons:
                                     {
                                         OK: function () {
                                                  $(this).dialog("close");
                                                  $(this).html("");
                                              }
                                       }

                             });

                    } //end of selected check

                   }, //END OF 'SEND' BUTTON

                  Cancel: function () 
                  {
                    $(this).dialog("close");
                    $(this).html("");
                }

             } //END OF BUTTONS

           });  // END OF JQUERY DIALOG

         $("#SendMail").load('/Notification/sendNote/' + vehCertId + '?type=' + 'Certificate').dialog('open');

     }; //END OF 'MessageCompAdmin' FUNCTION





    //This function allows a user, to send a message to the company admnis of a company (under which a vehicle has a certificate)
    function RequestCertApproval() {

        //var vehCertId = $(event.target).parent().children('.vehCertID').val();
        var vehCertId = $("#certId").val();

        $("#SendMail").dialog({
            modal: true,
            autoOpen: false,
            width: 500,
            height: 450,
            title: 'Request Certificate Approval',
            buttons:
            {
                Send: function () {

                    var selected = [];
                           var sendto = document.getElementById("sendto");
                           for (var i = 0; i < sendto.options.length; i++) {
                               if (sendto.options[i].selected) {
                                   selected.push(sendto.options[i].value);
                               }
                           }

                    if (selected.length != 0) {

                    //var vehCertId = $("#vehCertID").val();
                    var heading = $("#heading").val();
                    var content = $("#content").val();
                    
                    var parms = { vehId: vehCertId, admins: selected, heading: heading, content: content };

                    $.ajax({
                        type: "POST",
                        traditional: true,
                        url: "/Notification/sendNoteToComp/",
                        async: false,
                        data: parms,
                        dataType: "json",
                        success: function (data) {

                            if (data.result) {

                                $("#SendMail").html("");
                                $("#SendMail").dialog("close");
                                //Disabling the button
                                $("#reqApproval").remove();

                                $("<div>Message sent!</div>").dialog({
                                    title: "Alert",
                                    modal:true,
                                    buttons:
                                {
                                    OK: function () {
                                        $(this).dialog("close");
                                        $(this).html("");
                                    }
                                }
                                });
                            }
                        }
                    });
            } //END OF IF selected.length check-now to else
            else {

                $("<div>You have not selected any recipients, so you cannot send this message. Please select at least one recipient</div>").dialog({
                    title: "Alert",
                    modal:true,
                    buttons:
                               {
                                      OK: function () {
                                                  $(this).dialog("close");
                                                  $(this).html("");
                                              }
                                               }

                });

                     } //end of selected check
                }, //END OF 'SEND' BUTTON

                Cancel: function () {
                    $(this).dialog("close");
                    $(this).html("");
                }

            } //END OF BUTTONS

        });       // END OF JQUERY DIALOG

        $("#SendMail").load('/Notification/sendNote/' + vehCertId + '?type=' + 'ApproveCert').dialog('open');

    }; //END OF 'RequestCertApproval' FUNCTION

    function Print () {
        var thisCertID = $("#certId").val();

        $.ajax({
            url: "/VehicleCertificate/PrintCertificate/" +'?certs='+ thisCertID,
            async: false,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    window.open("/Pdf/ViewPdf/");

                }
                else if (!data.result && data.nocode) {
                    $("<div>You cannot print at this time. The Insurer code is not valid. Please contact your administrator to rectify this.</div>").dialog({
                        title: 'Alert!',
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                }
                else {
                    $("<div>There was an error. Please contact your administrator if the problem persists</div>").dialog({
                        title: 'Error!',
                        modal:true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                }
            },
            error: function () {
                $("<div>There was a networking error. Please contact your administrator if the problem persists</div>").dialog({
                    title: 'Error!',
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog('close');
                        }
                    }
                });
            }
        });
    };

    $(function () {
        $(".editCertificate").on('click', function () {
            var id = $("#certId").val();
            window.location.replace('/VehicleCertificate/Edit/' + id);
        });
    });