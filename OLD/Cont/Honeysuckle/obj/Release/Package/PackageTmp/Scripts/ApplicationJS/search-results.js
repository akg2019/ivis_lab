﻿$(function () {
    $(".search-term-rm").on("click", function () {
        var url = document.URL;
        var searchtext = url.substr(url.indexOf("?search="), url.length - url.indexOf("?search="));
        var urlbase = url.substr(0, url.indexOf("?search="));
        var searchvars = searchtext.substr(searchtext.indexOf("="), searchtext.length - searchtext.indexOf("="));
        searchtext = searchtext.substr(0, searchtext.indexOf("="));
        var searchtorm = $(this).parent().children(".search-term-content:first").text();
        if (searchvars.indexOf("%26" + searchtorm) > -1)
            searchvars = searchvars.replace("%26" + searchtorm, "");
        else {
            if (searchvars.indexOf(searchtorm + "%26") > -1)
                searchvars = searchvars.replace(searchtorm + "%26", "");
            else {
                searchvars = searchvars.replace(searchtorm, "");
            }
        }
        url = urlbase + searchtext + searchvars;
        window.location.replace(url);
    });
});


function advSearchPage() {
    window.location.replace("/Search/Advanced/");

}