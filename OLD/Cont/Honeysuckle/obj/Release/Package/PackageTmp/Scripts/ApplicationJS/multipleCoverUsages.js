﻿$(function () {

    $(".covers_dropdown").each(function () {
        var ThisCoverId = $(this).parent().find('.coverID').val();

        $(this).children().each(function () {

            if ($(this).val() == ThisCoverId) {
                $(this).prop('selected', true);
                return;
            }
        });

        $(this).prop("disabled", true);
    });

    $(".usage_dropdown").each(function () {
        var ThisUsageId = $(this).parent().find('.usageID').val();

        $(this).children().each(function () {

            if ($(this).val() == ThisUsageId) {
                $(this).prop('selected', true);
                return;
            }
        });
        $(this).prop("disabled", true);
    });


    $("#addusage").on('click', function () {

        $.ajax({
            type: "POST",
            traditional: true,
            url: "/PolicyCover/CoversUsagesSelect/" + $(".hidden-company-id").val(),
            async: false,
            success: function (html) {

                var usageSelect = $(html);
                var coverID = usageSelect.find('.covers_dropdown').val();

                $.ajax({
                    type: "POST",
                    url: '/Usage/UsageDropdownOptions/?coverID=' + coverID,
                    cache: false,
                    success: function (data) {

                        $.each(data, function (i, v) {

                            v = JSON.parse(v);

                            var elems = usageSelect.find('.usage_dropdown');
                            for (var i = 0; i < elems.length; i++) {

                                //clearing the data from the dropdowns

                                if (typeof elems[i].options !== 'undefined')
                                    for (p = elems[i].options.length - 1; p >= 0; p--) {
                                        elems[i].remove(p);
                                    }

                                // Placing the new options in the dropdown (if any)
                                for (c = 0; c < v.length; c++) {
                                    var option = document.createElement("option");
                                    option.text = v[c].usage;
                                    option.value = v[c].ID;

                                    elems[i].add(option);
                                }

                                $('.usage_dropdown').trigger('change');

                                if (elems[i].value != "")
                                    $(".usageIdentity").val(elems[i].value);
                                else $(".usageIdentity").val("");


                            } //END OF DROPDOWN FOR LOOP

                        }) //END OF FOREACH USAGE ITEM

                    } // END OF SUCCESS FUNCTION
                }); // END OF AJAX CALL FOR VEHICLE USAGES  

                $("#usages").append(usageSelect);
                usageSelect.find('.coverID').val(coverID);
            }
        });


    });

    $(document).on("click", ".savechanges", function (e) {

        var thisButton = $(this);
        var coverSelected = $(this).parent().find('.covers_dropdown').val();
        var usageSelected = $(this).parent().find('.usage_dropdown').val();

        var parms = { wording: $("#wordingID").val(), cover: coverSelected, usage: usageSelected };

        $.ajax({
            type: "POST",
            traditional: true,
            url: "/TemplateWording/AddCoverUsages/",
            async: false,
            data: parms,
            dataType: "json",
            success: function (data) {

                if (data.result) {

                    thisButton.parent().append("<input type=\"button\" value=\"Delete\" class=\"deletechanges inter-btn\"/> ");

                    //Disabling the association that is saved
                    thisButton.parent().find(".covers_dropdown").prop("disabled", true);
                    thisButton.parent().find(".usage_dropdown").prop("disabled", true);

                    thisButton.remove();

                    $("<div>Wording- Cover Usage association added</div>").dialog({
                        title: "Saved!",
                        modal: true,
                        buttons: {
                            OK: function () {
                                $(this).dialog("close");
                                $(this).dialog("destroy");
                            }
                        }
                    });
                }
                else {
                    $("<div>There was an error. Please ensure you are trying to add a unique association. </div>").dialog({
                        title: "Alert!",
                        modal: true,
                        buttons: {
                            OK: function () {
                                $(this).dialog("close");
                                $(this).dialog("destroy");
                            }
                        }
                    });
                }
            }
        }); //End of AJAX call
    });

    $("body").on("change", ".covers_dropdown", function (e) {

        var ThisDropdown = $(this);

        $.ajax({
            type: "POST",
            url: '/Usage/UsageDropdownOptions/?coverID=' + ThisDropdown.val(),
            cache: false,
            success: function (data) {

                $.each(data, function (i, v) {

                    v = JSON.parse(v);

                    var elems = ThisDropdown.parent().find('.usage_dropdown');
                    for (var i = 0; i < elems.length; i++) {

                        //clearing the data from the dropdowns

                        if (typeof elems[i].options !== 'undefined')
                            for (p = elems[i].options.length - 1; p >= 0; p--) {
                                elems[i].remove(p);
                            }

                        // Placing the new options in the dropdown (if any)
                        for (c = 0; c < v.length; c++) {
                            var option = document.createElement("option");
                            option.text = v[c].usage;
                            option.value = v[c].ID;

                            elems[i].add(option);
                        }

                        if (elems[i].value != "")
                            $(".usageIdentity").val(elems[i].value);
                        else $(".usageIdentity").val("");

                        ThisDropdown.parents('.coverUsagePair').find('.coverID').val(ThisDropdown.val());
                        ThisDropdown.parents('.coverUsagePair').find('.usage_dropdown').trigger('change');
                    } //END OF DROPDOWN FOR LOOP

                }) //END OF FOREACH USAGE ITEM

            } // END OF SUCCESS FUNCTION
        }); // END OF AJAX CALL FOR VEHICLE USAGES  
    });


    $("body").on('click', '.deletechanges', function () {

        var thisDiv = $(this).parent();
        var coverSelected = $(this).parent().find('.covers_dropdown').val();
        var usageSelected = $(this).parent().find('.usage_dropdown').val();

        var parms = { wording: $("#wordingID").val(), cover: coverSelected, usage: usageSelected };

        $("<div> You are about to delete this wording- Cover Usages asssociation. Are you sure you want to do this? </div>").dialog({
            title: "Alert!",
            modal: true,
            buttons: {
                OK: function () {
                    $(this).dialog("close");
                    $(this).dialog("destroy");

                    $.ajax({
                        type: "POST",
                        traditional: true,
                        url: "/TemplateWording/DeleteCoverUsages/",
                        async: false,
                        data: parms,
                        dataType: "json",
                        success: function (data) {

                            if (data.result) {

                                thisDiv.remove();
                                $("<div>Wording- Cover Usage association deleted</div>").dialog({
                                    title: "Deleted!",
                                    modal: true,
                                    buttons: {
                                        OK: function () {
                                            $(this).dialog("close");
                                            $(this).dialog("destroy");
                                        }
                                    }
                                });
                            }
                            else {
                                $("<div>There was an error. Please contact your administrator if the issue persists. </div>").dialog({
                                    title: "Alert!",
                                    modal: true,
                                    buttons: {
                                        OK: function () {
                                            $(this).dialog("close");
                                            $(this).dialog("destroy");
                                        }
                                    }
                                });
                            }
                        } //End of Buttons
                    }); //End of AJAX call

                }, //End of Ok button
                Cancel: function () {
                    $(this).dialog("close");
                    $(this).dialog("destroy");
                }
            } //End of Buttons
        }); //End of alert Dialog
    });
});

