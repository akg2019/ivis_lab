﻿var badresponse = "<div>The response you provided is incorrect. Please try again or contact your administrator to have him/her send you a new password.</div>"
$(function () {
    
    $("#responsebad").hide();

    if ($("#responsebad").text().length > 0) {
        $(badresponse).dialog({
            title: "Incorrect Response",
            modal:true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    }
});


$(function () {
    $("#Cancel").on("click", function () {
        window.location.replace("/Home/Index/");
    });
});