﻿var TRNmessage = "<div>You are about to edit the TRN of this user. Are you certain you want to do this?</div>";
var TRNbadmessage = "<div>The TRN you have selected is already taken (Please enter a valid TRN) and it must have exactly nine digits</div>";
var trnEdit = false;
var origTRN = "";

$(function () {


    $('#edit-trn').click(function () {
        origTRN = $("#trn-editPolHol.person-trn").val();
        $(TRNmessage).dialog({
            title: 'Edit TRN Dialog',
            resizable: false,
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    $("#trn-editPolHol.person-trn").removeClass('read-only');
                    $("#trn-editPolHol.person-trn").prop('disabled', false);
                    $("#trn-editPolHol").focus();
                    origTRN = $("#trn-editPolHol.person-trn").val().trim();
                    $('#edit-trn').prop('disabled', true);
                    trnEdit = true;
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });

    });

    $("#trn-editPolHol.person-trn").focus(function () { trnEdit = true });


    $("#trn-editPolHol.person-trn").blur(function () {

        if (trnEdit) {
            trnEdit = false;

            if (origTRN != $("#trn-editPolHol.person-trn").val().trim()) {
                // alert('hit');
                $.ajax({
                    url: '/Vehicle/GetUserByTRN/?id=' + $("#trn-editPolHol.person-trn").val().trim() + '&insured=true',
                    cache: false,
                    success: function (data) {
                        if (data.result) {
                            $('#InformationDialog').dialog({

                                resizable: false,
                                modal: true,
                                autoOpen: false,
                                width: 470,
                                title: "Person Exists",
                                height: 350,
                                buttons: {
                                    OK: function () {

                                        $('#fname').val(data.fname);
                                        $('#mname').val(data.mname);
                                        $('#lname').val(data.lname);

                                        $('#RoadNumber').val(data.roadno);
                                        $('#roadname').val(data.roadname);
                                        $('#roadtype').val(data.roadtype);
                                        $('#aptNumber').val(data.aptNumber);

                                        $('#city').val(data.city);
                                        $('.parishm #parishes').val(data.parishid);
                                        $('.parishm #parish').val(data.parishid);
                                        $('#country').val(data.country);

                                        if (data.longAddressUsed) $('#longAddress').val(data.longAddress);

                                        $('input').trigger('change');
                                        $('.parishm select').trigger('change');

                                        $("#trn-editPolHol").prop('readonly', true);
                                        $('#fname').prop('readonly', true);
                                        $('#mname').prop('readonly', true);
                                        $('#lname').prop('readonly', true);

                                        $("#trn-editPolHol").addClass('read-only');
                                        $('#fname').addClass('read-only');
                                        $('#mname').addClass('read-only');
                                        $('#lname').addClass('read-only');

                                        $('#RoadNumber').prop('readonly', true);
                                        $('#roadname').prop('readonly', true);
                                        $('#roadtype').prop('readonly', true);
                                        $('#aptNumber').prop('readonly', true);

                                        $('#city').prop('readonly', true);
                                        $('.parishm #parishes').prop('readonly', true);
                                        $('#country').prop('readonly', true);

                                        $('#RoadNumber').addClass('read-only');
                                        $('#roadname').addClass('read-only');
                                        $('#roadtype').addClass('read-only');
                                        $('#aptNumber').addClass('read-only');

                                        $('#city').addClass('read-only');
                                        $('.parishm #parishes').addClass('read-only');
                                        $('#country').addClass('read-only');

                                        $(this).dialog("close");
                                        $("#InformationDialog").html('');

                                    },
                                    Cancel: function () {
                                        $(this).dialog("close");
                                        $("#trn-editPolHol").val('');
                                        $("#trn-editPolHol").focus();
                                        $("#trn-editPolHol.person-trn").trigger('change');
                                        trnEdit = true;
                                    }
                                }

                            });
                            $("#InformationDialog").load('/People/FullPersonDetails/' + data.personid + "?create=" + true + "&taj=" + data.taj, function (value) { }).dialog('open');
                        }
                    },
                    error: function (data) {
                        $("#trn-editPolHol.person-trn").val('');
                        alert('Error validating data');
                    }
                });
            } //else alert('yoooo');
        }

        if (origTRN != $("#trn-editPolHol.person-trn").val().trim()) $('#trnChange').val(true); else $('#trnChange').val(false);

    });

});