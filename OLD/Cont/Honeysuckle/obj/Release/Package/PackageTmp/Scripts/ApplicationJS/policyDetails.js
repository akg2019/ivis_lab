﻿    //SETTING THE USAGE DROPDOWN TO THE CORRECT USAGE OF THE VEHICLE UNDER THE POLICY
    $(document).ready(function () {

          $(".chassisNumber").each(function () {

              var usagemodel = $(this).parent().children("#vehUsageID");
              var polID = $("#hiddenid").val();
              var dropdownbox = $(this).parent().parent().parent().children(".this_vehicle_dropdown").children(".useDropDown").children(".usage_dropdown");
              var usageID = usagemodel.val();

              dropdownbox.children().each(function () {
                  if ($(this).val() == usageID) {
                      $(this).prop('selected', true);
                      return;
                    }
                });
           
                //Disabling the usage dropdown menus
                dropdownbox.prop("disabled", true);

            }); //END OF Vehicle -chassis each block
        });

        $(function () {
            var Polcover = $("#CoverID").val();
            if (Polcover != 0) {
                $(".covers_dropdown").children().each(function () {
                    if ($(this).val() == Polcover) {
                        $(this).prop('selected', true);
                        return;
                    }
                });
            }
            $(".covers_dropdown").prop("disabled", true);
            $(".covers_dropdown").toggleClass("read-only");
        });


        $(function () {
            if (typeof $("#NewPolicyModal").val() !== 'undefined') {

                $("#NewPolicyModal").dialog({
                    modal: true,
                    width: 510,
                    title: "Policy Creation Complete",
                    height: 160,
                    buttons:
                    {
                        'Create Certificates': function () {
                            $(this).dialog("close");
                            $("#allCerts").click();
                        },

                        'Create Cover Notes': function () {
                            $(this).dialog("close");
                            $("#allCoverNotes").click();
                        },

                        Cancel: function () {
                            $(this).dialog("destroy");
                            $("#NewPolicyModal").text("");
                        }
                    }//End of buttons
                }); //End of dialog
            }
        });

        $(function () {
            if ($('#policyInfo') && $('#policyInfo').html() != "") {

                $('#policyInfo').dialog({
                    modal: true,
                    title: 'Policy Modified',
                    buttons: {
                        OK: function () {
                            $(this).dialog('close');
                            $(this).dialog('destroy');
                            $('#policyInfo').html('');
                        }
                    }
                });
            }

            
        });
      
        