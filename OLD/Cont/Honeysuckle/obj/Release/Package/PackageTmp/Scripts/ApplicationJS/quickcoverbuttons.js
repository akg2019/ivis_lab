﻿$(function () {

    $(".coverdays").click(function () {
       
        UpdateCoverPeriod($(this));
    });

});

function UpdateCoverPeriod(periodObj) {
    
    var period = periodObj.val();
    var DAY = 1000 * 60 * 60 * 24;
    var expiry = new Date(periodObj.parents('.vehicle-row').find('.expiryPicker').val());
    var effective = new Date(periodObj.parents('.vehicle-row').find('.effective').val());
    var expElem = periodObj.parents('.vehicle-row').find('.expiryPicker');

    if (period != "" && period != 'undefined') {
        if ((effective.getTime() + (parseInt(period) * DAY)) > get_expiry_date().getTime()) {

            $("<div>The  days effective period cannot go beyond the policy expiry date. </div>").dialog({
                title: 'Alert!',
                modal: true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                        $(this).dialog().dialog("destroy");
                    }
                }
            });

        }
        else {
            expiry = new Date();
            expiry.setDate(effective.getDate() + parseInt(period));
            expiry.setHours(23); expiry.setMinutes(59);
            expElem.val(expiry.dateFormat('d M Y h:i A'));
            expElem.trigger("change");
        }
    }

}