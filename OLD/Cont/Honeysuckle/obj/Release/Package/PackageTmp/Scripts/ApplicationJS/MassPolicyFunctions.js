﻿ 
 //////////////////// Approving ALL CERTIFICATES  ////////////////////////////////////

$(function () {
    $("#ApproveCerts").click(function () {
        var pid = $("#hiddenid").val();
       
        $.ajax({
            type: "POST",
            url: "/VehicleCertificate/GetCertsForApproval/" + pid,
            cache: false,
            async: false,
            success: function (data) {
                if (data.result) {
                    massApproval(data.certs, 'VehicleCertificate', data.canPrint);
                }
                else {
                    $("<div>You do not have the required permissions to do this!</div>").dialog({
                        title: "Alert",
                        modal:true,
                        buttons:
                        {
                            Ok: function () {
                                $(this).dialog('close');
                                $(this).html("");
                            }
                        }
                    });
                }

            }

        }); //END OF AJAX CALL

    });
});


$(function () {
    $("#ApproveNotes").click(function () {
        var pid = $("#hiddenid").val();
        $.ajax({
            type: "POST",
            url: "/VehicleCoverNote/GetNotesForApproval/" + pid,
            cache: false,
            async: false,
            success: function (data) {
                if (data.result) {
                    massApproval(data.NoteIds, 'VehicleCoverNote', data.canPrint);
                }
                else {
                    $("<div>You do not have the required permissions to do this!</div>").dialog({
                        title: "Alert",
                        modal:true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog('close');
                                $(this).html("");
                            }
                        }
                    });
                }
            }
        });
    });
});

$(function () {
    $("#PrintCerts").click(function () {
        var pid = $("#hiddenid").val();
        $.ajax({
            type: "POST",
            url: "/VehicleCertificate/GetCertsForPrinting/" + pid,
            cache: false,
            async: false,
            success: function (data) {
                if (data.result) {
                    massPrint(data.certs, 'VehicleCertificate');
                }
                else {
                    $("<div>You do not have the required permissions to do this!</div>").dialog({
                        title: "Alert",
                        modal:true,
                        buttons:
                        {
                            Ok: function () {
                                $(this).dialog('close');
                                $(this).html("");
                            }
                        }
                    });
                }
            }
        }); //END OF AJAX CALL
    });
});


$(function () {
    $("#PrintNotes").click(function () {
        var pid = $("#hiddenid").val();

        $.ajax({
            type: "POST",
            url: "/VehicleCoverNote/GetNotesForPrinting/" + pid,
            cache: false,
            async: false,
            success: function (data) {
                if (data.result) {
                    massPrint(data.NoteIds, 'VehicleCoverNote');
                }
                else {
                    $("<div>You do not have the required permissions to do this!</div>").dialog({
                        title: "Alert",
                        modal: true,
                        buttons:
                        {
                            Ok: function () {
                                $(this).dialog('close');
                                $(this).html("");
                            }
                        }
                    });
                }
            }
        }); //END OF AJAX CALL
    });
});



////////


/////Mass approval request
function massApprovalRequests(certs, typeofDoc) {
    var ThisUrlCall; var UrlCall;
    if (typeofDoc == 'VehicleCertificate')  UrlCall = '/VehicleCertificate/MassApproval' + '?certs=';
    else UrlCall = '/VehicleCoverNote/MassApproval' + '?coverNotes=';
    $("#vehicle-modal").dialog({
        modal: true,
        autoOpen: false,
        width: 800,
        title: "Mass Approval Request",
        height: 600,
        buttons: {
            'Request Approval': function () {
                ApprovedList = [];
                $(".vehcheck:checked").each(function () { //Loading the data into array for sending
                    ApprovedList[ApprovedList.length] = $(this).prop('id');
                });
                if (ApprovedList.length != 0) {
                    $("#vehicle-modal").dialog("close");
                    $("#vehicle-modal").html("");
                    var vehCertId = $("#certId").val();
                    $("#SendMail").dialog({
                        title: "Message Certificate Approval Users",
                        modal: true,
                        autoOpen: false,
                        width: 500,
                        height: 450,
                        buttons:
                        {
                            Send: function () {
                                var selected = [];
                                var sendto = document.getElementById("sendto");
                                for (var i = 0; i < sendto.options.length; i++) {
                                    if (sendto.options[i].selected) {
                                        selected.push(sendto.options[i].value);
                                    }
                                }
                                if (selected.length != 0) {
                                    //var vehCertId = $("#vehCertID").val();
                                    var heading = $("#heading").val();
                                    var content = $("#content").val();
                                    var parms = { vehId: vehCertId, admins: selected, heading: heading, content: content };
                                    $.ajax({
                                        type: "POST",
                                        traditional: true,
                                        url: "/Notification/sendNoteToComp/",
                                        async: false,
                                        data: parms,
                                        dataType: "json",
                                        success: function (data) {
                                            if (data.result) {
                                                $("#SendMail").html("");
                                                $("#SendMail").dialog("close");
                                                $("<div>Message sent!</div>").dialog({
                                                    title: "Alert",
                                                    modal: true,
                                                    buttons:
                                                    {
                                                        OK: function () {
                                                            $(this).dialog("close");
                                                            $(this).html("");
                                                            window.location.replace("/Policy/Details/" + $("#hiddenid").val());
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });
                                    //Removing the button
                                    $("#messageAdmins").remove();
                                    $.ajax({
                                        type: "POST",
                                        traditional: true,
                                        url: "/VehicleCertificate/PrintRequest/" + vehCertId,
                                        async: false,
                                        dataType: "json",
                                        success: function (data) { }
                                    });
                                } //END OF IF selected.length check-now to else
                                else {
                                    $("<div>You have not selected any recipients, so you cannot send this message. Please select at least one recipient</div>").dialog({
                                        title: "Alert",
                                        modal: true,
                                        buttons: {
                                            OK: function () {
                                                $(this).dialog("close");
                                                $(this).html("");
                                            }
                                        }
                                    });
                                } //end of selected check
                            }, //END OF 'SEND' BUTTON
                            Cancel: function () {
                                $(this).dialog("close");
                                $(this).html("");
                            }
                        } //END OF BUTTONS
                    });  // END OF JQUERY DIALOG
                    if (certs != null && certs != "") {
                        if (typeofDoc == "VehicleCertificate") $("#SendMail").load('/Notification/sendNote/' + ApprovedList + '?type=' + 'ApproveCert').dialog('open');
                        else $("#SendMail").load('/Notification/sendNote/' + ApprovedList + '?type=' + 'ApproveCoverNote').dialog('open');
                    }
                }
                //nothing was selected
                else  {
                    $("<div>Please select an item!</div>").dialog({
                        title: "Alert!",
                        modal: true,
                        buttons:
                        {
                            OK: function () {
                                $(this).dialog("close");
                                $(this).html("");
                            }
                        }
                    });
                }
            },
            Cancel: function () {
                $("#vehicle-modal").dialog("close");
                $("#vehicle-modal").html("");
                window.location.replace("/Policy/Details/" + $("#hiddenid").val());
            }
        }
    });
    $("#vehicle-modal").load(UrlCall + certs, function () { }).dialog('open');
}

function massCreateCertificates() {

    var pid = $("#hiddenid").val();
    vehiclesList = [];
    templatewordings = [];
    EffectiveDates = [];
    if ($(".vehcheck:checked").length > 0) {
        $(".vehcheck:checked").each(function () { //Loading the data into array for sending

            vehiclesList[vehiclesList.length] = $(this).prop('id');
            templatewordings[templatewordings.length] = $(this).parents(".vehicle-row").find(".formatDropdown").val();
            //EffectiveDates[EffectiveDates.length] = $(this).parents(".vehicle-row").find(".effective").val();

        });

        var parms = { vehiclesList: vehiclesList, templatewordings: templatewordings, polId: $("#hiddenid").val(),
                        //EffectiveDates: EffectiveDates, 
                        proceed: $("#Continue").val() };

        $.ajax({
            type: "POST",
            traditional: true,
            url: "/VehicleCertificate/CreateAllCerts/",
            data: parms,
            async: false,
            dataType: "json",
            success: function (data) {

                if (data.compShortening) {

                    $("<div>The insurance company for this policy does not have a Company Shortening code. Please contact your admin for assistance!</div>").dialog({
                        title: "Alert!",
                        modal: true,
                        buttons:
                        {
                            OK: function () {
                                $(this).dialog("close");
                                $(this).html("");
                            }
                        }
                    });
                }

                else if (data.wordingError) {
                    $("<div>Please ensure that all vehicles have a selected certificate wording, and effective dates!</div>").dialog({
                        title: "Alert!",
                        modal: true,
                        buttons:
                            {
                                OK: function () {
                                    $(this).dialog("close");
                                    $(this).html("");
                                    vehiclesList = [];
                                    templatewordings = [];
                                    EffectiveDates = [];
                                }
                            }
                    });
                }

                //////// If a previous cert exists

                else if (data.existingCert) {
                    $("<div>" + data.CertMessage + "</div>").dialog({
                        title: "Alert!",
                        modal: true,
                        buttons:
                        {
                            Proceed: function () {
                                $("#Continue").val("true");
                                $(this).dialog("close");
                                $(this).html("");
                                massCreateCertificates();
                            },
                            Cancel: function () {
                                $(this).dialog("close");
                                $(this).html("");
                                vehiclesList = [];
                                templatewordings = [];
                                EffectiveDates = [];
                            }
                        }
                    });
                }


                ////////


                else if (data.result) {
                    $("<div>Certificates created!</div>").dialog({
                        title: "Success!",
                        modal: true,
                        buttons:
                        {
                            OK: function () {
                                $(this).dialog("close");
                                $(this).html("");

                                $.each(data.certs, function (i, v) {
                                    v = JSON.parse(v);
                                });


                                $("#createCerts").dialog("close");
                                $("#createCerts").html("");

                                if (!data.autoApp) {
                                    if (data.canApprove)
                                        massApproval(data.certs, 'VehicleCertificate', data.canPrint);

                                    else massApprovalRequests(data.certs, 'VehicleCertificate');
                                }

                                else { //IF THE USER DOES HAVE THE AUTO APPROVE PERMISSION- PROCEED TO PRINTING
                                    if (data.canPrint)
                                        massPrint(data.certs, 'VehicleCertificate');

                                    else window.location.replace("/Policy/Details/" + $("#hiddenid").val());
                                }
                            }
                        }
                    });
                }

                else {
                    $("<div>You do not have the required permissions to do this!</div>").dialog({
                        title: "Alert!",
                        modal: true,
                        buttons:
                        {
                            OK: function () {
                                $(this).dialog("close");
                                $(this).html("");
                                vehiclesList = [];
                                templatewordings = [];
                                EffectiveDates = [];
                            }
                        }
                    });
                }

            }
        });
    }
    else {
        $('<div>No vehicle was selected</div>').dialog({
            modal: true,
            title: 'Alert!',
            buttons: {
                OK: function () {
                    $(this).dialog('close');
                }
            }
        });
    }

    $("#Continue").val("");

}

function massCreateCoverNotes() {

    var pid = $("#hiddenid").val();
    vehiclesList = [];
    templatewordings = [];
    EffectiveDates = [];
    ExpiryDates = [];
    var DAY = 1000 * 60 * 60 * 24;
    var noPeriod = false;

    $(".vehcheck:checked").each(function () { //Loading the data into array for sending

        vehiclesList[vehiclesList.length] = $(this).prop('id');
        templatewordings[templatewordings.length] = $(this).parents(".vehicle-row").find(".formatDropdown").val();
        EffectiveDates[EffectiveDates.length] = $(this).parents(".vehicle-row").find(".effective").val();
        ExpiryDates[ExpiryDates.length] = $(this).parents(".vehicle-row").find(".expiryPicker").val();

        var expiry = new Date($(this).parents(".vehicle-row").find(".expiryPicker").val());
        var effective = new Date($(this).parents(".vehicle-row").find(".effective").val());
        if (Math.floor(Math.abs(expiry.getTime() - effective.getTime()) / DAY) <= 0) {
            noPeriod = true;
            $('<div>At least one of the vehicles selected have a period of less than one day.</div>').dialog({
                modal: true,
                title: 'Alert!',
                buttons: {
                    OK: function () {
                        $(this).dialog('close');
                    }
                }
            });
        }

        if(noPeriod) return false;
    });

    if (!noPeriod) {

        var parms = { vehiclesList: vehiclesList, templatewordings: templatewordings, polId: $("#hiddenid").val(), EffectiveDates: EffectiveDates, ExpiryDates: ExpiryDates, proceed: $("#Continue").val() };

        $.ajax({
            type: "POST",
            traditional: true,
            url: "/VehicleCoverNote/CreateAllCovNotes/",
            data: parms,
            async: false,
            dataType: "json",
            success: function (data) {

                if (data.compShortening) {

                    $("<div>The insurance company does not have a company Shortening code. Please contact your admin for assistance!!</div>").dialog({
                        title: "Alert!",
                        modal: true,
                        buttons:
                    {
                        OK: function () {
                            $(this).dialog("close");
                            $(this).html("");
                        }
                    }
                    });
                }

                else if (data.wordingError) {
                    $("<div>Please ensure that all vehicles have a selected covernote wording, effective and expiry dates!</div>").dialog({
                        title: "Alert!",
                        modal: true,
                        buttons:
                            {
                                OK: function () {
                                    $(this).dialog("close");
                                    $(this).html("");
                                    vehiclesList = [];
                                    templatewordings = [];
                                    EffectiveDates = [];
                                    ExpiryDates = [];
                                }
                            }
                    });
                }
                else if (data.Exists) { //Need to make this message more specific (as it relates to which risk is invalid)
                    $("<div>" + data.noteMessage + "</div>").dialog({
                        title: "Alert!",
                        modal: true,
                        buttons: {
                            Proceed: function () {
                                $("#Continue").val("true");
                                $(this).dialog("close");
                                $(this).html("");
                                massCreateCoverNotes();
                            },
                            Cancel: function () {
                                $(this).dialog("close");
                                $(this).html("");
                                vehiclesList = [];
                                templatewordings = [];
                                EffectiveDates = [];
                                ExpiryDates = [];
                            }
                        }
                    });
                }
                else if (data.result) {
                    $("<div>Cover Notes Created!!</div>").dialog({
                        title: "Success!",
                        modal: true,
                        buttons:
                        {
                            OK: function () {
                                $(this).dialog("close");
                                $(this).html("");
                                $.each(data.NoteIDs, function (i, v) {
                                    v = JSON.parse(v);
                                });
                                $("#createCerts").dialog("close");
                                $("#createCerts").html("");
                                if (!data.autoApp) {
                                    if (data.canApprove) massApproval(data.NoteIDs, 'VehicleCoverNote', data.canPrint);
                                    else massApprovalRequests(data.NoteIDs, 'VehicleCoverNote');
                                }
                                else { //IF THE USER DOES HAVE THE AUTO APPROVE PERMISSION- PROCEED TO PRINTING
                                    if (data.canPrint) massPrint(data.NoteIDs, 'VehicleCoverNote');
                                    else window.location.replace("/Policy/Details/" + $("#hiddenid").val());
                                }
                            }
                        }
                    });
                }
                else {
                    $("<div>You do not have the required permissions to do this!</div>").dialog({
                        title: "Alert!",
                        modal: true,
                        buttons: {
                            OK: function () {
                                $(this).dialog("close");
                                $(this).html("");
                                vehiclesList = [];
                                templatewordings = [];
                                EffectiveDates = [];
                                ExpiryDates = [];
                            }
                        }
                    });
                }
            }
        });
        $("#Continue").val("");
    }
}