﻿$(function () {
    if (($("#chassis").val().trim().length > 0) || ($("#license").val().trim().length > 0)) $("#subbtn").prop("disabled", false);
    $("#chassis").on("keyup", function () {
        if (($("#chassis").val().trim().length > 0) || ($("#license").val().trim().length > 0)) $("#subbtn").prop("disabled", false);
        else $("#subbtn").prop("disabled", true);
    });
    $("#license").on("keyup", function () {
        if (($("#chassis").val().trim().length > 0) || ($("#license").val().trim().length > 0)) $("#subbtn").prop("disabled", false);
        else $("#subbtn").prop("disabled", true);
    });

    if ($('#searchModal').html() != "") {

        $('#searchModal').dialog({
            title: 'Search Complete',
            modal: true,
            buttons: {
                OK: function () {
                    $(this).dialog('close');
                    $('#searchModal').html('');
                    //window.location.replace =  '/web/index#Results';
                }
            }
        });
    }
});