﻿function BrowserCheck() {
    var browser = false;
    browser = (navigator.userAgent.toLowerCase().indexOf("msie") >= 0) || (navigator.userAgent.toLowerCase().indexOf(".net") >= 0) || (navigator.userAgent.search("Chrome") >= 0) || (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0);
    return browser;
}

$(function () {
    if (!BrowserCheck()) {
        //NOT SUPPORTED
        $(".not-supported")
			.css({
			    'position': 'absolute',
			    'top': '0px',
			    'left': '0px',
			    backgroundColor: 'black',
			    'opacity': '0.75',
			    'width': '100%',
			    'height': $(window).height(),
			    zIndex: 5000
			})
			.appendTo("body");

		$("<div><p><strong>Sorry! This page doesn't support your current browser choice.</strong><br />This application can be used with either <a href='http://www.microsoft.com/en-us/download/internet-explorer.aspx'>Internet Explorer</a>, <a href='http://www.google.com/chrome/'>Google Chrome</a> or <a href='https://www.apple.com/safari/'>Safari</a>.<br /> Thank you.</p>")
			.css({
			    backgroundColor: 'white',
			    'top': '50%',
			    'left': '50%',
			    marginLeft: -210,
			    marginTop: -100,
			    width: 410,
			    paddingRight: 10,
			    height: 200,
			    'position': 'absolute',
			    zIndex: 6000
			})
			.appendTo("body");
    }
});