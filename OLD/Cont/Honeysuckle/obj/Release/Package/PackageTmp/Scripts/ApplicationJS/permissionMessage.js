﻿$(function () {
    if ($('#permissionMessage').val() != 'undefined' && $('#permissionMessage').html() != "") {

        $('#permissionMessage').dialog({
            title: 'Permission',
            modal: true,
            buttons: {
                'OK': function () {
                    $(this).dialog('close');
                    $(this).dialog('destroy');
                    $('#permissionMessage').html('');
                }
            }
        });
    }
});