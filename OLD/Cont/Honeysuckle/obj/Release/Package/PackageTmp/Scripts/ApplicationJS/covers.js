﻿var successmessage = "<div>The cover was successfully deleted</div>";
var failuremessage = "<div>The cover you selected is currently being used by a policy. It cannot currently be deleted.</div>";
var create = "<div>You've successfully created a Policy Cover. Would you like to associate usages to the created cover? </div>";

$(function () {
    var $deleted = $(".deleted");
    
    $(".createpage").on('click', function () {
        window.location.replace('/PolicyCover/Create/');
    });

    if ($deleted.hasClass("success")) {
        $(successmessage).dialog({
            modal:true,
            title: "Success",
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    }
    if ($deleted.hasClass("failed")) {
        $(failuremessage).dialog({
            modal:true,
            title: "Failed",
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    }
    if ($(".create").length > 0) {
        $(".create").hide();
        $(create).dialog({
            title: "Cover Created!",
            modal:true,
            buttons: {
                Yes: function () {
                    window.location.replace("/PolicyCover/AssociateUsages/" + $(".create").text().trim());
                },

                No: function () {
                    $(this).dialog("close");
                }
            }
        });
    }

    $('#covers-data').perfectScrollbar({
        wheelSpeed: 0.3,
        wheelPropagation: true,
        minScrollbarLength: 10,
        suppressScrollX: true
    });
});



function editPolCover(e) {
    var id = $(e).parent().parent().find("#pcid").val();
    window.location.replace("/PolicyCover/Edit/" + id);
}

  function deletePolCover(e) {
     var id = $(e).parent().parent().find("#pcid").val();

        $("<div>You are about to delete this cover. Are you sure you want to do this? </div>").dialog({
            title: 'Alert!',
            modal:true,
            buttons: {
                Ok: function () {
                    window.location.replace("/PolicyCover/Delete/" + id);
                 
                  },
                  Cancel: function () {
                        $(this).dialog("close");
                        $(this).dialog().dialog("destroy");
                    }
                }
          });
     }
