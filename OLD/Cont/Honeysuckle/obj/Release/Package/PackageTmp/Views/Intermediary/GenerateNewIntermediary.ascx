﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Intermediary>" %>

<% using (Html.BeginCollectionItem("intermediaries")){%>
    <%: Html.ValidationSummary(true) %>
    
    <fieldset>

        <div class="intermediary save">
            <div class="view-data">
                <%: Html.HiddenFor(model => model.ID, new { @class = "interid dont-process" })%>
            
                <div class="data-row">
                    <div class="label">
                        Name:
                    </div>
                    <div class="field">
                        <%: Html.TextBoxFor(model => model.company.CompanyName, new { @class = "read-only", @readonly = "readonly" }) %>
                        <%: Html.HiddenFor(model => model.company.CompanyID, new { @class = "hidcid dont-process" })%>
                    </div>
                </div>
                <div class="data-row">
                    <div class="label">
                        <span class="help" title="Dictate if this intermediary can print certificates">Can Print Certificates?:</span>
                    </div>
                    <div class="field">
                        <%: Html.CheckBoxFor(model => model.PrintCertificate, new { @class = "int-check certcheck" })%>
                        <%: Html.ValidationMessageFor(model => model.PrintCertificate) %>
                    </div>
                </div>
                <div class="data-row">
                    <div class="label">
                        <span class="help" title="Dictate if this intermediary can print cover notes">Can Print Cover Notes?:</span>
                    </div>
                    <div class="field">
                        <%: Html.CheckBoxFor(model => model.PrintCoverNote, new { @class = "int-check cnotecheck" })%>
                        <%: Html.ValidationMessageFor(model => model.PrintCoverNote) %>
                    </div>
                </div>
                    
                <div class="data-row">
                    <div class="label">
                        <span class="help" title="Dictate how many Certificates or Cover Notes an Intermediary can print without any futher authorization. 0 represents limitless">Print Limit?:</span>
                    </div>
                    <div class="field">
                        <%: Html.TextBoxFor(model => model.PrintLimit, new { @class = "printcount" })%>
                        <%: Html.ValidationMessageFor(model => model.PrintLimit) %>
                    </div>
                </div>
            </div>
            <div class="inter-buttons">
                <% if (Model.enabled)
                    { %>
                    <input type="button" value="Disable" class="disable inter-btn"/> 
                <% } %>
                <% else
                    { %>
                    <input type="button" value="Enable" class="enable inter-btn"/> 
                <% } %>
                <input type="button" value="Delete" class="remCompany inter-btn"/> 
                <input type="button" value="Save" class="savechanges inter-btn"/> 
            </div>
        </div>

    </fieldset>

<% } %>