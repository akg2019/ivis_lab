﻿<%@ Page Title="Edit Company" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Model.CompanyName %> Edit Company Details:
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <%
        bool is_company_regular = (bool)Honeysuckle.Models.CompanyModel.IsCompanyRegular(Model.CompanyID);
        bool company_settings = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Settings"));
                            
        bool isAdmin = Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user);
        bool isIAJAdmin = Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user);
        bool userExistsForCompany = Honeysuckle.Models.UserModel.userExistsWithCompany(Model.CompanyID);
        int myComp = Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID;
                            
        bool allow = false;
        if (Honeysuckle.Models.Constants.user.newRoleMode) {
            allow = (Honeysuckle.Models.Constants.user.tempUserGroup.company.CompanyID == Model.CompanyID);
        }
    %>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <div id= "form-header">
                <img src="../../Content/more_images/Company.png" alt=""/>
                <div class="form-text-links">
                    <div class="form-header-text">
                        Edit 
                    </div>
                    <div class="menu-click">
                       <div class="icon">
                            <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                       </div> 
                       <ul id="menu-links"> 
                            <li>
                                <span class="menu-links-heading">Contacts</span>
                                <ul>
                                    <% if ((bool)Honeysuckle.Models.CompanyModel.IsCompanyRegular(Model.CompanyID)) { %>
                                        <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "EditInfo"))) { %> 
                                            <li> 
                                                <%: Html.ActionLink("Add Contacts", "EditCompContactInfo", new { id = Model.CompanyID })%> 
                                            </li>
                                        <% } %>
                                        <li> 
                                            <%: Html.ActionLink("View Contacts", "ViewContactInfo", new { id = Model.CompanyID })%> 
                                        </li>
                                    <% } %>

                                    <% else {  %>  
                                        <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "EditInfo")) && ( myComp == Model.CompanyID || allow)) { %> 
                                            <li>
                                                <%: Html.ActionLink("Add Contacts", "EditCompContact", new { id = Model.CompanyID })%> 
                                            </li>
                                        <% } %>
                                        <li>
                                            <%: Html.ActionLink("View Contacts", "ViewCompContactInfo", new { id = Model.CompanyID }) %> 
                                        </li>
                                    <% } %>
                                </ul>
                            </li>
                            <% if (!(bool)Honeysuckle.Models.CompanyModel.IsCompanyRegular(Model.CompanyID)) { %>
                                <li> 
                                    <span class="menu-links-heading">Users</span>
                                    <ul> 
                                        <% if ((Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "Create")) && (myComp == Model.CompanyID || allow)) || isAdmin || (isIAJAdmin && !userExistsForCompany)) { %>
                                            <li>
                                                <%: Html.ActionLink("Create", "Create", "User", new { UserCompId = Model.CompanyID }, null) %>
                                            </li>
                                        <% } %>
                                        <li>
                                            <%: Html.ActionLink("View Users", "UserList", new { id = Model.CompanyID })%>
                                        </li>
                                    </ul>
                                </li>
                            <% } %>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div id= "Alert">  <%: ViewData["TRN Exists"] %>  </div>

            <div class="field-data">
                <div id= "user-modal"></div>
                <%: Html.HiddenFor(model => model.CompanyID, new { id = "compID", @class = "dont-process" })%> 
                
                <div class="validate-section">
                    <div class="subheader"> 
                        <div class="arrow right down"></div>
                        Main Company Information
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="editor-label">
                            TRN
                        </div>
                        <div class="editor-field">
                            <% if ((bool)ViewData["TRNnotAvailable"])
                               {%>
                                <%: Html.HiddenFor(model => model.trn, new { @class = "dont-process" })%>
                                <%: Html.TextBox("TRNnotAvailable","Not Avaialble", new { id = "company-trn-not-available", @required = "required", @class = "company-trn-not-available" })%>
                            <% }
                               else
                               {%>
                               
                            <%: Html.TextBoxFor(model => model.trn, new { id = "trn", @required = "required", @readonly = "readonly", @class = "company-trn read-only" })%>
                            <%: Html.ValidationMessageFor(model => model.trn)%>
                             <button id="edit-trn" class ="editButton" type="button">Edit</button>
                            <% } %>
                           
                        </div>

                        <% if (!is_company_regular) { %>
                            <div class="editor-label">
                                IAJID
                            </div>
                            <div class="editor-field">
                                <%: Html.TextBoxFor(model => model.IAJID, new { id = "IAJID", @readonly = "readonly", @required = "required", @class = "read-only" })%>
                                <%: Html.ValidationMessageFor(model => model.IAJID)%>
                                <% if (isIAJAdmin || isAdmin)
                                   { %>
                                    <button id="edit-iajid" class ="editButton" type="button">Edit</button>
                                <% } %>
                            </div>
                        <% } %>
            
                        <div class="editor-label">
                            Name
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.CompanyName, new { id = "CompanyName", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.CompanyName) %>
                        </div>
            
                        <% if (!is_company_regular) { %>   

                            <% if (isIAJAdmin || isAdmin) { %>  
                                <div class="editor-label">
                                    Company Type: 
                                </div>
                                <div class="editor-field">
                                    <%: Html.DropDownListFor(m => m.CompanyType, new SelectList(new[] { " ", "Insurer", "Broker", "Agent", "Association" }), new { @class = "type", @required = "required" })%>
                                </div>
                            <% } %>
                            <% else { %>
                                <%: Html.HiddenFor(model => model.CompanyType, new { id = "compType", @class = "dont-process" })%> 
                            <% } %>
                    
                            <% if (isIAJAdmin || isAdmin) { %>
                                <div class="editor-label">
                                    Status:
                                </div>
                                <div class="editor-field">
                                    <%: Html.HiddenFor(model => model.Enabled, new { id = "hideenable", @class = "dont-process"}) %>
                                    <%: Html.DropDownList("enable", new SelectList(new List<Object>() { new { value = "enabled", text = "Enabled" }, new { value = "disabled", text = "Disabled" } }, "value", "text"), new { id = "enable" })%>
                                </div>
                            <% } %>

                    
                    </div> <!-- end of first data section -->
                </div> <!-- end of first validate section -->

                    <% if ((company_settings && Model.CompanyID == myComp) || isIAJAdmin || isAdmin) { %>
                          
                        <% if (isIAJAdmin || isAdmin) { %> 
                            <div class="validate-section">
                                <div class="subheader"> 
                                    <div class="arrow right"></div>
                                    Shortenings 
                                    <div class="valid"></div>
                                </div>
                                <div class="data">
                                    <div class="editor-label compCode">
                                        Insurer Code
                                    </div>
                                    <div class = "editor-field">
                                        <%: Html.TextBoxFor(model => model.insurercode, new { id = "compInsCode", @Class = "compCode", @required = "required" })%>
                                    </div>

                                    <div class="editor-label">
                                        Company Shortening Code
                                    </div>
                                    <div class = "editor-field">
                                        <%: Html.TextBoxFor(model => model.compshort, new { id = "ComShortening" })%>
                                    </div>
                                </div>
                            </div>
                        <% } %>
                        <% else { %>
                            <%: Html.HiddenFor(model => model.insurercode, new { id = "compInsCode", @class="dont-process" })%> 
                            <%: Html.HiddenFor(model => model.compshort, new { id = "ComShortening", @class="dont-process" })%> 
                        <% } %>

                        <div class="validate-section">
                            <div class="subheader"> 
                                <div class="arrow right"></div>
                                Admin Settings 
                                <div class="valid"></div>
                            </div>
                            <div class="data">
                                <div class="editor-label">
                                    Password Retries
                                </div>
                                <div class="editor-field">
                                    <%: Html.TextBoxFor(model => model.floginlimit, new { })%>
                                    <%: Html.ValidationMessageFor(model => model.floginlimit)%>
                                </div>
                                <div class="check-div">
                                    <div class="editor-label autoCanel">
                                        Auto Cancel Policy Risk
                                    </div>
                                    <div class="editor-field">
                                        <%: Html.CheckBoxFor(model => model.autoCancelVehicle, new { @id = "auto-cancel-vehicle", @Class = "autoCanel" })%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />

                    <% } %>
                <% } %>
                <% else{ %>
                    </div> <!-- end of first data section -->
                </div> <!-- end of first validate section -->
                <% } %>
 
                <div class="validate-section">
                    <div class="subheader"> 
                        <div class="arrow right"></div>
                        Company Address 
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                     <%if (Model.CompanyAddress.longAddressUsed)
                       {%>
                                 <div class="editor-label">
                                     <div>Combined Address:</div>
                                </div>
                                <div class="editor-field">
                                    <%: Html.TextBoxFor(model => model.CompanyAddress.longAddress, new { id = "longAddress",@required="required", @autocomplete = "off", @class = "address-details" })%>
                                    <%: Html.ValidationMessageFor(model => model.CompanyAddress.longAddress)%>
                                    <%: Html.HiddenFor(model => model.CompanyAddress.longAddressUsed, new { @Value = true,@class="dont-process" })%>
                                </div>
                        <%}
                       else
                       {%>
                    
                        <div class="editor-label">
                            Road:
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.CompanyAddress.roadnumber, new { id = "RoadNumber", @PlaceHolder = " 12" })%>
                            <%: Html.TextBoxFor(model => model.CompanyAddress.road.RoadName, new { id = "roadname", @PlaceHolder = " Short Hill" })%>
                            <%: Html.TextBoxFor(model => model.CompanyAddress.roadtype.RoadTypeName, new { id = "roadtype", @PlaceHolder = "Avenue" })%>
                
                            <%: Html.ValidationMessageFor(model => model.CompanyAddress.roadnumber)%>
                            <%: Html.ValidationMessageFor(model => model.CompanyAddress.road.RoadName)%>
                            <%: Html.ValidationMessageFor(model => model.CompanyAddress.roadtype.RoadTypeName)%>
                        </div>

                        <div class="editor-label">
                            Building/Apartment No. 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.CompanyAddress.ApartmentNumber, new { id = "aptNumber" })%>
                            <%: Html.ValidationMessageFor(model => model.CompanyAddress.ApartmentNumber)%>
                        </div>
           
                        <div class="editor-label">
                            City/Town 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.CompanyAddress.city.CityName, new { id = "CityName", @required = "required", @class = "city" })%>
                            <%: Html.ValidationMessageFor(model => model.CompanyAddress.city.CityName)%>
                        </div>
                        <div class="editor-label">
                            Parish 
                        </div>
                        <div class="editor-field">
                            <%: Html.HiddenFor(model => model.CompanyAddress.parish.ID, new { id = "parish", @class = "dont-process" })%>
                            <%  Html.RenderAction("ParishList", "Parish"); %>
                            <%: Html.ValidationMessage("parish")%>
                        </div>
                        <div class="editor-label">
                            Country 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.CompanyAddress.country.CountryName, new { id = "CountryName", @required = "required", @class = "country" })%>
                            <%: Html.ValidationMessageFor(model => model.CompanyAddress.country.CountryName)%>
                        </div> 
                         <%: Html.HiddenFor(model => model.CompanyAddress.longAddressUsed, new { @Value = false, @class = "dont-process" })%>
                     <% } %>
                    </div>
                </div> 

                <div class="btnlinks">
                    <% Html.RenderAction("BackBtn", "Back"); %>
                    <input type="submit" value="Save" id="Submit2"/>
                </div>
            </div> 
        </fieldset>

    <% } %>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/companyInsuredCodeSet.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/companyManagement.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>
    <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/iajid-edit.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/parishUpdate.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/TestCompanyTRN-Edit.js" type="text/javascript"></script>
</asp:Content>



