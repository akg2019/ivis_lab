﻿<%@ Page Title="Edit Contact Information" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Company>" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
        Edit Company Contact Information
    </asp:Content>

    <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

        <% using (Html.BeginForm()) { %>
            <%: Html.ValidationSummary(true) %>

            <fieldset>

                <div id= "form-header"><img src="../../Content/more_images/Contact.png" alt=""/> 
                    <div class="form-text-links">
                        <span class="form-header-text">Edit Company Contacts</span>
                    </div>
                </div> 
                <%: Html.Hidden("contactPage", "EditContactPage", new { id = "page", @class = "dont-process" })%>
                <%: Html.HiddenFor(model => model.CompanyID, new { id = "compID", @class = "dont-process" })%>
                <div id="user-modal"></div>
                <div class="error"><%:ViewData["PhoneError"]%></div>

                <div class="field-data">
                    <% if (Model.emails != null) { %>
                        <% for (int i = 0; i < Model.emails.Count(); i++){ %>
                            <div class="items validate-section">
                                <div class="subheader"><img src="../../Content/more_images/User.png" alt=""/><span>User Contact</span><input type="button" value="Remove" id= "Rem" class="remove btnclr dont-process"/><div class="valid"></div></div>
                                <% Html.RenderAction("UserInfo", "User", new { username = Model.emails.ElementAt(i).email }); %>

                                <% if (Model.emails.ElementAt(i).primary)
                                   { %>
                                       <span>Primary Email:</span>
                                       <% Html.RenderPartial("~/Views/People/Person.ascx", Model.emails.ElementAt(i)); %>
                                <% } %>
                                <% Html.RenderAction("EditPhone", "Phone", new { phone = Model.phoneNums.ElementAt(i) }); %> 
                                
                            </div>
                        <% } %>
                    <% } %>

                    <div id="contact"></div>
                    <div id="addUserContact" class="add-dynamic big">
                        <img src="../../Content/more_images/ivis_add_icon.png" />
                        <span>Add Contact</span>
                    </div>
                    <div class="btnlinks">
                         <% Html.RenderAction("BackBtn", "Back"); %>
                        <input type="submit" value="Done" id= "done" class="clrbtn" />
                    </div>
                </div>

            </fieldset>
        <% } %>
    </asp:Content>

    <asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
        <script src="../../Scripts/ApplicationJS/companyContacts.js" type="text/javascript"></script>
        <link href="../../Content/CompanyContact.css" rel="stylesheet" type="text/css" />
        <link href="../../Content/Policies.css" rel="stylesheet" type="text/css" />
    </asp:Content>

 
