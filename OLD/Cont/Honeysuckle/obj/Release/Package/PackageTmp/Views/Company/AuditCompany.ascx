﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Company>" %>

<div class="editor-label">
    TRN
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.trn, new { id = "trn", @class = "company-trn",  @required = "required", @PlaceHolder= " Eg. 0888776655"})%><br />
</div>

<div class="editor-label">
    IAJID
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.IAJID, new { id = "IAJID", @required = "required", @PlaceHolder = " Eg. 723476655645" })%><br />
</div>

<div class="editor-label">
    Company Name
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.CompanyName, new { id = "CompanyName", @required = "required" })%><br />
</div>
            
<div class="editor-label">
    Company Type
</div>
<div class="editor-field">
    <%: Html.DropDownListFor(m=> m.CompanyType, new SelectList(new[] {" ", "Insurer", "Broker", "Agent", "Association"}), new { @class = "type" } )%>
</div>

<div class="editor-label">
    Enabled: 
</div>
<div class="editor-field">
    <%: Html.DropDownList("enable", new SelectList(new List<Object>(){new {value = "enabled", text = "Enable"}, new{value = "disabled", text = "Disabled"}}, "value", "text"), new { id = "enable" }) %>
</div>

<div class="subheader"> 
    Shortenings 
</div>

<div class="editor-label  compCode">
    Insurer Code
</div>
<div class = "editor-field">
    <%: Html.TextBoxFor(model => model.insurercode, new { id = "compInsCode",@Class= "compCode", @required = "required" })%>
</div>

<div class="editor-label">
    Company Shortening Code
</div>
<div class = "editor-field">
    <%: Html.TextBoxFor(model => model.compshort, new { id = "ComShortening" })%>
</div>

<div class="subheader"> 
    Admin Settings 
</div>

<div class="editor-label">
    Password Retries <span>*</span>
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.floginlimit, new { @class = "floginlimit", @required = "required", @Value = 0 })%>
    <%: Html.ValidationMessageFor(model => model.floginlimit)%>
</div>
<div class="editor-label autoCanel">
    Auto Cancel Vehicle Under Policy 
</div>
<div class="editor-field">
    <%: Html.CheckBoxFor(model => model.autoCancelVehicle, new { @id = "auto-cancel-vehicle", @Class = "autoCanel" })%>
</div>
<br />

<div class="subheader"> 
    Company Address 
</div>
                
<div class="editor-label">
    Road 
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.CompanyAddress.roadnumber, new { id = "RoadNumber", @PlaceHolder = "12" })%>
    <%: Html.TextBoxFor(model => model.CompanyAddress.road.RoadName, new { id = "roadname", @PlaceHolder = " Short Hill" })%>
    <%: Html.TextBoxFor(model => model.CompanyAddress.roadtype.RoadTypeName, new { id = "roadtype", @PlaceHolder = " Avenue" })%>
</div>

<div class="editor-label">
    Building/Apartment No. 
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.CompanyAddress.ApartmentNumber, new { id = "aptNumber" })%>
</div>

<div class="editor-label">
    City/Town
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.CompanyAddress.city.CityName, new { id = "city", @required = "required", @class = "city" })%>
</div>

<div class="editor-label">
    Parish
</div>
<div class="editor-field">
    <%: Html.HiddenFor(model => model.CompanyAddress.parish.ID, new { id = "parish", @class = "dont-process" })%>
    <%  Html.RenderAction("ParishList", "Parish"); %>
</div>

<div class="editor-label">
    Country Name
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.CompanyAddress.country.CountryName, new { id = "country", @required = "required", @Value = "Jamaica", @class= "country"}) %>
</div>