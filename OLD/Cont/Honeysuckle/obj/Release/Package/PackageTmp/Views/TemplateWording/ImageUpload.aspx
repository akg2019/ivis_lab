﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.TemplateImage>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ImageUpload
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm("ImageUpload", "TemplateWording", FormMethod.Post, new { enctype = "multipart/form-data" }))
       { %>
        <fieldset>
             <div class="form-header"><img src="../../Content/more_images/Cover_Note.png" />
                <div class="form-text-links">
                    <span class="form-header-text">Cover Note/Certificate Image Uploader</span>
                </div>
             </div>

             <div class="field-data">

                <div class="error">
                    <% if (ViewData["error"] != null) { %>
                        <%: ViewData["error"].ToString()%>
                    <% } %>
                </div>

                <%//: Html.Hidden("id", int.Parse(ViewData["hiddenid"].ToString()))  %>

                <div class="certificates image-section validate-section">
                    <div class="subheader">
                        <div class="arrow right down"></div>
                        Certificate Images
                    </div>
                    <div class="images data">
                        <% Html.RenderAction("Image", new { id = Honeysuckle.Models.TemplateImageModel.RetrieveTemplateImage(Model.ToList(), "certificate", "header").ID, type = "certificate", position = "header" }); %>
                        <% Html.RenderAction("Image", new { id = Honeysuckle.Models.TemplateImageModel.RetrieveTemplateImage(Model.ToList(), "certificate", "footer").ID, type = "certificate", position = "footer" }); %>
                        <% Html.RenderAction("Image", new { id = Honeysuckle.Models.TemplateImageModel.RetrieveTemplateImage(Model.ToList(), "certificate", "logo").ID, type = "certificate", position = "logo" }); %>
                    </div>
                </div>
                <div class="covernotes image-section validate-section">
                    <div class="subheader">
                         <div class="arrow right"></div>
                         Cover Note Images
                    </div>
                    <div class="images data">
                        <% Html.RenderAction("Image", new { id = Honeysuckle.Models.TemplateImageModel.RetrieveTemplateImage(Model.ToList(), "cover note", "header").ID, type = "covernote", position = "header" }); %>
                        <% Html.RenderAction("Image", new { id = Honeysuckle.Models.TemplateImageModel.RetrieveTemplateImage(Model.ToList(), "cover note", "footer").ID, type = "covernote", position = "footer" }); %>
                        <% Html.RenderAction("Image", new { id = Honeysuckle.Models.TemplateImageModel.RetrieveTemplateImage(Model.ToList(), "cover note", "logo").ID, type = "covernote", position = "logo" }); %>
                    </div>
                </div>

            </div>

        </fieldset>
    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Scripts/jquery-upload/css/jquery.fileupload.css" rel="stylesheet" type="text/css" />
    
    <script src="../../Scripts/jquery-upload/js/vendor/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-upload/js/jquery.iframe-transport.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-upload/js/jquery.fileupload.js" type="text/javascript"></script>
    
    <link href="../../Content/image-upload.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/image-upload.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
