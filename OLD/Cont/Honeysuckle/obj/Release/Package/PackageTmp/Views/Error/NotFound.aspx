﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	NotFound
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% if ((bool)ViewData["release"]) { %>
    <div class="area-404 release">
        <div class="message release">
    <% } else { %>
    <div class="area-404 debug">
        <div class="message debug">
    <% } %>
            <span class="text">404</span>
            <span class="nf">Page Not Found</span>
            <span class="line">Oops. Seems like the page you're looking for doesn't exist. :(</span>
        </div>
        <% if ((bool)ViewData["release"]) { %>
            <div class="release-img">
                <img src="../../Content/more_images/IVIS_ICON_LARGE.png" class="img-404 release" />
            </div>
        <% } else { %>
            <img src="../../Content/more_images/404-debug.jpg" class="img-404 debug" />
        <% } %>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/404.css" rel="stylesheet" type="text/css" />
</asp:Content>
