﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.UserGroup>>" %>

<%: Html.ListBox("groups", new SelectList(Model, "UserGroupID", "UserGroupName"), new { id = "groups" })%>