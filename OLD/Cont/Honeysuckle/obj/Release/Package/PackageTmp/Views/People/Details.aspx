﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Person>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <fieldset> 
        <div id= "form-header"><img src="../../Content/more_images/User.png" alt=""/>
            <div class="form-text-links">
                 <span class="form-header-text">Person Details</span>
                 <div class="icon">
                    <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                </div>
            </div>
        </div>
    
        <div class="field-data">
            <div class="validate-section">
                <div class="subheader"> 
                    <div class="arrow right down"></div>
                    Main Information
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <%: Html.HiddenFor(model => model.PersonID, new { @class = "hiddenid dont-process" })%>

                    <div class ="editor-label">
                        TRN: 
                    </div>
                    <% if (Model.TRN == "10000000")
                       { %>
                     <div class ="editor-field">  
                        <%: Html.TextBox("defaultTRN", "Not Available" , new { @class = "read-only", @readonly = "readonly" })%> 
                    </div> 
                    <% }
                       else
                       { %>
                    <div class ="editor-field">  
                        <%: Html.TextBoxFor(model => model.TRN, new { @class = "read-only", @readonly = "readonly" })%> 
                    </div> 
                    <% } %>
                    <div class ="editor-label">
                        First Name: 
                    </div>
                    <div class ="editor-field">  
                        <%: Html.TextBoxFor(model => model.fname, new { @class = "read-only", @readonly = "readonly" })%> 
                    </div> 
            
                    <div class ="editor-label">
                        Middle Name:  
                    </div>
                    <div class ="editor-field">  
                        <%: Html.TextBoxFor(model => model.mname, new { @class = "read-only", @readonly = "readonly" })%> 
                    </div> 
       
                    <div class ="editor-label">
                        Last Name: 
                    </div> 
                    <div class ="editor-field">  
                        <%: Html.TextBoxFor(model => model.lname, new { @class = "read-only", @readonly = "readonly" })%> 
                    </div> 
                </div>
            </div>

            <div class="validate-section">
                <div class="subheader"> 
                        <div class="arrow right"></div>
                        Address 
                        <div class="valid"></div>
                </div>
                <div class="data">
                    <% if (Model.address != null)
                       { %>
                    <% Html.RenderAction("Address", "Address", new { id = Model.address.ID }); %>
                    <% }
                       else
                       {%>
                    <% Html.RenderAction("Address", "Address", new { id = 0 }); %>
                    <% } %>

                </div>
            </div>
            
                <div class="btnlinks">
                    <% Html.RenderAction("BackBtn", "Back"); %>
                    <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("PolicyHolder", "Update"))) { %>
                        <button type="button" id="edit-person">Edit Person</button>
                    <% } %>
                </div>
        </div>
    </fieldset>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/person-details.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

