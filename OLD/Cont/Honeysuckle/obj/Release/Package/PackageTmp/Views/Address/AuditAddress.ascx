﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Address>" %>

<div class="editor-label">
    Road No:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.roadnumber) %>
</div>

<div class="editor-label">
    Road Name:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.road.RoadName) %>
</div>

<div class="editor-label">
    Road Type:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.roadtype.RoadTypeName) %>
</div>

<div class="editor-label">
    <%: Html.LabelFor(model => model.ApartmentNumber) %>
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.ApartmentNumber) %>
</div>

<div class="editor-label">
    Zip Code:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.zipcode.ZipCodeName) %>
</div>

<div class="editor-label">
    City:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.city.CityName) %>
</div>

<div class="editor-label">
    Parish:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.parish.parish) %>
</div>

<div class="editor-label">
    Country:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.country.CountryName) %>
</div>

<div class="editor-field">
    <%: Html.TextBoxFor(model => model.longAddress) %>
</div>
