﻿<%@ Page Title="Policy Details" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Policy>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	 Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <%: Html.Hidden("systemdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), new { id = "sysdate", @class = "dont-process" })%>
    <div id= "form-header"><img src="../../Content/more_images/Policy.png" alt=""/>
        <div class="form-text-links">
            <span class="form-header-text">Policy Details</span>
            <% if (Model.cancelled)
               { %>
                  <div class = "cancelledRecord" title = "Cancelled Policy">Cancelled</div>
            <% } %>
            <div class="icon">
                <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="close sprite open-sections" />
            </div>
        </div>
     </div>

    <div class="field-data">
        
        <fieldset>
            <div id="insured-modal"></div>
            <div id="company-modal"></div>
            <div id="vehicle-modal"></div>
            <div id="createCerts"></div>
            <div id="InformationDialog"></div>
            <% if ((bool) ViewData["VehicleAlert"]) { %> <div id= "NewPolicyModal"> Would you like to create cover notes/ certificates for the policy's risk(s)? </div> <%  } %>

            <div class="validate-section">
                <div class="subheader"> 
                    <div class="arrow right down"></div>
                    Policy Information 
                    <div class="valid"></div>
                    <% if(Model.imported){ %>   
                        <img src="../../Content/more_images/imported.png" class ="imported" />
                    <% } %>
                </div>
                <div class="data">
                    <div id= "policyInformation" class="policy-box" >
                    <% if (ViewData["PolicySaved"] != null)
                       { %> 
                            <div id="policyInfo"><%: ViewData["PolicySaved"].ToString() %></div>
                        <% } %>
                        <!-- hidden info -->
                        <div id= "SendMail" > </div>

                        <%: Html.HiddenFor(model => model.ID, new { id = "hiddenid", @Value = Model.ID, @class = "dont-process" })%> <!-- THIS SHOWS THE ID OF THE POLICY -->
                        <%: Html.HiddenFor(model => model.ID, new { id = "compid", @Value = Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID, @class = "dont-process" })%> 
                        <%: Html.Hidden("imported", Model.imported, new { id = "import", @class = "dont-process" })%>
                        <%: Html.Hidden("page", "details", new { id="page",@class = "dont-process" })%>
            
                        <div id= "Alert"> 
                            <%: ViewData["No Vehicle"]  %>
                            <%: ViewData["No Insured"]  %> 
                            <%: ViewData["Date Error"]  %> 
                            <%: ViewData["IAJID"]  %> 
                        </div>
                
                        <!-- end of hidden info -->
                        <div class="shown-policy-data policy-box">
                    
                            <% bool isInsurer = (((Honeysuckle.Models.Constants.user.newRoleMode) && (Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Honeysuckle.Models.Constants.user.tempUserGroup.company.CompanyID))) || (Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID))); %>
                    
                            <div class="editor-label">
                                Insurance Company
                            </div>
                            <div class="editor-field">
                                <%: Html.HiddenFor(model => model.insuredby.CompanyID, new { id = "insID", @class = "dont-process" })%>
                                <%: Html.TextBoxFor(model => model.insuredby.CompanyName, new { @readonly = "readonly", @class = "read-only" }) %>
                            </div>

                    
                            <div class="editor-label">
                                Start Date <span class="format">(Day Month Year HH:mm tt)</span>
                            </div>
                            <div class="editor-field">
                                <%: Html.TextBoxFor(model => model.startDateTime, new { @class = "read-only", id = "start", @readonly = "readonly", @Value = ViewData["startDate"].ToString() })%>
                            </div>
            
                            <div class="editor-label">
                                End Date <span class="format">(Day Month Year HH:mm tt)</span>
                            </div>
                            <div class="editor-field">
                                <%: Html.TextBoxFor(model => model.endDateTime, new { @class = "read-only", id = "end", @readonly = "readonly", @Value = ViewData["endDate"].ToString() })%>
                            </div>

                            <div class="editor-label">
                                Policy Cover
                            </div>
                            <div class="editor-field">  
                                <%: Html.HiddenFor(model => model.policyCover.ID, new { id = "CoverID", @class = "dont-process" })%>
                                <% Html.RenderAction("CoversDropDown", "PolicyCover", new { Compid = Model.insuredby.CompanyID }); %>
                            </div>

                            <div class="editor-label">
                                Policy Prefix
                            </div>
                            <div class="editor-field">
                                <%: Html.TextBoxFor(model => model.policyCover.prefix, new { id = "policyprefix", @readonly = "readonly", @class = "read-only" }) %>
                                <%: Html.ValidationMessageFor(model => model.policyCover.prefix)%>
                            </div>

                             <div class="editor-label">
                                    No Policy Number
                               </div>
                               <div class="editor-field">
                                    <div class="WarningInfo">If you do not have a policy number,</br>this box should be checked</div>
                                    <div><%: Html.CheckBoxFor(model => model.no_policy_no, new { @disabled = "disabled", @class = "dont-process no_policy_no_check", title = "No Policy Numberr." })%> </div>
                                </div>

                            <%--<div class="check-div">
                                <div class="editor-label">
                                    Blank Policy No.
                                </div>
                                <div class="editor-field">
                                    <%: Html.CheckBoxFor(model => model.no_policy_no, new { @disabled = "disabled", @class = "dont-process no_policy_no_check", @title = "Check this if you don't have a policy number currently to attribute to this policy" }) %>
                                </div>
                            </div>--%>
            
                            <div class="editor-label">
                                Policy Number
                            </div>
                            <div class="editor-field">
                                <%: Html.TextBoxFor(model => model.policyNumber, new { id = "policyNum", @class = "read-only", @disabled = "disabled"  }) %>
                                <%: Html.ValidationMessageFor(model => model.policyNumber) %>
                            </div>
                            <% if ((!isInsurer || Model.imported )&& (Model.brokerPolicyNumber != null && Model.brokerPolicyNumber != ""))
                               { %>
                                <div class = "brokerPolicyDetails">
                                    <div class="editor-label">
                                        Broker/Temporary Number 
                                    </div>
                                    <div class="editor-field">
                                        <%: Html.TextBoxFor(model => model.brokerPolicyNumber, new { @class = "read-only", @disabled = "disabled", id = "BrokerPolicyNum" })%>
                                        <%: Html.ValidationMessageFor(model => model.brokerPolicyNumber) %>
                                    </div>
                                </div>
                                <% } %>
                            
                            <div class="check-div">
                                <div class="editor-label">
                                    Scheme
                                </div>
                                <div class="editor-field">
                                    <%: Html.CheckBoxFor(model => model.Scheme, new { id = "scheme", @disabled = "disabled", Style = "width: 17px; height:20px;" })%>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right"></div>
                    Source Company Information 
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div class="SourceComp policy-box">
                        <div class="editor-label">
                            Company Name 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.compCreatedBy.CompanyName, new { @readonly = "readonly", @class = "read-only" })%>
                        </div>

                        <div class="editor-label">
                            Company TRN 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.compCreatedBy.trn, new { @readonly = "readonly", @class = "read-only" })%>
                        </div>
                    </div>
                </div>
            </div>

            
            <div class="validate-section policyholders">
                <div class="subheader"> 
                    <div class="arrow right"></div>
                    Policy Holders
                    <div class="valid"></div>
                </div>
                <div id="policyholders" class="data">
                    <div class="editor-label"> 
                        Insured Type
                    </div>
                    <div class="editor-field">
                        <%: Html.DropDownListFor(m => m.insuredType, new SelectList(new[] {"Individual", "Company", "Individual/Company", "Joint Insured", "Blanket" }), new { id = "insuredType",@class = "InsuredType read-only", @disabled = "disabled" })%>
                    </div>
                    <div class="policy-holders">
                        <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("PolicyHolder", "Read"))){ %>
                            <div class="addInsured policy-box">
                                <div id="insured">
                                    <%
                                        if (Model.insured != null)
                                        {
                                            foreach (Honeysuckle.Models.Person person in Model.insured)
                                            {
                                                Html.RenderAction("personView", "People", new { perId = person.PersonID, type = "details" }); 
                                            }
                                        }
                                    %>
                                </div>
                            </div>

                            <div class="addCompany policy-box">
                                <div id="company">
                                    <%
                                        if (Model.company != null)
                                        { 
                                            foreach(Honeysuckle.Models.Company company in Model.company)
                                            {
                                                Html.RenderAction("CompanyView", "Company", new { CompanyIdNum = company.CompanyID, type = "details" }); 
                                            }
                                        }   
                                    %>
                                </div>
                            </div>
                        <% } %>
                    </div>
                </div>
            </div>

            <div class="policy-mailing-address validate-section">
                <div class="subheader"> 
                    <div class="arrow right"></div>
                    Mailing Address
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div class="editor-label">
                        Street 
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.mailingAddress.roadnumber, new { @class = "padding RoadNumber mailing read-only", @PlaceHolder = " 12", @readonly = "readonly" })%>
                        <%: Html.TextBoxFor(model => model.mailingAddress.road.RoadName, new { @class = "padding roadname mailing read-only",  @readonly = "readonly", @PlaceHolder = " Short Hill", @autocomplete = "false" })%>
                        <%: Html.TextBoxFor(model => model.mailingAddress.roadtype.RoadTypeName, new { @class = "padding roadtype mailing read-only", @PlaceHolder = " Avenue", @readonly = "readonly" })%>
          
                        <%: Html.ValidationMessageFor(model => model.mailingAddress.roadnumber)%>
                        <%: Html.ValidationMessageFor(model => model.mailingAddress.road.RoadName)%>
                        <%: Html.ValidationMessageFor(model => model.mailingAddress.roadtype.RoadTypeName)%>
                    </div>

                    <div class="editor-label">
                        Building/Apartment No. 
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.mailingAddress.ApartmentNumber, new { @class = "aptNumber mailing read-only", @readonly = "readonly" })%>
                        <%: Html.ValidationMessageFor(model => model.mailingAddress.ApartmentNumber)%>
                    </div>

                    <div class="editor-label">
                        City/Town
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.mailingAddress.city.CityName, new { @class = "city mailing read-only", @required = "required", @readonly = "readonly" })%>
                        <%: Html.ValidationMessageFor(model => model.mailingAddress.city.CityName)%>
                    </div>

                    <div class="editor-label">
                        Parish
                    </div>
                    <div class="editor-field">
                        <%: Html.HiddenFor(model => model.mailingAddress.parish.ID, new { @class = "parish mailing read-only dont-process" })%>
                        <% Html.RenderAction("ParishList", "Parish"); %>
                        <%: Html.ValidationMessage("parish") %>
                    </div>

                    <div class="editor-label">
                        Country
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.mailingAddress.country.CountryName, new { @class = "country mailing read-only", @required = "required", @readonly = "readonly" })%>
                        <%: Html.ValidationMessageFor(model => model.mailingAddress.country.CountryName)%>
                    </div>
                </div>

            </div>

            <div class="policy-billing-address validate-section">
                <div class="subheader"> 
                    <div class="arrow right"></div>
                    <span class="subtext">Billing Address</span>
                    <div class="valid"></div>
                    <div class="bilcheck"> Use Same As Above: <%: Html.CheckBox("billscheck", (bool)ViewData["billCheck"], new { @class = "billing check", @disabled = "disabled" })%> </div>
                </div>
                <div class="billing-address-section hidden data">
                    <div class="editor-label">
                        Street 
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.billingAddress.roadnumber, new { @class = "padding RoadNumber billing read-only", @readonly = "readonly", @PlaceHolder = " 12" })%>
                        <%: Html.TextBoxFor(model => model.billingAddress.road.RoadName, new { @class = "padding roadname billing read-only", @readonly = "readonly", @required = "required", @PlaceHolder = " Short Hill" })%>
                        <%: Html.TextBoxFor(model => model.billingAddress.roadtype.RoadTypeName, new { @class = "padding roadtype billing read-only", @readonly = "readonly", @PlaceHolder = " Avenue" })%>
          
                        <%: Html.ValidationMessageFor(model => model.billingAddress.roadnumber)%>
                        <%: Html.ValidationMessageFor(model => model.billingAddress.road.RoadName)%>
                        <%: Html.ValidationMessageFor(model => model.billingAddress.roadtype.RoadTypeName)%>
                    </div>

                    <div class="editor-label">
                        Building/Apartment No. 
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.billingAddress.ApartmentNumber, new { @class = "aptNumber billing read-only", @readonly = "readonly" })%>
                        <%: Html.ValidationMessageFor(model => model.billingAddress.ApartmentNumber)%>
                    </div>

                    <div class="editor-label">
                        City/Town
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.billingAddress.city.CityName, new { @class = "city billing read-only", @required = "required", @readonly = "readonly" })%>
                        <%: Html.ValidationMessageFor(model => model.billingAddress.city.CityName)%>
                    </div>

                    <div class="editor-label">
                        Parish
                    </div>
                    <div class="editor-field">
                        <%: Html.HiddenFor(model => model.billingAddress.parish.ID, new { @class = "parish billing read-only dont-process", @readonly = "readonly" })%>
                        <% Html.RenderAction("ParishList", "Parish"); %>
                        <%: Html.ValidationMessage("parish") %>
                    </div>

                    <div class="editor-label">
                        Country
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.billingAddress.country.CountryName, new { @class = "country billing read-only", @required = "required", @readonly = "readonly" })%>
                        <%: Html.ValidationMessageFor(model => model.billingAddress.country.CountryName)%>
                    </div>
                </div>
            </div>
                
            <%
                bool print_certs = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Print")) && Honeysuckle.Models.VehicleCertificateModel.MassPrintCertButton(Model);
                bool print_notes = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Print")) && Honeysuckle.Models.VehicleCoverNoteModel.MassPrintNotesButton(Model);
                bool approve_certs = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Approve")) && Honeysuckle.Models.VehicleCertificateModel.MassApproveCertButton(Model);
                bool approve_notes = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Approve")) && Honeysuckle.Models.VehicleCoverNoteModel.MassApproveNotesButton(Model);
                bool all_certs = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Create")) && Honeysuckle.Models.VehicleCertificateModel.MassCreateCertButton(Model);
                bool all_notes = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Create")) && Honeysuckle.Models.VehicleCoverNoteModel.MassCreateCoverButton(Model);
                bool activate_policy = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Policies", "Activate"));
            %>

            <div class="policy-risk validate-section">
                <div class="subheader"> 
                    <div class="arrow right"></div>
                    <span class="subtext">Vehicles</span>
                    <ul id="menu-links"> 
                        <li>
                            <span class="menu-links-heading">Drivers</span>
                            <ul>
                                <li>
                                    <a class="multiple" onclick="editdrivers()">Edit All Drivers</a> 
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span class="menu-links-heading">Cover Notes</span>
                            <ul>
                                <% if (all_notes) { %>
                                    <li>
                                        <a id="allCoverNotes" class="multiple">Create Cover Notes</a>
                                    </li>
                                <% } %>
                                <% if (approve_notes) { %>
                                    <li>
                                        <a id="ApproveNotes" class="multiple" >Approve Cover Notes</a>
                                    </li>
                                <% } %>
                                <% if (print_notes) { %>
                                    <li>
                                        <a id="PrintNotes" class="multiple" >Print Cover Notes</a>
                                    </li>
                                <% } %>
                            </ul>
                        </li>
                        <li> 
                            <span class="menu-links-heading">Certificates</span>
                            <ul> 
                                <% if (all_certs) { %>
                                    <li>
                                        <a id="allCerts" class="multiple" >Create Certificates</a>
                                    </li>
                                <% } %>
                                <% if (approve_certs) { %>
                                    <li>
                                        <a id="ApproveCerts" class="multiple" >Approve Certificates</a>
                                    </li>
                                <% } %>
                                <% if (print_certs){ %>
                                    <li>
                                        <a id="PrintCerts" class="multiple" >Print Certificates</a>
                                    </li>
                                <% } %>
                                    
                            </ul>
                        </li>
                            
                    </ul>
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div id="vehicle-box" class="addvehicle policy-box">
                        <div id="drivers"></div>
                        <div id="vehicles">
                            <%
                                if (Model.vehicles != null)
                                {
                                    foreach (Honeysuckle.Models.Vehicle vehicle in Model.vehicles)
                                    {
                                        Html.RenderAction("Vehicle", "Vehicle", new { test = vehicle.ID, startDate = Model.startDateTime.ToString("yyyy-MM-dd HH:mm:ss"), endDate = Model.endDateTime.ToString("yyyy-MM-dd HH:mm:ss"), type = "details", CoverID = Model.policyCover.ID, VehicleUsage = vehicle.usage.ID, polid = Model.ID, imported = Model.imported });
                                    }
                                }
                            %>
                        </div>
                    </div>
                </div>
            </div>
            <%: Html.Hidden("proceed", "", new { id = "Continue", @class = "dont-process" })%> 

            <div class="btnlinks">
                <% Html.RenderAction("BackBtn", "Back"); %>
                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Policies", "Update")) && DateTime.Parse(ViewData["endDate"].ToString()) > DateTime.Now && !Model.imported && !Model.cancelled)
                    { %>
                    <button class="editButton editPolicyButton" type="button">Edit Policy</button>
                <% } %>
                <% if (!Model.cancelled && Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Policies", "Cancel"))) { %>
                    <button class="cancel_policy" type="button">Cancel Policy</button>
                <% } %>
                <% if (Model.cancelled && activate_policy){ %>
                    <button class="activate_policy" type="button">Re-Activate Policy</button>
                <% } %>
            </div>
 
        </fieldset>
    </div>
   

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <!-- css -->
    <link href="../../Scripts/datetimepicker-master/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/Pages.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/Policies.css" rel="stylesheet" type="text/css" />
      
    <!-- JS -->
    <script src="../../Scripts/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/policy.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/personDetails.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/policyDetails.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/avascript"></script>
    <script src="../../Scripts/ApplicationJS/policyinsureddropdown.js" type="text/javascript"></script> <!-- this manages the mailing address auto fill drop down -->
    <script src="../../Scripts/ApplicationJS/mortgagees.js" type="text/javascript"></script> <!-- this is for the autofill of mortgagees field in the vehicle card -->
    <script src="../../Scripts/ApplicationJS/MassPolicyFunctions.js" type="text/javascript"></script>
</asp:Content>


