﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Company>" %>

<div class="company">
    <% using (Html.BeginCollectionItem("company")) {%>
        <%: Html.ValidationSummary(true) %>

        <%: Html.HiddenFor(Model => Model.CompanyID, new { @class = "comId dont-process" }) %>
        <%: Html.HiddenFor(model => model.base_id, new { @class = "base_comp_id dont-process" }) %>
        <%: Html.HiddenFor(Model => Model.ID, new{ @class="comImportID dont-process" }) %>

        <div class="label"> Name: </div>
        <div class="data-field">
            <%: Html.HiddenFor(model => model.IAJID, new { @readonly = "readonly",  @class= "CompIAJID dont-process"})%>
            <%: Html.TextBox("Names", ViewData["companyName"], new { @class = "companyNames", @readonly = "readonly" })%>
        </div>
            
        <% if (!(bool)ViewData["compDetails"]) { %> 
               <input type="button" value="Delete" class="remCompany"/> 
        <% } %>
        <% else { %> 
             <div class="blueSpace"></div> 
        <%  } %>
        
    <% } %>
</div>