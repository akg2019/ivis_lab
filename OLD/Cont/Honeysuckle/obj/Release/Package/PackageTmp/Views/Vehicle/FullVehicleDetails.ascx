﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Vehicle>" %>

<% string RegNo = "Not Available";
   if (Model.VehicleRegNumber != null && Model.VehicleRegNumber != "") RegNo = Model.VehicleRegNumber; %>

<div class="container big nomtop">
    <div class="stripes"></div>
    <div class="data">
        <div class="header">
            Chassis No.
        </div>
        <div class="label chassis">
            <%: Model.chassisno %>
        </div>
    </div>
</div>
<div class="container small left">
    <div class="stripes"></div>
    <div class="data">
        <div class="header">
            Model Data
        </div>
        <div class="label">
            <%: Model.VehicleYear %> <%: Model.make %> <%: Model.VehicleModel %> <%: Model.extension %> <%: Model.colour %>
        </div>
    </div>
</div>
<div class="container small">
    <div class="stripes"></div>
    <div class="data">
        <div class="header">
            Registration No.
        </div>
        <div class="label">
            <%: RegNo %>
        </div>
    </div>
</div>
<% if (Model.mainDriver.DriverId != 0){ %>
    <div class="container big"> 
        <div class="stripes"></div>
        <div class="data">
            <div class="header">
                Main Driver TRN
            </div>
            <div class="label">
                <% if ((bool)ViewData["noTRN"])
                   { %>
                    Not Available
                <%}
                   else
                   { %>
                <%: Model.mainDriver.person.TRN%>
                <%} %>
            </div>
        </div>
    </div>
<% } %>


