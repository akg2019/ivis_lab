﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

   <% string key = Honeysuckle.Models.RSA.Encrypt(Honeysuckle.Models.UserModel.GetSessionGUID(Honeysuckle.Models.Constants.user), "session");
      int kid = Honeysuckle.Models.Constants.rsa.id;
   %>
  
 <% using (Html.BeginForm("Upload", "PolicyUpload", FormMethod.Post, new { enctype = "multipart/form-data" }))
               { %>
        <%: Html.Hidden("kid",kid,new{id= "kidvalue"}) %>
        <%: Html.Hidden("key", key, new { id = "keyvalue" })%>
        <input type="file" name="files" value="" accept=".txt" class="policyUpload" multiple/>
        <input type="submit" id="fileSubmit"/>
        <div>(Max Size: 100 MB). Make sure the key and kid tags are present.</div>
<% } %>
<%-- 
<%: Html.TextArea("uploadKeys", "Please copy key and kid to your file to succesfully process it:" + Environment.NewLine + "\"key\": {" + Environment.NewLine + "\"" + key + "\"" + Environment.NewLine + "}," +Environment.NewLine + "\"kid\": {" + Environment.NewLine + "\"" + kid + "\"" + Environment.NewLine + "}", new { id = "uploadKeys", @Style="width:550px;height:400px;margin-top:5px" })%>
--%>
<script src="../../Scripts/ApplicationJS/polUploadDialog.js" type="text/javascript"></script>
