﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div id="adv-search-form">
    <% if (ViewData["search"] != null) { %> <%: Html.TextBox("advsearch", null, new { id = "advanced-search-bar", @class = "adv-search-text first", @Value = ViewData["search"].ToString(), @PlaceHolder = "Search [General]" })%> <% } %>
    <% else { %>  <%: Html.TextBox("advsearch", null, new { id = "advanced-search-bar", @class = "adv-search-text additional", @PlaceHolder = "Search [General]" })%> <% } %>
    <button id="advsearchbtn" value="Search" onclick="search()" class="search-btn-adv" ><img src="../../Content/more_images/Search_Icon.png" /></button>
    <div id="more-terms"></div> <!-- THIS IS WHERE ALL THE NEW TERMS WOULD GO -->
</div>
<div id="add-search-term" class="add-dynamic small">
    <img src="../../Content/more_images/ivis_add_icon.png" />
    <span>Add Search Term</span>
</div>