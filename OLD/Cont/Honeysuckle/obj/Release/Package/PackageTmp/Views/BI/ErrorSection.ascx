﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div class="container big">
    <div class="form-header">
        <img src="../../Content/more_images/Certificate.png" />
        <span class="form-header-text">Error Data</span>
    </div>
    <canvas id="errorChart"></canvas>
</div>