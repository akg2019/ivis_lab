﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.UserGroup>>" %>

<%: Html.ListBox("allgroups", new SelectList(Model, "UserGroupID", "UserGroupName"), new { id = "allgroups" })%>