﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Notification>>" %>

    <%
        bool firstpage = false;
        bool lastpage = false;
        bool morepages = false;
        bool lesspages = false;

        int page = 1; string search = null;
        if (Request.QueryString["page"] != null) page = int.Parse(Request.QueryString["page"].ToString());
        if (Request.QueryString["search"] != null) search = Request.QueryString["search"].ToString();
                
        lesspages = (page > 1);
        firstpage = !lesspages;
            
        switch (ViewData["pagename"].ToString())
        {
            case "all":
                morepages = int.Parse(ViewData["allcount"].ToString()) > (page * 10);
                break;
            case "unread":
                morepages = int.Parse(ViewData["unreadcount"].ToString()) > (page * 10);
                break;
            case "trash":
                morepages = int.Parse(ViewData["trashcount"].ToString()) > (page * 10);
                break;
            default:
                break;       
        } 
            
        lastpage = !morepages;
        
    %>

    <%: Html.Hidden("pagecount", page, new { @class = "dont-process" })%>

    <div id="notification-box">
        <div id="notification-menu">
            <% if (ViewData["pagename"] == "unread") { %> <div id="unread" class="menu-item select" > <% } %>
            <% else { %> <div id="unread" class="menu-item" > <% } %>
                <img src="../../Content/more_images/Unread_Mail.png" class="mail_img" />
                <span><%: Html.ActionLink("Unread", "Index", null, new { @class = "menu-item-link" })%></span>
                <div class="mail-count"><% if ((int.Parse(ViewData["unreadcount"].ToString()) / 1000) > 1) { %> 999+ <% } %> <% else { %> <%: ViewData["unreadcount"]%> <% } %></div>
            </div>
            <% if (ViewData["pagename"] == "all") { %>  <div id="all" class="menu-item above select"> <% } %>
            <% else { %> <div id="all" class="menu-item above"> <% } %>
                <img src="../../Content/more_images/All_Mail.png" class="mail_img" />
                <span><%: Html.ActionLink("All Mail", "AllMail", null, new { @class = "menu-item-link" })%></span>
                <div class="mail-count"><% if ((int.Parse(ViewData["allcount"].ToString()) / 1000) > 1) { %> 999+ <% } %> <% else { %> <%: ViewData["allcount"] %> <% } %></div>
            </div>
            <% if (ViewData["pagename"] == "trash") { %> <div id="trash" class="menu-item above select"> <% } %>
            <% else { %> <div id="trash" class="menu-item above"> <% } %>
                <img src="../../Content/more_images/Trash_Mail.png" class="mail_img" />
                <span><%: Html.ActionLink("Trash", "Trash", null, new { @class = "menu-item-link" })%></span>
                <div class="mail-count"><% if ((int.Parse(ViewData["trashcount"].ToString()) / 1000) > 1) { %> 999+ <% } %> <% else { %> <%: ViewData["trashcount"]%> <% } %></div>
            </div>
        </div>
        <div id="notification-notes">
        <% if (Model.Count() == 0)
           { %>        
                <div class="no-notes">You have no notifications at this time.</div>
        <% } %>
        <% else 
           { %>
                <% foreach (var item in Model) { %>
                    <div class="notification-s">
                        <%: Html.HiddenFor(model => item.ID, new { @class = "note-id dont-process" })%>
                        <div class="line">
                            <div class="heading">
                                <div class="star">
                                <% if (!item.read) { %>
                                    <img src="../../Content/more_images/Star_Unread.png" class="unread-star" />
                                <% } %>
                                </div>
                                <span><%: item.heading%></span>
                            </div>
                            <div class="from italic">
                                From: <%: item.NoteFrom.username %>
                            </div>
                            <div class="date italic">
                                Date: <%: item.timestamp.ToString("MMM d, yyyy - hh:mm tt") %>
                            </div>
                        </div>
                        <div class="line">
                            <div class="line2">
                                <div class="content italic">
                                    <%: item.content %>
                                </div>
                                <div class="ellipses italic">...</div>
                                <div class="action">
                                    <div class="more mail-btn"><img src="../../Content/more_images/Read_More.png" /></div>
                                    <% if (ViewData["pagename"].ToString() != "trash") { %> <div class="garbage mail-btn"><img src="../../Content/more_images/To-Trash.png" /></div> <% } %>
                                    <div class="less mail-btn"><img src="../../Content/more_images/Read_less.png" /></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <% } %>
                
            <% } %>
        </div>
        <div id="pagenation">
            <% if (lesspages){ %> <div class="prevpage pagebtn" onclick="prevpage(<%:page%>)" title="browse to the previous page"><</div> <% } %>
            <% if (morepages){ %> <div class="nextpage pagebtn" onclick="nextpage(<%:page%>)" title="browse to the next page">></div> <% } %>
        </div>

    </div>

    <%: Html.Hidden("pagename", ViewData["pagename"], new { @class = "dont-process" })%>

    
        
    
    