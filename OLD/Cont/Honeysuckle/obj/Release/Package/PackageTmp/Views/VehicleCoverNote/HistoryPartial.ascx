﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.VehicleCoverNote>>" %>

<% 
    bool covernotes_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Read"));
    bool covernotes_update = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Update"));
%>

<!--<div class="history-data">-->
    <%: Html.Hidden("history-page", ViewData["historyPage"], new { @id = "history-page", @class = "dont-process" })%>
    <div class="ivis-table history-table main">
        <div class="row menu">
            <div class="item covernoteid">
                Cover Note ID
            </div>
            <div class="item effective">
                Effective
            </div>
            <div class="item expiry">
                Expiry
            </div>
            <div class="item cancel">
                Cancelled
            </div>
            <div class="item approve">
                Approved
            </div>
            <div class="item cname">
                Company
            </div>
        </div>
        <div class="covers-history main">
            <% int i = 0; %>
            <% foreach(var cover in Model) { %>
                <% if (i%2 == 0) { %>
                    <div class="covernote row data odd">
                <% } %>
                <% else { %>
                    <div class="covernote row data even">
                <% } %>
                <% i++; %>
                    <%: Html.Hidden("t", cover.ID, new { @class = "id hiddenid dont-process" })%>
                    <div class="item covernoteid">
                        <%: cover.covernoteid %>
                    </div>
                    <div class="item effective">
                        <%: cover.effectivedate.ToString("dd/MM/yyyy") %>
                    </div>
                    <div class="item expiry">
                        <%: cover.expirydate.ToString("dd/MM/yyyy") %>
                    </div>
                    <div class="item cancel">
                        <%: Html.CheckBox("can",cover.cancelled, new { @disabled = "disabled"})  %>
                    </div>
                    <div class="item approve">
                        <%: Html.CheckBox("apr",cover.approved, new { @disabled = "disabled"})  %>
                    </div>
                    <div class="item cname">
                        <%: cover.company.CompanyName %>
                    </div>
                  
                  <% if (ViewData["historyPage"] == "main")
                     { %>
                    <div class="item functions">
                        <% if (covernotes_read)
                           { %>
                            <div class="icon covernote view" title="Cover Note Details">
                                <img src="../../Content/more_images/view_icon.png" class="sprite" />
                            </div>
                        <% } %>
                        <% if (covernotes_update && cover.expirydate >= DateTime.Now && cover.effectivedate <= DateTime.Now && cover.firstprinted == null && !cover.cancelled && !cover.approved)
                           {  %>
                            <div class="icon covernote edit" title="Cover Note Edit">
                                <img src="../../Content/more_images/edit_icon.png" class="sprite" />
                            </div>
                        <% } %>
                    </div>
                    <%} %>
                </div>
            <% } %>
        </div>
    </div>
<!--
</div>

</div>
-->