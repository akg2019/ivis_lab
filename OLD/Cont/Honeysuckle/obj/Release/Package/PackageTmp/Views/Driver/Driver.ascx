﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Driver>" %>

    <% using (Html.BeginCollectionItem("person")) {%>
        <%: Html.ValidationSummary(true) %>

        
         <!-- RENDER PERSON -->
            <% if (Model.person != null)
               { %>
                <div class="address">
                    <% Html.RenderPartial("~/Views/People/Person.ascx", Model.person); %>
                </div>
            <% } %>
            <% else
               { %>
               
                <div class="editor-label">
                TRN:
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.person.TRN, new { @required = "required", id = "trn" })%>
                <%: Html.ValidationMessageFor(model => model.person.TRN)%>
            </div>

            <div class="editor-label">
                First Name:
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.person.fname, new { @required = "required", id = "fname" })%>
                <%: Html.ValidationMessageFor(model => model.person.fname)%>
            </div>
            
            <div class="editor-label">
                Middle Name:
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.person.mname, new { id = "mname",@required="required" })%>
                <%: Html.ValidationMessageFor(model => model.person.mname)%>
            </div>
            
            <div class="editor-label">
                Last Name:
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.person.lname, new { @required = "required", id = "lname" })%>
                <%: Html.ValidationMessageFor(model => model.person.lname)%>
            </div>
            
         
            <% Html.RenderAction("Address", "Address"); %>
            
   
   <script src="../../Scripts/ApplicationJS/existingDriver.js" type="text/javascript"></script> 
   <script src="../../Scripts/ApplicationJS/multiple-emails.js" type="text/javascript"></script>
   <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>
   <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
            <% } %>


       <% } %>