﻿<%@ Page Title="Modify User Details" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.User>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <div id="error"><%:ViewData["error"] %></div>

            <div id= "form-header"><img src="../../Content/more_images/Profile.png" />
                <div class="form-text-links">
                    <span class="form-header-text">Edit My Profile</span>
                    <div class="icon">
                        <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                    </div>
                </div>
            </div> 
            <div class="field-data">
                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right down"></div>
                        User Information
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <%: Html.HiddenFor(model => model.ID, new { id = "hiddenid", @class= "userid dont-process" }) %>

                        <div class="editor-label">
                           User Name
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.username, new {@readonly = "readonly", id="username", @class ="read-only" })%>
                            <%: Html.ValidationMessageFor(model => model.username)%>
                        </div>
                    </div>
                </div>
                <div class="validate-section password-area">
                    <div class="subheader"> 
                        <div class="arrow right"></div>
                        Change Password 
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="editor-label">
                            Password 
                        </div>
                        <div class="editor-field">
                            <%: Html.PasswordFor(model => model.password, new { id = "password1", @autocomplete = "off", @class = "password" })%>
                            <%: Html.ValidationMessageFor(model => model.password)%>
                        </div>
            
                        <div class="editor-label">
                           Confirm Password 
                        </div>
                        <div class="editor-field">
                            <%: Html.PasswordFor(model => model.confirm_password, new { id = "password2", @autocomplete = "off", @class = "password" })%>
                            <%: Html.ValidationMessageFor(model => model.confirm_password)%>
                        </div>
                    </div>
                </div>

                <% if (Model.employee != null) { %>
                    <div class="validate-section">
                        <div class="subheader"> 
                            <div class="arrow right"></div>
                            Company Information
                            <div class="valid"></div>
                        </div>
                        <div class="data">
                            <% Html.RenderAction("EmployeeCompany", "Company", new { compId = Model.employee.company.CompanyID }); %>
                        </div>
                    </div>
                    <% Html.RenderPartial("~/Views/People/Person.ascx", Model.employee.person, new ViewDataDictionary{{"edit", true}, {"policyInsured", false}}); %>
                <% } %>
   
                <div class="btnlinks">
                    <% Html.RenderAction("BackBtn", "Back"); %>
                    <input type="submit" value="Save" id="modify_btn" class="submitbutton" />
                </div>

            </div>
        </fieldset>

    <% } %>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/person-trn-edit.js" type="text/javascript"></script>
    <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/loginform.js" type="text/javascript"></script>
    <link href="../../Content/email.css" rel="stylesheet" type="text/css" />
</asp:Content>


