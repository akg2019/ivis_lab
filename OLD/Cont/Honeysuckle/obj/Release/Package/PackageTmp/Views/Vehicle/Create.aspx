﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Vehicle>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

   
    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            <legend></legend>
            
            <div id= "form-header">  <img src="../../Content/more_images/Vehicle.png" alt=""/> 
                <div class="form-text-links">
                     Create Vehicle
                </div>
            </div> <br /><br /><br /><br /><br /><br />
            <br/><br />
            <div class="editor-label">
               Chassis Number <span>*</span>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.chassisno, new { @required = "required", id = "chassis" }) %>
                <%: Html.ValidationMessageFor(model => model.chassisno) %>
            </div>
            
            <div class="editor-label">
                VIN <span> *</span>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.VIN, new { @required = "required", id = "vin" }) %>
                <%: Html.ValidationMessageFor(model => model.VIN) %>
            </div>

            <div class="editor-label">
               Make <span>*</span>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.make, new { @required = "required", id = "make" }) %>
                <%: Html.ValidationMessageFor(model => model.make) %>
            </div>
            
            <div class="editor-label">
               Model <span>*</span>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.VehicleModel, new { @required = "required", id = "vehiclemodel" })%>
                <%: Html.ValidationMessageFor(model => model.VehicleModel)%>
            </div>
            
            <div class="editor-label">
                Vehicle Registration Number <span>*</span>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.VehicleRegNumber, new { @required = "required", id = "vehicleRegNum" })%>
                <%: Html.ValidationMessageFor(model => model.VehicleRegNumber) %>
            </div>

            <div class="editor-label">
                Model Type:
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.modelType, new { id = "modeltype" }) %>
                <%: Html.ValidationMessageFor(model => model.modelType) %>
            </div>
            
            <div class="editor-label">
               Body Type:
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.bodyType, new {id = "bodytype" })%>
                <%: Html.ValidationMessageFor(model => model.bodyType) %>
            </div>
            
            <div class="editor-label">
                Extension:
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.extension, new {id = "extension" }) %>
                <%: Html.ValidationMessageFor(model => model.extension) %>
            </div>
            
            <div class="editor-label">
                Colour: 
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.colour, new {id = "colour" })%>
                <%: Html.ValidationMessageFor(model => model.colour) %>
            </div>
            
            <div class="editor-label">
                Cylinders: 
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.cylinders, new {id = "cylinders" })%>
                <%: Html.ValidationMessageFor(model => model.cylinders) %>
            </div>
            
            <div class="editor-label">
                Engine Number: 
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.engineNo, new {id = "engineNo" })%>
                <%: Html.ValidationMessageFor(model => model.engineNo) %>
            </div>
            
            <div class="editor-label">
               Engine Modified:
           
                <%: Html.CheckBoxFor(model => model.engineModified, new { id = "engineModified", Style = "width: 14px; height:14px;" })%>
            </div><br /><br />
            
            <div class="editor-label">
                Engine Type:
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.engineType, new {id = "engineType" })%>
                <%: Html.ValidationMessageFor(model => model.engineType) %>
            </div>
            
            <div class="editor-label">
                Estimated Annual Mileage :
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.estAnnualMileage, new {id = "mileage" })%>
                <%: Html.ValidationMessageFor(model => model.estAnnualMileage) %>
            </div>
            
            <div class="editor-label">
               
                Expiry Date Of Fitness <span>*</span>
            </div>
            <div class="editor-field">
                 <%: Html.TextBox("expiryDateoffitness", null, new { @required = "required", id = "fitnessExpiry", @readonly = "readonly", @Value = ViewData["expiryDate"].ToString() })%>
               
            </div>
            <div class="editor-label">
                 HPPCC Unit Type: 
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.HPCCUnitType, new {id = "hpccUnit" })%>
                <%: Html.ValidationMessageFor(model => model.HPCCUnitType) %>
            </div>
            
            <div class="editor-label">
                Reference Number:
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.referenceNo, new {id = "refno" })%>
                <%: Html.ValidationMessageFor(model => model.referenceNo) %>
            </div>
            
            <div class="editor-label">
                Registration Location:
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.registrationLocation, new {id = "regLocation" })%>
                <%: Html.ValidationMessageFor(model => model.registrationLocation) %>
            </div>
            
            <div class="editor-label">
                Roof Type:
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.roofType, new {id = "roofType" })%>
                <%: Html.ValidationMessageFor(model => model.roofType) %>
            </div>
            
            <div class="editor-label">
                Seating Number 
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.seating, new {id = "seating" })%>
                <%: Html.ValidationMessageFor(model => model.seating) %>
            </div>
            
            <div class="editor-label">
               Seating Certificate:
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.seatingCert, new {id = "seatingCert" })%>
                <%: Html.ValidationMessageFor(model => model.seatingCert) %>
            </div>
            
            <div class="editor-label">
                Tonnage:
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.tonnage, new {id = "tonnage" })%>
                <%: Html.ValidationMessageFor(model => model.tonnage) %>
            </div>
            
            <div class="editor-label">
               Vehicle Year <span>*</span>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.VehicleYear, new { id = "vehicleYear", @required = "required" })%>
                <%: Html.ValidationMessageFor(model => model.VehicleYear) %>
            </div>

            <div class="editor-label">
               Hand Drive Type 
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.handDrive.handDriveType, new {id = "handDriveType" })%>
                <%: Html.ValidationMessageFor(model => model.handDrive.handDriveType)%>
            </div>

            <div class="editor-label">
                Transmission Type 
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.transmissionType.transmissionType, new {id = "tranType" })%>
                <%: Html.ValidationMessageFor(model => model.transmissionType.transmissionType)%>
            </div>

            <div class="editor-label">
               Registered Owner 
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.RegisteredOwner, new { id = "regOwner" })%>
                <%: Html.ValidationMessageFor(model => model.RegisteredOwner)%>
            </div>

            <br /><br />
             <div class="HeadingBar"> Main Driver of Vehicle </div><br />

            <%if (Model.mainDriver != null) { %> <% Html.RenderPartial("~/Views/People/MainDriver.ascx", Model.mainDriver.person); %> <% } %>
            <% else { %> <% Html.RenderAction("MainDriver", "People"); %> <% } %>

           <div id="drivers1"></div> 
          
            <br /><br />  <br /><br />
            <p>
                <input type="submit" value="Create" id= "Submit2"/>
            </p>
        </fieldset>

    <% } %>

    <div id="spinner"></div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/spin.min.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/vehicleDatePicker.js" type="text/javascript"></script>
    <!--<script src="../../Scripts/ApplicationJS/TestVIN.js" type="text/javascript"></script>-->
    <script src="../../Scripts/ApplicationJS/Vehiclemanagement.js" type="text/javascript"></script> 
    <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/addDrivers.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/transmissionAndHanddrive.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/TestChassis.js" type="text/javascript"></script>
    <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
    
</asp:Content>


