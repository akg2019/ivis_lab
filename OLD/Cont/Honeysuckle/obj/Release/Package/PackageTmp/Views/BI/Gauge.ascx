﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.KPI>>" %>

<div class="container small">

    <% 
        bool viewVehicles = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Vehicles", "Read"));
        bool viewCoverNotes = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Read")); 
    %>

    <%
        var heading = "";
        var div = "";
        var icon = "";
        var link = "";
        switch (ViewData["type"].ToString())
        {
            case "enquiry":
                div = "enquiryGauge";
                heading = "Enquiries";
                break;
            case "error":
                div = "errorGauge";
                heading = "Import Failure";
                break;
            case "covernote":
                heading = "Cover Note";
                div = "cnoteGauge";
                icon = "<img src=\"../../Content/more_images/Cover_Note.png\" />";
                link = "/VehicleCoverNote/";
                break;
            case "certificate":
                heading = "Certificate";
                div = "certGauge";
                icon = "<img src=\"../../Content/more_images/Certificate.png\" />";
                break;
            case "doubleInsured":
                heading = "Double Coverage";
                div = "doubleInsuredGauge";
                link = "/Vehicle/DoubleInsuredVehicles";
                break;
            default:
                break;
           
        }
        if (icon == "") icon = "<img src=\"../../Content/more_images/Company.png\" />"; 
    %>
    
    <% foreach (var item in Model) { %>
        <%: Html.Hidden("data", item.count, new { @class = item.mode + " data " + ViewData["type"].ToString() + " dont-process" }) %>
    <% } %>

    <% Html.RenderAction("GaugeCount", "BI", new { type = ViewData["type"].ToString(), datemode = "month" }); %>

    <div class="stripes"></div>
    <div class="form-header">
        <div class="form-text-links">
            <span class="form-header-text"><%: heading %> Gauge</span>
        </div>
    </div>
    
    <div id=<%: div %> style="width: 100%;height:200px;" ></div>
    <% if (!(bool)ViewData["iaj"]){ %>
        <% if (link != "" && div == "cnoteGauge" && viewCoverNotes) { %>
            <a href="<%: link %>" class="gauge-links" >See More Information</a>
        <% } %>

        <% if (link != "" && div == "doubleInsuredGauge" && viewVehicles) { %>
            <a href="<%: link %>" class="gauge-links" >See More Information</a>
        <% } %>
    <% } %>
</div>