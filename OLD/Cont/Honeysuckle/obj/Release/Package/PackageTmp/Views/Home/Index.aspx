﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.User>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% if (Honeysuckle.Models.Constants.user.username == "not initialized") 
        { %>
        <div class="error"><%:ViewData["error"] %></div>
    <%  } %>
    <div id="credentials" ><%:ViewData["Message"] %></div>
    <% if (ViewData["permissionMessage"] != null)
       { %>
        <div id="permissionMessage"><%: ViewData["permissionMessage"]%> </div>
    <% } %>

   <%  if (Honeysuckle.Models.Constants.user.username != "not initialized" && ViewData["FileUpload"] != null)
       {%>
        <div id="UploadMessage"><%: ViewData["FileUpload"]%></div>
    <% } %>

        <% if (ViewData["reset"] != null)
           { %>
             <div class="error">The user: <%: ViewData["reset"] %> has been sent a reset link</div>
        <% } %>
         <% if (Honeysuckle.Models.Constants.user.username == "not initialized")
            { %>
            <div id="login-form" class="box-shadow tiny-box">
                <% using (Html.BeginForm())
                   { %>
                    <div class="heading"> <div id="Icon"><img src="../../Content/more_images/IVIS-LOGO-WHITE.png" /></div><div id="welcomeMessage" class="century-gothic"> Welcome To IVIS</div></div>
                    <div id="LoginHeading" class="century-gothic">Login </div> <hr />
                    <div class="info">
                        <div id ="username"> <%: Html.TextBoxFor(Model => Model.username, new { @class="tiny-box-field top", @required = "required", placeholder = "Username" })%></div>
                        <div id="password"><%: Html.PasswordFor(Model => Model.password, new { @class = "tiny-box-field bottom", placeholder = "Password" })%></div>
                        <div id="submit"><input type="submit" name="submit" value="Sign In" class="tiny-box-btn"  /></div>
                        <div id="reset"><input id= "forgotPass" type="submit" name="submit" value="Forgot Password" /></div>
                    </div>
                <% } %>
            </div>
        <% } %>
 <% else{ %> <% Html.RenderAction("Dashboard", "Dashboard"); %><% } %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/loginform.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/EmailPopup.js" type="text/javascript"></script>
     <% if (Honeysuckle.Models.Constants.user.username == "not initialized")
         { %>
            <link href="../../Content/LoginPage.css" rel="stylesheet" type="text/css" />
      <% } %>

      <% else
         { %>
            <link href="../../Content/Dashboard.css" rel="stylesheet" type="text/css" />
            <script src="../../Scripts/ApplicationJS/dashboard.js" type="text/javascript"></script>
            
            <script src="../../Scripts/ApplicationJS/companies-list.js" type="text/javascript"></script>
            <script src="../../Scripts/ApplicationJS/certscover.js" type="text/javascript"></script>
            <link href="../../Content/companies-list.css" rel="stylesheet" type="text/css" />
            <link href="../../Content/certs-cover-dash.css" rel="stylesheet" type="text/css" />
            
            <!-- datepicker scripts -->
            <script src="../../Scripts/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript"></script>
            <link href="../../Scripts/datetimepicker-master/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
            
            <!-- charting scripts -->
            <script src="../../Scripts/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
            <script src="../../Scripts/amcharts/amcharts/serial.js" type="text/javascript"></script>
            <script src="../../Scripts/amcharts/amcharts/pie.js" type="text/javascript"></script>
            <script src="../../Scripts/amcharts/amcharts/gauge.js" type="text/javascript"></script>

            <script src="../../Scripts/ApplicationJS/business-intelligence.js" type="text/javascript"></script> 

      <% } %>

</asp:Content>