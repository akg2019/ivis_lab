﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<IEnumerable<Honeysuckle.Models.KPI>>>" %>

<div class="kpi-box">
  
    <% 
        string header = "";
        switch(ViewData["type"].ToString()) {
            case "enquiry":
                header = "Enquiry";
                break;
            case "covernote":
                header = "Cover Note";
                break;
            case "certificate":
                header = "Certificate";
                break;
            case "error":
                header = "Import Failure";
                break;
            case "doubleInsured":
                header = "Double Coverage";
                break;
            default:
                break;
        }
    %>
        
    <div>
        <% foreach(var kpi in Model){ %>
            <div class = "header-kpi"><span class = "header-kpi-name"><%: header %></span></div>
            <div class="kpi">
                <% if (kpi.Count() > 0) { %> <div class="type"><%//: kpi.ElementAt(0).index_type.ToUpper() %></div> <% } %>
                <% int i = 0; %>
                <% foreach(var item in kpi){ %>
                    <% using (Html.BeginCollectionItem("kpis")) { %>
                        <div class="stuff">

                            <% switch (item.mode)
                               {
                                   case "yellow": %>
                            <div class ="color-boxes yellow"></div>
                            <% break; %>
                                <% case "red": %>
                            <div class ="color-boxes red"></div>
                            <% break; %>
                            <% case "green": %>
                            <div class ="color-boxes green"></div>
                            <% break;
                               default:
                               break;
                               }%>

                            
                            <%: Html.Hidden("id", item.id, new { @class = "dont-process" })%>
                            <div class="kpi-line">
                                <span><%: item.mode.Substring(0, 1).ToUpper() + item.mode.Substring(1) %></span>
                            </div>
                            <div class="kpi-count">
                                <span>from</span>
                                <% if (i == 0) { %> <%: Html.TextBox("count-prev", 0, new { @readonly = "readonly", @class = "read-only count-prev" })%> <% } %>
                                <% else { %> <%: Html.TextBox("count-prev", kpi.ElementAt(i - 1).count, new { @readonly = "readonly", @class = "read-only count-prev" })%> <% } %>
                                <% i++; %>
                                <span>to</span>
                                <%: Html.TextBox("count", item.count, new { @class = "count "+ item.mode })%>
                            </div>
                        </div>
                    <% } %>
                <% } %>
            </div>
        <% } %>
    </div>
</div>
