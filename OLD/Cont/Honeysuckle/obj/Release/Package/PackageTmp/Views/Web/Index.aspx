﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head runat="server">
        <title>External Access</title>

        <!-- JQUERY -->
        <script src="../../Scripts/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../Scripts/jquery-ui-1.10.4.min.js" type="text/javascript"></script>
        <link href="../../Content/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

        <script src="../../Scripts/ApplicationJS/external.js" type="text/javascript"></script>
        <link href="../../Content/ExternalAccess.css" rel="stylesheet" type="text/css" />
        <link href="../../Content/ExternalAccess-PartialWidth.css" rel="stylesheet" media="screen and (max-width:1024px)" type="text/css" />
        <link href="../../Content/Site.css" rel="stylesheet" type="text/css" />

        <link href="../../Content/maindiv-hd.css" rel="stylesheet" type="text/css" />
        <link href="../../Content/maindiv-fullheight.css" media='screen and (min-height: 900px) and (max-height: 1000px)' rel="stylesheet" type="text/css" />
        <link href="../../Content/maindiv-partialheight.css" rel="stylesheet" media="screen and (max-height:900px)" type="text/css" />

    </head>
    <body>
        <div class="page">
            <div id="header">
                <div id="header-content">
                    <a href="/Home/"> <img src='/Content/more_images/IVIS_Logo.png' id = "IconContainer" /> </a>
                    <div id="selfdata"></div>
                </div>
            </div>
            <div id="main-div">
                <div id="maincontent">
                    <div id="maincontent-insert">
                        <div class="field-data">
                            <% using(Html.BeginForm()) { %>
                                <div id="searchModal"><%: ViewData["searchDone"].ToString() %></div>
                                <div id= "WelcomeMessage">  Welcome to the vehicle certificate and cover note verification sytem.</div>
                                <div id="instructions"> Follow the instructions below to verify if the vehicle is under cover by an insurance company </div>

                                <div id="TestRisk">
                                    <div class= "Steps"> 
                                       <div class="rectangle" id="rec1">Step</div>
                                       <div class="triangles" id="tri1"></div> 
                                       <div class="numbers">01</div> 
                                    </div>
                                    <div class="dataField"> Enter the license plate number  <%: Html.TextBox("license") %>  </div>
                                        <div id="OrDiv"> - OR - </div>
                                    <div class="dataField"> Enter the chassis number  <%: Html.TextBox("chassis") %> </div>  
                                </div>

                                <div id="submitRisk">
                                    <div class= "Steps">
                                    <div class="rectangle" id="rec2">Step</div>
                                    <div class="triangles" id="tri2"></div>
                                    <div class="numbers">02</div> 
                                    </div>
                                    <div class="dataField1"><br /> Click Search 
                                    <input type="submit" value="Search" disabled="disabled" id="subbtn" />
                                    </div>
                                </div>

                                <div id="Results">
                                    <div class= "Steps">
                                        <div class="rectangle" id="rec3">Step</div>
                                        <div class="triangles" id="tri3"></div>
                                        <div class="numbers">03</div> 
                                    </div>
                                    <div class="dataField1">Your results will appear here: </div>

                                    <% string Answer= ""; %>
                                    <% 
                                        if (ViewData["model"] != null)
                                        {
                                            if ((bool)ViewData["model"]) { Answer = "The vehicle in question IS CURRENTLY covered."; }
                                            else { Answer = "The vehicle in question IS NOT CURRENTLY covered."; }
                                        } 
                                    %>
                                    <%: Html.TextArea("reply", Answer, new {id= "ResponseField", @readonly= "readonly"}) %>

                                </div>
             
                            <% } %>
                        </div>
                    </div>
                </div>
            </div>
            <div id="footer">
                <div id="whitespace"></div>
                <div id="inner-footer">
                    <div id="Logofooter">
                        <img src="../../Content/more_images/IVIS_Logo_cropped.png" />
                        <span>2014 IVIS, All Rights Reserved</span>
                    </div> 
                    <div class="footlinks">
                        <%: Html.ActionLink("Send Feedback", "Index", "Feedback", null, new { id = "feedback" })%>
                        <%: Html.ActionLink("Help", "Index", "Help", null, new { id = "help" })%>
                    </div>
                </div>
            </div>      
        </div>
    
    </body>
</html>
