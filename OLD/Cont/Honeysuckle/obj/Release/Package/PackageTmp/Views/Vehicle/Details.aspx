﻿<%@ Page Title="Vehicle Details" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Vehicle>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <fieldset> 
        <div id= "form-header">
        <img src="../../Content/more_images/Vehicle.png" alt=""/>
            <div class="form-text-links">
                 <span class="form-header-text">Vehicle Details</span>
                 <div class="icon">
                    <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                </div>
            </div>
        </div>
    
        <div class="field-data">
            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right down"></div>
                    Main Vehicle Data
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <%: Html.HiddenFor(model => model.ID, new { @class = "vehdetails dont-process"})%>
    
                    <div class ="editor-label">
                        VIN
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.VIN, new { @class = "read-only", @readonly = "readonly" })%>
                    </div> 
            
                    <div class ="editor-label">
                        Chassis Number
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.chassisno, new { @class = "read-only", @readonly = "readonly" })%>
                    </div> 
            
                    <div class ="editor-label">
                        Make
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.make, new { @class = "read-only", @readonly = "readonly" })%>
                    </div> 
       
                    <div class ="editor-label">
                        Model
                    </div> 
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.VehicleModel, new { @class = "read-only", @readonly = "readonly" })%>
                    </div> 
        
                    <div class ="editor-label">
                        Model Type
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.modelType, new { @class = "read-only", @readonly = "readonly" })%>
                    </div> 
        
                    <div class ="editor-label">
                        Body Type
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.bodyType, new { @class = "read-only", @readonly = "readonly" })%>
                    </div> 
        
                    <div class ="editor-label">
                        Extension
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.extension, new { @class = "read-only", @readonly = "readonly" })%>
                    </div> 
        
                    <div class ="editor-label">
                        Registration Number
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.VehicleRegNumber, new { @class = "read-only", @readonly = "readonly" })%>
                    </div> 
                    <div class ="editor-label">
                        Vehicle Year
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.VehicleYear, new { @class = "read-only", @readonly = "readonly" })%>
                    </div>
                </div>
            </div>

            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right"></div>
                    Vehicle Meta Data
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div class ="editor-label">
                        Colour
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.colour, new { @class = "read-only", @readonly = "readonly" })%>
                    </div> 
        
                    <div class ="editor-label">
                        Cylinders
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.cylinders, new { @class = "read-only", @readonly = "readonly" })%>
                    </div> 
        
                    <div class ="editor-label">
                        Engine Number
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.engineNo, new { @class = "read-only", @readonly = "readonly" })%>
                    </div> 
        
                    <div class="check-div">
                        <div class ="editor-label">
                            Engine Modified
                        </div>
                        <div class="editor-field">
                            <%: Html.CheckBoxFor(model => model.engineModified, new { @disabled = "disabled" })%>
                        </div>
                    </div>
        
                    <div class ="editor-label">
                        Engine Type
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.engineType, new { @class = "read-only", @readonly = "readonly" })%>
                    </div> 
        
                    <div class ="editor-label">
                        Estimated Annual Mileage
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.estAnnualMileage, new { @class = "read-only", @readonly = "readonly" })%>
                    </div> 
        
                    <div class ="editor-label">
                        Expiry Date Of Fitness
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.expiryDateOfFitness, new { @class = "read-only", @readonly = "readonly", @Value = ViewData["fitnessExpiry"]})%>
                    </div> 
        
                    <div class ="editor-label">
                        HPCC Unit Type
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.HPCCUnitType, new { @class = "read-only", @readonly = "readonly" })%>
                    </div> 
        
                    <div class ="editor-label">
                        Reference Number
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.referenceNo, new { @class = "read-only", @readonly = "readonly" })%>
                    </div>
        
                    <div class ="editor-label">
                        Registration Location
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.registrationLocation, new { @class = "read-only", @readonly = "readonly" })%>
                    </div>
        
                    <div class ="editor-label">
                        Roof Type
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.roofType, new { @class = "read-only", @readonly = "readonly" })%>
                    </div>
                    
                    <div class="editor-label">
                        Transmission Type
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.transmissionType.transmissionType, new { id = "tranType", @class = "read-only", @readonly = "readonly" })%>
                    </div>
                    
                    <div class="editor-label">
                        Hand Drive Type
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.handDrive.handDriveType, new { id = "handDriveType", @class = "read-only", @readonly = "readonly" })%>
                    </div>
                    
                    <div class ="editor-label">
                        Seating
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.seating, new { @class = "read-only", @readonly = "readonly" })%>
                    </div>
        
                    <div class ="editor-label">
                        Seating Cert
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.seatingCert, new { @class = "read-only", @readonly = "readonly" })%>
                    </div>
        
                    <div class ="editor-label">
                        Tonnage
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.tonnage, new { @class = "read-only", @readonly = "readonly" })%>
                    </div>
                    <div class="editor-label"> 
                        Registered Owner
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.RegisteredOwner, new { @class = "read-only", @readonly = "readonly" })%>
                    </div>

                    <div class ="editor-label">
                        Estimated Value
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.estimatedValue, new { @class="read-only", @readonly="readonly" })%>
                    </div>
                    
                    <div class="check-div">
                        <div class ="editor-label">
                            Restricted Driving
                        </div>
                        <div class="editor-field">
                            <%: Html.CheckBoxFor(model => model.RestrictedDriving, new { @disabled = "disabled" })%>
                        </div>
                    </div>
                </div>
            </div>
            <% if (Model.mainDriver != null) { %>
                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right"></div>
                        Main Driver of Vehicle
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="editor-label">
                            TRN
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.mainDriver.person.TRN, new { @readonly = "readonly", @class = "read-only" })%>
                        </div>

                        <div class="editor-label">
                            First Name
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.mainDriver.person.fname, new { @readonly = "readonly", @class = "read-only" })%>
                        </div>

                        <div class="editor-label">
                            Middle Name
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.mainDriver.person.mname, new { @readonly = "readonly", @class = "read-only" })%>
                        </div>

                        <div class="editor-label">
                            Last Name
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.mainDriver.person.lname, new { @readonly = "readonly", @class = "read-only" })%>
                        </div>
                    </div>
                </div>
                
                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right"></div>
                        Main Driver Address Data
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <% Html.RenderAction("Address", "Address", new { id = Model.mainDriver.person.address.ID }); %>
                    </div>
                </div>
            <% } %>
        </div>
        
        <div class="btnlinks">
            <% Html.RenderAction("BackBtn", "Back"); %>
            <%  if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Vehicles", "Update"))) { %>
                <input type="button" id="btn_EditVehicle" value="Edit Vehicle" class="BtnSpecial" />
            <% } %>
        </div>
    </fieldset>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/personDetails.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/vehiclecovernote-list.js" type="text/javascript"></script>
    <link href="../../Content/vehiclecovernote-list.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/person-details.js" type="text/javascript"></script>
</asp:Content>
