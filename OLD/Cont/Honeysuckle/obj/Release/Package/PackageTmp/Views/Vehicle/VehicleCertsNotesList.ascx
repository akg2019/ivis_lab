﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.CertCover>>" %>

<%
    bool cert_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Read"));
    bool cert_edit = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Update"));
    bool cert_approve = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Approve"));
    bool cover_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Read"));
    bool cover_edit = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Update"));
    bool cover_approve = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Approve"));
%>

<div id="container-covercerts" class="container big">
    <div class="stripes"></div>
    <div class="dashboard-pad">
        <div class="form-header"><img src="../../Content/more_images/Company.png" />
              <div class="form-text-links">
                <span class="form-header-text">Cover List <span id = "filter-id">Filter Results: Both <%: Html.RadioButton("filter", "Both", true, new { @id = "both"})%> Cover <%: Html.RadioButton("filter", "Cover", new { @id = "cover" })%>  Certificate <%: Html.RadioButton("filter", "Certificate", new { @id = "certificate"})%></span></span>
               </div>
        </div>

        <div class="ivis-table">
            <div class="covercert menu">
                <div class="type item">Type</div>
                <div class="number item">Number</div>
                <div class="effective item">Effective Date</div>
                <div class="expiry item">Expiry Date</div>
                <div class="created item">Created</div>
                <div class="policy item">PolicyNo</div>
            </div>
            <div id="covercerts" class="table-data">
           
                <% int i = 0; %>
                <% foreach(var item in Model){ %>
                    
                    <% i++; %>
                    <% if (i%2 == 1) { %>
                        <div class="covercert data odd row">
                    <% } %>
                    <% else{ %>
                        <div class="covercert data even row">
                    <% } %>

                        <% 
                        switch(item.Type){
                            case "Cover": %>
                                <%: Html.Hidden("ID", item.vCoverNote.ID, new { @class = "id dont-process" })%>
                                <%: Html.Hidden("hiddentype", item.Type, new { @class = "CoverHidtype CoverCertHidType dont-process" })%>
                                <div class="type item">Cover Note</div>
                                <div class="number item"> <%: item.vCoverNote.covernoteid %></div>
                                <div class="effective item"> <%: item.vCoverNote.effectivedate.ToString("dd MMM yyyy") %></div>
                                <div class="expiry item"> <%: item.vCoverNote.expirydate.ToString("dd MMM yyyy")%></div>
                                <div class="created item"><%: item.vCoverNote.createdOn.ToString("dd MMM yyyy")%></div>
                                <div class="policy item"><%: item.vCoverNote.Policy.policyNumber %></div>
                            <% 
                                break; 
                            case "Certificate": %>
                                <%: Html.Hidden("ID", item.vCertificate.VehicleCertificateID, new { @class = "id dont-process" })%>
                                <%: Html.Hidden("hiddentype", item.Type, new { @class = "CertHidtype CoverCertHidType dont-process" })%>
                                <div class="type item">Certificate</div>
                                <div class="number item "><%: item.vCertificate.CertificateNo %></div>
                                <div class="effective item"><%: item.vCertificate.EffectiveDate.ToString("dd MMM yyyy") %></div>
                                <div class="expiry item"><%: item.vCertificate.ExpiryDate.ToString("dd MMM yyyy") %></div>
                                <div class="created item"><%: item.vCertificate.createdOn.ToString("dd MMM yyyy")%></div>
                                <div class="policy item"><%:  item.vCertificate.policy.policyNumber%></div>
                            <%
                                break;
                            default:
                                break;%>

                        <% } %>
                        <div class="functions item">
                            <% if ((item.Type == "Cover" && cover_read) || (item.Type == "Certificate" && cert_read)) { %>
                                <div class="icon view" title="View Cover/Certificate">
                                    <img src="../../Content/more_images/view_icon.png" class="sprite" />
                                </div>
                            <% } %>
                            <% if ((item.Type == "Cover" && cover_edit) || (item.Type == "Certificate" && cert_edit)) { %>
                                <div class="icon edit" title="Edit Cover/Certificate">
                                    <img src="../../Content/more_images/edit_icon.png" class="sprite" />
                                </div>
                            <% } %>
                            <% if ((item.Type == "Cover" && cover_approve) || (item.Type == "Certificate" && cert_approve)) { %>
                                <div class="icon enable" title="Approve Cover/Certificate">
                                    <img src="../../Content/more_images/enable_icon.png" class="sprite" />
                                </div>
                            <% } %>
                        </div>

                    </div>
                <% } %>
            </div>
        </div>
    </div>
</div>

