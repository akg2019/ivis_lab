﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Person>" %>

<div class="insured-row selected row">

  <%: Html.HiddenFor(model => model.base_id, new { @class = "baseid dont-process" })%>
  <% string TRN = "";
     if (Model.TRN == "10000000") TRN = "Not Available"; else TRN = Model.TRN;
     %>
    <div class="insured-check item select" >
        <%: Html.CheckBox("insuredcheck", true, new { @class = "insuredcheck", id = Model.PersonID.ToString(), @OnClick="changeCheckBox(this)" })%>
    </div>
    <div class="insured-trn item trn" >
        <%: TRN %>
    </div>
    <div class="insured-fname item fname" >
        <%: Model.fname%> 
    </div>
    <div class="insured-lname item lname" >
        <%: Model.lname%>
    </div>
</div>

