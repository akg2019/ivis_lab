﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Honeysuckle.Models;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Converters;
using System.Text;

namespace Honeysuckle.Controllers
{
    public class cover_searchController : Controller
    {
        [HttpPost]
        public ActionResult Index(string id = null)
        {
            Import import = new Import();
            JSON json = new JSON();
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            json.json = new StreamReader(req).ReadToEnd();

            try
            {
                switch (import.ParseJSONAuth(json, "coversearch"))
                {
                    case "loggedin":
                        return Json(new { success = true, key = RSA.Encrypt(UserModel.GetSessionGUID(Constants.user), "session"), kid = Constants.rsa.id, confirmation_code = "yes", comment = "logged in successfully" }, JsonRequestBehavior.DenyGet);
                    case "covers":
                        return Json(new { cover_notes = import.JsonToCoverNotes(json), certificates = import.JsonToCerts(json) }, JsonRequestBehavior.DenyGet);
                    case "bad login":
                        return Json(new { success = false, confirmation_code = "yes", comment = "failed authentication" }, JsonRequestBehavior.DenyGet); // for when session key is bad
                    default:
                        return Json(new { success = false, confirmation_code = "yes", comment = "something went wrong, please contact an administrator." }, JsonRequestBehavior.DenyGet); // for when session key is bad
                }
            }
            catch
            {
                return Json(new { success = false, comment = "Something wrong occurred, please contact an administrator." }, JsonRequestBehavior.DenyGet);
            }
        }
    }
}
