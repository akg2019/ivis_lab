﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using System.Globalization;
using PdfSharp.Pdf;
using System.Web.Script.Serialization;

namespace Honeysuckle.Controllers
{
    public class VehicleCertificateController : Controller
    {

        /// <summary>
        /// This function displays the index page for vehicle certificates
        /// </summary>
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }


        /// <summary>
        /// This function returns all the details about a particular vehicle's certificate
        /// </summary>
        /// <param name="id">Vehicle Certificate Id</param>
        public ActionResult Details(int id = 0)
        {
           if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Read")) && VehicleCertificateModel.IsCertificateReal(id)) {
                VehicleCertificate vc = new VehicleCertificate();
                vc.VehicleCertificateID = id;
                vc = VehicleCertificateModel.GetThisCertificate(vc);

                if (vc.policy != null && vc.policy.ID != 0) {
                    if (PolicyModel.CanIViewPolicy(vc.policy.ID) || UserGroupModel.AmAnAdministrator(Constants.user)) {
                        ViewData["multipleRisk"] = PolicyModel.HasMultipleVehicle(vc.policy.ID); //checks if the vehicle is under a policy that has more than one Risk
                        return View(vc);
                    }
                    else return RedirectToAction("Index", "Home");
                }
                else return RedirectToAction("Index", "Home");
            }
            else return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// This function is used to retrieve and display a vehicleCertificate's information for editing
        /// </summary>
        /// <param name="id">A valid vehicle certificate ID</param>
        public ActionResult Edit (int id=0)
        {
            VehicleCertificate vc = new VehicleCertificate();
            vc.VehicleCertificateID = id;

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Update")) && id != 0 && VehicleCertificateModel.IsCertificateReal(id)) {
                vc = VehicleCertificateModel.GetThisCertificate(vc);
                if (vc.policy != null && vc.policy.ID != 0) {
                    if (PolicyModel.CanIViewPolicy(vc.policy.ID) || UserGroupModel.AmAnAdministrator(Constants.user)) {
                        //Testing if the certificate is active- if not, editing is prohibited (only can be viewed via 'details')
                        if (VehicleCertificateModel.IsCertitificateCancelled(id)) {
                            TempData["CertInactive"] = "The certificate you have chosen is cancelled, and can not be edited. Please select 'details' to view the policy instead.";
                            return RedirectToAction("Index", "Home");
                        }
                        ViewData["multipleRisk"] = PolicyModel.HasMultipleVehicle(vc.policy.ID); //Checks if the vehicle is under a policy that has more than one Risk
                        if (vc.policy.imported || VehicleCertificateModel.IsCertitificateActive(id) || vc.ExpiryDate <= DateTime.Now) return RedirectToAction("Details", "VehicleCertificate", new { id = id });
                        else return View(vc);
                    }
                    else return RedirectToAction("AccessDenied", "Error");
                }
                else return RedirectToAction("AccessDenied", "Error");
            }
            else return RedirectToAction("AccessDenied", "Error");
        }


        /// <summary>
        /// This function is used to edit a vehicle certificate- postback
        /// </summary>
        [HttpPost]
        public ActionResult Edit(VehicleCertificate vehicleCertificate, IEnumerable<Policy> policy = null, IEnumerable<Company> company = null, int wording = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Update")) && (PolicyModel.CanIViewPolicy(vehicleCertificate.policy.ID) || UserGroupModel.AmAnAdministrator(Constants.user))) {

                TemplateWording templateWording = new TemplateWording();
                
                ModelState.Remove("EffectiveDate");
                ModelState.Remove("Company.IAJID");  
                ModelState.Remove("ExpiryDate");
                ModelState.Remove("wording");
                ModelState.Remove("wording.ID");
                ModelState.Remove("policy.Policycover.ID");
               
                ViewData["multipleRisk"] = PolicyModel.HasMultipleVehicle(vehicleCertificate.policy.ID); //Checks if the vehicle is under a policy that has more than one Risk

                vehicleCertificate.company = company.ToList()[0];
                vehicleCertificate.policy = policy.ToList()[0];
                vehicleCertificate.wording = new TemplateWording(wording);

                if (!VehicleModel.IsVehicleUnderPolTimeFrame(vehicleCertificate.Risk.ID)) {
                    ViewData["message"] = "This Vehicle is not under a valid policy within the time frame entered. Please try again!";
                    return View(vehicleCertificate);
                }
                if (ModelState.IsValid) {
                    VehicleCertificateModel.EditCertificate(vehicleCertificate);
                    TempData["EmailSent"] = "Vehicle Certicate Edited!";
                    return RedirectToAction("Index", "Home");
                }
                else return View(vehicleCertificate);
            }
            else return RedirectToAction("Index", "Home"); 
        }


        /// <summary>
        /// This function is used to cancel a vehicle certificate
        /// </summary>
        /// <param name="id">Vehicle Certificate Id</param>
        public ActionResult Cancel(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Approve")) && VehicleCertificateModel.IsCertificateReal(id)) {
                //testing if the certificate is cancelled previously- if not, cancelling is redundant //put this clause instead in view (to not show it)
                if (VehicleCertificateModel.IsCertitificateCancelled(id)) return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
                else {
                    VehicleCertificateModel.CancelCertificate(id);
                    return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                }
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Tests if a Printed Paper Number is already in the system
        /// </summary>
        /// <param name="id">Printed Paper Number to be tested</param>
        /// <returns>boolean value</returns>
        public ActionResult TestPrintedPaperNo(int id = 0)
        {
            if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Update"))|| UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Create"))))
            {
                if (VehicleCertificateModel.TestPrintedPaperNo(id)) return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else { TempData["CertInactive"] = "You do not have the required permissions to complete this request"; return RedirectToAction("Index"); }
        }


        /// <summary>
        /// This function is used to approve a vehicle certificate
        /// </summary>
        /// <param name="certificateId">Vehicle Certificate Id</param>
        public ActionResult ApproveCert(int certificateId = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Approve")) && VehicleCertificateModel.IsCertificateReal(certificateId))
            {
                VehicleCertificateModel.ApproveCert(certificateId);

                return Json(new { result = 1, certId = certificateId }, JsonRequestBehavior.AllowGet);
            }
            else 
            {
                return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// This function is used to create a pdf for a vehicle certificate -and update a vehicle certificate's print information 
        /// </summary>
        /// <param name="id">Vehicle Certificate ID</param>
        public ActionResult PrintCertificate(string certs= null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Print")) && certs != null) {
                List<VehicleCertificate> Vehcerts = new List<VehicleCertificate>();
                TemplateWording Wording = new TemplateWording();
                VehicleCoverNoteGenerated VehCertificateGenerated = new VehicleCoverNoteGenerated();
                List<VehicleCoverNoteGenerated> generatedCertList = new List<VehicleCoverNoteGenerated>();

                // THIS PART IS ADDED FOR WHEN THERE ARE MORE THAN ONE CERTS BEING SENT TO BE PRINTED (MASS PRINT)
                int i = 0;
                foreach (string s in certs.Split(',').ToList())  {
                    if (int.TryParse(s, out i)) {
                        if (VehicleCertificateModel.IsCertificateReal(i)) Vehcerts.Add(VehicleCertificateModel.GetThisCertificate(new VehicleCertificate(i)));
                        else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
                    }
                }

                foreach (VehicleCertificate vc in Vehcerts) {
                    //Pulling the image locations for the images to be added to the page
                    List<TemplateImage> images = TemplateImageModel.GetCompanyTemplateImages(vc.company);

                    if (VehicleCoverNoteGeneratedModel.IsGenerated(vc.VehicleCertificateID, "Certificate")) generatedCertList.Add(VehicleCoverNoteGeneratedModel.GetGenCertificate(vc.VehicleCertificateID));
                    else {
                        vc.policy = PolicyModel.getPolicyFromID(vc.policy.ID);
                        vc.Risk.AuthorizedDrivers = VehicleModel.GetDriverList(vc.Risk.ID, vc.policy.ID, 1);

                        Wording = TemplateWordingModel.GetWording(vc.wording.ID);

                        VehCertificateGenerated = TemplateWordingModel.ReplaceCertificateTags(Wording, vc);
                        
                        VehCertificateGenerated.HeaderImgLocation = TemplateImageModel.get_image_location(vc.policy.insuredby, "header", "certificate");
                        VehCertificateGenerated.FooterImgLocation = TemplateImageModel.get_image_location(vc.policy.insuredby, "footer", "certificate");
                        VehCertificateGenerated.LogoLocation = TemplateImageModel.get_image_location(vc.policy.insuredby, "logo", "certificate");
                        
                        generatedCertList.Add(VehCertificateGenerated);
                        VehicleCoverNoteGeneratedModel.GenerateVehicleCert(VehCertificateGenerated, vc.VehicleCertificateID);
                    }

                    //Testing if the certificate has been cancelled
                    generatedCertList[generatedCertList.Count-1].cancelled = vc.Cancelled;

                    // Adding the image locationg to the generated cert model
                    for (int x = 0; x < images.Count(); x++) {
                        if (images[x].type == "Certificate") {
                            switch (images[x].position) {
                                case "header":
                                    generatedCertList[generatedCertList.Count-1].HeaderImgLocation = Server.MapPath("/files/" + images[x].location);
                                    break;
                                case "footer":
                                    generatedCertList[generatedCertList.Count-1].FooterImgLocation = Server.MapPath("/files/" + images[x].location);
                                    break;
                                case "logo":
                                    generatedCertList[generatedCertList.Count-1].LogoLocation = Server.MapPath("/files/" + images[x].location);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }

                Constants.document = new PdfDocument();

                if (Vehcerts != null && Vehcerts.Count != 0) Constants.document = PDF.PrintCertToPDF(generatedCertList, Vehcerts[0].company.CompanyID); //If it's mass creation , then all certs will have the same comp Ids, so just send the first one

                if (Constants.document.PageCount == 0) {
                    if (Constants.document.Tag == "NO CODE") {
                        Constants.document = new PdfDocument();
                        return Json(new { result = 0, nocode = 1 }, JsonRequestBehavior.AllowGet);
                    }
                    else return Json(new { result = 0, nocode = 0 }, JsonRequestBehavior.AllowGet);
                }
                else {
                    //Updating the print count of the certificate
                    foreach (VehicleCertificate vc in Vehcerts) VehicleCertificateModel.Print(vc.VehicleCertificateID);
                    return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                }
            }
            else {
                TempData["CertInactive"] = "You do not have the required permissions to complete this request";
                return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// This function is used to pull all emails of the company admins of a particular company (which has a particular vehicle certificate of a vehicle)
        /// </summary>
        /// <param name="id">VehicleCertificate Id</param>
        public ActionResult CompanyAdmins(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Read")))
            {
                List<string> Admins = new List<string>();
                Admins = PolicyModel.getAdmins(id, "Certificate");
                return Json(new { result = 1, admins = Admins }, JsonRequestBehavior.AllowGet);
            }

            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);

        }


        /// <summary>
        /// //pull all emails for brokers
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CompanyBrokers(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Read")))
            {
                List<List<string>> Admins = new List<List<string>>();
                Admins = PolicyModel.getBrokers(id, "Certificate");
                return Json(new { result = 1, admins = Admins }, JsonRequestBehavior.AllowGet);
            }

            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);

        }


        public ActionResult PrintRequest(int id = 0)
        {
            if (VehicleCertificateModel.IsCertitificateActive(id))
            {
                VehicleCertificateModel.PrintRequest(id);
                return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This function is used to enable the printing for a particular vehicle certificate
        /// </summary>
        /// <param name="id">VehicleCertificate Id</param>
        public ActionResult EnablePrinting(int id = 0)
        {
            if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Approve")) && (VehicleCertificateModel.IsCertitificateActive(id)))) 
            {

                bool allow = false;

                if (Constants.user.newRoleMode) allow = CompanyModel.IsCompanyInsurer(Constants.user.tempUserGroup.company.CompanyID);
                else allow = CompanyModel.IsCompanyInsurer(UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID);
           
                if (allow)
                {
                    if (VehicleCertificateModel.EnablePrint(id)) return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                    else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData["EmailSent"] = "You do not have the required permissions to complete this request"; 
                    return RedirectToAction("Index", "Home");
                }
            } //End of permissions and certificate tests
            else
            {
                TempData["EmailSent"] = "You do not have the required permissions to complete this request"; 
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Create(int id = 0, int polid = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Create")))
            {
                if (id == 0)
                {
                    ViewData["vehId"] = id;
                    ViewData["CleanCreate"] = true;
                    return View(new VehicleCertificate());
                }
                else
                {
                    bool error = false;

                    ViewData["CleanCreate"] = false;
                    ViewData["type"] = "CertCreate";

                    VehicleCertificate vc = new VehicleCertificate();
                    if (polid == 0)
                    {
                        vc = VehicleCertificateModel.PullVehiclePolData(id);
                        if (vc.policy != null) vc.policy = PolicyModel.getPolicyFromID(vc.policy.ID);
                        else error = true;
                    }
                    else
                    {
                        vc.policy = PolicyModel.getPolicyFromID(polid);
                        vc.Risk = VehicleModel.GetVehicle(id);
                    }

                    if (!error)
                    {
                        List<TemplateWording> words = TemplateWordingModel.GetWordingBasedOnFilter(id, vc.policy.ID);

                        ViewData["vehId"] = id;

                        ViewData["type"] = "CertCreate";
                        vc.Risk.usage = new Usage(VehicleModel.GetUsage(vc.Risk.ID, vc.policy.ID));
                        return View(vc);
                    }
                    else return RedirectToAction("AccessDenied", "Error");
                    #region old
                    /*
                    switch (words.Count)
                    {
                        case 1:
                            vc.wording = words[0]; 
                            int certId = VehicleCertificateModel.CreateCertificate(vc, Constants.user.ID);
                            return RedirectToAction("Details", "VehicleCertificate", new { id = certId });
                        case 0:
                            vc.Risk.usage = new Usage(VehicleModel.GetUsage(vc.Risk.ID, vc.policy.ID));
                            return View(vc);    
                        default:
                            ViewData["vehId"] = id;

                            ViewData["type"] = "CertCreate";
                            vc.Risk.usage = new Usage(VehicleModel.GetUsage(vc.Risk.ID, vc.policy.ID));
                            return View(vc);
                    }
                    */
                    #endregion
                }
            }
            else return RedirectToAction("AccessDenied", "Error");
        }


        [HttpPost]
        public ActionResult Create(VehicleCertificate vehicleCertificate,  int wording = 0, IEnumerable<Vehicle> vehicles = null, string proceed="")
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Create"))) 
            {
                VehicleCertificate vc = new VehicleCertificate();
                TemplateWording templateWording = new TemplateWording ();
                int certId = 0;

                ViewData["vehId"] = vehicles;
                ViewData["CleanCreate"] = true;
                
                ModelState.Remove("policy");
                ModelState.Remove("Risk.ID");
                ModelState.Remove("Company.CompanyID");
                ModelState.Remove("Policy.ID");
                ModelState.Remove("Company.IAJID");
                ModelState.Remove("wording.ID");
                ModelState.Remove("wording");
                ModelState.Remove("policy.policyCover.ID");

                vc = VehicleCertificateModel.PullVehiclePolData(vehicleCertificate.Risk.ID, CompanyModel.GetCompanyFromPolicy(vehicleCertificate.policy.ID));
                vc.PrintedPaperNo = vehicleCertificate.PrintedPaperNo;
                //vc.EffectiveDate = vehicleCertificate.EffectiveDate;

                templateWording.ID = wording;
                vc.wording = templateWording;
                vc.policy = PolicyModel.getPolicyFromID(vehicleCertificate.policy.ID);
                vc.EffectiveDate = vc.policy.startDateTime;

                if (vehicles != null) vc.Risk = vehicles.ToArray()[0];
                else
                {
                    ViewData["Message"] = "A risk must be added to create a certificate!";
                    return View(vc);
                }

                if (wording == 0)
                {
                    ViewData["Message"] = "A wording template must be selected!";
                    ViewData["type"] = "CertCreateClean";
                    return View(vc);
                }
                else
                {
                    templateWording.ID = wording;
                    vehicleCertificate.wording = templateWording;
                }

                if (!CompanyModel.IsCompanyShortSet(new Company(vc.policy.insuredby.CompanyID)))
                {
                    ViewData["veherror"] = "The insurance company, for the policy that has this vehicle, does not have a company Shortening code. Please contact your admin for assistance!";
                    ViewData["type"] = "CertCreateClean";
                    return View(vc);
                }

              
                if (ModelState.IsValid)
                {
                    //Testing if a cert already exists for the vehicle during the selected time -only if the user has not yet chosen to proceed
                    if(proceed == "")
                    if (VehicleCertificateModel.IsVehicleUnderActiveCert(vc.Risk.ID, vc.EffectiveDate, vc.policy.endDateTime))
                    {
                        ViewData["CertExistsAlert"] = "The seleced risk already has an active certificate during this time period. Would you like to proceed?";
                        return View(vc);
                    }

                    if (VehicleModel.IsVehicleUnderPolTimeFrame(vc.Risk.ID))
                    {
                        certId = VehicleCertificateModel.CreateCertificate(vc, Constants.user.ID);
                        if (certId == 0)
                        {
                            ViewData["message"] = "This Certificate that you are trying to create, is already existent";
                            return View(vc);
                        }
                        else
                        {

                            if (UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "AutoApprove")))
                            {
                                VehicleCertificateModel.ApproveCert(certId);
                            }

                            return RedirectToAction("Details", "VehicleCertificate", new { id = certId });
                        }
                    }
                    else
                    {
                        ViewData["message"] = "This Vehicle is not under a valid policy within the time frame entered. Please try again!";
                        return View(vc);
                    }
                }
                else return View(vc);
            }
            else return RedirectToAction("AccessDenied", "Error");
        }


        /// <summary>
        /// This function pulls the policy associated with a vehicle (given a certain timeframe)
        /// </summary>
        /// <param name="id">Vehicle's id </param>
        public ActionResult GetPolicy(int vehId = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Update"))) //&& vehId != 0 && startDate != null && endDate != null)
            {
                if (VehicleModel.IsVehicleUnderPolTimeFrame(vehId))
                {
                    Policy pol = new Policy();

                    //Get Policy info- if the vehicle is under a policy at that time
                    pol = PolicyModel.GetVehiclePolicy(vehId);
                    return Json(new { result = 1, policyId= pol.ID}, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);

            }
            else return Json(new { result = 0}, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function retrieves the vehicle certificate of the vehicle
        /// </summary>
        //public ActionResult GetActiveCert(int id =0)
        //{
        //    if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Read")))
        //    {
        //        VehicleCertificate vc = new VehicleCertificate ();
        //        vc = VehicleCertificateModel.GetThisVehicleActiveCertificate(id);
        //        return RedirectToAction("Details", "VehicleCertificate", new { id = vc.VehicleCertificateID });
        //    }
        //    return RedirectToAction("Index", "Home");
        //}


        /// <summary>
        /// This function retrieves the active vehicle certificate of a particular vehicle , under a given time frame
        /// </summary>
        public ActionResult GetThisActiveCert(int id = 0, string start = null, string end = null, int policyid = 0 )
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Read")))
            {
                VehicleCertificate vc = new VehicleCertificate();
                vc = VehicleCertificateModel.GetThisActiveCertificate(id, start, end, policyid);
                return RedirectToAction("Details", "VehicleCertificate", new { id = vc.VehicleCertificateID });
            }
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// This function retrieves the inactive vehicle certificate of the vehicle (under a given policy)
        /// </summary>
        public ActionResult GetThisInactiveCert(int vehID = 0, int polID= 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Read")))
            {
                VehicleCertificate vc = new VehicleCertificate();
                vc = VehicleCertificateModel.GetThisInactiveCertificate(vehID, polID);
                return RedirectToAction("Details", "VehicleCertificate", new { id = vc.VehicleCertificateID });
            }
            return RedirectToAction("Index", "Home");
        }



        /// <summary>
        /// This function creates all the vehicle certifcates for risks under a given policy
        /// </summary>
        /// <param name="id">Vehicle's id </param>
        public ActionResult CreateAllCerts(List<string> vehiclesList = null, List<string> templatewordings = null, int polId = 0, List<DateTime> EffectiveDates = null, string proceed="")
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Create"))) 
            {
                List<VehicleCertificate> vehicleCertiticates = new List<VehicleCertificate>();
                string thisMessage =  "";
                List<int> CertiticatesIDs = new List<int>();

                bool autoApp = false; bool canPrint = false; bool canApprove = false;

                if (vehiclesList != null)
                {

                    Policy Policy = new Policy();
                    Policy = PolicyModel.getPolicyFromID(polId);

                    if (!CompanyModel.IsCompanyShortSet(Policy.insuredby))
                        return Json(new { compShortening = 1, result = 0, wordingError = 0 }, JsonRequestBehavior.AllowGet);

                    templatewordings.ToArray();
                    //EffectiveDates.ToArray();

                    for (int i = 0; i < vehiclesList.Count; i++)
                    {
                        if ((templatewordings == null ) || (templatewordings[i] == "" 
                            //|| EffectiveDates[i].ToString() == ""
                            )
                            )
                        {
                            return Json(new { wordingError = 1, result = 0, compShortening = 0 }, JsonRequestBehavior.AllowGet);
                        }
                        //vehicleCertiticates.Add (VehicleCertificateModel.PullVehiclePolData(int.Parse(vehiclesList[i])));
                        vehicleCertiticates.Add(new VehicleCertificate(Policy));
                        vehicleCertiticates[i].Risk = new Vehicle (int.Parse(vehiclesList[i]));
                        vehicleCertiticates[i].wording = new TemplateWording(int.Parse(templatewordings[i]));
                        //vehicleCertiticates[i].EffectiveDate = EffectiveDates[i];
                        vehicleCertiticates[i].company = Policy.insuredby;

                        //Testing if a previous certificate exits for the vehicle during the time frame
                        if(proceed=="")
                        {
                            if (VehicleCertificateModel.IsVehicleUnderActiveCert(vehicleCertiticates[i].Risk.ID, vehicleCertiticates[i].policy.startDateTime , vehicleCertiticates[i].policy.endDateTime))
                            {
                                Vehicle Thisveh = VehicleModel.GetVehicle(vehicleCertiticates[i].Risk.ID);
                                thisMessage = thisMessage + "The vehicle : " + Thisveh.VehicleYear + " " + Thisveh.make + " " + Thisveh.VehicleModel + "has an existing, active certificate during the time frame selected. " ;
                            }
                        }

                    }

                    //If previous certs exists, then alert the user
                    if (thisMessage !="")
                    {
                        return Json(new { wordingError = 0, result = 0, compShortening = 0, existingCert = 1, CertMessage = thisMessage }, JsonRequestBehavior.AllowGet);
                    }

                    autoApp = UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "AutoApprove"));
                    canPrint = UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Print"));
                    canApprove = UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Approve"));

                    //Creating all the certificates (once all the data is entered)
                    for (int vc = 0; vc < vehicleCertiticates.Count; vc++)
                    {
                        int certID = VehicleCertificateModel.CreateCertificate(vehicleCertiticates[vc], Constants.user.ID);
                        if (certID!= 0) CertiticatesIDs.Add(certID);

                        if (autoApp && certID!=0) //Approving the certificate once the cert has been successfully made, and the user has the auto-approve permission
                        {
                            VehicleCertificateModel.ApproveCert(certID);
                        }
                    }
                       JavaScriptSerializer jsonList = new JavaScriptSerializer();
                       string vehCerts = jsonList.Serialize(vehicleCertiticates);

                       return Json(new { wordingError = 0, result = 1, compShortening = 0, existingCert = 0, autoApp = autoApp, canPrint = canPrint, canApprove = canApprove, certs = CertiticatesIDs }, JsonRequestBehavior.AllowGet);
                  }

                else return Json(new { result = 0, wordingError = 0, compShortening = 0, existingCert = 0}, JsonRequestBehavior.AllowGet);

            }
            else return Json(new { result = 0, wordingError = 0, compShortening = 0, existingCert =0}, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function allows for mass approval of a number of certificates
        /// </summary>
        /// <param name="id">Cert id </param>
        public ActionResult MassApproval(string certs = null)
        {
              List<VehicleCertificate> Vehcerts = new List<VehicleCertificate>();
              List<int> policies = new List<int>();
              int i = 0;

              foreach (string s in certs.Split(',').ToList())
                {
                    if (int.TryParse(s, out i))
                   {
                       Vehcerts.Add(VehicleCertificateModel.GetThisCertificate(new VehicleCertificate(i)));
 
                   }
               }

            return View(Vehcerts);
        }


        /// <summary>
        /// This function allows for mass printing of a number of certificates
        /// </summary>
        /// <param name="id">Cert id </param>
        public ActionResult MassPrint(string certs = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Print")))
            {
                List<VehicleCertificate> Vehcerts = new List<VehicleCertificate>();
                List<int> policies = new List<int>();
                int i = 0;

                foreach (string s in certs.Split(',').ToList())
                {
                    if (int.TryParse(s, out i))
                    {
                        Vehcerts.Add(VehicleCertificateModel.GetThisCertificate(new VehicleCertificate(i)));

                    }
                }

                return View(Vehcerts);
            }
            else return new EmptyResult();
        }


        public ActionResult GetCertsForApproval(int id=0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Approve")) && id != 0 )
            {
                Policy policy = PolicyModel.getPolicyFromID(id);
                List<int> certs = new List<int>();
                bool canPrint = UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Print")); 

                foreach (Vehicle vehicle in policy.vehicles)
                {
                    if (!VehicleCertificateModel.IsVehicleUnderActiveCert(vehicle.ID, policy.startDateTime, policy.endDateTime, policy.ID) && VehicleCertificateModel.IsPolicyCertMade(vehicle.ID, policy.ID))
                    {
                      int certId = VehicleCertificateModel.GetThisInactiveCertificate(vehicle.ID, policy.ID).VehicleCertificateID;
                      if (certId!= 0) certs.Add(certId);
                    }
                }

                return Json(new { result = 1, certs = certs, canPrint= canPrint }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0}, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetCertsForPrinting(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Print")) && id != 0)
            {
                Policy policy = PolicyModel.getPolicyFromID(id);
                List<int> certs = new List<int>();

                foreach (Vehicle vehicle in policy.vehicles)
                {
                    if (VehicleCertificateModel.IsVehicleUnderActiveCert(vehicle.ID, policy.startDateTime, policy.endDateTime, policy.ID))
                    {
                        int certId = VehicleCertificateModel.GetThisActiveCertificate(vehicle.ID, policy.startDateTime.ToString(), policy.endDateTime.ToString(), policy.ID).VehicleCertificateID;
                        if (certId!= 0) certs.Add(certId);
                    }
                }

                return Json(new { result = 1, certs = certs}, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

    }
}
