﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class ServiceController : Controller
    {
        public ActionResult vehicle_search(string type, string value)
        {
            Vehicle vehicle = new Vehicle ();
            bool IsInsured = false;

            switch(type){
                case "chs":
                    vehicle = VehicleModel.GetVehicleByChassis(value);
                    IsInsured = PolicyModel.IsVehicleCovered(vehicle.chassisno, vehicle.VehicleRegNumber);
                    break;
                case "lcn":
                    vehicle = VehicleModel.GetVehicleByLicenseNo(value);
                    IsInsured = PolicyModel.IsVehicleCovered(vehicle.chassisno, vehicle.VehicleRegNumber);
                    break;
                default:
                    break;
            }
            return Json(new { chassis_no = vehicle.chassisno, license_no = vehicle.VehicleRegNumber, insurance_status = IsInsured.ToString() }, JsonRequestBehavior.AllowGet);
        } 

    }
}
