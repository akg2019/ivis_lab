﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class AuthorizedDriverWordingController : Controller
    {
        //
        // GET: /AuthorizedDriverWording/

        public ActionResult Index()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Authorized", "Read")))
            {
                int id = CompanyModel.GetMyCompany(Constants.user).CompanyID;
                if (CompanyModel.IsCompanyInsurer(id))
                {
                    ViewData["company_id"] = id;
                    List<AuthorizedDriverWording> adws = AuthorizedDriverWordingModel.get_all_wording_for_insurer(new Company(id));
                    return View(adws);
                }
                else return RedirectToAction("Index", "Home");
            }
            else return RedirectToAction("Index", "Home");
        }

        public ActionResult New()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Authorized", "Create"))) return View();
            else return new EmptyResult();
        }

        [HttpPost]
        public ActionResult Create(string wording = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Authorized", "Create")))
            {
                int id = CompanyModel.GetMyCompany(Constants.user).CompanyID;
                if (wording != null)
                {
                    AuthorizedDriverWording adw = new AuthorizedDriverWording();
                    adw.company = new Company(id);
                    adw.wording = wording;
                    adw = AuthorizedDriverWordingModel.create_wording(adw);
                    return Json(new { result = 1, word_id = adw.ID }, JsonRequestBehavior.DenyGet);
                }
                else return Json(new { result = 0 }, JsonRequestBehavior.DenyGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.DenyGet);
        }

        public ActionResult AuthorizedDriverWordingDropDown(int id = 0)
        {
            if (id != 0)
            {
                List<AuthorizedDriverWording> adws = new List<AuthorizedDriverWording>();
                adws.Add(new AuthorizedDriverWording(0,""));
                adws.AddRange(AuthorizedDriverWordingModel.get_all_wording_for_insurer(new Company(id)));
               
                return View(adws);
            }
            else return new EmptyResult();
        }

        public ActionResult Delete(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Authorized", "Delete")) && AuthorizedDriverWordingModel.get_wording(id).can_delete) 
                return Json(new { result = AuthorizedDriverWordingModel.delete(new AuthorizedDriverWording(id)) }, JsonRequestBehavior.AllowGet);
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateRiskADWording(int adwID = 0, int polID = 0, int vehID = 0)
        {
            if (polID != 0 && vehID != 0)
            {
                AuthorizedDriverWordingModel.updateRiskADWording(adwID, polID, vehID);
                return new EmptyResult();
            }
            else return new EmptyResult();
        }

        public ActionResult UpdateAuthorizedWording(int ID = 0, string wording = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Authorized", "Update")) && ID != 0 && wording != null)
            {
                AuthorizedDriverWordingModel.updateAuthorizedWording(ID, wording);
                return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

    }
}
