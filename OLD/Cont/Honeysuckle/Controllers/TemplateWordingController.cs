﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using System.Web.Script.Serialization;
using System.Drawing;
using System.IO;

namespace Honeysuckle.Controllers
{
    public class TemplateWordingController : Controller
    {

        /// <summary>
        /// This function returns the index page for template wordings
        /// </summary>
        public ActionResult Index()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Read")) && UserModel.TestGUID(Constants.user))
            {
                bool allow = false;

                if (Constants.user.newRoleMode) allow = (CompanyModel.IsCompanyInsurer(Constants.user.tempUserGroup.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));
                else allow = (CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));

                if (allow)
                {
                    if (TempData["delete"] != null)
                    {
                        ViewData["delete"] = TempData["delete"];
                        TempData["delete"] = null;
                    }


                    List<TemplateWording> tw = new List<TemplateWording>();
                    tw = TemplateWordingModel.GetWordings();
                    int i, j;
                    TemplateWording key = new TemplateWording();
                    for (i = 1; i < tw.Count(); i++)
                    {
                        key = tw[i];
                        j = i - 1;


                        while (j >= 0 && tw[j].CertificateType.CompareTo(key.CertificateType) > 0)
                        {
                            tw[j + 1] = tw[j];
                            j = j - 1;
                        }
                        tw[j + 1] = key;
                    }

                    return View(tw);
                }

                else return RedirectToAction("AccessDenied", "Error");
            }
            else return RedirectToAction("AccessDenied", "Error");
        }


        /// <summary>
        /// This function creates template wording
        /// </summary>
        public ActionResult Create()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Create")) && UserModel.TestGUID(Constants.user))
            {
                bool allow = false;

                if (Constants.user.newRoleMode)
                {
                    allow = (CompanyModel.IsCompanyInsurer(Constants.user.tempUserGroup.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));
                }
                else
                    allow = (CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));

                if (allow)
                {
                    TemplateWording word = new TemplateWording();

                    if (Constants.user.newRoleMode) word.CertificateInsuredCode = CompanyModel.GetInsurerCode(Constants.user.tempUserGroup.company.CompanyID);
                    else word.CertificateInsuredCode = CompanyModel.GetInsurerCode(CompanyModel.GetMyCompany(Constants.user).CompanyID);

                    return View(word);
                }
                else return RedirectToAction("AccessDenied", "Error");
            }
            else return RedirectToAction("AccessDenied", "Error");
        }


        /// <summary>
        /// This function creates template wording -postback
        /// </summary>
        [HttpPost]
        public ActionResult Create(TemplateWording wording, string inscode = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Create")) && UserModel.TestGUID(Constants.user))
            {
                bool allow = false;

                if (Constants.user.newRoleMode)
                {
                    allow = (CompanyModel.IsCompanyInsurer(Constants.user.tempUserGroup.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));
                }
                else
                    allow = (CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));

                if (allow)
                {

                    if (ModelState.IsValid && inscode != null)
                    {
                        wording.CertificateInsuredCode = inscode;
                        if (TemplateWordingModel.AddWording(wording, false).Item1) return RedirectToAction("Index");
                        else return View(wording);
                    }
                    else return View(wording);
                }

                else return RedirectToAction("AccessDenied", "Error");
            }
            else return RedirectToAction("AccessDenied", "Error");
        }


        /// <summary>
        /// This function edits template wording
        /// </summary>
        /// <param name="id">Template id</param>
        public ActionResult Edit(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Update")) && UserModel.TestGUID(Constants.user) && (TemplateWordingModel.CanIViewWording(new TemplateWording(id)) || UserGroupModel.AmAnAdministrator(Constants.user)))
            {
                bool allow = false;

                if (Constants.user.newRoleMode) allow = (CompanyModel.IsCompanyInsurer(Constants.user.tempUserGroup.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));
                else allow = (CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));

                if (allow)
                {
                    if (id != 0 && TemplateWordingModel.WordingExist(id)) return View(TemplateWordingModel.GetWording(id));
                    else return RedirectToAction("Index");
                }
                else return RedirectToAction("AccessDenied", "Error");
            }
            else return RedirectToAction("AccessDenied", "Error");
        }


       /// <summary>
       /// This function edits template wording -postback
       /// </summary>
       /// <param name="word">Template Wording object</param>
        [HttpPost]
        public ActionResult Edit(TemplateWording word, string inscode = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Update")) && UserModel.TestGUID(Constants.user) && (TemplateWordingModel.CanIViewWording(word) || UserGroupModel.AmAnAdministrator(Constants.user)))
            {
                bool allow = false;

                if (Constants.user.newRoleMode)
                {
                    allow = (CompanyModel.IsCompanyInsurer(Constants.user.tempUserGroup.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));
                }
                else
                    allow = (CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));

                if (allow)
                {

                    if (ModelState.IsValid && inscode != null)
                    {
                        word.CertificateInsuredCode = inscode;
                        TemplateWordingModel.UpdateWording(word);
                        return RedirectToAction("Index");
                    }
                    else return View(word);
                }

                else return RedirectToAction("AccessDenied", "Error");
            }
            else return RedirectToAction("AccessDenied", "Error");
        }


        /// <summary>
        /// This function returs the details of a template wording
        /// </summary>
        /// <param name="id">Template wording id</param>
        public ActionResult Details(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Read")) && UserModel.TestGUID(Constants.user) && (TemplateWordingModel.CanIViewWording(new TemplateWording(id)) || UserGroupModel.AmAnAdministrator(Constants.user)))
            {
                if (id != 0 && TemplateWordingModel.WordingExist(id)) return View(TemplateWordingModel.GetWording(id));
                else return RedirectToAction("Index");
            }
            else return RedirectToAction("AccessDenied", "Error");
        }


        /// <summary>
        /// This function renders the email partial- to add a new email address to a person
        /// </summary>
        /// <param name="id">Template wording id</param>
        public ActionResult Delete(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Delete")) && UserModel.TestGUID(Constants.user) && (TemplateWordingModel.CanIViewWording(new TemplateWording(id)) || UserGroupModel.AmAnAdministrator(Constants.user)))
            {
                bool allow = false;

                if (Constants.user.newRoleMode)
                {
                    allow = (CompanyModel.IsCompanyInsurer(Constants.user.tempUserGroup.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));
                }
                else
                    allow = (CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));

                if (allow)
                {
                    if (id != 0 && TemplateWordingModel.WordingExist(id))
                    {
                        if (TemplateWordingModel.DeleteWording(new TemplateWording(id))) return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                        else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
                    }
                    else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Generates a partial drop down list with all templates
        /// </summary>
        public ActionResult TemplateDropDown(int id = 0, int polId = 0)
        {
            List <UserPermission> permissions = new List<UserPermission> ();
            permissions.Add(new UserPermission("CoverNotes", "Create"));
            permissions.Add(new UserPermission("CoverNotes", "Update"));
            permissions.Add(new UserPermission("Certificates", "Create"));
            permissions.Add(new UserPermission("Certificates", "Update"));
            permissions.Add(new UserPermission("Wording", "Read"));
           

            if (UserModel.TestUserPermissions(Constants.user, permissions))
            {
                if (id == 0) return View(new List<TemplateWording>());
                else return View(TemplateWordingModel.GetWordingBasedOnFilter(id, polId));
            }
            else return View(new List<TemplateWording>()); // return empty if not permitted entry
        }


        /// <summary>
        /// This function deals with the dropdown of template wordings
        /// </summary>
        /// <param name="id">Vehicle ID</param>
        public ActionResult TemplateWordingFilter(int id = 0, int polId = 0)
        {
            if (id != 0)
            {
                 if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Read")))
                 {
                     JavaScriptSerializer jsonList = new JavaScriptSerializer();
                     List<TemplateWording> wording = TemplateWordingModel.GetWordingBasedOnFilter(id, polId);

                     string jsonFile = jsonList.Serialize(wording);
                     return Json(new { result = jsonFile }, JsonRequestBehavior.AllowGet);
                 }
                 else return new EmptyResult();
            }
            else return new EmptyResult();
        }

        public ActionResult Image(int id = 0, string type = null, string position = null, bool post = false)
        {
            TemplateImage template_image = new TemplateImage();
            ViewData["post"] = post;
            if (id != 0)
            {
                template_image = TemplateImageModel.GetTemplateImage(id);
                return View(template_image);
            }
            else
            {
                if (type != null && position != null)
                {
                    template_image.position = position.ToLower();
                    template_image.type = type.ToLower();
                    return View(template_image);
                }
                else return new EmptyResult();
            }
        }

        [HttpPost]
        public ActionResult JSONImageDelete(int id = 0)
        {
            if (id != 0 && UserGroupModel.IsAdministrative(Constants.user))
            {
                TemplateImage template_image = TemplateImageModel.GetTemplateImage(id);
                bool result = TemplateImageModel.DeleteTemplateImage(template_image);

                if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "/files/" + template_image.location) && result) System.IO.File.Delete(System.AppDomain.CurrentDomain.BaseDirectory + "/files/" + template_image.location);
                return Json(new { result = result, position = template_image.position, type = template_image.type }, JsonRequestBehavior.DenyGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.DenyGet);
        }
        
        [HttpPost]
        public ActionResult JSONImageUpload(HttpPostedFileBase file = null, string position = null, string type = null)
        {
            if (UserGroupModel.IsAdministrative(Constants.user))
            {
                if (file != null && position != null && type != null && TemplateImageModel.AcceptableLocation(position) && TemplateImageModel.AcceptableType(type))
                {
                    bool isValid = false;
                    int image_id = 0;
                    try
                    {
                        isValid = (
                                    file.ContentType.ToLower() == "image/jpg" ||
                                    file.ContentType.ToLower() == "image/jpeg" ||   
                                    file.ContentType.ToLower() == "image/pjpeg" ||
                                    file.ContentType.ToLower() == "image/gif" ||
                                    file.ContentType.ToLower() == "image/x-png" ||
                                    file.ContentType.ToLower() == "image/png"
                                  );
                        isValid = isValid && (file.ContentLength < 512000);
                    }
                    catch { }

                    if (isValid)
                    {
                        string companyname = "";
                        companyname = CompanyModel.GetInsurerCode(CompanyModel.GetMyCompany(Constants.user).CompanyID);

                        string filepath = companyname + "/" + type + "/" + position + ".jpg";

                        if (!(Directory.Exists(Server.MapPath("/files/" + companyname + "/")))) Directory.CreateDirectory(Server.MapPath("/files/" + companyname + "/"));
                        if (!(Directory.Exists(Server.MapPath("/files/" + companyname + "/" + type + "/")))) Directory.CreateDirectory(Server.MapPath("/files/" + companyname + "/" + type + "/"));

                        file.SaveAs(Server.MapPath("/files/" + filepath));
                        image_id = TemplateImageModel.AddTemplateImage(new TemplateImage(type, position, filepath, CompanyModel.GetMyCompany(Constants.user)));
                    }
                    else { ViewData["error"] = "Please make sure you are uploading an image and it isn't too large please."; }

                    return Json(new { result = 1, id = image_id }, JsonRequestBehavior.DenyGet);
                }
                return Json(new { result = 0 }, JsonRequestBehavior.DenyGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.DenyGet);
        }
        
        public ActionResult ImageUpload(int id = 0)
        {
            if (UserGroupModel.IsAdministrative(Constants.user))
            {
                ViewData["hiddenid"] = id;
                if (id != 0 && UserGroupModel.AmAnAdministrator(Constants.user) && CompanyModel.IsCompanyInsurer(id)) return View(TemplateImageModel.GetCompanyTemplateImages(new Company(id)));
                else if (CompanyModel.IsCompanyInsurer(CompanyModel.GetMyCompany(Constants.user).CompanyID)) return View(TemplateImageModel.GetCompanyTemplateImages(CompanyModel.GetMyCompany(Constants.user)));
                else return RedirectToAction("AccessDenied", "Error");
            }
            else return RedirectToAction("AccessDenied", "Error");
        }

        public ActionResult WordingToCoverUsages(int id)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Update")) && TemplateWordingModel.WordingExist(id) && (TemplateWordingModel.CanIViewWording(new TemplateWording(id)) || UserGroupModel.AmAnAdministrator(Constants.user)) ) 
            { 
                TemplateWording wording = new TemplateWording ();
                wording = TemplateWordingModel.GetWording(id);

                //Pulling the associated covers and usages for the wording
                wording.PolicyCovers = TemplateWordingModel.getWordingAssociations(id).PolicyCovers;
               
                return View(wording);
            }
            else return RedirectToAction("Index", "Home");
        }

        public ActionResult AddCoverUsages(int wording= 0, int cover = 0, int usage = 0 )
        {
            bool returnVal = false;
            if (wording != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Update")) && TemplateWordingModel.WordingExist(wording) && (TemplateWordingModel.CanIViewWording(new TemplateWording(wording)) || UserGroupModel.AmAnAdministrator(Constants.user))) 
                returnVal = TemplateWordingModel.addWordingCoverUsages(wording, cover, usage);
            return Json(new { result = returnVal }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteCoverUsages(int wording = 0, int cover = 0, int usage = 0)
        {
            bool result = false;
            if (wording != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Update")) && TemplateWordingModel.WordingExist(wording) && (TemplateWordingModel.CanIViewWording(new TemplateWording(wording)) || UserGroupModel.AmAnAdministrator(Constants.user))) 
                result = TemplateWordingModel.removeWordingCoverUsages(wording, cover, usage);
            return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Upload()
        {
            if (UserGroupModel.IsAdministrative(Constants.user) && CompanyModel.IsCompanyInsurer(CompanyModel.GetMyCompany(Constants.user).CompanyID))
            {
                return View();
            }
            else return RedirectToAction("AccessDenied", "Error");
        }

        [HttpPost]
        public ActionResult UploadJSON(HttpPostedFileBase file = null, string type = null)
        {
            if (UserGroupModel.IsAdministrative(Constants.user) && CompanyModel.IsCompanyInsurer(CompanyModel.GetMyCompany(Constants.user).CompanyID))
            {
                List<string> lines = new List<string>();
                List<response> responses = new List<response>();
                string message = "";
                bool isValid = false;
                try
                {
                    isValid = (
                                (file.ContentType.ToLower() == "text/plain" && file != null)
                              );
                    isValid = isValid && (file.ContentLength < 5120000 && file != null) && type != null;
                }
                catch
                {

                    message = "The file was either too large or not of the right type. Please upload text files only that are under 5MB.";
                    //return View();
                }
                if (isValid)
                {
                    if (file != null)
                    {
                        Stream stream = file.InputStream; string line;
                        using (StreamReader sr = new StreamReader(stream)) { while ((line = sr.ReadLine()) != null) { lines.Add(line); } }
                    }
                  
                    //wording_upload.ParseData(filetext);
                    switch (type)
                    {
                        case "wording":
                            responses = wording_upload.ParseData(lines);
                            break;
                        case "usage":
                            responses = wording_upload.ParseUsageData(lines);
                            break;
                        default:
                            break;
                    }
                    
                    return Json(new { result = 1, response = responses, message = message }, JsonRequestBehavior.DenyGet);
                }
                else return Json(new { result = 0, message = message }, JsonRequestBehavior.DenyGet);
                
            }
            else return Json(new { result = 0, message = "Access error" }, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file = null, HttpPostedFileBase usage_file = null)
        {
            if (UserGroupModel.IsAdministrative(Constants.user) && CompanyModel.IsCompanyInsurer(CompanyModel.GetMyCompany(Constants.user).CompanyID))
            {
                #if DEBUG
                ViewData["debug"] = true;
                #else
                ViewData["debug"] = false;
                #endif
                
                List<string> lines = new List<string>();
                List<string> usage_lines = new List<string>();
                bool isValid = false;
                try
                {
                    isValid = (file != null || usage_file != null);
                    if (file != null) isValid = isValid && (file.ContentType.ToLower() == "text/plain" && file.ContentLength < 512000);
                    if (usage_file != null) isValid = isValid && (usage_file.ContentType.ToLower() == "text/plain" && usage_file.ContentLength < 512000);
                }
                catch{

                    ViewData["error"] = "The file was either too large or not of the right type. Please upload text files only that are under 5MB.";
                    return View();
                }
                if (isValid)
                {
                    if (file != null)
                    {
                        Stream stream = file.InputStream; string line; 
                        using (StreamReader sr = new StreamReader(stream)) { while ((line = sr.ReadLine()) != null) { lines.Add(line); } }
                    }
                    if (usage_file != null)
                    {
                        Stream stream = usage_file.InputStream; string line;
                        using (StreamReader sr = new StreamReader(stream)) { while ((line = sr.ReadLine()) != null) { usage_lines.Add(line); } }
                    }
                    List<response> responses = wording_upload.ParseData(lines);
                    List<response> usage_response = wording_upload.ParseUsageData(usage_lines);
                    ViewData["response"] = responses;
                    ViewData["usage_response"] = usage_response;
                }
                else ViewData["error"] = "The file was either too large or not of the right type. Please upload text files only that are under 5MB.";
                return View();
            }
            else return RedirectToAction("AccessDenied", "Error");
        }

        public ActionResult GoodDataTags(bool maplock = false)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Mapping", "Read")))
            {
                ViewData["maplock"] = maplock;
                return View();
            }
            else return new EmptyResult();
        }

    }
}



  