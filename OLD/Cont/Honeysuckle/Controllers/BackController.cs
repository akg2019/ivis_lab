﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class BackController : Controller
    {
        //
        // GET: /Back/

        public ActionResult BackBtn()
        {
            return View();
        }

        public ActionResult ClickBack()
        {
            Constants.BackClicked();
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

    }
}
