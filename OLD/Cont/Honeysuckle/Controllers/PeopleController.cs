﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using System.Text.RegularExpressions;

namespace Honeysuckle.Controllers
{
    public class PeopleController : Controller
    {

        public ActionResult AuditPersonPolicyHolder()
        {
            return View();
        }

        public ActionResult AuditPeople()
        {
            return View();
        }

        public ActionResult Details(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read")) && (PersonModel.CanISeeThisPerson(new Person(id)) || UserGroupModel.AmAnAdministrator(Constants.user))) return View(PersonModel.GetPerson(id));
            else return RedirectToAction("AccessDenied", "Error");
        }

        public ActionResult Edit(int id = 0)
        {

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Update")) && (PersonModel.CanISeeThisPerson(new Person(id)) || UserGroupModel.AmAnAdministrator(Constants.user)))
            {
                ViewData["TRNnotAvailable"] = false;
                Person per = PersonModel.GetPerson(id);
                if (per.TRN == "10000000") ViewData["TRNnotAvailable"] = true;

                return View(per);
            }
            else return RedirectToAction("AccessDenied", "Error");
        }

        [HttpPost]
        public ActionResult Edit(Person person, List<Address> address = null, string TRNnotAvailable = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Update")) && (PersonModel.CanISeeThisPerson(person) || UserGroupModel.AmAnAdministrator(Constants.user)) && address != null)
            {
                person.address = address[0];

                ViewData["TRNnotAvailable"] = false;
                if (person.TRN == "10000000") ViewData["TRNnotAvailable"] = true;
                if (TRNnotAvailable != null && TRNnotAvailable != "") person.TRN = TRNnotAvailable;

                if (person.TRN == "10000000") ModelState.Remove("TRN");

                if (ModelState.IsValid)
                {
                    PersonModel.ModifyPerson(person);
                    return RedirectToAction("Details", new { id = person.PersonID });
                }
                else return View(person);
            }
            else return RedirectToAction("AccessDenied", "Error");
        }

        public ActionResult ShowShortPeople()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("People", "Read"))) return View(); //return View(PersonModel.GetShortPeople(id, cmp));
            else return new EmptyResult();
        }

        public ActionResult PersonListItemSelected(int id = 0, bool insured = true)
        {
            if (id != 0) return View(PersonModel.GetPerson(id, 0 , insured));
            return new EmptyResult();
        }
      
        /// <summary>
        /// This function retrieves a person's information, given the person Id
        /// </summary>
        /// <param name="id">Person Id</param>
        public ActionResult personView(int perId = 0, string type = null, bool changeTRN = false, string trn = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("People", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read")))
            {
                if (type == "details") ViewData["peopleDetails"] = true;
                else ViewData["peopleDetails"] = false;

                Person person = new Person();
                if (changeTRN && trn != null) person = PersonModel.GetPersonByTRN(trn);
                else if (perId != 0) person = PersonModel.GetPerson(perId, 0, true);

                ViewData["personName"] = person.lname + ", " + person.fname;
                return View(person);
            }
            else return new EmptyResult();
        }

        public ActionResult TestTRN(int id = 0, string trn = null, string type = null)
        {
            if (id != 0 && trn != null) return Json(new { result = UserModel.TestTRN(id, trn, type) }, JsonRequestBehavior.AllowGet);
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This function pulls the list of all persons yet to be added to to a particular vehicle as a driver
        /// </summary>
        /// <param name="id">VIN of vehicle</param>
        public ActionResult driverList(string id = null, string polId= null)
        {
            List<UserPermission> permissions = new List<UserPermission>();
            permissions.Add(new UserPermission("People","Read"));
            permissions.Add(new UserPermission("PolicyHolder","Read"));
            permissions.Add(new UserPermission("Policy","Create"));
            permissions.Add(new UserPermission("Policy","Update"));
            permissions.Add(new UserPermission("Policy","Read"));

            if (UserModel.TestUserPermissions(Constants.user, permissions) && id != null)
            {
                List<Person> person = new List<Person>();
                person = PersonModel.GetNonDriver(id, int.Parse(polId));
                return View(person);
            }
            return Json(new { fail = 1 }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function pulls the list of all persons in the system
        /// </summary>
        public ActionResult peopleList(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read")) && id != 0)
            {
                if (Constants.user.newRoleMode) return View(PersonModel.GetAllPeopleIveInsured(Constants.user.tempUserGroup.company));
                else return View(PersonModel.GetAllPeopleIveInsured(new Company(id)));
            }
            else return Json(new { fail = 1 }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function retrieves information about a particular person, given the person ID
        /// </summary>
        /// <param name="id">Person Id</param>
        public ActionResult Person(int id = 0, string type = null,bool edit = false)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("People", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read")))
            {
                Person person = new Person();

                ViewData["editButton"] = edit;
                if (type != null) ViewData["policyInsured"] = true; // The person being added, is being added to a policy
                else ViewData["policyInsured"] = false; 
                if (id != 0)
                {
                    person = PersonModel.GetPerson(id);
                    person.emails = EmailModel.GetMyEmails(person);
                }
                
                return View(person);
            }
            else return RedirectToAction("Index");
        }


        public ActionResult FullDriverDetails(string id = null, bool create = false)
        {
            return RedirectToAction("FullPersonDetails", "People", new { id = id, create = create, insured = false });
        }

        /// <summary>
        /// This function retrieves the information about a particular person
        /// </summary>
        /// <param name="id">Person's TRN</param>
        public ActionResult FullPersonDetails(string id = null, bool create = false, bool insured = true, bool taj = false)
        {
            Person Person = new Person();
            Person = PersonModel.GetPerson(int.Parse(id), 0, insured);
            Person.address = AddressModel.GetAddress(Person.address);

            if (Person.address.roadnumber == null) Person.address.roadnumber = "";
            if (Person.address.ApartmentNumber == null) Person.address.ApartmentNumber = "";
            if (Person.address.road.RoadName == null) Person.address.road.RoadName = "";
            if (Person.address.roadtype == null) Person.address.roadtype.RoadTypeName = "";

            //if (create) ViewData["HeadingMessage"] = "Would you like to add this person: ";
            //else ViewData["HeadingMessage"] = "Person Details: ";

            if (taj) ViewData["IVISRecords"] = false;
            else ViewData["IVISRecords"] = true;

            return View(Person);
        }


        /// <summary>
        /// This function renders the email partial- to add a new email
        /// </summary>
        /// <param name="email">The email that is added</param>
        public ActionResult Email(Email email = null)
        {
            if (email != null) return RedirectToAction("NewEmail", "Email", new { id = email.emailID });
            else return RedirectToAction("NewEmail", "Email", new { id = 0 });
        }


        /// <summary>
        ///  SPECICAL CASE OF MODEL INTERACTION - Adding a person from jquery dialog
        /// </summary>
        /// <param name="email">The email that is added</param>
        public ActionResult AddPerson(string trn = null, string fname = null, string mname = null, string lname = null,
                                      string roadno = null, string aptNumber = null, string road = null, string roadType = null, string zipcode = null,
                                      string city = null, int parish = 0, string country = null, List<string> email = null, List<bool> primary = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Create")))
            {

                Person per = new Person();
                per.address = new Address(0, roadno, aptNumber, new Road(road), new RoadType(roadType), new ZipCode(zipcode), new City(city), new Parish(parish), new Country(country));

                per.address = AddressModel.TrimAddress(per.address);

                per.address.ID = AddressModel.AddAddress(per.address);

                Regex rgx = new Regex(@"[1][0-9]{8}");
                if (!rgx.IsMatch(trn)) return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);

                per.TRN = trn;
                per.fname = fname;
                per.mname = mname;
                per.lname = lname;

                List<Email> emails = new List<Email>();

                if (email != null)
                    if (email.Count != 0)
                        for (int i = 0; i < primary.Count; i++)
                        {
                            emails.Add(new Email(email[i], primary[i]));
                        }
                per.emails = emails;

                PersonModel.AddPerson(per, true);

                return Json(new { result = 1, per = per.PersonID}, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function retrieves the information about the main driver of a particular vehicle
        /// </summary>
        /// <param name="id">Person Id</param>
        public ActionResult MainDriver(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("People", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read")))
            {
                Person person = new Person();
                if (id != 0)
                {
                    person = PersonModel.GetPerson(id);
                    person.emails = EmailModel.GetMyEmails(person);
                   
                }
                return View(person);
            }
            else return new EmptyResult();
        }

        public ActionResult person_json(int id = 0)
        {
            List<UserPermission> permissions = new List<UserPermission>();
            permissions.Add(new UserPermission("Policies", "Create"));
            permissions.Add(new UserPermission("Policies", "Update"));
            if (UserModel.TestUserPermissions(Constants.user, permissions))
            {
                if (id == 0) return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
                else
                {
                    Person person = PersonModel.GetPerson(id);
                    if (person.PersonID == 0) return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
                    else
                    {
                        person.address = AddressModel.GetAddress(person.address);
                        return Json(new { result = 1, person = person }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult testTRNAvailable(string trn = null)
        {
            if (trn != null && trn != "")
            {
                return Json(new { result = PersonModel.TestTRNAvailable(trn) }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 1 }, JsonRequestBehavior.AllowGet );
        }
        


    }
}
