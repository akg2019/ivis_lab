﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using System.Web.Script.Serialization;

namespace Honeysuckle.Controllers
{
    public class PolicyCoverController : Controller
    {
        //
        // GET: /PolicyCover/

        public ActionResult Index()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyCovers", "Read")))
            {
                bool allow = false;

                if (Constants.user.newRoleMode)
                {
                    allow = (CompanyModel.IsCompanyInsurer(Constants.user.tempUserGroup.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user)) ;
                }
                else
                    allow = (CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));

                if (allow)
                {
                    if (TempData["deleted"] != null)
                    {
                        if ((bool)TempData["deleted"]) ViewData["deleted"] = true;
                        else ViewData["deleted"] = false;
                        TempData["deleted"] = null;
                    }
                    if (TempData["create"] != null)
                    {
                        ViewData["create"] = true;
                        TempData["create"] = null;
                    }

                    if (TempData["coverId"] != null)
                    {
                        ViewData["coverId"] = TempData["coverId"];
                        TempData["coverId"] = null;
                    }

                    if (UserGroupModel.AmAnAdministrator(Constants.user)) return View(PolicyCoverModel.GetAllPolicyCovers());
                    else return View(PolicyCoverModel.GetAllPolicyCoversForCompany());
                }
                else return RedirectToAction("AccessDenied", "Error");
            }
            else return RedirectToAction("AccessDenied", "Error");
        }

        public ActionResult Create()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyCovers", "Create")))
            {
                bool allow = false;

                if (Constants.user.newRoleMode)
                {
                    allow = (CompanyModel.IsCompanyInsurer(Constants.user.tempUserGroup.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));
                }
                else
                    allow = (CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));

                if (allow)
                {
                    return View();
                }
               else return RedirectToAction("Index");
            }
            else return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Create(PolicyCover policycover)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyCovers", "Create")))
            {
                bool allow = false;

                if (Constants.user.newRoleMode)
                {
                    allow = (CompanyModel.IsCompanyInsurer(Constants.user.tempUserGroup.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));
                }
                else
                    allow = (CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));

                if (allow)
                {

                    if (policycover != null)
                    {
                        policycover.cover = policycover.cover.Trim();
                        if (ModelState.IsValid)
                        {
                            PolicyCoverModel.addPolicyCover(policycover);
                            TempData["create"] = true;
                            TempData["coverId"] = policycover.ID;
                            return RedirectToAction("Index");
                        }
                        else return View(policycover);
                    }
                    else return View();
                }

              else return RedirectToAction("Index");
            }
            else return RedirectToAction("Index");
        }

        public ActionResult Edit(int id = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyCovers", "Update")))
            {
                bool allow = false;

                if (Constants.user.newRoleMode)
                {
                    allow = (CompanyModel.IsCompanyInsurer(Constants.user.tempUserGroup.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));
                }
                else
                    allow = (CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));

                if (allow)
                {
                    return View(PolicyCoverModel.GetCoverByID(new PolicyCover(id)));
                }
                else return RedirectToAction("Index");
            }
            else return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(PolicyCover policycover)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyCovers", "Update")))
            {
               bool allow = false;

               if (Constants.user.newRoleMode)
               {
                   allow = (CompanyModel.IsCompanyInsurer(Constants.user.tempUserGroup.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));
               }
               else
                   allow = (CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));

               if (allow)
                {
                    if (ModelState.IsValid) PolicyCoverModel.UpdateCover(policycover);
                    else return View(policycover);
                }
               return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        public ActionResult Details(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyCovers", "Read")) && id != 0)
            {
                bool allow = false;

                if (Constants.user.newRoleMode) allow = (CompanyModel.IsCompanyInsurer(Constants.user.tempUserGroup.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));
                else allow = (CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));

                if (allow)
                {
                    return View(PolicyCoverModel.GetCoverByID(new PolicyCover(id)));
                }
               else return RedirectToAction("Index");
            }
            else return RedirectToAction("Index");
        }

        public ActionResult Delete(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyCovers", "Delete")))
            {
                bool allow = false;

                if (Constants.user.newRoleMode)
                {
                    allow = (CompanyModel.IsCompanyInsurer(Constants.user.tempUserGroup.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));
                }
                else
                    allow = (CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user));

                if (allow)
                {
                    return Json(new { result = PolicyCoverModel.DeleteCover(new PolicyCover(id)) }, JsonRequestBehavior.AllowGet);
                }
               else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AssociateUsages(int id = 0)
        {
            if (id != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("Usages", "Create"))) return View(PolicyCoverModel.GetCoverByID(new PolicyCover(id)));
            else return RedirectToAction("AccessDenied", "Error");
        }

        public ActionResult SaveUsage(string usage,int id)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Usages", "Create")))
            {
               
                    Usage newUsage = new Usage();

                    newUsage = UsageModel.CreateUsage(new Usage(usage));
                    PolicyCoverModel.AssociateUsageToCover(id, newUsage.ID);

                    return Json(new { result = 1, usageID = newUsage.ID }, JsonRequestBehavior.AllowGet);
                
            }
            else return Json(new { result = 0, usageID = 0 }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult AssociateUsages(PolicyCover policycover)
        { 
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Usages", "Create")))
            {
                if (ModelState.IsValid)
                {
                    PolicyCoverModel.UpdateUsagesInCover(policycover);
                    return RedirectToAction("Index");
                }
                else return View(policycover);
            }
            else return RedirectToAction("Index");
        }


        public ActionResult CoversDropDownJSON(int Compid = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Create")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Create")))
            {
                List<PolicyCover> pc = new List<PolicyCover>();
                pc = PolicyCoverModel.GetPolicyCovers(Compid);
                JavaScriptSerializer jsonList = new JavaScriptSerializer();
                string jsonFile = jsonList.Serialize(pc);
                return Json(new { result = jsonFile }, JsonRequestBehavior.AllowGet);
            }
            else return new EmptyResult();


        }
        
        public ActionResult CoversDropdown(int Compid = 0, string type = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Wording", "Create")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Create")))
            {

                List<PolicyCover> pc = new List<PolicyCover>();
                if (Compid != 0) pc = PolicyCoverModel.GetPolicyCovers(Compid);
                else if (Constants.user.newRoleMode) pc = PolicyCoverModel.GetPolicyCovers(Constants.user.tempUserGroup.company.CompanyID);
                else pc = PolicyCoverModel.GetPolicyCovers(CompanyModel.GetMyCompany(Constants.user).CompanyID);

                int i, j;
                PolicyCover key = new PolicyCover();
                for (i = 1; i < pc.Count(); i++)
                {
                    key = pc[i];
                    j = i - 1;


                    while (j >= 0 && pc[j].cover.CompareTo(key.cover) > 0)
                    {
                        pc[j + 1] = pc[j];
                        j = j - 1;
                    }
                    pc[j + 1] = key;
                }

                return View(pc);
            }
            else return new EmptyResult();
        }

        public ActionResult Prefix(int id = 0)
        {
            List<UserPermission> permissions = new List<UserPermission>();
            permissions.Add(new UserPermission("PolicyCovers","Read"));
            permissions.Add(new UserPermission("Policies", "Read"));
            permissions.Add(new UserPermission("Policies", "Update"));
            permissions.Add(new UserPermission("Policies", "Create"));

            if (id != 0 && UserModel.TestUserPermissions(Constants.user, permissions)) return Json(new { result = 1, prefix = PolicyCoverModel.GetPolicyPrefix(new PolicyCover(id))}, JsonRequestBehavior.AllowGet);
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CoversUsagesSelect(int id = 0, int CoverId = 0, int UsageID = 0)
        {
            PolicyCover pc = new PolicyCover();
            pc.usages = new List<Usage>();

            if (CoverId != 0 && UsageID != 0)
            {
                //pc.ID = CoverId;
                pc.usages.Add(new Usage(UsageID));
                ViewData["coverID"] = CoverId;
                ViewData["new"] = false;
            }
            else
            {
                ViewData["new"] = true;
                List<PolicyCover> polCovs = new List<PolicyCover>();
                polCovs = PolicyCoverModel.GetPolicyCovers(id);
                ViewData["coverID"] = 0;
            }

            return View(pc);
        }


    }
}




