﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class MortgageeController : Controller
    {
        //
        // GET: /Mortgagee/

        public ActionResult AuditMortgagee()
        {
            return View();
        }


        public ActionResult GetMortgagees(string type = null)
        {
            List<string> result = new List<string>();

            List<UserPermission> permissions = new List<UserPermission>();
            permissions.Add(new UserPermission("Policy", "Create"));
            permissions.Add(new UserPermission("Policy", "Update"));

            if (UserModel.TestUserPermissions(Constants.user, permissions) && type != null) result = MortgageeModel.GetAllMortgagees();

            var json = result.Where(x => x.ToLower().StartsWith(type.ToLower()))
                             .OrderBy(x => x)
                             .Select(r => new { value = r });

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateMortgagee(string mortgagee = null, int polid = 0, int vehid = 0)
        {

            List<UserPermission> permissions = new List<UserPermission>();
            permissions.Add(new UserPermission("Policy", "Create"));
            permissions.Add(new UserPermission("Policy", "Update"));

            if (UserModel.TestUserPermissions(Constants.user, permissions))
            {
                Mortgagee mort =  MortgageeModel.AddMortgagee(new Mortgagee(mortgagee));

            }
            
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

    }
}
