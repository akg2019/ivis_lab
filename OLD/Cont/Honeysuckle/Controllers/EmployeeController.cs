﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class EmployeeController : Controller
    {

        public ActionResult AuditEmployee()
        {
            return View();
        }

        /// <summary>
        /// This function retrieves information about a particular employee, from the system, given the employee ID
        /// </summary>
        /// <param name="id">Employee Id</param>
        public ActionResult Employee(int id = 0)
        {
            Employee employee = new Employee();
            if (id != 0)
            {
                employee.EmployeeID = id;
                EmployeeModel.GetEmployee(employee);
            }
            return View(employee);
        }

    }
}
