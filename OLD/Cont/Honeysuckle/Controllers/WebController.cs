﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class WebController : Controller
    {
        //
        // GET: /Web/

        public ActionResult Index()
        {
            ViewData["searchDone"] = "";
            return View();
        }

        [HttpPost]
        public ActionResult Index(string chassis = null, string license = null)
        {
            if (chassis != null && license != null)
            {
                ViewData["model"] = PolicyModel.IsVehicleCovered(chassis, license);
                ViewData["searchDone"] = "Your search is finish. See results.";

                #region managing logging enquiries
                Enquiry enquiry = new Enquiry(license, chassis);
                enquiry = EnquiryModel.LogEnquiry(enquiry);
                EnquiryModel.WasEnquiryValid(enquiry);
                #endregion

                return View();
            }
            else return View();
        }

    }
}
