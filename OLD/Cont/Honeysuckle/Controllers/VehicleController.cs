﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using System.Globalization;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Honeysuckle.Controllers
{
    public class VehicleController : Controller
    {

        public ActionResult AuditRiskFromPolicy()
        {
            return View();
        }

        public ActionResult CurrentPolicy(int id = 0)
        {
            if (id != 0) return RedirectToAction("Details", "Policy", new { id = PolicyModel.GetVehiclePolicy(id).ID });
            else return RedirectToAction("Index", "Home");
        }

        public ActionResult CurrentCover(int id = 0)
        {
            if (id != 0) return RedirectToAction("Details", "VehicleCoverNote", new { id = VehicleCoverNoteModel.GetCurrentCoverNote(new Vehicle(id)).ID });
            else return RedirectToAction("Index", "Home");
        }
        
        public ActionResult VehicleListItemSelected(int id = 0)
        {
            if (id != 0)
            {
                return View(VehicleModel.GetVehicle(id));
            }
            return new EmptyResult();
        }

        public ActionResult VehicleDropDown(int id = 0)
        {
            if (UserModel.TestGUID(Constants.user))
            {
                if (id != 0 && (UserGroupModel.AmAnAdministrator(Constants.user) || VehicleModel.VehicleCoveredByMyPolicy(id)))
                {
                    List<Vehicle> vehicles = new List<Vehicle>();
                    vehicles.Add(VehicleModel.GetVehicle(id));
                    return View(vehicles);
                }
                else return View(VehicleModel.GetVehiclesCanBeCertOrCNote());
            }
            else return new EmptyResult();
        }

        public ActionResult DisplayVehicle(int test = 0, string type = null, int CoverID = 0, int VehicleUsage = 0)
        {
            
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read")) && test != 0)
            { 
                PolicyCover polCov = new PolicyCover();
                Usage use = new Usage();

                if(CoverID !=0) ViewData["PolCoverId"] = CoverID;

                if (type == "CertCreate") //Because the vehicle has already been chosen (it isn't a clean create)
                    ViewData["certificateCreate"] = false;

                else
                    ViewData["certificateCreate"] = true;

                Vehicle Risk = new Vehicle();

                if (test != 0)
                   {
                      Risk = VehicleModel.GetVehicle(test);
                      if (VehicleUsage != 0) Risk.usage = UsageModel.GetUsage(VehicleUsage);
                     //if(
                      ViewData["vehicleInfoDisplay"] = Risk.VehicleYear + " " + Risk.make + "  " + Risk.VehicleModel;
                    }
                    return View(Risk);
                }

            else return new EmptyResult();
        }

        /// <summary>
        /// This function renders the email partial- to add a new email address to a person
        /// </summary>
        /// <param name="email">The email that is added</param>
        public ActionResult Email(Email email = null)
        {
            if (email != null) return RedirectToAction("NewEmail", "Email", new { id = email.emailID });
            else return RedirectToAction("NewEmail", "Email", new { id = 0 });
        }

        /// <summary>
        /// This function displays the index of the vehicles
        /// </summary>
        /// <returns>The list of all vehicles</returns>
        public ActionResult Index(int id = 0)
        {
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// This function gets a list of all the vehicles in the system
        /// </summary>
        /// <returns>Risk List</returns>
        public ActionResult VehiclesList(int id = 0, string type = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read")) && id != 0)
            {
                if (type == null) // The list is being called by the policy creation form
                {

                    ViewData["HeadingMessage"] = "Please tick the appropriate vehicle (s):";
                    ViewData["certificate"] = false;

                    if (Constants.user.newRoleMode) return View(VehicleModel.VehiclesMyCompanyCovered(Constants.user.tempUserGroup.company));

                    else return View(VehicleModel.VehiclesMyCompanyCovered(CompanyModel.GetMyCompany(Constants.user)));
                }

                else // The list is being called by the certificate/cover note create form
                    if (UserModel.TestGUID(Constants.user))
                    {
                        ViewData["HeadingMessage"] = "Please select a vehicle:";
                        ViewData["certificate"] = true;
                        if (id != 0)
                        {
                            switch (type)
                            {
                                case "certificate":
                                    if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Create"))) return View(VehicleModel.GetVehiclesWithoutCerts());
                                    else return new EmptyResult();
                                case "cover":
                                    if (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Create"))) return View(VehicleModel.GetVehiclesWithoutCover());
                                    else return new EmptyResult();
                                default:
                                    return new EmptyResult();
                            }
                            /*
                            if (type == "certificate" && )
                                
                            else if (type == "cover" && )
                                
                            else
                                return View(VehicleModel.GetVehiclesCanBeCertOrCNote());
                             */
                        }
                        else return new EmptyResult();
                    }
                    else return new EmptyResult();
            }
            else
            {
                return Json(new { fail = 1 }, JsonRequestBehavior.AllowGet);
            }
         }


        /// <summary>
        /// This function gets a list of all the vehicles under a policy
        /// </summary>
        /// <param name="id">Policy ID</param>
        /// <returns>Risk List</returns>
        public ActionResult PolicyVehiclesList(int id = 0, string type = "")
        {
            if (UserModel.TestGUID(Constants.user))
            {
                if (id != 0 && ((UserModel.TestUserPermissions(Constants.user, new UserPermission("Certificates", "Create")) && type == "certificate") || (UserModel.TestUserPermissions(Constants.user, new UserPermission("CoverNotes", "Create")) && type == "coverNote")))
                {
                    List <Vehicle> vehicles = new List <Vehicle>();

                    ViewData["PolID"] = id;
                    ViewData["type"] = type;
                    Policy policy = PolicyModel.getPolicyFromID(id);

                    foreach (Vehicle veh in policy.vehicles)
                    {
                        if (!VehicleCertificateModel.IsVehicleUnderActiveCert(veh.ID, policy.startDateTime, policy.endDateTime, policy.ID) && !VehicleCertificateModel.IsPolicyCertMade(veh.ID, id))
                        {
                            //If it is cert creation, then proceed to add the vehicle to the list, else test if anothe cover 
                            //note can be made for the vehicle under this given policy-then add
                            if(type == "certificate" || VehicleCoverNoteModel.CanIGenerateCoverNote(veh,policy)) vehicles.Add(veh);
                        }
                    }

                    return View (vehicles);
                }

                else return Json(new { fail = 1 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { fail = 1 }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function retrieves all the information about a particular Risk
        /// </summary>
        /// <param name="id">Risk's id</param>
        public ActionResult Vehicle(int test = 0, string type = null, string startDate = null, string endDate = null, 
                                    string CoverID = null, string VehicleUsage = null, int polid = 0, bool imported = false, 
                                    string page = null, string mortgagee = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read")) && test != 0)
            {
                ViewData["History"] = false;
                if (polid != 0) ViewData["polid"] = polid;
                
                DateTime start = new DateTime(); DateTime end = new DateTime();
                PolicyCover polCov = new PolicyCover();
                Usage use = new Usage();
                
                start = DateTime.Parse(startDate);
                end = DateTime.Parse(endDate);

                //TEST IF THE POLICY'S END DATE IS PAST (THEN CANCEL RISK, DELETE RISK AND CREATE COVER NOTES ETC , SHOULDN'T BE AVAILBALE ON VEHICLE VIEW)
                int result = DateTime.Compare(DateTime.Now, end);

                if (result > 0) //The end date of the policy is before the current datetime
                {
                    if (CoverID != "" && CoverID != null) ViewData["PolCoverId"] = int.Parse(CoverID);
                    ViewData["isinsured"] = ViewData["isinsuredatmyco"] =  false;
                    ViewData["expired"] = true;
                }
                else
                {
                    if (polid != 0) //If the policy id is sent, then it can be assumed that the vehicle is under that policy (no cancel pol ect is shown).
                    {
                        if (VehicleModel.isVehUnderActivePolicy(test, start, end,polid))
                        {
                            if (CompanyModel.CanAutoCancelVehiclePolicy(PolicyModel.GetPolicyCoveredUnder(test, start, end).insuredby.CompanyID))
                            {
                                ViewData["isinsuredatmyco"] = false;
                                ViewData["isinsured"] = false;
                            }
                            else
                            {
                                ViewData["isinsuredatmyco"] = VehicleModel.IsVehicleUnderPolicyAtMyCompany(test, start, end);  //Tests if the Risk is previously insured at my company
                                ViewData["isinsured"] = VehicleModel.IsVehicleUnderPolicy(test, polid, start, end); //Tests if the Risk is previously insured
                            }
                            }
                        else
                        {
                            ViewData["isinsuredatmyco"] = false;
                            ViewData["isinsured"] = false;
                        }
                    }
                    else
                    {

                        if (VehicleModel.isVehUnderActivePolicy(test, start, end))
                        {
                            if (CompanyModel.CanAutoCancelVehiclePolicy(PolicyModel.GetPolicyCoveredUnder(test, start, end).insuredby.CompanyID))
                            {
                                // VehicleModel.CancelVehicleToPolicyAssoc(test, 0, start, end);
                                ViewData["isinsuredatmyco"] = false;
                                ViewData["isinsured"] = false;
                            }
                            else
                            {
                                ViewData["isinsuredatmyco"] = VehicleModel.IsVehicleUnderPolicyAtMyCompany(test, start, end);  //Tests if the Risk is previously insured at my company
                                ViewData["isinsured"] = VehicleModel.IsVehicleUnderPolicy(test, polid, start, end); //Tests if the Risk is previously insured
                            }
                        }

                        else
                        {
                            ViewData["isinsuredatmyco"] = false;
                            ViewData["isinsured"] = false;
                        }
                    }

                    if (CoverID != "" && CoverID != null) ViewData["PolCoverId"] = int.Parse(CoverID);
                    if ((bool)ViewData["isinsured"] && !((bool)ViewData["isinsuredatmyco"])) ViewData["compName"] = CompanyModel.GetInsuranceCo(test, start, end).CompanyName;
                    ViewData["Expired"] = false;
                }

                ViewData["polid"] = polid;
                ViewData["PolStart"] = start;
                ViewData["PolEnd"] = end;

                ViewData["VehUnderThisPolicy"] = VehicleModel.isVehUnderThisPolicy(polid, test);
                if (polid != 0) ViewData["History"] = (VehicleModel.HasCoverNoteHistory(test, polid));

                ViewData["details"] = (type == "details");
                ViewData["Edit"] = (type == "Edit");
                ViewData["Create"] = (type == "Add");
                ViewData["imported"] = (imported);

                Vehicle Risk = new Vehicle();

                if (test != 0)
                {
                    Risk = VehicleModel.GetVehicle(test, true);
                    ViewData["vehicleInfoDisplay"] = Risk.VehicleYear + " " + Risk.make + "  " + Risk.VehicleModel;
                }

                if (mortgagee == null)
                {
                    if (polid != 0) Risk.mortgagee = VehicleModel.getMortgagee(Risk.ID, polid);
                }
                else Risk.mortgagee = new Mortgagee(mortgagee);


                if (VehicleUsage != null) { 
                    Risk.usage = new Usage(int.Parse(VehicleUsage));
                    //ViewData["VehicleUsage"] = VehicleUsage;
                }
                else
                {
                    if (CoverID != null)
                    {
                        polCov = new PolicyCover(int.Parse(CoverID));
                        if (UsageModel.GetPolicyCoverUsages(polCov).Count != 0) Risk.usage = UsageModel.GetPolicyCoverUsages(polCov)[0]; //RETRIEVING THE USAGE ID OF THE FIRST USAGE ASSOCIATED WITH THE COVER
                    }
                }

                //if (Risk.usage == null && VehicleUsage != null) Risk.usage = UsageModel.GetUsage(int.Parse(VehicleUsage));

                ViewData["VehicleID"] = test;
                if (page == null)
                {
                    VehicleCoverNote vcn = new VehicleCoverNote();
                    vcn = VehicleCoverNoteModel.GetActiveCoverNote(test, start.ToString(), end.ToString(), polid);
                    if (vcn.ID != 0)
                    {
                        vcn = VehicleCoverNoteModel.GetCoverNo(vcn.ID);
                        ViewData["coverDetails"] = vcn.covernoteno + "/" + vcn.effectivedate.ToString("MMM dd yyyy") + " - " + vcn.expirydate.ToString("MMM dd yyyy");

                    }

                    if (polid != 0)
                    {
                        List<VehicleCoverNote> vcnList = new List<VehicleCoverNote>();
                        vcnList = VehicleCoverNoteModel.CoverNoteHistory(VehicleModel.GetVehicle(test), new Policy(polid));
                        ViewData["coverHistory"] = (vcnList.Count()).ToString();
                    }
                    else ViewData["coverHistory"] = 0;
                }

                return View(Risk);
            }
            else return new EmptyResult(); // NO DATA OUT AT THIS POINT TO BE CHANGED TO 404 ERROR PAGE
        }

        /// <summary>
        /// This function deals with the auto-filling of transmission types
        /// </summary>
        /// <param name="type">Search letters/words </param>
        public ActionResult GetTransmissionTypes(string type)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read")))
            {
                List<string> result = new List<string>();
                TransmissionTypeModel tm = new TransmissionTypeModel();
                result = tm.GetTransmissionTypes();

                var json = result.Where(x => x.ToLower().StartsWith(type.ToLower()))
                                 .OrderBy(x => x)
                                 .Select(r => new { value = r });

                return Json(json, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { fail = 1 }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This function deals with the auto-filling of hand drive types
        /// </summary>
        /// <param name="type">Search letters/words </param>
        public ActionResult GetHandDriveTypes(string type)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read")))
            {
                List<string> result = new List<string>();
                HandDriveTypeModel hd = new HandDriveTypeModel();
                result = hd.GetHandDriveType();

                var json = result.Where(x => x.ToLower().StartsWith(type.ToLower()))
                                 .OrderBy(x => x)
                                 .Select(r => new { value = r });

                return Json(json, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { fail = 1 }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This function returns the details of a Risk
        /// </summary>
        /// <param name="id">Risk's id </param>
        public ActionResult Details(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read")) && id != 0)
            {
       
                Vehicle v = new Vehicle();
                v = VehicleModel.GetVehicle(id);

                ViewData["mainDriver"] = VehicleModel.GetMainDriver(id);
                ViewData["fitnessExpiry"] = v.expiryDateOfFitness.ToString("MMM d, yyyy");
                return View(v);
            }
            else return RedirectToAction("Index");
        }


        /// <summary>
        /// This function returns the full details of a Risk, to a jquery dialog
        /// </summary>
        /// <param name="id">Risk's id </param>
        public ActionResult FullVehicleDetails(string id = null, bool create = false)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read")))
            {
                if (id != null)
                {
                    if (create) ViewData["HeadingMessage"] = "Would you like to add this vehicle: ";
                    else ViewData["HeadingMessage"] = "Vehicle Details: ";
                    Vehicle veh = VehicleModel.GetVehicle(int.Parse(id));
                    ViewData["noTRN"] = false;
                    if(veh.mainDriver.person.TRN == "10000000") ViewData["noTRN"] = true;
                    return View(VehicleModel.GetVehicle(int.Parse(id)));
                }
                else return new EmptyResult();
            }
            else return new EmptyResult();
        }


        /// <summary>
        /// This function deals with the creation of a Risk
        /// </summary>
        public ActionResult Create()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Create")) && false)
            {
                Vehicle vm = new Vehicle();
                ViewData["expiryDate"] = DateTime.Now; 
                return View(vm);
            }
            else return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// This function deals with the creation of a Risk (post-back)
        /// </summary>
        [HttpPost]
        public ActionResult Create(Vehicle Risk = null, string expiryDateoffitness = null, IEnumerable<Email> emails = null, IEnumerable<Person> person = null, IEnumerable<Address> address = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Create")) && false)
            {

                Person per = new Person();
               
                //storing expiry date in viewdata to be displayed if page is called back
                ViewData["expiryDate"] = expiryDateoffitness;

                ModelState.Remove("mainDriver.person.address.parish.ID");
                ModelState.Remove("expiryDateOfFitness");
                if (ModelState.IsValid)
                {
                    per = person.ToList()[0];
                    Risk.mainDriver = new Driver (per);
                    Risk.mainDriver.person.address = address.ToList()[0];
                    Risk.mainDriver.person.address = AddressModel.TrimAddress(Risk.mainDriver.person.address);

                    if (emails != null) Risk.mainDriver.person.emails = emails.ToList();

                    DateTime expirydate = new DateTime();

                    string pattern = "MM/dd/yyyy";

                    if (DateTime.TryParseExact(expiryDateoffitness, pattern, null,
                                    DateTimeStyles.None, out expirydate))

                        Risk.expiryDateOfFitness = expirydate;

                    Risk = VehicleModel.addVehicle(Risk);

                    TempData["EmailSent"] = "Risk Edited!";
                    return RedirectToAction("Index", "Home");
                }
                else // if modelstate is invalid (page is brought back with data entered)
                {
                    Risk.mainDriver = new Driver(person.ToList()[0]);
                    Risk.mainDriver.person.address = address.ToList()[0];
                    if (emails != null) Risk.mainDriver.person.emails = emails.ToList();
                    return View(Risk);

                }
              
            }
            else return RedirectToAction("Index", "Home");
        }


        /// <summary>
        /// This function deals with the editing of a Risk
        /// </summary>
        /// <param name="id">Risk's id </param>
        public ActionResult Edit(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Update")) && id != 0)
            {
                Vehicle Risk = new Vehicle();
                Risk = VehicleModel.GetVehicle(id);

                ViewData["TRNnotAvailable"] = false;
                if (Risk.mainDriver != null)
                {
                    Risk.mainDriver.person.emails = EmailModel.GetMyEmails(Risk.mainDriver.person);
                    if (Risk.mainDriver.person.TRN == "10000000") ViewData["TRNnotAvailable"] = true;
                }

                if (Risk.expiryDateOfFitness.ToString("d MMM yyyy") == "1 Jan 0001")
                    ViewData["expiryDate"] = "";
                else
                    ViewData["expiryDate"] = Risk.expiryDateOfFitness.ToString("d MMM yyyy");
                
        
                return View(Risk);
            }
            else return RedirectToAction("Index", "Home");
        }


        /// <summary>
        /// This function deals with the editing of a Risk
        /// </summary>
        [HttpPost]
        public ActionResult Edit(Vehicle Risk, string expiryDateoffitness = null, IEnumerable<Email> emails = null, int parishes = 0, string prevRegNo = null, string TRNnotAvailable = null) 
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Update")))
            {

                if (VehicleModel.DoesRegNoExist(Risk.VehicleRegNumber,Risk.ID))
                {
                    //if (prevRegNo != Risk.VehicleRegNumber)
                    //{
                        ViewData["RegNo"] = "The Vehicle Registration Number is already in use.";
                        return View(Risk);
                    //}
                }

                Person per = new Person();
                DateTime expirydate = new DateTime();
                Usage use = new Usage();

                string pattern = "d MMM yyyy";
                Risk.usage = use;

                if (DateTime.TryParseExact(expiryDateoffitness, pattern, null,
                DateTimeStyles.None, out expirydate))

                Risk.expiryDateOfFitness = expirydate;

                ViewData["expiryDate"] = Risk.expiryDateOfFitness;
                ModelState.Remove("usage");

                if (parishes != 0) Risk.mainDriver.person.address.parish = new Parish(parishes);
                Risk.mainDriver.person.address = AddressModel.TrimAddress(Risk.mainDriver.person.address);

                ModelState.Remove("mainDriver.person.address.parish.ID");
                ModelState.Remove("expiryDateOfFitness");

                ViewData["TRNnotAvailable"] = false;
                Regex rgx = new Regex(@"^[1][0-9]{8}$");
                if (Risk.mainDriver.person.TRN == "10000000") ViewData["TRNnotAvailable"] = true;
                if (TRNnotAvailable != null && TRNnotAvailable != "") Risk.mainDriver.person.TRN = TRNnotAvailable;

                if (Risk.mainDriver.person != null && Risk.mainDriver.person.TRN != null && (Risk.mainDriver.person.TRN == "10000000" || rgx.IsMatch(Risk.mainDriver.person.TRN))) ModelState.Remove("mainDriver.person.TRN");
                
                if (ModelState.IsValid)
                {
                    if (emails != null) Risk.mainDriver.person.emails = emails.ToList(); 
                    VehicleModel.addVehicle(Risk);
                    TempData["EmailSent"] = "Risk Edited!";

                    return RedirectToAction("Details", "Vehicle", new { id = Risk.ID });
                }
                else // if modelstate is invalid (page is brought back with data entered)
                {
                    if (emails != null) Risk.mainDriver.person.emails = emails.ToList();
                    return View(Risk);
                }    
               
            }
            else return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// This function deals with the editing of a Risk's drivers
        /// </summary>
        /// <param name="id">Risk's id </param>
        public ActionResult EditDrivers(int id = 0, int pol = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Update")) && id != 0 && pol != 0)
            {
                Vehicle veh = new Vehicle();
                veh = VehicleModel.GetVehicle(id);

                //stored the TRN for main driver of Risk- for display
                ViewData["pol"] = pol;
                ViewData["mainDriver"] = VehicleModel.GetMainDriver(id);

                //gets the list of authorized, excepted and excluded drivers of the Risk
                veh.AuthorizedDrivers = VehicleModel.GetDriverList(id, pol, 1);
                veh.ExceptedDrivers = VehicleModel.GetDriverList(id, pol, 2);
                veh.ExcludedDrivers = VehicleModel.GetDriverList(id, pol, 3);
                return View(veh);
            }
            else
            {
                if (pol != 0) return RedirectToAction("Details", "Policy", new { id = pol });
                else return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// This function gets a user/person/employee based on a TRN
        /// </summary>
        /// <param name="id">person's TRN </param>
        public ActionResult GetUserByTRN(string id=null, bool insured = false,bool test = false)
        {
            #region accept certs
            //test code
            System.Net.ServicePointManager.ServerCertificateValidationCallback +=
            delegate(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                    System.Security.Cryptography.X509Certificates.X509Chain chain,
                                    System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                return true; // **** Always accept
            };
            #endregion
            if (id == "Not Available") id = "10000000";
            if (id == "10000000" && test) return Json(new { error = 0, result = 1, person = 0, blank = 1 }, JsonRequestBehavior.AllowGet);

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles","Update")))
            {
                bool result = false; bool personresult = false; bool userresult = false; bool fail = false;
                string trn = null;
                try
                {
                    trn = id;
                    result = true;
                }
                catch { fail = true; }
                if (!fail)
                {
                    Person person = new Person();
                    person.TRN = trn;
                    User user = PersonModel.TestPersonWithTRN(person, insured);
                    bool taj = false;

                    if (user.ID != 0) { userresult = true; result = true; }
                    if (!userresult) { if (user.employee != null) { if (user.employee.person != null) { if (user.employee.person.PersonID != 0) { personresult = true; result = true; } } } }
                    if (result)
                    {
                        //CHECK TAJ
                        if (!userresult && !personresult)
                        {
                            person = TAJ.GetPersonDetails(trn);
                            if (person.TRN != null && person.TRN != "")
                            {
                                taj = true;
                                personresult = true;
                            }
                        }

                        //SEND DATA
                        if (userresult || personresult)
                        {
                            if (person.address.longAddressUsed)
                                return Json(new
                                {
                                    error = 0,
                                    result = 1,
                                    user = 0,
                                    person = 1,
                                    taj = taj,
                                    personid = person.PersonID,
                                    fname = person.fname,
                                    mname = person.mname,
                                    lname = person.lname,
                                    longAddress = person.address.longAddress,
                                    longAddressUsed = person.address.longAddressUsed
                                }, JsonRequestBehavior.AllowGet);
                            else
                            //USER ALREADY EXISTS
                            return Json(new
                                {
                                    error = 0,
                                    result = 1,
                                    user = 0,
                                    person = 1,
                                    taj = taj,
                                    personid = person.PersonID,
                                    fname = person.fname,
                                    mname = person.mname,
                                    lname = person.lname,
                                    roadno = person.address.roadnumber,
                                    aptNumber = person.address.ApartmentNumber,
                                    roadname = person.address.road.RoadName,
                                    roadtype = person.address.roadtype.RoadTypeName,
                                    zipcode = person.address.zipcode.ZipCodeName,
                                    city = person.address.city.CityName,
                                    country = person.address.country.CountryName,
                                    parishid = person.address.parish.ID,
                                    parish = person.address.parish.parish
                               
                                }, JsonRequestBehavior.AllowGet);
                        }
                        else return Json(new { error = 0, message = "There was an error." }, JsonRequestBehavior.AllowGet); //no data
                    }
                    else return Json(new { error = 0, result = 0 }, JsonRequestBehavior.AllowGet); //TRN IS FREE DO NOTHING
                 }
                else return Json(new { error = 1, message = "You must enter a number with 9 digits into the TRN field." }, JsonRequestBehavior.AllowGet); //fail in parsing text to int
            }
            else return Json(new { error = 1, message = "You do not have permissions to be running this procedure." }, JsonRequestBehavior.AllowGet); //permission error
        }


        /// <summary>
        /// This function checks if a Risk is already in the system, gievn its chassis
        /// </summary>
        /// <param name="id">Risk chassis </param>
        public ActionResult GetVehicleByChassis(string id = null,int vehID = 0)
        {
            #region accept certs
            //test code
            System.Net.ServicePointManager.ServerCertificateValidationCallback +=
            delegate(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                    System.Security.Cryptography.X509Certificates.X509Chain chain,
                                    System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                return true; // **** Always accept
            };
            #endregion

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Update")))
            {
                // bool result = false; bool personresult = false; bool userresult = false; bool fail = false;
                bool result = false; bool vehicleExists = false; bool fail = false;
                string chassis = "";
                try
                {
                    chassis = id;
                    result = true;
                }
                catch { fail = true; }
                if (!fail)
                {
                    Vehicle v = new Vehicle();
                    v.chassisno = chassis;

                    v = VehicleModel.TestVehicleWithChassis(v,vehID); //changed

                    if (v.ID != 0) vehicleExists = true;

                    if (vehicleExists)
                    {
                        if (result)
                        {
                            string expiryfitness;
                            v = VehicleModel.GetVehicle(v.ID);
                            if (v.expiryDateOfFitness.ToString("d MMM yyyy") == "1 Jan 0001") expiryfitness = ""; else expiryfitness = v.expiryDateOfFitness.ToString("d MMM yyyy");
                            return Json(new { error = 0, result = 1, TAJ = 0, vehicle = 1, vid = v.ID, comp = 0, vehicle_data = v, fitnessExpiry = expiryfitness }, JsonRequestBehavior.AllowGet); //Risk ALREADY EXISTS
                        }
                        else return Json(new { error = 0, TAJ = 0, message = "There was an error." }, JsonRequestBehavior.AllowGet); //Risk doesn't exist
                    }
                    else
                    {
                        //check TAJ
                        Vehicle vehicle = TAJ.GetVehicleDetails(id, null, null);
                        if (vehicle.chassisno != null && vehicle.chassisno != "")
                        {
                            /*added 20/5/2016*/
                            Vehicle veh = VehicleModel.addVehicle(vehicle);
                            //we got something
                            return Json(new { error = 0, 
                                              result = 1, 
                                              vehicle = 1, 
                                              TAJ = 1,
                                              fitnessExpiry = vehicle.expiryDateOfFitness,
                                              vehicle_data = vehicle,
                                              vid = veh.ID,
                                              /*
                                              chassis = vehicle.chassisno,
                                              engineno = vehicle.engineNo,
                                              regno = vehicle.VehicleRegNumber,
                                              vehyear = vehicle.VehicleYear,
                                              model = vehicle.VehicleModel,
                                              expiry = vehicle.expiryDateOfFitness,
                                              owners = vehicle.RegisteredOwner,*/
                                              comp = 0 }, JsonRequestBehavior.AllowGet);
                        }
                        else return Json(new { error = 0, TAJ = 0, result = 0 }, JsonRequestBehavior.AllowGet); //VIN IS FREE, DO NOTHING
                    }
                }
                else return Json(new { error = 1, TAJ = 0, message = "You must enter only letters and numbers into the VIN field." }, JsonRequestBehavior.AllowGet); //fail in parsing text to int
            }
            else return Json(new { error = 1, TAJ = 0, message = "You do not have permissions to be running this procedure." }, JsonRequestBehavior.AllowGet); //permission error
        }


        
        /// <summary>
        /// This function gets a Risk's information, based on the ID of the Risk
        /// </summary>
        /// <param name="id">Risk's id </param>
        public ActionResult GetVehicleByID(int id=0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read")) && id != 0)
            {

                Vehicle v = new Vehicle();
                v = VehicleModel.GetVehicle(id);
                //returning policy data
                return Json(new { ExpiryDate = v.expiryDateOfFitness.ToString() }, JsonRequestBehavior.AllowGet);
            }
          else return new EmptyResult();

        }

        /// <summary>
        /// This function checks if a Registration No is already in the system
        /// </summary>
        /// <param name="id">RegNo</param>
        public ActionResult RegNoExists(string regNo = null)
        {


            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Update")))
            {
                if (VehicleModel.DoesRegNoExist(regNo))
                    return Json(new { result = 1, error = 0, regNoExist = 1 }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { result = 1, error = 0, regNoExist = 0 }, JsonRequestBehavior.AllowGet);

            }
            else
                 return Json(new { result = 0, error = 1, message = "You do not have permissions to be running this procedure." }, JsonRequestBehavior.AllowGet); 
        }


        /// <summary>
        /// This function deals with the adding adding of a new Risk
        /// </summary>
        public ActionResult AddNewVehicle (int PolCoverID = 0)
        {
            //Constants.user.UserGroups = UserModel.GetGroupsAndPermissions(Constants.user.ID);
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Create")))
            {
                ViewData["expiryDate"] = DateTime.Now;
                ViewData["PolCoverId"] = PolCoverID;
                return View();
            }
            else return new EmptyResult();
        }


        /// <summary>
        /// SPECICAL CASE OF MODEL INTERACTION - Adding a Risk from jquery dialog
        /// </summary>
        public ActionResult AddVehicles(string vin = null, string chassis = null, string make = null, string model = null, string modelType = null, string bodyType = null,
                                        string Extension = null, string vehicleRegNumber = null, string colour = null, string cylinders = null, string engineNo = null, bool engineModified = false, 
                                        string engineType = null, string estAnnualMileage = null, string expiryDateOfFitness = null, string HPCCUnitType = null, string ReferenceNo = null,
                                        string RegistrationLocation = null, string RoofType = null, string seating = null, string seatingCert = null, 
                                        string tonnage = null, string VehicleYear = null, string handDriveType = null, string transtype = null, string regOwner = null,
                                        bool restrictedDriving = false, string perId = null, string trn = null, string fname = null, string mname = null,
                                        string lname = null, string roadno = null, string aptNumber = null, string road = null, string roadType = null, 
                                        string zipcode = null, string city = null, string parish = null, string country = null, List<string> email = null, List<bool> primary = null,
                                        string estimatedvalue = null, bool advanced = false, int vehicleid = 0, string prevChassis = null, string prevRegNo = null
                                        )
        {

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Create")))
            {
               if(VehicleModel.DoesRegNoExist(vehicleRegNumber) && (prevChassis != null && prevRegNo != null))
                   if(prevChassis == "" || (prevRegNo != vehicleRegNumber && prevChassis != chassis) ||
                       (prevRegNo != vehicleRegNumber && prevChassis == chassis) ||
                       (prevRegNo == vehicleRegNumber && prevChassis != chassis))
                   return Json(new {result = 0 , regExist = 1},JsonRequestBehavior.AllowGet);
                    

                Person per = new Person();
                Vehicle Risk = new Vehicle();

                Risk.make = make;
                Risk.VehicleModel = model;
                Risk.VIN = vin;
                Risk.chassisno = chassis;
                if (VehicleYear != "") Risk.VehicleYear = int.Parse(VehicleYear);
                Risk.VehicleRegNumber = vehicleRegNumber;

                if (advanced)
                {
                    per.TRN = trn;
                    per.fname = fname;
                    per.mname = mname;
                    per.lname = lname;

                    per.address = new Address(0, roadno, aptNumber, new Road(road), new RoadType(roadType), new ZipCode(zipcode), new City(city), new Parish(int.Parse(parish)), new Country(country));
                    per.PersonID = int.Parse(perId);
                    Risk.mainDriver = new Driver(per);

                    List<Email> emails = new List<Email>();
                    if (email != null)
                        for (int i = 0; i < primary.Count; i++)
                        {
                            emails.Add(new Email(email[i], primary[i]));
                        }
                    per.emails = emails;

                    DateTime expirydate = new DateTime();

                    if (DateTime.TryParse(expiryDateOfFitness, null, DateTimeStyles.None, out expirydate)) Risk.expiryDateOfFitness = expirydate;

                    //Placing all that data in the Risk model
                    Risk.modelType = modelType;
                    Risk.bodyType = bodyType;
                    Risk.estimatedValue = estimatedvalue;

                    Risk.extension = Extension;
                    HandDriveType hdt = new HandDriveType();
                    hdt.handDriveType = handDriveType;
                    Risk.handDrive = hdt;
                    Risk.colour = colour;

                    Risk.engineNo = engineNo;
                    Risk.engineModified = engineModified;
                    Risk.engineType = engineType;

                    Risk.HPCCUnitType = HPCCUnitType;
                    Risk.referenceNo = ReferenceNo;
                    Risk.registrationLocation = RegistrationLocation;
                    Risk.roofType = RoofType;
                    
                    
                    if (cylinders != "") Risk.cylinders = int.Parse(cylinders);
                    if (seating != "") Risk.seating = int.Parse(seating);
                    if (seatingCert != "") Risk.seatingCert = int.Parse(seatingCert);
                    if (tonnage != "") Risk.tonnage = int.Parse(tonnage);
                    if (estAnnualMileage != "") Risk.estAnnualMileage = int.Parse(estAnnualMileage);
                    
                    TransmissionType tt = new TransmissionType();
                    tt.transmissionType = transtype;
                    Risk.transmissionType = tt;

                    Risk.RegisteredOwner = regOwner;
                    Risk.RestrictedDriving = restrictedDriving;
                    Usage use = new Usage();
                    Risk.usage = use;

                }
                Risk = VehicleModel.addVehicle(Risk);


                return Json(new { result = 1, RiskID = Risk.ID }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0}, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// This function deals with the adding of a person, that is not yet a driver, then adding the person as a driver
        /// </summary>
        /// <param name="id">Risk's id </param>
        public ActionResult AddPersonDriver(string trn = null, string fname = null, string mname = null, string lname = null, string roadno = null, string aptNumber = null, string road = null,
                                      string roadType = null, string zipcode = null, string city = null, string parish = null, string country = null, List<string> email = null, List<bool> primary = null)
        {

            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("People", "Create")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Create")))
            {

                Person per = new Person();
                per.address = new Address(0, roadno, aptNumber, new Road(road), new RoadType(roadType), new ZipCode(zipcode), new City(city), new Parish(int.Parse(parish)), new Country(country));

                per.address.ID = AddressModel.AddAddress(per.address);

                per.TRN = trn;
                per.fname = fname;
                per.mname = mname;
                per.lname = lname;

                List<Email> emails = new List<Email>();

                if (email != null)
                    if (email.Count != 0)
                        for (int i = 0; i < primary.Count; i++)
                        {
                            emails.Add(new Email(email[i], primary[i]));
                        }

                per.emails = emails;

                PersonModel.AddPerson(per); //adding the person
                DriverModel.addDriver(per.PersonID); //making the person a driver

                return Json(new { result = 1, perId = per.PersonID}, JsonRequestBehavior.AllowGet);

            }

            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This function deals with the adding of drivers to a Risk, from jquery dialog
        /// </summary>
        public ActionResult AddDrivers(string chassis = null, string PersonId = null, string type = null, string pol = null) 
        {
             List<UserPermission> permissions = new List<UserPermission>();
            permissions.Add(new UserPermission("Policies","Create"));
            permissions.Add(new UserPermission("Policies","Update"));

            if (UserModel.TestUserPermissions(Constants.user, permissions) && chassis != null && PersonId != null && type != null && pol != null)
            {
                VehicleModel.addDriversToVehicle(chassis, int.Parse(PersonId), int.Parse(type), int.Parse(pol));
                return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function deals with the  adding of drivers to a Risk
        /// </summary>
       public ActionResult AddAllDrivers(string id =null)
       {
           if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Create")))
           {
               ViewData["vin"] = id;
               return View();
           }
           else return new EmptyResult();
       }

       /// <summary>
       /// This function deals with the adding of drivers to a Risk
       /// </summary>
       /// <param name="vin">Risk's VIN </param>
       public ActionResult AddVehicleDrivers(Vehicle veh, string polId = null,int insurer = 0)
       {
           List<UserPermission> permissions = new List<UserPermission>();
           permissions.Add(new UserPermission("Policies", "Create"));
           permissions.Add(new UserPermission("Policies", "Update"));

           if (UserModel.TestUserPermissions(Constants.user, permissions))
           {
               veh.AuthorizedDrivers = VehicleModel.GetDriverList(veh.ID, int.Parse(polId), 1);
               veh.ExceptedDrivers = VehicleModel.GetDriverList(veh.ID, int.Parse(polId), 2);
               veh.ExcludedDrivers = VehicleModel.GetDriverList(veh.ID, int.Parse(polId), 3);
               ViewData["policy"] = polId;
               ViewData["insurer"] = insurer;
               ViewData["adw"] = AuthorizedDriverWordingModel.get_wording(int.Parse(polId), veh.ID).ID;

               //DEALING WITH THE MAIN DRIVER
               string trn = VehicleModel.GetMainDriver(veh.ID);
               Person per = PersonModel.GetPersonByTRN(trn);
               veh.mainDriver = new Driver (per);

               return View(veh);
           }
           else return RedirectToAction("Index");
       }

       /// <summary>
       /// This function deals with the deletion of drivers from a Risk (from jquery dialog)
       /// </summary>
       /// <param name="id">Risk's id </param>
       public ActionResult deleteDrivers(string chassis = null, string type = null, string perId1 = null, string pol = null)
       {
           List<UserPermission> permissions = new List<UserPermission>();
           permissions.Add(new UserPermission("Policies", "Create"));
           permissions.Add(new UserPermission("Policies", "Update"));


           if (UserModel.TestUserPermissions(Constants.user, permissions))
           {
               if (chassis != null && type != null && perId1 != null) { VehicleModel.deleteDrivers(chassis, perId1, type, pol); return Json(new { result = 1 }, JsonRequestBehavior.AllowGet); }
               else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
           }
           else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
       }

       /// <summary>
       /// This function changes the mainDriver for a vehicle
       /// </summary>
       /// <param name="vehID">Risk's id </param>
       /// /// <param name="trn">Person TRN</param>
       public ActionResult changeMainDriver(int vehID, string trn)
       {
           if ((UserModel.TestUserPermissions(Constants.user, new UserPermission("People", "Read")) || UserModel.TestUserPermissions(Constants.user, new UserPermission("PolicyHolder", "Read"))))
           {
               if (vehID != 0 && trn != "")
                   VehicleModel.UpdateMainDriver(vehID,trn);

              return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
           }

           return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
       }


       /// <summary>
       /// This function deals with the addition of the relation of drivers to the main driver , of a Risk
       /// </summary>
       public ActionResult AddRelation(string chassis = null, string per = null, string rel = null, string pol = null)
       {
           List<UserPermission> permissions = new List<UserPermission>();
           permissions.Add(new UserPermission("Policies", "Create"));
           permissions.Add(new UserPermission("Policies", "Update"));

           if (UserModel.TestUserPermissions(Constants.user, permissions))
           {
               VehicleModel.AddDriverRelation(chassis, int.Parse(per), rel, int.Parse(pol));

               return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
           }
           return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
       }


       /// <summary>
       /// This function cancels any association of a particular Risk, to any policy it is under, under a particular timeframe
       /// </summary>
       /// <param name="id">Risk's id </param>
       public ActionResult CancelRiskToPolicy(int vehId = 0, int polid = 0, string startDate = null, string endDate = null)
       {
           if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Update")) && vehId != 0)
           {
               VehicleModel.CancelVehicleToPolicyAssoc(vehId, polid, DateTime.Parse(startDate), DateTime.Parse(endDate));
                return Json(new { result = 1, date = 1, perm = 1 }, JsonRequestBehavior.AllowGet);
           }
           else return Json(new { result = 0, perm = 0 }, JsonRequestBehavior.AllowGet);
       }


       /// <summary>
       /// This function checks if a Risk is under a particular policy
       /// </summary>
       public ActionResult isVehUnderThisPol(string polId = null, string vehId = null)
       {
           if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Update")))
           {
               if (VehicleModel.isVehUnderThisPolicy(int.Parse(polId), int.Parse(vehId))) return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
               else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
           }
            return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
       }


       /// <summary>
       /// This function retrieves the usasge of a Risk under a particular policy
       /// </summary>
       public ActionResult getVehiclePolicyUsage(string vehID = null, string polID = null)
       {
           if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Policies", "Update")))
           {
               Policy policy = new Policy();

               if (polID == null) policy = VehicleCertificateModel.PullVehiclePolData(int.Parse(vehID),CompanyModel.GetMyCompany(Constants.user).CompanyID).policy;
               else policy = PolicyModel.getPolicyFromID(int.Parse(polID));

               int policyCoverID = 0;
               int usage = 0;
               string polNo = "";
               
               if (policy != null)
               {
                   usage = VehicleModel.GetUsage(int.Parse(vehID), policy.ID);
                   policyCoverID = policy.policyCover.ID;
                   polID = policy.ID.ToString();
                   polNo = policy.policyNumber.ToString();
               }
               return Json(new { result = 1, vehicle= vehID, use = usage, cover = policyCoverID, polId = polID, polNo = polNo  }, JsonRequestBehavior.AllowGet);
           }
           return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
       }

       /// <summary>
       /// This function returns a list of vehicle makes
       /// used by JavaScript for autocomplete
       /// </summary>
       /// <param name="make">Vehicle make</param>
       public ActionResult GetMakes(string make)
       {
           List<string> result = new List<string>();
           if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read"))) result = VehicleModel.GetVehicleMakes();

           var json = result.Where(x => x.ToLower().StartsWith(make.ToLower()))
                            .OrderBy(x => x)
                            .Select(r => new { value = r });

           return Json(json, JsonRequestBehavior.AllowGet);
       }

       /// <summary>
       /// This function returns a list of vehicle models
       /// used by JavaScript for autocomplete
       /// </summary>
       /// <param name="make">Vehicle make</param>
       public ActionResult GetModels(string model)
       {
           List<string> result = new List<string>();
           if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read"))) result = VehicleModel.GetVehicleModels();

           var json = result.Where(x => x.ToLower().StartsWith(model.ToLower()))
                            .OrderBy(x => x)
                            .Select(r => new { value = r });

           return Json(json, JsonRequestBehavior.AllowGet);
       }


       /// <summary>
       /// This function returns a list of vehicle model types
       /// used by JavaScript for autocomplete
       /// </summary>
       /// <param name="make">Vehicle make</param>
       public ActionResult GetModelType(string modelType)
       {
           List<string> result = new List<string>();
           if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read"))) result = VehicleModel.GetVehicleModelTypes();

           var json = result.Where(x => x.ToLower().StartsWith(modelType.ToLower()))
                            .OrderBy(x => x)
                            .Select(r => new { value = r });

           return Json(json, JsonRequestBehavior.AllowGet);
       }

       /// <summary>
       /// This function returns a list of vehicle body types
       /// used by JavaScript for autocomplete
       /// </summary>
       /// <param name="make">Vehicle make</param>
       public ActionResult GetBodyType(string bodyType)
       {
           List<string> result = new List<string>();
           if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read"))) result = VehicleModel.GetVehicleBodyTypes();

           var json = result.Where(x => x.ToLower().StartsWith(bodyType.ToLower()))
                            .OrderBy(x => x)
                            .Select(r => new { value = r });

           return Json(json, JsonRequestBehavior.AllowGet);
       }

       /// <summary>
       /// This function deals with getting Vehicle Cover Notes and Certificate List
       /// </summary>
       /// 
       public ActionResult VehicleCertsNotesList()
       {
           List<CertCover> VehicleCoverCert = CertCoverModel.GetCertCovers();
            return View(VehicleCoverCert);
       }


       /// <summary>
       /// This function gets vehicles that are insured by the company in question and other companies (double insured vehicles for the company)
       /// </summary>
       /// 
       public ActionResult DoubleInsuredVehicles()
       {
           if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read")))
               return View(VehicleModel.GetDoubleInsuredVehicles(Constants.user.employee.company.CompanyID));
           else
               return RedirectToAction("Index", "Home");
       }

       public ActionResult VehiclePolicyTest(int vehID, DateTime start, DateTime end)
       {
           if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Vehicles", "Read")))
           {
               if (VehicleModel.isVehUnderActivePolicy(vehID, start, end) && !(CompanyModel.CanAutoCancelVehiclePolicy(PolicyModel.GetPolicyCoveredUnder(vehID, start, end).insuredby.CompanyID)))
               {
                   return Json(new { returnData = 1, result = 1,  }, JsonRequestBehavior.AllowGet);
               }
               else if(VehicleModel.isVehUnderActivePolicy(vehID, start, end) && CompanyModel.CanAutoCancelVehiclePolicy(PolicyModel.GetPolicyCoveredUnder(vehID, start, end).insuredby.CompanyID))
               {
                  VehicleModel.CancelVehicleUnderPolicy(vehID, 0, start, end);
                  return Json(new {returnData = 0,result = 1},JsonRequestBehavior.AllowGet);
                  //PolicyModel.RiskDoubleInsuredEmail(policy.vehicles[x].ID, policy,true);
               }
               else return Json(new { returnData = 1, result = 0, }, JsonRequestBehavior.AllowGet);
           }
           else return Json(new { returnData = 0 },JsonRequestBehavior.AllowGet);
       }

       public ActionResult TestVin(int id = 0, string vin = null)
       {
           if (vin != null)
           {
              int result = 0,vehicle = 0;
               result = VehicleModel.TestVehicleVIN(vin,id);
               if (result != 0) vehicle = 1;
               return Json(new { result = 0, error = 0,vehicle = vehicle,vid = result }, JsonRequestBehavior.AllowGet);
           }
           else return Json(new { result = 0, error = 1 }, JsonRequestBehavior.AllowGet);
       }

       public ActionResult TestChassis(int id = 0, string chassis = null)
       {
           if (chassis != null)
           {
               return Json(new { result = 0, bad_data = 0 }, JsonRequestBehavior.AllowGet);
           }
           else return Json(new { result = 0, bad_data = 1 }, JsonRequestBehavior.AllowGet);
       }

       

    }
}
