﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class DataMappingController : Controller
    {
        //
        // GET: /DataMapping/

        public ActionResult AuditMappingData()
        {
            return View();
        }

        public ActionResult Index()
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Mapping", "Read"))) return View(MappingDataModel.GetAllMappingData());
            else return RedirectToAction("AccessDenied", "Error");
        }

        public ActionResult UpdateMapDataParish(int id = 0, int parish = 0, string cdata = null)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Mapping", "Update")))
            {
                if (id != 0 && (parish != 0 || cdata != null))
                {
                    MappingData map = new MappingData();
                    map.id = id;
                    if (parish == 0)
                    {
                        map.table = "wordings";
                        map.correct_data = cdata; 
                    }
                    if (cdata == null)
                    {
                        map.table = "parish";
                        map.refid = parish;
                    }
                    
                    return Json(new { result = MappingDataModel.UpdateMappingData(map) }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ToggleLock(int id = 0, bool maplock = false)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Mapping", "Lock")))
            {
                MappingData map = new MappingData(id);
                if (maplock) MappingDataModel.LockMappingData(map);
                else MappingDataModel.UnLockMappingData(map);
                return Json(new { result = true }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }

    }
}
