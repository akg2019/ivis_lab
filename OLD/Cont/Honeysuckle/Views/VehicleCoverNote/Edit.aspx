﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.VehicleCoverNote>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit Cover Note
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div id= "form-header">  <img src="../../Content/more_images/Cover_Note.png" />
     <div class="form-text-links">
        <span class="form-header-text"> Edit Cover Note</span>
        <div class="icon">
              <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
        </div>
     </div>
 </div> 
    <div class="field-data">

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            
                <%: Html.HiddenFor(model => model.ID, new { @class = "dont-process" })%>
                <%: Html.Hidden("systemdate", Model.effectivedate.ToString("MM/dd/yyyy hh:mm:ss tt"), new { id = "sysdate", @class = "dont-process" })%>
                <%: Html.HiddenFor(model => model.approved, new { @class = "vcn_approved dont-process" })%>
                <%: Html.HiddenFor(model => model.cancelled, new { @class = "vcn_cancelled dont-process" })%>
                <%: Html.HiddenFor(model => model.Policy.ID, new { id = "policyID", @class = "dont-process" })%>
                <%: Html.Hidden("VCedit","edit",new{ id = "VCpage"}) %>

                <% if (ViewData["approved"] != null) { %> <%: Html.Hidden("approvalerror", "test", new { @class = "dont-process" })%> <% } %>
                
                <div class="validate-section">
                    <div class="subheader">
                         <div class="arrow right down"></div>
                         Vehicle Information 
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="editor-label">
                            Chassis No. 
                        </div>
                    
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.risk.chassisno, new { @readonly = "readonly", @class = "read-only" })%>
                            <%: Html.ValidationMessageFor(model => model.risk.chassisno)%>
                        </div>

                    </div> <!-- end of first data section -->
                </div> <!-- end of first validate section -->

                <div class="validate-section">
                    <div class="subheader">
                         <div class="arrow right"></div>
                         Cover Note Information
                         <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="editor-label">
                            Manual Cover Note No.
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.covernoteno) %>
                            <%: Html.ValidationMessageFor(model => model.covernoteno) %>
                        </div>

                        <div class="editor-label">
                            Alterations 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.alterations) %>
                            <%: Html.ValidationMessageFor(model => model.alterations) %>
                        </div>

                        <div class="editor-label">
                            Cover Note Wording Template:
                        </div>
            
                        <div class="editor-field">
                        <%: Html.HiddenFor(model => model.wording.ID, new { id = "wordingID", @class = "dont-process" })%>
                        <% Html.RenderAction("TemplateDropDown", "TemplateWording", new{id= Model.risk.ID, polId = Model.Policy.ID}); %> 
                    </div>

                        <% if (Model.cancelled){  %>
                        <div class="editor-label">
                            Cancelled
                        </div>
                        <div class="editor-field">
                            <%: Html.CheckBoxFor(model => model.cancelled) %>
                        </div>
            
                        <div class="editor-label">
                            Cancelled On
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.cancelledon, new { @readonly = "readonly"})%>
                            <%: Html.ValidationMessageFor(model => model.cancelledon) %>
                        </div>
                        <% } %>

                        <%
                            string firstprint;
                            if (Model.firstprinted != new DateTime()) firstprint = Model.firstprinted.ToString("dd/MM/yyyy hh:mm tt");
                            else firstprint = "";
                        %>

                        <%
                            string lastprint;
                            if (Model.lastprinted != new DateTime()) lastprint = Model.lastprinted.ToString("dd/MM/yyyy hh:mm tt");
                            else lastprint = "";
                        %>
        
                        <div class="editor-label">
                            Date First Printed
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.firstprinted, new { @readonly = "readonly", @class = "read-only firstprinted", Value = firstprint })%>
                            <%: Html.ValidationMessageFor(model => model.firstprinted) %>
                        </div>
            
                        <div class="editor-label">
                            Date Last Printed
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.lastprinted, new { @readonly = "readonly", @class = "read-only", Value = lastprint })%>
                            <%: Html.ValidationMessageFor(model => model.lastprinted) %>
                        </div>
            
                        <div class="editor-label">
                            Cover Note Number
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.covernoteid) %>
                            <%: Html.ValidationMessageFor(model => model.covernoteid) %>
                        </div>

                        <div class="editor-label">
                            Effective Date
                        </div>
                        <div class="editor-field">
                            <%: Html.Hidden("edit_effectivedate", Model.effectivedate.ToString("MM/dd/yyyy hh:mm:ss tt"), new { id = "edit_hidden_effectivedate", @class = "dont-process" })%>
                            <%: Html.TextBoxFor(model => model.effectivedate, new { id = "effectivedate", @required = "required", @readonly = "readonly", @class = "datetime" })%>
                            <%: Html.ValidationMessageFor(model => model.effectivedate) %>
                        </div>
                    
                        <div class="editor-label">
                            Period
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.period, new { @required = "required", id = "period" }) %>
                            <button type="button" class="quickperiod submitbutton" value="7">7</button>
                            <button type="button" class="quickperiod submitbutton" value="14">14</button>
                            <button type="button" class="quickperiod submitbutton" value="30">30</button>
                            <%: Html.ValidationMessageFor(model => model.period) %>
                        
                        </div>

                        <div class="editor-label">
                            Expiry Date
                        </div>
                        <div class="editor-field">
                        <%: Html.Hidden("edit_expirydate", Model.expirydate.ToString("MM/dd/yyyy hh:mm:ss tt"), new { id = "edit_hidden_expirydate", @class = "dont-process" })%>
                            <%: Html.TextBoxFor(model => model.expirydate, new { id = "expirydate", @required = "required", @readonly = "readonly", @class = "datetime" })%>
                            <%: Html.ValidationMessageFor(model => model.expirydate)%>
                        </div>

                      

                   </div> <!-- end of first data section -->
             </div> <!-- end of first validate section -->

               <div class="btnlinks wider">
                  <%//: Html.ActionLink("Back to Home", "Index", "Home", null, new { @class = "link" })%>
                  <% Html.RenderAction("BackBtn", "Back"); %>
                  <input type="submit" value="Save Cover Note" class="submitbutton" />
               </div>

        </fieldset>

    <% } %>

      </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript"></script>
    <link href="../../Scripts/datetimepicker-master/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/vehiclecovernote-create.js" type="text/javascript"></script>
    <link href="../../Content/vehiclecovernote-create.css" rel="stylesheet" type="text/css" />
</asp:Content>

