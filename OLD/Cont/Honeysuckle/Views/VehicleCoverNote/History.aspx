﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.VehicleCoverNote>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	History
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="page-content">
        <div id="page-title"><img src="../../Content/more_images/Cover_Note.png" />
            <div class="form-text-links">
                <span class="form-header-text">Cover Note History</span>
            </div>
        </div>

        <div class="field-data history-page contentwidth">
            <div class="vehicle-data"></div>
            <div class="policy-data"></div>
            <% Html.RenderAction("HistoryPartial", "VehicleCoverNote", new { id = Model.risk.ID, historyPage = "main", polid = Model.Policy.ID }); %>
        <div class="btnlinks">
            <% Html.RenderAction("BackBtn", "Back"); %>
        </div>
    </div>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/covernote-history.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/coverHistory.js" type="text/javascript"></script>
</asp:Content>


