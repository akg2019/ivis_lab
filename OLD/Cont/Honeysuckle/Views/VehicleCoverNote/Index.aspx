﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.VehicleCoverNote>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page">
        <div id="form-header">
            <img src="../../Content/more_images/Cover_Note.png" />
            <div class="form-text-links">
                <span class="form-header-text">
                    Cover Notes Effective Now List
                </span>
            
            <div  class="search-box inter-search">
                <%= Html.TextBox("search-inter", null, new { @PlaceHolder = "Search [Cover Notes]", @autocomplete = "off", @class = "quick-search cover-note" }) %>
                <button type="button" class="quick-search-btn user-search-btn" onclick="clickSearchBtn()" ><img src="../../Content/more_images/Search_Icon.png" /></button>
            </div>
            </div>
        </div>
    
        <div class="field-data">
            <div class="ivis-table">
                <div class="menu">
                    <div class="item cover_no">
                        Cover Note No
                    </div>
                    <div class="effective item">
                        Effective Date
                    </div>
                    <div class="expiry item">
                        Expiry Date
                    </div>
                    <div class="item insurer">
                        Insurer
                    </div>
                    <div class="item broker">
                        Broker/Agent
                    </div>
                </div>
                <div class="scroll-table">
                    <% for(int i = 0; i < Model.Count(); i++){ %>
                        <% if (i % 2 == 0) { %> 
                            <div class="row even">
                        <%} %>
                        <% else { %>
                            <div class="row odd">
                        <%} %>
                            <div class="item cover_no">
                                <%: Model.ElementAt(i).covernoteno %>
                            </div>
                            <div class="effective item">
                                <%: Model.ElementAt(i).effectivedate.ToString("dd MMM yyyy")%>
                            </div>
                            <div class="expiry item">
                                <%: Model.ElementAt(i).expirydate.ToString("dd MMM yyyy")%>
                            </div>
                            <div class="item insurer">
                                <%: Model.ElementAt(i).Policy.insuredby.CompanyName %>
                            </div>
                            <div class="item broker">
                                <%: Model.ElementAt(i).Policy.compCreatedBy.CompanyName %>
                            </div>
                        </div>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/vehiclecovernote.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

