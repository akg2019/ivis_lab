﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.CertCover>>" %>

<%  
    bool cover_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Read"));
    bool cert_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Read"));
%>
<div id="covercertstab" class="note-segment">
    <div id="coverlists">
        <div id="coverscroll">
            <% foreach (var item in Model) { %>
                <% if((item.Type == "Cover" && cover_read) || (item.Type == "Certificate" && cert_read)) Html.RenderPartial("~/Views/Notification/ShowCoverCertNotification.ascx", item); %>
            <% } %>
        </div> 
    </div>
    <div id="seeallcover"><a href="/#container-covercerts">See List</a> </div>
    <div class="notify-borders"></div>
</div>