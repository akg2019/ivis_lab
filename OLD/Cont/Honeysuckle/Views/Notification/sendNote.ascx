﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Notification>" %>

 <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
    <div id="newnote">
        <% using(Html.BeginForm("send")) { %>
            
            <%: Html.Hidden("vehId", ViewData["vehicle"], new { id = "veh", @class = "dont-process" })%>
            <%: Html.Hidden("vehInformation", ViewData["vehicleInfo"], new { id = "vehInfo", @class = "dont-process" })%>
            <div id="to">
                <div class="NoteLabel">
                   To: 
                </div>
                <div class="NoteField">
                    <select class = "sendtobox" id="sendto" data-placeholder="Select Recipients" multiple></select>
                </div>
            </div> 
            <div id="subject">
                <div class="NoteLabel">
                    Subject: 
                </div>
                <div class="NoteField">
                    <%: Html.TextBoxFor(model => Model.heading, new { id = "heading", @class = "DataField read-only" })%>
                </div>
            </div> 
            <div id="notificaton">
                <div class="NoteLabel">
                    Content: 
                </div>
                <div class="NoteField">
                    <%: Html.EditorFor(model => Model.content, new {id = "note", @class= "DataField"}) %>
                </div>
            </div>
        <% } %>
    </div>
    
    <!-- scripts for message content for links and such for approval of certificates and cover notes -->
    <% if (ViewData["type"].ToString() == "Certificate")  { %> <script src="../../Scripts/ApplicationJS/ApproveCertificatePrinting.js" type="text/javascript"></script> <% } %>
    <% else if (ViewData["type"].ToString() == "CoverNote")  { %>  <script src="../../Scripts/ApplicationJS/ApproveCoverNotePrinting.js" type="text/javascript"></script> <% } %>
    <% else if (ViewData["type"].ToString() == "ApproveCert")  { %>  <script src="../../Scripts/ApplicationJS/ApproveCertificateRequest.js" type="text/javascript"></script> <% } %>
    <% else if (ViewData["type"].ToString() == "ApproveCoverNote")  { %>  <script src="../../Scripts/ApplicationJS/ApproveCoverNoteRequest.js" type="text/javascript"></script> <% } %>
    <% else { %> <script src="../../Scripts/ApplicationJS/sendToAdmins.js" type="text/javascript"></script> <% } %>

 <% } %>

