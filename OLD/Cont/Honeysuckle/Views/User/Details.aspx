﻿<%@ Page Title="User Details" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.User>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <fieldset>
        
        <div id= "form-header"><img src="../../Content/more_images/User.png" alt=""/>
            <div class="form-text-links">
                <span class="form-header-text">User Details</span>
                <div class="menu-click">
                   <div class="icon">
                        <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                   </div>
                    <!--<button id="auditlink">AUDIT</button>-->
                     <% if ((Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user) || Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user)
                             || Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "ResetPassword"))) && Model.active) { %> 
                            <ul id="menu-links"> 
                                <li>
                                    <span class="menu-links-heading">User Links</span>
                                    <ul>
                                        <li>
                                            <a href="#" onclick="resetPassword();">Reset Password To User</a> <br />
                                        </li>
                                        <li>
                                            <a href="#" onclick="resetQuest();">Reset Password and Question</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                      <% } %>
                  </div>
           </div>
        </div>
        
        <div class="field-data"> 
            
            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right down"></div>
                    Main User Data
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <%: Html.HiddenFor(model => model.ID, new { @class = "hiddenid dont-process" }) %>
                    <div class ="editor-label">
                        Username
                    </div>
                    <div class ="editor-field">  
                        <%: Html.TextBoxFor(model => model.username, new { @class = "read-only", @readonly = "readonly" })%> 
                    </div> 
                </div>
            </div> 

            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right"></div>
                    Company
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <% Html.RenderAction("EmployeeCompany", "Company", new { compId =Model.employee.company.CompanyID }); %>
                </div>
            </div>
            
            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right"></div>
                    Personal Data
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div class="editor-label">
                        TRN: 
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.employee.person.TRN, new { @class = "read-only", @readonly = "readonly", id = "trn", @PlaceHolder = " Eg. 0888776655" })%><br />
                        <%: Html.ValidationMessageFor(model => model.employee.person.TRN)%>
                    </div>
           
                    <div class ="editor-label">
                        First Name:
                    </div>
                    <div class ="editor-field">  
                        <%: Html.TextBoxFor(model => model.employee.person.fname, new { @class = "read-only", @readonly = "readonly" })%> 
                    </div> 
           
                    <div class ="editor-label">
                        Middle Name:
                    </div>
                    <div class ="editor-field">  
                        <%: Html.TextBoxFor(model => model.employee.person.mname, new { @class = "read-only", @readonly = "readonly" })%> 
                    </div> 
           
                    <div class ="editor-label">
                        Last Name:
                    </div>
                    <div class ="editor-field">  
                        <%: Html.TextBoxFor(model => model.employee.person.lname, new { @class = "read-only", @readonly = "readonly" })%> 
                    </div>
                </div>
            </div>
           
            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right"></div>
                    Address
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div class ="editor-label">
                        Road:
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.employee.person.address.roadnumber, new { id = "RoadNumber", @class = "read-only", @readonly = "readonly" })%>
                        <%: Html.TextBoxFor(model => model.employee.person.address.road.RoadName, new { id = "roadname", @class = "read-only", @readonly = "readonly" })%>
                        <%: Html.TextBoxFor(model => model.employee.person.address.roadtype.RoadTypeName, new { id = "roadtype", @class = "read-only", @readonly = "readonly" })%><br />
                    </div>

                    <div class ="editor-label">
                        Town/City:
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.employee.person.address.city.CityName , new { @class = "read-only", @readonly = "readonly" })%>
                    </div>

                    <div class ="editor-label">
                        Parish:
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.employee.person.address.parish.parish , new { @class = "read-only", @readonly = "readonly" })%>
                    </div>

                    <div class ="editor-label">
                        Country:
                    </div>
                    <div class ="editor-field">
                        <%: Html.TextBoxFor(model => model.employee.person.address.country.CountryName , new { @class = "read-only", @readonly = "readonly" })%>
                    </div>
                </div>
            </div>
    
            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right"></div>
                    Additional Emails
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div id="email-container">
                        <% if (Model.employee.person.emails != null) { %>
                            <% foreach (Honeysuckle.Models.Email email in Model.employee.person.emails) { %>
                                <% Html.RenderPartial("~/Views/Email/EmailView.ascx", email); %><br />
                            <% } %>
                        <% } %>
                    </div>
                </div>
            </div>

            <div class="validate-section">
                <div class="subheader">
                    <div class="arrow right"></div>
                    Phone Numbers
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div class="phones">
                        <% if (Model.employee.person.phoneNums != null) { %>
                             <% foreach (Honeysuckle.Models.Phone phone in Model.employee.person.phoneNums) { %>
                                        <% Html.RenderPartial("~/Views/Phone/NewPhone.ascx", phone, new ViewDataDictionary{{"Details",ViewData["Details"]}}); %> <br />
                                    <% } %>
                          <% } %>
                    </div>
                </div>
            </div>

            
 
            <div class="btnlinks">
                <% Html.RenderAction("BackBtn", "Back"); %>
                <% if ((Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users","Update")) 
                     && Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID == Model.employee.company.CompanyID)
                     || (Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user) || Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user)))
                    { %>
                      <button class="editButton">Edit</button>
                 <% } %>
            </div>
        </div>
        
         

    </fieldset>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/links.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/email.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/userManagement.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/user-active-deactive.js" type="text/javascript"></script>
</asp:Content>




