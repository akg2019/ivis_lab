﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.User>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	AddUserRole
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page">
        
        <div id="form-header"><img src="../../Content/more_images/User.png" />
              <div class="form-text-links">
                <span class="form-header-text">Manage Roles For User</span>
            </div>
        </div>

        <%: Html.Hidden("page", "adduser", new { @class = "dont-process" }) %>
        
        <% using (Html.BeginForm("AddUserRoles","User",FormMethod.Post)) {%>
            <%: Html.ValidationSummary(true) %>
        
            <fieldset>
                <%: Html.HiddenFor(m => Model.ID, new { @class = "dont-process userid" })%>
                <div class="field-data">
                    <div class="validate-section">
                        <div class="subheader">
                            <div class="arrow right down"></div>
                            Main User Data
                            <div class="valid"></div>
                        </div>
                        <div class="data">
                            <div class="editor-label userGroupName">
                                Username
                            </div>
                            <div class="editor-field">
                                <%: Html.HiddenFor(model => model.username, new { @class = "dont-process" })%>
                                <%: Html.TextBoxFor(model => model.username, new { @class = "read-only", @readonly = "readonly" })%>
                            </div>
                            <div class="editor-label userGroupName">
                                Company Name
                            </div>
                            <div class="editor-field">
                                <%: Html.HiddenFor(model => model.employee.company.CompanyName, new { @class = "dont-process" })%>
                                <%: Html.TextBoxFor(model => model.employee.company.CompanyName, new { @class = "read-only", @readonly = "readonly" })%>
                            </div>
                        </div>
                    </div>
                    <div class="validate-section permission-header">
                        <div class="subheader">
                            <div class="arrow right"></div>
                            Roles
                            <div class="valid"></div>
                            <div class="server-responses"></div>
                        </div>
                        <div class="lists data">
                            <div class="modify">
                                <div class="group not">
                                    <div class="title">Roles Not Assigned</div>
                                    <div class="search-section">
                                        <input placeholder="eg. group" class="searching not assign-group quick-search" autocomplete="off" />
                                        <button type="button" class="quick-search-btn" onclick="clickSearchBtn(this)"><img src="../../Content/more_images/Search_Icon.png"></button>
                                    </div>
                                    <% Html.RenderAction("ListBoxUserGroups", "UserGroup", new { id = Model.ID }); %>
                                </div>
                                <div class="move-buttons">
                                    <button type="button" class="add-button" onclick="addRole()" >>></button>
                                    <button type="button" class="rm-button" onclick="rmRole()" ><<</button>
                                    <!--
                                    <input type="submit" value=">>" name="submit" class="m-button submitbutton" />
                                    <input type="submit" value="<<" name="submit" class="m-button submitbutton" />
                                    -->
                                </div>
                                <div class="group is">
                                    <div class="title">Roles Assigned</div>
                                    <div class="search-section">
                                        <input placeholder="eg. group" class="searching is assign-group quick-search" autocomplete="off" />
                                        <button type="button" class="quick-search-btn" onclick="clickSearchBtn(this)"><img src="../../Content/more_images/Search_Icon.png"></button>
                                    </div>
                                    <% Html.RenderAction("MyListBoxUserGroups", "UserGroup", new { id = Model.ID }); %>
                                </div>
                            </div>
                        </div>
                    </div>
            
                    <div class="btnlinks">
                        <% Html.RenderAction("BackBtn", "Back"); %>
                    </div>
                </div>
            </fieldset>

        <% } %>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <!--<link href="../../Content/adduserroles.css" rel="stylesheet" type="text/css" />-->
    <script src="../../Scripts/ApplicationJS/rolemanagement.js" type="text/javascript"></script>
    <link href="../../Content/rolemanagement.css" rel="stylesheet" type="text/css" />
</asp:Content>



