﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.User>" %>

<div class="editor-label">
    <%: Html.LabelFor(model => model.username) %>
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.username) %>
</div>

<div class="editor-label">
    <%: Html.LabelFor(model => model.reset) %>
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.reset) %>
</div>

<div class="editor-label">
    <%: Html.LabelFor(model => model.faultylogins) %>
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.faultylogins) %>
</div>
            
<div class="editor-label">
    <%: Html.LabelFor(model => model.lastloginattempt) %>
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.lastloginattempt) %>
</div>
            
<div class="editor-label">
    <%: Html.LabelFor(model => model.passwordreset) %>
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.passwordreset) %>
</div>
            
<div class="editor-label">
    <%: Html.LabelFor(model => model.active) %>
</div>
<div class="editor-field">
    <%: Html.CheckBoxFor(model => model.active) %>
</div>
       
<% Html.RenderPartial("~/Views/Employee/AuditEmployee.ascx", Model.employee); %>
            
