﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.response>>" %>

<%
    List<Honeysuckle.Models.response> success = Honeysuckle.Models.response.gettype(Model.ToList(), "success");
    List<Honeysuckle.Models.response> errors = Honeysuckle.Models.response.gettype(Model.ToList(), "error");    
%>

<div id="response">
    <div class="stripes"></div>
    <% if (Model.Count() == 0) { %> <div class="message empty">There was no response from the server. Maybe you had no content in the file you uploaded.</div> <%} %>
    <% else { %>
        <div class="message empty">Your file has been processed and here are the relevant responses from the server.</div>
        <div class="responses">
            <div class="title"><div>There were <%: errors.Count() %> </div><div class="error">errors</div></div>
            <div class="data">
                <% for (int i = 0; i < errors.Count(); i++) { %>
                    <div class="message <%: errors.ElementAt(i).type %>"><%: errors.ElementAt(i).message %></div>
                <% } %>
            </div>
        </div>
        <div class="responses">
            <div class="title"><div>There were <%: success.Count() %> </div><div class="success">successes</div></div>
            <div class="data">
                <% for (int i = 0; i < success.Count(); i++) { %>
                    <div class="message <%: success.ElementAt(i).type %>"><%: success.ElementAt(i).message%></div>
                <% } %>
            </div>
        </div>
    <% } %>
</div>