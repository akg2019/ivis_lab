﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Company>>" %>

<%: Html.DropDownList("interCompanies", new SelectList(Model.ToList(), "CompanyID", "CompanyName"), new { @class = "inter-dropdown",id="inter-Drop-Down", @required = "required"})%>

