﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Audit>" %>

<div class="location"><% ViewData["location"].ToString(); %></div>

<div class="dataLabel" style="font-size: 1.5em;font-family: Arial, Helvetica, sans-serif;">Old Data</div>
<%: Html.TextArea("olddata", Model.oldObjectData.ToString() ,new{ @class="olddata dont-process" }) %>
<div class="dataLabel" style="font-size: 1.5em;font-family: Arial, Helvetica, sans-serif;">New Data</div>
<%: Html.TextArea("newdata", Model.newObjectData.ToString() ,new{ @class="newdata dont-process" }) %>

