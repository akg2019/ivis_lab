﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Audit>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Audit Log
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id= "form-header"><img src="../../Content/more_images/Cover_Note.png" />
    <div class="form-text-links">
        <span class="form-header-text">Audit Log</span> 
        <div class="icon">
              <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
        </div>
    </div>
  </div>
  <div id="logModal"></div>
  <div class="field-data">
    <div id="certificates" class="data-result">
            <div class="data-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Certificate.png" /><span class="category">Certificates:</span><span class="count"></span></div>
            <div id="data-results-certificates" class="data-content"></div>
        </div>
    <div id="covernotes" class="data-result">
            <div class="data-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Certificate.png" /><span class="category">Covernotes:</span><span class="count"></span></div>
            <div id="data-results-covernotes" class="data-content"></div>
        </div>
    <div id="policies" class="data-result">
            <div class="data-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Certificate.png" /><span class="category">Policies:</span><span class="count"></span></div>
            <div id="data-results-policies" class="data-content"></div>
        </div>
        <!--
    <div id="companies" class="data-result">
            <div class="data-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Certificate.png" /><span class="category">Companies:</span><span class="count"></span></div>
            <div id="data-results-companies" class="data-content"></div>
    </div>
    <div id="people" class="data-result">
            <div class="data-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Certificate.png" /><span class="category">People:</span><span class="count"></span></div>
            <div id="data-results-people" class="data-content"></div>
    </div>
    <div id="vehicles" class="data-result">
            <div class="data-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Certificate.png" /><span class="category">Vehicles:</span><span class="count"></span></div>
            <div id="data-results-vehicles" class="data-content"></div>
    </div>
    -->
  </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/audit.js" type="text/javascript"></script>
    <link href="../../Content/auditLog.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
