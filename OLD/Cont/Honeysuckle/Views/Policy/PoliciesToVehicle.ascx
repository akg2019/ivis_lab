﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Policy>>" %>

<% bool canView = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Policies", "Read"));  %>
<div class="ivis-table">
    <div class="menu">
           <div class="policyNo item">Policy No</div>
           <div class="ins item">Insurer</div>
           <div class="startDate item">Start Date</div>
           <div class="endDate item">End Date</div>
     </div>
     <div id="policy-data">
        <% int i = 0; %>
        <% foreach (Honeysuckle.Models.Policy policy in Model) { %>
            <% if (i % 2 == 1) { %> <div class="policy data odd row"><% } %>
            <% else {  %> <div class="policy data even row"><% } %>
                <%: Html.HiddenFor(model => policy.ID, new { @class = "id dont-process"}) %>
                <div class="policyNo item"><%: policy.policyNumber %></div>
                <div class="ins item"><%: policy.insuredby.compshort %></div>
                <div class="startDate item"><%: policy.startDateTime.ToString("d MMM, yyyy")%></div>
                <div class="endDate item"><%: policy.endDateTime.ToString("d MMM, yyyy")%></div>
                <div class="item functions">
                    <% if(canView) { %>
                        <div class="icon view" title="View Policy">
                            <img src="../../Content/more_images/view_icon.png" class="sprite"/>
                        </div>
                    <% } %>
                </div>
            </div>
            <% i++; %>
       <% } %>
     </div>
</div>