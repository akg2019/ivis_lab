﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Policy>" %>

<% using (Html.BeginCollectionItem("policy")) {%>
    <%: Html.ValidationSummary(true) %>

    <% if(Model.ID != 0) { %>
        
        <%: Html.HiddenFor(model => model.ID, new { @class = "dont-process" })%>
        
        <div class="editor-label">
            Policy Number:
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => model.policyNumber, new { @readonly = "readonly", @class = "read-only" })%>
            <%: Html.ValidationMessageFor(model => model.policyNumber)%>
        </div>

        <div class="editor-label">
            Cover:
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => Model.policyCover.cover, new { @readonly = "readonly", @class = "read-only" })%>
            <%: Html.ValidationMessageFor(model => Model.policyCover.cover)%>
        </div>

    <% } %>       
            
<% } %>