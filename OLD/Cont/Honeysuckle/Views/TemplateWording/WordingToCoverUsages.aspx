﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.TemplateWording>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Wording- cover to usage
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm()) { %>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>

            <div id="form-header">
                <img src="../../Content/more_images/Wording_Header.png" />
                <div class="form-text-links">
                    <span class="form-header-text">Associate Wording to Covers-Usages</span>
                    <div class="icon">
                        <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                    </div>
                </div>
            </div>
            
            <div class="usages-manage"></div>
            
            <div class="field-data">

                <%: Html.HiddenFor(model => model.ID, new { id = "wordingID", @class = "dont-process" }) %>
                <%: Html.HiddenFor(model => model.company.CompanyID, new { @class = "hidden-company-id dont-process" }) %>
                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right down"></div>
                        Main Template Wording Data
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="editor-label">
                            Certificate ID:
                        </div> 
                        <div class="editor-field input">
                            <%: Html.TextBoxFor(model => model.CertificateID, new { id = "certid", @class = "read-only", @readonly = "readonly" })%>
                        </div>
                        <div class="editor-label">
                            Certificate Type:
                        </div> 
                        <div class="editor-field input">
                            <%: Html.TextBoxFor(model => model.CertificateType, new { id = "certtype", @class = "read-only", @readonly = "readonly" })%>
                        </div> 
                    </div>
                </div>

                <!-- ADD COVER USAGES HERE -->
                <div class="validate-section cover-usages">
                    <div class="subheader">
                        <div class="arrow right"></div>
                        Covers-Usages
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div id="usages">
                            <% if (Model.PolicyCovers != null) { %>
                                <% foreach(var item in Model.PolicyCovers) { %>
                                     <% Html.RenderAction("CoversUsagesSelect", "PolicyCover", new { CoverId = item.ID, UsageID = item.usages[0].ID }); %>
                                <% } %>
                            <% } %>
                        </div>

                        <div id="addusage" class="add-dynamic small wider">
                            <img src="../../Content/more_images/ivis_add_icon.png" />
                            <span>Add Cover-Usages</span>
                        </div>
                    </div>
                </div>
            
            </div>

            <div class="btnlinks coverUsagesWidth">
                <% Html.RenderAction("BackBtn", "Back"); %>
            </div>

        </fieldset>

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/covers.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/multipleCoverUsages.js" type="text/javascript"></script>
    <link href="../../Content/cover-usages.css" rel="stylesheet" type="text/css" />
</asp:Content>


