﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.TemplateImage>" %>

<% if (!(bool)ViewData["post"]) { %>
<div class="image-parent">
<% } %>
    <div class="image">
        <div class="details"><span><%: Model.position.Substring(0,1).ToUpper() + Model.position.Substring(1, Model.position.Length - 1) %></span></div>
        <% if (Model.ID != 0) { %>
            <%: Html.HiddenFor(model => model.ID, new { @class = "image_id dont-process" })%>
            <div class="image-div">
                <div class="image-overlay"></div>
                <img src="/files/<%: Model.location %>" />
                <div class="icon delete" title="Delete Image">
                    <img src="../../Content/more_images/rm-image.png" />
                </div>
            </div>
        <% } %>
    </div>
    <% if (!(bool)ViewData["post"]) { %>
        <% 
            string hide = "";
            if (Model.ID != 0) hide = "hide";
        %>
        <div class="fileupload-parent <%: hide %>">
            <div class="click-this-to-work">
                <span>+ Add File</span>
            </div>
            <input type="file" name='file' value="" accept="image/*" class='fileupload <%: Model.type.ToLower() + " " +  Model.position.ToLower() %>'  />
        </div>
    <% } %>
<% if (!(bool)ViewData["post"]) { %>
</div>
<% } %>

