﻿<%@ Page Title="Company Contact" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%: Model.CompanyName %> Contacts: 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <% using (Html.BeginForm()) { %>
        <%: Html.ValidationSummary(true) %>

     <fieldset>

      <h4>Company Contact Information: </h4>

        <%: Html.HiddenFor(model => model.CompanyID, new { id = "compID", @class = "dont-process" })%>

        <div id="CompanyUserContact">

         <div id="user-modal"></div>
         <div id = "UsercontactHeader">ADD Company's Users contact Information <input type="button" value="Add New" id="addUserContact"/></div>
         <br />

         <div id="contact"></div>

        </div>
        <br /> <br /><br /><br />
            <p>
                <input type="submit" value="Done" id="Submit2"/>
            </p>

      </fieldset>
       <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
  <script src="../../Scripts/ApplicationJS/companyContacts.js" type="text/javascript"></script>
  <link href="../../Content/Pages.css" rel="stylesheet" type="text/css" />
  <link href="../../Content/CompanyContact.css" rel="stylesheet" type="text/css" />
</asp:Content>




