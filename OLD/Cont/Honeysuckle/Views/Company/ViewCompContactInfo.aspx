﻿<%@ Page Title="Contact Information" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Company Contact Information
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% 
        //Testing if the curent user has a temp role
        bool allow = false;
        if (Honeysuckle.Models.Constants.user.newRoleMode)
        {
            if (Honeysuckle.Models.Constants.user.tempUserGroup.company.CompanyID == Model.CompanyID) allow = true;
        }
    %>
    <% using (Html.BeginForm()) { %>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <div id= "form-header">  
                <img src="../../Content/more_images/Contact.png" alt=""/> 
                <div class="form-text-links">
                    <span class="form-header-text">Company Contact Information</span>
                </div>
                <ul id="menu-links">
                   <li>
                        <span class="menu-links-heading">Company</span>
                        <ul>
                            <li>
                                <%: Html.ActionLink("Company Details", "Details", new { id = Model.CompanyID })%>
                            </li>
                            <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company","EditInfo")) && (Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID == Model.CompanyID || allow)) 
                                { %> 
                                <li>
                                    <%: Html.ActionLink("Edit", "EditCompContact", new { id = Model.CompanyID })%> 
                                </li>
                            <%  } %>
                        </ul>
                    </li>
                </ul>
            </div> 
            <div class="field-data">
                

                <%: Html.HiddenFor(model => model.CompanyID, new { @class = "dont-process" })%>
                <% if (Model.emails != null)
                    { %>
                    <% for (int i = 0; i < Model.emails.Count(); i++) 
                        { %>
                        <div class = "items">
                            <div class="subheader"><img src="../../Content/more_images/User.png" alt=""/><span>User Contact</span></div>
                            <% Html.RenderAction("UserInfo", "User", new { username = Model.emails.ElementAt(i).email }); %>
                            <% Html.RenderAction("EditPhone", "Phone", new { phone = Model.phoneNums.ElementAt(i), type="View" }); %> 
                        </div>
                    <% } %>
                <% } %>
                  <% if (Model.emails.Count() == 0 )
                     { %>
                       <div id="NoContacts">  There is no contact information for this company at this time. </div>
                  <% } %>
              

                <div class="btnlinks">
                    <% Html.RenderAction("BackBtn", "Back"); %>
                </div>
            </div>
          

        </fieldset>
    
    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/CompanyContact.css" rel="stylesheet" type="text/css" />
</asp:Content>

 
