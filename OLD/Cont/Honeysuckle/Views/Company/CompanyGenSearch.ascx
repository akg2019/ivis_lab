﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Company>>" %>

    <div id="companies-search">
        <div class="row menu">
            <div class="item">
            </div>
            <div class="item">
                IAJID
            </div>
            <div class="item">
                CompanyName
            </div>
            <div class="item">
                CompanyType
            </div>
            <div class="item">
                trn
            </div>
        </div>

    <% foreach (var item in Model) { %>
    
        <div class="row">
            <div class="item">
                <%: Html.ActionLink("Edit", "Edit", new { id = item.CompanyID }) %> |
                <%: Html.ActionLink("Details", "Details", new { id = item.CompanyID })%> |
                <%: Html.ActionLink("Delete", "Delete", new { id = item.CompanyID })%>
            </div>
            <div class="item">
                <%: item.IAJID %>
            </div>
            <div class="item">
                <%: item.CompanyName %>
            </div>
            <div class="item">
                <%: item.CompanyType %>
            </div>
            <div class="item">
                <%: item.trn %>
            </div>
        </div>
    
    <% } %>

    </div>