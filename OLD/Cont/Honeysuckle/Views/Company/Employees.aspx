﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.Employee>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Employees
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Employees</h2>

    <div class="employees-list table">
        <div class="row menu">
            
            <div class="item">
                EmployeeID
            </div>
            <div class="item">
                Active
            </div>
        </div>
    <% int i = 1; %>
    <% foreach (var item in Model) { %>
        
        <% if (i % 2 == 0)
           { %>
            <div class="row data even">
        <% } %>
        <% else
           { %>
            <div class="row data odd">
        <% } %>
        <% i++; %>
            <div class="item">
                <%: Html.ActionLink("Edit", "Edit", new { /* id=item.PrimaryKey */ }) %> |
                <%: Html.ActionLink("Details", "Details", new { /* id=item.PrimaryKey */ })%> |
                <%: Html.ActionLink("Delete", "Delete", new { /* id=item.PrimaryKey */ })%>
            </div>
            <div class="item">
                <%: item.EmployeeID %>
            </div>
            <div class="item">
                <%: item.Active %>
            </div>
        </div>
    
    <% } %>

    </div>

    <p>
        <%: Html.ActionLink("Create New", "Create") %>
    </p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

