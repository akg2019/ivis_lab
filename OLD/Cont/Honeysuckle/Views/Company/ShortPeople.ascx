﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Person>>" %>

<%
    bool person_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("People", "Read"));
    bool person_update = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("People", "Update"));
%>

<%: Html.Hidden("count", Model.Count(), new { @class = "dont-process" })%>

    <div class="ivis-table">
        <div class="shortpeople menu">
            <div class="rowpeople fname item">
                First
            </div>
            <div class="rowpeople mname item">
                Middle
            </div>
            <div class="rowpeople lname item">
                Last
            </div>
            <div class="rowpeople tnr item">
                TRN
            </div>
        </div>
        <div class="scroll-table">
        <% int i = 0; %>
        <% foreach (var item in Model) { %>
            <% string TRN = "";
            if (item.TRN == "10000000") TRN = "Not Available"; else TRN = item.TRN;
            %>
            <% i++; %>
            <% if (i%2 == 0){ %>
            <div class="person data even row">
            <% } %>
            <% else{ %>
            <div class="person data odd row">
            <% } %>
                <%: Html.Hidden("did", item.PersonID, new { @class = "hiddenid id dont-process" })%>
                 <%: Html.Hidden("hidimport", item.imported, new { @class = "hidimport dont-process" })%>
                <div class="rowpeople fname item">
                    <%: item.fname %>
                </div>
                <div class="rowpeople mname item">
                    <%: item.mname %>
                </div>
                <div class="rowpeople lname item">
                    <%: item.lname %>
                </div>
                <div class="rowpeople trn item">
                    <%: TRN %>
                </div>
                <div class="rowpeople functions item">
                    <% if (person_read) { %>
                        <div class="icon view" title="View Person"> 
                            <img src="../../Content/more_images/View_icon.png" class="sprite"/>
                        </div>
                    <% } %>
                    <% if (person_update) { %>
                        <div class="icon edit" title="Edit Person"> 
                            <img src="../../Content/more_images/Edit_icon.png" class="sprite"/>
                        </div>
                    <% } %>
                </div>
            </div>
    
        <% } %>
    
    </div>
</div>