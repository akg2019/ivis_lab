﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Employee>" %>

    <% if (ViewData["edit"] == null) ViewData["edit"] = false; %>
    
    <% using (Html.BeginCollectionItem("employee")) {%>
        <%: Html.ValidationSummary(true) %>
        <%: Html.HiddenFor(model => model.EmployeeID, new { @class = "dont-process" })%>

        <!-- RENDER COMPANY ELEMENT OF AN EMPLOYEE ONLY IF THE USER IS A SYSTEM ANDMIN-->
        
        <% if (Model.company != null) { %>
            <div class="validate-section">
                <div class="subheader"> 
                    <div class="arrow right"></div>
                    Company 
                    <div class="valid"></div>
                </div>
                <div class="data">
                        <%: Html.HiddenFor(model => model.company.CompanyID, new { id = "hiddencompanyid", @class = "dont-process" })%>
                        <%: Html.HiddenFor(model => model.company.IAJID, new { @class = "dont-process" })%>
                    <% if (Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user)) { %>
                        <div class="editor-field"> 
                            <% Html.RenderAction("CompanyDropDown", "Company", new { type = "InsureAndInterComp" }); %>
                        </div>
                    <% } %>
                    <% else { %>
                        <% Html.RenderAction("EmployeeCompany", "Company", new { compId =Model.company.CompanyID }); %>
                    <% } %>
                </div>
            </div>
        <% } %>
        <!-- RENDER PERSON ELEMENT OF AN EMPLOYEE -->
            
        
        <% if (Model.person != null) { %>
            <% Html.RenderPartial("~/Views/People/Person.ascx", Model.person, new ViewDataDictionary{{"edit", ViewData["edit"]},{"policyInsured", false},{"editButton",true}}); %>
        <%  } %>
        <% else { %>
            <% ViewData["policyInsured"] = false;  %>
            <% Html.RenderAction("Person", "People"); %>
        <% } %>
               
            
    <% } %>

