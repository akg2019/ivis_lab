﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Person>>" %>

 <div id="insured" class="insured-modal">
 
    <div class="line">
        <span class="mess">Please tick the appropriate person (s):</span>
        <div id="searching" class="index-search">
            <%: Html.TextBox("search-modal", null, new { @placeholder = "Search Drivers", @autocomplete = "off", @class="modalSearch"})%> 
            <button type="button" class="quick-search-btn" onclick="searchPeople(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
        </div>
    </div>

 
     <div class="ivis-table modal-table">
         <div class= "heading menu"> 
            <div class="select item">Select</div>
            <div class="trn item">TRN</div>
            <div class="fname item">First Name</div>
            <div class="lname item">Last Name</div>
        </div>
        <div id="selected-insured" class="static selectedSection"></div>
        <div id="insureds" class="dynamic" >
            <% if(Model.Count() == 0) { %>
                    <div class="rowNone">There Are No Drivers. Please add one.</div>
            <% } %>
            <% else { %>

                <% if(Model.Count() > 50) { %>
                    <div class="rowNone">
                        Please search the system for existing insureds, or add a new insured. 
                    </div>
                <% } %>

                <% int i = 0; %>
                <% foreach (Honeysuckle.Models.Person item in Model) { %>
                    <% if (i%2 == 0) { %> <div class="insured-row row even" > <% } %>
                    <% else { %> <div class="insured-row row odd" > <% } %>
                    <% i++; %>
        
                        <div class="insured-check select item" >
                            <%: Html.CheckBox("insuredcheck", false, new { @class = "insuredcheck", id = item.PersonID.ToString(), @OnClick="changeCheckBox(this)" })%>
                        </div>
                        <div class="insured-trn trn item" >
                            <%: item.TRN %>
                        </div>
                        <div class="insured-fname fname item" >
                            <%: item.fname %> 
                        </div>
                        <div class="insured-lname lname item" >
                            <%: item.lname %>
                        </div>
        
                    </div>
                 <% } %>
            <% } %>
        </div>
    </div>
   
</div>