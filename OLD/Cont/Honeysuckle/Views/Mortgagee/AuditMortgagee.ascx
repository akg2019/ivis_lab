﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Mortgagee>" %>

<div class="editor-label">
    Mortgagee:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.mortgagee) %>
</div>
