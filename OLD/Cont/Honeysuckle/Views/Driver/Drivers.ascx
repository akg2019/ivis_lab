﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Driver>" %>

<div class="newAuthdriver driver-div">

    <% using (Html.BeginCollectionItem("Driver")) { %>
        <%: Html.ValidationSummary(true) %>

        <div class="insured-auth insured-data">
            <%: Html.HiddenFor(Model => Model.person.PersonID, new { @class = "authorizedId per dont-process" })%>
            <%: Html.Hidden("vehID", ViewData["vehicleID"], new { @id = "vehID", @class = "dont-process" })%>
            <% bool mainDriver = (bool)ViewData["MainDriver"]; %>
            
            <div class="line">
                <span class="label item">TRN:</span>
                <%: Html.TextBoxFor(model => model.person.TRN, new { @readonly = "readonly", @class = "perTRN item" })%>
                <div id="mainDriverImage"> 
                    <% if (mainDriver) { %>
                        <div class="mainImage">
                            <img src="../../Content/more_images/hat.png" title="Main Driver"/>
                        </div>
                    <% } %>
                </div>
            </div>
           
           <div class="line">
                <span class="driverName item"><%: ViewData["personName"] %></span>
            </div> 
          
        </div>
        <div class="DriverFooter">
            <a href="#" class="remDriver">Delete</a>
             <% if (!mainDriver)
                {%> 
             <a href="#" class="changeMain">Change to Main</a> 
             <%} %>
        </div>
    <% } %>
</div>