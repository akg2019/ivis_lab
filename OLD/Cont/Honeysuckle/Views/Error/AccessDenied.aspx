﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	AccessDenied
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!--<h2>Access Denied</h2>-->

    <div class="area-404 release">
        <div class="message release">
            <span class="text">Access Denied</span>
            <!--<span class="nf">Page Not Found</span>-->
            <span class="line">You do not have the permissions to see the data or page you requested. Please return to the <%: Html.ActionLink("Home Page", "Index", "Home") %>. </span>
        </div>
        <div class="release-img denied">
            <% if ((bool)ViewData["release"]) { %>
                <img src="../../Content/more_images/IVIS_ICON_LARGE.png" class="img-404 release" />
            <% } else { %>
                <img src="../../Content/more_images/accessdenied-debug.jpg" class="img-404 debug"  />
            <% } %>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/404.css" rel="stylesheet" type="text/css" /> 
</asp:Content>