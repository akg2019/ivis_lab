﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.KPI>>" %>

<%
    int company_id = Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID;
    bool is_iaj = Honeysuckle.Models.CompanyModel.is_company_iaj(company_id);
    bool is_insurer = Honeysuckle.Models.CompanyModel.IsCompanyInsurer(company_id);
    bool is_broker = Honeysuckle.Models.CompanyModel.IsCompanyBroker(company_id);
%>

<div class="container medium">
    <div class="stripes"></div>
    <div class="form-header">
        <div class="form-text-links">
            <span class="form-header-text">Cover Generation Chart</span>
        </div>
        <div class="date-pickers">
            <%: Html.TextBox("int_cover_count_start", DateTime.Now.AddMonths(-1), new { @class = "datepicker covercount start", @readonly = "readonly", @Value = DateTime.Now.AddMonths(-1).ToString("yyyy MM dd") })%>
            <strong>-</strong>
            <%: Html.TextBox("int_cover_count_end", DateTime.Now, new { @class = "datepicker covercount end", @readonly = "readonly", @Value = DateTime.Now.ToString("yyyy MM dd") })%>
            <button class="date-change cover-chart" onclick="update_chart_data()">Update</button>
        </div>
        <div class="cover-filters">
            <div class="import_div">
                <span>Hide Imports</span>
                <%: Html.CheckBox("import", false, new { @class = "import" }) %>
            </div>
            <div class="import_div">
                <span>Type:</span>
                <% if (is_iaj) { %>
                    <%: Html.DropDownList(  "ins_brk",
                                            new SelectList(new[] { new SelectListItem() { Text = "insurer", Value = "i", Selected = true }, new SelectListItem() { Text = "broker", Value = "b" }, new SelectListItem() { Text = "agent", Value = "a" } }, "Value", "Text"),
                                            new { @class = "ins_brk" }
                                          ) %>
                <% } %>
                <% else if(is_insurer){ %>
                    <%: Html.DropDownList(  "ins_brk",
                                        new SelectList(new[] {new SelectListItem() { Text = "insurer", Value = "i", Selected = true }, new SelectListItem() { Text = "broker", Value = "b" }, new SelectListItem() { Text = "agent", Value = "a" } }, "Value", "Text"),
                                        new { @class = "ins_brk" }
                                     ) %>
                <% } %>
                <% else if(is_broker){ %>
                    <%: Html.DropDownList(  "ins_brk",
                                        new SelectList(new[] { new SelectListItem() { Text = "broker", Value = "b", Selected = true }, new SelectListItem() { Text = "agent", Value = "a" } }, "Value", "Text"),
                                        new { @class = "ins_brk" }
                                     ) %>
                <% } %>
            </div>
        </div>
        
    </div>
    <div class="chart-data">
        <div id="inter-cover-count" style="height:500px; width:100%;"></div>
    </div>
</div>