﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.Duplicate>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="ivis-table">
    <div class="menu">
        <div class="ref item">
            Reference No
        </div>
        <div class="type item">
            Type
        </div>
        <div class="count item">
            Count
        </div>
    </div>
    <div class="table-data">
        <% for(int i = 0; i < Model.Count(); i++){ %>
            <% if (i % 2 == 0) { %> <div class="row data even"> <% } %>
            <% else { %> <div class="row data odd"> <% } %>
                <div class="ref item">
                    <%: Model.ElementAt(i).reference_no %>
                </div>
                <div class="type item">
                    <%: Model.ElementAt(i).type %>
                </div>
                <div class="count item">
                    <%: Model.ElementAt(i).duplicate_count %>
                </div>
            </div>
        <% } %>
    </div>
</div>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/duplicate.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/duplicate.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

