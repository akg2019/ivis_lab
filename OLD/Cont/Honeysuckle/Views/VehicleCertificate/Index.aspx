﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.VehicleCertificate>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

   <div id="NotActive" style="display:none;"> 

   <%: ViewData["CertNotActive"] %>
    </div>

    <!-- CHECKS IF THE CURRENT USER BELONGS TO AN INSURANCE COMPANY- As only insurance companies should view the dropdown list-->
   
    <% if (Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID)) { %>
           <h4> View Intermediary Vehicle Certificates: </h4> 
          <% Html.RenderAction("CompanyDropDown", "Company", new {type= "IntermediaryCompanies" }); %>
    <% } %>
     <br /> <br /> <br />

    <table>
        <tr>
            <th></th>
           
            <th>
                CertificateNo
            </th>
            <th>
                EffectiveDate
            </th>
            <th>
                ExpiryDate
            </th>
           
            <th>
                EndorsementNo
            </th>
            <th>
                PrintedPaperNo
            </th>
          
            
        </tr>

    <% foreach (var item in Model) { %>
    
        <tr>
            <td>
              <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Update"))) 
                    { %>
                       <% if (!Honeysuckle.Models.VehicleCertificateModel.IsCertitificateCancelled(item.VehicleCertificateID))  
                          { %>
                           <% if (!Honeysuckle.Models.VehicleCertificateModel.IsCertificatePrinted(item.VehicleCertificateID))  
                              { %>
                                   <%: Html.ActionLink("Edit", "Edit", new {  id=item.VehicleCertificateID }) %> |
                           <% } %>
                       <% } %>
                 <% } %> 


                <%: Html.ActionLink("Details", "Details", new { id = item.VehicleCertificateID })%> |


                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Approve"))) 
                     { %>
                       <% if (Honeysuckle.Models.VehicleCertificateModel.IsCertitificateActive(item.VehicleCertificateID))  
                          { %>
                               <%: Html.ActionLink("Cancel", "Cancel", new { id = item.VehicleCertificateID })%>
                       <% } %>
                  <% } %>


                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Delete"))) 
                     { %>
                          <%: Html.ActionLink("Delete", "Delete", new { id = item.VehicleCertificateID })%>
                  <% } %>


                 <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Read"))) 
                     { %>
                       <% if (!Honeysuckle.Models.VehicleCertificateModel.IsCertitificateActive(item.VehicleCertificateID))  
                          { %>
                                <%: Html.ActionLink("Request Approval", "Approval", "Notification", new { id = item.VehicleCertificateID, type = "certificate" }, null)%>
                       <% } %>
                  <% } %>

                  <!--A CERTIFICATE HAS TO BE EITHER 'APPROVED' OR 'CANCELLED' TO BE PRINTED, NO OTHER 'STATES' ARE APPLICABLE FOR PRINTING -->
                   <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Print"))) 
                       { %>
                         <% if (Honeysuckle.Models.VehicleCertificateModel.IsCertitificateActive(item.VehicleCertificateID) || (Honeysuckle.Models.VehicleCertificateModel.IsCertitificateCancelled(item.VehicleCertificateID)))
                             { %>
                                <% if (Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID)) %>
                                 <% if (Honeysuckle.Models.UserGroupModel.TestUserGroup(Honeysuckle.Models.Constants.user, (new Honeysuckle.Models.UserGroup("Company Administrator"))) || (Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user)))
                                   { %>
                                        <%: Html.ActionLink("Print", "PrintCertificate", new { id = item.VehicleCertificateID })%> 
                               <%  } %>  
                               
                              <% else 
                                  { %>   
                                      <% if (Honeysuckle.Models.VehicleCertificateModel.IsCertificatePrintable(item.VehicleCertificateID))
                                       { %>
                                            <%: Html.ActionLink("Print", "PrintCertificate", new { id = item.VehicleCertificateID })%> 
                                   <%  } %>  
                                      <% else 
                                      { %> 
                                          <div class="vehicleCertId"> 
                                             <%: Html.Hidden("vehId", item.VehicleCertificateID, new { @class = "vehCertID" })%>
                                             <input type="button" value="Message Admin To Enable Print" id = "messageAdmins" onclick="MessageCompAdmin();" /> 
                                          </div>
                                          <div id = "SendMail"> </div>
                                  <%  } %>    
                              <%  } %>    
                        <%  } %>
                   <% }  %>
            </td>
            
            <td>
                <%: item.CertificateType + item.InsuredCode + item.ExtensionCode + item.UniqueNum %>
            </td>
            <td>
                <%: String.Format("{0:g}", item.EffectiveDate) %>
            </td>
            <td>
                <%: String.Format("{0:g}", item.ExpiryDate) %>
            </td>
          
            <td>
                <%: item.EndorsementNo %>
            </td>
            <td>
                <%: item.PrintedPaperNo %>
            </td>
            
           
        </tr>
    
    <% } %>

    </table>

    <p>
        <%//: Html.ActionLink("Create New", "Create") %>
    </p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/GetIntermediaryData.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/CertificateNotActive.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/ApproveCertificate.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/VehicleCertificate.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftBar" runat="server">
</asp:Content>

