﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Vehicle>" %>

 <div class="Vehicle">

    <% using (Html.BeginCollectionItem("vehicles")) {%>
    <%: Html.ValidationSummary(true) %>

    <%: Html.HiddenFor(Model => Model.ID, new { @class = "vehId dont-process" })%> 
    
    <div class="editor-label">
        Year, Make, Model:
    </div>
    <div class="editor-field">
        <%: Html.TextBox("vehicleDataDisplay", ViewData["vehicleInfoDisplay"], new { @readonly = "readonly", @class = "read-only"})%> 
    </div>
    <div class="editor-label">
        Registration Number:
    </div>
    <div class="editor-field">
        <%: Html.TextBoxFor(model => model.VehicleRegNumber, new { @readonly = "readonly", @class = "read-only" })%>
    </div>

    <div class="editor-label">
        Chassis Number:
    </div>
    <div class="editor-field">
        <%: Html.TextBoxFor(model => model.chassisno, new { @readonly = "readonly", @class = "Chassis read-only", })%> 
        <%: Html.HiddenFor(model => model.VIN, new { @readonly = "readonly", @class = "vehicle_vin dont-process" })%> 
        <%: Html.HiddenFor(Model => Model.usage.ID, new { id = "vehUsageID", @class = "usageIdentity dont-process" })%> 
    </div>
    <div class="editor-label">
        Usage:
    </div>
    <div class="editor-field"> 
        <%: Html.TextBoxFor(model => model.usage.usage, new { @readonly = "readonly", @class = "read-only",@id = "usage" })%> 
    </div>

    <% if ((bool)ViewData["certificateCreate"]) 
        { %>
            <div class="vehicleButton line">
            <a href="#" class="rmvehicle">Delete Vehicle</a> 
            </div> 
    <% } %> 
       
    <% } %>
</div>
