﻿<%@ Page Title="Edit Vehicle" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Vehicle>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <div id= "form-header">
                <img src="../../Content/more_images/Vehicle.png" alt=""/>
                <div class="form-text-links">
                    <span class="form-header-text">Edit Vehicle</span>
                   <div class="icon">
                        <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                   </div>
                </div>
            </div>
            <div class="field-data">
                <%: Html.Hidden("page", "vehEdit", new { id="page" })%>
                <div id= "RegNoPrompt"> 
                    <%: ViewData["RegNo"]%>
                </div>

                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right down"></div>
                        Main Vehicle Data
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <%: Html.HiddenFor(model => model.ID, new { id = "vehicleId", @class = "dont-process" })%>
                        <%: Html.HiddenFor(model => model.base_id, new { id = "vehicle_base_id", @class = "dont-process" }) %>
                        <div class="editor-label">
                           VIN 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.VIN, new { @readonly = "readonly", id = "vin", @class= "read-only" })%>
                            <%: Html.ValidationMessageFor(model => model.VIN) %>
                            <button id="editVin" class ="editButton edit-btn edit-vin" type="button">Edit</button>
                        </div>

                        <div class="editor-label">
                           Chassis Number
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.chassisno, new { @readonly = "readonly", id = "chassis", @required = "required", @class = "read-only" })%>
                            <%: Html.ValidationMessageFor(model => model.chassisno) %>
                             <button id="editChassis" class ="editButton edit-chassis edit-btn" type="button">Edit</button>
                        </div>

                        <div class="editor-label">
                            Make
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.make, new { @required = "required", id = "make", @readonly = "readonly", @class = "read-only" })%>
                            <%: Html.ValidationMessageFor(model => model.make) %>
                            <button id="editMake" class ="editButton edit-make edit-btn" type="button">Edit</button>
                        </div>
            
                        <div class="editor-label">
                            Model
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.VehicleModel, new { @required = "required", id = "vehiclemodel", @readonly = "readonly", @class = "read-only" })%>
                            <%: Html.ValidationMessageFor(model => model.VehicleModel) %>
                            <button id="editModel" class ="editButton edit-model edit-btn" type="button">Edit</button>
                        </div>

                        <div class="editor-label">
                           Model Type
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.modelType, new { id = "modeltype", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.modelType) %>
                        </div>

                        <div class="editor-label">
                          Body Type
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.bodyType, new { id = "bodytype", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.bodyType) %>
                        </div>

                        <div class="editor-label">
                            Extension
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.extension, new { id = "extension", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.extension) %>
                        </div>

                        <div class="editor-label">
                           Registration Number
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.VehicleRegNumber, new { @required = "required", id = "vehicleRegNum" })%>
                            <%: Html.ValidationMessageFor(model => model.VehicleRegNumber)%>
                        </div>
                        <div class="editor-label">
                           Vehicle Year
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.VehicleYear, new { id = "vehicleYear", @required = "required", @readonly = "readonly", @class = "read-only" })%>
                            <%: Html.ValidationMessageFor(model => model.VehicleYear) %>
                            <button id="editYear" class ="editButton edit-year edit-btn" type="button">Edit</button>
                        </div>
                    </div>
                </div>
                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right"></div>
                        Vehicle Meta Data
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                
                        <div class="editor-label">
                           Colour
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.colour, new { id = "colour", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.colour) %>
                        </div>
                        <div class="editor-label">
                          Cylinders
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.cylinders, new { id = "cylinders", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.cylinders) %>
                        </div>
                        <div class="editor-label">
                          Engine Number
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.engineNo, new { id = "engineNo", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.engineNo) %>
                        </div>
                        <div class="check-div">
                            <div class="editor-label">
                                Engine Modified
                            </div>
                            <div class="editor-field">
                                <%: Html.CheckBoxFor(model => model.engineModified, new { id = "engineModified", Style = "width: 14px; height:14px;" })%>
                            </div>
                        </div>
                        <div class="editor-label">
                          Engine Type
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.engineType, new { id = "engineType", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.engineType) %>
                        </div>
                        <div class="editor-label">
                              Estimated Annual Mileage
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.estAnnualMileage, new { id = "mileage", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.estAnnualMileage) %>
                        </div>
                        <div class="editor-label">
                            Expiry Date Of Fitness 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBox("expiryDateOfFitness", null, new { id = "edit_fitnessExpiry", @readonly = "readonly",@class = "read-only" })%>
                            <%: Html.Hidden("hidden_fitnessExpiry", Model.expiryDateOfFitness.ToString("MM/dd/yyyy"), new { @id = "hidden_fitnessExpiry", @class = "dont-process" })%>
                            <button id="editFitness" class ="editButton edit-fitness edit-btn" type="button">Edit</button>
                        </div>
                        <div class="editor-label">
                            HPCC Unit Type
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.HPCCUnitType, new { id = "hpccUnit", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.HPCCUnitType) %>
                        </div>
                        <div class="editor-label">
                            Reference Number
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.referenceNo, new { id = "refno", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.referenceNo) %>
                        </div>
                        <div class="editor-label">
                           Registration Location
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.registrationLocation, new { id = "regLocation", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.registrationLocation) %>
                        </div>
                        <div class="editor-label">
                           Roof Type
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.roofType, new { id = "roofType", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.roofType) %>
                        </div>
                        <div class="editor-label">
                           Transmission Type
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.transmissionType.transmissionType, new { id = "tranType", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.transmissionType.transmissionType)%>
                        </div>

                         <div class="editor-label">
                            Hand Drive Type 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.handDrive.handDriveType, new { id = "handDriveType", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.handDrive.handDriveType)%>
                        </div>
                        <div class="editor-label">
                           Seating
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.seating, new { id = "seating", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.seating) %>
                        </div>
                        <div class="editor-label">
                            Seating Cert
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.seatingCert, new { id = "seatingCert", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.seatingCert) %>
                        </div>
                        <div class="editor-label">
                           Tonnage
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.tonnage, new { id = "tonnage", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.tonnage) %>
                        </div>
                        <div class="editor-label">
                           Registered Owner
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.RegisteredOwner, new { id = "regOwner", @required = "required" })%>
                            <%: Html.ValidationMessageFor(model => model.RegisteredOwner)%>
                        </div>
                        <div class ="editor-label">
                            Estimated Value
                        </div>
                        <div class="editor-field"> 
                            <%: Html.TextBoxFor(model => model.estimatedValue, new { id = "estimatedValue", @required = "required" })%>
                        </div>
                        <div class="check-div">
                            <div class="editor-label">
                               Restricted Driver
                            </div>
                            <div class="editor-field">
                                 <%: Html.CheckBoxFor(model => model.RestrictedDriving, new {id = "restrictedDriver", Style = "width: 14px; height:14px;" })%>
                            </div>
                        </div>
                    </div>
                </div>
            
                <% if (Model.mainDriver != null) { %>
                    <div class="validate-section">
                        <div class="subheader"> 
                            <div class="arrow right"></div>
                            Main Driver of Vehicle
                            <div class="valid"></div>
                        </div>
                        <div class="data">
                            <%: Html.HiddenFor(model => model.mainDriver.person.PersonID, new { id = "mainDriverId", @class="dont-process" })%>

                            <div class="editor-label">
                                TRN
                            </div>
                            <div class="editor-field">
                                 <% if ((bool)ViewData["TRNnotAvailable"])
                                    {%>
                                <%: Html.HiddenFor(model => model.mainDriver.person.TRN, new { @class = "dont-process" })%>
                                <%: Html.TextBox("TRNnotAvailable", "Not Available", new { id = "person-trn-not-available", @required = "required", @class = "person-trn-not-available" })%>
                            <% }
                                    else
                                    {%>

                                <%: Html.TextBoxFor(model => model.mainDriver.person.TRN, new { @required = "required", id = "trn", @class = "driver-trn", @PlaceHolder = " Eg. 1888776655" })%>
                                <%: Html.ValidationMessageFor(model => model.mainDriver.person.TRN)%>
                                <button id="editTRN" class ="editButton edit-trn edit-btn" type="button">Edit</button>
                                <% } %>
                            </div>

                            <div class="editor-label">
                                First Name
                            </div>
                            <div class="editor-field">
                                <%: Html.TextBoxFor(model => model.mainDriver.person.fname, new { @required = "required", id = "fname", @class = "driver-fname" })%>
                                <%: Html.ValidationMessageFor(model => model.mainDriver.person.fname)%>
                            </div>
            
                            <div class="editor-label">
                                Middle Name:
                            </div>
                            <div class="editor-field">
                                <%: Html.TextBoxFor(model => model.mainDriver.person.mname, new { id = "mname" })%>
                                <%: Html.ValidationMessageFor(model => model.mainDriver.person.mname)%>
                            </div>
            
                            <div class="editor-label">
                                Last Name
                            </div>
                            <div class="editor-field">
                                <%: Html.TextBoxFor(model => model.mainDriver.person.lname, new { @required = "required", id = "lname", @class = "driver-lname" })%>
                                <%: Html.ValidationMessageFor(model => model.mainDriver.person.lname)%>
                            </div>
                        </div>
                    </div>
                    <!-- Full adress layout -->

                    <div class="validate-section">
                        <div class="subheader">
                            <div class="arrow right"></div>
                            Main Driver Address Data
                            <div class="valid"></div>
                        </div>
                        <div class="data">
                            <div class="editor-label">
                                Street 
                            </div>
                            <div class="editor-field">
                                <%: Html.TextBoxFor(model => model.mainDriver.person.address.roadnumber, new { id = "RoadNumber", @PlaceHolder = " 12" })%>
                                <%: Html.TextBoxFor(model => model.mainDriver.person.address.road.RoadName, new { id = "roadname", @PlaceHolder = " Short Hill" })%>
                                <%: Html.TextBoxFor(model => model.mainDriver.person.address.roadtype.RoadTypeName, new { id = "roadtype", @PlaceHolder = " Avenue" })%>
                
                                <%: Html.ValidationMessageFor(model => model.mainDriver.person.address.roadnumber)%>
                                <%: Html.ValidationMessageFor(model => model.mainDriver.person.address.road.RoadName)%>
                                <%: Html.ValidationMessageFor(model => model.mainDriver.person.address.roadtype.RoadTypeName)%>
                            </div><br />

                            <div class="editor-label">
                                Building/Apartment No. 
                            </div>
                            <div class="editor-field">
                                <%: Html.TextBoxFor(model => model.mainDriver.person.address.ApartmentNumber, new { id = "aptNumber" })%>
                                <%: Html.ValidationMessageFor(model => model.mainDriver.person.address.ApartmentNumber)%>
                            </div>
           
                            <div class="editor-label">
                                City/Town
                            </div>
                            <div class="editor-field">
                                <%: Html.TextBoxFor(model => model.mainDriver.person.address.city.CityName, new { id = "city", @required = "required",@class = "city vehicle-edit" })%>
                                <%: Html.ValidationMessageFor(model => model.mainDriver.person.address.city.CityName)%>
                            </div>

                            <div class="editor-label">
                                Parish
                            </div>
                            <div class="editor-field vehicle-edit">
                                <%: Html.HiddenFor(model => model.mainDriver.person.address.parish.ID, new { id = "parish", @class = "dont-process" })%>
                                <% Html.RenderAction("ParishList", "Parish"); %>
                                <%: Html.ValidationMessage("parish") %>
                            </div><br />
                    
                            <div class="editor-label">
                                Country Name
                            </div>
                            <div class="editor-field">
                                <%: Html.TextBoxFor(model => model.mainDriver.person.address.country.CountryName, new { id = "country", @required = "required",@class = "country vehicle-edit" })%>
                                <%: Html.ValidationMessageFor(model => model.mainDriver.person.address.country.CountryName)%>
                            </div>
                        </div>
                    </div>
                <% } %>
            </div>
            <div class="btnlinks">
                <% Html.RenderAction("BackBtn", "Back"); %>
                <input type="submit" value="Save" id= "Submit2"/>
            </div>
        </fieldset>

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/multiple-emails.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/vehicleEdit.js" type="text/javascript"></script>
    <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/RegularCompContact.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/VehicleAutoComplete.js" type="text/javascript"></script>
    <script src="../../Scripts/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript"></script>
    <link href="../../Scripts/datetimepicker-master/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/parishUpdate.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/TestVIN.js" type="text/javascript"></script>
</asp:Content>


