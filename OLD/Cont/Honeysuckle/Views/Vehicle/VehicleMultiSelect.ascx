﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Vehicle>>" %>

<%: Html.ListBox("vehiclelistbox", new SelectList(Model.ToList(), "ID", "VIN"), new {@class = "vehicle_listbox"})%>