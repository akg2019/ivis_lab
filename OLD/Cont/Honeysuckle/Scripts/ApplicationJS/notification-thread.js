﻿$(function () {
    $(".note-extra-data").hide();
    $(".clicked").children(".note-extra-data").show();
    $(".clicked").children(".dots").hide();
    $(".note-heading").click(function () {
        if ($(this).parent().children('.note-extra-data:first').is(":hidden")) {
            $(this).parent().children('.note-extra-data:first').slideDown(200);
            $(this).parent().children(".dots").hide();
        }
        else {
            $(this).parent().children('.note-extra-data:first').slideUp(200);
            $(this).parent().children(".dots").show();
        }
    });
    $(".dots").click(function () {
        if ($(this).parent().children('.note-extra-data:first').is(":hidden")) $(this).parent().children('.note-extra-data:first').slideDown(200);
        else $(this).parent().children('.note-extra-data:first').slideUp(200);
    });
    $("#show-all-messages").click(function () {
        if ($('.note-extra-data:hidden')) $(".note-content").slideDown(200);
        else $(".note-extra-data").slideUp(200);
    });
});