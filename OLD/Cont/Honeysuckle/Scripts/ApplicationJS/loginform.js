$(function () {
    $("#clearbtn").click(function () {
        $("#usernamefield").val("");
        $("#passwordfield").val("");
    });

    if ($(".missmatched").length > 0) {
        $("<div></div>").text("Please make sure that the passwords you submit match.").dialog({
            modal: true,
            title: 'Match Error!',
            buttons: {
                Ok: function () {
                    $(this).dialog('close');
                }
            }
        });
    }

    if ($(".password-failed-validation").length > 0) {
        $("<div></div>").text("Password must contain: numbers, capital & common letters, special characters (!,@,#,$ ,%,^,&,*,(,)) and at least 8 characters").dialog({
            modal: true,
            title: 'Password Error!',
            buttons: {
                Ok: function () {
                    $(this).dialog('close');
                }
            }
        });
    }
});


function generatePassword() {
    $("#pass").dialog({
        modal: true,
        autoOpen: false,
        width: 400,
        title: "Password Generator",
        height: 200,
        buttons:
            {
                New: function () {
                    $("#pass").load('/Login/PasswordGenerator');
                },

                'Populate': function () {
                    $("#password1, #confirm_password").val($(".generatedText").text().trim());
                    $("#pass").dialog('close');
                    $("#pass").html("");
                },

                OK: function () {
                    $("#pass").dialog('close');
                    $("#pass").html("");
                }
            }
    });

    $("#pass").load('/Login/PasswordGenerator').dialog('open');
}


$(function () {
    if ($('.error').text().trim().length != 0) {

        $(".error").dialog({
            modal: true,
            autoOpen: false,
            title: "Alert!",
            buttons:
                {
                    OK:
                function () {

                    $('.error').dialog('close');
                    $('.error').html("");

                }
            }
        });

        $('.error').dialog('open');
    }
});

$(function () {

    if ($('.password-invalid').text().trim().length != 0) {

        $(".password-invalid").dialog({
            modal: true,
            autoOpen: false,
            title: "Alert!",
            buttons:
                {
                    OK:
                function () {

                    $('.password-invalid').dialog('close');
                    $('.password-invalid').html("");

                }
                }
        });

        $('.password-invalid').dialog('open');
    }

});

function isEmpty(el) {
    return !$.trim(el.html())
}
