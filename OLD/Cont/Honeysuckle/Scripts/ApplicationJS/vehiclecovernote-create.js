﻿var vehiclecoveredmessage = "<div>The vehicle you have selected either already has a certificate or cover note active, or it will no longer be covered by the current policy in the date period you've selected. Please selecte a different vehicle or a different time period and resubmit your request.</div>";
var effective;
var expiry;
var effective_static;
var noInsuredVeh = "<div class=\"rowNone\">There Are No Vehicles. Please add one. </div>";
var InsuredHiddenVeh = "<div class=\"rowNone\">Please search the system for existing vehicles, or add a new vehicle. </div>";
var min;
var max;
var expiryBefore = get_start_datetime();
var effectiveBefore = get_start_datetime();
var periodBefore=0;
var DAY;

function get_start_datetime() {
    var start;
    if ((typeof $('#isInsurer').val() === 'undefined') && ($('#covernoteno').val() == "")) start = new Date($("#sysdate").val())
    else if ($("#policyStartTime").val() == "") start = new Date($("#sysdate").val())
    else start = new Date($("#policyStartTime").val());

    return start;
}   

function get_end_datetime() {
    var end;
    if ($("#policyEndTime").val() == "") end = new Date($("#sysdate").val())
    else end = new Date($("#policyEndTime").val());

    
    return end;
}   

$(function () {

    expiryBefore = get_start_datetime();
    effectiveBefore = get_start_datetime();

    min = new Date($("#policyStartTime").val());
    max = new Date($("#policyEndTime").val());

    DAY = 1000 * 60 * 60 * 24;

    //var myDate1 =
    effective = min;
    effective_static = min;
    
    var logic = function( currentDateTime ){
        // 'this' is jquery object datetimepicker
        if( currentDateTime.toLocaleDateString() == effective_static.toLocaleDateString("en-US") )
        {
            this.setOptions({
                minTime: effective_static.toLocaleTimeString()
            });
        }
        else
        {
            this.setOptions({
                minTime:'12:00 AM'
            });
        }
    };

    var pickers = true;
    pickers = (!($(".vcn_approved").val() == "True") && $(".firstprinted").val() == "") || !($(".vcn_cancelled").val() == "True");
    if (($("#expirydate").val() != undefined && $("#expirydate").val() == "" && $("#expirydate").val() == "01/01/0001 12:00:00 AM") && pickers) {
        var date_array = $("#expirydate").val().split(/[/ ]/);
        pickers = ((new Date(date_array[1]+'/'+date_array[0]+'/'+date_array[2])) > new Date()) || ($("#expirydate").val() == "");
    }

    var ampm = "";
    if (effective.getHours() > 11) ampm = "PM";
    else ampm = "AM";
    
    if (pickers) {
        
         disablePeriodOnRange(min,max);       

         $("#effectivedate").datetimepicker({
            id: 'effect',
            value: ($('#VCpage').length > 0 && $('#VCpage').val() == "edit") ? new Date($('#edit_hidden_effectivedate').val()).dateFormat('d M Y h:i A') : get_start_datetime().dateFormat('d M Y h:i A'),
            format: 'd M Y h:i A',
            formatTime: 'h:i A',
            step: 5,
            lang: 'en-GB',
            onShow: function( ct ){
                // 'this' is jquery object datetimepicker
                this.setOptions({
                    minDate: get_start_datetime(),
                    maxDate: get_end_datetime()
                });
            },
            validateOnBlur: false,
            closeOnDateSelect: true,
            closeOnWithoutSelect: true,
            onSelectDate: function (ct) {
                effective = ct;
                expiry = new Date($("#expirydate").val());

                if (expiry != undefined) {

                    if (expiry.getTime() < effective.getTime()) {
                        $("<div>The effective date will be greater than the current expiry date. Is this what you want? </div>").dialog({
                            title: 'Alert!',
                            modal:true,
                            buttons: {
                                Ok: function () {
                                    $("#effectivedate").val(effective.dateFormat('d M Y h:i A'));
                                    $("#expirydate").val(effective.dateFormat('d M Y h:i A'));
                                    $(this).dialog("close");
                                    $(this).dialog().dialog("destroy");
                                },
                                Cancel: function () {
                                    $("#effectivedate").val(effectiveBefore.dateFormat('d M Y h:i A'));
                                    $(this).dialog("close");
                                    $(this).dialog().dialog("destroy");
                                }
                            }
                        });
                       
                    }
                    else {
                        $("#period").val(Math.floor(Math.abs(expiry.getTime() - effective.getTime())/DAY));
                        $("#period").trigger("change");
                        $("#effectivedate").val(effective.dateFormat('d M Y h:i A'));
                        disablePeriodOnRange(effective,max);
                        effectiveBefore = new Date($("#effectivedate").val());
                    }

                }
            }
        });

        $("#effectivedate").trigger("change");

        $("#expirydate").datetimepicker({
            id: 'expiry',
            format: 'd M Y h:i A',
            value: ($('#VCpage').length > 0 && $('#VCpage').val() == "edit") ? new Date($('#edit_hidden_expirydate').val()).dateFormat('d M Y h:i A') : get_end_datetime().dateFormat('d M Y h:i A'),
            formatTime: 'h:i A',
            step: 5,
            lang: 'en-GB',
            formatTime: 'h:i A',
            allowTimes: ['11:59 PM'],
            validateOnBlur: false,
            closeOnDateSelect: true,
            closeOnWithoutSelect: true,
            onShow: function () {
                this.setOptions({
                    value: new Date($("#expirydate").val()).dateFormat('d M Y h:i A'),
                    minDate: new Date($("#effectivedate").val()),
                    maxDate: get_end_datetime()
                });
            },
            onSelectDate: function (ct) {
                expiry = ct;
                effective = new Date($("#effectivedate").val());

                if (expiry.getTime() < effective.getTime()) {
                    $("<div>The effective date will be greater than the current expiry date. Is this what you want?</div>").dialog({
                        title: 'Alert!',
                        modal:true,
                        buttons: {
                            Ok: function () {
                                $("#expirydate").val(expiry.dateFormat('d M Y h:i A'));
                                $("#effectivedate").val(expiry.dateFormat('d M Y h:i A'));
                                $(this).dialog("close");
                                $(this).dialog().dialog("destroy");
                            },
                            Cancel: function () {
                                $("#expirydate").val(expiryBefore.dateFormat('d M Y h:i A'));
                                $(this).dialog("close");
                                $(this).dialog().dialog("destroy");
                                }
                        }
                    });
                
                }
                else{
                    $("#period").val(Math.floor(Math.abs(expiry.getTime() - effective.getTime())/DAY));
                    $("#period").trigger("change");
                    expiry.setHours(23); expiry.setMinutes(59);
                    $("#expirydate").val(expiry.dateFormat('d M Y h:i A'));
                    expiryBefore = new Date($("#expirydate").val());
                }

            }
        });

        $("#expirydate").trigger("change");
    }
    else {
        $("input[type=\"text\"]").each(function () {
            if (!$(this).hasClass("read-only")) {
                $(this).toggleClass("read-only").prop("readonly", true);
            }
        });
        $(".quickperiod,input[type=\"submit\"]").hide();
        $("#wording").prop("disabled", true).toggleClass("read-only");
    }


    if ($("#vehicle-error").val() != undefined) {
        if ($("#vehicle-error").text().length > 0) {
            $("#vehicle-error").dialog({
                title: 'Alert',
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    }

    $("#period").blur(function () {
        UpdatePeriod();
    });

     $("#covernoteno").blur(function () {
        var selected = new Date($("#effectivedate").val());
        var sysdate = new Date($("#sysdate").val())
        if($(this).val() == "" && selected < sysdate && (typeof $('#isInsurer').val() === 'undefined')) {
            $("<div>You cannot do a manual cover note with backdating if the Manual Cover Note No. field is empty. </div>").dialog({
                title: 'Alert!',
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                        $(this).dialog().dialog("destroy");
                    }
                }
            });
            var setEffective = new Date($("#sysdate").val());
            $("#effectivedate").val(setEffective.dateFormat('d M Y h:i A'));
        }
    });

    $(".quickperiod").on('click', function () {
        $("#period").val($(this).val());
        $("#period").trigger("change");
        UpdatePeriod();
    });

  //  UpdatePeriod();
});


function disablePeriodOnRange(min,max){
    //disable 7 period button
    if((min.getTime() + (7 * DAY)) > max.getTime())$("#quickperiod-7").attr('disabled', true);
    else $("#quickperiod-7").attr('disabled', false);
    //disable 14 period button
    if((min.getTime() + (14 * DAY)) > max.getTime()) $("#quickperiod-14").attr('disabled', true);
    else $("#quickperiod-14").attr('disabled', false);
    //disable 30 period button
    if((min.getTime() + (30 * DAY)) > max.getTime()) $("#quickperiod-30").attr('disabled', true);
    else $("#quickperiod-30").attr('disabled', false);
}                   

function UpdatePeriod() {
    var period = $("#period").val();
    expiry = new Date($("#effectivedate").val());
    if (period != "") {
        if((expiry.getTime() + (parseInt(period) * DAY)) > max.getTime()) {

            $("<div>The  days effective period cannot go beyond the policy expiry date. </div>").dialog({
                title: 'Alert!',
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                        $(this).dialog().dialog("destroy");
                    }
                }
            });

            $("#period").val(periodBefore);
            $("#period").trigger("change");
        }
        else{
            expiry.setDate(expiry.getDate() + parseInt(period));
            expiry.setHours(23); expiry.setMinutes(59);
            $("#expirydate").val(expiry.dateFormat('d M Y h:i A'));
            $("#expirydate").trigger("change");
            periodBefore = parseInt(period);
        }
    }

}


//////////////////////////////////////////////////////////////

var vehIDNum;
var selectedVeh;

$(function () {
    $("#addvehicle").click(function () {
        
        var cid = $("#companyId").val();
        $("#vehicle-modal").dialog({
            modal: true,
            autoOpen: false,
            width: 800,
            title: "Vehicles List",
            height: 600

        }); //END .DIALOG

        $("#vehicle-modal").load('/Vehicle/VehiclesList/' + cid + '?type=' + 'cover', function (value) {

            if (value.substring(2, 6) == 'fail') { //then user does not have the permissions
                $(this).dialog('close');
                $(this).html("");
                $(this).dialog().dialog("destroy");

                $("<div>You do not have the required permissions to complete this task. </div>").dialog({
                    title: 'Alert!',
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                            $(this).dialog().dialog("destroy");
                        }
                    }
                });
            }
            else {  
                
                $("#create").remove();

                if ($(document).find(".vehicle-row").length <= 50) $(document).find(".vehicle-row").show();
                else $(document).find(".vehicle-row").hide();

                $(".select").hide();
                $(".vehicle-check").hide();

                $("#vehiclesList").perfectScrollbar({
                    wheelSpeed: 0.3,
                    wheelPropagation: true,
                    minScrollbarLength: 10,
                    suppressScrollX: true
                });
            }

        }).dialog('open');

    }); //END CLICK

    $("input.createNewPol").hide();
    $("#createCover").show();
});


$(function () {
    if ($(".chassisNumber").val() != undefined) {

        //Setting the usage for the vehicle
        var usagemodel = $(".chassisNumber").parent().children("#vehUsageID");
        var Usedropdownbox = $(".chassisNumber").parent().parent().parent().find(".useDropDown").children(".usage_dropdown");

        var usageID = usagemodel.val();

        Usedropdownbox.children().each(function () {
            if ($(this).val() == usageID) {
                $(this).prop('selected', true);
                // return;
            }
        });

        Usedropdownbox.prop("disabled", true);
    }
});



$(function () {
    $(document).on("click", ".row", function (e) {

        //var searchValue = $(event.target).parents(".insured-modal").find(".modalSearch").val();

        $("#vehiclesList").children(".vehicle-row").each(function () {

            //Removing the styling from the previously sected vehicle
            if ($(this).hasClass("selectedRow"))
                $(this).toggleClass("selectedRow");
        });

        vehIDNum = $(event.target).parent(".row").children(".vehicle_Id").val();
        $(event.target).parent(".row").toggleClass("selectedRow");

    }); //END CLICK
});



//Search when enter-button is hit
$(document).on("keyup", ".modalSearch", function (event) {

    var text = $(".modalSearch").val();
    if (event.which == 13) {
        searchVehicleData(text);
    }
});

function updateVehicleView() {
    if (vehIDNum != 0) $("#vehiclelist-selected").append(selectedVeh);
}

function removeThisVeh() { //This function removes the element from the arrays

    selectedVeh = null;
    vehIDNum = 0;

    updateVehicleView();
}

function AddVehicleSelected(id) {
    var err = "<div>There was an error with adding the vehicle. Please try again. If the problem persists, please contact your administrator.</div>"

    $.ajax({
        type: "POST",
        url: '/Vehicle/VehicleListItemSelected/' + id,
        cache: false,
        success: function (data) {

            selectedVeh = $(data);
            vehIDNum = id;

            updateVehicleView();
        },
        error: function () {
            $(err).dialog({
                title: "Error",
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog('close');
                    }
                }
            })
        }
    });  //END OF AJAX CALL
}


function searchOptionsDrivers() {
    var name = $(event.target).parent().children('#search').val().toLowerCase();
    $(event.target).parent().parent().children(".insured-row").filter(function (div) {
        for (var i = 0; i < $(this).children().length; i++) {
            if ($($(this).children()[i]).text().toLowerCase().trim().indexOf(name) > -1) {

                $(this).show();
                break;
            }
            else {
                //uncheck and hide
                if ($(this).children().length - 1 == i) {
                    $(this).children('.insured-check').children('input.driverCheck').prop('checked', false);
                    $(this).hide();
                }
            }
        }
    });
};

function searchOptionsVehicles() {
    var name = $(event.target).parent().children('#search').val();
    $(event.target).parent().parent().children("#vehiclesList").children('.vehicle-row').filter(function (div) {
        for (var i = 0; i < $(this).children().length; i++) {
            if ($($(this).children()[i]).text().trim().indexOf(name) > -1) {
                //show
                $(this).show();
                break;
            }
            else {
                //uncheck and hide
                if ($(this).children().length - 1 == i) {
                    $(this).children('.vehicle-check').children('input.vehcheck').prop('checked', false);
                    $(this).hide();
                }
            }
        }
    });
};


function changeVehicleCheckBox(e) {

    //Stores search value, as it breaks if the value is not stored before the div is manipulated
    var searchValue = $(e).parents(".insured-modal").find(".modalSearch").val();

    $(e).parent().parent().remove();
    if ($(e).parent().parent().hasClass("selected")) {

        $("#vehiclesList").append($(e).parent().parent().toggleClass("selected"));
        removeThisVeh();
    }
    else {
        //Clearing the array with the selected item
        $(".selected").children(".vehicle-check").children(".vehcheck").prop('checked', false);
        $("#vehiclesList").append($(".selected").toggleClass("selected"));
        removeThisVeh();

        //Adding the newly selected vehicle
        AddVehicleSelected(e.id);
    }
    searchVehicleData(searchValue);
}


function searchVehicle(e) {
    var filter = $(e).parent().children(".modalSearch").val();
    searchVehicleData(filter);
}


function searchVehicleData(filter) {
 $(".rowNone").remove();
    if (filter.trim().length == 0) {
        if ($("#vehiclesList").children(".vehicle-row").length <= 50) $("#vehiclesList").children(".vehicle-row").show();
        else $("#vehiclesList").children(".vehicle-row").hide();
    }
    else {
        $("#vehiclesList").children(".vehicle-row").each(function () {

            //If the person is already on display, in the select box, then it doesn't need to be shown in the search area again
            if (($(this).children(".chassisno").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".make").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".model").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".regno").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1)) {
                $(this).show();
                if ($("#vehiclesList").height() + $("#vehiclelist-selected").height() < 380) $("#vehiclesList").height($("#vehiclesList").height() + 20);
            }
            else $(this).hide();
        });
    }

    redrawList();
    if ($("#vehiclesList").height() == 0 && !$($(".vehicle-row")[0]).is(':hidden')) $(document).find("#vehiclesList").prepend(noInsuredVeh);
    else if ( !$('.vehicle-row').is(':visible') ) $(document).find("#vehiclesList").prepend(InsuredHiddenVeh);
}


function getValue() {

    var vals = 0; var CoverID; var usageIdentity;

    if (vehIDNum == 0 || typeof vehIDNum == 'undefined') {

        $("<div> A risk must be selected. Please select, by clicking on the risk. </div>").dialog({
            title: 'Alert!',
            modal:true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                }
            }
        });

    }

    else {
        $(".Vehicle").remove();


        $.ajax({
            type: "POST",
            url: '/Vehicle/getVehiclePolicyUsage/' + '?vehID=' + vehIDNum,
            // data: parameters,
            cache: false,
            success: function (data) {

                if (data.result) {

                    $("#policyId").val(data.polId);
                    $("#policyNo").val(data.polNo);

                    var parameters = { test: data.vehicle, type: "CertCreateClean", CoverID: data.cover, VehicleUsage: data.use }

                    $.ajax({
                        type: "POST",
                        url: '/Vehicle/DisplayVehicle/',
                        data: parameters,
                        cache: false,
                        success: function (html) {
                            $("#vehicles").append(html);

                        }
                    }); //END OF SECOND AJAX CALL



                     if ($(".Vehicle") != undefined) {

                        if ($("#usage").val() == undefined && $("#policyId").val() == "") {

                            $("<div> This vehicle is not under any policy, so will not be able to create a CoverNote. Would you like to create one now?</div>").dialog({
                                title: 'No Policy!',
                                modal:true,
                                buttons: {
                                    Yes: function () {
                                        var vehID = $(".vehId").val();
                                        window.location.replace("/Policy/create/?vehID="+vehID);
                                        $(this).dialog("close");
                                        $(this).dialog().dialog("destroy");
                                    },
                                    No: function () {
                                       $(this).dialog("close");
                                       $(this).dialog().dialog("destroy");
                                    }
                                }
                            });
                            $("#createCover").hide();
                            $("input.createNewPol").show();
                        }
                    }


                    $.ajax({
                        url: '/Policy/JsonPolicyData/' + data.polId,
                        cache: false,
                        success: function(json) {
                            if (json.result) {
                                $("#policyStartTime").val(json.policy_start_date);
                                $("#policyEndTime").val(json.policy_end_date);
                            }
                        }
                    });

                    //Pulling the new approriate wordings
                    $.ajax({
                        type: "POST",
                        url: "/TemplateWording/TemplateWordingFilter/" + data.vehicle + "?polId=" + $("#policyId").val(),
                        cache: false,
                        success: function (data) {

                            $.each(data, function (i, v) {

                                v = JSON.parse(v);

                                var elems = document.getElementsByClassName('formatDropdown');
                                for (var i = 0; i < elems.length; i++) {

                                    //clearing the data from the dropdowns

                                    if (typeof elems[i].options !== 'undefined') {
                                        for (p = elems[i].options.length - 1; p >= 0; p--) {
                                            elems[i].remove(p);
                                        }
                                    }

                                    // Placing the new options in the dropdown (if any)
                                    for (c = 0; c < v.length; c++) {
                                        var option = document.createElement("option");
                                        option.text = v[c].CertificateType;
                                        option.value = v[c].ID;

                                        elems[i].add(option);
                                    }

                                    if (elems[i].value != "") $("#wordingID").val(elems[i].value);
                                    else $("#wordingID").val("");

                                } //END OF DROPDOWN FOR LOOP

                                 $(elems).trigger("change");
                            }) //END OF FOREACH Template wording ITEM

                           

                        } // END OF SUCCESS FUNCTION
                    }); //End of third AJAX call
                }
            }
        }); //END OF MAIN AJAX


        //Generating the cover note ID
        $.ajax({
            type: "POST",
            url: "/VehicleCoverNote/GenerateCoverNoteNo/" + $("#companyId").val(),
            cache: false,
            success: function (data) {

                if (data.result)  $("#covernoteid").val(data.covernoteID);
                else $("#covernoteid").val(""); //popup modal informing that the vehicle is not currently under a policy

                $("#covernoteid").trigger("change");
            }
        }); //End of third AJAX call


        $("#vehicleId").val(vehIDNum);
        $("#addvehicle").hide();

        $("#vehicle-modal").dialog('close');
        $("#vehicle-modal").html("");


    } //END OF ELSE

    vehIDNum = 0;
    selectedVeh = null;


} // END OF FUNCTION 'getValue'

$(function () {
    $(document).on("click", "a.rmvehicle", function () {
        var deleteItem = $(this).parents("div.Vehicle:first");

        $("<div>You are about to delete this vehicle. Are you sure you want to do this? </div>").dialog({
            title: 'Alert!',
            modal: true,
            buttons: {
                OK: function () {
                    deleteItem.remove();
                    $("#addvehicle").show();
                    $("#policyStartTime").val("");
                    $("#policyEndTime").val("");
                    $("#createCover").show();
                    $("input.createNewPol").hide();
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");

                },
                Cancel: function () {
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                }
            }
        });
    });

    $(document).on("click", ".createNewPol", function () {
          var vehID = $(".vehId").val();
        window.location.replace("/Policy/create/?vehID="+vehID);
    });
});

//Closing the dialog window
function closeD() {
    $("#vehicle-modal").dialog("close");
    $("#vehicle-modal").html("");
    vehIDNum = 0;
    selectedVeh = null;
}

$(function () {
    if ($("#wordingID").val() != undefined) {
        var wordingTemp = $("#wordingID").val();
        if (wordingTemp != 0) {
            $(".formatDropdown").children().each(function () {
                if ($(this).val() == wordingTemp) {
                    $(this).prop('selected', true);
                    return;
                }
            });
        }
    }
});


$(function () {
    if ($('#Alert').text().trim().length != 0) {

        $("#Alert").dialog({
            modal: true,
            autoOpen: false,
            title: "Alert!",
            buttons:
                        {
                            OK:
                        function () {

                            $('#Alert').dialog('close');
                            $('#Alert').html("");

                        }
                    }
            });

        $('#Alert').dialog('open');
    }
});


$(function () {
    $(document).ready()
        {
        if (typeof $(".vehId")[0] != 'undefined')
            $("#addvehicle").hide();
        }
});


$(function () {
    if ($('#NotePrompt').text().trim().length != 0) {

        $("#NotePrompt").dialog({
            modal: true,
            autoOpen: false,
            title: "Alert!",
            buttons: {
                Proceed: function () {
                    $("#Continue").val("true");
                    $("#createCover").click();
                },
                Cancel: function () {
                    $("#Continue").val("");
                    $("#NotePrompt").dialog('close');
                    $("#NotePrompt").html("");
                }
            }
        });

        $('#NotePrompt').dialog('open');
    }
});

$(function () {
    
    $('#view-wording-temp').val( $('#wording').val() );

    $(document).on("change", "#wording", function (e) {
        $('#view-wording-temp').val($(this).val());
    });

    $(document).on("click", ".view-template-wording.icon", function (e) {
        var id = $(this).find('#view-wording-temp').val();
        window.location.replace('/TemplateWording/Details/' + id);
    });
});
