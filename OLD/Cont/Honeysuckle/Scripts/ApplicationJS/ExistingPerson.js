﻿var trnFocus = false;
var trnSet = false;

$(function () {
    if (typeof $("#trn").val() != 'undefined')
        if ($("#trn").val().trim().length > 0) {
            trnSet = true;
        }

    $("#trn").focus(function () {
        trnFocus = true;
    });

    $("#trn").blur(function () {
        if (trnFocus) {
            var test = $("#trn").val();
            if (test.trim().length > 0) {
                var result = false;
                //  $.getJSON("/Vehicle/GetUserByTRN/" + test.trim() +'?insured = ' + true, function (data) {
                $.ajax({
                    type: "POST",
                    url: "/Vehicle/GetUserByTRN/" + test.trim() + '?insured=' + true + "&test=" + true,
                    success: function (data) {
                        if (!data.error) {
                            if (data.result) {
                                //something exists
                                if (data.person) {
                                    if (data.taj) {
                                        //got from TAJ so need to create
                                        $("#fname").val(data.fname);
                                        $("#mname").val(data.mname);
                                        $("#lname").val(data.lname);
                                        $("#RoadNumber").val(data.roadno);
                                        $("#roadname").val(data.roadname);
                                        $("#roadtype").val(data.roadtype);
                                        $("#city").val(data.city);
                                    }
                                    else {
                                        //there is a person who's not a user yet

                                        $("#InformationDialog").dialog({
                                            modal: true,
                                            autoOpen: false,
                                            width: 430,
                                            title: "Person Exists",
                                            height: 350,
                                            buttons: {
                                                Ok: function () {
                                                    $(this).dialog("close");
                                                    $(this).dialog("destroy");
                                                    $("#InformationDialog").html("");

                                                    $("#insured-modal1").dialog("close");
                                                    $("#insured-modal1").html("");
                                                    AddPersonSelected(data.personid, 1);
                                                },
                                                Edit: function () {
                                                    $("#fname").val(data.fname);
                                                    $("#mname").val(data.mname);
                                                    $("#lname").val(data.lname);
                                                    $("#RoadNumber").val(data.roadno);
                                                    $("#roadname").val(data.roadname);
                                                    $("#roadtype").val(data.roadtype);
                                                    $("#city").val(data.city);
                                                    $("#aptNumber").val(data.aptNumber);
                                                    $("#parish").val(data.parishid);
                                                    $("#country").val(data.country);
                                                    $("#editingPolHolder").val(true);

                                                    $("#fname, #mname, #lname").prop('disabled', true);
                                                    $("#fname, #mname, #lname").toggleClass("read-only");

                                                    var thisparishId = data.parishid;
                                                    if (thisparishId != 0) {
                                                        $(".parishes").children().each(function () {
                                                            if ($(this).val() == thisparishId) {
                                                                $(this).prop('selected', true);
                                                                return;
                                                            }
                                                        });
                                                    }
                                                    $("#fname,#lname,#mname,#RoadNumber,#roadname,#roadtype,#city,#aptNumber,#parish,#country").trigger('keyup');
                                                    $('.parishes').trigger('change');
                                                    $(this).dialog("close");
                                                    $(this).dialog("destroy");
                                                    $("#InformationDialog").html("");
                                                },
                                                Cancel: function () {
                                                    $("#trn").val("");
                                                    $(this).dialog("close");
                                                    $(this).dialog("destroy");
                                                    $("#InformationDialog").html("");
                                                }
                                            }
                                        });
                                        $("#InformationDialog").load('/People/FullPersonDetails/' + data.personid + "?create=" + true + "&taj=" + data.taj, function (value) { }).dialog('open');

                                    }
                                }

                                else if (data.blank) {
                                    $("<div>This is not a valid TRN!</div>").dialog({
                                        modal: true,
                                        title: 'Alert!',
                                        buttons: {
                                            OK: function () {
                                                $(this).dialog('close');
                                                $("#trn").val("");
                                                $("#trn").focus();
                                                $("#trn").trigger('keyup');
                                            }
                                        }
                                    });
                                }
                                else {
                                    if (data.user) {
                                        //there is a user
                                        $("<div>The TRN you have entered is already listed to a current user in the system. The person will be added to the selected list. </div>").dialog({
                                            modal: true,
                                            title: "Alert",
                                            buttons: {
                                                Ok: function () {
                                                    cid = $("#compid").val();
                                                    $(".ui-dialog-content").dialog("close");

                                                    $("#insured-modal").load('/People/peopleList/' + cid, function (value) {

                                                        if ($(document).find(".insured-row").length <= 50)
                                                            $(document).find(".insured-row").show();

                                                        else $(document).find(".insured-row").hide();

                                                        $("#insureds").perfectScrollbar({
                                                            wheelSpeed: 0.3,
                                                            wheelPropagation: true,
                                                            minScrollbarLength: 10,
                                                            suppressScrollX: true
                                                        });

                                                    }).dialog('open');

                                                    AddPersonSelected(data.personid, 1);

                                                },
                                                Cancel: function () {
                                                    $("#trn").val("");
                                                    $(this).dialog("close");
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                            else {
                                //TRN FREE
                                if (test.trim().length != 0) trnSet = true;
                                $("#country").val("");

                            }
                        }
                        else {
                            //ERROR
                            $("<div>" + data.message + "</div>").dialog({
                                modal: true,
                                Title: "Alert",
                                buttons: {
                                    Ok: function () {
                                        $("#trn").val("");
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        }
                    }
                }); //End of ajax call
                trnFocus = false;
            }
        }
    });
});

function StartSpinner() {
    $("body").append('<div class="ui-widget-overlay ui-front temp"></div>');
    var opts = {
        lines: 13, // The number of lines to draw
        length: 11, // The length of each line
        width: 7, // The line thickness
        radius: 26, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#fff', // #rgb or #rrggbb or array of colors
        speed: 0.7, // Rounds per second
        trail: 33, // Afterglow percentage
        shadow: true, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: '50%', // Top position relative to parent
        left: '50%' // Left position relative to parent
    };
    var target = document.getElementById('spinner');
    spinner = new Spinner(opts).spin(target);
}

function StopSpinner() {
    $(".ui-front.temp").remove();
    var target = document.getElementById('spinner');
    spinner.stop(target);

}

function disableAll() {
    $("#fname,#mname,#lname,#RoadNumber,#roadname,#roadtype,.parishes,#city,#country").prop('disabled', true);
}

function enableAll() {
    $("#fname,#mname,#lname,#RoadNumber,#roadname,#roadtype,.parishes,#city,#country").prop('disabled', false);
}