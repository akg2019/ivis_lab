﻿var vehIDNum;
var selectedVeh;
var noInsuredVeh = "<div class=\"rowNone\">There Are No Vehicles. Please add one. </div>";
var InsuredHiddenVeh = "<div class=\"rowNone\">Please search the system for existing vehicles, or add a new vehicle. </div>";


$(function () {
    if ($("#wordingID").val() != undefined) {
        var wordingTemp = $("#wordingID").val();
        if (wordingTemp != 0) {
            $(".formatDropdown").children().each(function () {
                if ($(this).val() == wordingTemp) {
                    $(this).prop('selected', true);
                    return;
                }
            });
        }
    }

    if ($('#Alert').text().trim().length != 0) {
        $("#Alert").dialog({
            modal: true,
            autoOpen: false,
            title: "Alert!",
            buttons: {
                OK: function () {
                    $('#Alert').dialog('close');
                    $('#Alert').html("");
                }
            }
        });
        $('#Alert').dialog('open');
    }

    if ($("#vehicle-error").val() != undefined) {
        if ($("#vehicle-error").text().length > 0) {
            $("#vehicle-error").dialog({
                title: 'Alert',
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                        $("#vehicle-error").html("");
                    }
                }
            });
        }
    }

    $("input.createNewPol").hide();
    $("input#Submit2").show();

});

function isEmpty(el) {
    return !$.trim(el.html())
}

$(function () {
    $(document).on("click", ".row", function (e) {

        $("#vehiclesList").children(".vehicle-row").each(function () {

        //Removing the styling from the previously sected vehicle
        if ($(this).hasClass("selectedRow"))
            $(this).toggleClass("selectedRow");
        });

        vehIDNum = $(event.target).parent(".row").children(".vehicle_Id").val();
        $(event.target).parent(".row").toggleClass("selectedRow");

    }); //END CLICK
});

//////////////////////////////////////////////////////////////////////////

$(function () {

    $("#addvehicle").click(function () {

        var cid = $("#companyId").val();

        $("#vehicle-modal").dialog({
            modal: true,
            autoOpen: false,
            width: 800,
            title: "Vehicles List",
            height: 600

        }); //END .DIALOG

        $("#vehicle-modal").load('/Vehicle/VehiclesList/' + cid + '?type=' + 'certificate', function (value) {

            if (value.substring(2, 6) == 'fail') { //then user does not have the permissions
                $(this).dialog('close');
                $(this).html("");
                $(this).dialog().dialog("destroy");

                $("<div>You do not have the required permissions to complete this task. </div>").dialog({
                    title: 'Alert!',
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                            $(this).dialog().dialog("destroy");
                        }
                    }
                });
            }
            else {

                $("#create").remove();

                if ($(document).find(".vehicle-row").length <= 50) $(document).find(".vehicle-row").show();
                else $(document).find(".vehicle-row").hide();

                $(".select.item").hide();
                $(".vehicle-check").hide();

                $("#vehiclesList").perfectScrollbar({
                    wheelSpeed: 0.3,
                    wheelPropagation: true,
                    minScrollbarLength: 10,
                    suppressScrollX: true
                });
            }

        }).dialog('open');

    }); //END CLICK

    if ($(".chassisNumber").val() != undefined) {

        //Setting the usage for the vehicle
        var usagemodel = $(".chassisNumber").parent().children("#vehUsageID");
        var Usedropdownbox = $(".chassisNumber").parent().parent().parent().find(".useDropDown").children(".usage_dropdown");

        var usageID = usagemodel.val();

        Usedropdownbox.children().each(function () {
            if ($(this).val() == usageID) {
                $(this).prop('selected', true);
                // return;
            }
        });

        Usedropdownbox.prop("disabled", true);
    }

    

});


//Search when enter-button is hit
$(document).on("keyup", ".modalSearch", function (event) {
    var text = $(".modalSearch").val();
    if (event.which == 13) searchVehicleData(text);
});

function updateVehicleView() {
    if (vehIDNum != 0) {
        $("#vehiclelist-selected").append(selectedVeh);
        $(document).find(".vehicle-check").hide();
        $("#vehiclelist-selected").toggleClass("selectedRow");
    }
}

function removeThisVeh() { //This function removes the element from the arrays
    selectedVeh = null;
    vehIDNum = 0;
    updateVehicleView();
}

function AddVehicleSelected(id) {
    var err = "<div>There was an error with adding the vehicle. Please try again. If the problem persists, please contact your administrator.</div>"

    $.ajax({
        type: "POST",
        url: '/Vehicle/VehicleListItemSelected/' + id,
        cache: false,
        success: function (data) {

            selectedVeh = $(data);
            vehIDNum = id;

            updateVehicleView();
        },
        error: function () {
            $(err).dialog({
                title: "Error",
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog('close');
                    }
                }
            });
        }
    });   //END OF AJAX CALL
}


function searchOptionsDrivers() {
    var name = $(event.target).parent().children('#search').val().toLowerCase();
    $(event.target).parent().parent().children(".insured-row").filter(function (div) {
        for (var i = 0; i < $(this).children().length; i++) {
            if ($($(this).children()[i]).text().toLowerCase().trim().indexOf(name) > -1) {

                $(this).show();
                break;
            }
            else {
                //uncheck and hide
                if ($(this).children().length - 1 == i) {
                    $(this).children('.insured-check').children('input.driverCheck').prop('checked', false);
                    $(this).hide();
                }
            }
        }
    });
}

function searchOptionsVehicles() {
    var name = $(event.target).parent().children('#search').val();
    $(event.target).parent().parent().children("#vehiclesList").children('.vehicle-row').filter(function (div) {
        for (var i = 0; i < $(this).children().length; i++) {
            if ($($(this).children()[i]).text().trim().indexOf(name) > -1) {
                //show
                $(this).show();
                break;
            }
            else {
                //uncheck and hide
                if ($(this).children().length - 1 == i) {
                    $(this).children('.vehicle-check').children('input.vehcheck').prop('checked', false);
                    $(this).hide();
                }
            }
        }
    });
}


function changeVehicleCheckBox(e) {

    //Stores search value, as it breaks if the value is not stored before the div is manipulated
    //var searchValue = $(e).parent().parent().parent().parent().children("#searching").children("#search").val();
    var searchValue = $(e).parents(".insured-modal").find(".modalSearch").val();

    $(e).parent().parent().remove();
    if ($(e).parent().parent().hasClass("selected")) {
        $("#vehiclesList").append($(e).parent().parent().toggleClass("selected"));
        removeThisVeh();
    }
    else {
        //Clearing the array with the selected item
        $(".selected").children(".vehicle-check").children(".vehcheck").prop('checked', false);
        $("#vehiclesList").append($(".selected").toggleClass("selected"));
        removeThisVeh();

        //Adding the newly selected vehicle
        AddVehicleSelected(e.id);
    }
    searchVehicleData(searchValue);
}

function searchVehicle(e) {
    var filter = $(e).parent().children(".modalSearch").val();
    searchVehicleData(filter);
}

function searchVehicleData(filter) {
    $(".rowNone").remove();
    if (filter.trim().length == 0) {
        if ($("#vehiclesList").children(".vehicle-row").length <= 50) $("#vehiclesList").children(".vehicle-row").show();
        else $("#vehiclesList").children(".vehicle-row").hide();
    }
    else {
        $("#vehiclesList").children(".vehicle-row").each(function () {

            //If the person is already on display, in the select box, then it doesn't need to be shown in the search area again
            if (($(this).children(".chassisno").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".make").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".model").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1) || ($(this).children(".regno").html().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) > -1)) {
                $(this).show();
                if ($("#vehiclesList").height() + $("#vehiclelist-selected").height() < 380) $("#vehiclesList").height($("#vehiclesList").height() + 20);
            }
            else $(this).hide();
        });
    }
    redrawList();

    if ($("#vehiclesList").height() == 0 && !$($(".vehicle-row")[0]).is(':hidden')) $(document).find("#vehiclesList").prepend(noInsuredVeh);
    else if (!$('.vehicle-row').is(':visible')) $(document).find("#vehiclesList").prepend(InsuredHiddenVeh);

}


function getValue() {

    var vals = 0; var CoverID; var usageIdentity;

    if (vehIDNum == 0 || typeof vehIDNum == 'undefined') {

        $("<div> A risk must be selected. Please select, by clicking on the risk. </div>").dialog({
            title: 'Alert!',
            modal:true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                }
            }
        });
    }
    else {
        $(".Vehicle").remove();

        $.ajax({
            type: "POST",
            url: '/Vehicle/getVehiclePolicyUsage/' + '?vehID=' + vehIDNum,
            // data: parameters,
            cache: false,
            success: function (data) {

                if (data.result) {

                    $("#policyId").val(data.polId);
                    $("#policyNo").val(data.polNo);

                    var parameters = { test: data.vehicle, type: "CertCreateClean", CoverID: data.cover, VehicleUsage: data.use }

                    $.ajax({
                        type: "POST",
                        url: '/Vehicle/DisplayVehicle/',
                        data: parameters,
                        cache: false,
                        success: function (html) {
                            $("#vehicles").append(html);

                        }
                    }); //END OF SECOND AJAX CALL

                    if ($(".Vehicle") != undefined) {

                        if ($("#usage").val() == undefined && $("#policyId").val() == "") {

                            $("<div> This vehicle is not under any policy. Would you like to create one now?</div>").dialog({
                                title: 'No Policy!',
                                modal: true,
                                buttons: {
                                    Yes: function () {
                                        var vehID = $("#vehicleId").val();
                                        window.location.replace("/Policy/create/?vehID=" + vehID);
                                        $(this).dialog("close");
                                        $(this).dialog().dialog("destroy");
                                    },

                                    No: function () {
                                        $(this).dialog("close");
                                        $(this).dialog().dialog("destroy");
                                    }
                                }
                            });

                            $("input.createNewPol").show();
                            $("input#Submit2").hide();

                        }
                    }
                    //getting policy date range
                    $.ajax({
                        type: "POST",
                        url: '/Policy/JsonPolicyData/' + data.polId,
                        data: parameters,
                        cache: false,
                        success: function (json) {
                            if (json.result) {
                                $("#effective_date_policy").val(json.policy_start_date);
                                $("#effective_date_policy").trigger("change");
                                $("#end_date_policy").val(json.policy_end_date);
                            }
                        }
                    });


                    //Pulling the new approriate wordings
                    $.ajax({
                        type: "POST",
                        url: "/TemplateWording/TemplateWordingFilter/" + data.vehicle + '?polId=' + data.polId,
                        cache: false,
                        success: function (data) {

                            $.each(data, function (i, v) {

                                v = JSON.parse(v);

                                var elems = document.getElementsByClassName('formatDropdown');
                                for (var i = 0; i < elems.length; i++) {

                                    //clearing the data from the dropdowns

                                    if (typeof elems[i].options !== 'undefined') {
                                        for (p = elems[i].options.length - 1; p >= 0; p--) {
                                            elems[i].remove(p);
                                        }
                                    }

                                    // Placing the new options in the dropdown (if any)
                                    for (c = 0; c < v.length; c++) {
                                        var option = document.createElement("option");
                                        option.text = v[c].CertificateType;
                                        option.value = v[c].ID;

                                        elems[i].add(option);
                                    }

                                    if (elems[i].value != "") $("#wordingID").val(elems[i].value);
                                    else $("#wordingID").val("");

                                } //END OF DROPDOWN FOR LOOP

                                $(elems).trigger("change");

                            }) //END OF FOREACH Template wording ITEM

                        } // END OF SUCCESS FUNCTION
                    }); //End of third AJAX call


                }
            }
        });          //END OF MAIN AJAX

        $("#vehicleId").val(vehIDNum);
        $("#addvehicle").hide();

        $("#vehicle-modal").dialog('close');
        $("#vehicle-modal").html("");

    } //END OF MAIN ELSE

    vehIDNum =0;
    selectedVeh = null;
           

} // END OF FUNCTION 'getValue'


$(function () {
    $(document).on("click", "a.rmvehicle", function () {
        var deleteItem = $(this).parents("div.Vehicle:first");

        $("<div>You are about to delete this vehicle. Are you sure you want to do this? </div>").dialog({
            title: 'Alert!',
            modal:true,
            buttons: {
                OK: function () {
                    deleteItem.remove();
                    $("#effective_date_policy").val("")
                    $("#effective_date_policy").trigger("change");
                    $("#end_date_policy").val("")
                    var now = new Date($("#now_date_time").val());
                    $("#effective").val(now.dateFormat('d M, Y h:i A')),
                    $("#addvehicle").show();
                    $("input.createNewPol").hide();
                    $("input#Submit2").show();
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");

                },
                Cancel: function () {
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                }
            }
        });
    });

    $(document).on("click", ".createNewPol", function () {
        var vehID = $("#vehicleId").val();
        window.location.replace("/Policy/create/?vehID="+vehID);
    });


});

//Closing the dialog window
function closeD() {
    $("#vehicle-modal").dialog("close");
    $("#vehicle-modal").html("");
    vehIDNum = 0;
    selectedVeh = null;
}

$(function () {
    var now = new Date($("#now_date_time").val());
    $("#effective").datetimepicker({
        value: effective_or_now().dateFormat('d M, Y h:i A'),
        format: 'd M, Y h:i A',
        onShow: function (ct) {
            this.setOptions({
                minDate: get_effective_date(),
                maxDate: get_expiry_date()
            });
        }
    });

    $("#effective").trigger("change");

});


$(function () {
    if ($("#vehicles").hasClass("Vehicle")) {

        if ($("#usage").val() == undefined && $("#policyId").val() == undefined) {
            $("<div> This vehicle is not under any policy. Would you like to create one now?</div>").dialog({
                title: 'No Policy!',
                modal:true,
                buttons: {

                    Yes: function () {
                        var vehID = $("#vehicleId").val();
                        window.location.replace("/Policy/create/?vehID=" + vehID);
                        $(this).dialog("close");
                        $(this).dialog().dialog("destroy");
                    }

                   , Cancel: function () {
                       $(this).dialog("close");
                       $(this).dialog().dialog("destroy");
                   }
                }
            });
        }
    }
});

function effective_or_now() {
    var now = get_now_date();
    var effective = get_effective_date();
    if (effective > now) return effective;
    else return now;
}

function get_effective_date() {
    //var effective = new Date($("#effectivedate_policy").val());
    var effective;
    if ($("#effective_date_policy").val() != "") effective = new Date($("#effective_date_policy").val());
    else effective = get_now_date();
    return effective;
}

function get_now_date() {
    var now = new Date($("#now_date_time").val());
    return now;
}

function get_expiry_date() {
    var expiry;
    if ($("#end_date_policy").val() != "") expiry = new Date($("#end_date_policy").val());
    else expiry = new Date($("#now_date_time").val());
    return expiry;
}

$(function () {
    if ($('#CertPrompt').text().trim().length != 0) {

        $("#CertPrompt").dialog({
            modal: true,
            autoOpen: false,
            title: "Alert!",
            buttons: {
                Proceed: function () {
                    $("#Continue").val("true");
                    $("#Submit2").click();
                },
                Cancel: function () {
                    $("#Continue").val("");
                    $("#CertPrompt").dialog('close');
                    $("#CertPrompt").html("");
                }
            }
        });

        $('#CertPrompt').dialog('open');
    }
});

$(function () {

    $('#view-wording-temp').val( $('#wording').val() );

    $(document).on("change", "#wording", function (e) {
        $('#view-wording-temp').val($(this).val());
    });

    $(document).on("click", ".view-template-wording.icon", function (e) {
        var id = $(this).find('#view-wording-temp').val();
        window.location.replace('/TemplateWording/Details/' + id);
    });
});