﻿$(function () {

    if ($("#page").val() == "details") {
        $(".vup_mortgagee").toggleClass("read-only");
        $(".vup_mortgagee").prop('readonly', true);
    }
    else {
        $(document).on("focus", ".vup_mortgagee", function (e) {
            if (!$(this).data("autocomplete")) { // If the autocomplete wasn't called yet:
                $(this).autocomplete({             //   call it
                    source: function (req, resp) {
                        $.getJSON('/Mortgagee/GetMortgagees?type=' + req.term, function (data) {
                            var values = []
                            $.each(data, function (i, v) {
                                values.push(v.value);
                            })
                            resp(values);
                        });
                    }
                });
            }
        });

        $(document).on("blur", ".vup_mortgages", function () {
            var mortgagee = $(this).val();
            if (mortgagee.length > 0 && $("#hiddenid")) {
                var vehid = $(this).parents(".newvehicle").find(".vehId").val();
                var polid = $("#hiddenid").val();

            }
        });
    }

});