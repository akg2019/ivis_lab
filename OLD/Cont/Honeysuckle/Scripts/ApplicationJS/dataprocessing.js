﻿$(function () {

    $(':required, input').each(function () {
        validate_element($(this));
    });

    $('html').on('change','input,select:not(#sendto),.usage_dropdown', function () {
        validate_element($(this));
    });

    $('html').on('keyup', 'input', function () {
        validate_element($(this));
    });
    
//   $('body').on('change', '.usage_dropdown', function (){
//       validate_element($(this));
//   });

    

    $('html').on('click',"input[type='submit']", function () {
        if (!validate_page()) {
            show_first_section_not_valid();
            return false;
        }
        else return true;
    });

});

function validate_element(e) {
    var checkElemems = ( !$(e).is('#aptNumber') && !$(e).is('#RoadNumber') && !$(e).is('#roadname') && !$(e).is('#roadtype') && !$(e).is('#mname') && !$(e).is('#vin') && !$(e).is('#edit_fitnessExpiry') && !$(e).is('#search') && !$(e).is(':checkbox'));

    if (validate_field($(e).val(), get_validation_type($(e)))){
         $(e).addClass('ok');
         if($('#page').val() == "vehEdit" && checkElemems )
            $(e).prop('required',false);
        if($(e).hasClass('extNum') || $(e).is('#mname') || $(e).is("#vehiclemodel") || $(e).is("#vehicleRegNum") || $(e).is("#VehicleRegNumber") || $(e).is('#company-trn-not-available')  || $(e).is('#person-trn-not-available'))
            $(e).prop('required',false);
     }
    else {
        $(e).removeClass('ok');
        if($('#page').val() == "vehEdit" && checkElemems)
            $(e).prop('required',true);
        if($(e).hasClass('extNum') || $(e).is('#mname') || $(e).is("#vehiclemodel") || $(e).is("#vehicleRegNum") || $(e).is("#VehicleRegNumber") || $(e).is('#company-trn-not-available') || $(e).is('#person-trn-not-available'))
            $(e).prop('required',true);
    }
}

function validate_field(content, type) {
    var regex;
    var result = false;

    if((content == 0 || content == null) && type == 'parishes-edit')
        return true;

    if (type == 'enable' || type == 'ignore') result = true;
    else if (type == 'policynumber' && $(".no_policy_no_check").is(":checked")) result = true;
    else if (content == undefined) {
        result = (
            type == 'roadname' ||
            type == 'roadno' ||
            type == 'roadtype' ||
            type == 'apt' ||
            type == 'ext-code' ||
            type == 'mname' ||
            type == 'ppn' ||
            type == 'covernoteno' ||
            type == 'alterations' ||
            type == 'password'
        );
    }
    else if (content.length == 0) {
        result = (
            type == 'roadname' ||
            type == 'roadno' ||
            type == 'roadtype' ||
            type == 'apt' ||
            type == 'ext-code' ||
            (type == 'company-code-short' &&  !$("#ComShortening").prop("required")) ||
            type == 'mname' ||
            type == 'ppn' ||
            type == 'covernoteno' ||
            type == 'alterations' ||
            type == 'password'||
            type == 'estimatedValue' ||
            type == 'modeltype' ||
            type == 'bodytype' ||
            type === 'extension' ||
            type == 'colour' ||
            type == 'cylinders' ||
            type == 'engineno' ||
            type == 'engineType' ||
            type == 'hpccUnit' ||
            type == 'refno' ||
            type == 'regLocation' ||
            type == 'roofType' ||
            type == 'tranhandType' ||
            type == 'vehicleInt' ||
            type == 'regOwner' ||
            type == 'driver-trn' ||
            type == 'driver-fname' ||
            type == 'driver-lname' ||
            type == 'city-edit' ||
            type == 'country-edit' ||
            type == 'extNum' ||
            type == 'vehiclemodel' ||
            type == 'vehicleRegNum' ||
            type == 'longAddress' ||
            type == 'CompanyTRN-not-available' ||
            type == 'person-trn-not-available'||
            (type == 'BrokerPolicyNum' &&  $('#BrokerPolicyNum').is(':hidden'))
        );
    }
    else {
        content = content.trim();
        switch (type) {
            case 'roadno':
                regex = /^([a-zA-Z0-9])[ a-zA-Z0-9-,/& ]*[a-zA-Z0-9 ]*$/;
                break;
            case 'roadname':
                regex = /^[a-zA-Z](([a-zA-Z0-9&,#. '-])*)$/;
                break;
            case 'apt':
                regex = /^(([a-zA-Z0-9&,#. '-])*)$/;
                break;
            case 'datetime':
                regex = /^[0-9]{1,2}[ ][a-z]+([,])?[ ][0-9]{2,4}[ ][0-9]{1,2}[:][0-9]{1,2}([ ][a|p][m])?$/;
                break;
            case 'date':
                regex = /^[0-9]{1,2}[ ][a-z]+([,])?[ ][0-9]{2,4}$/;
                break;
            case 'words':
                regex = /^[a-z ]+$/;
                break;
            case 'tranhandType':
            case 'colour':
            case 'company-code':
            case 'company-code-short':
            case 'roadtype':
                regex = /^[a-zA-Z]+$/;
                break;
            case 'covernoteid':
                regex = /^[a-z]+[-][0-9]+$/;
                break;
            case 'vehicleInt':
            case 'cylinders':
            case 'number':
            case 'iajid':
            case 'floginlimit':
            case 'printcount':
            case 'ppn':
                regex = /^[0-9]+$/;
                break;
            case 'parishes-edit':
            case 'parishes':
            case 'dropdown':
            case 'period':
                regex = /^[1-9][0-9]*$/;
                break;
            case 'refno':
            case 'type':
            case 'alphanum':
                regex = /^[a-z0-9]+$/;
                break;
            case 'country-edit':
            case 'more-words':
            case "country":
                regex = /^[a-zA-Z](([a-zA-Z0-9.& '-])*[a-zA-Z0-9])?$/;
                break;
            case 'city-edit':
            case "city":
                regex = /^(([a-zA-Z](([a-zA-Z0-9.& '-])*[a-zA-Z0-9])?)|([a-zA-Z](([a-zA-Z0-9.& '-])*([ ])?[(]([a-zA-Z0-9.& '-])+[a-zA-Z0-9][)])?))$/;
                break;
            case 'roadname':
                regex = /^[a-zA-Z](([a-zA-Z0-9&,#. '-])*)?$/;
                break;
            case 'company-trn':
            case 'CompanyTRN-not-available':
                regex = /^(([ ]*[01][0-9]{12}[ ]*)|([ ]*[0][0-9]{8}[ ]*))$/;
                break;
            case 'driver-trn':
            case 'trn':
            case 'person-trn':
            case 'person-trn-not-available':
                regex = /^(([1][0-9]{8}))$/;
                break;
            case 'email':
                regex = /^([a-zA-Z0-9.-_]+[@][a-zA-Z0-9]+([.][a-z]+)+)$/;
                break;
            case 'regOwner':
                regex = /^[a-zA-Z0-9][a-zA-Z0-9\-@!?\/#&$+,.\' ]*$/;
                break;
            case 'driver-lname':
            case 'driver-fname':
            case 'name':
            case 'mname':
                regex = /^(([a-zA-Z])[a-zA-Z-'. ]*)$/;
                break;
            case 'certtype':
                regex = /^[a-z]+[.][a-z][.][0-9a-z]+$/
                break;
            case 'certid':
                regex = /^[a-z]+[.][a-z][.][0-9]+[:](indv|jtin|inco)[:](x)?(r)?(m)?(e)?(o)?(s)?$/
                break;
            case 'ext-code':
                regex = /^(x)?(r)?(m)?(e)?(o)?(s)?$/;
                break;
            case "certinscodedropdown":
                regex = /^(indv|jtin|inco|blnk|comp)$/;
                break;
            case 'usage_dropdown':
            case 'cover':
            case 'prefix':
            case 'anything':
            case 'alterations':
            case 'covernoteno':
            case 'taj':
                regex = /^.+$/;
                break;
            case 'regLocation':
            case 'hpccUnit':
            case 'engineType':
            case 'engineno':
            case 'extension': 
            case 'modeltype':
            case 'any-nothing':
            case 'BrokerPolicyNum':
            case 'roofType':
            case 'bodytype':
                regex = /^.*$/;
                break;
            case 'company-name':
            case 'longAddress':
                regex = /[^()]/;
                break;
            case 'chassis':
                regex = /^[a-zA-Z0-9]+$/;
                break;
            case 'vehiclemake':
                regex = /^[a-zA-Z0-9][a-zA-Z0-9-.,\'& ]*[a-zA-Z0-9]$/;
                break;
            case 'vehiclemodel':
                regex = /^[a-zA-Z0-9][a-zA-Z0-9-.&\', ]*[a-zA-Z0-9]*$/;
                break;
            case 'vehicleRegNum':
                regex = /^[a-zA-Z0-9]+$/;
                break;
            case 'policynumber':
            //case 'BrokerPolicyNum':
                regex = /^[a-zA-Z0-9][a-zA-Z0-9- ]*[a-zA-Z0-9]$/;
                break;
            case 'password':
                regex = /^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*()]))[a-zA-Z0-9!@#$%^&*()]{8,}$/;
                break;
            case 'telNum':
                //regex = /^([0-9-()+ ]+)$/;
                regex = /^[(][0-9]{3}[)][0-9]{3}[-][0-9]{4}$/;
                break;
            case 'extNum':
                regex = /^([0-9-()+ ]*)$/;
                break;
            case 'item-collection':
                regex = /^[a-fA-F0-9]{8}(?:-[a-fA-F0-9]{4}){3}-[a-fA-F0-9]{12}$/;
                break;
            case 'estimatedValue':
                regex = /^([$])?[1-9][0-9]{0,2}(([,][0-9]{3})*|([0-9]*))([.][0-9]{0,2})?$/;
                break;
            default:
                break;
        }
    }
    if (regex != undefined) {
        result = regex.test(content.toLowerCase());
    }
    return result;
}

function validate_passwords(){
    var result = false;
    regex = /^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*()]))[a-zA-Z0-9!@#$%^&*()]{8,}$/;
    result = $("#password1").val() == $("#password2").val();
    if (result) result = ($("#password1").val().length == 0 || regex.test($("#password1").val()));
    if (result) result = ($("#password2").val().length == 0 || regex.test($("#password2").val()));
    return result;
}

function get_validation_type(e) {
    var type = '';
    if ($(e).hasClass('number') || $(e).is("#vehicleYear")) type = 'number';
    if ($(e).hasClass('words')) type = 'words';
    if ($(e).hasClass('alphanum')) type = 'alphanum';
    if ($(e).hasClass('more-words')) type = 'more-words';
    if ($(e).hasClass('datetime')) type = 'datetime';
    if ($(e).hasClass('roadname')) type = 'roadname';
    if($(e).hasClass('RoadNumber')) type='roadno';
    if ($(e).is('#RoadNumber')) type = 'roadno';
    if ($(e).is('#roadtype')) type = 'roadtype';
    if($(e).hasClass('roadtype')) type = 'roadtype';
    if ($(e).is('#aptNumber')) type = 'apt';
    if($(e).hasClass('aptNumber')) type = 'apt';
    if ($(e).hasClass('city')) type = 'city';
    if ($(e).hasClass('country')) type = 'country';
    if ($(e).hasClass('parishes')) type = 'parishes';
    
    if ($(e).hasClass('floginlimit') || $(e).is("#floginlimit")) type = 'floginlimit';
    if ($(e).hasClass('usage-dropdown') || $(e).hasClass('usage_dropdown')) type = 'dropdown';
    if ($(e).hasClass('covers_dropdown')) type = 'dropdown';
    if ($(e).hasClass('formatDropdown')) type = 'dropdown';
    if ($(e).hasClass('inter-dropdown')) type = 'dropdown';
    if ($(e).hasClass('company_dropdown')) type = 'dropdown';
    if ($(e).hasClass('name')) type = 'name';
    if ($(e).hasClass('user-group-name')) type = 'name';
    if ($(e).hasClass('certid')) type = 'certid';
    if ($(e).hasClass('certtype')) type = 'certtype';
    if ($(e).hasClass('ext-code')) type = 'ext-code';
    if ($(e).hasClass('email') || $(e).is('#email')) type = 'email';
    if ($(e).hasClass('cover')) type = 'cover';
    if ($(e).hasClass('prefix')) type = 'prefix';
    if ($(e).is("#IAJID")) type = "iajid";
    if ($(e).is("#period")) type = "period";
    if ($(e).is("#covernoteid")) type = "covernoteid";
    if ($(e).hasClass('printcount')) type = "printcount";
    if ($(e).hasClass('type')) type = "type";
    if ($(e).hasClass('chassis') || $(e).is('#chassis')) type = "chassis";
    if ($(e).is("#certinscodedropdown")) type = "certinscodedropdown";
    if ($(e).is("#CompanyName")) type = "company-name";
    if ($(e).is("#compInsCode")) type = "company-code";
    if ($(e).is("#ComShortening")) type = "company-code-short";
    if ($(e).is("#vehiclemodel")) type = "vehiclemodel";
    if ($(e).is("#make")) type = "vehiclemake";
    if ($(e).is("#vehicleRegNum") || $(e).is("#VehicleRegNumber")) type = "vehicleRegNum";
    if ($(e).is("#trn") && !($(e).hasClass('company-trn'))) type = "trn";
    if ($(e).hasClass('company-trn')) type = 'company-trn';
    if ($(e).hasClass('person-trn')) type = 'person-trn';
    if ($(e).is("#fname") || $(e).is("#lname")) type = 'name';
    if ($(e).is("#city")) type = 'city';
    if ($(e).is("#country")) type = 'country';
    if ($(e).is("#roadname")) type = 'roadname';
    if ($(e).is("#enable")) type = 'enable';
    if ($(e).hasClass('user-group-name')) type = 'anything';
    if ($(e).hasClass('anything')) type = 'anything';
    if ($(e).is('#mname')) type = 'mname';
    if ($(e).hasClass('quick-search')) type = 'ignore';
    if ($(e).hasClass('ignore')) type = 'ignore';
    if ($(e).hasClass('read-only')) type = 'ignore';
    if ($(e).parents('.permissions,.groups,.chosen-search').length > 0) type = 'ignore';
    if ($(e).is("#policyNum")) type = "policynumber";
    if ($(e).is("#ppn")) type = "ppn";
    if ($(e).is("#alterations")) type = "alterations";
    if ($(e).is("#covernoteno")) type = "covernoteno";
    if ($(e).hasClass('password')) type = 'password';
    if($(e).hasClass('telNum')) type = 'telNum';
    if($(e).hasClass('extNum')) type = 'extNum'
    if($(e).hasClass('taj')) type = 'taj';
    if($(e).is('#estimatedValue') || $(e).hasClass('estimatedValue')) type = 'estimatedValue'; 
    //if($(e).hasClass('usage_dropdown')) type = 'usage_dropdown';

    //if($(e).is('#IAJID') && $(e).hasClass('IAJID-create')) type = 'IAJID-create'; 

    //Vehicle Edit Checks
    if($(e).is('#modeltype')) type = 'modeltype';
    if($(e).is('#bodytype')) type = 'bodytype';
    if($(e).is('#extension')) type = 'extension';
    if($(e).is('#colour')) type = 'colour';
    if($(e).is('#cylinders')) type = 'cylinders';
    if($(e).is('#engineNo')) type = 'engineno';
    if($(e).is('#engineType')) type = 'engineType';
    if($(e).is('#hpccUnit')) type = 'hpccUnit';
    if($(e).is('#refno')) type = 'refno';
    if($(e).is('#regLocation')) type = 'regLocation';
    if($(e).is('#roofType')) type = 'roofType';
    if($(e).is('#tranType') || $(e).is('#handDriveType')) type = 'tranhandType';
    if($(e).is('#seating') || $(e).is('#seatingCert') || $(e).is('#tonnage') || $(e).is("#mileage")) type = 'vehicleInt';
    if($(e).is('#regOwner')) type = 'regOwner';
    if($(e).is('#edit_fitnessExpiry')) type = 'date';
    if($(e).is("#country")) type = "country";
    if($(e).hasClass('country') && $(e).hasClass('vehicle-edit')) type = 'country-edit';
    if($(e).hasClass('city') && $(e).hasClass('vehicle-edit')) type = 'city-edit';
    if($(e).hasClass('parishes') && $(e).parents('.editor-field').hasClass('vehicle-edit')) type = 'parishes-edit';
    if($(e).hasClass('driver-trn')) type = 'driver-trn';
    if($(e).hasClass('driver-fname')) type = 'driver-fname';
    if($(e).hasClass('driver-lname')) type = 'driver-lname';
    //End Vehicle Edit Checks

    //longAddress
    if($(e).is('#longAddress')) type = 'longAddress';

    //no TRN
    if($(e).is('#company-trn-not-available')) type = 'CompanyTRN-not-available';
    if($(e).is('#person-trn-not-available')) type = 'person-trn-not-available';

    if($(e).is('#BrokerPolicyNum')) type = 'BrokerPolicyNum';

    if ($(e).hasClass('any-nothing')) type = 'any-nothing';
    if (type == '' && $(e).is(':hidden')) type = 'item-collection';

    return type;
}

var valid_icon   = "<img src=\"../../Content/more_images/succeed.png\" />";
var invalid_icon = "<img src=\"../../Content/more_images/fail.png\" />";


$(function () {

    $(".validate-section .data").hide();
    $(".validate-section .data:first").show();

    $("body").on("click", ".validate-section .subheader:not(.vehicleChassisHeader,.dont-process)", function () {
        var validobj = new ValidObj(false, "");
        var keep_going = true;
        
        ///dont validate policy-billing address
        if ($(this).parents('.validate-section').hasClass('policy-billing-address')){
            //dont validate if check is checked
            if ($(".billing.check").is(":checked")) {
                $(this).find(".valid").html(valid_icon + "<div class=\"valid-message\">Data Valid</div>"); //valid
                $("<div></div>").text("If you would like to modify this policy's billing address please uncheck the billing address check.").dialog({
                    title:'Billings Check',
                    modal:true,
                    buttons: {
                        Ok: function(){
                            $(this).dialog('close');
                        }
                    }
                });
                keep_going = false;
            }
        }
        
        if (keep_going){
            if (is_visible($(this).parents(".validate-section"))){
                validobj = validate_section($(this).parents(".validate-section"));
                if (validobj.result) $(this).find(".valid").html(valid_icon + "<div class=\"valid-message\">Data Valid</div>"); //valid
                else $(this).find(".valid").html(invalid_icon + "<div class=\"invalid-message\">" + validobj.errors + "</div>"); //invalid

                collapse_section($(this).parents(".validate-section"));
            }
            else{
                var section = $(".data:visible").parents(".validate-section");
                validobj = validate_section(section);

                if (validobj.result) $(section).find(".valid").html(valid_icon + "<div class=\"valid-message\">Data Valid</div>"); //valid
                else $(section).find(".valid").html(invalid_icon + "<div class=\"invalid-message\">" + validobj.errors + "</div>"); //invalid

                if($(".open-sections").hasClass('close')) collapse_section($(section));
                open_section($(this).parents(".validate-section"));
            }
        }

    });
});

function validate_section(e) {
    var validobj = new ValidObj(true, "");
    if ($(e).hasClass('policyholders')){
        validobj.result = ($(".insured").length > 0 || $(".company").length > 0);
        if (!validobj.result) validobj.errors = "You must select at least one policyholder.";
    }
    else if ($(e).hasClass('policy-risk')){

        validobj.result =  $('.usage_dropdown').val() != null;
        if(!validobj.result) validobj.errors = "Vehicle usage";



        if($(".newvehicle").length <= 0){
            validobj.result = false;
            if (!validobj.result) validobj.errors = "You must select at least one risk.";
        }
        if($('.underPolicy').length > 0){
            validobj.result = false;
            if (!validobj.result) 
            {
                validobj.errors = "One or more of the selected risks need to be dealt with.";
                if ($('.dbl-cover-modal').length == 0){
                    $("<div class='dbl-cover-modal'></div>").text('There are one or many vehciles selected for this policy that are covered under previous policies. Please confirm that you would like to add these vehicles to this policy by pressing \'proceed\' on each vehicle that is pending.').dialog({
                        title:'Double Cover',
                        modal:true,
                        buttons:{
                            Ok: function(){
                                $(this).dialog('close');
                                $(this).dialog('destroy');
                            }
                        }
                    });
                }
            }
        }
    }
    else if($(e).hasClass('policy-veh-drivers') || $(e).hasClass('cover-usages') || $(e).hasClass('permission-header')){
        //DO NOTHING HERE
    }
    else if($(e).hasClass('password-area')){
        validobj.result = validate_passwords();
        if (!validobj.result) validobj.errors = "Your passwords must both match and follow the predetermined requirements.";
    }
    else if ($(e).hasClass('policy-billing-address')){
            //dont validate if check is checked
        if ($(".billing.check").is(":checked")) {
            $(e).find(".valid").html(valid_icon + "<div class=\"valid-message\">Data Valid</div>"); //valid
        }
        else{
            $(e).find("input,select").each(function () {
                if (!(validateCheckBoxes($(this)) || $(this).is(':checkbox') || $(this).hasClass('dont-process') || $(this).is(':submit'))) {
                    if (!(validate_field($(this).val(), get_validation_type($(this))))) {
                        validobj.result = false;

                        if($('#page').length > 0 && $('#page').val() == "EditContactPage"){
                            if (validobj.errors == "" || validobj.errors == undefined) validobj.errors = $(this).prev().html().trim();
                            else validobj.errors = validobj.errors + ',' + $(this).prev().html().trim();
                        }
                        else if (validobj.errors == "" || validobj.errors == undefined) validobj.errors = get_field_name($(this).parents('.editor-field'));
                        else validobj.errors = validobj.errors + ',' + get_field_name($(this).parents('.editor-field'));
                    } 
                }
            });
        }
    }
    else{
        $(e).find("input,select").each(function () {
            if (!(validateCheckBoxes($(this)) || $(this).is(':checkbox') || $(this).hasClass('dont-process') || $(this).is(':submit') || ($(this).is('#compInsCode') && !($(this).is(':required')) ))) {
                if (!(validate_field($(this).val(), get_validation_type($(this))))) {
                    validobj.result = false;
                    if($('#page').length > 0 && $('#page').val() == "EditContactPage"){
                        if (validobj.errors == "" || validobj.errors == undefined) validobj.errors = $(this).prev().html().trim();
                        else validobj.errors = validobj.errors + ',' + $(this).prev().html().trim();
                    }
                    else if (validobj.errors == "" || validobj.errors == undefined) validobj.errors = get_field_name($(this).parents('.editor-field'));
                    else validobj.errors = validobj.errors + ',' + get_field_name($(this).parents('.editor-field'));
                } 
            }
        });
    }
    return validobj;
}

function get_field_name(e) {
    var previous_div = $(e).prev();
    if ($(previous_div).hasClass('editor-label')) return $(previous_div).html().trim();
    else return 'error';
}

function ValidObj(result, errors) {
    this.result = result;
    this.errors = errors;
}

function collapse_section(e) {
    $(e).find(".data").slideUp(400);
    $(e).find('.arrow').removeClass('down');
    $(e).find('.success-message, .error-message, .remove-message').remove();
}

function open_section(e) {
    $(e).find(".data").slideDown(400);
    if (!($(e).find('.arrow').hasClass('down'))) $(e).find('.arrow').toggleClass('down');
}

function is_visible(e) {
    return $(e).find(".data").is(":visible");
}

function validate_page() {
    var result = true;
    var validobj;
    $(".validate-section").each(function () {
        validobj = validate_section($(this));
        if (result) result = validobj.result;

        if (validobj.result) $(this).find(".valid").html(valid_icon + "<div class=\"valid-message\">Data Valid</div>"); //valid
        else $(this).find(".valid").html(invalid_icon + "<div class=\"invalid-message\">" + validobj.errors + "</div>"); //invalid
    });
    return result;
}

function show_first_section_not_valid() {
    var keep_going = true;
    $(".validate-section").each(function () {
        var validobj = validate_section($(this));
        if (!validobj.result && keep_going) {
            keep_going = false;
            if (!(is_visible($(this)))) open_section($(this));
        }
        else collapse_section($(this));
    });
}

function validateCheckBoxes(e){
    var result = false;
    if($(e).is(':hidden')) {
        if($(e).parents('.editor-field, .editor-label, .bilcheck')){
            $(e).parent().children('input').each(function (){
                if ($(this).is(':checkbox')) result = true;
                if (result) return true;
            });
        }
    }
    return result;
}


//Open All Sections

$(function(){

    $(".open-sections").click(function(){
        if($(".open-sections").hasClass('close')){
            $(".validate-section .data").slideDown(400);
            $(".validate-section .data").show();
            $('.arrow').addClass('down');
            $(".open-sections").removeClass('close');
            $(".open-sections").addClass('open');
        }
        else if($(".open-sections").hasClass('open')){
            $(".validate-section .data").slideUp(400);
            //$(".validate-section .data").hide();
            $('.arrow').removeClass('down');
            $(".open-sections").removeClass('open');
            $(".open-sections").addClass('close');
        }
    });

   


});

                                   