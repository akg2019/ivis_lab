﻿$(document).ready(function () {
    testSession();
    setInterval(testSession, 180000);
});

function testSession() {
    $.ajax({
        url: "/Login/KeepAlive/" + Date.parse(new Date()).toString(),
        cache: false,
        success: function (d) {
            if (!(d.result)) {
                location.replace("/Login/Logout/"); //something happened, kick user
            }
        },
        error: function () {
            location.replace("/Login/Logout/"); //something happened, kick user
        }
    });
    return false;
}


function initBody() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + dd + mm;
    $.ajax({
        url: '/Session/GetSession/' + today,
        success: function (data) {
            if (data.data = 'data')
                alert("yes");
        },
        error: function () {
            alert("no");
        }
    });
}
