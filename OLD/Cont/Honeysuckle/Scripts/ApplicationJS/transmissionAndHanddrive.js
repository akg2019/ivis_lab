﻿$(function () {
    $("#tranType").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Vehicle/GetTransmissionTypes?type=' + req.term, function (data) {
                var values = []
                $.each(data, function (i, v) {
                    values.push(v.value);
                })
                resp(values);
            });
        }
    })
});


$(function () {
    $("#handDriveType").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Vehicle/GetHandDriveTypes?type=' + req.term, function (data) {
                if (!data.fail) {
                    var values = []
                    $.each(data, function (i, v) {
                        values.push(v.value);
                    })
                    resp(values);
                }
            });
        }
    })
});
