﻿
$(function () {


    if ($('.type').val() == 'Insurer') {

        $(".compCode").show();
        /*$(".autoCanel").show();*/
        $("#compInsCode").prop('required', true);
        $("#ComShortening").prop('required', true);
        $("#ComShortening").trigger('keyup');
    }
    else {
        $("#compInsCode").val("");
        $("#compInsCode").removeAttr('required');
        $("#ComShortening").removeAttr('required');
        $("#ComShortening").trigger('keyup');
        /*$(".autoCanel").prop('checked', false);*/
        $(".compCode").removeClass('ok');
        $(".compCode").hide();
        /*$(".autoCanel").hide();*/
    }


    //ON Change
    $('.type').on('change', function () {
        if ($(this).val() == 'Insurer') {

            $(".compCode").show();
            /*$(".autoCanel").show();*/
            $("#compInsCode").prop('required', true);
            $("#ComShortening").prop('required', true);
            $("#ComShortening").trigger('keyup');
        }
        else {
            $("#compInsCode").val("");
            $("#compInsCode").removeAttr('required');
            $("#ComShortening").removeAttr('required');
            $("#ComShortening").trigger('keyup');
            /*$(".autoCanel").prop('checked', false);*/
            $(".compCode").removeClass('ok');
            $(".compCode").hide();
            /*$(".autoCanel").hide();*/
        }
    });
});            //end of MAIN Function