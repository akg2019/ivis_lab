﻿
document.getElementById("heading").style.width = "400px";
document.getElementById("sendto").style.width = "300px";


$("#sendto").prop('readonly', true);
$("#heading").val("Vehicle Cover Note approval required.");


//This part is needed for when there are more than one cover notes to be approved
var NoteList = $("#veh").val().split(',');
var message = "Approval request for the following cover notes(s): ";

for (var i = 0; i < NoteList.length; i++) {

    message = message + window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + "/VehicleCoverNote/Details/" + NoteList[i];

    if (i !== NoteList.length - 1) message = message + ",";

}

$("#content").val(message);
$("#heading").prop('readonly', true);

var vehicleId = $("#veh").val();

$("#veh").hide();
var admins = [];

if (NoteList != null && NoteList != "") {

    $.getJSON("/VehicleCoverNote/CompanyBrokers/" + NoteList[0], function (data) {
        if (data.result) {
            admins = data.admins;

            var selectbox = $("#sendto");
            var options = "";
            selectbox.empty();

            for (var i = 0; i < admins.length; i++) {
                options += "<option value='" + admins[i][0] + "'>" + admins[i][1] + "</option>";
            }
            selectbox.append(options);

            selectbox.chosen();

            // $("#sendto").val(admins);

        }
    });
}