﻿var trnFocus = false;
var trnSet = false;
var alreadyok = false;

$(function () {
    SetAllReadOnly();
});

function SetAllReadOnly() {
    $("#fname").prop('readonly', true);
    $("#mname").prop('readonly', true);
    $("#lname").prop('readonly', true);
    $("#roadno").prop('readonly', true);
    $("#aptNumber").prop('readonly', true);
    $("#roadname").prop('readonly', true);
    $("#roadtype").prop('readonly', true);
    $("#zipcode").prop('readonly', true);
    $("#city").prop('readonly', true);
    $("#country").prop('readonly', true);

    if ($(".parishes").length > 1)
        $(".address-details.parishes").prop('disabled', true);
    else
    $(".parishes").prop('disabled', true);

    $(".rmemail").hide();
    $("#addemail").hide();
    $(".emailprimary").attr("disabled", true);
};

function SetAllNotReadOnly() {
    $("#fname").prop('readonly', false);
    $("#mname").prop('readonly', false);
    $("#lname").prop('readonly', false);
    $("#RoadNumber").prop('readonly', false);
    $("#aptNumber").prop('readonly', false);
    $("#roadname").prop('readonly', false);
    $("#roadtype").prop('readonly', false);
    $("#zipcode").prop('readonly', false);
    $("#city").prop('readonly', false);
    $("#country").prop('readonly', false);
    $(".parishes").prop('disabled', false);
    $(".rmemail").show();
    $("#addemail").show();
    $(".emailprimary").removeAttr("disabled");
};

function RemoveEmails() {
    $(".newemail").remove();
};

function GetPersonsEmails(id){
    $.getJSON("/Email/GetPersonEmails/" + id, function (data) {
        var obj = jQuery.parseJSON(data);
        for (var i = 0; i < obj.length; i++) {
            $.ajax({
                url: '/Email/NewEmail/' + obj[i].emailID,
                cache: false,
                success: function (html) { $("#email-container").append(html); $(".emailprimary").attr("disabled", true); $(".rmemail").hide(); }
            });
            return false;
        }
    });
    
};

$(function () {
    if ($("#trn").val().trim().length > 0) trnSet = true;
});


$(function () {
    $("#trn").focus(function () {
        trnFocus = true;
    })
});

$(function () {
    $("#trn").blur(function () {
        if (trnFocus) {
            var test = $("#trn").val();
            if (test.trim().length > 0) {
                var result = false;
                $.getJSON("/Vehicle/GetUserByTRN/" + test.trim() + '?insured=' + false, function (data) {
                    if (!data.error) {
                        if (data.result) {
                            //something exists
                            if (data.person) {
                                //There is a person who's not a user yet
                                alreadyok = false;

                                $("#InformationDialog").dialog({
                                    modal: true,
                                    autoOpen: false,
                                    width: 310,
                                    title: "Person Exists",
                                    height: 350,
                                    buttons: {
                                        Ok: function () {

                                            $(this).html("");
                                            $(this).dialog("destroy");
                                            $("#InformationDialog").html("");
                                            $("#RoadNumber,#aptNumber, #roadname,#city,#roadtype,#parishes,#country,#fname,#mname,#lname").prop('readonly', true);
                                            $("#RoadNumber, #aptNumber, #roadname,#city,#roadtype,#parishes,#country,#fname,#mname,#lname").prop('disabled', false);
                                            $("#fname").val(data.fname);
                                            $("#mname").val(data.mname);
                                            $("#lname").val(data.lname);
                                            $("#RoadNumber").val(data.roadno);
                                            $("#aptNumber").val(data.aptNumber);
                                            $("#roadname").val(data.roadname);
                                            $("#roadtype").val(data.roadtype);
                                            $("#zipcode").val(data.zipcode);
                                            $("#city").val(data.city);
                                            $("#country").val(data.country);
                                            $("#personid").val(data.personid);
                                            $(".parishes").children().each(function () {
                                                if ($(this).val() == data.parishid) {
                                                    $(this).prop('selected', true);
                                                    return;
                                                }
                                            });
                                            trnSet = true;
                                            SetAllReadOnly();
                                            RemoveEmails();
                                            GetPersonsEmails(data.personid);

                                            $("#fname,#mname,#lname,#RoadNumber,#aptNumber,#roadtype,#roadname,#city,#country,#personid").trigger('keyup');
                                            $('.parishes').trigger('change');
                                        },

                                        Cancel: function () {

                                            $(this).html("");
                                            $(this).dialog("destroy");
                                            $("#InformationDialog").html("");
                                            $("#trn").val("");
                                            $("#trn").focus();

                                        }
                                    }
                                });

                                $("#InformationDialog").load('/People/FullPersonDetails/' + data.personid + "?insured=" + false, function (value) { }).dialog('open');

                            }
                            else {
                                if (data.user) {
                                    //There is a user
                                    alreadyok = false;
                                    $("<div>The TRN you have entered is already listed to a current user in the system.  Would you like to have the Person's Details populate the form now?</div>").dialog({
                                        modal: true,
                                        title: "Alert",
                                        buttons: {
                                            Ok: function () {
                                                $("#RoadNumber,#aptNumber, #roadname,#city,#roadtype,#parishes,#country,#fname,#mname,#lname").prop('readonly', true);
                                                $("#RoadNumber,#aptNumber, #roadname,#city,#roadtype,#parishes,#country,#fname,#mname,#lname").prop('disabled', false);
                                                $("#fname").val(data.fname);
                                                $("#mname").val(data.mname);
                                                $("#lname").val(data.lname);
                                                $("#RoadNumber").val(data.roadno);
                                                $("#aptNumber").val(data.aptNumber);
                                                $("#roadname").val(data.roadname);
                                                $("#roadtype").val(data.roadtype);
                                                $("#zipcode").val(data.zipcode);
                                                $("#city").val(data.city);
                                                $("#country").val(data.country);
                                                $("#personid").val(data.personid);
                                                $(".parishes").children().each(function () {
                                                    if ($(this).val() == data.parishid) {
                                                        $(this).prop('selected', true);
                                                        return;
                                                    }
                                                });
                                                RemoveEmails();
                                                GetPersonsEmails(data.personid);
                                                trnSet = true;
                                                SetAllReadOnly();
                                                $(this).dialog("destroy");


                                            },
                                            Cancel: function () {

                                                //$("#trn").value = "";
                                                $(this).dialog("destroy");
                                                $("#trn").val("");
                                                $("#trn").focus();

                                            }
                                        }
                                    });
                                }
                            }
                        }
                        else {
                            //TRN FREE
                            if (test.trim().length != 0) trnSet = true;
                            SetAllNotReadOnly();
                            $("#RoadNumber, #aptNumber, #roadname,#city,#roadtype,#parishes,#country,#fname,#mname,#lname").prop('disabled', false);
                            if (!alreadyok) {
                                $("#fname").val("");
                                $("#mname").val("");
                                $("#lname").val("");
                                $("#RoadNumber").val("");
                                $("#aptNumber").val("");
                                $("#roadname").val("");
                                $("#roadtype").val("");
                                $("#zipcode").val("");
                                $("#city").val("");
                                $("#country").val("");
                                $("#personid").val(0);
                                $("#fname").focus();
                            }

                            alreadyok = true;
                            RemoveEmails();

                        }
                    }
                    else {
                        //ERROR
                        $("<div>" + data.message + "</div>").dialog({
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $("#trn").val("");
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }

                });
                trnFocus = false;
            }
            else { $("#trn").focus(); }
        }
    })
});


$(function () {
    $("#RoadNumber,#roadname,#city, #aptNumber, #roadtype,#country,#fname,#mname,#lname").prop('disabled', true);
    if ($(".parishes").length > 1)
        $(".address-details#parishes").prop('disabled', true);
    else
        $("#parishes").prop('disabled', true);
    //check
});
