﻿$(function () {
    $(document).on("change", ".company_dropdown", function () {

        var intermediaryCompId = $(".company_dropdown").val();
        var curhref = window.location.href.toString();

        //Testing which page it is currently on- so as to re-load to appropriate page
        if (curhref.search("policy") != -1 || curhref.search("Policy") != -1) window.location.replace("/Policy/Index/?id=" + intermediaryCompId);
        else {
            if (curhref.search("covernote") != -1 || curhref.search("CoverNote") != -1) window.location.replace("/VehicleCoverNote/Index/?id=" + intermediaryCompId);
            else {
                if (curhref.search("certificate") != -1 || curhref.search("Certificate") != -1) window.location.replace("/VehicleCertificate/Index/?id=" + intermediaryCompId);
                else window.location.replace("/Vehicle/Index/?id=" + intermediaryCompId);
            }
        }

    });
});

$(function () {
    var i = getUrlVars()["id"];
    $(".company_dropdown option").each(function () {
        if ($(this).val() == i) {
            $(this).prop('selected', true);
        }
        if ($(this).val() == "0") {
            $(this).text("All");
        }
    });
});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}