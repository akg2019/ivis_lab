﻿
var regNoFocus = false;

$(function () {

    $("#prevRegNo").val("");
    $("#prevChassis").val("");

    $("#vehicleRegNum").focus(function () {
        regNoFocus = true;
    });

    $("#vehicleRegNum").blur(function () {
        if (regNoFocus && $("#vehicleRegNum").val() != '') {
            var RegNoString = $("#vehicleRegNum").val();
            if (RegNoString.trim().length > 0) {
                //var result = false;
                $.getJSON("/Vehicle/RegNoExists/?regNo=" + RegNoString.trim(), function (data) {

                    if (!data.error && data.result) {

                        if (data.regNoExist) {

                            var prevRegNo = $("#prevRegNo").val();
                            var prevChassis = $("#prevChassis").val();
                            var currentChassis = $("#chassis").val();

                            if (prevChassis == "" ||
                            (prevRegNo.trim() != RegNoString.trim() && prevChassis.trim() != currentChassis.trim()) ||
                            (prevRegNo.trim() != RegNoString.trim() && prevChassis.trim() == currentChassis.trim()) ||
                            (prevRegNo.trim() == RegNoString.trim() && prevChassis.trim() != currentChassis.trim()))
                                $("<div> The Vehicle Registration Number is already in use.</div>").dialog({
                                    modal: true,
                                    title: "Alert",
                                    buttons: {
                                        Ok: function () {
                                            $(this).dialog("close");
                                            $("#vehicleRegNum").val("");
                                            $("#vehicleRegNum").focus();
                                            $("#vehicleRegNum").trigger('keyup');
                                        }
                                    }
                                });
                        }
                    } //end of if for data.regNoExist
                    else {// if error
                        $("<div>" + data.message + "</div>").dialog({
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $("#vehicleRegNum").val("");
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }



                }); // end of JSON function
            } // end of if RegString > 0
        } // end of if RegFocus
        regNoFocus = false;
    }); // end of blur

});    // end of function