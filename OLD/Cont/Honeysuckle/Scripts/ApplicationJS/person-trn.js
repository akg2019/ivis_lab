﻿
    var spinner;
    var alreadyok = false;
    var tab = false;
    var create = false;
    var trnFocus = false;
    var trnSet = false;
    var trnIsMessage = "<div>The TRN you have entered already exists in the system. Would you like to have the Person's Details populate the form now?</div>";
    var UserIsMessage = "<div>The TRN you have entered already exists in the system, belonging to a user, in another company. The user has to first be deleted from that company.</div>";
    var IncorrectTrn = "<div>The TRN enetered is not valid.</div>"


   
    //TESTS IF USER BEING DISPLAYED IS TRUE
    //0 (WHICH IS ADMIN) IS INVALID, BECAUSE 
    //WE SHOULDN'T TOUCH HIM
    $(function () {
        disableAll();
        if ($("#trn").val().trim().length > 0 && $("#trn").val() != "0") {
            trnSet = true;
            enableAll();
        }
    });

    $(function () {
        if(!trnSet) $("#trn").val("");
    });

    $(function () {
        $("#trn").focus(function () {
            trnFocus = true;
        })
    });

    $(function () {
        CheckTrn;
        tab = false;
        $("#trn").on("keydown", function (event) {
            if (event.which == 9) tab = true;
        });

        create = ($("#page").val() == "create");
        alreadyok = false;
        alreadyok = ($("#postback").val().toLowerCase() == "true");

        $("#trn").blur(function () {
            StartSpinner();
            if (trnFocus) {
                var test = $("#trn").val();
                var result = false;
                if (test.trim().length > 0) {
                    $.getJSON("/User/GetUserByTRN/" + test.trim() + "?test=" + true, function (data) {
                        if (!data.error) {
                            if (data.result) {
                                if (data.invalid) {
                                    $('<div>This TRN is invalid!</div>').dialog({
                                        modal: true,
                                        title: 'Alert!',
                                        buttons: {
                                            OK: function () {
                                                $(this).dialog('close');
                                            }
                                        }
                                    });
                                }
                                else {
                                    //something exists
                                    if (data.person) {
                                        StopSpinner();
                                        alreadyok = false;
                                        //there is a person who's not a user yet
                                        $(trnIsMessage).dialog({
                                            modal: true,
                                            title: "Alert",
                                            buttons: {
                                                Ok: function () {

                                                    enableAll();
                                                    $("#companies").prop("disabled", false);

                                                    $("#fname").val(data.fname);
                                                    $("#mname").val(data.mname);
                                                    $("#lname").val(data.lname);
                                                    $("#RoadNumber").val(data.roadno);
                                                    $("#aptNumber").val(data.aptNumber);
                                                    $("#roadname").val(data.roadname);
                                                    $("#roadtype").val(data.roadtype);
                                                    $("#zipcode").val(data.zipcode);
                                                    $("#city").val(data.city);
                                                    $("#country").val(data.country);
                                                    $("#personid").val(data.personid);
                                                    $(".parishes").children().each(function () {
                                                        if ($(this).val() == data.parishid) {
                                                            $(this).prop('selected', true);
                                                            return;
                                                        }
                                                    });
                                                    ReadOnlyPerson();
                                                    trnSet = true;
                                                    $(this).dialog("close");
                                                    if (tab) $("#username").focus();
                                                },
                                                Cancel: function () {
                                                    $("#trn").val("");
                                                    $("#companies").prop("disabled", false);
                                                    $(this).dialog("close");
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        if (data.user) {
                                            if (data.exist) {
                                                //there is a user
                                                //in my company 
                                                //no to existing user
                                                window.location.href = "/User/Details/" + data.uid;
                                            }
                                            else {
                                                StopSpinner();
                                                //there is a user
                                                //not in my company 
                                                //create new user
                                                alreadyok = false;
                                                $(UserIsMessage).dialog({
                                                    modal: true,
                                                    title: "Alert",
                                                    buttons: {
                                                        Ok: function () { // A user(TRN) can't exists in two companies at once..One has to be "deleted" or "disabled"

                                                            $("#trn").val("");
                                                            $(this).dialog("close");

                                                        },
                                                        Cancel: function () {
                                                            $("#trn").val("");
                                                            $(this).dialog("close");
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                        else {
                                            if (data.emp) {
                                                StopSpinner();
                                                //there is a employee
                                                alreadyok = false;
                                                $(trnIsMessage).dialog({
                                                    modal: true,
                                                    buttons: {
                                                        Ok: function () {
                                                            $("#fname").val(data.fname);
                                                            $("#mname").val(data.mname);
                                                            $("#lname").val(data.lname);
                                                            $("#RoadNumber").val(data.roadno);
                                                            $("#aptNumber").val(data.aptNumber);
                                                            $("#roadname").val(data.roadname);
                                                            $("#roadtype").val(data.roadtype);
                                                            $("#zipcode").val(data.zipcode);
                                                            $("#city").val(data.city);
                                                            $("#country").val(data.country);
                                                            $("#personid").val(data.personid);
                                                            $("#companies").val(data.companyID);
                                                            $("#companies").prop("disabled", true);

                                                            ReadOnlyPerson();
                                                            trnSet = true;
                                                            $(this).dialog("close");
                                                            if (tab) $("#username").focus();
                                                        },
                                                        Cancel: function () {
                                                            $("#trn").val("");
                                                            $("#companies").prop("disabled", false);
                                                            $(this).dialog("close");
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                //TRN FREE
                                if (test.trim().length != 0) {
                                    StopSpinner();
                                    trnSet = true;
                                    enableAll();
                                    NotReadOnly();
                                    if (!alreadyok) $("#username,#aptNumber, #fname,#mname,#lname,#RoadNumber,#roadname,#roadtype,.parishes,#city,#companies").val("");
                                    alreadyok = true;
                                    $("#companies").prop("disabled", false);
                                    if (tab) $("#username").focus();

                                }
                            }
                        }
                        else {
                            //ERROR
                            $("<div>" + data.message + "</div>").dialog({
                                modal: true,
                                buttons: {
                                    Ok: function () {
                                        StopSpinner();
                                        $("#trn").val("");
                                        enableAll();
                                        $("#companies").prop("disabled", false);
                                        $(this).dialog("close");

                                    }
                                }
                            });
                        }
                        StopSpinner();
                    });
                }
                else {
                    $("#trn").val("");
                    if (trnSet) {
                        trnSet = false;
                    }
                    StopSpinner();
                    disableAll();
                }
                trnFocus = false; //reset variable so that things dont go wonky

            }

        });


    });
    
    function disableAll() {
        $("#username, #aptNumber,#fname,#mname,#lname,#RoadNumber,#roadname,#roadtype,.parishes,#city,#country,#companies,#usr_crt_btn").prop('disabled', true);
        $(".dont-disable").prop('disabled', false);
    }

    function ReadOnlyPerson() {
        $("#aptNumber,#fname,#mname,#lname,#RoadNumber,#roadname,#roadtype,.parishes,#city,#country,#companies").prop('readonly', true);
        $("#addNewEmail").hide();
        $(".dont-disable").prop('disabled', false);
        if (!($("#mname").hasClass("read-only"))) $("#aptNumber,#fname,#mname,#lname,#RoadNumber,#roadname,#roadtype,.parishes,#city,#country,#companies").toggleClass("read-only");
    }

    function NotReadOnly() {
        $("#aptNumber,#fname,#mname,#lname,#RoadNumber,#roadname,#roadtype,.parishes,#city,#country,#companies").prop('readonly', false);
        $("#addNewEmail").show();
        if ($("#mname").hasClass("read-only")) $("#aptNumber,#fname,#mname,#lname,#RoadNumber,#roadname,#roadtype,.parishes,#city,#country,#companies").toggleClass("read-only");
    }

    function enableAll() {
        $("#username,#fname,#mname,#lname,#RoadNumber, #aptNumber, #roadname,#roadtype,.parishes,#city,#country,#companies,#usr_crt_btn").prop('disabled', false);
        $("#addNewEmail").show();
    }



    //Eliminates overflow of Trn

    function CheckTrn() {
        var Tr = document.getElementById("trn");
        if (Tr.value = 100000000) {
            alert("Invalid Trn#")
            Tr.value = "";
        }
    }


//    var el = document.getElementById("trn");
//    if (el.addEventListener) {
//        el.addEventListener("onmouseout", CheckTrn, false);
//    } else {
//        el.attachEvent('onclick', CheckTrn);
//        el.att
//    }  

