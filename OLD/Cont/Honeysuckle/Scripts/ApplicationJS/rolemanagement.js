﻿$(function () {
    $("#assign-done").on('click', function () {
        window.location.replace('/RoleManagement/');
    });

});


function clean_up_server_responses() {
    $(".server-responses").find(".success-message:hidden,.error-message:hidden").remove();
    $(".server-responses").children().each(function () {
        var div = $(this);
        setTimeout(function () {
            role_management_response_fade_out(div);
        }, 5000);
    });
}

function role_management_response_fade_out(e) {
    $(e).fadeOut("slow");
}

function role_managment_success_save(message, type) {
    var name = "";
    switch (type) {
        case 'add':
            message = "The permission \"" + message + "\" was added";
            name = "success-message";
            break;
        case 'rm':
            message = "The permission \"" + message + "\" was removed";
            name = "remove-message";
            break;
        case 'addgroup':
            message = "The group \"" + message + "\" was added";
            name = "success-message";
            break;
        case 'rmgroup':
            message = "The group \"" + message + "\" was removed";
            name = "remove-message";
            break;
        case 'adduser':
            if ($("#page").val() == "adduser") message = "The role \"" + message + "\" was added";
            else message = "The user \"" + message + "\" was added";
            name = "success-message";
            break;
        case 'rmuser':
            if ($("#page").val() == "adduser") message = "The role \"" + message + "\" was removed";
            else message = "The user \"" + message + "\" was removed";
            name = "remove-message";
            break;
        case 'IAJ':
            message = "You cannot remove an IAJ created user";
            name = "error-message";
        default:
            break;        
    }
    var success = "<div class=\"" + name + "\">" + message + "</div>";
    $(".permission-header").find('.server-responses').append(success);
    clean_up_server_responses();
}

function role_managment_error_save(message) {
    var error = "<div class=\"error-message\">The permissions \"" + message + "\" was not saved. " + note + "</div>";
    $(".permission-header").find('.server-responses').append(error);
    clean_up_server_responses();
}


function addPermission() {
    if (is_safari) var i = $("#allpermissions").children("option:selected").length;
    var ajax = $("#page").val() == "edit"
    var count = 0;
    var ids = []
    if (ajax) {
        count = $("#allpermissions").children("option:selected").length;
    }
    $("#allpermissions").children("option:selected").each(function (index) {
        if (ajax) add_permission_ajax($(".hidid").val(), $(this).val(), $(this).html().trim());
        $("#mypermissions").prepend($(this));
        $(this).remove;
        
        if (is_safari) if (index == i - 1) setup();
    });
}

function add_permission_ajax(id, pid, permission) {
    $.ajax({
        url: '/RoleManagement/AddPermissionToGroup/' + id + '?pid=' + pid,
        cache: false,
        success: function (json) {
            if (json.result) role_managment_success_save(permission,'add');
            else {
                role_managment_error_save(permission);
                put_permission_back(pid, 'add');
            }
        },
        error: function () {
            role_managment_error_save(permission);
            put_permission_back(pid, 'add');
        }
    });

}

function put_permission_back(id, type) {
    switch (type) {
        case 'add':
            $("#mypermissions").children("option").each(function () {
                if ($(this).val() == id) {
                    $("#allpermissions").prepend($(this));
                    $(this).remove();
                    return false;
                }
            });
            break;
        case 'rm':
            $("#allpermissions").children("option").each(function () {
                if ($(this).val() == id) {
                    $("#mypermissions").prepend($(this));
                    $(this).remove();
                    return false;
                }
            });
            break;
        default:
            break;
    }
}

function rmPermission() {
    if (is_safari) var i = $("#mypermissions").children("option:selected").length;
    var ajax = $("#page").val() == "edit"
    $("#mypermissions").children("option:selected").each(function (index) {
        if (ajax) rm_permission_ajax($(".hidid").val(), $(this).val(), $(this).html().trim());
        $("#allpermissions").prepend($(this));
        $(this).remove;
        
        if (is_safari) if (index == i - 1) setup();
    });
}

function rm_permission_ajax(id, pid, permission) {
    $.ajax({
        url: '/RoleManagement/RmPermissionToGroup/' + id + '?pid=' + pid,
        cache: false,
        success: function (json) {
            if (json.result) role_managment_success_save(permission, 'rm');
            else {
                role_managment_error_save(permission);
                put_permission_back(pid, 'rm');
            }
        },
        error: function () {
            role_managment_error_save(permission);
            put_permission_back(pid, 'rm');
        }
    });
}

function addGroup() {
    if (is_safari) var i = $("#allgroups").children("option").length;
    var ajax = $("#page").val() == "edit"
    $("#allgroups").children("option:selected").each(function (index) {
        if (ajax) add_group_ajax($(".hidid").val(), $(this).val(), $(this).html().trim());
        $("#mygroups").append($(this));
        $(this).remove;

        if (is_safari) if (index == i - 1) setup();
    });
}

function add_group_ajax(id, gid, group) {
    $.ajax({
        url: '/RoleManagement/AddChildToGroup/' + id + '?gid=' + gid,
        cache: false,
        success: function (json) {
            if (json.result) role_managment_success_save(group, 'addgroup');
            else {
                role_managment_error_save(permission);
                //put_permission_back(pid, 'add');
            }
        },
        error: function () {
            role_managment_error_save(permission);
            //put_permission_back(pid, 'add');
        }
    });
}

function rm_group_ajax(id, gid, group) {
    $.ajax({
        url: '/RoleManagement/RmChildToGroup/' + id + '?gid=' + gid,
        cache: false,
        success: function (json) {
            if (json.result) role_managment_success_save(group, 'rmgroup');
            else {
                role_managment_error_save(group);
                //put_permission_back(pid, 'add');
            }
        },
        error: function () {
            role_managment_error_save(permission);
           // put_permission_back(pid, 'add');
        }
    });
}

function rmGroup() {
    if (is_safari) var i = $("#mygroups").children("option").length;
    var ajax = $("#page").val() == "edit"
    $("#mygroups").children("option:selected").each(function (index) {
        if (ajax) rm_group_ajax($(".hidid").val(), $(this).val(), $(this).html().trim());
        $("#allgroups").append($(this));
        $(this).remove;
        if (is_safari) if (index == i - 1) setup();
    });
}

function addUser() {
    $("#notgroupusers").children("option:selected").each(function () {
        add_user_ajax($(".hidid").val(), $(this).val(), $(this).html().trim());
        $("#groupusers").append($(this));
        //$(this).remove();
    });
}

function add_user_ajax(id, uid, user) {
    $.ajax({
        url: '/RoleManagement/AddUserToGroup/' + id + '?uid=' + uid,
        cache: false,
        success: function (json) {
            if (json.result) role_managment_success_save(user, 'adduser');
            else {
                role_managment_error_save(user);
                //put_permission_back(pid, 'add');
            }
        },
        error: function () {
            role_managment_error_save(user);
            // put_permission_back(pid, 'add');
        }
    });
}

function rmUser() {
    $("#groupusers").children("option:selected").each(function () {
        rm_user_ajax($(".hidid").val(), $(this).val(), $(this).html().trim());
        $("#notgroupusers").append($(this)); 
        //$(this).remove();
    });
}

function rm_user_ajax(id, uid, user) {
    $.ajax({
        url: '/RoleManagement/RmUserToGroup/' + id + '?uid=' + uid,
        cache: false,
        success: function (json) {
            if (json.result) role_managment_success_save(user, 'rmuser');
            else {
                if (json.note)
                    role_managment_success_save(user, "IAJ");
                else
                     role_managment_error_save(user);

                //put_permission_back(pid, 'add');
            }
        },
        error: function () {
            role_managment_error_save(user);
            // put_permission_back(pid, 'add');
        }
    });
}

function addRole() {
    $("#notgroups").children("option:selected").each(function () {
        add_user_ajax($(this).val(), $(".userid").val(), $(this).html().trim());
        $("#groups").append($(this));
    });    
}

function rmRole() {
    $("#groups").children("option:selected").each(function () {
        rm_user_ajax($(this).val(), $(".userid").val(), $(this).html().trim());
        $("#notgroups").append($(this));
    });    
}

var allowSuper = true;
var is_safari = (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0);
var options;
var allpermissions;
var mypermissions;
var allgroups;
var mygroups;

function ToggleSuperGroups() {
    if (allowSuper) {
        $("div.permissions").toggleClass("hide");
        $("div.groups").toggleClass("hide");
    }
    else {
        $("#supergroupcheckbox").prop("checked", false);
        $("<div>You cannot have a System or Stock group that is a Super Group. Please unselect the Stock and System Group options in order to create a Super Group</div>").dialog({
            title: 'Group Settings Error',
            modal:true,
            buttons: {
                Ok: function () {
                    $(this).dialog('close');
                }
            }
        })
    }
};

//this is to be refactored... not quite correct yet
function ToggleStock() {
    $("#stockcheckbox").toggleClass("readonly");
    if ($("#stockcheckbox").is(":checked")) $("#stockcheckbox").prop("readonly", true);
    else $("#stockcheckbox").prop("readonly", false);
}

function ToggleStockGroups() {
    if ($("#supergroupcheckbox").is(":checked")) {
        $("#supergroupcheckbox").prop("checked", false);
        ToggleSuperGroups();
    }

    if ($("#stockcheckbox").is(":checked") || $("#systemcheckbox").is(":checked")) allowSuper = false;
    else allowSuper = true;
}

function ToggleSystemGroups() {

    var sys = $("#systemcheckbox").is(":checked");
    var id = $(".hidid").val();

    UpdateMyPermissions(id, sys);
    UpdateNotMyPermissions(id, sys);
    UpdatePermissionsCompleted();
}


function UpdateMyPermissions(id, sys) {
    if (id == undefined) id = '';
    myper = $("#mypermissions");
    $.ajax({
        url: '/UserPermission/MyListBoxUserPermissions/' + id + '?system=' + sys,
        cache: false,
        success: function (data) {
            $("#mypermissions").replaceWith(data);
            myperdone = true;
        },
        error: function () {
            error = true;
            $("#systemcheckbox").prop('checked', !sys);
            $("<div>There was an error. Please try again. If the problem persists please contact your administrator</div>").dialog({
                title: 'Error',
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog('close');
                    }
                }
            });
        }
    });
}

function UpdateNotMyPermissions(id, sys) {
    if (id == undefined) id = '';
    allper = $("#allpermissions");
    $.ajax({
        url: '/UserPermission/ListBoxUserPermissions/' + id + '?system=' + sys,
        cache: false,
        success: function (data) {
            $("#allpermissions").replaceWith(data);
            allperdone = true;
        },
        error: function () {
            error = true;
            $("#systemcheckbox").prop('checked', !sys);
            $("<div>There was an error. Please try again. If the problem persists please contact your administrator</div>").dialog({
                title: 'Error',
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog('close');
                    }
                }
            });
        }
    });
}

var allper = null;
var myper = null;
var allperdone = false;
var myperdone = false;
var error = false;

function UpdatePermissionsCompleted() {
    var timer = setTimeout(function () {
        if (error) {
            $("#allpermissions").replaceWith(allper);
            $("#mypermissions").replaceWith(myper);
            CleanFlags();
            clearInterval(timer);
            //end
        }
        else {
            if (allperdone && myperdone) {
                CleanFlags();
                clearInterval(timer);
                //end
            }
        }
    }, 1000);
}

function CleanFlags() {
    allper = null;
    myper = null;
    allperdone = false;
    myperdone = false;
    error = false;
}

$(function () {
    setup();
    if ($("#supergroupcheckbox").is(":checked")) ToggleSuperGroups();
});




//FOR SAFARI CSS
$(function () {

    if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {

        $('.quick-search-btn').addClass('safari');
    }

});