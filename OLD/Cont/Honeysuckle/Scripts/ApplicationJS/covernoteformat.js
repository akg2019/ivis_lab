﻿$(function () {

    var options = {
        width: 800,
        height: 500,
        controls: "bold italic underline | font size " +
                  "style | bullets numbering | outdent " +
                  "indent | alignleft center alignright justify | undo redo | " +
                  "rule | cut copy paste pastetext | source | " + 
                  "vehyr vehmake vehmod vehmodtype vehext vehregno mainins maindri vehdrivers"
    };

    //<[Risk Items]Veh Year> 
    $.cleditor.buttons.vehyr = {
        name: "vehyr",
        image: "code-icon.png",
        command: "inserthtml",
        title: "Add Vehicle Year",
        buttonClick: function (e, data) { data.editor.execCommand(data.command, "<[Risk Items]Veh Year>", null, data.button); }
    };
    
    //<[Risk Items]Veh Make> 
    $.cleditor.buttons.vehmake = {
        name: "vehmake",
        image: "code-icon.png",
        command: "inserthtml",
        title: "Add Vehicle Make",
        buttonClick: function (e, data) { data.editor.execCommand(data.command, "<[Risk Items]Veh Make>", null, data.button); }
    };
    
    //<[Risk Items]Veh Model> 
    $.cleditor.buttons.vehmod = {
        name: "vehmod",
        image: "code-icon.png",
        command: "inserthtml",
        title: "Add Vehicle Model",
        buttonClick: function (e, data) { data.editor.execCommand(data.command, "<[Risk Items]Veh Model>", null, data.button); }
    };
    //<[Risk Items]Veh Model Type>
    $.cleditor.buttons.vehmodtype = {
        name: "vehmodtype",
        image: "code-icon.png",
        command: "inserthtml",
        title: "Add Vehicle Model Type",
        buttonClick: function (e, data) { data.editor.execCommand(data.command, "<[Risk Items]Veh Model Type>", null, data.button); }
    };
    //<[Risk Items]Veh Extension>
    $.cleditor.buttons.vehext = {
        name: "vehext",
        image: "code-icon.png",
        command: "inserthtml",
        title: "Add Vehicle Extension",
        buttonClick: function (e, data) { data.editor.execCommand(data.command, "<[Risk Items]Veh Extension>", null, data.button); }
    };
    //<[Risk Items]Veh Reg No>
    $.cleditor.buttons.vehregno = {
        name: "vehregno",
        image: "code-icon.png",
        command: "inserthtml",
        title: "Add Vehicle Registration Number",
        buttonClick: function (e, data) { data.editor.execCommand(data.command, "<[Risk Items]Veh Reg No>", null, data.button); }
    };
    //<[Policy Info]Main Insured> ( indicate all the insureds)
    $.cleditor.buttons.mainins = {
        name: "mainins",
        image: "code-icon.png",
        command: "inserthtml",
        title: "Add Main Insured",
        buttonClick: function (e, data) { data.editor.execCommand(data.command, "<[Policy Info]Main Insured>", null, data.button); }
    };
    //<[Risk Items]Veh Main Driver Name> (indicate who is the main driver)
    $.cleditor.buttons.maindri = {
        name: "maindri",
        image: "code-icon.png",
        command: "inserthtml",
        title: "Add Main Driver",
        buttonClick: function (e, data) { data.editor.execCommand(data.command, "<[Risk Items]Veh Main Driver Name", null, data.button); }
    };
    //<[Risk Items]Veh Driver Names> (List all the drivers)
    $.cleditor.buttons.vehdrivers = {
        name: "vehdrivers",
        image: "code-icon.png",
        command: "inserthtml",
        title: "Add Drivers",
        buttonClick: function (e, data) { data.editor.execCommand(data.command, "<[Risk Items]Veh Driver Names>", null, data.button); }
    };

    $("#format").cleditor(options)[0];


});