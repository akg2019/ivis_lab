﻿$(function () {
    $("#RoadNumber,#roadname,#roadtype,#aptNumber,#city,#country").toggleClass("read-only").prop('readonly', true);
    $("#parishes").prop('disabled', true).toggleClass('read-only');

    $("#edit-person").on('click', function () {
        window.location.replace("/People/Edit/" + $(".hiddenid").val());
    });
});