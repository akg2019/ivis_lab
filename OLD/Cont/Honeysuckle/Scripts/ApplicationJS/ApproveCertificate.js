﻿
function ApproveCert () {
    var certificateId = $("#certId").val();
    var parms = { certificateId: certificateId };
    $.ajax({
        type: "POST",
        traditional: true,
        url: "/VehicleCertificate/ApproveCert/",
        async: false,
        data: parms,
        dataType: "json",
        success: function (data) {

            $("<div> Certificate Approved! </div>").dialog({
                modal:true,
                title: "Alert!",
                buttons:
                    {
                        OK: function () {
                            $(this).dialog("close")
                            window.location.replace("/VehicleCertificate/Details/" + certificateId);

                        }
                    }

            }); //End of alert dialog

        }
    });
} //end of MAIN Function


function CancelCert () {

    var certificateId = $("#certId").val();
    $.ajax({
        type: "POST",
        traditional: true,
        url: "/VehicleCertificate/Cancel/" + certificateId,
        async: false,
        dataType: "json",
        success: function (data) {
            $("<div> Certificate Cancelled! </div>").dialog({
                modal: true,
                title: "Alert!",
                buttons: {
                    OK: function () {
                        $(this).dialog("close")
                        window.location.replace("/VehicleCertificate/Details/" + certificateId);
                    }
                }
            }); //End of alert dialog
        }
    });
}   //end of MAIN Function

//Enabling the printing for a particular vehicle certificate
function EnablePrint() {

    var CertID = $("#certId").val();
    var vehicleID = $("#vehId").val();
    var CompanyId = $("#companyid").val();

    $.ajax({
        type: "POST",
        traditional: true,
        url: "/VehicleCertificate/EnablePrinting/" + CertID,
        async: false,
        dataType: "json",
        success: function (data) {
            if (data.result) {
                $("#EnablePrint").remove();
                $("<div>The printing for this certificate was enabled. </div>").dialog({
                    modal: true,
                    title: "Alert!",
                    buttons: {
                        OK: function () {
                            $(this).dialog("close")
                            $(this).html("");

                        }
                    }
                });
            }
        }
    });
};