﻿var is_safari = false;
var groups;
var notgroups;

function setup() {
    notgroups = $("#notgroupusers,#notgroups").children("option");
    groups = $("#groupusers,#groups").children("option");
}   

function buildHtmlFromList(list) {
    var string = "";
    $(list).each(function () {
        string += "<option value=\"" + $(this).val() + "\">" + $(this).text() + "</option>";
    });
    return string;
}

$(function () {

    is_safari = (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0);

    setup();

    $(".searching").on('keyup', function () {
        var filter = $(this).val();
        if ($(this).hasClass('in') || $(this).hasClass('is')) {
            if (is_safari) {

                $("#groupusers,#groups").children("option").remove();
                if (filter.trim() == "") {
                    $("#groupusers,#groups").append(buildHtmlFromList(groups));
                }
                else {
                    var list = [];
                    var i = 0;
                    $(groups).each(function () {
                        if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                            list[i] = $(this);
                            i++;
                        }
                    });
                    $("#groupusers,#groups").append(buildHtmlFromList(list));
                }
            }
            else {
                $("#groupusers,#groups").children("option").each(function () {
                    if (filter.trim() == "") {
                        $(this).show();
                    }
                    else {
                        if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                            $(this).show();
                        }
                        else {
                            $(this).hide();
                        }
                    }
                });
            }
        }
        if ($(this).hasClass('out') || $(this).hasClass('not')) {
            if (is_safari) {

                $("#notgroupusers,#notgroups").children("option").remove();
                if (filter.trim() == "") {
                    $("#notgroupusers,#notgroups").append(buildHtmlFromList(notgroups));
                }
                else {
                    var list = [];
                    var i = 0;
                    $(notgroups).each(function () {
                        if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                            list[i] = $(this);
                            i++;
                        }
                    });
                    $("#notgroupusers,#notgroups").append(buildHtmlFromList(list));
                }
            }
            else {
                $("#notgroupusers,#notgroups").children("option").each(function () {
                    if (filter.trim() == "") {
                        $(this).show();
                    }
                    else {
                        if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                            $(this).show();
                        }
                        else {
                            $(this).hide();
                        }
                    }
                });
            }
        }
    });
});
