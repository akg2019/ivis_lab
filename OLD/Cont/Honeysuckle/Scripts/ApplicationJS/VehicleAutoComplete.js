﻿$(function () {

    $("#make").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Vehicle/GetMakes?make=' + req.term, function (data) {
                var values = []
                $.each(data, function (i, v) {
                    values.push(v.value);
                })
                resp(values);
            });
        }
    });

    $("#vehiclemodel").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Vehicle/GetModels?model=' + req.term, function (data) {
                var values = []
                $.each(data, function (i, v) {
                    values.push(v.value);
                })
                resp(values);
            });
        }
    });


    $("#modeltype").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Vehicle/GetModelType?modelType=' + req.term, function (data) {
                var values = []
                $.each(data, function (i, v) {
                    values.push(v.value);
                })
                resp(values);
            });
        }
    });

    $("#bodytype").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Vehicle/GetBodyType?bodyType=' + req.term, function (data) {
                var values = []
                $.each(data, function (i, v) {
                    values.push(v.value);
                })
                resp(values);
            });
        }
    });

    $("#tranType").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Vehicle/GetTransmissionTypes?type=' + req.term, function (data) {
                var values = []
                $.each(data, function (i, v) {
                    values.push(v.value);
                })
                resp(values);
            });
        }
    });

    $("#handDriveType").autocomplete({
        source: function (req, resp) {
            $.getJSON('/Vehicle/GetHandDriveTypes?type=' + req.term, function (data) {
                if (!data.fail) {
                    var values = []
                    $.each(data, function (i, v) {
                        values.push(v.value);
                    })
                    resp(values);
                }
            });
        }
    });


});
