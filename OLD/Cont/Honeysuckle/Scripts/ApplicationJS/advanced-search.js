﻿var processing = "<div class=\"dataprocessing\"><div class=\"rect1\"></div><div class=\"rect2\"></div><div class=\"rect3\"></div><div class=\"rect4\"></div><div class=\"rect5\"></div></div>";

$(function () {

    //hides all results on page load
    $(".results").hide();
    $('.scroll-table').perfectScrollbar({
        wheelSpeed: 0.3,
        wheelPropagation: true,
        minScrollbarLength: 10,
        suppressScrollX: true
    });

    //when you click something it shows it & hides others
    $(".search-headers").on('click', function () {
        if ($(this).parent().children(".results").is(':visible')) {
            $(this).parent().children(".results").css("margin-bottom", "20px");
            $(this).parent().children(".results").slideUp(400);
            $(this).parents(".search-result").find(".arrow").toggleClass('down');
        }
        else {
            $(".results").each(function () {
                $(this).parent().children(".results").css("margin-bottom", "20px");
                if ($(this).is(':visible')) $(this).slideUp(400);
                if ($(this).parents(".search-result").find(".arrow").hasClass("down")) $(this).parents(".search-result").find(".arrow").toggleClass('down');
            });
            $(this).parent().children(".results").css("margin-bottom", "20px");
            $(this).parent().children(".results").slideDown(400);
            $(this).parents(".search-result").find(".arrow").toggleClass('down');

            //Add the perfect scroll bar to the dropped down search element
            $(this).parent().find('.scroll-table').perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });

        }
    });

    if ($("#advanced-search-bar").val().length > 0) search();

    $(".company_dropdown").children().each(function () {
        if ($(this).val() == 0) {
            $(this).html("Default");
        }
    });

    $(".company_dropdown").on("change", function () {
        company = $(this).val();
        updateLists();
        getResultsToDisplay();
    });

    $("#add-search-term").on("click", function () {
        //TEST IF TEXT IN PREVIOUS ELEMENT
        var text = false;
        if ($("#more-terms").children().length > 0) {
            text = $("#more-terms").children(".search-field:last").children(".search-add").val() != "";
        }
        else {
            text = $("#advanced-search-bar").val().length > 0;
        }

        if (text) {
            $(this).parent().find(".andor").prop("disabled", true);
            $(this).parent().find(".module").prop("disabled", true);
            $(this).parent().find(".search-add").prop("readonly", true);
            $(this).parent().find(".search-add").prop("disabled", true);

            $.ajax({
                url: '/Search/AddField/',
                cache: false,
                success: function (html) {
                    $("#advanced-search-bar").prop("readonly", true);
                    $("#advanced-search-bar").prop("disabled", true);
                    $("#advsearchbtn").prop("disabled", true);
                    id++;
                    var idhtml = "<div class=\"termid\">" + id + "</div>";
                    html = $(html).append(idhtml);
                    $("#more-terms").append(html);
                }
            });
        }
        else {
            $("<div>Please use the above search area before adding another field. Thank You!</div>").dialog({
                title: 'Alert!',
                modal:true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }

    });
});

function rmField(e) {
    //REMOVE HTML AND REMOVE SEARCH
    
    $(e).parent().parent().remove();
    if ($("#more-terms").children().length == 0) {
        $("#advanced-search-bar").prop("readonly", false);
        $("#advanced-search-bar").prop("disabled", false);
        $("#advsearchbtn").prop("disabled", false);
    }
    else {
        $("#more-terms").children().children(".andor:last").prop("disabled", false);
        $("#more-terms").children().children(".andor:last").prop("readonly", false);
        $("#more-terms").children().children(".search-add:last").prop("disabled", false);
        $("#more-terms").children().children(".search-add:last").prop("readonly", false);
        $("#more-terms").children().children(".module:last").prop("disabled", false);
        $("#more-terms").children().children(".module:last").prop("readonly", false);
    }
    updateSearch();
};

var id = 0;

function add_processing() {
    $(".search-result").find(".subheader").find(".count").html("");
    $(".search-result .subheader .count").append(processing);
}

function rm_processing(section) {
    switch(section){
        case 'certificate':
            $("#certificates").find(".dataprocessing").remove();
            break;
        case 'company':
            $("#companies-search").find(".dataprocessing").remove();
            break;
        case 'policy':
            $("#policies").find(".dataprocessing").remove();
            break;
        case 'user':
            $("#users").find(".dataprocessing").remove();
            break;
        case 'vehicle':
            $("#vehicles").find(".dataprocessing").remove();
            break;
        case 'covernote':
            $("#covernotes").find(".dataprocessing").remove();
            break;
        case 'person':
            $("#people").find(".dataprocessing").remove();
            break;
        default:
            $(".dataprocessing").remove();
            break;
    }
}

function search(e) {
    add_processing();
    if (e != null) {
        var andor = $(e).parent().parent().children(".andor").val();
        var type = $(e).parent().parent().children(".module").val();
        var search = $(e).parent().parent().children(".search-add").val();
        var id = $(e).parent().parent().children(".termid").html();
        var URL = '/Search/AdvancedSearch/?search=' + search + '&andor=' + andor + '&type=' + type;
    }
    else {
        var type = "all";
        var andor = "or";
        var search = $("#advanced-search-bar").val();
        var id = 0;
        var URL = '/Search/AdvancedSearch/?search=' + search;
    }
    $.ajax({
        url: URL,
        cache: false,
        success: function (data) {
            if (data.result) {
                getResultsToDisplay();
            }
        }
    });
}

//THIS FUNCTION RUNS AND DISPLAYS ALL RELEVANT DATA
//BASED ON WHAT IS IN THE LISTS STORED IN THE FILE
function getResultsToDisplay() {
    
    $("#search-results-certificates").html("");
    $("#search-results-companies").html("");
    $("#search-results-policies").html("");
    $("#search-results-users").html("");
    $("#search-results-vehicles").html("");
    $("#search-results-covernotes").html("");
    $("#search-results-people").html("");

    $.ajax({
        url: '/Search/ShortCertificate/',
        cache: false,
        success: function (html) {
            rm_processing("certificate");
            if (html == "") $("#certificates").children(".search-headers").children(".count").text("No Permission to view");
            else if ($($(html)[0]).val() != 0) $("#certificates").children(".search-headers").children(".count").text($($(html)[0]).val() + " Found");
            else $("#certificates").children(".search-headers").children(".count").text("No Results Found");
            $("#search-results-certificates").html(html);

            if ($("#search-results-certificates").is(":visible")) $("#search-results-certificates").find('.scroll-table').perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });
        },
        error: function () {
            rm_processing("certificate");
            $('<div>There was an error in your search attempt. Please try again</div>').dialog({
                modal: true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    });
    
    $.ajax({
        url: '/Search/ShortCompany/',
        cache: false,
        success: function (html) {
            rm_processing("company");
            if (html == "") $("#companies-search").children(".search-headers").children(".count").text("No Permission to view");
            else if ($($(html)[0]).val() != 0) $("#companies-search").children(".search-headers").children(".count").text($($(html)[0]).val() + " Found");
            else $("#companies-search").children(".search-headers").children(".count").text("No Results Found");
            $("#search-results-companies").html(html);

            if ($("#search-results-companies").is(":visible")) $("#search-results-companies").find('.scroll-table').perfectScrollbar({
                                                                    wheelSpeed: 0.3,
                                                                    wheelPropagation: true,
                                                                    minScrollbarLength: 10,
                                                                    suppressScrollX: true
                                                                });
        },
        error: function () {
            rm_processing("company");
            $('<div>There was an error in your search attempt. Please try again</div>').dialog({
                modal: true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    });
    
    $.ajax({
        url: '/Search/ShortPolicies/',
        cache: false,
        success: function (html) {
            rm_processing("policy");
            if (html == "") $("#policies").children(".search-headers").children(".count").text("No Permission to view");
            else if ($($(html)[0]).val() != 0) $("#policies").children(".search-headers").children(".count").text($($(html)[0]).val() + " Found");
            else $("#policies").children(".search-headers").children(".count").text("No Results Found");
            $("#search-results-policies").html(html);

            if ($("#search-results-policies").is(":visible")) $("#search-results-policies").find('.scroll-table').perfectScrollbar({
                                                                    wheelSpeed: 0.3,
                                                                    wheelPropagation: true,
                                                                    minScrollbarLength: 10,
                                                                    suppressScrollX: true
                                                                });
        },
        error: function () {
            rm_processing("policy");
            $('<div>There was an error in your search attempt. Please try again</div>').dialog({
                modal: true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    });
    
    $.ajax({
        url: '/Search/ShortUser/',
        cache: false,
        success: function (html) {
            rm_processing("user");
            if (html == "") $("#users").children(".search-headers").children(".count").text("No Permission to view");
            else if ($($(html)[0]).val() != 0) $("#users").children(".search-headers").children(".count").text($($(html)[0]).val() + " Found");
            else  $("#users").children(".search-headers").children(".count").text("No Results Found");
            $("#search-results-users").html(html);

            if ($("#search-results-users").is(":visible")) $("#search-results-users").find('.scroll-table').perfectScrollbar({
                                                                    wheelSpeed: 0.3,
                                                                    wheelPropagation: true,
                                                                    minScrollbarLength: 10,
                                                                    suppressScrollX: true
                                                                });
        },
        error: function () {
            rm_processing("user");
            $('<div>There was an error in your search attempt. Please try again</div>').dialog({
                modal: true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    });
    
    $.ajax({
        url: '/Search/ShortVehicle/',
        cache: false,
        success: function (html) {
            rm_processing("vehicle");
            if (html == "") $("#vehicles").children(".search-headers").children(".count").text("No Permission to view");
            else if($($(html)[0]).val() != 0) $("#vehicles").children(".search-headers").children(".count").text($($(html)[0]).val() + " Found");
            else $("#vehicles").children(".search-headers").children(".count").text("No Results Found");
            $("#search-results-vehicles").html(html);

            if ($("#search-results-vehicles").is(":visible")) $("#search-results-vehicles").find('.scroll-table').perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });
        },
        error: function () {
            rm_processing("vehicle");
            $('<div>There was an error in your search attempt. Please try again</div>').dialog({
                modal: true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    });
    
    $.ajax({
        url: '/Search/ShortCoverNote/',
        cache: false,
        success: function (html) {
            rm_processing("covernote");
            if (html == "") $("#covernotes").children(".search-headers").children(".count").text("No Permission to view");
            else if ($($(html)[0]).val() != 0) $("#covernotes").children(".search-headers").children(".count").text($($(html)[0]).val() + " Found");
            else $("#covernotes").children(".search-headers").children(".count").text("No Results Found");
            $("#search-results-covernotes").html(html);

            if ($("#search-results-covernotes").is(":visible")) $("#search-results-covernotes").find('.scroll-table').perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });
        },
        error: function () {
            rm_processing("covernote");
            $('<div>There was an error in your search attempt. Please try again</div>').dialog({
                modal: true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    });

    $.ajax({
        url: '/Search/ShortPeople/',
        cache: false,
        success: function (html) {
            rm_processing("person");
            if (html == "") $("#people").children(".search-headers").children(".count").text("No Results Found");
            else if ($($(html)[0]).val() != 0) $("#people").children(".search-headers").children(".count").text($($(html)[0]).val() + " Found");
            else $("#people").children(".search-headers").children(".count").text("No Results Found");
            $("#search-results-people").html(html);

            if ($("#search-results-people").is(":visible")) $("#search-results-people").find('.scroll-table').perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });
        },
        error: function () {
            rm_processing("person");
            $('<div>There was an error in your search attempt. Please try again</div>').dialog({
                modal: true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    });
    
}


/* //////This section is for the edit/delete/details views of the different categories /////// */

function editVehicle(e) {
    var id = $(e).parent().parent().find("#vid").val();
    window.location.replace("/Vehicle/Edit/" + id);
}

function deleteVehicle(e) {
    var id = $(e).parent().parent().find("#vid").val();

    $("<div>You are about to delete this vehicle. Are you sure you want to do this? </div>").dialog({
        title: 'Alert!',
        modal:true,
        buttons: {
            OK: function () {

                $(this).dialog("close");
                $(this).dialog().dialog("destroy");

                $.ajax({
                    url: '/Vehicle/Delete/' + id,
                    cache: false,
                    success: function (data) {
                        if (data.result) {

                            $(e).parents(".shortveh").remove();

                            $("<div> Successfully Deleted! </div>").dialog({
                                modal:true,
                                title: 'Alert!',
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog("close");
                                        $(this).dialog().dialog("destroy");
                                    }
                                }
                            });
                        }
                        else {
                            $("<div> Vehicle could not be deleted! </div>").dialog({
                                title: 'Alert!',
                                modal:true,
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog("close");
                                        $(this).dialog().dialog("destroy");
                                    }
                                }
                            });

                        }
                    }
                });
            },
            Cancel: function () {
                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
            }
        }
    });
}

function editPolicy(e) {
    var id = $(e).parent().parent().find("#pid").val();
    window.location.replace("/Policy/Edit/" + id);
}

function deletePolicy(e) {

  var id = $(e).parent().parent().find("#pid").val();

  $("<div>You are about to delete this policy. Are you sure you want to do this? </div>").dialog({
    title: 'Alert!',
    modal:true,
    buttons: {
        OK: function () {

            $(this).dialog("close");
            $(this).dialog().dialog("destroy");

          $.ajax({
              url: '/Policy/Delete/' + id,
              cache: false,
              success: function (data) {
                  if (data.result) {

                      $(e).parents(".shortpol").remove();

                      $("<div> Successfully Deleted! </div>").dialog({
                          title: 'Alert!',
                          modal:true,
                          buttons: {
                              Ok: function () {
                                  $(this).dialog("close");
                                  $(this).dialog().dialog("destroy");
                              }
                          }
                      });
                  }
                  else {
                      $("<div> Policy could not be deleted! </div>").dialog({
                          title: 'Alert!',
                          modal:true,
                          buttons: {
                              Ok: function () {
                                  $(this).dialog("close");
                                  $(this).dialog().dialog("destroy");
                              }
                          }
                      });

                  }
              }
          });
      }, 
     Cancel: function () {
                $(this).dialog("close");
                $(this).dialog().dialog("destroy");
            }
        }
    });
}

function updateSearch() {
    
    var search = "";
    var URL = '';
    var andor = '';
    var type = '';

    add_processing();

    search = $("#advanced-search-bar").val();
    URL = '/Search/AdvancedSearch/?search=' + search;
    
    $.ajax({
        url: URL,
        cache: false,
        success: function (data) {
            if ($(".search-field").length == 0)
                getResultsToDisplay();
        }
    });


    setTimeout(timeOut, 1);

}

function ajaxCall(URL,i) {

    var result = false;

    $.ajax({
        url: URL,
        cache: false,
        async: false,
        success: function (data) {
            if ($(".search-field").length == i + 1)
                getResultsToDisplay();

            result = true;
            return true;
        },
        error: function () {

            $("<div>There was an error in searching.</div>").dialog({
                title: "Search Error",
                modal: true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                        $(this).dialog().dialog("destroy");
                    }
                }
            });
            return true;
        }
    });

    return result;
}

$(function () {
    if($('#page').val() == 'advancedSearchPage')
        $('#compDropDown').prop('required', false);
});

function timeOut() {

    $(".search-field").each(function (i, v) {
        andor = $(this).children(".andor").val();
        type = $(this).children(".module").val();
        search = $(this).children(".search-add").val();
        URL = '/Search/AdvancedSearch/?search=' + search + '&andor=' + andor + '&type=' + type;

        return ajaxCall(URL, i);

    });

}
