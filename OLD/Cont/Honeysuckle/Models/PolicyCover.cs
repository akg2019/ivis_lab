﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;

namespace Honeysuckle.Models
{
    public class PolicyCover
    {
        public int ID { get; set; }

        [RegularExpression("[a-zA-Z0-9 ][ a-zA-Z0-9-]*[a-zA-Z0-9 ]", ErrorMessage = "Incorrect policy cover format  (Only letters, numbers and hyphens allowed). Do not start or end the cover with a hypen please.")]
        public string prefix { get; set; }
        public string cover { get; set; }
        public bool isCommercialBusiness { get; set; }
        public List<Usage> usages { get; set; }
        public bool can_be_deleted { get; set; }

        public PolicyCover() { }

        public PolicyCover(string cover) 
        {
            this.cover = cover;
        }
        public PolicyCover(string prefix, string cover)
        {
            this.prefix = prefix;
            this.cover = cover;
        }
        public PolicyCover(int ID)
        {
            this.ID = ID;
        }
        public PolicyCover(int ID, string cover)
        {
            this.ID = ID;
            this.cover = cover;
        }
        public PolicyCover(int ID, string prefix, string cover)
        {
            this.ID = ID;
            this.prefix = prefix;
            this.cover = cover;
        }
        public PolicyCover(string cover, bool isCommercialBusiness)
        {
            this.cover = cover;
            this.isCommercialBusiness = isCommercialBusiness;
        }
        public PolicyCover(int Id, string cover, bool isCommercialBusiness)
        {
            this.ID = Id;
            this.cover = cover;
            this.isCommercialBusiness = isCommercialBusiness;
        }
        public PolicyCover(int Id, string prefix, string cover, bool isCommercialBusiness)
        {
            this.ID = Id;
            this.prefix = prefix;
            this.cover = cover;
            this.isCommercialBusiness = isCommercialBusiness;
        }
        public PolicyCover(int ID, string cover, bool isCommmercialBusiness, List<Usage> usages)
        {
            this.ID = ID;
            this.cover = cover;
            this.isCommercialBusiness = isCommercialBusiness;
            this.usages = usages;
        }
        public PolicyCover(int ID, string prefix, string cover, bool isCommmercialBusiness, List<Usage> usages)
        {
            this.ID = ID;
            this.prefix = prefix;
            this.cover = cover;
            this.isCommercialBusiness = isCommercialBusiness;
            this.usages = usages;
        }

    }


    public class PolicyCoverModel
    {
        /// <summary>
        /// this is on view (DONT DELETE)
        /// this function checks to see if a policy cover can be deleted
        /// </summary>
        /// <param name="cover"></param>
        /// <returns></returns>
        public static bool CanCoverBeDeleted(PolicyCover cover)
        {
            sql sql = new sql();
            bool result = false;
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC CanCoverBeDeleted " + cover.ID);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function updates the usages as it relates to the a specific cover
        /// </summary>
        /// <param name="cover"></param>
        public static void UpdateUsagesInCover(PolicyCover cover)
        {
            UpdateUsagesInCover(cover, Constants.user.ID);
        }

        /// <summary>
        /// this function updates the usages as it relates to the a specific cover
        /// </summary>
        /// <param name="cover"></param>
        /// <param name="uid"></param>
        public static void UpdateUsagesInCover(PolicyCover cover, int uid)
        {
            List<int> systemUsageID = new List<int>();
            List<int> newUsageID = new List<int>();
            
            //DeleteAllUsagesToCovers(cover, uid); // to be changed

            systemUsageID = GetUsagesWithCover(cover);
            foreach (Usage u in cover.usages)
                newUsageID.Add(u.ID);


            Tuple<List<int>, List<int>> difference = find_diff(systemUsageID, newUsageID);
            //DeleteAllUsagesToCovers()
            int count = 0;
            foreach (Usage u in cover.usages)
            {
                if (newUsageID.Count() != 0)
                {
                    if(difference.Item2.Count != 0)
                    AssociateUsageToCover(cover.ID, difference.Item2.ElementAt(count), uid);
                }
                else AssociateUsageToCover(cover.ID, UsageModel.CreateUsage(u).ID, uid);
            }
        }

        /// <summary>
        /// this function adds a cover/usage association
        /// </summary>
        /// <param name="coverid"></param>
        /// <param name="usageid"></param>
        public static void AssociateUsageToCover(int coverid, int usageid)
        {
            AssociateUsageToCover(coverid, usageid, Constants.user.ID);
        }
        
        /// <summary>
        /// this function adds a cover/usage association
        /// </summary>
        /// <param name="coverid"></param>
        /// <param name="usageid"></param>
        public static void AssociateUsageToCover(int coverid, int usageid, int uid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL() && usageid != 0)
            {
                SqlDataReader reader = sql.QuerySQL("EXEC AssociateUsageToCover " + coverid + "," + usageid + "," + uid, "create", "usage", new Usage(usageid));
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function removes all usages from a specific cover
        /// </summary>
        /// <param name="cover"></param>
        /// <param name="uid"></param>
        public static void DeleteAllUsagesToCovers(PolicyCover cover, int uid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DeleteAllUsagesToCovers " + cover.ID + "," + uid;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "usage", cover);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function receive usage IDs 
        /// </summary>
        /// <param name="cover"></param>
        public static List<int> GetUsagesWithCover(PolicyCover cover)
        {
            List<int> UsageIdList = new List<int>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC GetUsagesWithCover " + cover.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "usage", cover);

                while (reader.Read())
                {
                    UsageIdList.Add(int.Parse(reader["UsageID"].ToString()));
                }

            }

            return UsageIdList;
        }

        /// <summary>
        /// this function removes a cover from the system
        /// </summary>
        /// <param name="cover"></param>
        /// <returns></returns>
        public static bool DeleteCover(PolicyCover cover)
        {
            bool result = false;
            sql sql = new sql();
            if (CanCoverBeDeleted(cover))
            {
                if (sql.ConnectSQL())
                {
                    SqlDataReader reader = sql.QuerySQL("EXEC DeleteCover " + cover.ID + "," + Constants.user.ID, "delete", "policycover", cover);
                    while (reader.Read())
                    {
                        result = (bool)reader["result"];
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            return result;
        }
        
        /// <summary>
        /// this function updates all cover information in the database
        /// </summary>
        /// <param name="cover"></param>
        public static void UpdateCover(PolicyCover cover)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC UpdateCover " + cover.ID + ",'"
                                                   + Constants.CleanString(cover.prefix) + "','"
                                                   + Constants.CleanString(cover.cover) + "','"
                                                   + cover.isCommercialBusiness + "',"
                                                   + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "policycover", cover);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function retrieves all the policy covers in the system
        /// </summary>
        /// <returns></returns>
        public static List<PolicyCover> GetAllPolicyCovers()
        {
            List<PolicyCover> covers = new List<PolicyCover>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetAllPolicyCovers", "read", "policycover", new PolicyCover());
                while (reader.Read())
                {
                    PolicyCover cover = new PolicyCover(int.Parse(reader["ID"].ToString()), reader["prefix"].ToString(), reader["cover"].ToString(), (bool)reader["commercial"]);
                    cover.can_be_deleted = CanCoverBeDeleted(cover);
                    covers.Add(cover);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return covers;
        }

        /// <summary>
        /// this function retrieves all the policy covers in the system for your company
        /// </summary>
        /// <returns></returns>
        public static List<PolicyCover> GetAllPolicyCoversForCompany()
        {
            List<PolicyCover> covers = new List<PolicyCover>();
            sql sql = new sql();
            SqlDataReader reader;
            if (sql.ConnectSQL() && UserModel.TestGUID(Constants.user))
            {
                if (Constants.user.newRoleMode) reader = sql.QuerySQL("EXEC GetAllPolicyCoversForCompany " + Constants.user.tempUserGroup.company.CompanyID, "read", "policycover", new PolicyCover());
                else reader = sql.QuerySQL("EXEC GetAllPolicyCoversForCompany " + CompanyModel.GetMyCompany(Constants.user).CompanyID, "read", "policycover", new PolicyCover());

                while (reader.Read())
                {
                    PolicyCover cover = new PolicyCover(int.Parse(reader["ID"].ToString()), reader["prefix"].ToString(), reader["cover"].ToString(), (bool)reader["commercial"]);
                    cover.can_be_deleted = CanCoverBeDeleted(cover);
                    covers.Add(cover);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return covers;
        }

        /// <summary>
        /// This function adds a new policy cover to the system
        /// </summary>
        /// <param name="policyCover">Policy cover name </param>
        public static PolicyCover addPolicyCover(PolicyCover policyCover)
        {
            return addPolicyCover(policyCover, Constants.user.ID, CompanyModel.GetMyCompany(Constants.user).CompanyID);
        }

        /// <summary>
        /// this function adds a new policy cover to the system
        /// </summary>
        /// <param name="policyCover"></param>
        /// <param name="uid"></param>
        /// <param name="companyid"></param>
        /// <returns></returns>
        public static PolicyCover addPolicyCover(PolicyCover policyCover, int uid, int companyid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                //If statements to determine if isCommercialBusiness is true or false
                //policyCover.isCommercialBusiness = true; // will change depending on cover

                string query = "EXECUTE AddPolicyCover '"   + Constants.CleanString(policyCover.prefix) + "','" 
                                                            + Constants.CleanString(policyCover.cover) + "'," 
                                                            + policyCover.isCommercialBusiness + "," 
                                                            + uid + "," 
                                                            + companyid;
                SqlDataReader reader = sql.QuerySQL(query, "create", "policycover", policyCover);

                while (reader.Read())
                {
                    policyCover.ID = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return policyCover;
        }

        /// <summary>
        /// This function gets a list of all existing policy covers in the system
        /// </summary>
        /// <returns>List of policy covers</returns>
        public static List<PolicyCover> GetPolicyCovers(int compID)
        {
            List<PolicyCover> result = new List<PolicyCover>();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXECUTE GetCoversList " + compID);
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        string cov = (reader["Cover"].ToString());
                        int Id = int.Parse(reader["ID"].ToString());
                        result.Add(new PolicyCover(Id, cov));
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function retrieves a policy cover based on it's database id
        /// </summary>
        /// <param name="cover"></param>
        /// <returns></returns>
        public static PolicyCover GetCoverByID(PolicyCover cover)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetCoverByID " + cover.ID, "read", "policycover", new PolicyCover());
                while (reader.Read())
                {
                    cover = new PolicyCover(cover.ID, reader["prefix"].ToString(), reader["cover"].ToString(), (bool)reader["commercial"]);
                }
                reader.Close();
                cover.usages = UsageModel.GetPolicyCoverUsages(cover);
                sql.DisconnectSQL();
            }
            return cover;
        }

        /// <summary>
        /// this function retrieves the policy prefix of a cover by it's database id
        /// </summary>
        /// <param name="cover"></param>
        /// <returns></returns>
        public static string GetPolicyPrefix(PolicyCover cover)
        {
            string prefix = null;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetPolicyPrefix " + cover.ID, "read", "policycover", new PolicyCover());
                while (reader.Read())
                {
                    prefix = reader["prefix"].ToString();
                }
                sql.DisconnectSQL();
            }
            return prefix;
        }

        /// <summary>
        /// this function discovers the difference between two lists of integers
        /// </summary>
        /// <param name="current_list"></param>
        /// <param name="new_list"></param>
        /// <param name="imported"></param>
        /// <returns></returns>
        private static Tuple<List<int>, List<int>> find_diff(List<int> current_list, List<int> new_list, bool imported = false)
        {
            List<int> to_be_added = new List<int>();
            List<int> to_be_removed = new List<int>();

            foreach (int i in current_list)
            {
                if (!new_list.Contains(i)) to_be_added.Add(i);
            }

            if (!imported)
            {
                foreach (int i in new_list)
                {
                    if (!current_list.Contains(i)) to_be_removed.Add(i);
                }
            }

            Tuple<List<int>, List<int>> result = new Tuple<List<int>, List<int>>(to_be_added, to_be_removed);
            return result;
        }

    }
}
