﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;

namespace Honeysuckle.Models
{
    public class VehicleCoverData
    {
        public VehicleCoverData() { }
        public static string jsonData { get; set; }

        public static string parseJsonLoginInfo(JSON json)
        {
            JObject obj = JObject.Parse(json.json);
            string output = "";
            

            try
            {
                if (obj.Property("username") != null && obj.Property("password") != null)
                {
                     user_login_object ulo = UserModel.LoginUser(new User(obj["username"].ToString(), obj["password"].ToString()));
                     if (ulo.session_free && ulo.valid_user)
                     {
                         Constants.user = UserModel.GetUser(obj["username"].ToString());
                         UserModel.ResetFaultyLogins(obj["username"].ToString());
                         Constants.user.sessionguid = UserModel.GetSessionGUID(Constants.user);
                         output = "loggedIn";
                     }
                     else output = "logIn failed";
                }
                if (obj.Property("key") != null && obj.Property("kid") != null)
                {
                    if (UserModel.TestWebGUID(RSA.Decrypt(obj["key"].ToString(), "session", int.Parse(obj["kid"].ToString()))))
                    {
                        Constants.user = new User();
                        Constants.user = UserModel.getUserFromGUID(RSA.Decrypt(obj["key"].ToString(), "session", int.Parse(obj["kid"].ToString())));

                        if (obj.Property("logout") != null)
                        {
                            if ((bool)obj["logout"])
                            {
                                UserModel.Logout();
                                output = "loggedOut";
                            }
                        }
                        //else if()
                    }
                    else output = "failed";
                }
            }
            catch
            {
                output = "failed";
            }

            return output;
        }
    }
}