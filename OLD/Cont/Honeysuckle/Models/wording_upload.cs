﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Honeysuckle.Models
{
    public class wording_upload
    {
        /// <summary>
        /// This function takes a series of strings, parses those strings and 
        /// generates template wordings to be uploaded via the TemplateWording/Upload page
        /// </summary>
        /// <param name="data_lines"></param>
        /// <returns></returns>
        public static List<response> ParseData(List<string> data_lines)
        {
            List<TemplateWording> wordings = new List<TemplateWording>();
            TemplateWording word = new TemplateWording();
            List<response> responses = new List<response>();
            List<string> data_order = new List<string>();

            int i = 0;
            foreach (string data in data_lines)
            {
                i++;
                if (i == 1) data_order = get_data_order(data);
                else
                {
                    if (data_order.Count() == 0) break;
                    word = new TemplateWording();
                    if (data != "")
                    {
                        List<string> data_words = data.Split('\t').ToList();
                        if (data_words.Count() == data_order.Count())
                        {
                            for (int x = 0; x < data_words.Count(); x++)
                            {
                                try
                                {
                                    update_model(word, data_words[x], data_order[x]);
                                }
                                catch
                                {
                                    responses.Add(new response("error", "The file failed to process. Please check to ensure the correct file was uploaded."));
                                    return responses;
                                }
                            }
                            wordings.Add(word);
                        }
                        else responses.Add(new response("error", "The line '" + data + "' failed due to it having more or less fields than expected."));
                    }
                }
            }
            responses = TemplateWordingModel.AddWordings(wordings, responses);
            return responses;
        }

        /// <summary>
        /// this function parses a series of lines to create wording/cover/usage relationships in the database
        /// </summary>
        /// <param name="data_lines"></param>
        /// <returns></returns>
        public static List<response> ParseUsageData(List<string> data_lines)
        {
            List<TemplateWording> wordings = new List<TemplateWording>();
            List<word_usage_cover> word_usage_covers = new List<word_usage_cover>();
            List<response> responses = new List<response>();
            List<string> data_order = new List<string>();

            int i = 0;
            foreach (string data in data_lines)
            {
                i++;
                if (i == 1) data_order = get_data_order(data);
                else
                {
                    if (data_order.Count() == 0) break;
                    word_usage_cover word_usage_cover = new word_usage_cover();

                    if (data != "")
                    {
                        List<string> data_words = data.Split('\t').ToList();
                        if (data_words.Count() == data_order.Count())
                        {
                            for (int x = 0; x < data_words.Count(); x++)
                            {
                                try
                                {
                                    update_model(word_usage_cover, data_words[x], data_order[x]);
                                }
                                catch {
                                    responses.Add(new response("error", "The file failed to process. Please check to ensure the correct file was uploaded."));
                                    return responses;
                                }
                            }
                            word_usage_covers.Add(word_usage_cover);
                        }
                        else responses.Add(new response("error", "The line '" + data + "' failed due to it having more or less fields than expected."));
                    }
                }
            }
            responses = TemplateWordingModel.addWordingCoverUsages(word_usage_covers, responses);
            return responses;
        }

        /// <summary>
        /// this function splits a string into a list of strings that is relates to data being imported
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        private static List<string> get_data_order(string order)
        {
            return order.Split('\t').ToList();
        }

        /// <summary>
        /// this function updates hte wording object that is handed off here
        /// with attributes that are in the uploaded document to eventually be 
        /// fully formed with data required for the creation in the system
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="data"></param>
        /// <param name="data_type"></param>
        private static void update_model(object obj, object data, string data_type)
        {
            switch (data_type)
            {
                case "Type of Certificate":
                    ((TemplateWording)obj).CertificateType = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Cert ID":
                    ((TemplateWording)obj).CertificateID = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Cert Insured Code":
                    ((TemplateWording)obj).CertificateInsuredCode = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Cert Extension Code":
                    ((TemplateWording)obj).ExtensionCode = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Flag Excluded Driver":
                    ((TemplateWording)obj).ExcludedDriver = (data.ToString().ToLower().Trim() == "true");
                    break;
                case "Flag Excluded Insured":
                    ((TemplateWording)obj).ExcludedDriver = (data.ToString().ToLower().Trim() == "true");
                    break;
                case "Flag Multiple Vehicle":
                    ((TemplateWording)obj).MultipleVehicle = (data.ToString().ToLower().Trim() == "true");
                    break;
                case "Flag Registered Owner":
                    ((TemplateWording)obj).RegisteredOwner = (data.ToString().ToLower().Trim() == "true");
                    break;
                case "Flag Restricted Driver":
                    ((TemplateWording)obj).RestrictedDriver = (data.ToString().ToLower().Trim() == "true");
                    break;
                case "Flag is For a Scheme":
                    ((TemplateWording)obj).Scheme = (data.ToString().ToLower().Trim() == "true");
                    break;


                case "Is Excluded Driver":
                    ((TemplateWording)obj).ExcludedDriver = (data.ToString().ToLower().Trim() == "true");
                    break;
                case "Is Excluded Insured":
                    ((TemplateWording)obj).ExcludedDriver = (data.ToString().ToLower().Trim() == "true");
                    break;
                case "Has Multiple Vehicle":
                    ((TemplateWording)obj).MultipleVehicle = (data.ToString().ToLower().Trim() == "true");
                    break;
                case "Is Registered Owner":
                    ((TemplateWording)obj).RegisteredOwner = (data.ToString().ToLower().Trim() == "true");
                    break;
                case "Is Restricted Driver":
                    ((TemplateWording)obj).RestrictedDriver = (data.ToString().ToLower().Trim() == "true");
                    break;
                case "Is for a Scheme":
                    ((TemplateWording)obj).Scheme = (data.ToString().ToLower().Trim() == "true");
                    break;
                case "Auth Drivers":
                    ((TemplateWording)obj).AuthorizedDrivers = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Lim of Use":
                    ((TemplateWording)obj).LimitsOfUse = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Marks and Reg No":
                    ((TemplateWording)obj).VehRegNo = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Policy Holders":
                    ((TemplateWording)obj).PolicyHolders = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Veh Make":
                    ((TemplateWording)obj).VehMake = ProcessBadCarraigeReturn(data.ToString());
                    break;


                case "Text Auth Drivers":
                    ((TemplateWording)obj).AuthorizedDrivers = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Text Lim of Use":
                    ((TemplateWording)obj).LimitsOfUse = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Text Marks and Reg No":
                    ((TemplateWording)obj).VehRegNo = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Text Policy Holders":
                    ((TemplateWording)obj).PolicyHolders = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Text Veh Make":
                    ((TemplateWording)obj).VehMake = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Type of Cover":
                    ((word_usage_cover)obj).cover.cover = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Policy Prefix":
                    ((word_usage_cover)obj).cover.prefix = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Usage":
                    ((word_usage_cover)obj).usage.usage = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "Cert Type":
                    ((word_usage_cover)obj).cert_type = ProcessBadCarraigeReturn(data.ToString());
                    break;
                case "":
                    break;
                default:
                    throw new Exception();
                    break;
            }
        }

        /// <summary>
        /// this function changes all uses of the string _cr_ to a real carraige return
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static string ProcessBadCarraigeReturn(string input)
        {
            return input.Replace("_cr_", Environment.NewLine);
        }
    }

    /// <summary>
    /// this class manages the relationship between covers and usages and wordings
    /// </summary>
    public class word_usage_cover{
        public int word_id { get; set; }
        public string cert_type { get; set; }
        public Usage usage { get; set; }
        public PolicyCover cover { get; set; }

        public word_usage_cover() {
            this.usage = new Usage();
            this.cover = new PolicyCover();
        }

        public word_usage_cover(string cert_type, Usage usage, PolicyCover cover) {
            this.cert_type = cert_type;
            this.usage = usage;
            this.cover = cover;
        }

        public word_usage_cover(int word_id, Usage usage, PolicyCover cover)
        {
            this.word_id = word_id;
            this.usage = usage;
            this.cover = cover;
        }
    }
}