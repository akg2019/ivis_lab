﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;

namespace Honeysuckle.Models
{
    public class Usage
    {
        [RegularExpression("[0-9]+")]
        public int ID { get; set; }
        public string usage { get; set; }
        public bool in_use { get; set; }

        public Usage() { }
        public Usage(string usage)
        {
            this.usage = usage;
        }

        public Usage(int usage)
        {
            this.ID = usage;
        }
        public Usage(int ID, string usage)
        {
            this.ID = ID;
            this.usage = usage;
        }
    }
    public class UsageModel
    {
        /// <summary>
        /// this function adds a usage to the system
        /// </summary>
        /// <param name="usage">usage</param>
        /// <returns></returns>
        public static Usage CreateUsage(Usage usage)
        {
            return CreateUsage(usage, Constants.user.ID);
        }

        /// <summary>
        /// this function adds a usage to the system
        /// </summary>
        /// <param name="usage">usage</param>
        /// <param name="uid">user id</param>
        /// <returns></returns>
        public static Usage CreateUsage(Usage usage, int uid = 0)
        {
            sql sql = new sql();
            if (sql.ConnectSQL() && usage.usage != null && usage.usage != "")
            {
                string query = "EXEC CreateUsage '" + Constants.CleanString(usage.usage) + "'," + uid;
                SqlDataReader reader = sql.QuerySQL(query, "create", "usage", usage);
                while (reader.Read())
                {
                    usage.ID = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return usage;
        }

        //public static List<Usage> GetAllUsages()
        //{
        //    List<Usage> usages = new List<Usage>();
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        SqlDataReader reader = sql.QuerySQL("EXEC GetAllUsages");
        //        while (reader.Read())
        //        {
        //            usages.Add(new Usage(int.Parse(reader["ID"].ToString()), reader["usage"].ToString()));
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return usages;
        //}

        /// <summary>
        /// this function returns all the usages related to a specific cover
        /// </summary>
        /// <param name="cover"></param>
        /// <returns></returns>
        public static List<Usage> GetPolicyCoverUsages(PolicyCover cover)
        {
            List<Usage> usages = new List<Usage>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetPolicyCoverUsages " + cover.ID, "read", "usage", new Usage());
                while (reader.Read())
                {
                    usages.Add(new Usage(int.Parse(reader["ID"].ToString()), reader["usage"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();

                int i, j;
                Usage key = new Usage();
                for (i = 1; i < usages.Count(); i++)
                {
                    key = usages[i];
                    j = i - 1;


                    while (j >= 0 && usages[j].usage.CompareTo(key.usage) > 0)
                    {
                        usages[j + 1] = usages[j];
                        j = j - 1;
                    }
                    usages[j + 1] = key;
                }
            }
            return usages;
        }

        /// <summary>
        /// this function retrieves a specific usage
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cover_id"></param>
        /// <returns></returns>
        public static Usage GetUsage(int id, int cover_id = 0)
        {
            Usage usage = new Usage();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetUsage " + id, "read", "usage", new Usage());
                while (reader.Read())
                {
                    usage = new Usage(id, reader["usage"].ToString());
                    usage.in_use = IsUsagesInUse(usage, cover_id);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return usage;
        }

        /// <summary>
        /// this function determines whether a usage is currently in use in the system
        /// </summary>
        /// <param name="usage">usage</param>
        /// <param name="coverid">cover id</param>
        /// <returns></returns>
        private static bool IsUsagesInUse(Usage usage, int coverid = 0)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsUsagesInUse " + usage.ID;
                if (coverid != 0) query += "," + coverid;
                SqlDataReader reader = sql.QuerySQL(query, "read", "usage", new Usage());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// remove usage from a cover
        /// </summary>
        /// <param name="usage"></param>
        /// <param name="cover"></param>
        /// <returns></returns>
        public static bool RmUsageFromCover(Usage usage, PolicyCover cover)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC RmUsageFromCover " + usage.ID + "," + cover.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "usage", usage);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this returns to top usage from the database of the current policy cover associated to a specified policy
        /// </summary>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static Usage get_top_usage(Policy policy)
        {
            Usage usage = new Usage();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC get_top_usage " + policy.ID);
                while (reader.Read())
                {
                    usage.ID = int.Parse(reader["id"].ToString());
                    usage.usage = reader["usage"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return usage;
        }

        public static bool EditUsage(Usage usage)
        {
            sql sql = new sql();
            bool result = false;

            try
            {
                if (sql.ConnectSQL())
                {
                    SqlDataReader reader = sql.QuerySQL("EXEC EditUsage " + usage.ID + ",'" + Constants.CleanString(usage.usage) + "'");
                    result = true;
                }
            }
            catch (Exception e)
            {
                result = false;
            }

            return result;
        }

        
    }
}