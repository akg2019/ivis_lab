﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Honeysuckle.Models
{
    public class sql
    {
        #region old
        //private string datasource { get; set; }
        //private string dbname { get; set; }
        //private string username { get; set; }
        //private string password { get; set; }
        #endregion
        private string sqlConnectionString { get; set; }
        private SqlConnection conn { get; set; }
        
        /// <summary>
        /// This function connects the application to the database
        /// </summary>
        /// <returns>boolean indicating as to whether the application succeeded in connection</returns>
        public bool ConnectSQL()
        {
            bool result;
            try
            {
                #if DEBUG
                sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;//ApplicationServices
                #else
                sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ExternalConnection"].ConnectionString; 
         #endif

                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                conn = new SqlConnection(sqlConnectionString);
                if (conn.State == ConnectionState.Closed) conn.Open();
                result = true;
            }
            catch (Exception e)
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                Log.LogRecord("sql", "failure to connect");
                Log.LogRecord("default", e.Message);
                result = false;
            }
            return result;
            
        }

        /// <summary>
        /// This function closes a database connection
        /// </summary>
        /// <returns></returns>
        public bool DisconnectSQL() 
        {
            bool result;
            try
            {
                if (conn == null)
                {  //ReleaseConnection ExternalConnection
                    #if DEBUG
                    sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;//ApplicationServices
                    #else
                    sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ExternalConnection"].ConnectionString; 
                    #endif

                    conn = new SqlConnection(sqlConnectionString);
                }
                conn.Close();
                conn.Dispose();
                SqlConnection.ClearAllPools();
                result = true;
            }
            catch (Exception e)
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                Log.LogRecord("sql", "failure to disconnect");
                Log.LogRecord("default", e.Message);
                result = false;
            }
            return result;
        }

        /// <summary>
        /// This function manages making an SQL query to the database
        /// </summary>
        /// <param name="query">full database request</param>
        /// <returns>return SQLDataReader</returns>
        public SqlDataReader QuerySQL(string query)
        {
            try
            {
                if (conn != null && conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                SqlCommand command = new SqlCommand(query, conn);
                return command.ExecuteReader();
            }
            catch (Exception e)
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                Log.LogRecord("sql", "failure to query; " + query);
                Log.LogRecord("default", e.Message);
                return null;
            }
        }

        /// <summary>
        /// this is the version of the query sql function that is called by the application when the user is logged into the website and wants to log changes to the database
        /// </summary>
        /// <param name="query">query</param>
        /// <param name="crud">create, read, update, delete</param>
        /// <param name="location">table name</param>
        /// <param name="obj">new data</param>
        /// <param name="sub_object">further elaboration as to action being taken</param>
        /// <param name="multiples">if more than one record will be affected</param>
        /// <returns>rows from query</returns>
        public SqlDataReader QuerySQL(string query, string crud, string location, object obj, string sub_object = null, bool multiples = false)
        {
            return QuerySQL(query, crud, location, obj, Constants.user.ID, sub_object, multiples);
        }

        /// <summary>
        /// this is the version of the query sql function that is called by the application when the user is not logged into the website (i.e. webservice) and wants to log changes to the database
        /// </summary>
        /// <param name="query">query</param>
        /// <param name="crud">create, read, update, delete</param>
        /// <param name="location">table name</param>
        /// <param name="obj">new data</param>
        /// <param name="uid">user who is making the modification</param>
        /// <param name="sub_object">further elaboration as to action being taken</param>
        /// <param name="multiples">if more than one record will be affected</param>
        /// <returns>rows from query</returns>
        public SqlDataReader QuerySQL(string query, string crud, string location, object obj, int uid, string sub_object = null, bool multiples = false)
        {
            bool log = true;                
            object old_data; object new_data; int reference_id; string note;
            List<DifferentObjects> diff = new List<DifferentObjects>();
            string oldValString = "", newValString = "",SerializedOldData = "", SerializedNewData ="";

            switch (crud)
            {
                case "create":
                    old_data = new object();
                    new_data = obj;
                    reference_id = 0;
                    //reference_id = get_data_id(new_data);
                    break;
                case "read":
                    log = false;
                    old_data = new object();
                    new_data = new object();
                    reference_id = 0;
                    break;
                case "update":
                    old_data = get_old_data(obj);
                    new_data = obj;
                    //diff = findDiffInObject(AuditModel.SerializeObject(old_data, old_data.GetType().ToString()), AuditModel.SerializeObject(new_data, new_data.GetType().ToString()));
                    reference_id = get_data_id(old_data);
                    break;
                case "delete":
                    old_data = obj;
                    new_data = new object();
                    reference_id = get_data_id(old_data);
                    break;
                default:
                    log = false;
                    old_data = new object();
                    new_data = new object();
                    reference_id = 0;
                    break;
            }
            try
            {
                if (log) old_data = update_old_date(crud, location, old_data, new_data, reference_id, log, sub_object, multiples);
                SqlDataReader reader = QuerySQL(query);
                try
                {
                    string[] locationArr = { "policy", "vehiclecovernote", "vehiclecertificate", "driver", "notification","vehicle","user","person" };
                    if ( reference_id == 0 && crud == "create" && locationArr.Contains(location)
                        ) reference_id = AuditModel.getLastInsertedID(location);
                }
                catch
                {

                }
               // if (crud == "update")
                //{
                if (log)
                {
                    
                    if (crud == "update")
                    {
                        object temp = null;
                        temp = get_old_data(new_data);
                        if (temp != null) new_data = temp;
                        temp = null;
                    }
                    

                    SerializedOldData = AuditModel.SerializeObject(old_data, old_data.GetType().ToString());
                    SerializedNewData = AuditModel.SerializeObject(new_data, new_data.GetType().ToString());

                    if (crud == "update")
                        diff = findDiffInObject(SerializedOldData, SerializedNewData);
                }
               // }
                    if (log)  
                {
                    User user = UserModel.GetUser(uid);
                    int pid = 0;
                    if (user.employee != null){
                        if (user.employee.person != null) pid = user.employee.person.PersonID;
                    }
                    //new_data = update_new_data(crud, location, old_data, new_data, reference_id, log, sub_object, multiples);
                    #region update new data
                    note = get_note(crud, location, sub_object);
                    if ((crud == "create" || crud == "delete") && (location == "company" || location == "person" || location == "vehicle") && sub_object == "policyholder")
                    {
                        crud = "update";
                        location = "policy";
                        if (crud == "create") new_data = PolicyModel.getPolicyFromID(((Policy)new_data).ID);
                        if (crud == "delete") new_data = PolicyModel.getPolicyFromID(((Policy)old_data).ID);
                    }
                    #endregion


                    if (crud == "update")
                    {
                        newValString = "";
                        oldValString = "";

                        oldValString += "{";
                        foreach (DifferentObjects d in diff)
                        {
                            oldValString += "\"" + d.property + "\"" + ":" + "\"" + d.oldVal + "\"" + ",";
                        }
                        oldValString = oldValString.Substring(0, oldValString.Length - 1).Length > 0 ? oldValString.Substring(0, oldValString.Length - 1) + "}" : "{}";

                        newValString += "{";
                        foreach (DifferentObjects d in diff)
                        {
                            newValString += "\"" + d.property + "\"" + ":" + "\"" + d.newVal + "\"" + ",";
                        }
                        newValString = newValString.Substring(0, newValString.Length - 1).Length > 0 ? newValString.Substring(0, newValString.Length - 1) + "}" : "{}";
                    }
                    else
                    {
                        newValString = SerializedNewData;
                        oldValString = SerializedOldData;
                    }
                    AuditModel.AuditLogThreadHandler(new Audit(crud, note, DateTime.Now, oldValString, newValString, new Person(pid), Constants.ipaddress, location, reference_id, multiples, new_data), uid);
                       // AuditModel.AuditLogThreadHandler(new Audit(crud, note, DateTime.Now, old_data, new_data, new Person(pid), Constants.ipaddress, location, reference_id, multiples), uid);
                }
                return reader;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// this function creates the note of what the query is actually doing for the audit log.
        /// </summary>
        /// <param name="crud">create, read, update, delete</param>
        /// <param name="location">table name</param>
        /// <param name="sub_object">further elaboration on object</param>
        /// <returns>string decribing action</returns>
        private static string get_note(string crud, string location, string sub_object)
        {
            string note = "";
            switch (crud)
            {
                case "create":
                    switch (location)
                    {
                        case "company":
                        case "person":
                        case "vehicle":
                            if (sub_object == "policyholder")
                            {
                                note = "add " + location + " to policy";
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case "delete":
                    switch (location)
                    {
                        case "company":
                        case "person":
                        case "vehicle":
                            if (sub_object == "policyholder")
                            {
                                note = "remove " + location + " to policy";
                            }
                            break;
                        case "policy":
                            if (sub_object == "remove drivers") note = "remove driver from policy";
                            break;
                        default:
                            break;
                    }
                    break;
                case "update":
                case "read":
                default:
                    break;
            }
            if (note == "") note = crud + " " + location;
            return note;
        }

        /// <summary>
        /// this function retrieves the 'old' version of this object being updated for the audit log purposes
        /// </summary>
        /// <param name="crud">create, read, update, delete</param>
        /// <param name="location">table name</param>
        /// <param name="old_data">old data</param>
        /// <param name="new_data">new data</param>
        /// <param name="reference_id">id of record being affected</param>
        /// <param name="log">whether this will be logged or not</param>
        /// <param name="sub_object">further description</param>
        /// <param name="multiples">if more than one record is being affected</param>
        /// <returns>old data updated</returns>
        private static object update_old_date(string crud, string location, object old_data, object new_data, int reference_id, bool log, string sub_object = null, bool multiples = false)
        {
            if (multiples && log)
            {
                if (sub_object == "usergroup" && location == "company" && crud == "delete") old_data = UserGroupModel.GetAllUserGroups(reference_id);
            }
            if (log)
            {
                if ((location == "company" || location == "person" || location == "vehicle") && sub_object == "policyholder")
                {
                    if (crud == "create") old_data = PolicyModel.getPolicyFromID(((Policy)new_data).ID);
                    if (crud == "delete") old_data = PolicyModel.getPolicyFromID(((Policy)old_data).ID);
                }
                if (location == "intermediary")
                {
                    if (crud == "delete") old_data = IntermediaryModel.GetIntermediary(((Intermediary)old_data).ID);
                }
                if (location == "policy" && sub_object == "remove drivers" && crud == "delete")
                {
                    old_data = PolicyModel.getPolicyFromID(((Policy)old_data).ID);
                }
            }
            return old_data;
        }

        //private static object update_new_data(string crud, string location, object old_data, object new_data, int reference_id, bool log, string sub_object = null, bool multiples = false)
        //{
        //    if (crud == "create" && location == "company" && sub_object == "policyholder")
        //    {
        //        crud = "update";
        //        location = "policy";
        //        new_data = PolicyModel.getPolicyFromID(((Policy)new_data).ID);
        //    }
        //    return new_data;
        //}

        /// <summary>
        /// this function retrieves the id of the object in question
        /// </summary>
        /// <param name="obj">any object from the system</param>
        /// <returns>the database record id</returns>
        private int get_data_id(object obj)
        {
            int id = 0;
            switch (obj.GetType().ToString().ToLower())
            {
                #region switch code
                case "honeysuckle.models.address":
                    id = ((Address)obj).ID;
                    break;
                case "honeysuckle.models.company":
                    id = ((Company)obj).CompanyID;
                    break;
                case "honeysuckle.models.driver":
                    id = ((Driver)obj).DriverId;
                    break;
                case "honeysuckle.models.email":
                    id = ((Email)obj).emailID;
                    break;
                case "honeysuckle.models.employee":
                    id = ((Employee)obj).EmployeeID;
                    break;
                case "honeysuckle.models.intermediary":
                    id = ((Intermediary)obj).ID;
                    break;
                case "honeysuckle.models.mappingdata":
                    id = ((MappingData)obj).id;
                    break;
                case "honeysuckle.models.notification":
                    id = ((Notification)obj).ID;
                    break;
                case "honeysuckle.models.person":
                    id = ((Person)obj).PersonID;
                    break;
                case "honeysuckle.models.phone":
                    id = ((Phone)obj).PhoneNumberId;
                    break;
                case "honeysuckle.models.policy":
                    id = ((Policy)obj).ID;
                    break;
                case "honeysuckle.models.policycover":
                    id = ((PolicyCover)obj).ID;
                    break;
                case "honeysuckle.models.road":
                    id = ((Road)obj).RoadID;
                    break;
                case "honeysuckle.models.roadtype":
                    id = ((RoadType)obj).RTID;
                    break;
                case "honeysuckle.models.rsa":
                    id = ((RSA)obj).id;
                    break;
                case "honeysuckle.models.taj":
                    id = ((TAJ)obj).id;
                    break;
                case "honeysuckle.models.templateimage":
                    id = ((TemplateImage)obj).ID;
                    break;
                case "honeysuckle.models.templatewording":
                    id = ((TemplateWording)obj).ID;
                    break;
                case "honeysuckle.models.usage":
                    id = ((Usage)obj).ID;
                    break;
                case "honeysuckle.models.user":
                    id = ((User)obj).ID;
                    break;
                case "honeysuckle.models.usergroup":
                    id = ((UserGroup)obj).UserGroupID;
                    break;
                case "honeysuckle.models.userpermission":
                    id = ((UserPermission)obj).id;
                    break;
                case "honeysuckle.models.vehicle":
                    id = ((Vehicle)obj).ID;
                    break;
                case "honeysuckle.models.vehiclecertificate":
                    id = ((VehicleCertificate)obj).VehicleCertificateID;
                    break;
                case "honeysuckle.models.vehiclecovernote":
                    id = ((VehicleCoverNote)obj).ID;
                    break;
                case "honeysuckle.models.vehiclecovernotegenerated":
                    id = ((VehicleCoverNoteGenerated)obj).ID;
                    break;
                case "honeysuckle.models.zipcode":
                    id = ((ZipCode)obj).ZCID;
                    break;
                case "honeysuckle.models.kpi":
                    id = ((KPI)obj).id;
                    break;
                case "honeysuckle.models.businessintelligence":
                case "honeysuckle.models.certcover":
                case "honeysuckle.models.country":
                case "honeysuckle.models.enquiry":
                case "honeysuckle.models.error":
                case "honeysuckle.models.handdrivetype":
                case "honeysuckle.models.import":
                case "honeysuckle.models.intelcount":
                case "honeysuckle.models.intelcountcompany":
                case "honeysuckle.models.json":
                case "honeysuckle.models.mortgagee":
                case "honeysuckle.models.parish":
                //case "honeysuckle.models.phone":
                case "honeysuckle.models.city":
                case "honeysuckle.models.suberror":
                default:
                    obj = null;
                    break;
                #endregion
            }
            return id;
        }

        /// <summary>
        /// this function retrieves the old data from the database for a record before any action is taken on it.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private object get_old_data(object obj)
        {
            switch (obj.GetType().ToString().ToLower())
            {
                #region switch code
                case "honeysuckle.models.address":
                    obj = AddressModel.GetAddress((Address)obj);
                    break;
                case "honeysuckle.models.company":
                    obj = CompanyModel.GetCompany(((Company)obj).CompanyID);
                    break;
                case "honeysuckle.models.driver":
                    obj = DriverModel.getDriver(((Driver)obj).DriverId);
                    break;
                case "honeysuckle.models.email":
                    obj = EmailModel.GetEmail((Email)obj);
                    break;
                case "honeysuckle.models.employee":
                    EmployeeModel.GetEmployee((Employee)obj);
                    break;
                case "honeysuckle.models.intermediary":
                    obj = IntermediaryModel.GetIntermediary(((Intermediary)obj).ID);
                    break;
                case "honeysuckle.models.mappingdata":
                    obj = MappingDataModel.GetMappingData((MappingData)obj);
                    break;
                case "honeysuckle.models.notification":
                    obj = NotificationModel.GetThisNotification(((Notification)obj).ID);
                    break;
                case "honeysuckle.models.person":
                    obj = PersonModel.GetPerson(((Person)obj).PersonID);
                    break;
                case "honeysuckle.models.phone":
                    obj = PhoneModel.GetPhone((Phone)obj);
                    break;
                case "honeysuckle.models.policy":
                    obj = PolicyModel.getPolicyFromID(((Policy)obj).ID);
                    break;
                case "honeysuckle.models.policycover":
                    obj = PolicyCoverModel.GetCoverByID((PolicyCover)obj);
                    break;
                case "honeysuckle.models.taj":
                    obj = TAJModel.GetCompanyTAJCredentials((TAJ)obj);
                    break;
                case "honeysuckle.models.templateimage":
                    obj = TemplateImageModel.GetTemplateImage(((TemplateImage)obj).ID);
                    break;
                case "honeysuckle.models.templatewording":
                    obj = TemplateWordingModel.GetWording(((TemplateWording)obj).ID);
                    break;
                case "honeysuckle.models.usage":
                    obj = UsageModel.GetUsage(((Usage)obj).ID);
                    break;
                case "honeysuckle.models.user":
                    obj = UserModel.GetUser(((User)obj).ID);
                    break;
                case "honeysuckle.models.usergroup":
                    obj = UserGroupModel.GetUserGroup(((UserGroup)obj).UserGroupID);
                    break;
                case "honeysuckle.models.vehicle":
                    obj = VehicleModel.GetVehicle(((Vehicle)obj).ID);
                    break;
                case "honeysuckle.models.vehiclecertificate":
                    obj = VehicleCertificateModel.GetThisCertificate((VehicleCertificate)obj);
                    break;
                case "honeysuckle.models.vehiclecovernote":
                    obj = VehicleCoverNoteModel.GetVehicleCoverNoteByNumber(((VehicleCoverNote)obj).ID);
                    break;
                case "honeysuckle.models.vehiclecovernotegenerated":
                    obj = VehicleCoverNoteGeneratedModel.GetGenCoverNote(((VehicleCoverNoteGenerated)obj).ID);
                    break;
                case "honeysuckle.models.kpi":
                    obj = KPIModel.get_single_kpi((KPI)obj);
                    break;
                case "honeysuckle.models.city":
                case "honeysuckle.models.zipcode":
                case "honeysuckle.models.userpermission":
                case "honeysuckle.models.suberror":
                case "honeysuckle.models.rsa":
                case "honeysuckle.models.road":
                case "honeysuckle.models.roadtype":
                case "honeysuckle.models.businessintelligence":
                case "honeysuckle.models.certcover":
                case "honeysuckle.models.country":
                case "honeysuckle.models.enquiry":
                case "honeysuckle.models.error":
                case "honeysuckle.models.handdrivetype":
                case "honeysuckle.models.import":
                case "honeysuckle.models.intelcount":
                case "honeysuckle.models.intelcountcompany":
                case "honeysuckle.models.json":
                case "honeysuckle.models.mortgagee":
                case "honeysuckle.models.parish":
                //case "honeysuckle.models.phone":
                default:
                    obj = null;
                    break;
                #endregion
            }
            return obj;
        }

        private List<DifferentObjects> findDiffInObject(string oldObj, string newObj)
        {
           
            List<DifferentObjects> diffObj = new List<DifferentObjects>();
           

            JObject oldJson = JObject.Parse(oldObj);
            JObject newJson = JObject.Parse(newObj);

            var newProps = newJson.Properties().ToList();
            var oldProps = oldJson.Properties().ToList();



            foreach (var newprop in newProps)
            {
                foreach (var oldprop in oldProps)
                {
                    if (oldprop.Name == newprop.Name && newprop.Value.ToString() != oldprop.Value.ToString())
                        diffObj.Add(new DifferentObjects(newprop.Name, oldprop.Value.ToString(), newprop.Value.ToString()));
                }
            }


            return diffObj;
        }

        


        private class DifferentObjects
        {
            public string property;
            public string oldVal;
            public string newVal;

            public DifferentObjects(string property, string oldVal, string newVal)
            {
                this.property = property;
                this.oldVal = oldVal;
                this.newVal = newVal;
            }
            public DifferentObjects() { }
            
        }

    }

}
