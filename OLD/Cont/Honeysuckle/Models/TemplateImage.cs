﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.IO;

namespace Honeysuckle.Models
{
    public class TemplateImage
    {
        public int ID { get; set; }
        public string type { get; set; }
        public string position { get; set; }
        public string location { get; set; }
        public Company company { get; set; }

        public TemplateImage() { }
        public TemplateImage(int ID)
        {
            this.ID = ID;
        }
        public TemplateImage(string type, string position, string location)
        {
            this.type = type;
            this.position = position;
            this.location = location;
        }
        public TemplateImage(int ID, string type, string position, string location)
        {
            this.ID = ID;
            this.type = type;
            this.position = position;
            this.location = location;
        }
        public TemplateImage(string type, string position, string location, Company company)
        {
            this.type = type;
            this.position = position;
            this.location = location;
            this.company = company;
        }
        public TemplateImage(int ID, string type, string position, string location, Company company)
        {
            this.ID = ID;
            this.type = type;
            this.position = position;
            this.location = location;
            this.company = company;
        }
    }
    public class TemplateImageModel
    {

        ///<summary>
        ///this function returns a specific template image from a list of template images for a position and type of 
        ///</summary>
        ///<param name="images">List of Template Images</param>
        ///<param name="type">certificate/cover</param>
        ///<param name="position">logo/header/footer</param>
        ///<returns>template image that matches that type and position</returns>
        public static TemplateImage RetrieveTemplateImage(List<TemplateImage> images, string type, string position)
        {
            TemplateImage templage_image = new TemplateImage();
            for (int i = 0; i < images.Count(); i++)
            {
                if (images.ElementAt(i).type.ToLower() == type.ToLower() && images.ElementAt(i).position.ToLower() == position.ToLower())
                {
                    templage_image = images.ElementAt(i);
                    break;
                }
            }
            if (templage_image.ID == 0)
            {
                templage_image.type = type.ToLower();
                templage_image.position = position.ToLower();
            }
            return templage_image;
        }

        /// <summary>
        /// this function tests if the type of image being tagged is acceptable
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool AcceptableType(string type)
        {
            return (type.ToLower().Trim() == "certificate" || type.ToLower().Trim() == "cover note");
        }

        /// <summary>
        /// this function tests if the location of the image is acceptable
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public static bool AcceptableLocation(string location)
        {
            return (location.ToLower().Trim() == "header" || location.ToLower().Trim() == "footer" || location.ToLower().Trim() == "logo");
        }
        
        /// <summary>
        /// this function removes the reference to the image location in the database
        /// </summary>
        /// <param name="templateimage"></param>
        /// <returns></returns>
        public static bool DeleteTemplateImage(TemplateImage templateimage)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DeleteTemplateImage " + templateimage.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "templateimage", templateimage);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }
                
        /// <summary>
        /// this function adds a reference to the database of the location of images needed for 
        /// cover notes and certificate prints
        /// </summary>
        /// <param name="templateimage"></param>
        public static int AddTemplateImage(TemplateImage templateimage)
        {
            int result = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "AddTemplateImage '" + Constants.CleanString(templateimage.type) + "','"
                                                    + Constants.CleanString(templateimage.position) + "','"
                                                    + Constants.CleanString(templateimage.location) + "',"
                                                    + templateimage.company.CompanyID + ","
                                                    + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "create", "templateimage", templateimage);
                while (reader.Read())
                {
                    result = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function returns a specific template image record
        /// </summary>
        /// <param name="templateImage">A template image model with a valid id</param>
        /// <returns></returns>
        public static TemplateImage GetTemplateImage(int id)
        {
            TemplateImage template_image = new TemplateImage();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetTemplateImage " + id, "read", "templateimage", new TemplateImage());
                while (reader.Read())
                {
                    template_image.ID = id;
                    template_image.type = reader["template_type"].ToString();
                    template_image.position = reader["image_position"].ToString();
                    template_image.location = reader["image_location"].ToString();
                    template_image.company = new Company(int.Parse(reader["companyid"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return template_image;
        }

        /// <summary>
        /// this function returns a company's entire images uploaded to the server
        /// </summary>
        /// <param name="company">A company model that represents the insurer you want to get images of</param>
        /// <returns></returns>
        public static List<TemplateImage> GetCompanyTemplateImages(Company company)
        {
            List<TemplateImage> templateimages = new List<TemplateImage>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetCompanyTemplateImages " + company.CompanyID);
                while (reader.Read())
                {
                    templateimages.Add(new TemplateImage(int.Parse(reader["ID"].ToString()), reader["template_type"].ToString(), reader["image_position"].ToString(), reader["image_location"].ToString(), company));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return templateimages;
        }

        /// <summary>
        /// this function retrieves the server location of the image based on position, type and company
        /// </summary>
        /// <param name="company">insurance company who's images we're checking for</param>
        /// <param name="position">header, footer, logo</param>
        /// <param name="type">certificate, cover note</param>
        /// <returns>location of image</returns>
        public static string get_image_location(Company company, string position, string type)
        {
            string location = "";
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC get_image_location '" + position + "','" + type + "'," + company.CompanyID);
                while (reader.Read())
                {
                    if (reader["location"] != DBNull.Value) location = "/files/" + reader["location"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            if (location == "/files/") location = "";
            return location;
        }

    }

}