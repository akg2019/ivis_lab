﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Honeysuckle.Models
{
    public class BusinessIntelligence
    {
        
        public List<KPI> kpis { get; set; }
        public Company company { get; set; }

        public BusinessIntelligence() { }
        public BusinessIntelligence(List<KPI> kpis, Company company)
        {
            this.kpis = kpis;
            this.company = company;
        }

    }
    public class BusinessIntelligenceModel
    {
        /// <summary>
        /// this function is uded to retreive all KPI data as it relates to any specific company
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public static BusinessIntelligence GetAllBusinessIntelligence(Company company)
        {
            return new BusinessIntelligence(KPIModel.GetKpis(company), company);
        }

        /// <summary>
        /// this function updates all the KPI limits for a company
        /// </summary>
        /// <param name="bi"></param>
        public static void UpdateIntelligence(BusinessIntelligence bi)
        {
            foreach (KPI kpi in bi.kpis)
            {
                KPIModel.UpdateKPI(kpi);
            }
        }
    }
}