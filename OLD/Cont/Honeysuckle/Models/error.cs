﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class error
    {
        public string type { get; set; }
        public string id { get; set; }
        public List<suberror> issues { get; set; }

        public error() { }
        public error(string type, string id)
        {
            this.type = type;
            this.id = id;
        }
        public error(string type, string id, List<suberror> issues)
        {
            this.type = type;
            this.id = id;
            this.issues = issues;
        }
    }

    public class errorModel
    {
        /// <summary>
        /// this function is used to add an error to the constantly being 
        /// built error list and store it in the database as data is being processed through the web service
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="type"></param>
        /// <param name="reqkey"></param>
        /// <param name="error"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public static error AddError(JToken policy, string type, string reqkey, error error = null, string comment = null, JToken sub_data = null)
        {
            suberror err = new suberror();
            if (error == null) error = InstantiateError(policy, type, reqkey, sub_data);
            switch (type)
            {
                case "risk":
                    err = new suberror("risk", Import.GetRiskEDIIDFromJson(policy["risk"]["vehicles"], Import.GetChassisFromCoverJson(policy["cover"], error.id)), comment);
                    suberrorModel.StoreError(err, reqkey, error.id);
                    error.issues.Add(err);
                    break;
                case "policy":
                    err = new suberror("policy", Import.GetPolicyEDIIDFromJson(policy), comment);
                    suberrorModel.StoreError(err, reqkey, error.id);
                    error.issues.Add(err);
                    break;
                case "company":
                    err = new suberror("company", Import.GetCompanyEDIIDFromJson(sub_data), comment);
                    suberrorModel.StoreError(err, reqkey, error.id);
                    error.issues.Add(err);
                    break;
                case "driver":
                    err = new suberror("driver", Import.GetDriverEDIIDFromJson(sub_data), comment);
                    suberrorModel.StoreError(err, reqkey, error.id);
                    error.issues.Add(err);
                    break;
                case "person":
                    err = new suberror("person", Import.GetPersonEDIIDFromJson(sub_data), comment);
                    suberrorModel.StoreError(err, reqkey, error.id);
                    error.issues.Add(err);
                    break;
                case "covernote":
                    err = new suberror("cover", Import.GetCoverNoteEDIIDFromJson(policy), comment);
                    suberrorModel.StoreError(err, reqkey, error.id);
                    error.issues.Add(err);
                    break;
                case "certificate":
                    err = new suberror("certificate", Import.GetCertEDIIDFromJson(policy), comment);
                    suberrorModel.StoreError(err, reqkey, error.id);
                    error.issues.Add(err);
                    break;
                case "Insured":
                    err = new suberror("Insured", Import.GetInsuredTRNFromJson(policy), comment);
                    suberrorModel.StoreError(err, reqkey, Import.GetDriverTRNFromJson(policy));
                    error.issues.Add(err);
                    break;
                case "Company":
                    err = new suberror("Company", Import.GetInsuredTRNFromJson(policy), comment);
                    suberrorModel.StoreError(err, reqkey, Import.GetDriverTRNFromJson(policy));
                    error.issues.Add(err);
                    break;
                case "Vehicle":
                    err = new suberror("Vehicle", Import.GetVehicleChassisFromJson(policy), comment);
                    suberrorModel.StoreError(err, reqkey, Import.GetDriverTRNFromJson(policy));
                    error.issues.Add(err);
                    break;
                case "File":
                    err = new suberror("File", "error", comment);
                    suberrorModel.StoreError(err, reqkey);
                    error.issues.Add(err);
                    break;
                default:
                    break;
            }
            return error; //so we can repeat this process again

        }

        /// <summary>
        /// this function is used to begin our error model for later reporting
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="type"></param>
        /// <param name="reqkey"></param>
        /// <returns></returns>
        private static error InstantiateError(JToken policy, string type, string reqkey, JToken sub_data = null)
        {
            error error = new error();
            error = new error();

            if (type == "certificate" || type == "covernote") error.type = type;

            else if (type == "Insured" || type == "Company" || type == "Vehicle" || type == "File") 
               error.type = "removal_of_data";

            else error.type = Import.CoverOrCert(policy);

            switch (error.type)
            {
                case "covernote":
                    error.id = Import.GetCoverEDIIDFromJSON(policy["cover"].First["cover_note"]);
                    break;
                case "certificate":
                    error.id = Import.GetCoverEDIIDFromJSON(policy["cover"].First["certificate"]);
                    break;
                case "Removal of Data":
                    error.id = Import.elementNo.ToString();
                    break;
                default:
                    error.id = "";
                    break;
            }

            suberrorModel.StoreError(new suberror(error.type, error.id), reqkey);

            if (error.issues == null) error.issues = new List<suberror>();
            return error;
        }

        /// <summary>
        /// this is used to determine how many errors were incurred in a specified month's period for a specified company
        /// </summary>
        /// <param name="company"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static int ErrorCount(Company company, string month, string year)
        {
            int output = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC ErrorCount " + company.CompanyID + ",'" + Constants.CleanString(month) + "','" + Constants.CleanString(year) + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    output = int.Parse(reader["count"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return output;
        }

    }
}