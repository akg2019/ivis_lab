﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class KPI
    {
        public int id { get; set; }
        public string type { get; set; } //this is the Enquiry, Cover Note, Certificate, Error
        public string index_type { get; set; } //this is Year, Month, Day
        public int count { get; set; }
        public int covercount { get; set; }
        public int certcount { get; set; }
        public string mode { get; set; } //this is Green, Yellow, Red
        public Company company { get; set; } //this is the intermediary/company that the data is linked to

        public KPI() { }
        public KPI(int id)
        {
            this.id = id;
        }
        public KPI(string type, string mode, int count)
        {
            this.type = type;
            this.mode = mode;
            this.count = count;
        }
        public KPI(int id, string type, string mode, int count)
        {
            this.id = id;
            this.type = type;
            this.mode = mode;
            this.count = count;
        }
        public KPI(int id, string type, string mode, int count, string index_type)
        {
            this.id = id;
            this.type = type;
            this.mode = mode;
            this.count = count;
            this.index_type = index_type;
        }
        public KPI(Company company, int count, int covercount, int certcount)
        {
            this.company = company;
            this.count = count;
            this.covercount = covercount;
            this.certcount = certcount;
        }

        public KPI(Company company, int count)
        {
            this.company = company;
            this.count = count;
        }
    }
    public class KPIModel
    {
        /// <summary>
        /// this function retrieves all the kpi data as it relates to a single company
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public static List<KPI> GetKpis(Company company)
        {
            List<KPI> kpis = new List<KPI>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetKpis " + company.CompanyID, "read", "kpi", new KPI());
                while (reader.Read())
                {
                    kpis.Add(new KPI(int.Parse(reader["id"].ToString()), reader["type"].ToString(), reader["mode"].ToString(), int.Parse(reader["count"].ToString()), reader["datemode"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return kpis;
        }

        /// <summary>
        /// this function takes a list of kpis and returns only a the kpis
        /// that match a particular type
        /// </summary>
        /// <param name="kpis"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<KPI> KPIsByType(List<KPI> kpis, string type)
        {
            return kpis.Where(x => x.type == type).ToList();
        }

        /// <summary>
        /// this function takes a raw list of kpis and collects them by type
        /// currently the function limits it's data to just monthly limits
        /// </summary>
        /// <param name="kpis"></param>
        /// <returns></returns>
        public static List<List<KPI>> BunchKPIs(List<KPI> kpis)
        {
            List<List<KPI>> output = new List<List<KPI>>();
            foreach (string datemode in GetKPIDateModes())
            {
                if (datemode == "month") output.Add(kpis.Where(x => x.index_type == datemode).ToList());
            }
            return output;
        }

        /// <summary>
        /// this function retreives all the possible date modes for kpis
        /// i.e. Month, Year, Day, Week
        /// </summary>
        /// <returns></returns>
        private static List<string> GetKPIDateModes()
        {
            List<string> modes = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetKPIDateModes", "read", "kpi", new KPI());
                while (reader.Read())
                {
                    modes.Add(reader["mode"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return modes;
        }

        /// <summary>
        /// this function that creates all the initial records for a company in the system
        /// </summary>
        /// <param name="company"></param>
        /// <param name="type"></param>
        public static void InitializeKPI(Company company, string type)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC InitializeKpis " + company.CompanyID + ",'" + Constants.CleanString(type) + "'", "read", "kpi", new KPI());
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function updates a kpi with specific updated data
        /// </summary>
        /// <param name="kpi"></param>
        public static void UpdateKPI(KPI kpi)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC UpdateKPI " + kpi.id + "," + kpi.count, "update", "kpi", kpi);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function returns the kpis for a specific company and a type and datemode
        /// </summary>
        /// <param name="company">company object with id of company</param>
        /// <param name="type"></param>
        /// <param name="datemode"></param>
        /// <returns></returns>
        public static List<KPI> GetKPI(Company company, string type = null, string datemode = null)
        {
            List<KPI> kpis = new List<KPI>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                if (type == null) type = "";
                if (datemode == null) type = "";
                string query = "EXEC GetKPI " + company.CompanyID + ",'" + Constants.CleanString(type) + "','" + Constants.CleanString(datemode) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "kpi", new KPI());
                while (reader.Read())
                {
                    kpis.Add(new KPI(int.Parse(reader["id"].ToString()), reader["type"].ToString(), reader["mode"].ToString(), int.Parse(reader["count"].ToString()), reader["datemode"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return kpis;
        }

        /// <summary>
        /// this function retrieves the kpis (containing cover,covernote and certificate count) of a specific company 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="insurer">show insurers</param>
        /// <param name="import">include imported records</param>
        /// <returns></returns>
        public static List<KPI> get_intermediary_cover_count(DateTime start, DateTime end, string type = null, bool import = false)
        {
            if (type == null)
            {
                int cid = CompanyModel.GetMyCompany(Constants.user).CompanyID;
                if (CompanyModel.is_company_iaj(cid)) type = "i";
                else if (CompanyModel.IsCompanyBroker(cid)) type = "b";
                else if (CompanyModel.IsCompanyInsurer(cid)) type = "i";
            }

            List<KPI> kpis = new List<KPI>();
            sql sql = new sql();
            end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59);
            if (sql.ConnectSQL())
            {
                string query = "EXEC CoverCountIntermediaries " + CompanyModel.GetMyCompany(Constants.user).CompanyID + ",'"
                                                                + start.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                                + end.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                                + type + "','" 
                                                                + import + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    kpis.Add(new KPI(new Company(int.Parse(reader["CompanyID"].ToString()), reader["CompanyName"].ToString()), 
                                     int.Parse(reader["covercount"].ToString()),
                                     int.Parse(reader["covernotecount"].ToString()),
                                     int.Parse(reader["certcount"].ToString())));
                }
                sql.DisconnectSQL();
            }
            return kpis;
        }

        /// <summary>
        /// this function retrieves a specific kpi using kpi id
        /// </summary>
        /// <param name="kpi"></param>
        /// <returns></returns>
        public static KPI get_single_kpi(KPI kpi)
        {
            if (false)
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    string query = "EXEC get_single_kpi " + kpi.id;
                    SqlDataReader reader = sql.QuerySQL(query);
                    while (reader.Read())
                    {
                        kpi.id = int.Parse(reader["id"].ToString());
                        kpi.mode = reader["mode"].ToString();
                        kpi.count = int.Parse(reader["kpi"].ToString());
                        kpi.type = reader["type"].ToString();
                        kpi.index_type = reader["datemode"].ToString();
                    }
                    sql.DisconnectSQL();
                }
            }
            return kpi;
        }

        /// <summary>
        /// this function retrieves intermediaries and the number of enquiries they have made
        /// </summary>
        /// <param name="start"></param>
        ///  /// <param name="end"></param>
        /// <returns></returns>
        public static List<KPI> GetEnquiryCount(DateTime start, DateTime end, string type = null)
        {
            if (type == null)
            {
                int cid = CompanyModel.GetMyCompany(Constants.user).CompanyID;
                if (CompanyModel.is_company_iaj(cid)) type = "i";
                else if (CompanyModel.IsCompanyInsurer(cid)) type = "i";
                else if (CompanyModel.IsCompanyBroker(cid)) type = "b";
            }

            int records = 0;
            List<KPI> kpis = new List<KPI>();
            sql sql = new sql();
            end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59);
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCompanyEnquiries " + CompanyModel.GetMyCompany(Constants.user).CompanyID + ",'"
                                                                + start.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                                + end.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                                + type + "'";

                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    kpis.Add(new KPI(new Company(int.Parse(reader["CompanyID"].ToString()), reader["CompanyName"].ToString()),int.Parse(reader["count"].ToString())));
                    records++;
                }
                sql.DisconnectSQL();
            }
            return kpis;
        }
    }
}