﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;

namespace Honeysuckle.Models
{
    public class Company
    {
        public int CompanyID { get; set; }
        public int base_id { get; set; }
        public string EDIID { get; set; }
        [RegularExpression("([ ]*[0-9]+[ ]*)", ErrorMessage = "The IAJID must be an integer")]
        public string IAJID { get; set; }
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "Message")]
        [RegularExpression("([ ]*[0-9]+[ ]*)", ErrorMessage = "The Faulty Login Limit must be an integer")]
        public int floginlimit { get; set; }
        public bool autoCancelVehicle { get; set; }
        public string CompanyType { get; set; }
        public Address CompanyAddress { get; set; }
        public List<Email> emails { get; set; }
        public List<Phone> phoneNums { get; set; }
        public string insurercode { get; set; }
        public string compshort { get; set; }
        public string ID { get; set; }
        public int createdBy { get; set; }

        public bool imported { get; set; }

        [RegularExpression("(([ ]*[01][0-9]{12}[ ]*)|([ ]*[0][0-9]{8}[ ]*))", ErrorMessage = "The Company TRN must be 13 digits long when 'Trading As' applies, or begin with a 0 when 9 digits long")]
        public string trn { get; set; }
        public bool Enabled { get; set; }
        public int EmpCount { get; set; }

        public List<Intermediary> intermediaries { get; set; }

        public bool has_data { get; set; } //added for companies list and other index pages
        public Email primary_email { get; set; } //added for companies list (dashboard)
        public Phone primary_phone { get; set; } //added for companies list  (dashboard)
        

        public Company() { }
        public Company(int CompanyID, string CompanyName)
        {
            this.CompanyID = CompanyID;
            this.CompanyName = CompanyName;
        }
        public Company(string CompanyName, string trn)
        {
            this.CompanyName = CompanyName;
            this.trn = trn;
        }
        public Company(int CompanyID,string CompanyName, string trn,bool enabled)
        {
            this.CompanyID = CompanyID;
            this.CompanyName = CompanyName;
            this.trn = trn;
            this.Enabled = enabled;
        }
        public Company(int CompanyID, string CompanyName, string IAJID)
        {
            this.CompanyID = CompanyID;
            this.CompanyName = CompanyName;
            this.IAJID = IAJID;
        }
        public Company(int CompanyID, string CompanyName, string IAJID, string Type, bool Enabled)
        {
            this.CompanyID = CompanyID;
            this.CompanyName = CompanyName;
            this.IAJID = IAJID;
            this.CompanyType = Type;
            this.Enabled = Enabled;
        }
        public Company(int CompanyID)
        {
            this.CompanyID = CompanyID;
        }
        public Company(string CompanyName)
        {
            this.CompanyName = CompanyName;
        }
        public Company(int CompanyID, string IAJID, string CompanyName, string CompanyType, int floginlimit, Address CompanyAddress, string trn)
        {
            this.CompanyID = CompanyID;
            this.IAJID = IAJID;
            this.CompanyName = CompanyName;
            this.CompanyType = CompanyType;
            this.floginlimit = floginlimit;
            this.CompanyAddress = CompanyAddress;
            this.trn = trn;
        }
        public Company(int CompanyID, bool Enabled, string IAJID, string CompanyName, string CompanyType, int floginlimit, Address CompanyAddress, string trn)
        {
            this.CompanyID = CompanyID;
            this.Enabled = Enabled;
            this.IAJID = IAJID;
            this.CompanyName = CompanyName;
            this.CompanyType = CompanyType;
            this.floginlimit = floginlimit;
            this.CompanyAddress = CompanyAddress;
            this.trn = trn;
        }
        public Company(int CompanyID, string CompanyName, string IAJID, string TRN, string CompanyType)
        {
            this.CompanyID = CompanyID;
            this.CompanyName = CompanyName;
            this.IAJID = IAJID;
            this.trn = TRN;
            this.CompanyType = CompanyType;
        }

        public static List<int> toInt(List<Company> companies)
        {
            List<int> result = new List<int>();
            foreach (Company company in companies)
            {
                result.Add(company.CompanyID);
            }
            return result;
        }

    }

    public class CompanyModel
    {
        
        /// <summary>
        /// this function tests if there is any related data in the system to this company
        /// USED ON SITE MASTER
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        private static bool DoesCompanyHaveData(Company company)
        {
            bool result = false;
            sql sql = new sql();
            if (company.CompanyID != 0 && sql.ConnectSQL())
            {
                string query = "EXEC DoesCompanyHaveData " + company.CompanyID;
                //SqlDataReader reader = sql.QuerySQL(query, "read", "company", company);
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }
        
        /// <summary>
        /// this function retrieves the address of a company
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public static Address GetCompanyAddress(Company company)
        {
            Address address = new Address();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCompanyAddress " + company.CompanyID;
                //SqlDataReader reader = sql.QuerySQL(query, "read", "address", address);
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    Parish parish = new Parish();
                    if (DBNull.Value != reader["PID"]) parish = new Parish(int.Parse(reader["PID"].ToString()), reader["Parish"].ToString());

                    if ((bool)reader["LongAddressUsed"])
                    {

                        address = new Address();
                        address.ID = int.Parse(reader["AID"].ToString());
                        address.longAddressUsed = (bool)reader["LongAddressUsed"];
                        address.longAddress = reader["LongAddressUsed"].ToString();
                    }
                    else
                    {
                        address = new Address(int.Parse(reader["AID"].ToString()),
                                              reader["RoadNumber"].ToString(),
                                              reader["AptNumber"].ToString(),
                                              new Road(reader["RoadName"].ToString()),
                                              new RoadType(reader["RoadType"].ToString()),
                                              new ZipCode(reader["ZipCode"].ToString()),
                                              new City(reader["City"].ToString()),
                                              parish,
                                              new Country(reader["CountryName"].ToString()));
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return address;
        }
        
        /// <summary>
        /// this tests if the company short code is set for this company in question
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public static bool IsCompanyShortSet(Company company)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsCompanyShortSet " + company.CompanyID;
                //SqlDataReader reader = sql.QuerySQL(query, "read", "company", company);
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function returns a list of companies that are policy holders for a company in question
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public static List<Company> GetCompaniesIveInsuredBefore(Company company)
        {
            List<Company> companies = new List<Company>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCompaniesIveInsuredBefore " + company.CompanyID;
                //SqlDataReader reader = sql.QuerySQL(query, "read", "company", company);
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    companies.Add(new Company(int.Parse(reader["ID"].ToString()), reader["cname"].ToString(), reader["TRN"].ToString(),(bool) reader["Enabled"]));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return companies;
        }

        /// <summary>
        /// Retrieves the company insured code
        /// </summary>
        /// <param name="companyid">Company ID</param>
        /// <returns>boolean</returns>
        public static string GetInsurerCode(int companyid)
        {
            string output = "NO RESULT";
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetInsurerCode " + companyid;
                //SqlDataReader reader = sql.QuerySQL(query, "read", "company", new Company());
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    output = reader["icode"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            if (output == "" || output == null) output = "NO RESULT";
            return output;
        }

        /// <summary>
        /// Retrieves the company shortened code
        /// </summary>
        /// <param name="companyid">Company ID</param>
        /// <returns>boolean</returns>
        public static string GetShortenedCode(int companyid)
        {
            string output = "NO RESULT";
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetShortCode " + companyid;
                //SqlDataReader reader = sql.QuerySQL(query, "read", "company", new Company());
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    output = reader["CompShortening"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            if (output == "" || output == null) output = "NO RESULT";
            return output;
        }

        /// <summary>
        /// returns the enabled flag in the databse as it relates to company
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public static bool IsCompanyEnabled(Company company)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsCompanyEnabled " + company.CompanyID;
                //SqlDataReader reader = sql.QuerySQL(query, "read", "company", company);
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// updates a company record in the database to be enabled
        /// </summary>
        /// <param name="company"></param>
        public static void EnableCompany(Company company)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC EnableCompany " + company.CompanyID + "," + Constants.user.ID, "update", "company", company);
                reader.Close();
                sql.DisconnectSQL();
            }
        }
        
        /// <summary>
        /// updates a company record in the database to be disabled
        /// </summary>
        /// <param name="company"></param>
        public static void DisableCompany(Company company)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC DisableCompany " + company.CompanyID + "," + Constants.user.ID, "update", "company", company);
                reader.Close();
                sql.DisconnectSQL();
            }
        }
        
        /// <summary>
        /// This function searches for companies that match a certain search criteria
        /// </summary>
        /// <param name="search">Search criteria</param>
        /// <returns>A list of companies that match the search</returns>
        //public static List<Company> CompanyGenSearch(string search)
        //{
        //    List<Company> companies = new List<Company>();
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        string query = "EXEC CompanyGenSearch '" + Constants.CleanString(search) + "'";
        //        //SqlDataReader reader = sql.QuerySQL(query, "read", "company", new Company());
        //        SqlDataReader reader = sql.QuerySQL(query);
        //        while (reader.Read())
        //        {
        //            companies.Add(new Company(int.Parse(reader["ID"].ToString()), reader["name"].ToString(), reader["IAJID"].ToString(), reader["TRN"].ToString(), reader["Type"].ToString()));
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return companies;
        //}

        /// <summary>
        /// This function takes a company object with a valid ID in the system and returns the object with the correct data
        /// </summary>
        /// <param name="company">A Company object with a valid ID</param>
        /// <returns>A filled company object from the database</returns>
        public static Company GetCompanyName(Company company)
        {
            if (company.CompanyID != 0)
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    string query = "EXEC GetCompanyByID " + company.CompanyID;
                    //SqlDataReader reader = sql.QuerySQL(query, "read", "company", company);
                    SqlDataReader reader = sql.QuerySQL(query);
                    while (reader.Read())
                    {
                        company.CompanyName = reader["companyname"].ToString();
                        company.IAJID = reader["IAJID"].ToString();
                        company.CompanyType = reader["companytype"].ToString();
                        company.floginlimit = int.Parse(reader["floginlimit"].ToString());
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            return company;
        }

        /// <summary>
        /// This function retrieves the company admins of a particular company, given the user Id of a particular user in that company
        /// </summary>
        /// <param name="user">A user object- with valid user Id</param>
        /// <returns>A list of company admins</returns>
        public static List<User> GetMyCompanyAdmins(User user)
        {
            List<User> cadmins = new List<User>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetMyCompanyAdmins " + user.ID;
                //SqlDataReader reader = sql.QuerySQL(query, "read", "user", new User());
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    cadmins.Add(new User(int.Parse(reader["UID"].ToString())));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return cadmins;
        }

        /// <summary>
        /// This function returns all the emails of the users who are designated Company Administrators of a particular Company
        /// </summary>
        /// <param name="company">A company object with a valid company ID</param>
        /// <returns>A List of Emails</returns>
        public static List<Email> GetCompanyAdminEmails(Company company)
        {
            List<Email> emails = new List<Email>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCompanyAdminEmails " + company.CompanyID;
                //SqlDataReader reader = sql.QuerySQL(query, "read", "email", new Email());
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    emails.Add(new Email(reader["email"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return emails;
        }


        /// <summary>
        /// This function returns all the emails and ids of the users who are designated Company Administrators of a particular Company
        /// </summary>
        /// <param name="company">A company object with a valid company ID</param>
        /// <returns>A List of Users</returns>
        public static List<User> GetCompanyAdmin(Company company)
        {
            List<User> users = new List<User>();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetCompanyAdminEmails " + company.CompanyID, "read", "email", new Email());
                while (reader.Read())
                {
                    if (reader["UID"].ToString() != "")
                        users.Add(new User(int.Parse(reader["UID"].ToString()), reader["email"].ToString()));
                    //emails.Add(new Email(reader["email"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return users;
        }

        /// <summary>
        /// This function removes a company record from the database if that company has no employees attached to it
        /// </summary>
        /// <param name="id">a valid company id in the system</param>
        /// <returns>a boolean indicating whether the deletion was completed or not</returns>
        public static bool DeleteCompany(int id)
        {
            Company company = GetCompany(id);
            UserGroupModel.DeleteAllUserGroupsForCompany(new Company(id));
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                String query = "EXEC DeleteCompany " + id + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "company", company);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function adds a company to the database. This version is to be used by users who are logged into 
        /// the web site version of the application
        /// </summary>
        /// <param name="company">Company object with all related data</param>
        /// <param name="policyHolder">Set flag to true if the company being created is a policyholder rather than a company in the system with users and such</param>
        /// <returns>boolean stating whether the creation was successful</returns>
        public static bool AddCompany(Company company, bool policyHolder = false,bool importing = false)
        {
            return AddCompany(company, Constants.user.ID, policyHolder,importing);
        }

        /// <summary>
        /// This function adds a company to the database
        /// </summary>
        /// <param name="company">a valid company object with all relevant data</param>
        /// <param name="policyHolder">Set flag to true if the company being created is a policyholder rather than a company in the system with users and such</param>
        /// <returns>boolean indicating whether the company was created</returns>
        public static bool AddCompany(Company company, int uid, bool policyHolder = false,bool importing = false)
        {
            bool result = false;
            if(company.trn == "1000000000000") return result;
            if (company.CompanyAddress != null)
            {
                if (company.CompanyAddress.roadnumber != null || company.CompanyAddress.road != null || company.CompanyAddress.roadtype != null ||
                    company.CompanyAddress.city != null || company.CompanyAddress.country != null)
                {
                    company.CompanyAddress.ID = AddressModel.AddAddress(company.CompanyAddress, uid); // Adding address to the system based on relevant data
                }
                else if(company.CompanyAddress.longAddress != null && company.CompanyAddress.longAddress != "")
                    company.CompanyAddress.ID = AddressModel.AddAddress(company.CompanyAddress, uid);
            }
            else company.CompanyAddress = new Address(); //defaults data
            
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                //if (company.CertificateWording == null) company.CertificateWording = new TemplateWording(0); //certTemplate Is set to zero

                string query = "EXEC AddCompany '" + Constants.CleanString(company.IAJID) + "','"
                                                   + Constants.CleanString(company.EDIID) + "','"
                                                   + Constants.CleanString(company.CompanyName) + "',"
                                                   + company.floginlimit + ",'"
                                                   + Constants.CleanString(company.CompanyType) + "',"
                                                   + company.CompanyAddress.ID + ",'"
                                                   + Constants.CleanString(company.trn) + "',"
                                                   + company.Enabled + ","
                                                   + uid + ",'"
                                                   + Constants.CleanString(company.insurercode) + "','"
                                                   + Constants.CleanString(company.compshort)+"',"
                                                   + policyHolder + ","
                                                   + company.autoCancelVehicle;
                if (company.ID != null && company.ID != "") query += ",'" + Constants.CleanString(company.ID) + "'";

                SqlDataReader reader = sql.QuerySQL(query, "create", "company", company, uid);
                while (reader.Read())
                {
                    if ((bool)reader["result"])
                    {
                        result = (bool)reader["result"];
                        company.CompanyID = int.Parse(reader["ID"].ToString());
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }

            if (result && !policyHolder) UserGroupModel.CopyStockGroups(company.CompanyID); // copy comp admin role to this company
            if (result && importing) MarkCompanyImported(company);

            return result;
        }

        /// <summary>
        /// The function returns all companies stored in the database
        /// </summary>
        /// <returns>List of Companies stored in the database</returns>
        public static List<Company> GetAllCompanies()
        {
            List<Company> companies = new List<Company>();
            
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetAllCompanies";
                //SqlDataReader reader = sql.QuerySQL(query, "read", "company", new Company());
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    companies.Add(new Company(int.Parse(reader["CompanyID"].ToString()), 
                                              reader["CompanyName"].ToString(),                      
                                              reader["IAJID"].ToString(),
                                              reader["Type"].ToString(), 
                                              (bool)reader["enabled"]));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return companies;
        }

        /// <summary>
        /// This function returns the corresponding Company object to an ID on the system
        /// </summary>
        /// <param name="CompanyID">A valid company ID in the system</param>
        /// <returns>A valid company object</returns>
        public static Company GetCompany(int CompanyID, bool policyHolder = false)
        {
            Company result = new Company();
            
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCompanyByID " + CompanyID + "," + policyHolder;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    int flogin = 0;
                    Address address = new Address();
                    Parish parish = new Parish();

                    if (reader["AID"] != DBNull.Value) {
                        if (reader["AID"] != DBNull.Value)address.ID = int.Parse(reader["AID"].ToString());
                        if (reader["RoadNumber"] != DBNull.Value)address.roadnumber = reader["RoadNumber"].ToString();
                        if (reader["AptNumber"] != DBNull.Value)address.ApartmentNumber = reader["AptNumber"].ToString();
                        if (reader["RoadName"] != DBNull.Value)address.road = new Road(reader["RoadName"].ToString());
                        if (reader["RoadType"] != DBNull.Value)address.roadtype = new RoadType(reader["RoadType"].ToString());
                        if (reader["ZipCode"] != DBNull.Value)address.zipcode = new ZipCode(reader["ZipCode"].ToString());
                        if (reader["City"] != DBNull.Value)address.city = new City(reader["City"].ToString());
                        if (reader["CountryName"] != DBNull.Value)address.country = new Country(reader["CountryName"].ToString());
                        if (DBNull.Value != reader["LongAddress"]) address.longAddress = reader["LongAddress"].ToString();
                        if (DBNull.Value != reader["LongAddressUsed"]) address.longAddressUsed = Convert.ToBoolean(reader["LongAddressUsed"].ToString());
                        
                        
                        if (reader["PID"] != DBNull.Value) parish.ID = int.Parse(reader["PID"].ToString());
                        if (reader["Parish"] != DBNull.Value) parish.parish = reader["Parish"].ToString();
                        address.parish = parish;
                    }
                    
                    //test if keys are null
                    if (reader["floginlimit"] != DBNull.Value) flogin = int.Parse(reader["floginlimit"].ToString());


                    result = new Company(int.Parse(reader["ID"].ToString()), 
                                         (bool)reader["enabled"], 
                                         reader["IAJID"].ToString(), 
                                         reader["companyname"].ToString(), 
                                         reader["companytype"].ToString(), 
                                         flogin, 
                                         address, 
                                         reader["trn"].ToString());

                    result.insurercode = reader["icode"].ToString();
                    result.compshort = reader["short"].ToString();
                    result.base_id = int.Parse(reader["base_id"].ToString());
                    result.autoCancelVehicle = (bool)reader["acv"];
                    if (DBNull.Value != reader["ImportID"]) result.ID = reader["ImportID"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            
            return result;
        }

        /// <summary>
        /// This function updates all data in the database as it relates to a specified company
        /// </summary>
        /// <param name="company">A valid company object</param>
        public static void UpdateCompany(Company company)
        {
            company.CompanyAddress.ID = AddressModel.AddAddress(company.CompanyAddress);

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC UpdateCompany " + company.CompanyID + ","
                                                     + "'" + Constants.CleanString(company.IAJID) + "',"
                                                     + "'" + Constants.CleanString(company.CompanyName)+ "'"
                                                     + ",'" + Constants.CleanString(company.CompanyType) + "'"
                                                     + "," + company.floginlimit + "," 
                                                     +  company.CompanyAddress.ID
                                                     + ",'" + Constants.CleanString(company.trn)
                                                     + "'," + company.Enabled + ",'"
                                                     + Constants.CleanString(company.insurercode) + "','"
                                                     + Constants.CleanString(company.compshort) + "',"
                                                     + Constants.user.ID + ","
                                                     + company.autoCancelVehicle;

                SqlDataReader reader = sql.QuerySQL(query, "update", "company", company);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function returns a List of Types of Companies in the database
        /// </summary>
        /// <returns>List of strings</returns>
        public List<string> GetCompanyTypes()
        {
            List<string> result = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetAllCompanyTypes";
                //SqlDataReader reader = sql.QuerySQL(query, "read", "company", new Company());
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result.Add(reader["Type"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function returns a list of Company Names that exists in the database
        /// </summary>
        /// <returns>List of strings of Company Names</returns>
        public static List<string> GetCompanyNames()
        {
            List<string> result = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCompanyNames";
                //SqlDataReader reader = sql.QuerySQL(query, "read", "company", new Company());
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result.Add(reader["CompanyName"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function returns the company that a particular user is associated with
        /// </summary>
        /// <param name="user">A valid user in the system with a valid ID</param>
        /// <returns>A company object that contains an ID and Name</returns>
        public static Company GetMyCompany(User user)
        {
            Company company = new Company();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetMyCompany " + user.ID, "read", "company", company);
                while (reader.Read())
                {
                    company.CompanyID = int.Parse(reader["CompanyID"].ToString());
                    company.CompanyName = reader["CompanyName"].ToString();
                   // if (DBNull.Value != reader["Type"]) company.CompanyType = reader["Type"].ToString();
                    if (DBNull.Value != reader["TRN"]) company.trn = reader["TRN"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return company;
        }

        /// <summary>
        /// This function verifies as to wheter an IAJID is associated to an existing company in the system
        /// </summary>
        /// <param name="iajid">an integer</param>
        /// <returns>a boolean indicating whether the IAJID exists in the sysetm</returns>
        public static bool CheckCompanyByIAJID(string iajid, int id = 0)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC CheckCompanyByIAJID '" + Constants.CleanString(iajid) + "'";
                if (id != 0) query += "," + id;
                SqlDataReader reader = sql.QuerySQL(query, "read", "company", new Company());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function retrieves a company's information, given the IAJID of the company
        /// </summary>
        /// <param name="iajid">Company's IAJID</param>
        /// <returns>Company object</returns>
        public static Company GetCompanyByIAJID(string IAJID)
        {
            Company comp = new Company();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetCompanyByIAJID " + Constants.CleanString(IAJID), "read", "company", comp);
                while (reader.Read())
                {
                    string CompanyName; string CompanyType; string trn; int CompId;
                    int flogin = 0; int AID; string roadnumber = ""; string RoadName;
                    string ZipCode; string City; string CountryName; string RoadType;
                    string AptNumber = "";


                    CompId = int.Parse(reader["CompanyID"].ToString());
                    CompanyName = reader["companyname"].ToString();
                    CompanyType = reader["companytype"].ToString();
                    AID = int.Parse(reader["AID"].ToString());
                    roadnumber = reader["RoadNumber"].ToString();
                    AptNumber = reader["AptNumber"].ToString();
                    RoadName = reader["RoadName"].ToString();
                    RoadType = reader["RoadType"].ToString();
                    ZipCode = reader["ZipCode"].ToString();
                    City = reader["City"].ToString();
                    CountryName = reader["CountryName"].ToString();
                    trn = reader["trn"].ToString();
                   
                    roadnumber = reader["RoadNumber"].ToString();

                    comp = new Company(CompId, IAJID, CompanyName, CompanyType, flogin, new Address(AID, roadnumber, AptNumber, new Road(RoadName), new RoadType(RoadType), new ZipCode(ZipCode), new City(City), new Parish(reader["Parish"].ToString()), new Country(CountryName)), trn);
                   
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return comp;
        }

        /// <summary>
        /// This function returns the intermediaries associated with a particular company (insurance company)
        /// </summary>
        /// <param name="CompanyID">A company Id</param>
        /// <returns>A list of intermediary (companies) associated with the company in question</returns>
        public static List<Company> GetIntermediates(int CompanyID)
        {
            List<Company> intermediaries = new List<Company>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetCompanyIntermediaries " + CompanyID, "read", "company", new Company());
                while (reader.Read())
                {
                    intermediaries.Add(new Company(int.Parse(reader["ID"].ToString()), reader["CompanyName"].ToString(), reader["IAJID"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return intermediaries;
        }
        
        /// <summary>
        /// This function returns all the intermediary companies present in the system
        /// </summary>
        /// <returns>A list of intermediary (companies)</returns>
        public static List<Company> IntermediaryList()
        {
            List<Company> intermediaries = new List<Company>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetIntermediaries");
                while (reader.Read())
                {
                    intermediaries.Add(new Company(int.Parse(reader["ID"].ToString()), reader["CompanyName"].ToString(), reader["IAJID"].ToString(),reader["TRN"].ToString(), reader["type"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return intermediaries;
        }

        /// <summary>
        /// this function returns all the companies that can be viewed by any specific user.
        /// the list populates the dashboard partial Company Quick List
        /// </summary>
        /// <returns></returns>
        public static List<Company> MyCompanyList()
        {

            List<Company> companies = new List<Company>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
               SqlDataReader reader = sql.QuerySQL("EXEC MyCompanyList " + Constants.user.ID);
              
                while (reader.Read())
                {
                    Company c = new Company(int.Parse(reader["ID"].ToString()),
                                              reader["CompanyName"].ToString(),
                                              reader["IAJID"].ToString(),
                                              reader["type"].ToString(),
                                              (bool)reader["enabled"]);
                    c.CompanyAddress = new Address(reader["roadnum"].ToString(),
                                                   reader["road"].ToString(),
                                                   reader["rtype"].ToString(),
                                                   reader["zcode"].ToString(),
                                                   reader["city"].ToString(),
                                                   reader["parish"].ToString(),
                                                   reader["country"].ToString());
                    c.compshort = reader["short"].ToString();
                    c.EmpCount = int.Parse(reader["empcount"].ToString());
                    c.has_data = DoesCompanyHaveData(c);
                    c.primary_email = get_company_primary_contact_email(c.CompanyID);
                    c.primary_phone = get_company_primary_contact_phone(c.CompanyID);
                    companies.Add(c);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return companies;
        }

        /// <summary>
        /// This function returns all the insurance companies present in the system
        /// </summary>
        /// <returns>A list of intermediary (companies)</returns>
        public static List<Company> InsurersList()
        {
            List<Company> insurers = new List<Company>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetAllInsurers", "read", "company", new Company());
                while (reader.Read())
                {
                    insurers.Add(new Company(int.Parse(reader["ID"].ToString()), reader["CompanyName"].ToString(), reader["IAJID"].ToString(), reader["type"].ToString(), (bool)reader["enabled"]));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return insurers;
        }

        /// <summary>
        /// This function returns all the insurance companies that are associated with the current intermediary user
        /// </summary>
        /// <returns>A list of insurance (companies)</returns>
        public static List<Company> GetInsurers(bool notDisabled = false)
        {
            List<Company> companies = new List<Company>();

            sql sql = new sql();
            int compID;

            if (Constants.user.newRoleMode) compID = Constants.user.tempUserGroup.company.CompanyID;
            else compID = CompanyModel.GetMyCompany(Constants.user).CompanyID;

            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetMyInsurers " + compID + "," + notDisabled);
                while (reader.Read())
                {
                    int CompanyID; string IAJID; string CompanyName;
                    CompanyID = int.Parse(reader["CompanyID"].ToString());
                    IAJID = reader["IAJID"].ToString();
                    CompanyName = reader["CompanyName"].ToString();
                    companies.Add(new Company(CompanyID, CompanyName, IAJID));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return companies;
        }

        /// <summary>
        /// This function is used to test if a particular company is an insurance company
        /// </summary>
        /// <param name="CompanyId">Company Id of the company in question</param>
        /// <returns>A list of intermediary (companies)</returns>
        public static bool IsCompanyInsurer(int CompanyId)
        {
            bool isInsurer = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC IsCompanyInsurer " + CompanyId);
                while (reader.Read())
                {
                    isInsurer = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return isInsurer;
        }

        /// <summary>
        /// This function is used to test if a particular company is a broker
        /// </summary>
        /// <param name="CompanyId">Company Id of the company in question</param>
        /// <returns>boolean</returns>
        public static bool IsCompanyBroker(int CompanyId)
        {
            bool isBroker = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC IsCompanyBroker " + CompanyId);
                while (reader.Read())
                {
                    isBroker = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return isBroker;
        }

        /// <summary>
        /// this function tests if the company in question is the IAJ
        /// </summary>
        /// <param name="company_id"></param>
        /// <returns></returns>
        public static bool is_company_iaj(int company_id)
        {
            bool is_iaj = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC is_company_iaj " + company_id);
                while (reader.Read())
                {
                    is_iaj = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return is_iaj;
        }

        /// <summary>
        /// This function takes a vehicle Id, of a vehicle under a current policy, and finds the insurance company which has the veh under cover
        /// </summary>
        /// <param name="vehId">The vehicle Id</param>
        /// <returns>A filled company object from the database</returns>
        public static Company GetInsuranceCo(int vehId, DateTime start, DateTime end)
        {
            Company company = new Company ();

            sql sql = new sql();
            sql.ConnectSQL();

            string query = "EXEC GetVehicleInsurer " + vehId + ",'"
                                                     + start.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                     + end.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                
            SqlDataReader reader = sql.QuerySQL(query, "read", "company", new Company());
            while (reader.Read())
            {
                company.CompanyID = int.Parse(reader["CompanyID"].ToString());
                company.CompanyName = reader["companyname"].ToString();
                company.IAJID = reader["IAJID"].ToString();
                company.trn = reader["TRN"].ToString();
            }
            reader.Close();
            sql.DisconnectSQL();

            return company;
        }

        /// <summary>
        /// This function returns all the contact info associated to a insurer/broker/agent
        /// </summary>
        /// <param name="company">The company in question</param>
        /// <returns>List of emails</returns>
        public static Company GetCompanyContact(int CompanyID)
        {
            List<Email> emails = new List<Email>();
            List<Phone> Phones = new List<Phone>();
            Company comp = new Company(CompanyID);
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string Query = "EXEC GetCompanyContacts " + comp.CompanyID;
                SqlDataReader reader = sql.QuerySQL(Query, "read", "email", new Email());
                while (reader.Read())
                {
                    emails.Add(new Email(reader["email"].ToString()));
                    Phones.Add(new Phone(int.Parse(reader["PID"].ToString()), reader["PhoneNo"].ToString(), reader["Ext"].ToString(), (bool)reader["Primary"]));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            comp.emails = emails;
            comp.phoneNums = Phones;
            return comp;
        }

        /// <summary>
        /// this function returns a company with the specified trn
        /// </summary>
        /// <param name="TRN"></param>
        /// <returns></returns>
        public static Company GetCompanyByTRN(string TRN, int CompID = 0, bool insured= false)
        {
            Company company = new Company();
            company.trn = TRN;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetCompanyByTRN '" + Constants.CleanString(TRN) + "'," + CompID + "," + insured, "read", "company", new Company());
                while(reader.Read())
                {
                    company = new Company(int.Parse(reader["ID"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return company;
        }

        /// <summary>
        /// This function is used to test if a particular company is an insurance company/broker/agency
        /// </summary>
        /// <param name="CompanyId">Company Id of the company in question</param>
        /// <returns>A list of intermediary (companies)</returns>
        public static bool IsCompanyRegular(int CompanyId)
        {
            bool isRegular = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC IsCompanyRegular " + CompanyId, "read", "company", new Company());
                while (reader.Read())
                {
                    isRegular = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return isRegular;
        }

        /// <summary>
        /// This function tests whether this company has it's 'auto cancel' data flag set to true
        /// </summary>
        /// <param name="compId"></param>
        /// <returns></returns>
        public static bool CanAutoCancelVehiclePolicy(int compId) {

            bool canCancel = false;
            string query = "";
            sql sql = new sql();

            if (sql.ConnectSQL())
            {
                query = "EXEC DoAutoCancelVehicle " + compId;

                SqlDataReader reader = sql.QuerySQL(query, "read", "company", new Company());

                while (reader.Read())
                {
                    canCancel = (bool) reader["AutoCancelVehicle"];
                }

                reader.Close();
                sql.DisconnectSQL();
            }
            return canCancel;
        }

        /// <summary>
        /// this function gets the company's primary phone contact
        /// </summary>
        /// <param name="companyid"></param>
        /// <returns></returns>
        private static Phone get_company_primary_contact_phone(int companyid)
        {
            Phone phone = new Phone();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCompanyPrimaryPhoneContact " + companyid;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    if (DBNull.Value != reader["PhoneNo"]) phone.PhoneNumber = reader["PhoneNo"].ToString();
                    if (DBNull.Value != reader["Ext"]) phone.Extension = reader["Ext"].ToString();
                    if (DBNull.Value != reader["name"]) phone.name = reader["name"].ToString();
                }
                sql.DisconnectSQL();
            }
            return phone;
        }


        /// <summary>
        /// this function returns the company's primary email contact
        /// </summary>
        /// <param name="companyid"></param>
        /// <returns></returns>
        private static Email get_company_primary_contact_email(int companyid)
        {
            Email email = new Email();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetCompanyPrimaryEmailContact " + companyid;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    if (DBNull.Value != reader["email"]) email.email = reader["email"].ToString();
                }
                sql.DisconnectSQL();
            }
            return email;
        }

        /// <summary>
        /// this function retrieves the insurer of a policy
        /// </summary>
        /// <param name="policyNo"></param>
        /// <returns></returns>
        public static int GetCompanyFromPolicy(int policyID)
        {
            sql sql = new sql();
            int compID = 0;
            if (sql.ConnectSQL())
            {
                string query = "EXEC PolicyToCompany " + policyID;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    compID = int.Parse(reader["CompanyID"].ToString());
                }

                reader.Close();
                sql.DisconnectSQL();
            }

            return compID;
        }

        public static List<Company> GetCompanyCompanies(int compID, DateTime start, DateTime end, string typeOfDate = null)
        {
            List<Company> companies = new List<Company>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                if (typeOfDate == null) typeOfDate = "created";
                string query = "EXEC GetCompanyCompanies " + compID + ",'" + typeOfDate + "'";
                if ((start == DateTime.Parse("1969-12-31 00:00:00") || (start == DateTime.Parse("1969-12-31 19:00:00")) && (end == DateTime.Parse("1969-12-31 00:00:00") || end == DateTime.Parse("1969-12-31 19:00:00"))))
                {
                    query += ",'" + null + "','" + null + "'";
                }
                else
                    query += ",'" + start.ToString("yyyy-MM-dd HH:mm:ss") + "','" + end.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                
                SqlDataReader reader = sql.QuerySQL(query);
                Company comp = new Company();
                while (reader.Read())
                {

                    comp.CompanyID = int.Parse(reader["CompanyID"].ToString());
                    comp.trn = reader["TRN"].ToString() == "1000000000000" ? "Not Available" : reader["TRN"].ToString();
                    comp.Enabled = (bool)reader["Enabled"];
                    if (DBNull.Value != reader["IAJID"]) comp.IAJID = reader["IAJID"].ToString();
                    else comp.IAJID = "Not Available";

                    comp.CompanyName = reader["CompanyName"].ToString();
                    comp.CompanyType = "Regular";
                    comp.has_data = DoesCompanyHaveData(new Company(int.Parse(reader["CompanyID"].ToString())));
                    comp.imported = (bool)reader["imported"];
                    

                    companies.Add(comp);
                    comp = new Company();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return companies;
        }

        public static void MarkCompanyImported(Company comp)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC MarkCompanyImported " + comp.CompanyID;
                SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        public static bool IsUserAnEmployeeOfCompany(int uid, int compid)
        {
            sql sql = new sql();
            bool result = false;
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsUserEmployeeOfCompany " + uid + "," + compid;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool) reader["result"];
                }
            }

            return result;
        }

       
    }
}
