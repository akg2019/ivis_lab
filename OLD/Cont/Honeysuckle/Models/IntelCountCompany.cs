﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Honeysuckle.Models
{
    
    [DataContract]
    public class IntelCountCompany : IntelCount
    {
        [DataMember]
        public string company { get; set; }
        [DataMember]
        public int count { get; set; }

        public IntelCountCompany() { }

        public IntelCountCompany(IntelCount intel)
        {
            this.company = intel.company.CompanyName;
            this.count = intel.count;
        }

        /// <summary>
        /// this function converts a list of intelcount objects to a more friendly json structure
        /// </summary>
        /// <param name="intels"></param>
        /// <returns></returns>
        public static List<IntelCountCompany> Convert(List<IntelCount> intels)
        {
            List<IntelCountCompany> intelcomps = new List<IntelCountCompany>();
            foreach (IntelCount intel in intels)
            {
                intelcomps.Add(new IntelCountCompany(intel));
            }
            return intelcomps;
        }
    }
}