﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Honeysuckle.Models
{
    public class TemplateWording
    {
        public int ID { get; set; }
        public string CertificateType { get; set; }
        public string CertificateID { get; set; }
        public string CertificateInsuredCode { get; set; }
        public string ExtensionCode { get; set; }
        //public PolicyCover PolicyCover { get; set; }
        public List<PolicyCover> PolicyCovers { get; set; }
        //public Usage CoverUsage { get; set; }
        public bool ExcludedDriver { get; set; }
        public bool ExcludedInsured { get; set; }
        public bool MultipleVehicle { get; set; }
        public bool RegisteredOwner { get; set; }
        public bool RestrictedDriver { get; set; }
        public bool Scheme { get; set; }
        public string AuthorizedDrivers { get; set; }
        public string LimitsOfUse { get; set; }
        public string VehicleDesc { get; set; }
        public string VehRegNo { get; set; }
        public string PolicyHolders { get; set; }
        public bool imported { get; set; }
        //public string usage { get; set; }
        public string VehMake { get; set; }
        public string VehModel { get; set; }
        public string VehModelType { get; set; }
        public Company company { get; set; }

        public bool can_delete { get; set; } //this is for view level data

        public TemplateWording() { }
        public TemplateWording(int ID)
        {
            this.ID = ID;
        }
        public TemplateWording(int ID, string CertificateType, string CertificateID, string CertificateInsuredCode,
                               string ExtensionCode, bool ExcludedDriver, bool ExcludedInsured,
                               bool MultipleVehicle, bool RegisteredOwner, bool RestrictedDriver, bool Scheme, 
                               string AuthorizedDrivers, string LimitsOfUse, string VehicleDesc, string VehRegNo, 
                               string PolicyHolders, bool imported)
        {
            this.ID = ID;
            this.CertificateType = CertificateType;
            this.CertificateID = CertificateID;
            this.CertificateInsuredCode = CertificateInsuredCode;
            this.ExtensionCode = ExtensionCode;
            this.ExcludedDriver = ExcludedDriver;
            this.ExcludedInsured = ExcludedInsured;
            this.MultipleVehicle = MultipleVehicle;
            this.RegisteredOwner = RegisteredOwner;
            this.RestrictedDriver = RestrictedDriver;
            this.Scheme = Scheme;
            this.AuthorizedDrivers = AuthorizedDrivers;
            this.LimitsOfUse = LimitsOfUse;
            this.VehicleDesc = VehicleDesc;
            this.VehRegNo = VehRegNo;
            this.PolicyHolders = PolicyHolders;
            this.imported = imported;
        }

        public TemplateWording(int ID, string CertificateType, string CertificateID, string CertificateInsuredCode,
                               string ExtensionCode, bool ExcludedDriver, bool ExcludedInsured,
                               bool MultipleVehicle, bool RegisteredOwner, bool RestrictedDriver, bool Scheme, 
                               string AuthorizedDrivers, string LimitsOfUse, string VehicleDesc, string VehRegNo,
                               string PolicyHolders, bool imported, string make, string model, string modelType)
        {
            this.ID = ID;
            this.CertificateType = CertificateType;
            this.CertificateID = CertificateID;
            this.CertificateInsuredCode = CertificateInsuredCode;
            this.ExtensionCode = ExtensionCode;
            //this.PolicyCover = PolicyCover;
            //this.CoverUsage = CoverUsage;
            this.ExcludedDriver = ExcludedDriver;
            this.ExcludedInsured = ExcludedInsured;
            this.MultipleVehicle = MultipleVehicle;
            this.RegisteredOwner = RegisteredOwner;
            this.RestrictedDriver = RestrictedDriver;
            this.Scheme = Scheme;
            this.AuthorizedDrivers = AuthorizedDrivers;
            this.LimitsOfUse = LimitsOfUse;
            this.VehicleDesc = VehicleDesc;
            this.VehRegNo = VehRegNo;
            this.PolicyHolders = PolicyHolders;
            this.imported = imported;
            this.VehMake = make;
            this.VehModel = model;
            this.VehModelType = modelType;
            //this.usage = (usage);
        }
        public TemplateWording(int ID, string CertificateType)
        {
            this.ID = ID;
            this.CertificateType = CertificateType;
        }
        public TemplateWording(int ID, string CertificateType,string CertificateID)
        {
            this.ID = ID;
            this.CertificateType = CertificateType;
            this.CertificateID = CertificateID;
        }
    }
    public class TemplateWordingModel
    {
        /// <summary>
        /// this function returns all possible template wordings for any specific policy/vehicle 
        /// </summary>
        /// <param name="id">vehicle id</param>
        /// <param name="polId">policy id</param>
        /// <returns>list of possible template wordings</returns>
        public static List<TemplateWording> GetWordingBasedOnFilter(int id, int polId)
        {
            List<TemplateWording> wordings = new List<TemplateWording>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetWordingBasedOnFilter " + id + "," + polId + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    wordings.Add(new TemplateWording(int.Parse(reader["ID"].ToString()), reader["certtype"].ToString(), reader["certid"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();

               
                    
                    int i, j;
                    TemplateWording key = new TemplateWording();
                    for (i = 1; i < wordings.Count(); i++)
                    {
                        key = wordings[i];
                        j = i - 1;


                        while (j >= 0 && wordings[j].CertificateType.CompareTo(key.CertificateType) > 0)
                        {
                            wordings[j + 1] = wordings[j];
                            j = j - 1;
                        }
                        wordings[j + 1] = key;
                    }
            }
            return wordings;
        }
        
        
        /// <summary>
        /// This function returns all template wordings in the system
        /// </summary>
        public static List<TemplateWording> GetWordings()
        {
            List<TemplateWording> words = new List<TemplateWording>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetWordings " + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {

                    TemplateWording word = new TemplateWording(int.Parse(reader["ID"].ToString()),
                                                                reader["CertificateType"].ToString(),
                                                                reader["CertificateID"].ToString(),
                                                                reader["CertificateInsuredCode"].ToString(),
                                                                reader["ExtensionCode"].ToString(),
                                                                (bool)reader["ExcludedDriver"],
                                                                (bool)reader["ExcludedInsured"],
                                                                (bool)reader["MultipleVehicle"],
                                                                (bool)reader["RegisteredOwner"],
                                                                (bool)reader["RestrictedDriver"],
                                                                (bool)reader["Scheme"],
                                                                reader["AuthorizedDrivers"].ToString(),
                                                                reader["LimitsOfUse"].ToString(),
                                                                reader["VehicleDesc"].ToString(),
                                                                reader["VehRegNo"].ToString(),
                                                                reader["PolicyHolders"].ToString(),
                                                                (bool)reader["Imported"]);
                    word.can_delete = CanWordingBeDeleted(word);
                    words.Add(word);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return words;
        }

        /// <summary>
        /// This function adds a template wording to the system
        /// <returns>
        /// Tuple<bool,bool> the first return will represent whether the record was created in the database
        /// and the second return will represent that there were bad tags in the record to be created
        /// </returns>
        /// </summary>
        public static Tuple<bool,bool> AddWording(TemplateWording word, bool import)
        {
            bool result = false;
            
            //this tests for bad tags and updates those that are already fixed by previous
            Tuple <bool, TemplateWording> answer = manage_tags(word, CompanyModel.GetMyCompany(Constants.user).CompanyID);
            bool bad_tag_exists = answer.Item1;
            word = answer.Item2;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC AddWording " + Constants.user.ID + "," +
                                              "'" + Constants.CleanString(word.CertificateType) + "'," +
                                              "'" + Constants.CleanString(word.CertificateID) + "'," +
                                              "'" + Constants.CleanString(word.CertificateInsuredCode) + "'," +
                                              "'" + Constants.CleanString(word.ExtensionCode) + "'," +
                                              "'" + word.ExcludedDriver + "'," +
                                              "'" + word.ExcludedInsured + "'," +
                                              "'" + word.MultipleVehicle + "'," +
                                              "'" + word.RegisteredOwner + "'," +
                                              "'" + word.RestrictedDriver + "'," +
                                              "'" + word.Scheme + "'," +
                                              "'" + Constants.CleanString(word.AuthorizedDrivers) + "'," +
                                              "'" + Constants.CleanString(word.LimitsOfUse) + "'," +
                                              "'" + Constants.CleanString(word.VehicleDesc) + "'," +
                                              "'" + Constants.CleanString(word.VehRegNo) + "'," +
                                              "'" + Constants.CleanString(word.PolicyHolders) + "'," +
                                              "'" + import + "'";
                SqlDataReader reader = sql.QuerySQL(query, "create", "templatewording", word);
                while (reader.Read())
                {
                    result = (bool)reader["added"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return Tuple.Create<bool,bool>(result, bad_tag_exists);
        }

        /// <summary>
        /// this function adds many wordings and is used from the upload template page
        /// </summary>
        /// <param name="words"></param>
        /// <param name="responses"></param>
        /// <returns></returns>
        public static List<response> AddWordings(List<TemplateWording> words, List<response> responses)
        {
            foreach (TemplateWording word in words)
            {
                try
                {
                    Tuple<bool,bool> answer = AddWording(word, true);
                    if (!answer.Item1)
                    {
                        responses.Add(new response("error", "The wording '" + word.CertificateID + "' was not created because it already exists in the database."));
                    }
                    else responses.Add(new response("success", "The wording '" + word.CertificateID + "' was successfully created in the database."));

                    if (answer.Item2) responses.Add(new response("bad_tag", "There was a bad tag in the record(s) created. Please visit")); // message will be dealt with on JS level
                }
                catch
                {
                    responses.Add(new response("error", "The wording '" + word.CertificateID + "' failed to be created in the database due to an unexpected error."));
                }
            }
            return responses;
        }

        /// <summary>
        /// This function checks if a template exists
        /// </summary>
        /// <returns> boolean value </returns>
        public static bool WordingExist(int ID)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC WordingExist " + ID;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function deletes a template wording from the system
        /// </summary>
        /// <param name="word"> Template wording object</param>
        /// <returns> boolean value </returns>
        public static bool DeleteWording(TemplateWording word)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DeleteWording " + word.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "templatewording", word);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function tests as to whether we will allow you to delete a tempalte_wording
        /// a wording can only be deleted if there are no certificates/cover notes that are using it currently
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public static bool CanWordingBeDeleted(TemplateWording word)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC CanWordingBeDeleted " + word.ID;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function tests whether this wording is your data
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public static bool CanIViewWording(TemplateWording word)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC CanIViewWording " + word.ID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function edits template wording
        /// </summary>
        public static bool UpdateWording(TemplateWording word)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC UpdateWording " + word.ID + ","
                                                     + Constants.user.ID + "," +
                                                 "'" + Constants.CleanString(word.CertificateType) + "'," +
                                                 "'" + Constants.CleanString(word.CertificateID) + "'," +
                                                 "'" + Constants.CleanString(word.CertificateInsuredCode) + "'," +
                                                 "'" + Constants.CleanString(word.ExtensionCode) + "'," +
                                                 "'" + word.ExcludedDriver + "'," +
                                                 "'" + word.ExcludedInsured + "'," +
                                                 "'" + word.MultipleVehicle + "'," +
                                                 "'" + word.RegisteredOwner + "'," +
                                                 "'" + word.RestrictedDriver + "'," +
                                                 "'" + word.Scheme + "'," +
                                                 "'" + Constants.CleanString(word.AuthorizedDrivers) + "'," +
                                                 "'" + Constants.CleanString(word.LimitsOfUse) + "'," +
                                                 "'" + Constants.CleanString(word.VehicleDesc) + "'," +
                                                 "'" + Constants.CleanString(word.VehRegNo) + "'," +
                                                 "'" + Constants.CleanString(word.PolicyHolders) + "'," +
                                                 "'" + word.imported + "'";
                SqlDataReader reader = sql.QuerySQL(query, "update", "templatewording", word);
                reader.Close();
                sql.DisconnectSQL();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This function retrieves a template wording, given the wording id
        /// </summary>
        /// <param name="wordingID"> Template wording ID</param>
        /// <returns> Template wording object </returns>
        public static TemplateWording GetWording(int wordingID)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                TemplateWording word = new TemplateWording();
                string query = "EXEC GetWording " + wordingID;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    word = new TemplateWording(int.Parse(reader["ID"].ToString()),
                                                         reader["CertificateType"].ToString(),
                                                         reader["CertificateID"].ToString(),
                                                         reader["CertificateInsuredCode"].ToString(),
                                                         reader["ExtensionCode"].ToString(),
                                                         (bool)reader["ExcludedDriver"],
                                                         (bool)reader["ExcludedInsured"],
                                                         (bool)reader["MultipleVehicle"],
                                                         (bool)reader["RegisteredOwner"],
                                                         (bool)reader["RestrictedDriver"],
                                                         (bool)reader["Scheme"],
                                                         HttpUtility.HtmlDecode(reader["AuthorizedDrivers"].ToString()),
                                                         HttpUtility.HtmlDecode(reader["LimitsOfUse"].ToString()),
                                                         HttpUtility.HtmlDecode(reader["VehicleDesc"].ToString()),
                                                         HttpUtility.HtmlDecode(reader["VehRegNo"].ToString()),
                                                         HttpUtility.HtmlDecode(reader["PolicyHolders"].ToString()),
                                                         (bool)reader["imported"],
                                                         HttpUtility.HtmlDecode(reader["VehicleMake"].ToString()),
                                                         HttpUtility.HtmlDecode(reader["VehicleModel"].ToString()),
                                                         HttpUtility.HtmlDecode(reader["VehicleModelType"].ToString()));
                    word.company = new Company(int.Parse(reader["company_id"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
                return word;
            }
            else
            {
                return new TemplateWording(0); // SHOW THERE WAS NO RETURN
            }
        }

        /// <summary>
        /// This function is used to replace tags in a template
        /// </summary>
        public static string ReplaceCertificateTags(string word, VehicleCertificate vehicleCert)
        {
            /*
            <[Risk Items]Veh Year> 
            <[Risk Items]Veh Make> 
            <[Risk Items]Veh Model> 
            <[Risk Items]Veh Model Type> 
            <[Risk Items]Veh Extension>
            <[Risk Items]Veh Reg No>
            <[Policy Info]Main Insured> ( indicate all the insureds)
            <[Risk Items]Veh Main Driver Name> (indicate who is the main driver)
            <[Risk Items]Veh Driver Names> (List all the drivers)
            <[Risk Items]Veh Authorized Drivers> (Print Authrized Driver Wording)
            */
            string vehAuth = "";
            Regex vyr = new Regex("([<])([[])(Risk Items)([]])(Veh Year)([>])");
            Regex vmk = new Regex("([<])([[])(Risk Items)([]])(Veh Make)([>])");
            Regex vmd = new Regex("([<])([[])(Risk Items)([]])(Veh Model)([>])");
            Regex vehModelType = new Regex("([<])([[])(Risk Items)([]])(Veh Model Type)([>])");
            Regex vext = new Regex("([<])([[])(Risk Items)([]])(Veh Extension)([>])");
            Regex vrn = new Regex("([<])([[])(Risk Items)([]])(Veh Reg No)([>])");
            Regex ins = new Regex("([<])([[])(Policy Info)([]])(Main Insured)([>])");
            Regex driver = new Regex("([<])([[])(Risk Items)([]])(Veh Main Driver Name)([>])");
            Regex drivers = new Regex("([<])([[])(Risk Items)([]])(Veh Driver Names)([>])");
            Regex auth_drivers = new Regex("([<])([[])(Risk Items)([]])(Veh Authorized Drivers)([>])");
            //Regex drivers = new Regex("([<])([[])(Risk Items)([]])(Veh Authorized Drivers)([>])");

            //Put these tags for now (can be removed)-just for testing
            Regex usage = new Regex("([<])([[])(Policy Info)([]])(Usage)([>])");
            Regex limitOfUse = new Regex("([<])([[])(Policy Info)([]])(LimitsOfUse)([>])");
            Regex cover = new Regex("([<])([[])(Policy Info)([]])(Cover)([>])");

            string mainInsured = "";
            int count = 0;

            if (vyr.IsMatch(word))
            {
                if (vehicleCert.Risk.VehicleYear != 0)
                {
                    word = vyr.Replace(word, vehicleCert.Risk.VehicleYear.ToString());
                }
                else word = vyr.Replace(word, "");
            }
            if (vmk.IsMatch(word)) word = vmk.Replace(word, vehicleCert.Risk.make);
            if (vmd.IsMatch(word)) word = vmd.Replace(word, vehicleCert.Risk.VehicleModel);
            if (vehicleCert.Risk.modelType != null)
            if (vehModelType.IsMatch(word)) word = vehModelType.Replace(word, vehicleCert.Risk.modelType);
            if (vehicleCert.Risk.extension != null)
            if (vext.IsMatch(word)) word = vext.Replace(word, vehicleCert.Risk.extension);
            if (vehicleCert.Risk.VehicleRegNumber != null)
            if (vrn.IsMatch(word)) word = vrn.Replace(word, vehicleCert.Risk.VehicleRegNumber);


            if (vehModelType.IsMatch(word))
            {
                if (vehicleCert.Risk.modelType != null)
                    word = vehModelType.Replace(word, vehicleCert.Risk.modelType);
                else word = vehModelType.Replace(word, "");
            }

            if (vext.IsMatch(word))
            {
                if (vehicleCert.Risk.extension != null)
                    word = vext.Replace(word, vehicleCert.Risk.extension);
                else word = vext.Replace(word, "");
            }

            if (vrn.IsMatch(word))
            {
                if (vehicleCert.Risk.VehicleRegNumber != null)
                    word = vrn.Replace(word, vehicleCert.Risk.VehicleRegNumber);
                else word = vrn.Replace(word,"");
            }


            if (auth_drivers.IsMatch(word)) //Authorized drivers wording  drivers.IsMatch(word)
            {
                string authDrivers = "";

                if(vehicleCert.Risk.AuthorizedDrivers != null)
                   {
                      int AuthCount = vehicleCert.Risk.AuthorizedDrivers.Count;

                      foreach (Driver driv in vehicleCert.Risk.AuthorizedDrivers)
                        {
                            authDrivers = authDrivers + " " + driv.person.fname + " " + driv.person.lname;

                            AuthCount --;
                            if (AuthCount != 0)
                                authDrivers = authDrivers + ", ";
                        }
                    }
                 word = drivers.Replace(word, authDrivers);
                 vehAuth += authDrivers + Environment.NewLine + " ";
              }


            if (ins.IsMatch(word)) //Main insuredes wording
            {

                count = vehicleCert.policy.insured.Count + vehicleCert.policy.company.Count;

                if (vehicleCert.policy.insured != null)
                    foreach (Person per in vehicleCert.policy.insured)
                    {
                        mainInsured = mainInsured + " " + per.fname + " " + per.lname;
                        
                        count --;
                        if (count != 0)
                            mainInsured = mainInsured + ", ";
                    }

                if (vehicleCert.policy.company != null)
                    foreach (Company comp in vehicleCert.policy.company)
                    {
                        mainInsured = mainInsured + " " + comp.CompanyName + " ";

                        count--;
                        if (count != 0)
                            mainInsured = mainInsured + ", ";
                    }

                word = ins.Replace(word, mainInsured);

            }
           

            if (driver.IsMatch(word)) word = driver.Replace(word, "");
            if (drivers.IsMatch(word)) word = drivers.Replace(word, "");
            if (usage.IsMatch(word)) 
                word = usage.Replace(word, vehicleCert.Risk.usage.usage);
            if (cover.IsMatch(word)) word = cover.Replace(word, vehicleCert.policy.policyCover.cover);

            if (auth_drivers.IsMatch(word))
            {
                word = word.Replace(word, AuthorizedDriverWordingModel.get_wording(vehicleCert.policy, vehicleCert.Risk));
                vehAuth += AuthorizedDriverWordingModel.get_wording(vehicleCert.policy, vehicleCert.Risk);
            }
            if (vehAuth != "") word = vehAuth;

            return word;
        }

        /// <summary>
        /// This function is used to replace tags in a template
        /// </summary>
        public static string ReplaceCoverNoteTags(string word, VehicleCoverNote coverNote)
        {
            /*
            <[Risk Items]Veh Year> 
            <[Risk Items]Veh Make> 
            <[Risk Items]Veh Model> 
            <[Risk Items]Veh Model Type> 
            <[Risk Items]Veh Extension>
            <[Risk Items]Veh Reg No>
            <[Policy Info]Main Insured> ( indicate all the insureds)
            <[Risk Items]Veh Main Driver Name> (indicate who is the main driver)
            <[Risk Items]Veh Driver Names> (List all the drivers)
            <[Risk Items]Veh Authorized Drivers> (Print Authrized Driver Wording)
            */
            string vehDesc = "";
            string vehAuth = "";
            Regex vyr = new Regex("([<])([[])(Risk Items)([]])(Veh Year)([>])");
            Regex vmk = new Regex("([<])([[])(Risk Items)([]])(Veh Make)([>])");
            Regex vmd = new Regex("([<])([[])(Risk Items)([]])(Veh Model)([>])");
            Regex vehModelType = new Regex("([<])([[])(Risk Items)([]])(Veh Model Type)([>])");
            Regex vext = new Regex("([<])([[])(Risk Items)([]])(Veh Extension)([>])");
            Regex vrn = new Regex("([<])([[])(Risk Items)([]])(Veh Reg No)([>])");
            Regex ins = new Regex("([<])([[])(Policy Info)([]])(Main Insured)([>])");
            Regex driver = new Regex("([<])([[])(Risk Items)([]])(Veh Main Driver Name)([>])");
            Regex drivers = new Regex("([<])([[])(Risk Items)([]])(Veh Driver Names)([>])");
            Regex auth_drivers = new Regex("([<])([[])(Risk Items)([]])(Veh Authorized Drivers)([>])");

            //Put these tags for now (can be removed)-just for testing
            Regex usage = new Regex("([<])([[])(Policy Info)([]])(Usage)([>])");
            Regex limitOfUse = new Regex("([<])([[])(Policy Info)([]])(LimitsOfUse)([>])");
            Regex cover = new Regex("([<])([[])(Policy Info)([]])(Cover)([>])");

            int count = 0;
            string mainInsured = "";
            
            if (vyr.IsMatch(word))
            {
                if (coverNote.risk.VehicleYear != 0)
                {
                    word = vyr.Replace(word, coverNote.risk.VehicleYear.ToString());
                    vehDesc += coverNote.risk.VehicleYear + " ";
                }
                else word = vyr.Replace(word, "");
            }
            if (vmk.IsMatch(word)) { 
                word = vmk.Replace(word, coverNote.risk.make);
                vehDesc += coverNote.risk.make + " ";
            }
            if (vmd.IsMatch(word))
            {
                word = vmd.Replace(word, coverNote.risk.VehicleModel);
                vehDesc += coverNote.risk.VehicleModel + " ";
            }

            /*
             * if (vehModelType.IsMatch(word))
            {
                if (coverNote.risk.modelType != null)
                {
                    word = vehModelType.Replace(word, coverNote.risk.modelType);
                    vehDesc += coverNote.risk.modelType + " ";
                }
                else word = vehModelType.Replace(word, "");
            }
             * */
            if (vehModelType.IsMatch(word))
            {
                if (coverNote.risk.bodyType != null)
                {
                    word = vehModelType.Replace(word, coverNote.risk.bodyType);
                    vehDesc += coverNote.risk.bodyType + " ";
                }
                else word = vehModelType.Replace(word, "");
            }

            if (vext.IsMatch(word))
            {
                if (coverNote.risk.extension != null)
                    word = vext.Replace(word, coverNote.risk.extension);
                else word = vext.Replace(word, "");
            }

            if (vrn.IsMatch(word))
            {
                if (coverNote.risk.VehicleRegNumber != null)
                    word = vrn.Replace(word, coverNote.risk.VehicleRegNumber);
                else word = vrn.Replace(word, "");
            }

            if (auth_drivers.IsMatch(word)) //Authorized drivers wording auth_drivers.IsMatch(word)
            {
                string authDrivers = "";

                if (coverNote.risk.AuthorizedDrivers != null)
                {
                  int AuthCount = coverNote.risk.AuthorizedDrivers.Count;

                  foreach (Driver driv in coverNote.risk.AuthorizedDrivers)
                    {
                        authDrivers = authDrivers + " " + driv.person.fname + " " + driv.person.lname;

                        AuthCount--;
                        if (AuthCount != 0)
                            authDrivers = authDrivers + ", ";
                    }
                }
               word = drivers.Replace(word, authDrivers);
               vehAuth += authDrivers + Environment.NewLine + " ";
            }


            if (ins.IsMatch(word)) //Main insured wording
            {
                //Pullling the main insured on the policy (the policy holders) -this can be moved if necessary
                
                count = coverNote.Policy.insured.Count + coverNote.Policy.company.Count;


                if (coverNote.Policy.insured != null)
                    foreach (Person per in coverNote.Policy.insured)
                    {
                        mainInsured = mainInsured + " " + per.fname + " " + per.lname;
                        
                        count --;
                        if (count != 0)
                            mainInsured = mainInsured + ", ";
                    }

                if (coverNote.Policy.company != null)
                    foreach (Company comp in coverNote.Policy.company)
                    {
                        mainInsured = mainInsured + " " + comp.CompanyName + " ";

                        count--;
                        if (count != 0)
                            mainInsured = mainInsured + ", ";
                    }

                word = ins.Replace(word, mainInsured);
            }


            if (driver.IsMatch(word)) word = driver.Replace(word, "");
            if (drivers.IsMatch(word)) word = drivers.Replace(word, "");
            if (usage.IsMatch(word)) word = usage.Replace(word, coverNote.risk.usage.usage);
            if (cover.IsMatch(word)) word = cover.Replace(word, coverNote.Policy.policyCover.cover);
            if (auth_drivers.IsMatch(word))
            {
                word = word.Replace(word, AuthorizedDriverWordingModel.get_wording(coverNote.Policy, coverNote.risk));
                vehAuth += AuthorizedDriverWordingModel.get_wording(coverNote.Policy, coverNote.risk);
            }
            //if (auth_drivers.IsMatch(word)) word = word.Replace(word, AuthorizedDriverWordingModel.get_wording(coverNote.Policy, coverNote.risk));
            if (vehDesc != "") word = vehDesc;
            if (vehAuth != "") word = vehAuth;
            return word;
        }

        /// <summary>
        /// This function is used to replace tags in a template
        /// </summary>
        public static VehicleCoverNoteGenerated ReplaceCertificateTags(TemplateWording wording, VehicleCertificate certificate)
        {

            VehicleCoverNoteGenerated generatedCN = new VehicleCoverNoteGenerated();

            generatedCN.limitsofuse = ReplaceCertificateTags(wording.LimitsOfUse, certificate);
            generatedCN.vehExt = ReplaceCertificateTags(wording.ExtensionCode, certificate);
            generatedCN.authorizedwording = ReplaceCertificateTags(wording.AuthorizedDrivers, certificate);
            generatedCN.vehdesc = ReplaceCertificateTags(wording.VehicleDesc, certificate);
            generatedCN.vehregno = ReplaceCertificateTags(wording.VehRegNo, certificate);
            generatedCN.PolicyHolders = ReplaceCertificateTags(wording.PolicyHolders, certificate);

            if(certificate.Risk.usage != null)
            generatedCN.Usage = certificate.Risk.usage.usage;

            generatedCN.cover = certificate.policy.policyCover.cover;
            generatedCN.vehmake = ReplaceCertificateTags(wording.VehMake, certificate);
            generatedCN.vehmodel = ReplaceCertificateTags(wording.VehModel, certificate);
            generatedCN.vehmodeltype = ReplaceCertificateTags(wording.VehModelType, certificate);
            generatedCN.authorizedwording = ReplaceCertificateTags(wording.AuthorizedDrivers, certificate);

            generatedCN.PolicyNo = certificate.policy.policyNumber;

            generatedCN.PolicyHoldersAddress = certificate.policy.mailingAddress.roadnumber + " " + certificate.policy.mailingAddress.road.RoadName + " " + certificate.policy.mailingAddress.roadtype.RoadTypeName + ", " + certificate.policy.mailingAddress.city.CityName + ", "  + certificate.policy.mailingAddress.parish.parish + ", " + certificate.policy.mailingAddress.country.CountryName;
            
            generatedCN.certificateType = wording.CertificateType;
            generatedCN.certitificateNo = certificate.CertificateNo;
            generatedCN.certificateCode = wording.CertificateID;

            generatedCN.endorsementNo = certificate.EndorsementNo;
            generatedCN.bodyType = certificate.Risk.bodyType;

            if(certificate.Risk.VehicleYear != 0)
               generatedCN.vehyear = certificate.Risk.VehicleYear.ToString();
            else generatedCN.vehyear = "";

            generatedCN.seating = certificate.Risk.seating.ToString();
            generatedCN.chassisNo = certificate.Risk.chassisno.ToString();
            generatedCN.engineNo = certificate.Risk.engineNo;
            generatedCN.VIN = certificate.Risk.VIN;
            generatedCN.referenceNo = certificate.Risk.referenceNo;
            generatedCN.HPCC = certificate.Risk.HPCCUnitType;

            switch (CompanyModel.GetInsurerCode(certificate.company.CompanyID))
            {
                case "JIIC":
                case "GGJ":
                case "KEY":
                case "GA":
                case "AGI":
                case "NO RESULT":
                            generatedCN.effectivedate = certificate.policy.startDateTime.ToString("MMMM dd, yyyy");
                            generatedCN.expirydate = certificate.policy.endDateTime.ToString("MMMM dd, yyyy");
                            break;
                case "JNGI":
                case "ICWI":
                            generatedCN.effectivedate = certificate.policy.startDateTime.ToString("dd MMMM, yyyy");
                            generatedCN.expirydate = certificate.policy.endDateTime.ToString("dd MMMM, yyyy");
                            break;

                case "BCIC":
                            generatedCN.effectivedate = certificate.policy.startDateTime.ToString("yyyy, MMMM d,");
                            generatedCN.expirydate = certificate.policy.endDateTime.ToString("yyyy, MMMM d,");
                            break;

                default:
                            generatedCN.effectivedate = certificate.policy.startDateTime.ToString("MMMM dd, yyyy");
                            generatedCN.expirydate = certificate.policy.endDateTime.ToString("MMMM dd, yyyy");
                            break;
                
            }

            generatedCN.effectiveTime = certificate.policy.startDateTime.ToString("h:mm tt");
            generatedCN.expiryTime = certificate.policy.endDateTime.ToString("h:mm tt");

            return generatedCN;
        }

        /// <summary>
        /// This function is used to replace tags in a template
        /// </summary>
        public static VehicleCoverNoteGenerated ReplaceCoverNoteTags(TemplateWording wording, VehicleCoverNote coverNote)
        {

            VehicleCoverNoteGenerated generatedCN = new VehicleCoverNoteGenerated();

            generatedCN.limitsofuse = ReplaceCoverNoteTags(wording.LimitsOfUse, coverNote);
            generatedCN.vehExt = ReplaceCoverNoteTags(wording.ExtensionCode, coverNote);
            generatedCN.authorizedwording = ReplaceCoverNoteTags(wording.AuthorizedDrivers, coverNote);
            generatedCN.vehdesc = ReplaceCoverNoteTags(wording.VehicleDesc, coverNote);
            generatedCN.vehregno = ReplaceCoverNoteTags(wording.VehRegNo, coverNote);
            generatedCN.PolicyHolders = ReplaceCoverNoteTags(wording.PolicyHolders, coverNote);
            
            if (coverNote.risk.usage != null) generatedCN.Usage = coverNote.risk.usage.usage;
            if (coverNote.risk.estimatedValue != null)
            {
                float value = 0;
                if (coverNote.risk.estimatedValue != "") value = float.Parse(Regex.Match(coverNote.risk.estimatedValue, @"\d+").Value);
                generatedCN.estimatedValue = string.Format("{0:#,###0.##}", value); ;
            }

            generatedCN.cover = coverNote.Policy.policyCover.cover;
            generatedCN.vehmake = ReplaceCoverNoteTags(wording.VehMake, coverNote);
            generatedCN.vehmodel = ReplaceCoverNoteTags(wording.VehModel, coverNote);
            generatedCN.vehmodeltype = ReplaceCoverNoteTags(wording.VehModelType, coverNote);
            generatedCN.authorizedwording = ReplaceCoverNoteTags(wording.AuthorizedDrivers, coverNote);

            generatedCN.PolicyNo = coverNote.Policy.policyNumber;

            generatedCN.PolicyHoldersAddress = coverNote.Policy.mailingAddress.roadnumber + " " + coverNote.Policy.mailingAddress.road.RoadName + " " + coverNote.Policy.mailingAddress.roadtype.RoadTypeName + ", " + coverNote.Policy.mailingAddress.city.CityName + ", " + coverNote.Policy.mailingAddress.parish.parish + ", " + coverNote.Policy.mailingAddress.country.CountryName;
            
            if (coverNote.covernoteid != null) generatedCN.CovernoteNo = coverNote.covernoteid;
            else generatedCN.CovernoteNo = "";

            generatedCN.Period = coverNote.period.ToString();

            generatedCN.certificateType = wording.CertificateType;
            generatedCN.certificateCode = wording.CertificateID;

            generatedCN.endorsementNo = coverNote.endorsementno;
            generatedCN.bodyType = coverNote.risk.bodyType;

            if (coverNote.risk.VehicleYear != 0) generatedCN.vehyear = coverNote.risk.VehicleYear.ToString();
            else generatedCN.vehyear = "";

            generatedCN.seating = coverNote.risk.seating.ToString();
            generatedCN.chassisNo = coverNote.risk.chassisno.ToString();
            if(coverNote.Policy.compCreatedBy != null) generatedCN.companyCreatedBy = coverNote.Policy.compCreatedBy.CompanyName;
            generatedCN.referenceNo = coverNote.risk.referenceNo;
            generatedCN.engineNo = coverNote.risk.engineNo;
            generatedCN.HPCC = coverNote.risk.HPCCUnitType;
            if (coverNote.mortgagee != null) generatedCN.mortgagees = coverNote.mortgagee.mortgagee;


            switch (CompanyModel.GetInsurerCode(coverNote.company.CompanyID))
            {
                case "JIIC":
                case "KEY":
                case "GA":
                case "AGI":
                case "NO RESULT":
                            generatedCN.effectivedate = coverNote.effectivedate.ToString("MMMM dd, yyyy");
                            generatedCN.expirydate = coverNote.expirydate.ToString("MMMM dd, yyyy");
                            break;

                case "GGJ":
                            generatedCN.effectivedate = PDF.AddOrdinal(coverNote.effectivedate.Day) + " " + coverNote.effectivedate.ToString("MMMM yyyy");
                            generatedCN.expirydate = PDF.AddOrdinal(coverNote.expirydate.Day) + " " + coverNote.expirydate.ToString("MMMM yyyy");
                            break;

                case "JNGI":
                            generatedCN.effectivedate = coverNote.effectivedate.ToString("MMM dd, yyyy");
                            generatedCN.expirydate = coverNote.expirydate.ToString("MMM dd, yyyy");
                            break;

                case "BCIC":
                            generatedCN.effectivedate = coverNote.effectivedate.ToString("yyyy, MMMM d");
                            generatedCN.expirydate = coverNote.expirydate.ToString("yyyy, MMMM d");
                            break;

                case "ICWI":
                            generatedCN.effectivedate = coverNote.effectivedate.ToString("d MMMM, yyyy");
                            generatedCN.expirydate = coverNote.expirydate.ToString("d MMMM, yyyy");
                            break;

                default:
                            generatedCN.effectivedate = coverNote.effectivedate.ToString("MMMM dd, yyyy");
                            generatedCN.expirydate = coverNote.expirydate.ToString("MMMM dd, yyyy");
                            break;

            }

            generatedCN.effectiveTime = coverNote.effectivedate.ToString("h:mm tt");
            generatedCN.expiryTime = coverNote.expirydate.ToString("h:mm tt");
         

            return generatedCN;
        }

        /// <summary>
        /// this function adds cover/usages relationships to a wordings
        /// </summary>
        /// <param name="word_usage_covers"></param>
        /// <param name="responses"></param>
        /// <returns></returns>
        public static List<response> addWordingCoverUsages(List<word_usage_cover> word_usage_covers, List<response> responses)
        {
            foreach (word_usage_cover wuc in word_usage_covers)
            {
                if (test_if_wording_type_exists(wuc.cert_type))
                {
                    wuc.cover = PolicyCoverModel.addPolicyCover(wuc.cover);
                    wuc.usage = UsageModel.CreateUsage(wuc.usage);
                    PolicyCoverModel.AssociateUsageToCover(wuc.cover.ID, wuc.usage.ID);
                    if (addWordingCoverUsages(wuc.cert_type, wuc.cover.ID, wuc.usage.ID)) responses.Add(new response("success", "The usage/cover relationship '" + wuc.cover.cover + "/" + wuc.usage.usage + "' for the wording '" + wuc.cert_type + "' was created successfully."));
                    else responses.Add(new response("error", "The usage/cover relationship '" + wuc.cover.cover + "/" + wuc.usage.usage + "' for the wording '" + wuc.cert_type + "' failed to be created."));
                }
                else responses.Add(new response("error", "The wording '" + wuc.cert_type + "' does not exist in our database. Please create it before attempting to process this data."));
            }
            return responses;
        }

        /// <summary>
        /// this functiont tests if a wording type exists in the database for the user logged in
        /// </summary>
        /// <param name="certificate_type"></param>
        /// <returns></returns>
        private static bool test_if_wording_type_exists(string certificate_type)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC test_if_cert_type_exists '" + certificate_type + "'," + CompanyModel.GetMyCompany(Constants.user).CompanyID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "templatewording", new TemplateWording());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function creates a relationship between a cover/usage and a wording type
        /// </summary>
        /// <param name="type">cert type</param>
        /// <param name="coverId">cover id</param>
        /// <param name="usageID">usage id</param>
        /// <returns></returns>
        private static bool addWordingCoverUsages(string type, int coverId, int usageID)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                word_usage_cover word_usage_cover = new word_usage_cover(type, new Usage(usageID), new PolicyCover(coverId));
                string query = "EXEC addWordingCoverUsagesWithType '" + type + "'," +
                                                                        coverId + "," +
                                                                        usageID + "," +
                                                                        Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "create", "word_usage_cover", word_usage_cover);

                while (reader.Read())
                {
                    result = int.Parse(reader["count"].ToString()) > 0;
                    if (!result)
                    {
                        Log.LogRecord("wording", query);
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }
        
        /// <summary>
        /// This function adds a cover usage association to a wording
        /// </summary>
        /// <returns> boolean value </returns>
        public static bool addWordingCoverUsages(int ID, int coverId, int usageID)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL() && coverId != 0 && usageID !=0)
            {
                word_usage_cover word_usage_cover = new word_usage_cover(ID, new Usage(usageID), new PolicyCover(coverId));
                string query = "EXEC addWordingCoverUsages " + ID + "," 
                                                             + coverId + "," 
                                                             + usageID + "," 
                                                             + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "create", "word_usage_cover", word_usage_cover);

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function removes a cover usage association to a wording
        /// </summary>
        /// <returns> boolean value </returns>
        public static bool removeWordingCoverUsages(int ID, int coverId, int usageID)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                word_usage_cover word_usage_cover = new word_usage_cover(ID, new Usage(usageID), new PolicyCover(coverId));
                string query = "EXEC removeWordingCoverUsages " + ID + "," 
                                                                + coverId + "," 
                                                                + usageID + "," 
                                                                + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "word_usage_cover", word_usage_cover);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function returns the covers and usages associated to a wording
        /// </summary>
        /// <returns> Returns the policy covers and uages associated to a wording  </returns>
        public static TemplateWording getWordingAssociations(int ID)
        {
            TemplateWording wording = new TemplateWording();
            wording.PolicyCovers = new List <PolicyCover> ();
          
            PolicyCover polCov = new PolicyCover();
            Usage Usage = new Usage();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC getWordingAssociations " + ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "word_usage_cover", new word_usage_cover());
                while (reader.Read())
                {
                    polCov = new PolicyCover(int.Parse(reader["CoverID"].ToString()), reader["Cover"].ToString(), false,new List<Usage>() );
                    Usage = new Usage(int.Parse(reader["UsageID"].ToString()), reader["usage"].ToString());

                    polCov.usages.Add(Usage);
                    wording.PolicyCovers.Add(polCov);

                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return wording;
        }

        /// <summary>
        /// this function tests if a tag (wording tag) matches my pre-defined list
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public static bool does_tag_exist(string tag)
        {
            /*
            <[Risk Items]Veh Year> 
            <[Risk Items]Veh Make> 
            <[Risk Items]Veh Model> 
            <[Risk Items]Veh Model Type> 
            <[Risk Items]Veh Extension>
            <[Risk Items]Veh Reg No>
            <[Policy Info]Main Insured> ( indicate all the insureds)
            <[Risk Items]Veh Main Driver Name> (indicate who is the main driver)
            <[Risk Items]Veh Driver Names> (List all the drivers)
            <[Risk Items]Veh Authorized Drivers> (Print Authrized Driver Wording)
            */

            Regex risk_tags = new Regex("([<])([[])Risk Items([]])Veh (Year|Make|Model|Model Type|Extension|Reg No|Main Driver Name|Authorized Drivers)([>])");
            Regex policy_tags = new Regex("([<])([[])(Policy Info)([]])(Usage|LimitsOfUse|Cover|Main Insured)([>])");
            
            return (risk_tags.IsMatch(tag) || policy_tags.IsMatch(tag));
        }

        /// <summary>
        /// this function tests if the tag has been fixed already for the company in quetion
        /// and returns a tuple with the relevant data
        /// </summary>
        /// <param name="tag">the tag we're testing</param>
        /// <param name="company_id">the company's data we're looking through</param>
        /// <returns>Tuple<bool,string> the bool states if fixed or not and the string contains the fixed tag (if the bool is true)</returns>
        public static Tuple<bool, string> has_tag_been_fixed(string tag, int company_id)
        {
            bool result = false;
            string correct_tag = "";
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC has_tag_been_fixed '" + tag + "'," + company_id);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                    if (result) correct_tag = reader["correct_tag"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return Tuple.Create<bool, string>(result, correct_tag);
        }

        /// <summary>
        /// this function tests if tags have been fixed and modify to suit if needed
        /// </summary>
        /// <param name="wording"></param>
        /// <param name="company_id"></param>
        /// <returns></returns>
        private static Tuple<bool,TemplateWording> manage_tags(TemplateWording wording, int company_id)
        {
            bool result = false;
            //THIS WILL UPDATE THE INFORMATION WITH ANY ALREADY FIXED TAGS
            if (wording.VehRegNo != null) wording.VehRegNo = update_tags(wording.VehRegNo, company_id);
            if (wording.VehicleDesc != null) wording.VehicleDesc = update_tags(wording.VehicleDesc, company_id);
            if (wording.PolicyHolders != null) wording.PolicyHolders = update_tags(wording.PolicyHolders, company_id);
            if (wording.AuthorizedDrivers != null) wording.AuthorizedDrivers = update_tags(wording.AuthorizedDrivers, company_id);

            //THIS TESTS IF ANY BAD TAGS REMAIN AFTER UPDATING TAGS WITH FIXED DATA
            if (wording.VehRegNo != null) result = result || get_bad_tags(wording.VehRegNo, company_id);
            if (wording.VehicleDesc != null) result = result || get_bad_tags(wording.VehicleDesc, company_id);
            if (wording.PolicyHolders != null) result = result || get_bad_tags(wording.PolicyHolders, company_id);
            if (wording.AuthorizedDrivers != null) result = result || get_bad_tags(wording.AuthorizedDrivers, company_id);
            return Tuple.Create<bool, TemplateWording>(result, wording);
        }

        /// <summary>
        /// this function updates the tags if already fixed
        /// </summary>
        /// <param name="word"></param>
        /// <param name="company_id"></param>
        /// <returns></returns>
        private static string update_tags(string word, int company_id)
        {
            word = HttpUtility.HtmlDecode(word);
            Regex tag = new Regex("([<])([[])([a-zA-Z0-9 -])+([]])([a-zA-Z0-9 -])+([>])");
            foreach (Match m in tag.Matches(word))
            {
                if (!does_tag_exist(m.Value))
                {
                    Tuple<bool, string> answer = has_tag_been_fixed(m.Value, company_id);
                    if (answer.Item1) word.Replace(word, answer.Item2);
                }
            }
            return word;
        }

        /// <summary>
        /// this returns a tag if it's not fixed or logged in the database already
        /// </summary>
        /// <param name="wording"></param>
        /// <param name="company_id"></param>
        /// <returns></returns>
        private static bool get_bad_tags(string wording, int company_id)
        {
            bool bad_tag = false;
            Regex tag = new Regex("([<])([[])([a-zA-Z0-9 -])+([]])([a-zA-Z0-9 -])+([>])");
            foreach (Match m in tag.Matches(wording))
            {
                if (!does_tag_exist(m.Value)) store_bad_tag(m.Value, company_id); 
            }
            return bad_tag;
        }

        /// <summary>
        /// put bad tags in database
        /// </summary>
        /// <param name="wording"></param>
        /// <param name="company_id"></param>
        private static void store_bad_tag(string wording, int company_id)
        {
            MappingData map = new MappingData(0, "wordings", 0, wording, false);
            MappingDataModel.AddMappingData(map);
        }


    }
}