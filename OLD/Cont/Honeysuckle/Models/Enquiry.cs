﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class Enquiry
    {
        public int ID { get; set; }
        public string license { get; set; }
        public string chassis { get; set; }
        public DateTime requesttime { get; set; }
        public Vehicle risk { get; set; }
        public Policy policy { get; set; }
        public bool valid { get; set; }

        public Enquiry() { }
        public Enquiry(string license, string chassis)
        {
            this.license = license;
            this.chassis = chassis;
        }
        public Enquiry(int id, string license, string chassis, DateTime requesttime, Vehicle risk, Policy policy, bool valid)
        {
            this.ID = id;
            this.license = license;
            this.chassis = chassis;
            this.requesttime = requesttime;
            this.risk = risk;
            this.policy = policy;
            this.valid = valid;
        }
        public Enquiry(int id, string license, string chassis, DateTime requesttime, int risk, int policy, bool valid)
        {
            this.ID = id;
            this.license = license;
            this.chassis = chassis;
            this.requesttime = requesttime;
            this.risk = new Vehicle(risk);
            this.policy = new Policy(policy);
            this.valid = valid;
        }
    }
    public class EnquiryModel
    {
        /// <summary>
        /// this function logs an enquiry to the database made from the external access page
        /// </summary>
        /// <param name="enquiry"></param>
        /// <returns></returns>
        public static Enquiry LogEnquiry(Enquiry enquiry)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC LogEnquiry '" + Constants.CleanString(enquiry.license) + "','" + Constants.CleanString(enquiry.chassis) + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    enquiry.ID = int.Parse(reader["id"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return enquiry;
        }

        /// <summary>
        /// this function checks whether the enquiry had any relevant data in our database
        /// </summary>
        /// <param name="enquiry"></param>
        public static void WasEnquiryValid(Enquiry enquiry)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC WasEnquiryValid " + enquiry.ID);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function retrieves previously made enquiries
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Enquiry GetEnquiry(int id)
        {
            Enquiry enquiry = new Enquiry();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetEnquiry " + id, "read", "enquiry", new Enquiry());
                while (reader.Read())
                {
                    string license = null; string chassis = null; int risk = 0; int policy = 0;
                    if (!DBNull.Value.Equals(reader["license"])) license = reader["license"].ToString();
                    if (!DBNull.Value.Equals(reader["chassis"])) chassis = reader["chassis"].ToString();
                    if (!DBNull.Value.Equals(reader["risk"])) risk = int.Parse(reader["risk"].ToString());
                    if (!DBNull.Value.Equals(reader["policy"])) policy = int.Parse(reader["license"].ToString());

                    enquiry = new Enquiry(id, license, chassis, DateTime.Parse(reader["RequestedOn"].ToString()), risk, policy, (bool)reader["valid"]);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return new Enquiry();
        }

        /// <summary>
        /// this function returns all enquiries made to the server through a range of time
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static List<Enquiry> GetEnquiryRange(DateTime start, DateTime end)
        {
            List<Enquiry> enquiries = new List<Enquiry>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetEnquiryRange '" + start.ToString("yyyy-MM-dd") + "','" + end.ToString("yyyy-MM-dd") + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "enquiry", new Enquiry());
                while (reader.Read())
                {
                    string license = null; string chassis = null; int risk = 0; int policy = 0;
                    if (!DBNull.Value.Equals(reader["license"])) license = reader["license"].ToString();
                    if (!DBNull.Value.Equals(reader["chassis"])) chassis = reader["chassis"].ToString();
                    if (!DBNull.Value.Equals(reader["risk"])) risk = int.Parse(reader["risk"].ToString());
                    if (!DBNull.Value.Equals(reader["policy"])) policy = int.Parse(reader["license"].ToString());

                    enquiries.Add(new Enquiry(int.Parse(reader["id"].ToString()), license, chassis, DateTime.Parse(reader["RequestedOn"].ToString()), risk, policy, (bool)reader["valid"]));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return enquiries;
        }

    }
}