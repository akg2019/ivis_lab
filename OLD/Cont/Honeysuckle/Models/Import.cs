﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading;

namespace Honeysuckle.Models
{
    public class Import
    {
        public Import() { }
        public error error { get; set; }
        public static int elementNo { get; set; }
        public static int companyID { get; set; }
        public static List<string> missingTags { get; set; }
        /// <summary>
        /// this function kicks off the parsing of data on a new thread
        /// </summary>
        /// <param name="json"></param>
        /// <param name="web"></param>
        /// <param name="guid"></param>
        public void ParseJSONThreadHandler(JSON json, bool web, string guid = null)
        {
            Thread thread = new Thread(() => ParseJSON(json, web, guid));
            thread.Start();
        }

        public JSON ParseJSONThreadHandler(JSON json, int uid = 0, bool internalImport = false)
        {
            if (internalImport)
            {
                json = JSONModel.StoreJsonRecord(json.json, json.method, uid);
            }
            else
            {
                json = JSONModel.StoreJSON(json.json, json.method, uid);
            }
            string guid = null;
            if (uid != 0) guid = UserModel.GetSessionGUID(new User(uid));
            Thread thread = new Thread(() => ParseJSON(json, (json.method == "web"), guid, json.reqkey));
            thread.Start();
            return json;
        }

        /// <summary>
        /// this function kicks off the parsing of data on a new thread
        /// </summary>
        /// <param name="json"></param>
        /// <param name="web"></param>
        /// <param name="guid"></param>
        public JSON ParseJSONThreadHandler(List<JSON> jsons, int uid = 0,bool internalImport = false)
        {
            JSON js = new JSON();
            foreach (JSON json in jsons)
            {

                if (internalImport)
                {
                    js = JSONModel.StoreJsonRecord(json.json, json.method, uid);
                }
                else
                {
                    js = JSONModel.StoreJSON(json.json, json.method, uid);
                }

            }

            string guid = null;
            if (uid != 0) guid = UserModel.GetSessionGUID(new User(uid));
            Thread thread = new Thread(() => processEachJSON(jsons, guid));
            thread.Start();
            return js;
        }

        public void ParseJSONSInThread(List<JSON> jsons, int uid = 0, bool internalImport = false)
        {
            string guid = null;
            if (uid != 0) guid = UserModel.GetSessionGUID(new User(uid));
            Thread thread = new Thread(() => processEachJSON(jsons, guid));
            thread.Start();
        }

        public JSON StoreJSONS(JSON json, int uid = 0, bool internalImport = false)
        {
            JSON js = new JSON();
            

                if (internalImport)
                {
                    js = JSONModel.StoreJsonRecord(json.json, json.method, uid);
                }
                else
                {
                    js = JSONModel.StoreJSON(json.json, json.method, uid);
                }

            

            return js;
        }

        public void processEachJSON( List<JSON> jsons, string guid )
        {
            foreach (JSON json in jsons)
            {
                ParseJSON(json, (json.method == "web"), guid, json.reqkey);
            }
        }

        /// <summary>
        /// this function returns tie kid (key identifier) from a particular JSON set
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public int returnKid(string json)
        {
            JObject jobject = JObject.Parse(json);
            return int.Parse(jobject["kid"].ToString());
        }

        /// <summary>
        /// this function parses all data of the JSON and enters the data into the system
        /// </summary>
        /// <param name="json"></param>
        /// <param name="web"></param>
        /// <param name="guid"></param>
        /// <param name="reqkey"></param>
        public void ParseJSON(JSON json, bool web, string guid = null, string reqkey = null)
        {
            User user = new User();
            Company company = new Company();
            List<Vehicle> vehicles = new List<Vehicle>();
            List<VehicleCertificate> certificates = new List<VehicleCertificate>();
            List<VehicleCoverNote> covernotes = new List<VehicleCoverNote>();
            int recordsSuccess = 0, recordsFail = 0, completefail = 0; // x = 0;
            missingTags = new List<string>();

            int count = 0;

            if (guid != null) user = UserModel.getUserFromGUID(guid);
            else user = Constants.user;

            company = CompanyModel.GetMyCompany(user);
            Constants.parseerrors = new List<error>();

            try
            {
                JObject obj = JObject.Parse(json.json);
               
                //This check to see if there is a "removal_data" tag to see if the purpose of the data being sent is to remove record(s) instead of adding
                if (obj.Property("remove_data") != null && obj["remove_data"].Count() > 0)
                {
                    //ParseRemovalofData(obj, reqkey);  //Sends data to be parsed for removal
                    this.error = errorModel.AddError(obj["remove_data"], "policy", reqkey, null, "You cannot execute this action");
                    throw new Exception();
                }

                else
                {
                    //adding data from import
                    #region policy parsing
                    List<Policy> policies = new List<Policy>();
                    int records = obj["policy_data"].Count();
                    // obj["policy_data"].
                    //Added error
                    if (records <= 0) this.error = errorModel.AddError(obj["policy_data"], "policy", reqkey, null, "No data in policy");


                    foreach (var item in obj["policy_data"])
                    {
                        //x++;
                       // System.Diagnostics.Debug.WriteLine("Record: "+x);
                       //List<string> words = getTokenName(obj["policy_data"].Children().ToList());
                        count++;
                        try
                        {
                            if (item["policy"] != null)
                            {
                                ///GENERAL POLICY DATA
                                var policyindex = item["policy"];
                                Policy pol = ParsePolicyData(policyindex, item, user.ID, json.reqkey);
                                if (pol.policyNumber == "error")
                                {
                                    //if there is an error,add it to error model for a response to the sender later
                                    item.GetType();
                                    this.error = errorModel.AddError(item, "policy", reqkey, null, "there was an error in the policy_data");
                                    throw new Exception();
                                }
                                pol.insured = new List<Person>();
                                pol.company = new List<Company>();

                                ///INSURED DATA
                                Person personInsured = new Person();

                                if (policyindex["insured"] != null)
                                {
                                    if (policyindex["insured"]["company"] != null)
                                    {
                                        foreach (var comp in policyindex["insured"]["company"].Children())
                                        {
                                            pol.company.Add(ParseInsuredCompanyData(comp, item, user.ID, json.reqkey));
                                        }
                                    }
                                    else missingTags.Add("policy_data -> policy -> insured -> company");

                                    if (policyindex["insured"]["person"] != null)
                                    {
                                        foreach (var pers in policyindex["insured"]["person"].Children())
                                        {
                                            pol.insured.Add(ParseInsuredPersonData(pers, item, user.ID, json.reqkey));
                                        }
                                    }
                                    else missingTags.Add("policy_data -> policy -> insured -> person");
                                }
                                else
                                {
                                    missingTags.Add("policy_data -> policy -> insured");
                                    //break;
                                }

                                if (pol.company.Count() == 0 && pol.insured.Count() == 0)
                                {
                                    item.GetType();
                                    this.error = errorModel.AddError(item, "policy", reqkey, null, "there were no insureds on the policy");
                                    throw new Exception();
                                }
                                else if (pol.company.Count() == 0 && pol.insured.Count() == 1) pol.insuredType = "Individual";
                                else if (pol.company.Count() == 1 && pol.insured.Count() == 0) pol.insuredType = "Company";
                                else if (pol.company.Count() == 1 && pol.insured.Count() == 1) pol.insuredType = "Individual/Company";
                                else if ((pol.company.Count() > 1 && pol.insured.Count() > 0) || (pol.insured.Count() > 1 && pol.company.Count() > 0)) pol.insuredType = "Joint Insured";
                                else pol.insuredType = "Blanket";


                                ///Risk DATA
                                if (item["risk"] != null)
                                {
                                    var riskitem = item["risk"];

                                    List<Driver> drivers = new List<Driver>();
                                    Person person = new Person();
                                    List<Person> people = new List<Person>();

                                    //Added Error
                                    //if (riskitem["drivers"] == null) this.error = errorModel.AddError(item, "drivers", reqkey, null, "the drivers tag failed");

                                    if (riskitem["drivers"] == null) missingTags.Add("policy_data -> risk -> drivers");
                                    foreach (var driver in riskitem["drivers"].Children())
                                    {
                                        drivers.Add(ParseDriverData(driver, item, user.ID, json.reqkey));
                                    }

                                    Vehicle Risk = new Vehicle();
                                    pol.vehicles = new List<Vehicle>();

                                    //Added Error
                                    if (riskitem["vehicles"] == null) missingTags.Add("policy_data -> risk -> vehicles");

                                    foreach (var riskindex in riskitem["vehicles"].Children())
                                    {
                                        Risk = ParseVehicleData(riskindex, item, user.ID, json.reqkey, pol, riskitem["drivers"]);
                                        if (Risk != null) pol.vehicles.Add(Risk);
                                        else throw new Exception();
                                    }


                                }
                                else missingTags.Add("policy_data -> risk");

                                //COVER/CERTIFICATE DATA
                                covernotes = new List<VehicleCoverNote>();
                                certificates = new List<VehicleCertificate>();
                                if (item["cover"] != null)
                                {
                                    var coveritem = item["cover"];
                                    foreach (var cover in coveritem.Children())
                                    {
                                        if (cover["certificate"] != null)
                                        {
                                            VehicleCertificate vc = ParseCertificateData(item, cover["certificate"], pol, user.ID, json.reqkey);
                                            if (vc != null) certificates.Add(vc);
                                        }
                                        else missingTags.Add("policy_data -> cover -> certificate");

                                        if (cover["cover_note"] != null)
                                        {
                                            VehicleCoverNote vcn = ParseCoverNoteData(item, cover["cover_note"], user.ID, pol, json.reqkey);
                                            if (vcn != null) covernotes.Add(vcn);
                                        }
                                        else missingTags.Add("policy_data -> cover -> cover_note");
                                    }
                                }
                                else missingTags.Add("policy_data -> cover");


                                //create policy in system
                                pol.ID = PolicyModel.get_policy_by_edi(pol.EDIID);
                                if (pol.insuredby.CompanyID == 0) pol.insuredby.CompanyID = CompanyModel.GetCompanyByTRN(pol.insuredby.trn).CompanyID;
                                if (pol.compCreatedBy.CompanyID == 0) pol.compCreatedBy.CompanyID = CompanyModel.GetCompanyByTRN(pol.compCreatedBy.trn).CompanyID;
                                if (pol.ID == 0)
                                {
                                    if (pol.compCreatedBy.trn == pol.insuredby.trn)
                                    {
                                        if (!pol.no_policy_no && pol.policyNumber != null && (PolicyModel.PolicyNoExist(pol.policyNumber, pol.insuredby.CompanyID)))
                                        {
                                            pol.ID = PolicyModel.GetPolicyByNo(pol.policyNumber).ID;
                                            PolicyModel pm = new PolicyModel();
                                            pm.modifyPolicy(pol, user.ID);
                                        }
                                        else pol.ID = PolicyModel.addPolicy(pol, user.ID, true);
                                    }

                                    else if (PolicyModel.BrokerNoExist(pol.brokerPolicyNumber, pol.compCreatedBy.CompanyID) == 0)
                                    {
                                        pol.ID = PolicyModel.addPolicy(pol, user.ID, true);
                                    }
                                    else
                                    {
                                        //this.error = errorModel.AddError(item, "policy", reqkey, null, "the policy you are attempting to upload has a broker policy number already in use in IVIS");
                                        //throw new Exception();

                                        PolicyModel pm = new PolicyModel();
                                        pol.ID = PolicyModel.BrokerNoExist(pol.brokerPolicyNumber, pol.compCreatedBy.CompanyID); //insuredby
                                        pm.modifyPolicy(pol, user.ID);

                                    }
                                }
                                //else PolicyModel.UpdatePolicyData(pol, user.ID);
                                else
                                {
                                    try
                                    {
                                        PolicyModel pm = new PolicyModel();
                                        pm.modifyPolicy(pol, user.ID);
                                    }
                                    catch(Exception e)
                                    {
                                        this.error = errorModel.AddError(item, "policy", reqkey, null, "there was an error in updating this policy which already has the same EDIID in the system");
                                        throw new Exception();
                                    }

                                   /* if (pol.compCreatedBy.trn == pol.insuredby.trn)
                                    {
                                        PolicyModel pm = new PolicyModel();
                                        pm.modifyPolicy(pol, user.ID);
                                    }
                                    else if (PolicyModel.BrokerNoExist(pol.brokerPolicyNumber, pol.compCreatedBy.CompanyID, pol.ID) == 0) //insuredby
                                    {
                                        PolicyModel pm = new PolicyModel();
                                        pm.modifyPolicy(pol, user.ID);
                                    }
                                    else
                                    {
                                        this.error = errorModel.AddError(item, "policy", reqkey, null, "the policy you are attempting to upload has a broker policy number already in use in IVIS");
                                        throw new Exception();
                                    }*/
                                }

                                if (pol.ID == 0)
                                {
                                    this.error = errorModel.AddError(item, "policy", reqkey, null, "there was an error in the policy_data");
                                    throw new Exception("policy error");
                                }
                                //mark policy as imported
                                PolicyModel.MarkPolicyImported(pol);
                                //get all usages used on this policy
                                pol.policyCover.usages = PolicyModel.getAllUsagesinPolicy(pol);
                                // add all those usages to policy cover
                                PolicyCoverModel.UpdateUsagesInCover(pol.policyCover, user.ID);

                                bool cancelled = false;
                                foreach (Vehicle vehicle in pol.vehicles)
                                    if (VehicleModel.isVehUnderActivePolicy(vehicle.ID, pol.startDateTime, pol.endDateTime) && !VehicleModel.isVehUnderThisPolicy(pol.ID, vehicle.ID))
                                    {
                                        if (CompanyModel.CanAutoCancelVehiclePolicy(PolicyModel.GetPolicyCoveredUnder(vehicle.ID, pol.startDateTime, pol.endDateTime).insuredby.CompanyID))
                                        {
                                            PolicyModel.RiskDoubleInsuredEmail(vehicle.ID, pol, true, true);
                                            VehicleModel.CancelVehicleUnderPolicy(vehicle.ID, 0, pol.startDateTime, pol.endDateTime, user.ID);
                                            cancelled = true;
                                        }
                                        else
                                        {
                                            PolicyModel.RiskDoubleInsuredEmail(vehicle.ID, pol, false, true);
                                            this.error = errorModel.AddError(item, "risk", reqkey, this.error, "The risk is under a previous,active policy, during the time frame selected.");
                                            //throw new Exception("vehicle under previous policy");
                                        }
                                    }

                                //add insureds to policy
                                PolicyModel.addPolicyInsured(pol, user.ID, true, cancelled);


                                //add excluded,excepted,authorized drivers to policy
                                foreach (Vehicle risk in pol.vehicles)
                                {
                                    foreach (Driver auth in risk.AuthorizedDrivers)
                                    {
                                        VehicleModel.addDriversToVehicle(risk.chassisno, auth.person.PersonID, 1, pol.ID, user.ID);
                                    }
                                    foreach (Driver except in risk.ExceptedDrivers)
                                    {
                                        VehicleModel.addDriversToVehicle(risk.chassisno, except.person.PersonID, 2, pol.ID, user.ID);
                                    }
                                    foreach (Driver exclude in risk.ExcludedDrivers)
                                    {
                                        VehicleModel.addDriversToVehicle(risk.chassisno, exclude.person.PersonID, 3, pol.ID, user.ID);
                                    }
                                }

                                //add certificates
                                certificates = VehicleCertificateModel.CreateManyCertificates(certificates, user.ID, pol.insuredby.CompanyID);
                                //add cover notes
                                VehicleCoverNoteModel.AddManyCoverNotes(covernotes, user.ID, pol.ID, pol.insuredby.CompanyID, true);
                                foreach (VehicleCoverNote vcn in covernotes)
                                {
                                    if (vcn.ID == 0)
                                    {
                                        errorModel.AddError(item, "cover", reqkey, null, "failed to create cover note");
                                        //throw new Exception();
                                    }
                                    else VehicleCoverNoteModel.MarkCoverNoteImported(vcn);

                                }
                                foreach (VehicleCertificate vc in certificates)
                                {
                                    if (vc.VehicleCertificateID == 0)
                                    {
                                        errorModel.AddError(item, "cover", reqkey, null, "failed to create certificate");
                                        //throw new Exception();
                                    }
                                    else VehicleCertificateModel.MarkCertificateImported(vc);
                                }

                                //clear cover notes
                                covernotes = new List<VehicleCoverNote>();
                                //clear certs
                                certificates = new List<VehicleCertificate>();


                                if (this.error != null) Constants.parseerrors.Add(this.error);
                                this.error = null;

                                JSON js = new JSON();
                                js.reqkey = reqkey;
                                js.count = count;
                                JSONModel.StoreCount(js);
                                recordsSuccess++;
                            }
                            else missingTags.Add("policy_data -> policy");
                        }
                        catch
                        {
                            //fail in some big way

                            if (this.error != null) Constants.parseerrors.Add(this.error);
                            else Constants.parseerrors.Add(errorModel.AddError(item, "data", reqkey, null, "an error occured while sending your data. Please check your tags to ensure valid data is being sent."));

                            this.error = null;

                            JSON js = new JSON();
                            js.reqkey = reqkey;
                            js.count = count;
                            JSONModel.StoreCount(js);
                            recordsFail++;
                        }
                    }

                    #endregion

                }
            }
            catch (Exception)
            {
                //fail in some bigger way
                if (this.error != null) Constants.parseerrors.Add(this.error);
                this.error = null;
                completefail++;
            }

            if (reqkey != null)
            {
                JSON js = new JSON();
                js.reqkey = reqkey;
                js.count = count;
                js.processedjson = JsonConvert.SerializeObject(Constants.parseerrors);
                JSONModel.StoreResponse(js);

               // Notification notification = new Notification();
                Mail mail = new Mail();
                List<User> users = new List<User>();
                //notification.heading = "Policy Upload Complete";
                //notification.content = "Your last policy upload is complete";
                try
                {
                    Constants.user = UserModel.getUserFromGUID(guid);
                }
                catch
                { 
                }
                //notification.NoteFrom = UserModel.GetUser("system");
                users.Add(user);
                //notification.NoteTo = users;
                //NotificationModel.CreateNotification(notification);
                
                if (EmailModel.GetPrimaryEmail(user) != null && EmailModel.GetPrimaryEmail(user).email != "")
                {
                    mail.to = new List<string>();
                    mail.to.Add(EmailModel.GetPrimaryEmail(user).email);
                    mail.subject = "Policy Upload Complete";
                    mail.body = "Your last policy upload is complete.\r\nRecords Successful:" + recordsSuccess + " \r\nRecords with Issues:" + recordsFail + "\r\nSee result JSON:\r\n" + Environment.NewLine + js.processedjson;
                    MailModel.SendMailWithThisFunction(users, mail);
                }
            }
        }

        #region DataRemoval
        /*
        /// <summary>
        /// this function parses all data of the JSON and removes the data from the system
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="reqkey"></param>
        private void ParseRemovalofData(JObject obj,string reqkey)
        {
            //incremented number used as an id for display purposes for sender
            elementNo = 0; 
            
            string trnOrchassisNo;
            string policyNo;
            string policyEdiid;
            bool remove = false;
            bool dataRemoved = false;

            try
            {

                foreach (var item in obj["remove_data"])
                {
                    //check to see that access is allowed to this record by insurer
                    if (!canRemoveData(item))
                    {
                        this.error = errorModel.AddError(item, "File", reqkey, null, "You do not have access to this record!");
                        if (this.error != null) Constants.parseerrors.Add(this.error);
                        this.error = null;
                    }
                    else
                    {
                        elementNo++;
                        switch (item["type"].ToString())
                        {
                            //parse insured person data
                            case "insured":
                                remove = bool.Parse(item["remove"].ToString());
                                if (remove)
                                {
                                    trnOrchassisNo = item["trn"].ToString();
                                    policyNo = item["policy_number"].ToString();
                                    policyEdiid = item["policy_ediid"].ToString();

                                    dataRemoved = DeleteFromPolicy(trnOrchassisNo, policyNo, policyEdiid, 1);

                                    if (!dataRemoved)
                                        this.error = errorModel.AddError(item, "Insured", reqkey, null, "This person is not linked to the stated policy or the record has already been deleted. Please check your data.");
                                }
                                else
                                    this.error = errorModel.AddError(item, "Insured", reqkey, null, "You did not request to remove this person. Please check your data.");

                                if (this.error != null) Constants.parseerrors.Add(this.error);
                                this.error = null;

                                break;
                            //parse insured company data
                            case "company":
                                remove = bool.Parse(item["remove"].ToString());
                                if (remove)
                                {
                                    trnOrchassisNo = item["trn"].ToString();
                                    policyNo = item["policy_number"].ToString();
                                    policyEdiid = item["policy_ediid"].ToString();

                                    dataRemoved = DeleteFromPolicy(trnOrchassisNo, policyNo, policyEdiid, 2);

                                    if (!dataRemoved)
                                        this.error = errorModel.AddError(item, "Company", reqkey, null, "This company is not linked to the stated policy or the record has already been deleted. Please check your data.");
                                }

                                else
                                    this.error = errorModel.AddError(item, "Company", reqkey, null, "You did not request to remove this company. Please check your data.");

                                if (this.error != null) Constants.parseerrors.Add(this.error);
                                this.error = null;

                                break;
                            //parse insured vehicle data
                            case "vehicle":
                                remove = bool.Parse(item["remove"].ToString());
                                if (remove)
                                {
                                    trnOrchassisNo = item["chassis_number"].ToString();
                                    policyNo = item["policy_number"].ToString();
                                    policyEdiid = item["policy_ediid"].ToString();

                                    dataRemoved = DeleteFromPolicy(trnOrchassisNo, policyNo, policyEdiid, 3);
                                    if (!dataRemoved)
                                        this.error = errorModel.AddError(item, "Vehicle", reqkey, null, "This vehicle is not linked to the stated policy or the record has already been deleted. Please check your data.");
                                }

                                else
                                    this.error = errorModel.AddError(item, "Vehicle", reqkey, null, "You did not request to remove this vehicle. Please check your data.");

                                if (this.error != null) Constants.parseerrors.Add(this.error);
                                this.error = null;
                                break;
                            default:
                                this.error = errorModel.AddError(item, "File", reqkey, null, "There was an error in operation. Please check your data.");
                                break;
                        }
                    }
                }
            }

            catch (Exception)
            {
             JToken token = null;
             this.error = errorModel.AddError(token, "File", reqkey, null, "Parsing error! There may be errors in the file. Please check your data.");
             if (this.error != null) Constants.parseerrors.Add(this.error);
             this.error = null;               
            }
           


            if (reqkey != null)
            {
                JSON js = new JSON();
                js.reqkey = reqkey;
                js.processedjson = JsonConvert.SerializeObject(Constants.parseerrors);
                JSONModel.StoreResponse(js);
            }
            
        }

        /// <summary>
        /// this function checks if a insurer or intermediary created a policy and hence has permission to remove
        /// </summary>
        /// <param name="item"></param>
        private bool canRemoveData(JToken item)
        {
            sql sql = new sql();
            string query = "";
            int insurerID=0;
            int brokerID=0;

            if (sql.ConnectSQL())
            {
                query = "EXEC PolicyToCompany '" + item["policy_number"].ToString() + "','" + item["policy_ediid"].ToString() + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "policy", new Policy());
                while (reader.Read())
                {
                    insurerID = (int)reader["CompanyID"];
                    brokerID = (int)reader["CompanyCreatedBy"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }

            return (companyID == insurerID || companyID == brokerID);
        }

        /// <summary>
        /// this function deletes company or person or removes vehicle from a policy
        /// </summary>
        /// <param name="trnOrChassisNo"></param>
        /// <param name="policyNo"></param>
        /// <param name="policyEdiid"></param>
        /// <param name="type"></param>
        private bool DeleteFromPolicy(string trnOrChassisNo, string policyNo, string policyEdiid, int type)
        {
            sql sql = new sql();
            string query = "";
            bool rowsAffected = false;

            switch (type)
            {
                case 1:
                    if (sql.ConnectSQL()) {
                        query = "EXEC DeleteInsuredfromPolicy '" + trnOrChassisNo + "','"
                                                                 + policyNo + "','"
                                                                 + policyEdiid + "'";
                        SqlDataReader reader = sql.QuerySQL(query, "delete", "person", PersonModel.GetPersonByTRN(trnOrChassisNo));
                        while (reader.Read()) {
                            rowsAffected = (bool)reader["rowsAffected"];
                        }
                        reader.Close();
                        sql.DisconnectSQL();
                    }
                    break;
                case 2:
                     if (sql.ConnectSQL()) {
                         query = "EXEC DeleteCompanyfromPolicy '" + trnOrChassisNo + "','"
                                                                 + policyNo + "','"
                                                                 + policyEdiid + "'";
                        SqlDataReader reader = sql.QuerySQL(query, "delete", "company", CompanyModel.GetCompanyByTRN(trnOrChassisNo));
                        while (reader.Read()) {
                            rowsAffected = (bool)reader["rowsAffected"];
                        }
                        reader.Close();
                        sql.DisconnectSQL();
                    }
                    break;
                case 3:
                    if (sql.ConnectSQL()) {
                        query = "EXEC CancelVehicleUnderPolicy '" + trnOrChassisNo + "','"
                                                                 + policyNo + "','"
                                                                 + policyEdiid + "',"
                                                                 + Constants.user.ID;

                        SqlDataReader reader = sql.QuerySQL(query, "delete", "vehicle", VehicleModel.GetVehicleByChassis(trnOrChassisNo));
                        while (reader.Read()) {
                            rowsAffected = (bool)reader["rowsAffected"];
                        }
                        reader.Close();
                        sql.DisconnectSQL();
                    }
                    break;
                default:
                    break;
            }
            return rowsAffected;
        }
         * */
        #endregion

        /// <summary>
        /// this function return cover notes from web service search
        /// </summary>
        /// <param name="js"></param>
        /// <returns></returns>
        public List<VehicleCoverNoteJson> JsonToCoverNotes(JSON js)
        {
            JObject obj = JObject.Parse(js.json);
            List<VehicleCoverNote> covers = new List<VehicleCoverNote>();
            if (obj["cover"].ToString() == "covernote" || obj["cover"].ToString() == "both") covers = VehicleCoverNoteModel.CoverSearchCoverNotes(DateTime.Parse(obj["start_date"].ToString()), DateTime.Parse(obj["end_date"].ToString()), Constants.user);
            return VehicleCoverNoteJsonModel.ParseFromCovers(covers);
        }

        /// <summary>
        /// this function retrieves relevant certificates from web service search
        /// </summary>
        /// <param name="js"></param>
        /// <returns></returns>
        public List<VehicleCertificateJson> JsonToCerts(JSON js)
        {
            JObject obj = JObject.Parse(js.json);
            List<VehicleCertificate> certs = new List<VehicleCertificate>();
            if (obj["cover"].ToString() == "certificate" || obj["cover"].ToString() == "both") certs = VehicleCertificateModel.CoverSearchCerts(DateTime.Parse(obj["start_date"].ToString()), DateTime.Parse(obj["end_date"].ToString()), Constants.user);
            return VehicleCertificateJsonModel.ParseCertsToJson(certs);
        }

        /// <summary>
        /// this function parses json data to authenticate users over the web service
        /// </summary>
        /// <param name="js"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public string ParseJSONAuth(JSON js, string type)
        {
            JObject obj = JObject.Parse(js.json);
            string output = null;
            bool success = false;
            
            try
            {
                
                if (obj.Property("username") != null && obj.Property("password") != null)
                {
                    user_login_object ulo = UserModel.LoginUser(new User(obj["username"].ToString(), obj["password"].ToString()));
                    if (ulo.session_free && ulo.valid_user)
                    {
                        //User u = new User(obj["username"].ToString());
                        User u = UserModel.GetUser(obj["username"].ToString());
                        UserModel.ResetFaultyLogins(u.username);
                        u.sessionguid = UserModel.GetSessionGUID(u);
                        if ((UserModel.TestUserPermissions(u, new UserPermission("Import", "Import")) && type == "import") || (UserModel.TestUserPermissions(new User(obj["username"].ToString()), new UserPermission("Import", "CoverSearch")) && type == "coversearch"))
                        {
                            output = "loggedin";
                            Constants.user = new User();
                            Constants.user = UserModel.GetUser(obj["username"].ToString()); //storing user for import
                            Constants.user.sessionguid = UserModel.GetSessionGUID(Constants.user);
                            User user = UserModel.GetUser(Constants.user.ID);
                            companyID = user.employee.company.CompanyID;
                            success = true;
                        } //end if
                        else
                        {
                            UserModel.Logout(u.username);
                            output = "not permitted";
                        }
                    } // end if
                    else output = "bad login";
                } //end if
                if (obj.Property("key") != null && obj.Property("kid") != null)
                {
                    if (UserModel.TestWebGUID(RSA.Decrypt(obj["key"].ToString(), "session", int.Parse(obj["kid"].ToString()))))
                    {
                        Constants.user = new User();
                        Constants.user = UserModel.getUserFromGUID(RSA.Decrypt(obj["key"].ToString(), "session", int.Parse(obj["kid"].ToString())));

                        //test if json is logging out the user
                        if (obj.Property("logout") != null)
                        {
                            if ((bool)obj["logout"])
                            {
                                UserModel.Logout();
                                success = true;
                                output = "loggedout";
                            } //end if
                        } //end if
                        //else just validating for data entry
                        else if (obj.Property("reqkey") != null && obj.Property("kid") != null)
                        {
                            js.reqkey = RSA.Decrypt(obj["reqkey"].ToString(), "request", int.Parse(obj["kid"].ToString()));
                            js = JSONModel.ReturnResponse(js);
                            if (js.inprogress) output = "inprogress";
                            else
                            {
                                if (js.fail) output = "fail";
                                else if (js.completed) output = "completed";
                                else output = "bad key";
                            } //end else
                        } //end if
                        
                        // COVER SEARCH MODULE
                        else if (obj.Property("start_date") != null && obj.Property("end_date") != null && obj.Property("cover") != null)
                        {
                            output = "covers";
                        } //end if
                        else output = "valid";
                        
                        success = true;
                        
                    } //end if
                    else output = "bad login";
                } //end if
                if (output == null) output = "no key/username/pass";
            } //end try
            catch
            {
                output = "failed";
            }
            
            if (!success) JSONModel.JsonFailed(js);
            Constants.JsonResponse = js;
            return output;
        }

        /// <summary>
        /// this function takes a JToken and parses out the Polic Data from it and places it into a policy object
        /// The function is to be used only with the importation of data. Upon failure it will store the ID of the 
        /// line of JSON that it failed to capture for later return
        /// </summary>
        /// <param name="policyindex"></param>
        /// <returns></returns>
        private Policy ParsePolicyData(JToken policyindex, JToken policy_data, int uid, string reqkey = null)
        {
            string comment = null;
            Policy pol = new Policy();
            try
            {
                comment = "The edi_system_id failed";
                pol.EDIID = policyindex["data"]["edi_system_id"].ToString();
                comment = "The start_date_time failed";
                pol.startDateTime = DateTime.Parse(policyindex["data"]["start_date_time"].ToString());
                comment = "The end_date_time failed";
                pol.endDateTime = DateTime.Parse(policyindex["data"]["end_date_time"].ToString());

                comment = "The created_on failed. The system will use the current date as the created_on date";
                try
                {
                    pol.CreatedOn = DateTime.Parse(policyindex["data"]["created_on"].ToString());
                    if(policyindex["data"]["created_on"].ToString() == "" || policyindex["data"]["created_on"] == null) throw new Exception();
                }
                catch
                {
                    this.error = errorModel.AddError(policy_data, "policy", reqkey, this.error, comment); 
                }

                comment = "The empty_policy_number flag failed";
                pol.no_policy_no = (bool)policyindex["data"]["empty_policy_number"];
                if (!pol.no_policy_no)
                {
                    comment = "The policy_number failed";
                    if (policyindex["data"]["policy_number"].ToString() == "") throw new Exception();
                    pol.policyNumber = policyindex["data"]["policy_number"].ToString();
                }
                comment = "The broker_policy_number failed";
                pol.brokerPolicyNumber = policyindex["data"]["broker_policy_number"].ToString();

                comment = "The policy_prefix, policy_cover or is_commercial_business failed";
                pol.policyCover = new PolicyCover(0, policyindex["data"]["policy_prefix"].ToString(), policyindex["data"]["policy_cover"].ToString(), (bool)policyindex["data"]["is_commercial_business"]);

                if (policyindex["data"]["mailing_address"] == null && policyindex["data"]["billing_address"] == null)
                {
                    comment = "no billing or mailing address";
                    throw new Exception();
                }
                else
                {
                    bool mail = false; bool bill = false;
                    string country = "";
                    if (policyindex["data"]["mailing_address"] != null)
                    {

                        comment = "the mailing_address failed";
                        mail = true;
                        if (policyindex["data"]["mailing_address"]["Address_Line_1"] != null && policyindex["data"]["mailing_address"]["Address_Line_2"] != null && policyindex["data"]["mailing_address"]["Address_Line_3"] != null)
                        {
                            pol.mailingAddress = new Address(policyindex["data"]["mailing_address"]["Address_Line_1"].ToString() + Environment.NewLine + policyindex["data"]["mailing_address"]["Address_Line_2"].ToString() + Environment.NewLine + policyindex["data"]["mailing_address"]["Address_Line_3"].ToString(), true);
                        }
                        else
                        {

                            if (policyindex["data"]["mailing_address"]["country"].ToString() == "") country = "Jamaica";
                            else country = policyindex["data"]["mailing_address"]["country"].ToString();
                            pol.mailingAddress = new Address(policyindex["data"]["mailing_address"]["number"].ToString(),
                                                    policyindex["data"]["mailing_address"]["road"].ToString(),
                                                    policyindex["data"]["mailing_address"]["type"].ToString(),
                                                    null,
                                                    policyindex["data"]["mailing_address"]["city"].ToString(),
                                                    policyindex["data"]["mailing_address"]["parish"].ToString(),
                                                    country);
                        }
                    }
                    else missingTags.Add("policy_data -> policy -> data -> mailing_address");

                    if (policyindex["data"]["billing_address"] != null)
                    {
                        comment = "the billing_address failed";
                        bill = true;
                        if (policyindex["data"]["billing_address"]["Address_Line_1"] != null && policyindex["data"]["billing_address"]["Address_Line_2"] != null && policyindex["data"]["billing_address"]["Address_Line_3"] != null)
                        {
                            pol.billingAddress = new Address(policyindex["data"]["billing_address"]["Address_Line_1"].ToString() + Environment.NewLine + policyindex["data"]["billing_address"]["Address_Line_2"].ToString() + Environment.NewLine + policyindex["data"]["billing_address"]["Address_Line_3"].ToString(),true);
                        }
                        else
                        {

                            if (policyindex["data"]["billing_address"]["country"].ToString() == "") country = "Jamaica";
                            else country = policyindex["data"]["billing_address"]["country"].ToString();
                            pol.billingAddress = new Address(policyindex["data"]["billing_address"]["number"].ToString(),
                                                    policyindex["data"]["billing_address"]["road"].ToString(),
                                                    policyindex["data"]["billing_address"]["type"].ToString(),
                                                    null,
                                                    policyindex["data"]["billing_address"]["city"].ToString(),
                                                    policyindex["data"]["billing_address"]["parish"].ToString(),
                                                    country);
                        }
                    }
                    else missingTags.Add("policy_data -> policy -> data -> billing_address");

                    if (pol.mailingAddress == null && pol.billingAddress == null)
                    {
                        comment = "no billing or mailing address tested";
                        throw new Exception();
                    }
                    else
                    {
                        if (pol.mailingAddress == null) pol.mailingAddress = pol.billingAddress;
                        if (pol.billingAddress == null) pol.billingAddress = pol.mailingAddress;
                    }
                }

                #region optional data
                if (policyindex["data"]["policy_type"] != null && policyindex["data"]["policy_type"].ToString() != "")
                {
                    comment = "The policy_type failed";
                    pol.insuredType = policyindex["data"]["policy_type"].ToString();
                }
                #endregion
                
                pol.insuredby = new Company();
                pol.compCreatedBy = new Company();
                bool my_own_broker = false;
               
                comment = "The insurer_trn failed";
                if (policyindex["data"]["insurer_trn"].ToString() == "") 
                { 
                    comment = "the insurer_trn is blank"; 
                    throw new Exception(); 
                }
                if (!(CompanyModel.IsCompanyInsurer(CompanyModel.GetCompanyByTRN(policyindex["data"]["insurer_trn"].ToString()).CompanyID)))
                { 
                    comment = "The insurer_trn is not linked to an insurer. Please check your data."; 
                    throw new Exception(); 
                }
                comment = "The broker_trn failed";
                if (policyindex["data"]["insurer_trn"].ToString() == policyindex["data"]["broker_trn"].ToString() && (CompanyModel.IsCompanyInsurer(CompanyModel.GetCompanyByTRN(policyindex["data"]["insurer_trn"].ToString()).CompanyID)))
                {
                    my_own_broker = true;
                }
                else if (!(IntermediaryModel.IsBrokerIntermediary(CompanyModel.GetCompanyByTRN(policyindex["data"]["insurer_trn"].ToString()), CompanyModel.GetCompanyByTRN(policyindex["data"]["broker_trn"].ToString()))))
                {
                    comment = "The insurer_trn has not linked you as an intermediary currently. This policy will not be processed.";
                    throw new Exception();
                }
               
                
                if (policyindex["data"]["broker_trn"].ToString() == "")
                {
                    comment = "the broker_trn is blank";
                    throw new Exception();
                }
                else
                {
                
                    if (!my_own_broker) //not valid if I'm my own broker
                    {
                        pol.compCreatedBy = CompanyModel.GetCompanyByTRN(policyindex["data"]["broker_trn"].ToString());
                        comment = "The broker_trn is not linked to any company. Please check your data.";
                        if (pol.compCreatedBy == null) throw new Exception();
                    }
               }

                pol.insuredby.trn = policyindex["data"]["insurer_trn"].ToString();
                pol.compCreatedBy.trn = policyindex["data"]["broker_trn"].ToString();
            }
            catch(Exception){
                this.error = errorModel.AddError(policy_data, "policy", reqkey, this.error, comment);
                pol = null;
            }
            return pol;
        }

        /// <summary>
        /// This function parses the data required for the policy holders that are people
        /// </summary>
        /// <param name="insured"></param>
        /// <returns></returns>
        private Person ParseInsuredPersonData(JToken insured, JToken policy_data, int uid, string reqkey = null)
        {
            string comment = null;
            Person personInsured = new Person();
            try
            {
                comment = "The edi_system_id failed";
                personInsured.EDIID = insured["edi_system_id"].ToString();
                comment = "The first_name failed";
                personInsured.fname = insured["first_name"].ToString();
                comment = "The middle_name failed";
                personInsured.mname = insured["middle_name"].ToString();
                comment = "The last_name failed";
                personInsured.lname = insured["last_name"].ToString();

                comment = "The trn failed";
                if (insured["trn"] != null && insured["trn"].ToString() != "")
                    personInsured.TRN = insured["trn"].ToString();
                else
                    personInsured.TRN = String.Empty;

                comment = "The ID you have specified for this record failed";
                if (insured["ID"] != null && insured["ID"].ToString() != "")
                    personInsured.ID = insured["ID"].ToString();
                else
                    personInsured.ID = String.Empty;

                comment = "The address failed";
                if (insured["address"]["Address_Line_1"] != null && insured["address"]["Address_Line_2"] != null && insured["address"]["Address_Line_3"] != null)
                {
                    personInsured.address = new Address(insured["address"]["Address_Line_1"].ToString() + Environment.NewLine + insured["address"]["Address_Line_2"].ToString() + Environment.NewLine + insured["address"]["Address_Line_3"].ToString(),true);
                }
                else
                {
                    personInsured.address = new Address(insured["address"]["number"].ToString(),
                                                        insured["address"]["road"].ToString(),
                                                        insured["address"]["type"].ToString(),
                                                        null,
                                                        insured["address"]["city"].ToString(),
                                                        insured["address"]["parish"].ToString(),
                                                        insured["address"]["country"].ToString());
                }
                personInsured.address.ID = AddressModel.AddAddress(personInsured.address, uid); //adding address to the system and setting ID
                if (personInsured.address.ID == 0)
                {
                    throw new Exception("address error");
                }
                PersonModel.AddPerson(personInsured, uid, true,true); //add person to system
            }
            catch(Exception){
                this.error = errorModel.AddError(policy_data, "person", reqkey, this.error, comment, insured);
                personInsured = null;
                
            }
            return personInsured;
        }

        /// <summary>
        /// This function parses the data required to add a driver to the system
        /// </summary>
        /// <param name="driverindex"></param>
        /// <param name="policy_data"></param>
        /// <param name="uid"></param>
        /// <param name="reqkey"></param>
        /// <returns></returns>
        private Driver ParseDriverData(JToken driverindex, JToken policy_data, int uid, string reqkey = null)
        {
            string comment = null;
            Driver driver = new Driver();
            driver.person = new Person();
            try
            {
                
                comment = "The first_name failed";
                driver.person.fname = driverindex["first_name"].ToString();
                comment = "The middle_name failed";
                driver.person.mname = driverindex["middle_name"].ToString();
                comment = "The last_name failed";
                driver.person.lname = driverindex["last_name"].ToString();

                comment = "The trn failed";
                if (driverindex["trn"] != null && driverindex["trn"].ToString() != "")
                    driver.person.TRN = driverindex["trn"].ToString();
                else
                    driver.person.TRN = String.Empty;

                comment = "The ID failed";
                if(driverindex["ID"] != null && driverindex["ID"].ToString() != "") 
                    driver.person.ID = driverindex["ID"].ToString();

                try
                {
                    comment = "The date_of_birth failed";
                    if (driverindex["date_of_birth"] != null && driverindex["date_of_birth"].ToString() != "")
                        driver.person.DOB = DateTime.Parse(driverindex["date_of_birth"].ToString());
                    comment = "The date_license_issued failed";
                    if (driverindex["date_license_issued"] != null && driverindex["date_license_issued"].ToString() != "")
                        driver.dateissued = DateTime.Parse(driverindex["date_license_issued"].ToString());
                    comment = "The type_of_license failed";
                    driver.licenseclass = driverindex["type_of_license"].ToString();

                    if (driverindex["edi_system_id"] != null && driverindex["edi_system_id"].ToString() != "")
                    {
                        comment = "The edi_system_id failed";
                        driver.ediid = driverindex["edi_system_id"].ToString();
                    }

                }
                catch { }
                PersonModel.AddPerson(driver.person, uid,false,true); //add person to system
                DriverModel.addDriver(driver, uid);

            }
            catch (Exception)
            {
                this.error = errorModel.AddError(policy_data, "driver", reqkey, this.error, comment, driverindex);
                driver = null;
            }
            return driver;
        }

        /// <summary>
        /// this function parses the companies who are marked as the insured's in the policy being read.
        /// It automatically adds the companies to the database and awaits to be added to the policy
        /// </summary>
        /// <param name="companyindex"></param>
        /// <returns></returns>
        private Company ParseInsuredCompanyData(JToken companyindex, JToken policy_data, int uid, string reqkey = null)
        {
            string comment = null;
            Company company = new Company();
            try
            {
                //comment = "The name or trn failed";
               // company = new Company(companyindex["name"].ToString(), companyindex["trn"].ToString());

                comment = "The name or trn failed";
                if (companyindex["trn"] != null && companyindex["trn"].ToString() != "")
                    company = new Company(companyindex["name"].ToString(), companyindex["trn"].ToString());
                else
                    company = new Company(companyindex["name"].ToString(), String.Empty);

                comment = "The ID you specified for this record failed";
                if (companyindex["ID"] != null && companyindex["ID"].ToString() != "")
                    company.ID = companyindex["ID"].ToString();
                else
                    company.ID = String.Empty;

                comment = "The edi_system_id failed";
                company.EDIID = companyindex["edi_system_id"].ToString();
                comment = "The address failed";

                if (companyindex["address"]["Address_Line_1"] != null && companyindex["address"]["Address_Line_2"] != null && companyindex["address"]["Address_Line_3"] != null)
                {
                    company.CompanyAddress = new Address(companyindex["address"]["Address_Line_1"].ToString() + Environment.NewLine + companyindex["address"]["Address_Line_2"].ToString() + Environment.NewLine + companyindex["address"]["Address_Line_3"].ToString(),true);
                }
                else
                {
                    company.CompanyAddress = new Address(companyindex["address"]["number"].ToString(),
                                                            companyindex["address"]["road"].ToString(),
                                                            companyindex["address"]["type"].ToString(),
                                                            null,
                                                            companyindex["address"]["city"].ToString(),
                                                            companyindex["address"]["parish"].ToString(),
                                                            companyindex["address"]["country"].ToString());
                }
                if (!CompanyModel.AddCompany(company, uid, true,true)) throw new Exception("company error");
                //{
                    //if (company.trn == "")
                    //{
                    //    comment = "company policy_insured's trn is empty";
                    //    throw new Exception();
                    //}
                    //try
                    //{
                    //    company.CompanyID = CompanyModel.GetCompanyByTRN(company.trn, 0, true).CompanyID;
                    //}
                    //catch
                    //{
                    //    comment = "company policy_insured's trn failed to be processed";
                    //   // throw new Exception();
                    //}
                //}
                //else throw new Exception("company error");
            }
            catch (Exception)
            {
                this.error = errorModel.AddError(policy_data, "company", reqkey, this.error, comment, companyindex);
                company = null;
            }
            return company;
        }

        /// <summary>
        /// this function converts JTokens to a Vehicle model
        /// </summary>
        /// <param name="riskindex"></param>
        /// <param name="uid"></param>
        /// <param name="reqkey"></param>
        /// <returns></returns>
        private Vehicle ParseVehicleData(JToken riskindex, JToken policy_data, int uid, string reqkey = null, Policy pol = null, JToken drivers = null)
        {
            bool driverCheck = true;
            string comment = null;
            Vehicle Risk = new Vehicle();
            List<Driver> authorizeddrivers = new List<Driver>();
            List<Driver> expecteddrivers = new List<Driver>();
            List<Driver> excludeddrivers = new List<Driver>();
            try
            {
                comment = "The edi_system_id failed";
                Risk.EDIID = riskindex["edi_system_id"].ToString();

                comment = "The make failed";
                Risk.make = riskindex["make"].ToString();
                if (Risk.make.Length == 0) throw new Exception();

                if (riskindex["model"] != null && riskindex["model"].ToString() != "")
                {
                    comment = "The model failed";
                    Risk.VehicleModel = riskindex["model"].ToString();
                }
                //if (Risk.VehicleModel.Length == 0) throw new Exception();

                comment = "The model_type failed";
                Risk.modelType = riskindex["model_type"].ToString();
                
                comment = "The body_type failed";
                Risk.bodyType = riskindex["body_type"].ToString();

                comment = "The extension failed";
                Risk.extension = riskindex["extension"].ToString();

                if (riskindex["registration_number"] != null && riskindex["registration_number"].ToString() != "")
                {
                    comment = "The registration_number failed";
                    Risk.VehicleRegNumber = riskindex["registration_number"].ToString();
                }
                //if (Risk.VehicleRegNumber.Length == 0) throw new Exception();

                comment = "The chassis_number failed";
                Risk.chassisno = riskindex["chassis_number"].ToString();
                if (Risk.chassisno.Length == 0) throw new Exception();

                comment = "The colour failed";
                Risk.colour = riskindex["colour"].ToString();
                comment = "The usage failed";
                Risk.usage = new Usage(riskindex["usage"].ToString());
                

                #region optionaldata
                driverCheck = true;
                if (riskindex["authorized_drivers"] != null)
                {
                    driverCheck = false;
                    comment = "The authorized_drivers failed";
                    foreach (var authdrivers in riskindex["authorized_drivers"].Children())
                    {
                        //comment = "The trn in authorized_drivers failed";

                        Person p = new Person();

                        comment = "No details for authorized driver";
                        foreach(var driver in drivers.Children())
                        {
                            if (authdrivers["trn"] != null && authdrivers["trn"].ToString() != "")
                            {
                                p = new Person(authdrivers["trn"].ToString());
                                if ((driver["trn"] != null && driver["trn"].ToString() != "") && driver["trn"].ToString() == authdrivers["trn"].ToString())
                                {
                                    p.fname = driver["first_name"].ToString();
                                    p.mname = driver["middle_name"].ToString();
                                    p.lname = driver["last_name"].ToString();
                                    driverCheck = true;
                                    PersonModel.AddPerson(p, uid,false,true);
                                    break;
                                }
                                
                            }
                            else if (authdrivers["ID"] != null && authdrivers["ID"].ToString() != "")
                            {
                                //constructor needed
                                p = new Person(String.Empty);
                                p.ID = authdrivers["ID"].ToString();
                                if ((driver["ID"] != null && driver["ID"].ToString() != "") && driver["ID"].ToString() == authdrivers["ID"].ToString())
                                {
                                    p.fname = driver["first_name"].ToString();
                                    p.mname = driver["middle_name"].ToString();
                                    p.lname = driver["last_name"].ToString();
                                    p.PersonID = PersonModel.GetPersonByImportID(authdrivers["ID"].ToString());
                                    driverCheck = true;
                                    break;
                                }
                                
                            }
                        }

                        if (!driverCheck) this.error = errorModel.AddError(policy_data, "risk", reqkey, this.error, comment);

                        if (p.PersonID != 0)
                        {
                            Driver d = new Driver(p);
                            DriverModel.addDriver(d, uid);

                            authorizeddrivers.Add(d);
                        }
                    }
                }
                driverCheck = true;
                if (riskindex["excepted_drivers"] != null)
                {
                    driverCheck = false;
                    comment = "The excepted_drivers failed";
                    foreach (var exceptdrivers in riskindex["excepted_drivers"].Children())
                    {
                        //comment = "The trn in excepted_drivers failed";
                        Person p = new Person(String.Empty);

                        comment = "No details for excepted driver";
                        foreach (var driver in drivers.Children())
                        {
                            
                            if (exceptdrivers["trn"] != null && exceptdrivers["trn"].ToString() != "")
                            {
                                p = new Person(exceptdrivers["trn"].ToString());
                                if ((driver["trn"] != null && driver["trn"].ToString() != "") && driver["trn"].ToString() == exceptdrivers["trn"].ToString())
                                {
                                    p.fname = driver["first_name"].ToString();
                                    p.mname = driver["middle_name"].ToString();
                                    p.lname = driver["last_name"].ToString();
                                    driverCheck = true;
                                    PersonModel.AddPerson(p, uid,false,true);
                                    break;
                                }
                                
                            }
                            else if (exceptdrivers["ID"] != null && exceptdrivers["ID"].ToString() != "")
                            {
                                //property needed
                                p = new Person(String.Empty);
                                p.ID = exceptdrivers["ID"].ToString();
                                if ((driver["ID"] != null && driver["ID"].ToString() != "") && driver["ID"].ToString() == exceptdrivers["ID"].ToString())
                                {
                                    p.fname = driver["first_name"].ToString();
                                    p.mname = driver["middle_name"].ToString();
                                    p.lname = driver["last_name"].ToString();
                                    p.PersonID = PersonModel.GetPersonByImportID(exceptdrivers["ID"].ToString());
                                    driverCheck = true;
                                    break;
                                }
                                
                            }



                        }
                        if (!driverCheck) this.error = errorModel.AddError(policy_data, "risk", reqkey, this.error, comment);
                       // PersonModel.AddPerson(p, uid);
                        if (p.PersonID != 0)
                        {
                            Driver d = new Driver(p);
                            DriverModel.addDriver(d, uid);

                            expecteddrivers.Add(d);
                        }
                    }
                }
                driverCheck = true;
                if (riskindex["excluding_drivers"] != null)
                {
                    driverCheck = false;
                    comment = "The excluding_drivers failed";
                    foreach (var excluded in riskindex["excluding_drivers"].Children())
                    {
                        //comment = "The trn in excluding_drivers failed";
                        Person p = new Person(String.Empty);

                        comment = "No details for excluding driver";
                        foreach (var driver in drivers.Children())
                        {
                         
                            if (excluded["trn"] != null && excluded["trn"].ToString() != "")
                            {
                                p = new Person(excluded["trn"].ToString());
                                if ( ( driver["trn"] != null && driver["trn"].ToString() != "" ) && (driver["trn"].ToString() == excluded["trn"].ToString()))
                                {
                                    p.fname = driver["first_name"].ToString();
                                    p.mname = driver["middle_name"].ToString();
                                    p.lname = driver["last_name"].ToString();
                                    driverCheck = true;
                                    PersonModel.AddPerson(p, uid,false,true);
                                    break;
                                }
                                
                            }
                            else if (excluded["ID"] != null && excluded["ID"].ToString() != "")
                            {
                                //constructor needed
                                p = new Person(String.Empty);
                                p.ID = excluded["ID"].ToString();
                                if ((driver["ID"] != null && driver["ID"].ToString() != "") && (driver["ID"].ToString() == excluded["ID"].ToString()))
                                {
                                    p.fname = driver["first_name"].ToString();
                                    p.mname = driver["middle_name"].ToString();
                                    p.lname = driver["last_name"].ToString();
                                    p.PersonID = PersonModel.GetPersonByImportID(excluded["ID"].ToString());
                                    driverCheck = true;
                                    break;
                                }
                              
                            }
                        }
                        if (!driverCheck) this.error = errorModel.AddError(policy_data, "risk", reqkey, this.error, comment); 
                        //PersonModel.AddPerson(p, uid);
                        if (p.PersonID != 0)
                        {
                            Driver d = new Driver(p);
                            DriverModel.addDriver(d, uid);

                            excludeddrivers.Add(d);
                        }
                    }
                }
                if ((riskindex["left_hand_drive"] != null && riskindex["left_hand_drive"].ToString() != "") || (riskindex["right_hand_drive"] != null && riskindex["right_hand_drive"].ToString() != ""))
                {
                    comment = "The left_hand_drive or right_hand_drive failed";
                    if ((riskindex["left_hand_drive"] != null && riskindex["left_hand_drive"].ToString() != "") && (riskindex["right_hand_drive"] != null && riskindex["right_hand_drive"].ToString() != ""))
                    {
                        comment = "The left_hand_drive failed";
                        if ((bool)riskindex["left_hand_drive"]) Risk.handDrive = new HandDriveType("Left");
                        comment = "The right_hand_drive failed";
                        if ((bool)riskindex["right_hand_drive"]) Risk.handDrive = new HandDriveType("Right");
                    }
                    else
                    {
                        if (riskindex["left_hand_drive"] != null && riskindex["left_hand_drive"].ToString() != "")
                        {
                            comment = "The left_hand_drive failed";
                            if ((bool)riskindex["left_hand_drive"]) Risk.handDrive = new HandDriveType("Left");
                        }
                        if (riskindex["right_hand_drive"] != null && riskindex["right_hand_drive"].ToString() != "")
                        {
                            comment = "The right_hand_drive failed";
                            if ((bool)riskindex["right_hand_drive"]) Risk.handDrive = new HandDriveType("Right");
                        }
                    }
                }
                if (riskindex["cylinders"] != null && riskindex["cylinders"].ToString() != "")
                {
                    comment = "The cylinders failed";
                    Risk.cylinders = int.Parse(riskindex["cylinders"].ToString());
                }
                if (riskindex["engine_modified"] != null && riskindex["engine_modified"].ToString() != "")
                {
                    comment = "The engine_modified failed";
                    Risk.engineModified = (bool)riskindex["engine_modified"];
                }
                if (riskindex["engine_number"] != null && riskindex["engine_number"].ToString() != "")
                {
                    comment = "The engine_number failed";
                    Risk.engineNo = riskindex["engine_number"].ToString();
                }
                if (riskindex["engine_type"] != null && riskindex["engine_type"].ToString() != "")
                {
                    comment = "The engine_type failed";
                    Risk.engineType = riskindex["engine_type"].ToString();
                }
                if (riskindex["estimated_annual_mileage"] != null && riskindex["estimated_annual_mileage"].ToString() != "")
                {
                    comment = "The estimated_annual_mileage failed";
                    Risk.estAnnualMileage = int.Parse(riskindex["estimated_annual_mileage"].ToString());
                }
                
                if (riskindex["vin"] != null && riskindex["vin"].ToString() != "")
                {
                    comment = "The vin failed";
                    Risk.VIN = riskindex["vin"].ToString();
                }
                if (riskindex["registration_location"] != null && riskindex["registration_location"].ToString() != "")
                {
                    comment = "The registration_location failed";
                    Risk.registrationLocation = riskindex["registration_location"].ToString();
                }
                if (riskindex["registration_location"] != null && riskindex["registration_location"].ToString() != "" && riskindex["registration_location"].ToString() != "0")
                {
                    comment = "The seating_for_cert failed";
                    Risk.seatingCert = int.Parse(riskindex["seating_for_cert"].ToString());
                }
                driverCheck = true;
                Risk.mainDriver = new Driver();
                if (riskindex["main_driver_trn"] != null && riskindex["main_driver_trn"].ToString() != "")
                {
                    driverCheck = false;
                    comment = "The main_driver_trn failed";

                    if (riskindex["main_driver_trn"].Children().Count() > 0 && riskindex["main_driver_trn"]["ID"] != null && riskindex["main_driver_trn"]["ID"].ToString() != "")
                    {
                        if (riskindex["main_driver_trn"]["trn"] != null && riskindex["main_driver_trn"]["trn"].ToString() != "")
                            Risk.mainDriver.person = new Person(riskindex["main_driver_trn"]["trn"].ToString());
                        else
                            Risk.mainDriver.person = new Person(String.Empty);

                        comment = "The ID specified for this record failed";
                        Risk.mainDriver.person.ID = riskindex["main_driver_trn"]["ID"].ToString();
                        
                        comment = "No details for main_driver driver";
                        foreach (var driver in drivers.Children())
                        {
                            if (driver["ID"].ToString() == riskindex["main_driver_trn"]["ID"].ToString())
                            {
                                Risk.mainDriver.person.fname = driver["first_name"].ToString();
                                Risk.mainDriver.person.mname = driver["middle_name"].ToString();
                                Risk.mainDriver.person.lname = driver["last_name"].ToString();
                                driverCheck = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        Risk.mainDriver.person = new Person(riskindex["main_driver_trn"].ToString());
                        comment = "No details for main_driver driver";
                        foreach (var driver in drivers.Children())
                        {
                            if (driver["trn"].ToString() == riskindex["main_driver_trn"].ToString())
                            {
                                Risk.mainDriver.person.fname = driver["first_name"].ToString();
                                Risk.mainDriver.person.mname = driver["middle_name"].ToString();
                                Risk.mainDriver.person.lname = driver["last_name"].ToString();
                                driverCheck = true;
                                break;
                            }
                        }
                    }

                    
                    if (!driverCheck) this.error = errorModel.AddError(policy_data, "risk", reqkey, this.error, comment); 
                }

                if (riskindex["date_license_issued"] != null && riskindex["date_license_issued"].ToString() != "")
                {
                    comment = "The date_license_issued failed";
                    Risk.mainDriver.dateissued = DateTime.Parse(riskindex["date_license_issued"].ToString());
                }
                if (riskindex["type_of_license"] != null && riskindex["type_of_license"].ToString() != "")
                {
                    comment = "The type_of_license failed";
                    Risk.mainDriver.licenseclass = riskindex["type_of_license"].ToString();
                }

                if (riskindex["mileage"] != null && riskindex["mileage"].ToString() != "")
                {
                    comment = "The mileage failed";
                    Risk.mileage = riskindex["mileage"].ToString();
                }
                if (riskindex["expiry_date_of_fitness"] == null) Risk.expiryDateOfFitness = new DateTime();
                else if (riskindex["expiry_date_of_fitness"].ToString() == "") Risk.expiryDateOfFitness = new DateTime();
                else
                {
                    comment = "The expiry_date_of_fitness failed";
                    Risk.expiryDateOfFitness = DateTime.Parse(riskindex["expiry_date_of_fitness"].ToString());
                }
                #endregion

                Risk.ExcludedDrivers = excludeddrivers;
                Risk.AuthorizedDrivers = authorizeddrivers;
                Risk.ExceptedDrivers = expecteddrivers;
                comment = "The hpcc failed";
                Risk.HPCCUnitType = riskindex["hpcc"].ToString();
                comment = "The reference_number failed";
                Risk.referenceNo = riskindex["reference_number"].ToString();
                comment = "The restricted_driving failed";
                Risk.RestrictedDriving = (bool)riskindex["restricted_driving"];
                comment = "The roof_type failed";
                Risk.roofType = riskindex["roof_type"].ToString();
                comment = "The seating failed";
                Risk.seating = int.Parse(riskindex["seating"].ToString());
                comment = "The tonnage failed";
                try
                {
                    Risk.tonnage = int.Parse(riskindex["tonnage"].ToString());
                }
                catch
                {
                    Risk.tonnage = 0;
                }

                comment = "The transmission_type failed";
                Risk.transmissionType = new TransmissionType(riskindex["transmission_type"].ToString());
                comment = "The year failed";
                Risk.VehicleYear = int.Parse(riskindex["year"].ToString());

                Risk.usage = UsageModel.CreateUsage(new Usage(Risk.usage.usage), uid);
                if (Risk.usage.ID == 0)
                {
                    if (Risk.usage.usage.Length == 0) comment = "the usage is blank";
                    else comment = "the usage failed";
                    throw new Exception();
                }

                comment = "the chassis bad failed";
                Risk.ID = VehicleModel.GetVehicleByChassis(Risk.chassisno, uid).ID;
                if (Risk.ID == 0)
                {
                    comment = "the risk failed";
                    Risk = VehicleModel.addVehicle(Risk, uid);
                    VehicleModel.MarkVehicleImported(Risk);
                    if (Risk.ID == 0)
                    {
                        comment = "the risk failed";
                        throw new Exception();
                    }
                }
                else
                {
                   
                    comment = "the risk failed";
                    VehicleModel.addVehicle(Risk, uid);
                }

            }
            catch(Exception e)
            {
                e.ToString();
                Risk = new Vehicle();
                this.error = errorModel.AddError(policy_data, "risk", reqkey, this.error, comment); 
                Risk = null;
            }
            return Risk;
        }

        /// <summary>
        /// this function converts JToken to a Certificate Model
        /// </summary>
        /// <param name="certificate"></param>
        /// <param name="pol"></param>
        /// <param name="uid"></param>
        /// <param name="reqkey"></param>
        /// <returns></returns>
        private VehicleCertificate ParseCertificateData(JToken policydata, JToken certificate, Policy pol, int uid, string reqkey = null)
        {
            string comment = null;
            VehicleCertificate vc = new VehicleCertificate();
            VehicleCoverNoteGenerated vcg = new VehicleCoverNoteGenerated();
            try
            {
                #region optional data
                if (certificate["signed_on_at"] != null && certificate["signed_on_at"].ToString() != "" && certificate["signed_on_at"].ToString() != "0000-00-00 00:00:00")
                {
                    comment = "The signed_on_at failed";
                    vc.SignedAt = DateTime.Parse(certificate["signed_on_at"].ToString());
                }
                if (certificate["signed_by_trn"] != null && certificate["signed_by_trn"].ToString() != "")
                {
                    comment = "The signed_by_trn failed";
                    vc.SignedBy = new Person(certificate["signed_by_trn"].ToString());
                }
                if (certificate["last_printed_by_trn"] != null && certificate["last_printed_by_trn"].ToString() != "")
                {
                    comment = "The last_printed_by_trn failed";
                    vc.LastPrintedBy = new Person(certificate["last_printed_by_trn"].ToString());
                }
                if (certificate["last_printed_by"] != null)
                {
                    comment = "The last_printed_by failed";
                    vc.last_printed_by_flat = certificate["last_printed_by"]["first_name"].ToString() + " " + certificate["last_printed_by"]["last_name"].ToString();
                }
                if (certificate["last_printed_by_plain_text"] != null && certificate["last_printed_by_plain_text"].ToString() != "")
                {
                    comment = "The last_printed_by_plain_text failed";
                    vc.last_printed_by_flat = certificate["last_printed_by_plain_text"].ToString(); 
                }

                #endregion

                comment = "certificate_type failed";
                vc.CertificateType = certificate["certificate_type"].ToString();
                vcg.certificateType = certificate["certificate_type"].ToString();
                comment = "The edi_system_id failed";
                vc.EDIID = certificate["edi_system_id"].ToString();

                comment = "The certificate_no failed";
                if (certificate["certificate_no"] != null)
                {
                    if (certificate["certificate_no"].ToString() != "")
                    vc.CertificateNo = certificate["certificate_no"].ToString();
                }
                comment = "The effective_date failed";
                if (certificate["effective_date"] == null || certificate["effective_date"].ToString() == "" || certificate["effective_date"].ToString() == "0000-00-00") vc.EffectiveDate = new DateTime();
                else vc.EffectiveDate = DateTime.Parse(certificate["effective_date"].ToString());

                comment = "The date_first_printed failed";
                if (certificate["date_first_printed"] == null || certificate["date_first_printed"].ToString() == "0000-00-00" || certificate["date_first_printed"].ToString() == "") vc.DateFirstPrinted = new DateTime();
                else vc.DateFirstPrinted = DateTime.Parse(certificate["date_first_printed"].ToString());
                comment = "The date_time_last_printed_on failed";
                if (certificate["date_time_last_printed_on"] == null || certificate["date_time_last_printed_on"].ToString() == "0000-00-00 00:00:00" || certificate["date_time_last_printed_on"].ToString() == "") vc.DateFirstPrinted = new DateTime();
                else vc.LastPrintedOn = DateTime.Parse(certificate["date_time_last_printed_on"].ToString());
                //vc.last_printed_by_flat = certificate["last_printed_by_plain_text"].ToString(); 

                comment = "The endorsement_number failed";
                vc.EndorsementNo = certificate["endorsement_number"].ToString();
                comment = "The printed_paper_number failed";
                vc.PrintedPaperNo = certificate["printed_paper_number"].ToString();
                comment = "The authorized_drivers_wording failed";
                vcg.authorizedwording = certificate["authorized_drivers_wording"].ToString();
                comment = "The limitations_of_use failed";
                vcg.limitsofuse = certificate["limitations_of_use"].ToString();
                comment = "The marks_and_registration_no failed";
                vc.MarksRegNo = certificate["marks_and_registration_no"].ToString();
                vc.policy = pol;
                comment = "The chassis_number failed";
                foreach (Vehicle risk in pol.vehicles)
                {
                    if (risk.chassisno == certificate["chassis_number"].ToString())
                    {
                        vc.Risk = risk;
                        break;
                    }
                }
                if (vc.Risk == null) throw new Exception();

                if (certificate["last_printed_by"] != null) vc.last_printed_by_flat = certificate["last_printed_by"]["first_name"].ToString() + " " + certificate["last_printed_by"]["last_name"].ToString();
                else if (certificate["last_printed_by_plain_text"] != null) vc.last_printed_by_flat = certificate["last_printed_by_plain_text"].ToString();
                
                
                comment = "The cancelled failed";
                try { vc.Cancelled = (bool)certificate["cancelled"]; }
                catch { vc.Cancelled = false; }
                if (vc.Cancelled)
                {
                    #region more optional data
                    if (certificate["cancelled_on_at"] != null && certificate["cancelled_on_at"].ToString() != "" && certificate["cancelled_on_at"].ToString() != "0000-00-00 00:00:00")
                    {
                        comment = "The cancelled_on_at failed";
                        vc.CancelledOn = DateTime.Parse(certificate["cancelled_on_at"].ToString());
                    }
                    if (certificate["cancelled_reason"] != null && certificate["cancelled_reason"].ToString() != "")
                    {
                        comment = "The cancelled_reason failed";
                        vc.CancelReason = certificate["cancelled_reason"].ToString();
                    }
                    if (certificate["cancelled_by_trn"] != null && certificate["cancelled_by_trn"].ToString() != "")
                    {
                        comment = "The cancelled_by_trn failed";
                        vc.CancelledBy = new Person(certificate["cancelled_by_trn"].ToString());
                    }
                    #endregion
                }

                comment = "The certificate failed";
                
                vc.generated_cert = vcg;
            }
            catch
            {
                if (Constants.certificateerrors == null) Constants.certificateerrors = new List<VehicleCertificate>();
                vc = new VehicleCertificate();
                this.error = errorModel.AddError(policydata, "certificate", reqkey, this.error, comment, certificate); //this parenting is all due to design of json
                vc = null;
            }
            return vc;
        }

        /// <summary>
        /// this function converts JToken data to a Cover Note Model
        /// </summary>
        /// <param name="cnote"></param>
        /// <param name="uid"></param>
        /// <param name="reqkey"></param>
        /// <returns></returns>
        private VehicleCoverNote ParseCoverNoteData(JToken policydata, JToken cnote, int uid, Policy pol, string reqkey = null)
        {
            string comment = null;
            VehicleCoverNote vcn = new VehicleCoverNote();
            vcn.gen = new VehicleCoverNoteGenerated();
            try
            {
                comment = "The alterations failed";
                vcn.alterations = cnote["alterations"].ToString();
                comment = "The cancelled failed";
                try { vcn.cancelled = (bool)cnote["cancelled"]; }
                catch { vcn.cancelled = false; }
                comment = "The cancelled_by failed";
                
                #region optionaldata
                if (cnote["edi_system_id"] != null && cnote["edi_system_id"].ToString() != "")
                {
                    comment = "The edi_system_id failed";
                    vcn.EDIID = cnote["edi_system_id"].ToString();
                }
                if (cnote["cancelled_by_trn"] != null && cnote["cancelled_by_trn"].ToString() != "")
                {
                    comment = "The cancelled_by_trn failed";
                    vcn.cancelledby = new Person(cnote["cancelled_by_trn"].ToString());
                }
                if (cnote["cancelled_by"] != null)
                {
                    comment = "The cancelled_by failed";
                    if (cnote["cancelled_by"]["first_name"] != null && cnote["cancelled_by"]["first_name"].ToString() != "" && cnote["cancelled_by"]["last_name"] != null && cnote["cancelled_by"]["last_name"].ToString() != "" && cnote["cancelled_by"]["email"] != null && cnote["cancelled_by"]["email"].ToString() != "") vcn.cancelledby = new Person(cnote["cancelled_by"]["first_name"].ToString(), cnote["cancelled_by"]["last_name"].ToString(), new List<Email> { new Email(cnote["cancelled_by"]["email"].ToString()) });
                }
                if (cnote["cancelled_by_plain_text"] != null && cnote["cancelled_by_plain_text"].ToString() != "")
                {
                    comment = "The cancelled_by_plain_text failed";
                    vcn.cancelledbyflat = cnote["cancelled_by_plain_text"].ToString();
                }
                if (cnote["cancelled_reason"] != null && cnote["cancelled_reason"].ToString() != "")
                {
                    comment = "The cancelled_reason failed";
                    vcn.cancelledreason = cnote["cancelled_reason"].ToString();
                }
                if (cnote["cancelled_on_at"] != null && cnote["cancelled_on_at"].ToString() != "")
                {
                    comment = "The cancelled_on_at failed";
                    vcn.cancelledon = DateTime.Parse(cnote["cancelled_on_at"].ToString());
                }
                if (cnote["date_first_printed"] != null && cnote["date_first_printed"].ToString() != "0000-00-00 00:00:00" && cnote["date_first_printed"].ToString() != "")
                {
                    comment = "The date_first_printed failed";
                    vcn.firstprinted = DateTime.Parse(cnote["date_first_printed"].ToString());
                }
                if (cnote["last_printed_on_at"] != null && cnote["last_printed_on_at"].ToString() != "0000-00-00 00:00:00" && cnote["last_printed_on_at"].ToString() != "")
                {
                    comment = "The last_printed_on_at failed";
                    vcn.lastprinted = DateTime.Parse(cnote["last_printed_on_at"].ToString());
                }
                if (cnote["last_printed_by"] != null)
                {
                    comment = "The last_printed_by failed";
                    vcn.lastprintflat = cnote["last_printed_by"]["first_name"].ToString() + " " + cnote["last_printed_by"]["last_name"].ToString();
                }
                if (cnote["last_printed_by_plain_text"] != null && cnote["last_printed_by_plain_text"].ToString() != "")
                {
                    comment = "The last_printed_by_plain_text failed";
                    vcn.lastprintflat = cnote["last_printed_by_plain_text"].ToString();
                }
                #endregion

                comment = "The cover_note_id failed";
                if (cnote["cover_note_id"] != null)
                {
                    if (cnote["cover_note_id"].ToString() != "")
                        vcn.covernoteid = cnote["cover_note_id"].ToString();
                }


                comment = "The printed_paper_number failed";
                vcn.printpaperno = cnote["printed_paper_number"].ToString();
                comment = "The printed_count failed";

                try {vcn.printcount = int.Parse(cnote["printed_count"].ToString());}
                catch { vcn.printcount = 0; }

                comment = "The effective_date_time failed";
                vcn.effectivedate = DateTime.Parse(cnote["effective_date_time"].ToString());
                comment = "The expiry_date failed";
                vcn.expirydate = DateTime.Parse(cnote["expiry_date"].ToString());
                comment = "The days_effective failed";
                vcn.period = int.Parse(cnote["days_effective"].ToString());
                comment = "The authorized_drivers_wording failed";
                vcn.gen.authorizedwording = cnote["authorized_drivers_wording"].ToString();
                comment = "The limitations_of_use failed";
                vcn.gen.limitsofuse = cnote["limitations_of_use"].ToString();
                comment = "The mortgagees failed";
                vcn.gen.mortgagees = cnote["mortgagees"].ToString();
                comment = "The endorsement_number failed";
                vcn.endorsementno = cnote["endorsement_number"].ToString();
                comment = "The limitations_of_use failed";
                vcn.limitsofuse = cnote["limitations_of_use"].ToString();
                comment = "The manual_cover_note_id failed";
                vcn.covernoteno = cnote["manual_cover_note_id"].ToString();
                comment = "The chassis_number failed";

                foreach (Vehicle risk in pol.vehicles)
                {
                    if (risk.chassisno == cnote["chassis_number"].ToString())
                    {
                        vcn.risk = risk;
                        break;
                    }
                }
                if (vcn.risk == null) throw new Exception();
            }
            catch
            {
                vcn = new VehicleCoverNote();
                this.error = errorModel.AddError(policydata, "covernote", reqkey, this.error, comment, cnote); //this parenting is all due to design of json
                vcn = null;
            }
            return vcn;
        }

        #region helper functions for error handling
        
        /// <summary>
        /// this function retrieves the EDIID from jtoken for when errors need to be reported
        /// </summary>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static string GetPolicyEDIIDFromJson(JToken policy)
        {
            string ediid = "not found";
            try
            {
                if (policy["policy"]["data"] != null)
                {
                    ediid = policy["policy"]["data"]["edi_system_id"].ToString();
                }
            }
            catch
            {
                //not really sure what to put here yet
                ediid = "error";
            }
            return ediid;

        }
        
        /// <summary>
        /// this function retrieves the EDIID from jtoken for when errors need to be reported
        /// </summary>
        /// <param name="cover"></param>
        /// <returns></returns>
        public static string GetCoverEDIIDFromJSON(JToken cover)
        {
            string ediid = "not found";
            try
            {
                ediid = cover["edi_system_id"].ToString();
            }
            catch
            {
                //not really sure what to put here yet
                ediid = "error";
            }
            return ediid;
        }
        
        /// <summary>
        /// this function retrieves the EDIID from jtoken for when errors need to be reported
        /// </summary>
        /// <param name="vehicles"></param>
        /// <param name="chassis"></param>
        /// <returns></returns>
        public static string GetRiskEDIIDFromJson(JToken vehicles, string chassis)
        {
            string ediid = "not found";
            try
            {
                foreach (var risk in vehicles.Children())
                {
                    if (risk["chassis_number"].ToString() == chassis) 
                    {
                        ediid = risk["edi_system_id"].ToString();
                        break;
                    }
                }
            }
            catch
            {
                ediid = "error";
            }
            return ediid;
        }
        
        /// <summary>
        /// this function retrieves the chassis from jtoken for when errors need to be reported
        /// </summary>
        /// <param name="cover"></param>
        /// <param name="ediid"></param>
        /// <returns></returns>
        public static string GetChassisFromCoverJson(JToken cover, string ediid)
        {
            string chassis = "not found";
            try
            {
                foreach (var c in cover)
                {
                    if (c["cover_note"] != null)
                    {
                        if (c["cover_note"]["chassis_number"] != null)
                        {
                            chassis = c["cover_note"]["chassis_number"].ToString();
                            break;
                        }
                    }
                    if (c["certificate"] != null)
                    {
                        if (c["certificate"]["chassis_number"] != null)
                        {
                            chassis = c["certificate"]["chassis_number"].ToString();
                            break;
                        }
                    }
                }
            }
            catch
            {
                chassis = "error";
            }
            
            return chassis;
        }
        
        /// <summary>
        /// this function determines if the data being pushed contains a cover note or a certificate
        /// </summary>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static string CoverOrCert(JToken policy)
        {
            string cc = "not found";
            if (policy["cover"] != null)
            {
                foreach(var cover in policy["cover"].Children())
                {
                    if (cover["cover_note"] != null)
                    {
                        cc = "covernote";
                        break;
                    }
                    if (cover["certificate"] != null)
                    {
                        cc = "certificate";
                        break;
                    }
                }
            }
            return cc;
        }
        
        /// <summary>
        /// this function retrieves the EDIID from jtoken for when errors need to be reported
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public static string GetCompanyEDIIDFromJson(JToken company)
        {
            string ediid = "not found";
            if (company["edi_system_id"] != null) ediid = company["edi_system_id"].ToString();
            return ediid;
        }
        
        /// <summary>
        /// this retrieves the driver's edi_system_id from the json
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static string GetDriverEDIIDFromJson(JToken driver)
        {
            string trn = "not found";
            if (driver["edi_system_id"] != null) trn = driver["edi_system_id"].ToString();
            return trn;
        }

        /// <summary>
        /// this function returns the driver's trn from the json
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static string GetDriverTRNFromJson(JToken driver)
        {
            string trn = "not found";
            if (driver["trn"] != null) trn = driver["trn"].ToString();
            return trn;
        }

        
        /// <summary>
        /// this function retrieves the EDIID from jtoken for when errors need to be reported
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        public static string GetPersonEDIIDFromJson(JToken person)
        {
            string ediid = "not found";
            if (person["edi_system_id"] != null) ediid = person["edi_system_id"].ToString();
            return ediid;
        }
        
        /// <summary>
        /// this function retrieves the EDIID from jtoken for when errors need to be reported
        /// </summary>
        /// <param name="cover"></param>
        /// <returns></returns>
        public static string GetCoverNoteEDIIDFromJson(JToken cover)
        {
            string ediid = "not found";
            if (cover["edi_system_id"] != null) ediid = cover["edi_system_id"].ToString();
            return ediid;
        }
        
        /// <summary>
        /// this function retrieves the EDIID from jtoken for when errors need to be reported
        /// </summary>
        /// <param name="cover"></param>
        /// <returns></returns>
        public static string GetCertEDIIDFromJson(JToken cover)
        {
            string ediid = "not found";
            if (cover["edi_system_id"] != null) ediid = cover["edi_system_id"].ToString();
            return ediid;
        }

        /// <summary>
        /// this function returns the trn for a policy holder insured
        /// </summary>
        /// <param name="insured"></param>
        /// <returns></returns>
        public static string GetInsuredTRNFromJson(JToken insured)
        {
            string trn = "not found";
            if (insured["trn"] != null) trn = insured["trn"].ToString();
            return trn;
        }

        /// <summary>
        /// this function returns the chassis number of a vehicle in a jtoken
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public static string GetVehicleChassisFromJson(JToken vehicle)
        {
            string chassisNo = "not found";
            if (vehicle["chassis_number"] != null) chassisNo = vehicle["chassis_number"].ToString();
            return chassisNo;
        }

        public void ProcessFilesThreadHandler(List<HttpPostedFileBase> files, JSON json = null, int kid = 0, string key = null)
        {
            Thread thread = new Thread(() => processFiles(files,json,kid,key));
            thread.Start();
        }

        public void processFiles(List<HttpPostedFileBase> files, JSON json = null, int kid = 0, string key = null)
        {
            //string tempData = "";
            string filepath = "";
            Import import = new Import();
            JSON js = new JSON();
            string path = AppDomain.CurrentDomain.BaseDirectory;
            string fileName;
            string sqlConnectionString;
            List<String> fileDetails = new List<string>();
            List<JSON> jsonObjects = new List<JSON>();
            string JsonString = "";


#if DEBUG
            sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;//ApplicationServices
#else
                sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ExternalConnection"].ConnectionString; 
#endif

            foreach (HttpPostedFileBase file in files)
            {
                
                if (file.ContentType.ToLower() == "text/plain" && file != null)
                {
                    try
                    {
                        fileName = kid + "_" + file.FileName;
                        filepath = path + "uploads\\" + fileName;
                        file.SaveAs(filepath);
                        JsonString = OmaxFramework.Utilities.FileHandler.ReadText(filepath);

                        JObject obj = JObject.Parse(JsonString);
                        obj["key"] = key;
                        obj["kid"] = kid;
                        JsonString = obj.ToString();
                        json.json = JsonString;

                        try
                        {
                            switch (import.ParseJSONAuth(json, "import"))
                            {
                                case "valid":
                                    json = import.StoreJSONS(json, Constants.user.ID, true);
                                    jsonObjects.Add(json);
                                    fileDetails.Add( "The file " + file.FileName  + " was uploaded succesfully. You will be notified when processing is done. " + Environment.NewLine + "Success: true. " + Environment.NewLine + "ID: " + json.ID + Environment.NewLine );
                                    OmaxFramework.Utilities.FileHandler.UploadFile(OmaxFramework.Utilities.FileHandler.ReadAllBytes(filepath), sqlConnectionString, "UPDATE JsonAudit SET JsonFile = @JsonFile, JsonTitle = @JsonTitle WHERE [key] = '" + json.reqkey + "'", "JsonFile", fileName, "JsonTitle");
                                    
                                    break;
                                case "inprogress":
                                    fileDetails.Add( "We are still processing your previous request. Please try back shortly. " + Environment.NewLine + "Count: " + json.count + Environment.NewLine + " Success: true. " + Environment.NewLine + "Status:inprogress");
                                    break;
                                case "completed":
                                    js = Constants.JsonResponse;
                                    Constants.JsonResponse = new JSON();
                                    fileDetails.Add("The process is complete on " + file.FileName + ". Send more requests if you like. " + Environment.NewLine + "Processed: " + js.processedjson);

                                    break;
                                case "fail":
                                    js = Constants.JsonResponse;
                                    Constants.JsonResponse = new JSON();
                                    fileDetails.Add("Completed but something failed on " + file.FileName + "." + Environment.NewLine + "Processed: " + json.processedjson);
                                    break;
                                case "loggedin":
                                     fileDetails.Add("You are already logged in");
                                    break;
                                case "bad key":
                                    fileDetails.Add("Success: false. " + Environment.NewLine + "You did something wrong.");
                                    break;
                                default:
                                    fileDetails.Add("Success: false. " + Environment.NewLine + "Status:bad session key. " + Environment.NewLine + "Failed Authentication");
                                    break;
                            }
                        }
                        catch (Exception e)
                        {

                            fileDetails.Add("Invalid JSON was sent on file " + file.FileName + ". Please check your file");
                        }


                    }
                    catch
                    {
                       fileDetails.Add("There was an error in uploading your file " + file.FileName + ". Please try again later");
                    }


                }
                else
                {
                    fileDetails.Add( "Invalid file: " + file.FileName + ".");
                }

                json = new JSON() ;
                OmaxFramework.Utilities.FileHandler.DeleteFile(filepath);
            }

            

            if (EmailModel.GetPrimaryEmail(Constants.user) != null && EmailModel.GetPrimaryEmail(Constants.user).email != "")
            {
                Mail mail = new Mail();
                List<User> users = new List<User>();
                users.Add(Constants.user);
                mail.to = new List<string>();
                mail.to.Add(EmailModel.GetPrimaryEmail(Constants.user).email);
                mail.subject = "File Upload Status";

                mail.body = "This is the result of processed files:\r\n";
                foreach (string message in fileDetails) mail.body += message + "\r\n";
                
                MailModel.SendMailWithThisFunction(users, mail);

            }
  
            import.ParseJSONSInThread(jsonObjects, Constants.user.ID, true);
               
            

            //return tempData;
        }

        //public static List<string> getTokenName(List<JToken> tokens)
        //{
        //    bool defaultCheck = false,polCheck = false, riskCheck = false, coverCheck = false;
        //    int records = 0;
        //    JProperty item;
        //    List<string> ChildrenList = new List<string>();
        //    Object Tname;
        //    JProperty Jprop;
        //    JObject Jobj;
        //    JEnumerable<JToken> JT = new JEnumerable<JToken>();

        //    foreach (JObject ch in tokens)
        //    {
        //        records++;
        //        JT = ch.Children();
        //        foreach (JToken toks in JT)
        //        {

        //            foreach (JProperty toksProp in toks.AsJEnumerable().Children())
        //            {
        //                ChildrenList.Add(toksProp.Name);
        //            }



        //            //Tname = i.Name;
        //          //  ChildrenList.Add(Tname.ToString());

        //            //switch (i.Name)
        //            //{
        //            //    case "policy":
        //            //        polCheck = true;
        //            //        forJT.ElementAt(0).Children()
        //            //        foreach (JToken polToken in JT)
        //            //        {
        //            //            polToken.ElementAt(0);
        //            //        }
        //            //        break;
        //            //    case "risk":
        //            //        riskCheck = true;
        //            //        break;
        //            //    case "cover":
        //            //        coverCheck = true;
        //            //        break;
        //            //    default:
        //            //        defaultCheck = true;
        //            //        break;
        //            //}
        //        }

        //    }

        //    //if (defaultCheck)
        //    //{
        //    //    if (!riskCheck) ChildrenList.Add("No Risk Object");
        //    //    if (!polCheck) ChildrenList.Add("No Policy Object");
        //    //    if (!coverCheck) ChildrenList.Add("No Cover Object");
        //    //}

        //    return ChildrenList;
        //}

           


        #endregion
        
    }
}
