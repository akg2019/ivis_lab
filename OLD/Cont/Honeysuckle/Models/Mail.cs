﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mail;
using System.Data.SqlClient;
using System.Threading;
using System.Text.RegularExpressions;

namespace Honeysuckle.Models
{
    public class Mail
    {
        public int outboundmailID {get;set;}
        public int userInitiatedBy { get; set; }
        public List<string> to { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public string joinedTo { get; set; }
        public bool sent { get; set; }
        
        public Mail() { }
        public Mail(int outboundmailID, List<string> to, string subject, string body, int userInitiatedBy)
        {
            this.outboundmailID = outboundmailID;
            this.to = to;
            this.subject = subject;
            this.body = body;
            this.userInitiatedBy = userInitiatedBy;
        }
        public Mail(int outboundmailID,List<string> to, string subject, string body)
        {
            this.outboundmailID = outboundmailID;
            this.to = to;
            this.subject = subject;
            this.body = body;
        }

        public Mail(List<string> to, string subject, string body)
        {
            this.to = to;
            this.subject = subject;
            this.body = body;
        }

    }

    public class MailModel
    {
        private static string host = "smtpout.secureserver.net";
        private static int port = 465;
        private static string username = "ivis@etechja.com";
        private static string password = "insurance@2015"; 


        /// <summary>
        /// function that sends an email using deprecated class
        /// </summary>
        /// <param name="mail">valid mail</param>
        private static Mail SendMail(Mail mail, List<User> userto = null,bool resend = false)
        {
            bool mailSent = false;
            if (mail.to.Count > 0)
            {
                MailMessage message = new MailMessage();
                message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserver", host);
                message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", port.ToString());
                message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");
                message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");//Use 0 for anonymous
                message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", username);
                message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", password);
                message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", "true");
                message.From = username;
                string to = "";

                if (mail.joinedTo != null && mail.joinedTo != "") to = mail.joinedTo;
                else
                {
                    foreach (string email in mail.to)
                    {
                        if (to == "") to = email + ";";
                        else to = to + email + ";";
                    }
                }
                message.To = to;
                message.Subject = mail.subject;
                message.Body = mail.body;
                SmtpMail.SmtpServer = host + ":" + port.ToString();

                for (int i = 0; i < 3; i++)
                {
                    try
                    {
                        SmtpMail.Send(message);
                        Constants.tempuser = null;
                        mailSent = true;
                        break;
                    }
                    catch (HttpException e)
                    {
                        
                        mailSent = false;
                        
                    }
                }


                if (!mailSent)
                {

                    try
                    {

                        SmtpMail.Send(message);
                        Constants.tempuser = null;
                    }
                    catch (HttpException e)
                    {
                        if (!resend)
                        {
                            mail.sent = false;
                            LogFailedEmail(mail, e);
                            mail.outboundmailID = MailModel.storeMail(mail, false);
                            NotificationModel.CreateNotification(new Notification(0, userto, Constants.user, mail.subject, "Email for " + message.Subject + " was not sent to " + to + ". An Error occured. The system will re-attempt", false, false, false));
                        }
                        return mail;
                    }
                    catch (Exception ex)
                    {
                        //CODE FOR WHEN NOT HTTPEXCEPTION
                        if (!resend)
                        {
                            mail.sent = false;
                            mail.outboundmailID = MailModel.storeMail(mail, false);
                            NotificationModel.CreateNotification(new Notification(0, userto, Constants.user, mail.subject, "Email for " + message.Subject + " was not sent to " + to + ". An Error occured. The system will re-attempt", false, false, false));
                        }
                        return mail;
                    }
                }

                if(!resend){
                    mail.outboundmailID = MailModel.storeMail(mail, true);
                    mail.sent = true;
                }
            }
            else
            {
                //there are no users to send to
            }
            return mail;
        }
        
        /// <summary>
        /// begins the sending the email on a new thread
        /// </summary>
        /// <param name="mail">Valid Mail</param>
        private static void ThreadedMailHandler(Mail mail, List<User> userto = null) 
        {
            Thread thread = new Thread(() => SendMail(mail,userto));
            thread.Start();
        }

        /// <summary>
        /// this is the public email function, it also creates a notification first before threading
        /// </summary>
        /// <param name="userto">valid user who the email is being sent to</param>
        /// <param name="mail">valid message to be sent</param>
        public static Mail SendMailWithThisFunction(List<User> userto, Mail mail, bool resend = false)
        {
          
            //Playing with regex to find url and add html tags (doesn't quite work here, only in js)
            string regex =  @"(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])";
            Regex r = new Regex(regex, RegexOptions.IgnoreCase);
            string BodyEdited = r.Replace(mail.body, "<a href='$1'>$1</a>");

            NotificationModel.CreateNotification(new Notification(0, userto, Constants.user, mail.subject, mail.body, false, false, false));
            //ThreadedMailHandler(mail,userto);
            return SendMail(mail, userto, resend);
        }

        /// <summary>
        /// this logs a failed email to a table in the database
        /// </summary>
        /// <param name="mail">valid mail</param>
        /// <param name="e">httpexception</param>
        private static void LogFailedEmail(Mail mail, HttpException e)
        {
            sql sql = new sql();
            sql.ConnectSQL();
            string to = "";
            foreach (string email in mail.to)
            {
                if (to == "") to = email + ";";
                else to = to + email + ";";
            }
            if (Constants.tempuser != null)
            {
                string query = "EXEC LogFailedEmail '" + Constants.CleanString(to) + "','" + Constants.CleanString(mail.body) + "','" + Constants.CleanString(mail.subject) + "','" + Constants.CleanString(e.Message) + "','" + Constants.CleanString(Constants.tempuser.username) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "email", new Email());
                reader.Close();
                Constants.tempuser = null;
            }
            else
            {
                string query = "EXEC LogFailedEmail '" + Constants.CleanString(to) + "','" + Constants.CleanString(mail.body) + "','" + Constants.CleanString(mail.subject) + "','" + Constants.CleanString(e.Message) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "email", new Email());
                reader.Close();
            }
            sql.DisconnectSQL();
        }

        public static int storeMail(Mail mail, bool sent = true)
        {
            string query = "",to="";
            int outboundMailID = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                foreach (string email in mail.to)
                {
                    if (to == "") to = email + ";";
                    else to = to + email + ";";
                }
                query = "EXEC StoreOutboundMail '" + Constants.CleanString(to) + "','" + Constants.CleanString(mail.subject) + "','" + Constants.CleanString(mail.body) + "'," + sent + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query);

                while (reader.Read())
                {
                    outboundMailID = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }

            return outboundMailID;
        }

        public static Mail getMailsNotSent()
        {
            sql sql = new sql();
            Mail mail = new Mail();
            string query = "";
            if (sql.ConnectSQL())
            {
                query = "EXEC getMailNotSent " + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query);

                while (reader.Read())
                {
                    if (DBNull.Value != reader["To"]) mail.joinedTo = reader["To"].ToString();
                    if (DBNull.Value != reader["Subject"]) mail.subject = reader["Subject"].ToString();
                    if (DBNull.Value != reader["Body"]) mail.body = reader["Body"].ToString();
                    mail.sent = bool.Parse(reader["Sent"].ToString());


                }
                reader.Close();
                sql.DisconnectSQL();

            }

            
            return mail;
        }

        public static Mail GetMailByID(int mailID)
        {
            sql sql = new sql();
            Mail mail = new Mail();
            string query = "";
            if (sql.ConnectSQL())
            {
                query = "EXEC GetMailByID " + mailID;
                SqlDataReader reader = sql.QuerySQL(query);

                while (reader.Read())
                {
                    if (DBNull.Value != reader["To"]) mail.joinedTo = reader["To"].ToString();
                    if (DBNull.Value != reader["Subject"]) mail.subject = reader["Subject"].ToString();
                    if (DBNull.Value != reader["Body"]) mail.body = reader["Body"].ToString();
                    mail.sent = bool.Parse(reader["Sent"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();

            }

            return mail;
        }

    }
}
