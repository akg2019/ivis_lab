﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Honeysuckle.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class VehicleCertificate
    {
        public int VehicleCertificateID { get; set; }
        public string EDIID { get; set; }
        public Company company { get; set; }
        public Policy policy { get; set; }
        public string CertificateNo { get; set; }

        [RegularExpression("[a-zA-Z0-9][a-zA-Z0-9-#&,:;/ ]*[a-zA-Z0-9]", ErrorMessage = "Incorrect Certificate-type format (only, letters, numbers and select symbols allowed) ")]
        public string CertificateType { get; set; }

        [RegularExpression("[a-zA-Z0-9][a-zA-Z0-9-#&,:;/ ]*[a-zA-Z0-9]*", ErrorMessage = "Incorrect Insured-code format (only, letters, numbers and select symbols allowed) ")]
        public string InsuredCode { get; set; }

        [RegularExpression("[0-9]+", ErrorMessage = "Incorrect ExtensionCode format (only numbers and select symbols allowed) ")]
        public int ExtensionCode { get; set; }

        public int UniqueNum { get; set; }

        [RegularExpression("[a-zA-Z0-9][a-zA-Z0-9-#&,:;/ ]*[a-zA-Z0-9]", ErrorMessage = "Incorrect usage format (only, letters, numbers and select symbols allowed) ")]
        public string Usage { get; set; }

        [RegularExpression("[a-zA-Z0-9][a-zA-Z0-9-#&,:;/ ]*[a-zA-Z0-9]", ErrorMessage = "Incorrect scheme format (only, letters, numbers and select symbols allowed) ")]
        public string Scheme { get; set; }

        public DateTime EffectiveDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime DateFirstPrinted { get; set; }
        public DateTime LastPrintedOn { get; set; }
        public Person LastPrintedBy { get; set; }
        public string last_printed_by_flat { get; set; }
        public string first_printed_by_flat { get; set; }
        public int printcount { get; set;  }
        public int printcountInsurer { get; set; }

        [RegularExpression("[0-9-,]+", ErrorMessage = "Incorrect Endorsement-number format (only numbers and select symbols allowed) ")]
        public string EndorsementNo { get; set; }

        [RegularExpression("[0-9]+", ErrorMessage = "Incorrect Printed Paper Number format (only numbers and select symbols allowed) ")]
        public string PrintedPaperNo { get; set; }

        public Vehicle Risk { get; set; }
        public DateTime SignedAt { get; set; }
        public Person SignedBy { get; set; }

        [RegularExpression("[0-9]+[0-9-,]*[0-9]*", ErrorMessage = "Incorrect MarksRegNo format (only numbers and select symbols allowed) ")]
        public string MarksRegNo { get; set; }

        public bool Cancelled { get; set; }
        public string CancelReason { get; set; }
        public Person CancelledBy { get; set; }
        public DateTime CancelledOn { get; set; }
        public TemplateWording wording { get; set; }
        public VehicleCoverNoteGenerated generated_cert { get; set; }
        public bool imported { get; set; }
        public bool approved { get; set; }
        public DateTime createdOn { get; set; }

        public VehicleCertificate() { }
        public VehicleCertificate(int VehicleCertificateID)
        {
            this.VehicleCertificateID = VehicleCertificateID;
        }
        public VehicleCertificate
            (
                int VehicleCertificateID,
                Company company,
                Policy policy,
                string CertificateType,
                string InsuredCode,
                int ExtensionCode,
                int UniqueNum,
                DateTime EffectiveDate,
                DateTime ExpiryDate,
                DateTime DateFirstPrinted,
                DateTime LastPrintedOn,
                string EndorsementNo,
                string PrintedPaperNo,
                Vehicle Risk,
                DateTime SignedAt,
                Person SignedBy,
                string MarksRegNo,
                bool Cancelled,
                Person CancelledBy,
                DateTime CancelledOn
            )

        {
            this.VehicleCertificateID = VehicleCertificateID;
            this.company = company;
            this.policy = policy;
            this.CertificateType = CertificateType;
            this.InsuredCode = InsuredCode;
            this.ExtensionCode = ExtensionCode;
            this.UniqueNum = UniqueNum;
            this.EffectiveDate = EffectiveDate;
            this.ExpiryDate = ExpiryDate;
            this.DateFirstPrinted = DateFirstPrinted;
            this.LastPrintedOn = LastPrintedOn;
            this.EndorsementNo = EndorsementNo;
            this.PrintedPaperNo = PrintedPaperNo;
            this.Risk = Risk;
            this.SignedAt = SignedAt;
            this.SignedBy = SignedBy;
            this.MarksRegNo = MarksRegNo;
            this.Cancelled = Cancelled;
            this.CancelledBy = CancelledBy;
            this.CancelledOn = CancelledOn;
        }

        public VehicleCertificate( Company company, Vehicle Risk, Policy policy)
            {
                this.company = company;
                this.Risk = Risk;
                this.policy = policy; 
            }

        public VehicleCertificate(Policy policy)
        {
            this.policy = policy;
        }

        public VehicleCertificate(int VehicleCertificateID, Company company, Policy policy, Vehicle Risk)
        {
            this.VehicleCertificateID = VehicleCertificateID;
            this.company = company;
            this.policy = policy;
            this.UniqueNum = UniqueNum;
            this.PrintedPaperNo = PrintedPaperNo;
            this.Risk = Risk;
            this.wording = wording;

        }

        public VehicleCertificate(int VehicleCertificateID, Company company, Policy policy, int UniqueNum, DateTime EffectiveDate, DateTime ExpiryDate)
        {
            this.VehicleCertificateID = VehicleCertificateID;
            this.company = company;
            this.policy = policy;
            this.UniqueNum = UniqueNum;
            this.EffectiveDate = EffectiveDate;
            this.ExpiryDate = ExpiryDate;
        }

        public VehicleCertificate(int VehicleCertificateID, Company company, Policy policy, Vehicle Risk,  DateTime EffectiveDate, DateTime ExpiryDate, string CertificateType)
        {
            this.VehicleCertificateID = VehicleCertificateID;
            this.company = company;
            this.policy = policy;
            this.Risk = Risk;
            this.EffectiveDate = EffectiveDate;
            this.ExpiryDate = ExpiryDate;
            this.CertificateType = CertificateType;
        }

        
    }


    public class VehicleCertificateModel
    {

        /// <summary>
        /// this function returns all certs in a certain date range
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<VehicleCertificate> CoverSearchCerts(DateTime start, DateTime end, User user)
        {
            List<VehicleCertificate> certs = new List<VehicleCertificate>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC CoverSearchCerts '" + start.ToString("yyyy-MM-dd HH:mm:ss") + "','" + end.ToString("yyyy-MM-dd HH:mm:ss") + "'," + user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    VehicleCertificate cert = new VehicleCertificate();
                    cert.generated_cert = new VehicleCoverNoteGenerated();
                    cert.Risk = new Vehicle();
                    cert.policy = new Policy();

                    cert.EffectiveDate = DateTime.Parse(reader["effective"].ToString());
                    cert.ExpiryDate = DateTime.Parse(reader["expiry"].ToString());

                    
                    cert.SignedAt = DateTime.Parse(reader["signon"].ToString());
                    cert.SignedBy = new Person(reader["signtrn"].ToString());

                    cert.Risk.ID = int.Parse(reader["risk_id"].ToString());
                    cert.Risk.chassisno = reader["chassis"].ToString();
                    cert.policy.ID = int.Parse(reader["polid"].ToString());

                    cert.Risk.AuthorizedDrivers = DriverModel.getAuthorizedDrivers(cert.Risk, cert.policy);
                    
                    cert.Cancelled = (bool)reader["cancelled"];
                    
                    #region optional data
                    if (DBNull.Value != reader["vin"]) cert.Risk.VIN = reader["vin"].ToString();
                    if (DBNull.Value != reader["ediid"]) cert.EDIID = reader["ediid"].ToString();
                    if (DBNull.Value != reader["certno"]) cert.CertificateNo = reader["certno"].ToString();
                    if (DBNull.Value != reader["firstprinted"]) cert.DateFirstPrinted = DateTime.Parse(reader["firstprinted"].ToString());
                    if (DBNull.Value != reader["lastprinted"]) cert.LastPrintedOn = DateTime.Parse(reader["lastprinted"].ToString());
                    if (DBNull.Value != reader["lastprintedbytrn"]) cert.LastPrintedBy = new Person(reader["lastprintedbytrn"].ToString());
                    if (DBNull.Value != reader["endorsementno"]) cert.EndorsementNo = reader["endorsementno"].ToString();
                    if (DBNull.Value != reader["printedpaperno"]) cert.PrintedPaperNo = reader["printedpaperno"].ToString();
                    if (DBNull.Value != reader["limits"]) cert.generated_cert.limitsofuse = reader["limits"].ToString();
                    if (DBNull.Value != reader["marksregno"]) cert.MarksRegNo = reader["marksregno"].ToString();
                    if (cert.Cancelled)
                    {
                        if (DBNull.Value != reader["cancelledtrn"]) cert.CancelledBy = new Person(reader["cancelledtrn"].ToString());
                        if (DBNull.Value != reader["cancelledreason"]) cert.CancelReason = reader["cancelledreason"].ToString();
                        if (DBNull.Value != reader["cancelledon"]) cert.CancelledOn = DateTime.Parse(reader["cancelledon"].ToString());
                    }
                    #endregion

                    certs.Add(cert);

                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return certs;
        }
        #region old
        /// <summary>
        /// This function searches the certificates table, given a particular search criteria
        /// </summary>
        /// <param name="search">Search criteria</param>
        /// <return>Risk certificate object lists - containing relevant Risk Certificate data, and matching the search criteria</returns>
        //public static List<VehicleCertificate> CertificateGenSearch(string search)
        //{
        //    List<VehicleCertificate> certs = new List<VehicleCertificate>();
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        string query = "EXEC CertificateGenSearch '" + Constants.CleanString(search) + "'";
        //        SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
        //        while (reader.Read())
        //        {
        //            certs.Add(new VehicleCertificate(int.Parse(reader["ID"].ToString()),
        //                                             new Company(reader["CompanyName"].ToString()),
        //                                             new Policy(int.Parse(reader["PID"].ToString()), reader["PolicyNumber"].ToString(), new PolicyCover(reader["cover"].ToString())),
        //                                             int.Parse(reader["unique"].ToString()),
        //                                             DateTime.Parse(reader["effective"].ToString()),
        //                                             DateTime.Parse(reader["expiry"].ToString())));
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return certs;
        //}
        #endregion
        /// <summary>
        /// This function generates the details about a Risk, in order to create its certificate
        /// </summary>
        /// <param name="vehicleId">Valid Risk Id in the system</param>
        /// <returns VehicleCertificate>Risk object - Filled with relevant Risk Certificate data for creating the certificate</returns>
        public static VehicleCertificate PullVehiclePolData(int vehicleId,int compID = 0)
        {
            VehicleCertificate cert = new VehicleCertificate();
            Vehicle veh = new Vehicle ();
            Person per = new Person ();
            Policy pol = new Policy();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC PullVehiclePolData " + vehicleId + "," + Constants.user.ID + "," + compID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if (DBNull.Value != reader["TRN"])
                        {
                            per.PersonID = int.Parse(reader["PeopleID"].ToString());
                            per.TRN = reader["TRN"].ToString();
                            if (DBNull.Value != reader["FName"]) per.fname = reader["FName"].ToString();
                            if (DBNull.Value != reader["LName"]) per.lname = reader["LName"].ToString();
                        }

                        veh.ID = vehicleId;
                        if (DBNull.Value != reader["VIN"]) veh.VIN = reader["VIN"].ToString();
                        veh.make = reader["Make"].ToString();
                        veh.VehicleModel = reader["Model"].ToString();
                        veh.mainDriver = new Driver(per);

                        pol.ID = int.Parse(reader["PolicyID"].ToString());
                        pol.insuredby = new Company(int.Parse(reader["CompanyID"].ToString()));
                        pol.policyCover = new PolicyCover(reader["Cover"].ToString());
                        pol.policyCover.ID = int.Parse(reader["ID"].ToString());
                        pol.policyNumber = reader["Policyno"].ToString();

                        veh.AuthorizedDrivers = VehicleModel.GetDriverList(vehicleId, pol.ID, 1);
                        veh.ExceptedDrivers = VehicleModel.GetDriverList(vehicleId, pol.ID, 2);
                        veh.ExcludedDrivers = VehicleModel.GetDriverList(vehicleId, pol.ID, 3);



                        cert = new VehicleCertificate(new Company(int.Parse(reader["CompanyID"].ToString())), veh, pol);

                    }
                }

                reader.Close();
                sql.DisconnectSQL();
            }
            return cert;
        }

        /// <summary>
        /// This function creates a new Risk certificate
        /// </summary>
        /// <param name="vehicleCert"> Risk Certificate object , storing all the certificate data</param>
        /// <returns>Integer- certificate Id </returns>
        public static int CreateCertificate(VehicleCertificate vehicleCert, int uid, bool import = false)
        {
            int certID = 0;
            int wid = 0;
            string compShort = CompanyModel.GetShortenedCode(vehicleCert.company.CompanyID);
            TemplateWording wording = new TemplateWording();
            int uniqueNumber = GenerateUniqueNum(vehicleCert.company.CompanyID);

            if (vehicleCert.PrintedPaperNo == null) vehicleCert.PrintedPaperNo = "0";
           
            if (vehicleCert.wording != null) wid = vehicleCert.wording.ID;
            wording = TemplateWordingModel.GetWording(wid);
            if (vehicleCert.CertificateNo == null || vehicleCert.CertificateNo == "") vehicleCert.CertificateNo = compShort  + uniqueNumber.ToString();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC CreateVehicleCertificate " + vehicleCert.Risk.ID + ","
                                                                + uid + ","
                                                                + vehicleCert.company.CompanyID + ",'"
                                                                + vehicleCert.policy.startDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                                + Constants.CleanString(vehicleCert.PrintedPaperNo) + "',"
                                                                + vehicleCert.policy.ID + ","
                                                                + wid + ",'"
                                                                + Constants.CleanString(vehicleCert.CertificateNo) + "',"
                                                                + uniqueNumber + ",'"
                                                                + Constants.CleanString(vehicleCert.EndorsementNo) + "','"
                                                                + vehicleCert.DateFirstPrinted.ToString("yyyy-MM-dd") + "','"
                                                                + vehicleCert.LastPrintedOn.ToString("yyyy-MM-dd") + "','"
                                                                + Constants.CleanString(vehicleCert.last_printed_by_flat) + "','"
                                                                + Constants.CleanString(vehicleCert.MarksRegNo) + "','"
                                                                + Constants.CleanString(vehicleCert.EDIID) + "'";
                if (import) query += ",'true'";

                SqlDataReader reader = sql.QuerySQL(query, "create", "vehiclecertificate", vehicleCert);

                while (reader.Read())
                {
                    if ((bool)reader["result"]) certID = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return certID;
        }
        #region old
        //public static List<VehicleCertificate> CreateManyCertificates(List<VehicleCertificate> certs)
        //{
        //    return CreateManyCertificates(certs, Constants.user.ID);
        //}
        #endregion
        /// <summary>
        /// this funcion adds many certificates to the system. it is also ONLY TO BE USED BY THE IMPORT MODEL/WEB SERVICE
        /// </summary>
        /// <param name="certs"></param>
        /// <param name="uid"></param>
        /// <param name="insuredid"></param>
        /// <returns></returns>
        public static List<VehicleCertificate> CreateManyCertificates(List<VehicleCertificate> certs, int uid, int insuredid = 0)
        {
            //foreach (VehicleCertificate cert in certs)
            int id = 0;
            for (int i = 0; i < certs.Count(); i++)
            {
                if (insuredid != 0) certs[i].company = new Company(insuredid);
                id = CreateCertificate(certs[i], uid, true);
                if (id != 0)
                {
                    certs[i].VehicleCertificateID = id;
                    ApproveCert(id, uid);
                    certs[i].policy = PolicyModel.getPolicyFromID(certs[i].policy.ID);
                    
                    //if (certs[i].generated_cert != null) VehicleCoverNoteGeneratedModel.GenerateVehicleCert(certs[i].generated_cert, id, uid);
                   VehicleCoverNoteGenerated generatedCert = new VehicleCoverNoteGenerated (certs[i].Risk.VehicleYear.ToString(), certs[i].Risk.make, 
                                                             certs[i].Risk.VehicleRegNumber, certs[i].Risk.extension, "", "", 
                                                             certs[i].Risk.make +" "+ certs[i].Risk.VehicleModel + " " + certs[i].Risk.modelType, 
                                                             certs[i].policy.policyCover.cover, certs[i].Risk.modelType, certs[i].Risk.VehicleModel, 
                                                             certs[i].generated_cert.authorizedwording, "",certs[i].generated_cert.limitsofuse, certs[i].Risk.usage.usage, certs[i].policy.policyNumber,
                                                             certs[i].policy.mailingAddress.roadnumber + " " + certs[i].policy.mailingAddress.road.RoadName + " " + certs[i].policy.mailingAddress.roadtype.RoadTypeName
                                                             + ", " + certs[i].policy.mailingAddress.city.CityName + ", " + certs[i].policy.mailingAddress.parish.parish + ", " + certs[i].policy.mailingAddress.country.CountryName
                                                             , "", "", certs[i].policy.startDateTime.ToString("H:mm tt"), certs[i].policy.endDateTime.ToString("H:mm tt"),
                                                             certs[i].policy.startDateTime.ToString("MMMM dd, yyyy"),certs[i].policy.endDateTime.ToString("MMMM dd, yyyy"),
                                                             certs[i].EndorsementNo, certs[i].Risk.bodyType, certs[i].Risk.seating.ToString(), certs[i].Risk.chassisno,
                                                             certs[i].CertificateType, certs[i].CertificateNo, "", certs[i].Risk.engineNo, certs[i].Risk.HPCCUnitType);

                   VehicleCoverNoteGeneratedModel.GenerateVehicleCert(generatedCert, id, uid);

                }
                else certs.RemoveAt(i);
            }
            return certs;
        }


        /// <summary>
        /// This function edits a Risk certificate
        /// </summary>
        /// <param name="vehicleCert"> Risk Certificate object , storing all the certificate data to be edited</param>
        public static int EditCertificate(VehicleCertificate vehicleCert)
        {
           int result = 0;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC UpdateVehicleCertificate " + vehicleCert.VehicleCertificateID + ","
                                                                + vehicleCert.Risk.ID + ","
                                                                + Constants.user.ID + ","
                                                                + vehicleCert.wording.ID;

                if (vehicleCert.PrintedPaperNo != null) query += "," +  Constants.CleanString(vehicleCert.PrintedPaperNo);
                                                                

                SqlDataReader reader = sql.QuerySQL(query, "update", "vehiclecertificate", vehicleCert);

                while (reader.Read())
                {
                    result = int.Parse(reader["result"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to approve a Risk certificate in the system
        /// </summary>
        /// <param name="VehCertId">Risk Certificate Id</param>
        public static void ApproveCert(int VehCertId)
        {
            ApproveCert(VehCertId, Constants.user.ID);
        }

        /// <summary>
        /// This function is used to approve a Risk certificate in the system
        /// </summary>
        /// <param name="VehCertId">Risk Certificate Id</param>
        /// <param name="uid">User Id who's approving the certificate</param>
        private static void ApproveCert(int VehCertId, int uid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC ApproveCertificate " + VehCertId + "," + uid;
                SqlDataReader reader = sql.QuerySQL(query, "update", "vehiclecertificate", new VehicleCertificate(VehCertId), uid, null, false);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function deletes a Risk certificate from the system
        /// </summary>
        /// <param name="CertificateId">Risk Certificate Id</param>
        public static void DeleteCertificate(int CertificateId)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DeleteVehicleCertificate " + CertificateId + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "vehiclecertificate", new VehicleCertificate(CertificateId));
                reader.Close();
                sql.DisconnectSQL();
            }

        }

        /// <summary>
        /// Getting a Risk's active certificate, given the Risk object
        /// </summary>
        /// <param name="Risk">Risk object with Risk's information</param>
        /// <returns>The certificate information</returns>
        public static VehicleCertificate GetThisActiveCertificate(int vehicleId, string start, string end, int polId = 0)
        {
            VehicleCertificate cert = new VehicleCertificate();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetThisActiveCertificate '" + DateTime.Parse(start).ToString("yyyy-MM-dd HH:mm:ss") + "','" 
                                                                 + DateTime.Parse(end).ToString("yyyy-MM-dd HH:mm:ss") + "'," 
                                                                 + vehicleId +","
                                                                 + polId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    cert = new VehicleCertificate(int.Parse(reader["CertID"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return cert;
        }

        /// <summary>
        /// Getting a Risk's inactive certificate, given the Risk object
        /// </summary>
        /// <param name="Risk">Risk object with Risk's information</param>
        /// <returns>The certificate information</returns>
        public static VehicleCertificate GetThisInactiveCertificate(int vehicleId, int polId)
        {
            VehicleCertificate cert = new VehicleCertificate();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetInactiveCertificate " + polId + "," + vehicleId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    cert = new VehicleCertificate(int.Parse(reader["CertID"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return cert;
        }

        /// <summary>
        /// Retrieving a certificate's data, given the certificate ID
        /// </summary>
        /// <param name="vehicleCert">Risk Certificate object- containing certificate data</param>
        /// <returns>Risk certificate object with certificate information</returns>
        public static VehicleCertificate GetThisCertificate(VehicleCertificate vehicleCert)
        {
            VehicleCertificate cert = new VehicleCertificate();
            Vehicle veh = new Vehicle();
            Person per = new Person();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                SqlDataReader reader = sql.QuerySQL("EXEC GetThisCertificate " + vehicleCert.VehicleCertificateID, "read", "vehiclecertificate", new VehicleCertificate());

                while (reader.Read())
                {
                    if (DBNull.Value != reader["PeopleID"]) per.PersonID = int.Parse(reader["PeopleID"].ToString());
                    if (DBNull.Value != reader["FName"]) per.fname = reader["FName"].ToString();
                    if (DBNull.Value != reader["LName"]) per.lname = reader["LName"].ToString();
                    if (DBNull.Value != reader["TRN"]) per.TRN = reader["TRN"].ToString();

                    cert = new VehicleCertificate(int.Parse(reader["ID"].ToString()),
                                                  new Company(int.Parse(reader["CompanyID"].ToString()), reader["CompanyName"].ToString()),
                                                  new Policy(int.Parse(reader["PolicyID"].ToString()), new PolicyCover(reader["Cover"].ToString()), reader["Policyno"].ToString()),
                                                  new Vehicle(int.Parse(reader["RiskItemNo"].ToString())));

                    cert.printcount = int.Parse(reader["PrintCount"].ToString());
                    cert.printcountInsurer = int.Parse(reader["PrintCountInsurer"].ToString());
                    cert.Cancelled = (bool)reader["Cancelled"];
                    cert.EffectiveDate = DateTime.Parse(reader["StartDateTime"].ToString());
                    cert.ExpiryDate = DateTime.Parse(reader["EndDateTime"].ToString());
                    cert.policy.imported = (bool)reader["imported"];

                    if (DBNull.Value != reader["CertificateNo"]) cert.CertificateNo = reader["CertificateNo"].ToString();
                    if (DBNull.Value != reader["PrintedPaperNo"]) cert.PrintedPaperNo = reader["PrintedPaperNo"].ToString();
                    if (DBNull.Value != reader["LastPrintedByFlat"]) cert.last_printed_by_flat = reader["LastPrintedByFlat"].ToString();
                    if (DBNull.Value != reader["FirstPrintedByFlat"]) cert.first_printed_by_flat = reader["FirstPrintedByFlat"].ToString();


                    if (DBNull.Value != reader["VIN"]) cert.Risk.VIN = reader["VIN"].ToString();
                    if (DBNull.Value != reader["ChassisNo"]) cert.Risk.chassisno = reader["ChassisNo"].ToString();
                    if (DBNull.Value != reader["Make"]) cert.Risk.make = reader["Make"].ToString();
                    if (DBNull.Value != reader["Model"]) cert.Risk.VehicleModel = reader["Model"].ToString();
                    if (DBNull.Value != reader["VehicleYear"]) cert.Risk.VehicleYear = int.Parse(reader["VehicleYear"].ToString());
                    if (DBNull.Value != reader["usage"]) cert.Risk.usage = new Usage(reader["usage"].ToString());
                    if (DBNull.Value != reader["VehicleRegistrationNo"]) cert.Risk.VehicleRegNumber = reader["VehicleRegistrationNo"].ToString();
                    if (DBNull.Value != reader["EngineNo"]) cert.Risk.engineNo = reader["EngineNo"].ToString();
                    if (DBNull.Value != reader["Seating"]) cert.Risk.seating = int.Parse(reader["Seating"].ToString());
                    if (DBNull.Value != reader["HPCCUnitType"]) cert.Risk.HPCCUnitType = reader["HPCCUnitType"].ToString();

                    //if (DBNull.Value != reader["WordingTemplate"]) cert.wording = new TemplateWording(int.Parse(reader["WordingTemplate"].ToString()));
                    if (DBNull.Value != reader["WordingTemplate"]) cert.wording = TemplateWordingModel.GetWording(int.Parse(reader["WordingTemplate"].ToString()));
                    if (DBNull.Value != reader["ReferenceNo"]) cert.Risk.referenceNo = reader["ReferenceNo"].ToString();
                    if (DBNull.Value != reader["approved"]) cert.approved = (bool)reader["approved"];
                    cert.Risk.mainDriver = new Driver(per);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return cert;
        }

        /// <summary>
        /// This function is used to generate a unique number -to be added to the Risk certificate
        /// </summary>
        /// <returns>integer- unique, generated number</returns>
        private static int GenerateUniqueNum(int compID)
        {
            Random random = new Random();
            int UniqueNum = random.Next(1, 1000000);

            bool result = TestUniqueNo(UniqueNum, compID);

            while (result == true)
            {
                UniqueNum = random.Next(1, 1000000);
                result = TestUniqueNo(UniqueNum, compID);

            }
            return UniqueNum;
        }

        /// <summary>
        /// This function is used to test if a particular printed paper number is already in the  system
        /// </summary>
        /// <param name="ppn">Printed Paper Number to be tested</param>
        /// <returns>boolean value</returns>
        public static bool TestPrintedPaperNo(int ppn)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC TestPrintedPaperNo " + ppn, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to test if a particular 'unique number' is already in the  system (apart of a Risk-certificate)
        /// </summary>
        /// <param name="UniqueNumber">Unique Number to be tested</param>
        /// <returns>boolean value</returns>
        private static bool TestUniqueNo(int UniqueNumber, int compID)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC testUniqueNumber " + compID + "," + UniqueNumber, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to cancel a particular Risk certificate
        /// </summary>
        /// <param name="CertId">Certificate Id- of certificate to be cancelled</param>
        /// <returns>boolean value</returns>
        public static bool CancelCertificate (int CertId = 0)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC CancelCertificate " + CertId + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "vehiclecertificate", new VehicleCertificate(CertId));

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;

        }

        /// <summary>
        /// This function is used to test if a certificate is active
        /// </summary>
        /// <param name="CertId">Certificate Id- of certificate to be tested</param>
        /// <returns>boolean value</returns>
        public static bool IsCertitificateActive(int CertId)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC IsCertificateActive " + CertId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;

        }

        /// <summary>
        /// This function is used to test if a certificate is cancelled
        /// </summary>
        /// <param name="CertId">Certificate Id- of certificate to be tested</param>
        /// <returns>boolean value</returns>
        public static bool IsCertitificateCancelled(int CertId)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC IsCertificateCancelled " + CertId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;

        }

        /// <summary>
        /// This function is used to test if a Risk is under an active certificate
        /// </summary>
        /// <param name="vehId">Risk Id- of Risk to checked </param>
        /// <returns>boolean value</returns>
        public static bool IsVehicleUnderActiveCert(int vehId, DateTime startDate, DateTime endDate, int polId =0)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC IsUnderactiveCertificate '" + startDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + endDate.ToString("yyyy-MM-dd HH:mm:ss") + "'," + vehId +","+polId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to test if a Risk is under an inactive certificate (under a particular policy)
        /// </summary>
        /// <param name="vehId">Risk Id- of Risk to checked </param>
        /// <returns>boolean value</returns>
        public static bool IsPolicyCertMade(int vehID, int polID)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsInactiveCertificateMade " + polID + "," + vehID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }


        /// <summary>
        /// This function is used to update the print count of a particulat certificate
        /// </summary>
        /// <param name="VehCertID">Risk Certifivate Id</param>
        public static void Print (int VehCertID)
        {
            sql sql = new sql();
            string LastPrintedByName = Constants.user.employee.person.lname + ", " + Constants.user.employee.person.fname;

            if (sql.ConnectSQL())
            {

                string query = "EXEC PrintCertificate " + VehCertID + "," + Constants.user.ID + ",'" + Constants.CleanString(LastPrintedByName)+ "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
                reader.Close();
                sql.DisconnectSQL();
            }
        }
        #region old
        /// <summary>
        /// this function returns a condensed form of the certificate data
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cmp"></param>
        /// <returns></returns>
        //private static VehicleCertificate GetShortCertificate(int id, int cmp = 0)
        //{
        //    VehicleCertificate vc = new VehicleCertificate(id);
        //    sql sql = new sql();
        //    if (sql.ConnectSQL() && id != 0)
        //    {
        //        string query = null;
        //        if (cmp != 0) query = "EXEC GetShortCertificate " + id + "," + cmp;
        //        else query = "EXEC GetShortCertificate " + id;
        //        SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
        //        while (reader.Read())
        //        {
        //            vc = new VehicleCertificate(id,
        //                                        new Company(reader["CompanyName"].ToString()),
        //                                        new Policy(int.Parse(reader["PolicyID"].ToString())),
        //                                        new Vehicle(reader["VehicleRegistrationNo"].ToString(), int.Parse(reader["VehicleID"].ToString())),
        //                                        DateTime.Parse(reader["EffectiveDate"].ToString()),
        //                                        DateTime.Parse(reader["ExpiryDate"].ToString()),
        //                                        reader["CertificateNo"].ToString());
        //            vc.imported = (bool)reader["imported"];
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return vc;
        //}

        /// <summary>
        /// this function returns a condensed for of the certificates in the system
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="cmp"></param>
        /// <returns></returns>
        //public static List<VehicleCertificate> GetShortCertificate(List<int> ids, int cmp = 0)
        //{
        //    List<VehicleCertificate> VehicleCertificates = new List<VehicleCertificate>();
        //    foreach (int i in ids)
        //    {
        //        VehicleCertificate v = GetShortCertificate(i, cmp);
        //        if (v.company != null) //If the company is null, then no data was pulled for that cert
        //        VehicleCertificates.Add(v);
        //    }

        //    return VehicleCertificates;
        //}
        #endregion
        /// <summary>
        /// this function determines if the certificate exsits in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool IsCertificateReal(int id)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC IsCertificateReal " + id, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to request a print for a particular certificate
        /// </summary>
        /// <param name="VehCertID">Risk Certifivate Id</param>
        public static bool PrintRequest(int VehCertID)
        {
            bool result = false;
            int compId;

            if (Constants.user.newRoleMode) compId = Constants.user.tempUserGroup.company.CompanyID;
            else compId = CompanyModel.GetMyCompany(Constants.user).CompanyID;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXECUTE PrintRequest " + VehCertID + ",'" + "Certificate" + "'," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());

                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to enable the printing for a particular certificate
        /// </summary>
        /// <param name="VehCertID">Risk Certifivate Id</param>
        public static bool EnablePrint(int VehCertID)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXECUTE MakePrintable '" + "Certificate" + "'," + VehCertID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function returns the amount of certs that are effective in a specified
        /// month and year for a specified company
        /// </summary>
        /// <param name="company">company</param>
        /// <param name="month">month</param>
        /// <param name="year">year</param>
        /// <returns></returns>
        public static int CertificateCountEffective(Company company, string month, string year)
        {
            int output = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC CertificateCountEffective " + company.CompanyID + ",'" + Constants.CleanString(month) + "','" + Constants.CleanString(year) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    output = int.Parse(reader["count"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return output;
        }

        /// <summary>
        /// This function is used to determine if the mass approval button should be shown on the policy page
        /// </summary>
        /// <param name="policy">Policy Model </param>
        /// <returns>bool</returns>
        public static bool MassApproveCertButton(Policy policy =null)
        {
            bool show = false;

              if (policy != null)
              foreach (Vehicle vehicle in policy.vehicles)
                {
                    if (!VehicleCertificateModel.IsVehicleUnderActiveCert(vehicle.ID, policy.startDateTime, policy.endDateTime, policy.ID) && VehicleCertificateModel.IsPolicyCertMade(vehicle.ID, policy.ID))
                    {
                        show = true;
                        return show;
                    }
                }
                return show;
        }

        /// <summary>
        /// This function is used to determine if the mass create cert button should be shown on the policy page
        /// </summary>
        /// <param name="policy">Policy Model </param>
        /// <returns>bool</returns>
        public static bool MassCreateCertButton(Policy policy = null)
        {
            bool show = false;

              if (policy != null)
                foreach (Vehicle vehicle in policy.vehicles)
                {
                    if (!VehicleCertificateModel.IsVehicleUnderActiveCert(vehicle.ID, policy.startDateTime, policy.endDateTime, policy.ID) && !VehicleCertificateModel.IsPolicyCertMade(vehicle.ID, policy.ID))
                    {
                        //There only needs to be a min of one vehicle that can potentially have a cert, to show the button
                        show = true;
                        return show;
                    }
                }
              return show;
         }

        /// <summary>
        /// This function is used to determine if the mass print cert button should be shown on the policy page
        /// </summary>
        /// <param name="policy">Policy Model </param>
        /// <returns>bool</returns>
        public static bool MassPrintCertButton(Policy policy = null)
        {
            bool show = false;

            if(policy != null)
            foreach (Vehicle vehicle in policy.vehicles)
            {
                if (VehicleCertificateModel.IsVehicleUnderActiveCert(vehicle.ID, policy.startDateTime, policy.endDateTime, policy.ID))
                {
                    //There only needs to be a min of one vehicle that can potentially have a cert, to show the button
                    show = true;
                    return show;
                }
            }
            return show;
        }

        /// <summary>
        /// Get certificate ID using certificate number
        /// </summary>
        /// <param name="certNo"></param>
        /// <returns></returns>
        public static VehicleCertificate GetVehicleCertificateByCertificateNo(string certNo)
        {
            VehicleCertificate cert = new VehicleCertificate();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetVehicleCertificateByCertificateNo '" + certNo + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehiclecertificate", new VehicleCertificate());
                while (reader.Read())
                {
                    cert = new VehicleCertificate(int.Parse(reader["VehicleCertificateID"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return cert;
        }

        public static List<VehicleCertificate> GetCompanyCertificates(int compID, DateTime start, DateTime end, string typeOfDate = null)
        {
            List<VehicleCertificate> certs = new List<VehicleCertificate>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                if (typeOfDate == null) typeOfDate = "created";
                string query = "EXEC GetCompanyCertificates " + compID + ",'" + typeOfDate + "'";
                if ((start == DateTime.Parse("1969-12-31 00:00:00") || (start == DateTime.Parse("1969-12-31 19:00:00")) && (end == DateTime.Parse("1969-12-31 00:00:00") || end == DateTime.Parse("1969-12-31 19:00:00"))))
                {
                    query += ",'" + null + "','" + null + "'";
                }
                else
                    query += ",'" + start.ToString("yyyy-MM-dd HH:mm:ss") + "','" + end.ToString("yyyy-MM-dd HH:mm:ss") + "'";

                SqlDataReader reader = sql.QuerySQL(query);
                VehicleCertificate vc = new VehicleCertificate();
                while (reader.Read())
                {
                    vc.VehicleCertificateID = int.Parse(reader["VehicleCertificateID"].ToString());
                    vc.Risk = new Vehicle(int.Parse(reader["vehID"].ToString()));
                    vc.policy = new Policy(int.Parse(reader["PolicyID"].ToString()));
                    vc.CertificateNo = reader["CertificateNo"].ToString();
                    vc.EffectiveDate = DateTime.Parse(reader["EffectiveDate"].ToString());
                    vc.Risk.VehicleRegNumber = reader["VehicleRegistrationNo"].ToString();
                    vc.company = CompanyModel.GetCompany(int.Parse(reader["CompanyID"].ToString()));
                    vc.imported = (bool)reader["imported"];

                    certs.Add(vc);
                    vc = new VehicleCertificate();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return certs;
        }

        public static void MarkCertificateImported(VehicleCertificate vc)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC MarkCertificateImported " + vc.VehicleCertificateID;
                SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

    }
}
