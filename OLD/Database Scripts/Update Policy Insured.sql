Go
alter table PolicyInsured add PolicyNo nvarchar(50)
GO
alter table PolicyInsured add PolicyHolder nvarchar(100)
GO
alter table PolicyInsured add TRN nvarchar(20)
GO

create TABLE [Policy Insured Dump] (
	[Company_ID] [int] NULL,
	[National_ID] [nvarchar](50) NULL,
	[Policy_No] [nvarchar](50) NULL,
	[Full_name] [nvarchar](100) NULL,
	[Is_Main_Insured] [nvarchar](10) NULL
)

GO

/****** Object:  StoredProcedure [dbo].[linkToSource]    Script Date: 19/4/2019 10:49:57 AM ******/
CREATE procedure linkToInsured @fileName nvarchar(50)

AS 

declare @folder nvarchar(50)
declare @location nvarchar(50)
declare @sql nvarchar(100)

select @folder = 'c:/InsuredDumps/'
select @location = @folder + @fileName + '.txt'

drop table #insureds

select * into #insureds from PolicyInsured

delete from #insureds

alter table #insureds
alter column PolicyId int null

delete from [Policy Insured Dump]

set @sql = N'BULK INSERT [Policy Insured Dump] FROM ''' + @location + ''''  --N''' WITH (ROWTERMINATOR = \n)'
exec(@sql)

insert into #insureds (InsurerId,policyNo,TRN,PolicyHolder,CreatedOn,CreatedBy,LastModifiedBy) select Company_Id,Policy_no,National_ID,Full_name,GETDATE(),1,1 from [Policy Insured Dump]

delete from PolicyInsured where InsurerId in (select insurerid from #insureds)

drop table #tempPolicy

select p.* into #tempPolicy from Policies p join #insureds ins on p.Policyno = ins.PolicyNo and ins.InsurerId = p.CompanyID

drop table #tempPeopleIds

select distinct p.PeopleID into #tempPeopleIds from ( select * from People where trn like '%[^0-9]%') p join [Policy Insured Dump] pid on p.trn = pid.National_ID --WHERE p.CompanyId in (select Company_ID from [Policy Insured Dump])

insert into #tempPeopleIds(PeopleID) select distinct p.PeopleID from ( select * from People where trn not like '%[^0-9]%') p join (select * from [Policy Insured Dump] where National_ID not like  '%[^0-9]%') pid on Cast(p.TRN as bigint) = Cast(pid.National_ID as bigint) --WHERE p.CompanyId in (select Company_ID from [Policy Insured Dump])

drop table #peopleIds

select distinct PeopleId into #peopleIds from #tempPeopleIds 

drop table #tempPeople

select * into #tempPeople from People where PeopleID in (select PeopleID from #peopleIds)

drop table #tempCompanyIds

select distinct CompanyID into #tempCompanyIds from Company c join [Policy Insured Dump] pid on c.TRN = pid.National_ID WHERE CompanyInsurer in (select Company_ID from [Policy Insured Dump])  

insert into #tempCompanyIds(CompanyID) select distinct CompanyID from ( select * from Company where trn not like '%[^0-9]%') c join (select * from [Policy Insured Dump] where National_ID not like  '%[^0-9]%') pid on CAST(c.TRN as bigint) =  cast(pid.National_ID as bigint) WHERE CompanyInsurer in (select Company_ID from [Policy Insured Dump])  

drop table #companyIds

select distinct CompanyId into #companyIds from #tempCompanyIds 

drop table #tempCompany
select * into #tempCompany  from Company where CompanyID in (select CompanyId from #companyIds) 

update #insureds set PolicyID = pol.PolicyID, CompanyID = c.CompanyId, PersonID = peo.PeopleID  
from #insureds ins join  #tempPolicy pol on pol.Policyno = ins.PolicyNo AND ins.InsurerId = pol.CompanyID
left join #tempPeople peo on peo.TRN = ins.TRN
left join #tempCompany c on c.TRN = ins.TRN

insert into PolicyInsured (PolicyID, CompanyID, PersonID, PolicyHolder, PolicyNo, CreatedOn, CreatedBy,LastModifiedBy,InsurerId,IntermediaryId,TRN) select policyId,CompanyId,personId,PolicyHolder,PolicyNo,CreatedOn,CreatedBy,LastModifiedBy,InsurerId,IntermediaryId,TRN from #insureds where PolicyID is not null AND (PersonID is not null OR CompanyID is not null)

delete from [Policy Insured Dump]
drop table #insureds
drop table #tempPolicy
drop table #tempPeople
drop table #tempPeopleIds
drop table #peopleIds
drop table #tempCompany
drop table #tempCompanyIds
drop table #companyIds




