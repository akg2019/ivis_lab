/* =============================================
-- Author: Randolph McClean
-- Email: randolph@particularpresence.com
-- Created: 13/04/2019
-- Description:	Adds companyId and intermediaryId
as well as their respetive foriegn key references
to the Company table
-- ============================================= */

--PolicyInsured
GO
alter table PolicyInsured ADD IntermediaryId int null
GO
ALTER TABLE PolicyInsured
ADD CONSTRAINT FK_InsuredIntermediary
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO
alter table PolicyInsured ADD InsurerId int null
GO
ALTER TABLE PolicyInsured
ADD CONSTRAINT FK_InsuredInsurer
FOREIGN KEY (InsurerId) REFERENCES Company(CompanyId);
GO
update PolicyInsured set insurerId = pol.CompanyID, IntermediaryId = pol.CompanyCreatedBy from PolicyInsured polI join Policies pol  on polI.PolicyID = pol.PolicyID  
GO
--AuthorizedDrivers
GO
alter table AuthorizedDrivers ADD CompanyId int null 
Go
ALTER TABLE AuthorizedDrivers
ADD CONSTRAINT FK_AuthorizedDriverCompany
FOREIGN KEY (CompanyID) REFERENCES Company(CompanyId);
GO
alter table AuthorizedDrivers ADD IntermediaryId int null
GO
ALTER TABLE AuthorizedDrivers
ADD CONSTRAINT FK_AuthorizedDriverIntermediary
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO
update AuthorizedDrivers set companyId =  p.CompanyID, intermediaryId = p.CompanyCreatedBy from AuthorizedDrivers ad join Policies p  on ad.PolicyID = p.PolicyID 
--Company
GO
alter table Company ADD CompanyInsurer int null
GO
ALTER TABLE Company
ADD CONSTRAINT FK_CompanyInsurer
FOREIGN KEY (CompanyInsurer) REFERENCES Company(CompanyId);
GO
alter table Company ADD IntermediaryId int null
GO
ALTER TABLE Company
ADD CONSTRAINT FK_CompanyIntermediary
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO

update Company set CompanyInsurer = polI.InsurerId, IntermediaryId = polI.intermediaryId from PolicyInsured polI join Company c on c.CompanyID = polI.CompanyID

GO
--People
GO
alter table People add IntermediaryId int null
GO
ALTER TABLE People
ADD CONSTRAINT FK_PeopleIntermediary
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO
alter table People add CompanyId int null
GO
ALTER TABLE People
ADD CONSTRAINT FK_PeopleCompany
FOREIGN KEY (CompanyId) REFERENCES Company(CompanyId);
GO
update People set companyId = polI.companyId, intermediaryId = polI.intermediaryId from People p join PolicyInsured polI on polI.PersonID = p.PeopleID;
GO
--PeopleAddress
GO
alter table PeopleAddress add IntermediaryId int null
GO
ALTER TABLE PeopleAddress
ADD CONSTRAINT FK_PeopleAddressIntermediary
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO
alter table PeopleAddress add CompanyId int null
GO
ALTER TABLE PeopleAddress
ADD CONSTRAINT FK_PeopleAddressCompany
FOREIGN KEY (CompanyId) REFERENCES Company(CompanyId);
GO
update PeopleAddress set intermediaryId = p.intermediaryId, companyId = p.companyId from People p join PeopleAddress pa on pa.PersonID = p.PeopleID;
GO
--PeopleToEmails
GO
alter table PeopleToEmails add IntermediaryId int null
GO
ALTER TABLE PeopleToEmails
ADD CONSTRAINT FK_PeopleEmailIntermediary
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO
alter table PeopleToEmails add CompanyId int null
GO
ALTER TABLE PeopleToEmails
ADD CONSTRAINT FK_PeopleToEmailsCompany
FOREIGN KEY (CompanyId) REFERENCES Company(CompanyId);
GO
update PeopleToEmails set intermediaryId = p.intermediaryId, companyId = p.companyId from People p join PeopleToEmails pte on pte.PeopleID = p.PeopleID;
GO
--PeopleToPhones
GO
alter table PeopleToPhones add IntermediaryId int null
GO
ALTER TABLE PeopleToPhones
ADD CONSTRAINT FK_PeoplePhoneIntermediary
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO
alter table PeopleToPhones add CompanyId int null
GO
ALTER TABLE PeopleToPhones
ADD CONSTRAINT FK_PeoplePhoneCompany
FOREIGN KEY (CompanyId) REFERENCES Company(CompanyId);
GO

update PeopleAddress set intermediaryId = p.intermediaryId, companyId = p.companyId from People p join PeopleToPhones ptp on ptp.PeopleID = p.PeopleID;

--Drivers
GO
alter table Drivers add CompanyId int null
GO
ALTER TABLE Drivers
ADD CONSTRAINT FK_DriverCompany
FOREIGN KEY (CompanyId) REFERENCES Company(CompanyId);
GO
alter table Drivers add IntermediaryId int null
GO
ALTER TABLE Drivers
ADD CONSTRAINT FK_DriverIntermediary
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO
update drivers set companyId = p.companyId, intermediaryId = p.intermediaryId from Drivers d join People p on p.PeopleID = d.PersonID join PolicyInsured polI on polI.PersonID = p.PeopleID
GO
--ExceptedDrivers
GO
alter table ExceptedDrivers add CompanyId int null
GO
ALTER TABLE ExceptedDrivers
ADD CONSTRAINT FK_ExceptedDriversCompany
FOREIGN KEY (CompanyId) REFERENCES Company(CompanyId);
GO
alter table ExceptedDrivers add IntermediaryId int null
GO
ALTER TABLE ExceptedDrivers
ADD CONSTRAINT FK_ExceptedDriversIntermediary
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO
update ExceptedDrivers set companyId = d.companyId, intermediaryId = d.intermediaryId from ExceptedDrivers ed join Drivers d on d.DriverID = ed.DriverID
GO
--ExcludedDrivers
GO
alter table ExcludedDrivers add CompanyId int null
GO
ALTER TABLE ExcludedDrivers
ADD CONSTRAINT FK_ExcludedDriversCompany
FOREIGN KEY (CompanyId) REFERENCES Company(CompanyId);
GO
alter table ExcludedDrivers add IntermediaryId int null
GO
ALTER TABLE ExcludedDrivers
ADD CONSTRAINT FK_ExcludedDriversIntermediary
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO
update ExcludedDrivers set intermediaryId = d.intermediaryId, companyId = d.companyId from ExcludedDrivers ed join Drivers d on d.DriverID = ed.DriverID
GO
--VehicleCertificates
GO
alter table VehicleCertificates add IntermediaryId int null
GO
ALTER TABLE VehicleCertificates
ADD CONSTRAINT FK_CertificateIntermediary
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO
update VehicleCertificates set CompanyID = pol.companyId, intermediaryId = pol.CompanyCreatedBy from VehicleCertificates vc join Policies pol on pol.PolicyID = vc.PolicyID
GO
--VehicleCoverNote
GO
alter table VehicleCoverNote add IntermediaryId int null
GO
ALTER TABLE VehicleCoverNote
ADD CONSTRAINT FK_CoverNoteIntermediary
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO
--There were some cover notes which were attached to deleted policies
update VehicleCoverNote set CompanyID = pol.companyId, intermediaryId = pol.CompanyCreatedBy from VehicleCoverNote vcn join Policies pol on pol.PolicyID = vcn.PolicyID where vcn.PolicyID in (select policyId from Policies)
GO

--GeneratedCertificate
GO
alter table GeneratedCertificate add IntermediaryId int null
GO
ALTER TABLE GeneratedCertificate
ADD CONSTRAINT FK_GeneratedCertificateIntermediary
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO

alter table GeneratedCertificate add CompanyId int null
GO
ALTER TABLE GeneratedCertificate
ADD CONSTRAINT FK_GeneratedCertificateCompany
FOREIGN KEY (CompanyId) REFERENCES Company(CompanyId);

GO
update GeneratedCertificate set CompanyID = vcert.companyId, intermediaryId = vcert.intermediaryId from GeneratedCertificate gcert join VehicleCertificates vcert on vcert.VehicleCertificateID = gcert.VehicleCertificateID
GO
--GeneratedCoverNotes
GO
alter table GeneratedCoverNotes add IntermediaryId int null
GO
ALTER TABLE GeneratedCoverNotes
ADD CONSTRAINT FK_GeneratedCoverNoteIntermediaries
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO
alter table GeneratedCoverNotes add CompanyId int null
GO
ALTER TABLE GeneratedCoverNotes
ADD CONSTRAINT FK_GeneratedCoverNoteCompany
FOREIGN KEY (CompanyId) REFERENCES Company(CompanyId);
GO
update GeneratedCoverNotes set CompanyID = vcn.companyId, intermediaryId = vcn.intermediaryId from GeneratedCoverNotes gcn join VehicleCoverNote vcn on vcn.VehicleCoverNoteID = gcn.VehicleCoverNoteID
GO
--VehiclesUnderPolicy
GO
alter table VehiclesUnderPolicy add IntermediaryId int null
GO
ALTER TABLE VehiclesUnderPolicy
ADD CONSTRAINT FK_VehicleUnderPolicyIntermediary
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO
update VehiclesUnderPolicy set CompanyID = pol.companyId, intermediaryId = pol.companycreatedby from Policies pol join VehiclesUnderPolicy vup on vup.PolicyID = pol.PolicyID
GO

--Vehicles
GO
alter table Vehicles add IntermediaryId int null
GO
ALTER TABLE Vehicles
ADD CONSTRAINT FK_VehicleIntermediary
FOREIGN KEY (IntermediaryId) REFERENCES Company(CompanyId);
GO
alter table Vehicles add CompanyId int null
GO
ALTER TABLE Vehicles
ADD CONSTRAINT FK_VehicleCompany
FOREIGN KEY (CompanyId) REFERENCES Company(CompanyId);
GO
update Vehicles set CompanyID = vup.companyId, intermediaryId = vup.IntermediaryId from Vehicles v join VehiclesUnderPolicy vup on vup.VehicleID = v.VehicleID
GO
