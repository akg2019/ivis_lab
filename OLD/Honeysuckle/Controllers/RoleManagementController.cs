﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using System.Text.RegularExpressions;
using System.IO;
using System.Web.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;


namespace Honeysuckle.Controllers
{
    public class RoleManagementController : Controller
    {

        public RoleManagementController() { }
        
        public ActionResult TestRoleName(int id = 0, string name = null)
        {
            List<UserPermission> permissions = new List<UserPermission>();
            permissions.Add(new UserPermission("UserGroups", "Update"));
            permissions.Add(new UserPermission("UserGroups", "Create"));
             
            if (UserModel.TestUserPermissions(Constants.user, permissions) && name != null)
            {
                UserGroup usergroup = new UserGroup();
                usergroup.UserGroupID = id;
                usergroup.UserGroupName = name;
                usergroup.company = CompanyModel.GetMyCompany(Constants.user);
                if (UserGroupModel.TestUserGroupUnique(usergroup)) return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        
        /// <summary>
        /// This function is used to return the index page of role management- showing all user groups
        /// </summary>
        public ActionResult CopyRole(int id = 0)
        {
            if (UserGroupModel.AmAnAdministrator(Constants.user))
            {
                UserGroup group = UserGroupModel.GetUserGroup(id);
                return View(group);
            }
            else return RedirectToAction("Index");
        }
        
        public ActionResult Index()
        {
            //SET DATA AS NULL IN CASE WE'RE COMING FROM ELSEWHERE IN THE CONTROLLER
            

            //ACTUAL INDEX WORK
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups", "Read")))
            {
                if( TempData["EmailSent"] !=null)
                {
                    ViewData["userCreated"] = TempData["EmailSent"];
                    TempData["EmailSent"] = null;
                }

                if (TempData["error"] != null)
                {
                    ViewData["error"] = TempData["error"];
                    TempData["error"] = null;
                }
                else
                {
                    if (TempData["message"] != null)
                    {
                        ViewData["message"] = TempData["message"];
                        TempData["message"] = null;
                    }
                }
                if (UserGroupModel.AmAnAdministrator(Constants.user)) return View(UserGroupModel.GetAllUserGroups());
                else return View(UserGroupModel.GetAllUserGroupsNotSA());
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }


        /// <summary>
        /// This function is used to remove all users from a particular usergroup
        /// </summary>
        /// <param name="id">usergroup id</param>
        /// <param name="name">usergroup name</param>
        public ActionResult Purge(int id)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups","Update")) && UserGroupModel.AmAnAdministrator(Constants.user))
            {
                UserGroupModel.PurgeUsersFromGroup(new UserGroup(id));
                TempData["message"] = "The Group '" + UserGroupModel.GetUserGroup(id).UserGroupName + "' has been purged of all users associated to it. You can now remove it from the system with ease.";
            }
            return RedirectToAction("Index");
        }


        /// <summary>
        /// This function is used to create a new user group
        /// </summary>
        public ActionResult Create()
        {
            /* WHEN BRINGING UP THE USERGROUP TO BE CREATED YOU SHOULD HAVE
             * TO CREATE A USER GROUP OBJECT JUST SO THAT WE CAN GRAB THE LIST
             * OF PERMISSIONS AVAILABLE SO THAT THEY CAN BE SET IN THE 
             * SAME FORM. AS OPPOSED TO MAKING THE USER GO TO ANOTHER FORM
             * TO SET SPECIFIC PERMISSIONS ON A USER GROUP
             */
            ViewData["search"] = "";
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups", "Create")))
            {
                ViewData["fillpermissions"] = 0;
                return View();
            }
            else return RedirectToAction("Index");
        }


        /// <summary>
        /// This function is used to create a new usergroup with various permissions
        /// </summary>
        [HttpPost]
        public ActionResult Create(UserGroup usergroup, List<int> mypermissions = null, List<int> allpermissions = null, List<int> allgroups = null, List<int> mygroups = null, string submit = null, string search = null, int companies = 0)
        {
            //they've created a new usergroup, to be sent to the database
            //doesnt send permissions yet
            ViewData["search"] = "";
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups","Create")))
            {
                ModelState.Remove("UserGroupID");
                if (ModelState.IsValid)
                {
                    //for administrator
                    if (companies != 0) usergroup.company = new Company(companies);
                    else usergroup.company = CompanyModel.GetMyCompany(Constants.user);

                    ViewData["fillpermissions"] = 0;
                    if (UserGroupModel.TestUserGroupUnique(usergroup))
                    {
                        if (usergroup.SuperGroup)
                        {
                            switch (submit)
                            {
                                case "Create":
                                    //SUPERGROUPS
                                    UserGroupModel.CreateUserGroup(usergroup);

                                    if (usergroup.children == null) usergroup.children = new List<UserGroup>();
                                    if (mygroups != null) usergroup.children = UserGroupModel.AddGroupToList(usergroup.children, mygroups);

                                    UserGroupModel.CreateSuperGroup(usergroup, usergroup.children);
                                    return RedirectToAction("AssignToUsers", new { id = usergroup.UserGroupID });

                                default:
                                    return RedirectToAction("Index"); // hell broke loose
                            }
                        }
                        else
                        {
                            switch (submit)
                            {
                                case "Create":
                                    if (usergroup.UserGroupName != null)
                                    {
                                        if (usergroup.Permissions == null) usergroup.Permissions = new List<UserPermission>();
                                        if (mypermissions == null) mypermissions = new List<int>();
                                        usergroup.Permissions = UserPermissionModel.AddPermissionsToList(usergroup.Permissions, mypermissions);
                                        UserGroupModel.CreateUserGroup(usergroup);
                                        UserGroupModel.UpdateUserGroupPermissions(usergroup);
                                        return RedirectToAction("AssignToUsers", new { id = usergroup.UserGroupID });
                                    }
                                    else
                                    {
                                        return View(usergroup); //need data filled
                                    }

                                default:
                                    return RedirectToAction("Index");
                            }

                        }
                    }
                    else
                    {
                        ModelState.AddModelError("UserGroupName", "This name is already in use. Please use a different name.");
                        return View(usergroup);
                    }
                }
                else
                {
                    //not right
                    ViewData["fillpermissions"] = 0;
                    return View(usergroup);
                }
            }
            else return RedirectToAction("Index"); //failed security
            
        }

        [HttpPost]
        public ActionResult Creates(UserGroup usergroup, List<int> mypermissions = null, List<int> allpermissions = null, List<int> allgroups = null, List<int> mygroups = null, string submit = null, string search = null, int companies = 0)
        {

            string sqlConnectionString = "";
#if DEBUG
            sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;//ApplicationServices
#else
                        sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ExternalConnection"].ConnectionString; 
#endif

            JArray result = new JArray();

            string query = "SELECT COMPANYID FROM COMPANY";

            DataTable dt = (DataTable)OmaxFramework.Utilities.OmaxData.HandleQuery(query, sqlConnectionString, OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.ScalarQuery);

            result = JArray.Parse(JsonConvert.SerializeObject(dt));


            foreach (var Comp in result)
            {

                companies = int.Parse(Comp["COMPANYID"].ToString());

                //they've created a new usergroup, to be sent to the database
                //doesnt send permissions yet
                ViewData["search"] = "";
                if (UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups", "Create")))
                {
                    ModelState.Remove("UserGroupID");
                    if (ModelState.IsValid)
                    {

                        //for administrator
                        if (companies != 0) usergroup.company = new Company(companies);
                        else usergroup.company = CompanyModel.GetMyCompany(Constants.user);

                        ViewData["fillpermissions"] = 0;
                        if (UserGroupModel.TestUserGroupUnique(usergroup))
                        {
                            if (usergroup.SuperGroup)
                            {
                                switch (submit)
                                {
                                    case "Create":
                                        //SUPERGROUPS
                                        UserGroupModel.CreateUserGroup(usergroup);

                                        if (usergroup.children == null) usergroup.children = new List<UserGroup>();
                                        if (mygroups != null) usergroup.children = UserGroupModel.AddGroupToList(usergroup.children, mygroups);

                                        UserGroupModel.CreateSuperGroup(usergroup, usergroup.children);
                                        break;// return RedirectToAction("AssignToUsers", new { id = usergroup.UserGroupID });


                                }
                            }
                            else
                            {
                                switch (submit)
                                {
                                    case "Create":
                                        if (usergroup.UserGroupName != null)
                                        {
                                            if (usergroup.Permissions == null) usergroup.Permissions = new List<UserPermission>();
                                            if (mypermissions == null) mypermissions = new List<int>();
                                            usergroup.Permissions = UserPermissionModel.AddPermissionsToList(usergroup.Permissions, mypermissions);
                                            UserGroupModel.CreateUserGroup(usergroup);
                                            UserGroupModel.UpdateUserGroupPermissions(usergroup);
                                            break;// return RedirectToAction("AssignToUsers", new { id = usergroup.UserGroupID });
                                        }
                                        else
                                        {
                                            break;// return View(usergroup); //need data filled
                                        }


                                    // return RedirectToAction("Index");
                                }

                            }
                        }
                        else
                        {
                            //ModelState.AddModelError("UserGroupName", "This name is already in use. Please use a different name.");
                            // return View(usergroup);
                        }
                    }
                    else
                    {
                        //not right
                        ViewData["fillpermissions"] = 0;
                        // return View(usergroup);
                    }
                }
                //else return RedirectToAction("Index"); //failed security

            }


            return View();


        }



        /// <summary>
        /// This function is used to retrieve and return a user group's information, given the user group id
        /// </summary>
        /// <param name="id">usergroup id</param>
        public ActionResult Edit(int id)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups","Update")))
            {
                ViewData["fillpermissions"] = 0;
                UserGroup usergroup = UserGroupModel.GetUserGroup(id);
                return View(usergroup);
            }
            else return RedirectToAction("Index");
        }


        /// <summary>
        /// This function is used to delete a usergroup from the system
        /// </summary>
        /// <param name="id">usergroup id</param>
        public ActionResult Delete(int id)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups","Delete")))
            {
                if (UserGroupModel.DeleteUserGroup(id)) return Json(new { result = 1 }, JsonRequestBehavior.AllowGet); //TempData["error"] = "This Group cannot be deleted as it has users assigned to it";
                else return Json(new { result = 0, assigned = 1 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function is used to retrieve a user group in the system
        /// </summary>
        /// <param name="id">usergroup id</param>
        public ActionResult AssignToUsers(int id = 0)
        {
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups","Update")))
            {
                ViewData["search"] = "";
                if (id == 0) return RedirectToAction("Index", "Home");
                else return View(UserGroupModel.GetUserGroup(id));
            }
            else return RedirectToAction("AccessDenied","Error");
        }


        /// <summary>
        /// This function is used to temporarily give system admin a different role
        /// </summary>
        /// <param name="id">usergroup id</param>
        public ActionResult TempRole(int id = 0)
        {
            if (UserGroupModel.AmAnAdministrator(Constants.user))
            {
                ViewData["search"] = "";
                if (id == 0) return RedirectToAction("Index", "Home");
                else
                {
                    Constants.user.tempUserGroup = UserGroupModel.GetUserGroup(id);
                    Constants.user.newRoleMode = true;
                    TempData["EmailSent"] = "You have assumed the temporary role selected!";

                    return RedirectToAction("Index", "Home");
                }
            }
            else return RedirectToAction("AccessDenied", "Error");
        }

        /// <summary>
        /// This function is used to temporarily give system admin a different role
        /// </summary>
        /// <param name="id">usergroup id</param>
        public ActionResult ReleaseTempRole()
        {
            if (Constants.user.newRoleMode)
            {
                ViewData["search"] = "";
                
                Constants.user.tempUserGroup = null;
                Constants.user.newRoleMode = false;
                TempData["EmailSent"] = "Temporary role dropped.";

                return RedirectToAction("Index", "Home");
            }
            else return RedirectToAction("AccessDenied", "Error");
        }

        public ActionResult Upload()
        {
            if (false)
            {
                if (UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups", "Upload"))) return View();
                else return RedirectToAction("AccessDenied", "Error");
            }
            else return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase UploadedFile = null)
        {
            if (false)
            {
                if (UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups", "Upload")))
                {
                    //string errors = "";
                    if (UploadedFile != null)
                    {
                        Stream stream = UploadedFile.InputStream; string line; string text = null;
                        using (StreamReader sr = new StreamReader(stream)) { while ((line = sr.ReadLine()) != null) { text += line; } }
                        XML.UploadUserGroups(text);
                    }
                    return View();
                }
                else return RedirectToAction("AccessDenied", "Error");
            }
            else return RedirectToAction("Index");
        }

        public ActionResult AddPermissionToGroup(int id = 0, int pid = 0)
        {
            if (id != 0 && pid != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups", "Update")))
            {
                if ((UserGroupModel.is_my_group(new UserGroup(id))) || UserGroupModel.AmAnAdministrator(Constants.user))
                {
                    UserPermissionModel.UpdateUserGroupPermission(new UserGroup(id), new UserPermission(pid));
                    return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RmPermissionToGroup(int id = 0, int pid = 0)
        {
            if (id != 0 && pid != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups", "Update")))
            {
                if ((UserGroupModel.is_my_group(new UserGroup(id))) || UserGroupModel.AmAnAdministrator(Constants.user))
                {
                    UserPermissionModel.RemoveUserPermissionFromGroup(new UserPermission(pid), new UserGroup(id));
                    return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);

            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddChildToGroup(int id = 0, int gid = 0)
        {
            if (id != 0 && gid != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups", "Update")))
            {
                if ((UserGroupModel.is_my_group(new UserGroup(id)) && UserGroupModel.is_my_group(new UserGroup(gid))) || UserGroupModel.AmAnAdministrator(Constants.user))
                {
                    UserGroupModel.AddChildToSuperGroup(new UserGroup(id), new UserGroup(gid));
                    return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RmChildToGroup(int id = 0, int gid = 0)
        {
            if (id != 0 && gid != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups", "Update")))
            {
                if ((UserGroupModel.is_my_group(new UserGroup(id)) && UserGroupModel.is_my_group(new UserGroup(gid))) || UserGroupModel.AmAnAdministrator(Constants.user)) 
                {
                    UserGroupModel.RemoveChildFromSuperGroup(new UserGroup(id), new UserGroup(gid));
                    return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddUserToGroup(int id = 0, int uid = 0)
        {
            if (id != 0 && uid != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups", "Update")))
            {
                if (UserGroupModel.is_my_group(new UserGroup(id)) || UserGroupModel.AmAnAdministrator(Constants.user))
                {
                    UserGroupModel.AssociateUserGroup(new UserGroup(id), new User(uid));
                    return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RmUserToGroup(int id = 0, int uid = 0)
        {
            if (id != 0 && uid != 0 && UserModel.TestUserPermissions(Constants.user, new UserPermission("UserGroups", "Update")))
            {
                if (UserGroupModel.is_my_group(new UserGroup(id)) || UserGroupModel.AmAnAdministrator(Constants.user))
                {
                    if (UserModel.IsIAJcreated(uid))
                        return Json(new { result = 0, note = 1 }, JsonRequestBehavior.AllowGet);
                    else
                        UserGroupModel.DisassociateUserGroup(new UserGroup(id), new User(uid));
                        return Json(new { result = 1, note= 0}, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { result = 0, note = 0}, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0, note = 0}, JsonRequestBehavior.AllowGet);
        }

    }
}
