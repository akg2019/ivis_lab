﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Web.Routing;

namespace Honeysuckle.Controllers
{
    public class NotificationController : Controller
    {

        public ActionResult AdditionalNote(int id = 0, string page = null)
        {
            if (page != null && id != 0)
            {
                ViewData["pagename"] = page;
                return View(NotificationModel.GetTheNewLastNotification(id,page));
            }
            else return new EmptyResult();
        }

        //Newly Added
        public ActionResult GetUnreadCount()
         {
            return Json(new { count = NotificationModel.GetUnreadNotificationsCount() }, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// This function returns the index page for notifications - showing user's unread notifications
        /// </summary>
        public ActionResult Index(int page = 0)
        {
            if (Constants.user.username != "not initialized")
            {
                #region on the bubble
                if (TempData["sent"] != null)
                {
                    ViewData["sent"] = TempData["sent"];
                    TempData["sent"] = null;
                }
                if (TempData["sendblank"] != null)
                {
                    ViewData["senderror"] = "There was an error in creating your message. Please try again later. If the error persists please contact your administrator";
                    TempData["sendblank"] = null;
                }
                #endregion

                ViewData["unreadcount"] = NotificationModel.GetUnreadNotificationsCount();
                ViewData["allcount"] = NotificationModel.GetAllMyNotesCount();
                ViewData["trashcount"] = NotificationModel.GetMyTrashCount();
                ViewData["pagename"] = "unread";
                if (page == 0) page = 1;
                return View(NotificationModel.GetUnreadNotifications(page));
            }
            else return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// This function retrieves and displays all of the current user's notifications
        /// </summary>
        public ActionResult AllMail(int page = 0)
        {
            ViewData["unreadcount"] = NotificationModel.GetUnreadNotificationsCount();
            ViewData["allcount"] = NotificationModel.GetAllMyNotesCount();
            ViewData["trashcount"] = NotificationModel.GetMyTrashCount();
            ViewData["pagename"] = "all";
            if (page == 0) page = 1;
            if (Constants.user.username != "not initialized") return View(NotificationModel.GetAllMyNotes(page));
            else return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// This function retrieves all the current user's notifications that are in the trash
        /// </summary>
        public ActionResult Trash(int page = 0)
        {

            ViewData["unreadcount"] = NotificationModel.GetUnreadNotificationsCount();
            ViewData["allcount"] = NotificationModel.GetAllMyNotesCount();
            ViewData["trashcount"] = NotificationModel.GetMyTrashCount();
            ViewData["pagename"] = "trash";
            if (page == 0) page = 1;
            if (Constants.user.username != "not initialized") return View(NotificationModel.GetMyTrash(page));
            else return RedirectToAction("Index", "Home");
        }


        /// <summary>
        /// This function displays the mail creation page - to create a new notification/message
        /// </summary>
        public ActionResult Send(string header = null, string content = null, List<string> email =null)
        {
            if (Constants.user.username != "not initialized")
            {

                if (header != null || content != null)
                {
                    Notification note = new Notification();
                    note.heading = header;
                    note.content = content;
                    return View(note);
                }
                else
                {
                    TempData["sendblank"] = false;
                    return RedirectToAction("Index", "Notification");
                }
            }
            else return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// This function is used to send/clear a new notification/message 
        /// </summary>
        [HttpPost]
        public ActionResult Send(Notification notification, string submit = null, string send = null)
        {
            if (Constants.user.username != "not initialized")
            {
                if (ModelState.IsValid)
                {
                    if (send != null)
                    {
                        switch (submit)
                        {
                            case "Send":
                                List<User> to = new List<User>();
                                string[] names = send.Split(';');
                                foreach (string name in names)
                                {
                                    if (UserModel.UserExist(name)) to.Add(UserModel.GetUser(name));
                                }
                                notification.NoteTo = to;
                                notification.NoteFrom = Constants.user;
                                NotificationModel.CreateNotification(notification);
                                TempData["sent"] = true;
                                return RedirectToAction("Index");
                            case "Clear":
                                notification = new Notification();
                                return View(notification);
                            default:
                                return View(notification);
                        }
                    }
                    else return View(notification);
                }
                else return View(notification);
            }
            else return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// This function is used to reply to one or all notifications in the thread
        /// </summary>
        /// <param name="id">Notification id</param>
        /// <param name="type">The action to be performed- either replying to one or all</param>
        public ActionResult Reply(int id = 0, string type = null)
        {
            if (Constants.user.username != "not initialized")
            {
                if (id != 0)
                {
                    Notification note = new Notification();
                    note.NoteTo = new List<User>();
                    Notification old_note = NotificationModel.GetThisNotification(id);
                    note.parent = new Notification(id);
                    if (type != null)
                    {
                        switch (type)
                        {
                            case "reply":
                                note.NoteTo.Add(old_note.NoteFrom);
                                break;
                            case "replyall":
                                note.NoteTo.Add(old_note.NoteFrom);
                                note.NoteTo.AddRange(old_note.NoteTo);
                                break;
                            default:
                                break;
                        }
                    }
                    note.heading = old_note.heading;
                    return View(note);
                }
                else return RedirectToAction("Index");
            }
            else return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// This function is used to reply to or clear a notification
        /// </summary>
        [HttpPost]
        public ActionResult Reply(Notification notification, string submit = null, string send = null)
        {
            if (Constants.user.username != "not initialized")
            {
                if (ModelState.IsValid)
                {
                    if (send != null)
                    {
                        switch (submit)
                        {
                            case "Send":
                                List<User> to = new List<User>();
                                string[] names = send.Split(';');
                                foreach (string name in names)
                                {
                                    if (UserModel.UserExist(name)) to.Add(UserModel.GetUser(name));
                                }
                                notification.NoteTo = to;
                                notification.NoteFrom = Constants.user;
                                NotificationModel.CreateNotification(notification);
                                TempData["sent"] = true;
                                return RedirectToAction("Index");
                            case "Clear":
                                notification.content = null;
                                return View(notification);
                            default:
                                return View(notification);
                        }
                    }
                    else
                    {
                        List<User> to = new List<User>();
                        string[] names = send.Split(';');
                        foreach (string name in names)
                        {
                            if (UserModel.UserExist(name)) to.Add(UserModel.GetUser(name));
                        }
                        notification.NoteTo = to;
                        return View(notification);
                    }
                }
                else
                {
                    List<User> to = new List<User>();
                    string[] names = send.Split(';');
                    foreach (string name in names)
                    {
                        if (UserModel.UserExist(name)) to.Add(UserModel.GetUser(name));
                    }
                    notification.NoteTo = to;
                    return View(notification);
                }
            }
            else return RedirectToAction("Index", "Home");

        }

        /// <summary>
        /// This function is used to retrieve a message thread based on the notification id
        /// </summary>
        /// <param name="id">Notification id</param>
        public ActionResult ViewMessage(int id = 0)
        {
            if (id != 0)
            {
                if (NotificationModel.IsMyNotification(id) && Constants.user.username != "not initialized")
                {
                    NotificationModel.MarkNotifcationRead(new Notification(id));
                    return View(NotificationModel.GetThisNotification(id));
                }
                else return RedirectToAction("Index");
            }
            else return RedirectToAction("Index");
        }

        /// <summary>
        /// This function is used to retrieve all the unread notifications of a user
        /// </summary>
        public ActionResult NotificationBar()
        {
            //return View(NotificationModel.GetUnreadNotifications()); //Get my Notes that I haven't seen
            return View(new List<Notification>());
        }

        /// <summary>
        /// This function is used to retrieve and show a notification, based on its ID
        /// </summary>
        /// <param name="id">Notification id</param>
        public ActionResult ShowNotification(int id = 0, string show = null)
        {
            if (UserModel.TestGUID(Constants.user) && id != 0)
            {
                if (NotificationModel.IsMyNotification(id))
                {
                    if (show != null) ViewData["show"] = show;
                    return View(NotificationModel.GetThisNotification(id));
                }
                else return new EmptyResult();
            }
            else return new EmptyResult();
        }

        /// <summary>
        /// This function is used to retrieve and show the unseen notifications of a user
        /// </summary>
        public ActionResult GetUnseenNotifications()
        {
            List<Notification> notifications = NotificationModel.GetUnseenNotifications();
            string jsonString = JsonConvert.SerializeObject(notifications);
            return Json(jsonString, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function is used to retrieve and show the previously seen notifications of a user
        /// </summary>
        public ActionResult GetUnnewNotifications()
        {
            string jsonString = "";
            if (UserModel.TestGUID(Constants.user))
            {
                List<Notification> notifications = NotificationModel.GetUnnewNotifications();
                jsonString = JsonConvert.SerializeObject(notifications);
            }
            else jsonString = JsonConvert.SerializeObject(new List<Notification>());
            return Json(jsonString, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function is used to mark a notification as seen
        /// </summary>
        /// <param name="id">Notification id</param>
        public ActionResult Seen(int id = 0)
        {
            if (id != 0)
            {
                if (UserModel.TestGUID(Constants.user) && NotificationModel.IsMyNotification(id))
                {
                    NotificationModel.MarkNotifcationSeen(new Notification(id));
                    return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function is used to mark a notification as read
        /// </summary>
        /// <param name="id">Notification id</param>
        public ActionResult Read(int id = 0)
        {
            if (id != 0)
            {
                if (UserModel.TestGUID(Constants.user) && NotificationModel.IsMyNotification(id))
                {
                    NotificationModel.MarkNotifcationRead(new Notification(id));
                    return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function is used to delete a notification
        /// </summary>
        /// <param name="id">Notification id</param>
        public ActionResult Delete(int id)
        {
            if (id != 0)
            {
                if (UserModel.TestGUID(Constants.user) && NotificationModel.IsMyNotification(id))
                {
                    NotificationModel.MarkNotifcationDeleted(new Notification(id));
                    return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function is used to mark a notification as read
        /// </summary>
        /// <param name="id">Notification id</param>
        public ActionResult MarkAsRead(int id = 0)
        {
            if (id != 0)
            {
                if (UserModel.TestGUID(Constants.user) && NotificationModel.IsMyNotification(id))
                {
                    NotificationModel.MarkNotifcationRead(new Notification(id));
                }
            }
            return RedirectToAction("Index");
        }


        /// <summary>
        /// This function is used to mark a notification as deleted
        /// </summary>
        /// <param name="id">Notification id</param>
        public ActionResult MarkAsDeleted(int id = 0)
        {
            if (id != 0) if (UserModel.TestGUID(Constants.user) && NotificationModel.IsMyNotification(id)) NotificationModel.MarkNotifcationDeleted(new Notification(id));
            return RedirectToAction("Index");
        }

        /// <summary>
        /// This function loads the create-notification page (and fills in some of the required info on the page)
        /// </summary>
        public ActionResult Approval(int id = 0, string type = null)
        {
            Notification note = new Notification();
            Company Insurer = new Company ();
            if (id != 0 && type != null)
            {
                ViewData["id"] = id;

                string link = null;
                switch (type)
                {
                    case "certificate":
                        note.heading = "Vehicle certificate pending approval.";
                        link = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + Url.Action("Details", "VehicleCertificate") + "/" + id;
                        break;
                    case "covernote":
                        note.heading = "Vehicle cover note pending approval.";
                        link = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + Url.Action("Edit", "VehicleCoverNote") + "/" + id;
                        break;
                    default:
                        break;
                }
                
                if (link != null)
                {
                    note.content = link;
                    RouteValueDictionary dict = new RouteValueDictionary();
                    dict.Add("header", note.heading);
                    dict.Add("content", note.content);
                    return RedirectToAction("Send", "Notification", dict);
                }
                else return RedirectToAction("Index", "Home");
            }
            else return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// This function sends a notification about a vehicle (under covereage at another company)- to admins at that company
        /// </summary>
        public ActionResult sendNoteToComp(string vehId = null, List<string> admins = null, string heading = null, string content = null)
        {
            Notification note = new Notification();
            note.NoteTo = new List<User>();

            for (int x = 0; x < admins.Count(); x++)
            {

                if(UserModel.UserExist(admins[x]))
                    note.NoteTo.Add(new User(UserModel.GetUser(admins[x]).ID));
               // Gets the user Id of all admins, gievn their username
               // note.NoteTo.Add(new User(UserModel.GetUser(admins[x]).ID));
            }

            note.heading = heading;
            note.content = content;

            note.NoteFrom = new User(Constants.user.ID);

            NotificationModel.CreateNotification(note);
            return Json(new { result = 1 }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// This function loads the sendTo partial, which allows the user to send a message to the admins of a 
        /// company for which a vehicle/certificate/cover note is currently under policy
        /// </summary>
        public ActionResult sendNote(string id = null, string type=null) 
        {
            if (UserModel.TestGUID(Constants.user) && id != null)
            {
                Vehicle veh = new Vehicle ();
                //ViewData["vehicle"] = int.Parse(id);

                if (type != null)
                {
                    ViewData["vehicle"] = id;
                    ViewData["type"] = type;
                    ViewData["vehicleInfo"] = "";
                }
                else
                {
                    ViewData["vehicle"] = int.Parse(id);
                    veh = VehicleModel.GetVehicle(int.Parse(id));
                    ViewData["type"] = "Vehicle";

                    //Pulling more info about the vehicle for display purposes
                    ViewData["vehicleInfo"] = veh.chassisno + " " + veh.make + " " + veh.VehicleModel;
                }
                return View();
            }
            else return new EmptyResult();
        }

        public ActionResult CoverCerts()
       {
           List<CertCover> VehicleCoverCert = CertCoverModel.GetCertCovers();
            return View(VehicleCoverCert);
       }

        
    }
}
