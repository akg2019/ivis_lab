﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class AddressController : Controller
    {

        public ActionResult AuditAddress()
        {
            return View();
        }

        /// <summary>
        /// this function is what generates the address form view
        /// </summary>
        public ActionResult Address(int id = 0)
        {
            Address address = new Address();
            if (id != 0)
            {
                address.ID = id;
                address = AddressModel.GetAddress(address);
            }
           
            return View(address);
           
        }

        public ActionResult GetHolderAddress(string id = null)
        {
            List<UserPermission> permissions = new List<UserPermission>();
            permissions.Add(new UserPermission("Policies","Create"));
            permissions.Add(new UserPermission("Policies","Update"));

            if (UserModel.TestUserPermissions(Constants.user, permissions)){
                Address address = new Address();
                switch (id.Substring(0, 1))
                {
                    case "p":
                        address = PersonModel.GetPersonAddress(new Person(int.Parse(id.Substring(1, id.Length - 1))));
                        break;
                    case "c":
                        address = CompanyModel.GetCompanyAddress(new Company(int.Parse(id.Substring(1, id.Length - 1))));
                        break;
                    default:
                        break;
                }
                if (address.longAddressUsed) return Json(new { success = 0, longAddressUsed = address.longAddressUsed, longAddress = address.longAddress }, JsonRequestBehavior.AllowGet);
                if (address.ID == 0) return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
                else return Json(new {  success = 1, 
                                        aid = address.ID,
                                        roadno = (address.roadnumber == null ? "" : address.roadnumber), 
                                        roadname = (address.road == null ? "" : address.road.RoadName),
                                        roadtype = (address.roadtype == null ? "" : address.roadtype.RoadTypeName),
                                        apt = (address.ApartmentNumber == null ? "" : address.ApartmentNumber),
                                        city = (address.city == null ? "" : address.city.CityName),
                                        parishid = (address.parish == null ? 0 : address.parish.ID),
                                        parish = (address.parish == null ? "" : address.parish.parish),
                                        country = (address.country == null ? "" : address.country.CountryName)
                                        }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this function returns a list of roadtypes
        /// used by JavaScript for autocomplete
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult GetRoadTypes(string type)
        {
            List<string> result = new List<string>();
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Address", "Read"))) result = RoadTypeModel.GetRoadType();

            var json = result.Where(x => x.ToLower().StartsWith(type.ToLower()))
                             .OrderBy(x => x)
                             .Select(r => new { value = r });
            
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this function returns a list of roads
        /// used by JavaScript for autocomplete
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult GetRoads(string type)
        {
            List<string> result = new List<string>();
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Address", "Read"))) result = RoadModel.GetRoadList();

            var json = result.Where(x => x.ToLower().StartsWith(type.ToLower()))
                             .OrderBy(x => x)
                             .Select(r => new { value = r });

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this function returns a list of zip codes
        /// used by JavaScript for autocomplete
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult GetZipCodes(string type)
        {
            List<string> result = new List<string>();
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Address", "Read"))) result = ZipCodeModel.GetZipList();

            var json = result.Where(x => x.ToLower().StartsWith(type.ToLower()))
                             .OrderBy(x => x)
                             .Select(r => new { value = r });

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this function returns a list of cities
        /// used by JavaScript for autocomplete
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult GetCities(string type)
        {
            List<string> result = new List<string>();
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Address", "Read"))) result = CityModel.GetCityList();

            var json = result.Where(x => x.ToLower().StartsWith(type.ToLower()))
                             .OrderBy(x => x)
                             .Select(r => new { value = r });

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this function returns a list of countries
        /// used by JavaScript for autocomplete
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult GetCountries(string type)
        {
            List<string> result = new List<string>();
            if (UserModel.TestUserPermissions(Constants.user, new UserPermission("Address", "Read"))) result = CountryModel.GetCountryList();

            var json = result.Where(x => x.ToLower().StartsWith(type.ToLower()))
                             .OrderBy(x => x)
                             .Select(r => new { value = r });

            return Json(json, JsonRequestBehavior.AllowGet);
        }

    }
}
