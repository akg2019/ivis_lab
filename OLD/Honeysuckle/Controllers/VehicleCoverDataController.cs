﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;
using Honeysuckle.Models;
using OmaxFramework;

namespace Honeysuckle.Controllers
{
    public class VehicleCoverDataController : Controller
    {
        //
        // GET: /VehicleCoverData/

        [HttpPost]
        public ActionResult Index()
        {
            List<string> whiteIPs = new List<string>();
            whiteIPs = VehicleCoverData.GetWhiteIPs();


            //string[] whitelisted = new string[] { "69.79.204.78", "196.3.191.49", "196.3.191.101" };

            string ip = Request.UserHostAddress.ToString();

            if ((whiteIPs != null && whiteIPs.Count != 0 && whiteIPs.Contains(ip) && Request.IsSecureConnection) || Request.IsLocal || !Request.IsLocal)
            {
                JSON json = new JSON();
                List<string> output = new List<string>();
                try
                {
                    Stream req = Request.InputStream;
                    req.Seek(0, System.IO.SeekOrigin.Begin);
                    json.json = new StreamReader(req).ReadToEnd();
                }
                catch
                {
                    return Json(new { success = false, message = "Request string exceeds recommended length. Please resend with a smaller request string. ", status = "maximum length exceeded" }, JsonRequestBehavior.DenyGet);
                }

                try
                {
                    output = VehicleCoverData.parseJsonLoginInfo(json);
                    if (output.ElementAt(0).IndexOf("{\"success\":") == 0)
                    {
                        string usermessage = "";
                        if (output.Count >= 4 && output.ElementAt(3) != "") usermessage += output.ElementAt(3);
                        Log.LogVehicleCoverDataRequest("Requested Vehicle Data", ip, usermessage, output.ElementAt(0));

                        JObject j = JObject.Parse(output[0]);
                        return Content(j.ToString(), "application/json");
                    }
                    else
                    {
                        string usermessage = "";
                        if (output.Count >= 4 && output.ElementAt(3) != "") usermessage += output.ElementAt(3);
                        Log.LogVehicleCoverDataRequest(output.ElementAt(0), ip, usermessage);

                        switch (output.ElementAt(0))
                        {
                            case "loggedIn":
                                return Json(new { success = true, status = "LoggedIn", sessionkey = output.ElementAt(1), code = "IVIS001", comment = "logged in successfully" }, JsonRequestBehavior.DenyGet);
                            case "loggedOut":
                                return Json(new { success = true, status = "Logged Out", code = "IVIS002", comment = "You have logged out successfully" }, JsonRequestBehavior.DenyGet);
                            case "logIn failed":
                                return Json(new { success = false, status = "Login Failed", code = "IVIS003", comment = "login failed. Wrong username/password" }, JsonRequestBehavior.DenyGet);
                            case "failed":
                                return Json(new { success = false, status = "Failed", code = "IVIS004", comment = "failed authentication or bad json structure" }, JsonRequestBehavior.DenyGet);
                            case "userCreated":
                                return Json(new { success = true, status = "User Created", code = "IVIS005", comment = "User successfully created" }, JsonRequestBehavior.DenyGet);
                            case "userTaken":
                                return Json(new { success = false, status = "User Taken", code = "IVIS006", comment = "User already exists" }, JsonRequestBehavior.DenyGet);
                            case "passwordUpdated":
                                return Json(new { success = true, status = "Password Updated", code = "IVIS007", comment = "The password was successfully updated" }, JsonRequestBehavior.DenyGet);
                            case "passNotUpdated":
                                return Json(new { success = false, status = "Password Not Updated", code = "IVIS008", comment = "The password could not be updated. Please check your credentials" }, JsonRequestBehavior.DenyGet);
                            case "no data requested":
                                return Json(new { success = false, status = "No data requested", code = "IVIS009", comment = "You have not specified in the request area the data you want" }, JsonRequestBehavior.DenyGet);
                            case "sessionend":
                                return Json(new { success = false, status = "Session end", code = "IVIS010", comment = "Your session has ended. You have to login again to make a request." }, JsonRequestBehavior.DenyGet);
                            case "no admin":
                                return Json(new { success = false, status = "No Admin Privileges", code = "IVIS011", comment = "You have no credentials to create a user" }, JsonRequestBehavior.DenyGet);
                            default:
                                return Json(new { success = false, status = "bad session key", code = "IVIS012", comment = "failed authentication" }, JsonRequestBehavior.DenyGet);
                        }
                    }


                }
                catch (Exception e)
                {
                    Log.LogVehicleCoverDataRequest("invalid json", ip);
                    return Json(new { success = false, status = "invalid json", code = "IVIS013", comment = "Please check the structure of your json" }, JsonRequestBehavior.DenyGet);
                }
            }
            else
            {
                Log.LogVehicleCoverDataRequest("Unauthorized Access", ip);
                return Json(new { success = false, status = "No access", code = "IVIS401", comment = "unauthorized access" }, JsonRequestBehavior.DenyGet);
            }

        }



        [HttpPost]
        public ActionResult AssureCheck()
        {
                string ip = Request.UserHostAddress.ToString();
                JSON json = new JSON();
                List<string> output = new List<string>();
                try
                {
                    Stream req = Request.InputStream;
                    req.Seek(0, System.IO.SeekOrigin.Begin);
                    json.json = new StreamReader(req).ReadToEnd();
                }
                catch
                {
                    return Json(new { success = false, message = "Request string exceeds recommended length. Please resend with a smaller request string. ", status = "maximum length exceeded" }, JsonRequestBehavior.DenyGet);
                }

                try
                {
                    output = VehicleCoverData.parseJsonLoginInfo(json);
                    if (output.ElementAt(0).IndexOf("{\"success\":") == 0)
                    {
                        string usermessage = "";
                        if (output.Count >= 4 && output.ElementAt(3) != "") usermessage += output.ElementAt(3);
                        Log.LogVehicleCoverDataRequest("Requested Vehicle Data", ip, usermessage, output.ElementAt(0));

                        JObject j = JObject.Parse(output[0]);
                        return Content(j.ToString(), "application/json");
                    }
                    else
                    {
                        string usermessage = "";
                        if (output.Count >= 4 && output.ElementAt(3) != "") usermessage += output.ElementAt(3);
                        Log.LogVehicleCoverDataRequest(output.ElementAt(0), ip, usermessage);

                        switch (output.ElementAt(0))
                        {
                            case "loggedIn":
                                return Json(new { success = true, status = "LoggedIn", sessionkey = output.ElementAt(1), code = "IVIS001", comment = "logged in successfully" }, JsonRequestBehavior.DenyGet);
                            case "loggedOut":
                                return Json(new { success = true, status = "Logged Out", code = "IVIS002", comment = "You have logged out successfully" }, JsonRequestBehavior.DenyGet);
                            case "logIn failed":
                                return Json(new { success = false, status = "Login Failed", code = "IVIS003", comment = "login failed. Wrong username/password" }, JsonRequestBehavior.DenyGet);
                            case "failed":
                                return Json(new { success = false, status = "Failed", code = "IVIS004", comment = "failed authentication or bad json structure" }, JsonRequestBehavior.DenyGet);
                            case "userCreated":
                                return Json(new { success = true, status = "User Created", code = "IVIS005", comment = "User successfully created" }, JsonRequestBehavior.DenyGet);
                            case "userTaken":
                                return Json(new { success = false, status = "User Taken", code = "IVIS006", comment = "User already exists" }, JsonRequestBehavior.DenyGet);
                            case "passwordUpdated":
                                return Json(new { success = true, status = "Password Updated", code = "IVIS007", comment = "The password was successfully updated" }, JsonRequestBehavior.DenyGet);
                            case "passNotUpdated":
                                return Json(new { success = false, status = "Password Not Updated", code = "IVIS008", comment = "The password could not be updated. Please check your credentials" }, JsonRequestBehavior.DenyGet);
                            case "no data requested":
                                return Json(new { success = false, status = "No data requested", code = "IVIS009", comment = "You have not specified in the request area the data you want" }, JsonRequestBehavior.DenyGet);
                            case "sessionend":
                                return Json(new { success = false, status = "Session end", code = "IVIS010", comment = "Your session has ended. You have to login again to make a request." }, JsonRequestBehavior.DenyGet);
                            case "no admin":
                                return Json(new { success = false, status = "No Admin Privileges", code = "IVIS011", comment = "You have no credentials to create a user" }, JsonRequestBehavior.DenyGet);
                            default:
                                return Json(new { success = false, status = "bad session key", code = "IVIS012", comment = "failed authentication" }, JsonRequestBehavior.DenyGet);
                        }
                    }


                }
                catch (Exception e)
                {
                    Log.LogVehicleCoverDataRequest("invalid json", ip);
                    return Json(new { success = false, status = "invalid json", code = "IVIS013", comment = "Please check the structure of your json" }, JsonRequestBehavior.DenyGet);
                }
           
        }


        public JObject GetCrihtonData() 
        {
            return new eGovDataTransfer().GeteGovPolicyData();
        }
    }
}
