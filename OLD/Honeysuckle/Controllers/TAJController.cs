﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class TAJController : Controller
    {
        //
        // GET: /TAJ/

        public ActionResult GetTRNs(string id)
        {
            //test code
            System.Net.ServicePointManager.ServerCertificateValidationCallback +=
            delegate(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                    System.Security.Cryptography.X509Certificates.X509Chain chain,
                                    System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                return true; // **** Always accept
            };

            Driver driver = TAJ.GetDriversOnTRN(id);
            return Json(new { driver }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetVehs(string plateno = null, string chassis = null, string vehid = null)
        {
            if (plateno != null || chassis != null || vehid != null)
            {
                //test code
                System.Net.ServicePointManager.ServerCertificateValidationCallback +=
                delegate(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                                        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                {
                    return true; // **** Always accept
                };

                Vehicle veh = TAJ.GetVehicleDetails(chassis, vehid, plateno);
                return Json(new { veh }, JsonRequestBehavior.AllowGet);
            }
            else return Json(new { fail = true }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function is used to set a company's TAJ credentials
        /// </summary>
        /// <param name="id">Company Id</param>
        public ActionResult TAJCredentials(int id = 0)
        {
            if (UserGroupModel.IsAdministrative(Constants.user) && ((CompanyModel.GetMyCompany(Constants.user).CompanyID == id) || UserGroupModel.AmAnAdministrator(Constants.user)) && !CompanyModel.IsCompanyRegular(id))
            {
                TAJ compTAJ = new TAJ();

                if (id != 0)
                {
                    //Get TAJ credentials for company
                    compTAJ = TAJModel.GetCompanyTAJCredentials(id);
                    compTAJ.Company = CompanyModel.GetCompany(id);

                    return View(compTAJ);
                }
                else return RedirectToAction("Index", "Home");
            }

            else return RedirectToAction("Index", "Home");

        }


        /// <summary>
        /// This function is used to set a company's TAJ credentials
        /// </summary>
        /// <param name="id">Company Id</param>
        [HttpPost]
        public ActionResult TAJCredentials(TAJ CompTAJCredentials)
        {
            if (UserGroupModel.IsAdministrative(Constants.user) && ((CompanyModel.GetMyCompany(Constants.user).CompanyID == CompTAJCredentials.Company.CompanyID) || UserGroupModel.AmAnAdministrator(Constants.user)) && !CompanyModel.IsCompanyRegular(CompTAJCredentials.Company.CompanyID))
            {
                if (ModelState.IsValid)
                {

                    if (TAJModel.EditCompTAJCredentials(CompTAJCredentials))
                    {
                        TempData["EmailSent"] = "TAJ Credentials Modified!";
                    }

                    return RedirectToAction("Index", "Home");
                }
                else return View(CompTAJCredentials);
            }
            else return RedirectToAction("Index", "Home");
        }


    }
}
