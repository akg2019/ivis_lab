﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;
using System.Web.Routing;
using System.Text.RegularExpressions;
using OmaxFramework;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.IO;

namespace Honeysuckle.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {

        /// <summary>
        /// This function displays the home page for the application
        /// </summary>
        public ActionResult Index(string message =null)
        {
            bool debug = false;
            #if DEBUG
            debug = true;
            #else
               debug = false;
            #endif

           // if (!Request.IsLocal) return RedirectToAction("PublicMessage");

            if (Request.Url.Scheme == "http")
            {
               //Response.Redirect("https://ivisja.com/"); // + System.Web.HttpContext.Current.Request.Url.Host + (System.Web.HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + System.Web.HttpContext.Current.Request.Url.Port));
            }

            if (TempData["FileUpload"] != null)
            {
                ViewData["FileUpload"] = TempData["FileUpload"];
                TempData["FileUpload"] = null;
            }
            if (TempData["permissionMessage"] != null)
            {
                ViewData["permissionMessage"] = TempData["permissionMessage"];
                TempData["permissionMessage"] = null;
            }
            if (message != null)
            {
                ViewData["Message"] = "Policy Created!";
            }

            if (TempData["reset"] != null)
            {
                ViewData["reset"] = TempData["reset"];
                TempData["reset"] = null;
            }

            if (TempData["error"] != null)
            {
                ViewData["error"] = TempData["error"];
                TempData["error"] = null;
            }

            if (UserModel.TestGUID(Constants.user))
            {

                if (TempData["EmailSent"] != null)
                {
                    ViewData["Message"] = TempData["EmailSent"];
                    TempData["EmailSent"] = null;
                }
            }
            if (Honeysuckle.Models.Constants.user.username == "not initialized")
            {
                if (Request.Cookies.AllKeys.Contains("Cookie"))
                {
                    HttpCookie cookie = Request.Cookies["Cookie"];
                    //User u = UserModel.getUserFromGUID(cookie.Values.Get("gusession"));
                    try
                    {
                        Constants.user = UserModel.getUserFromGUID(Honeysuckle.Models.RSA.Decrypt(Request.Cookies["Cookie"]["sessions"], "session", int.Parse(Request.Cookies["Cookie"]["kid"])));
                    }
                    catch(Exception e){}
                    UserModel.Logout();
                    Constants.user = null;

               }
            }
            return View();
        }


        [HttpPost]
        public ActionResult Index(User user, string submit)
        {
            

            if (Constants.user.username == "not initialized")
            {
                switch (submit)
                {
                    case "Sign In":
                        if (user.username != null)
                        {
                            if (user.password != null)
                            {
                                user_login_object result = UserModel.LoginUser(user);

                                if (result.valid_user && !result.session_free && result.company_active)
                                {

                                    if (!result.active)
                                    {
                                        ViewData["error"] = "The user account you are trying to access is inactive, please contact your system administrator for assistance. ";
                                        return View(user);
                                    }

                                    //IF USER IS LOGGING IN FROM SECOND LOCATION
                                    ViewData["error"] = "This account is already authenticated. Please contact your administrator.";
                                    if (!result.session_free)
                                    {
                                        User tempuser = UserModel.GetUser(user.username);
                                        List<User> to = CompanyModel.GetMyCompanyAdmins(tempuser);
                                        to.Add(tempuser);
                                        List<string> toList = new List<string>();
                                        foreach (User u in to)
                                        {
                                            toList.Add(EmailModel.GetPrimaryEmail(u).email);
                                        }

                                        MailModel.SendMailWithThisFunction(to, new Mail(toList, "Double Login", "We detected a double login attempt into your account: " + user.username + " on " + System.DateTime.Now.ToString("dddd, MMMM dd, yyyy") + " at " + System.DateTime.Now.ToString("h:mm tt.") + " If this was you, please disregard this email. If this was not you, please secure your account here: " + System.Web.HttpContext.Current.Request.Url.AbsoluteUri + "  or contact your administrator, as someone else may be attempting to access it. \r\n \r\n  Thanks, IVIS Team."));
                                    }   
                                    return View(user);
                                }
                                else
                                {
                                    if (result.valid_user)
                                    {
                                        if (!result.company_active)
                                        {
                                            ViewData["error"] = "The company that this user is a part of has been disabled. Please contact the an administrator.";
                                            return View(user);
                                        }
                                        else if (result.first_login)
                                        {
                                            if (result.first_login_timer_lapsed)
                                            {
                                                //IF THIS IS THE FIRST TIME THIS USER IS LOGGING INTO THE SYSTEM
                                                string cryptname = Crypto.Encrypt(user.username, "epic-7234ivis903"); 
                                                RouteValueDictionary dict = new RouteValueDictionary();
                                                dict.Add("usernam", cryptname);
                                                //dict.Add("usernam", OmaxFramework.Utilities.InformationHiding.MakeIllusion(user.username));
                                                //Models.UserValidator.CurrentUser = OmaxFramework.Utilities.InformationHiding.MakeIllusion(user.username).ToString();
                                                Models.UserValidator.CurrentUser = cryptname;
                                                return RedirectToAction("FirstLogin", "Login", dict);
                                            }
                                            else
                                            {
                                                //timer times out show error message
                                                ViewData["error"] = "You have taken more than 24 hours to login in to the system. Please contact your administrator and have them renew your access.";
                                                return View(user);
                                            }
                                        }
                                        else
                                        {
                                            if (result.active)
                                            {
                                                if (!result.faulty_login_limit_passed)
                                                {
                                                    if (result.force_reset)
                                                    {
                                                        RouteValueDictionary dict = new RouteValueDictionary();
                                                        dict.Add("username", user.username);
                                                        dict.Add("reset", true);
                                                        //Constants.resetflag = true;
                                                        //Constants.user = user;
                                                        return RedirectToAction("ResetPassword", "Login", dict);
                                                    }
                                                    else
                                                    {
                                                        //SUCCESSFUL LOGIN
                                                        UserModel.ResetFaultyLogins(user.username); //reset fault login counter
                                                        User auth = UserModel.GetUser(user.username);
                                                        auth.sessionguid = UserModel.GetSessionGUID(auth);
                                                        Constants.user = auth;
                                                        Constants.user.UserGroups = UserModel.GetGroupsAndPermissions(Constants.user.ID);
                                                        
                                                        return RedirectToAction("Create", "Cookie", new { GUID = auth.sessionguid });
                                                    }
                                                }
                                                else
                                                {
                                                    //IF USER HAD PREVIOUSLY LOGGED IN ERRONEOUSLY MORE TIMES THAN ARE ALLOWED
                                                    ViewData["error"] = "You have made erroneous logins more than the system allows. Please contact the administrator to reactivate your account.";
                                                    return View(user);
                                                }
                                            }
                                            else
                                            {
                                                //ACCOUNT IS INACTIVE
                                                ViewData["error"] = "The user account you are trying to access is inactive, please contact your stystem administrator for assistance. ";
                                                UserModel.Logout(user.username);
                                                return View(user);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //BAD LOGIN INFORMATION
                                        if (result.user_exist)
                                        {
                                            if (CompanyModel.IsCompanyEnabled(CompanyModel.GetMyCompany(UserModel.GetUser(user.username))))
                                            {
                                                if (result.faulty_login_limit_passed)
                                                {
                                                    if (!result.active)
                                                    {
                                                        ViewData["error"] = "The user account you are trying to access is inactive, please contact your stystem administrator for assistance. ";
                                                        return View(user);
                                                    }

                                                    else
                                                    {
                                                        ViewData["error"] = "You have made erroneous logins more than the system allows. Please contact the administrator to reactivate your account.";
                                                        return View(user);
                                                    }
                                                }
                                                else
                                                {
                                                    if (result.db_error) ViewData["error"] = "System error. Unable to connect. Please contact your administrator or try back later.";
                                                    else
                                                    {
                                                        UserModel.FaultyLogin(user.username); // increment fault login
                                                        ViewData["error"] = "The username or password you submitted is incorrect."; //bad password?
                                                    }
                                                }
                                            }
                                            else ViewData["error"] = "The company for your user is yet to be enabled. Please contact the Administrator to enable your company";
                                        }
                                        else
                                        {
                                            if (result.db_error) ViewData["error"] = "System error. Unable to connect. Please contact your administrator or try back later.";
                                            else
                                            {
                                                ViewData["error"] = "The username or password you submitted is incorrect"; //user does not exist
                                            }
                                        }
                                        return View(user);
                                    }
                                }
                            }
                            else
                            {
                                //NO PASSWORD
                                ViewData["error"] = "Please enter a password when logging in";
                                return View(user);
                            }
                        }
                        else
                        {
                            //NO USERNAME
                            ViewData["error"] = "Please enter a username when logging in";
                            return View(user);
                        }
                    case "Forgot Password":
                        if (UserModel.IsActive(user.username))
                        {
                            if (!UserModel.FirstLogin(user.username))
                            {
                                if (UserModel.IsValidUser(user.username)) return RedirectToAction("AnswerQuestion", "Login", new { id = user.username });
                                else
                                {
                                    ViewData["error"] = "The username you have provided does not exist in the system.";
                                    return View(user);
                                }
                            }
                            else
                            {
                                ViewData["error"] = "This user is a new account. Please authenticate with the credentials that was sent to your email. If you did not receive any credentials please contact your administrator.";
                                return View(user);
                            }
                        }
                        else
                        {
                            ViewData["error"] = "This user has been deactivated from the system. Please contact your administrator.";
                            return View(user);
                        }

                    case "Clear":
                        return View();
                    default:
                        return View(user);

                }
            }
            else return new EmptyResult();
        }

        /// <summary>
        /// This function returns the ABOUT page of the application
        /// </summary>
        public ActionResult About()
        {
            return View();
        }




    
        /// <summary>
        /// This function retrieves all the insured individuals associated with a particular policy
        /// </summary>
        /// <param name="Id">Policy ID</param>
        /// <returns>A list of people insured to a Risk- and policy</returns>
        public static List<Person> GetInsuredList(int PolicyId)
        {
            List<Person> persons = new List<Person>();
            Address add = new Address();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetInsured " + PolicyId, "read", "person", new Person());
                while (reader.Read())
                {
                    Person per = new Person();
                    per.PersonID = int.Parse(reader["PeopleID"].ToString());
                    per.fname = reader["FName"].ToString();
                    per.lname = reader["LName"].ToString();
                    per.TRN = reader["TRN"].ToString();

                    add.city = new City(reader["City"].ToString());
                    add.roadnumber = reader["RoadNumber"].ToString();
                    add.road = new Road(reader["RoadName"].ToString());
                    add.country = new Country(reader["CountryName"].ToString());

                    per.address = add;

                    persons.Add(per);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return persons;
        }

        /// <summary>
        /// This function retrieves all the insured companies associated with a particular policy
        /// </summary>
        /// <param name="Id">Policy ID</param>
        /// <returns>A list of companies insured to a Risk- and policy</returns>
        public static List<Company> GetInsuredCompanies(int PolicyId)
        {
            List<Company> companies = new List<Company>();
            Address add = new Address();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetInsuredCompanies " + PolicyId, "read", "company", new Company());
                while (reader.Read())
                {
                    Company com = new Company();
                    com.CompanyID = int.Parse(reader["CompanyID"].ToString());
                    com.CompanyName = reader["CompanyName"].ToString();
                    com.IAJID = reader["IAJID"].ToString();

                    add.city = new City(reader["City"].ToString());
                    add.roadnumber = reader["RoadNumber"].ToString();
                    add.road = new Road(reader["RoadName"].ToString());
                    add.country = new Country(reader["CountryName"].ToString());

                    com.CompanyAddress = add;
                    companies.Add(com);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return companies;
        }
   
        //Site under construction
        public ActionResult PublicMessage(){return View();}

        /// <summary>
        /// Action Menu Items for creating covernotes, certs, Policy and Generate cover report in .csv 
        /// </summary>
        /// <returns></returns>
        public ActionResult ActionMenu() { return View(); }

        /// <summary>
        /// Generate Cover Report
        /// </summary>
        /// <returns></returns>
        public JObject GenerateCoverReport(string TypeOfCover ="both",string Intermediary = "0",string FromDate = "", string ToDate = "") 
        {
            JObject Response = new JObject();
            StringBuilder sb = new StringBuilder();
            Response["file"] = "";
            Response["success"] = false;
            DateTime F = new DateTime();
            DateTime T = new DateTime();

            //Extract Date from unfriendly date format to maintain control over date format going to the database
            try
            {
                var FD = FromDate.Split(' '); //Split from date
                var TD = ToDate.Split(' '); //Splt to date

                FromDate = FD[0].ToString() + " " + FD[1].ToString() + " " + FD[2].ToString() + " " + FD[3].ToString() + " " + FD[4].ToString(); //Concat from date 'MM/dd/yyyy'
                ToDate = TD[0].ToString() + " " + TD[1].ToString() + " " + TD[2].ToString() + " " + TD[3].ToString() + " " + TD[4].ToString(); //Concat to date 'MM/dd/yyyy'
            }
            catch (Exception)
            {
              
            }
            
            DateTime.TryParse(FromDate, out F); //Parse from date to ensure the data type is correct
            DateTime.TryParse(ToDate, out T); //Parse to date to ensure the data type is correct


            //Check for intermediary/CompanyType and initialize value to 0 to return all records for provided company else get company id from company name provided
            if (Intermediary == "All")
            {
                Intermediary = "0";
            }
            else
            {
                DataTable dts = (DataTable)Utilities.OmaxData.HandleQuery("SELECT COMPANYNAME, COMPANYID FROM COMPANY WHERE TYPE BETWEEN 1 AND 2 AND (COMPANYNAME = '" + Intermediary.Trim() + "')", sql.GetCurrentCOnnectionString(), Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery);
                JArray Interm = JArray.Parse(JsonConvert.SerializeObject(dts));
                               
                foreach (var CompanyIDs in Interm) { Intermediary = CompanyIDs["COMPANYID"].ToString(); }
            }
            
            //Build report query to retrieve data
            string Query = "EXEC GetCoverInformationDump " + Constants.user.CompanyID + "," + Intermediary + ",'" + TypeOfCover + "'";
            if (F.Year > 2000 && T.Year > 2000) Query += (",'" + F + "','" + T + "'");

            DataTable dt = (DataTable)Utilities.OmaxData.HandleQuery(Query, sql.GetCurrentCOnnectionString(), Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery);
            JArray ReportData = JArray.Parse(JsonConvert.SerializeObject(dt));

            //Build CSV File
            string CompName = "";
            try { CompName = Constants.user.employee.company.CompanyName; }catch (Exception) { }
          
            string fileName = "GeneratedCoverReports\\" + CompName + "-coverreport-" + DateTime.Now.ToString("yyyyMMddHHmm") + ".csv";
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);
            Response["file"] = path;
            
            //Header row
            sb.Append("Date Created,Insurer,Intermediary,Policy Number,Start Date,End Date,Vehicle Description,Cover Status,Comment,Type Of Cover,Certificate No/Cover Note No,Create By" + Environment.NewLine);

            //Report Body Content
            foreach (var Row in ReportData) 
            {
              string Record = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}",
                  DateTime.Parse(Row["CreatedOn"].ToString()).ToString("yyyy-dd-MM")
                  ,Row["Insurer"].ToString()
                  , Row["Intermediary"].ToString()
                  , Row["PolicyNumber"].ToString()
                  , DateTime.Parse(Row["StartDate"].ToString()).ToString("yyyy-dd-MM")
                  , DateTime.Parse(Row["EndDate"].ToString()).ToString("yyyy-dd-MM")
                  , Row["VehicleDescription"].ToString()
                  , Row["CoverStatus"].ToString()
                  , Row["Comment"].ToString()
                  , Row["TypeOfCover"].ToString()
                  , Row["InternalCertificateNo"].ToString()
                  , Row["CreateBy"].ToString());

              sb.Append(Record + Environment.NewLine);
            }

            //Indicate that the transaction was successful and there is a file to be downloaded
            Response["success"] = true; 

            //Check if base directory exist and creates it if otherwise
            try { if (!System.IO.Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "GeneratedCoverReports"))) System.IO.Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "GeneratedCoverReports")); }
            catch (Exception) { Response["success"] = false; }

            //Writes report content to the existing .csv file created earlier
            using (var stream = System.IO.File.Create(path)) { }
            System.IO.File.AppendAllText(path, sb.ToString());
            
            //Returns the formatted path for the report file to be downloaded.
            return Response; 
        }



        /// <summary>
        /// Downloads a template example for companies to use for importing their configuration data
        /// </summary>
        /// <param name="TypeOfTemplate">Type of template to be downloaded</param>
        /// <returns></returns>
        public JObject GenerateImportTemplates(string TypeOfTemplate = "")
        {
            JObject Response = new JObject();
            StringBuilder sb = new StringBuilder();
            Response["file"] = "";
            Response["success"] = false;
           
            //Build CSV File
            string CompName = "";
            try { CompName = Constants.user.employee.company.CompanyName; }
            catch (Exception) { }

            string fileName = "GeneratedCoverReports\\Templates\\" + CompName + "-" + TypeOfTemplate + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + ".csv";
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);
            Response["file"] = path;


            switch (TypeOfTemplate) 
            { 
               case "PolicyCover":
                    //Header row
                    sb.Append("Policy Cover Name,PolicyPrefix,isCommercialBusiness,IsMotorTrade,IsActive" + Environment.NewLine);
                    string Record = string.Format("{0},{1},{2},{3},{4}","Private Car comprehensive","PCC","False","False","False");
                    sb.Append(Record + Environment.NewLine);
                    break;
               case "Certificate":
                    //Header row
                    sb.Append(@"Type of Certificat,Cert ID,Cert Insured Code,Cert Extension Code,Flag Excluded Driver,Flag Excluded Insured,Flag Multiple Vehicle,Flag Registered Owner,Flag Restricted Driver,
                    Flag is For a Scheme,Is Excluded Driver,Is Excluded Insured,Has Multiple Vehicle,Is Registered Owner,Is Restricted Driver,Is for a Scheme,Auth Drivers,Lim of Use,Marks and Reg No,
                    Policy Holders,Veh Make,Text Auth Drivers,Text Lim of Use,Text Marks and Reg No,Text Policy Holders,Text Veh Make,Type of Cover,Policy Prefix,Usage,Cert Type" + Environment.NewLine);
                    string RecordCert = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30}",
                       "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    sb.Append(RecordCert + Environment.NewLine);
                    break;
               case "Usages":
                    //Header row
                    sb.Append("usage" + Environment.NewLine);
                    string RecordU = string.Format("{0}", "Socail Domestic and Pleasure");
                    sb.Append(RecordU + Environment.NewLine);
                    break;
               case "UsagesToCover":
                    //Header row
                    Response["success"] = false;
                    return Response;
                    //break;
            }

            //Indicate that the transaction was successful and there is a file to be downloaded
            Response["success"] = true;

            //Check if base directory exist and creates it if otherwise
            try { if (!System.IO.Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "GeneratedCoverReports\\Templates"))) System.IO.Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "GeneratedCoverReports\\Templates")); }
            catch (Exception) { Response["success"] = false; }

            //Writes content to the existing .csv file created earlier
            using (var stream = System.IO.File.Create(path)) { }
            System.IO.File.AppendAllText(path, sb.ToString());

            //Returns the formatted path for the template file to be downloaded.
            return Response;
        }



    }
}
