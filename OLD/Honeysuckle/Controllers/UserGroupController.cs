﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Honeysuckle.Models;

namespace Honeysuckle.Controllers
{
    public class UserGroupController : Controller
    {

        /// <summary>
        /// This function retrieves and returns all groups not associated with a specific user
        /// </summary>
        /// <param name="id">User Id</param>
        public ActionResult ListBoxUserGroups(int id = 0)
        {
            // SHOWS ALL GROUPS NOT ASSOCIATED TO A SPECIFIC USER
            List<UserGroup> usergroups = new List<UserGroup>();
            if (id != 0) usergroups = UserGroupModel.GetNotMyUserGroups(new User(id));
            else usergroups = UserGroupModel.GetAllUserGroups();
            return View(usergroups);
        }


        /// <summary>
        /// This function retrieves and returns all groups associated with a specific user
        /// </summary>
        /// <param name="id">User Id</param>
        public ActionResult MyListBoxUserGroups(int id = 0)
        {
            // SHOWS ALL GROUPS ASSOCIATED TO A SPECIFIC USER
            List<UserGroup> usergroups = new List<UserGroup>();
            if (id != 0) usergroups = UserGroupModel.GetMyUserGroups(new User(id));
            return View(usergroups);
        }


        /// <summary>
        /// This function retrieves and returns all children groups not associated to a specific group
        /// </summary>
        /// <param name="id">Usergroup Id</param>
        /// <param name="search">Input to be used to searched against</param>
        public ActionResult SuperGroupListBoxUserGroups(int id = 0, int comp = 0)
        {
            // SHOWS ALL CHILDREN GROUPS NOT ASSOCIATED TO A SPECIFIC GROUP
            List<UserGroup> usergroups = new List<UserGroup>();
            UserGroup ug = new UserGroup();
            if (comp != 0) ug.company = new Company(comp);
            if (id != 0)
            {
                ug.UserGroupID = id;
                if (comp != 0) usergroups = UserGroupModel.GetNotChildren(ug, comp);
                else usergroups = UserGroupModel.GetNotChildren(ug);
            }
            else usergroups = UserGroupModel.GetNotTheseGroups(usergroups, ug.company.CompanyID);
            return View(usergroups);
        }


        /// <summary>
        /// This function retrieves and returns all children groups associated to a specific group
        /// </summary>
        /// <param name="id">Usergroup Id</param>
        /// <param name="search">Input to be used to searched against</param>
        public ActionResult SuperGroupMyListBoxUserGroups(int id = 0, int comp = 0)
        {
            // SHOWS ALL CHILDREN GROUPS ASSOCIATED TO A SPECIFIC GROUP
            List<UserGroup> usergroups = new List<UserGroup>();
            if (id != 0) usergroups = UserGroupModel.GetChildren(new UserGroup(id));
            //else usergroups = Constants.creategroups;
            return View(usergroups);
        }

    }
}
