﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.CertCover>" %>

<div class="covercertnote">
<div class="note-meat"> 
 <% if (Model.Type == "Cover")
    { %>
    <a href="<%= Url.Action("Approve", "VehicleCoverNote", new {id = Model.vCoverNote.ID}) %>" class="covercert_remove icon" title="Approve CoverNote" >
      <img src="../../Content/more_images/mark_read_icon.png" class="sprite" />
    </a>
<%: Html.HiddenFor(model => model.vCoverNote.ID, new { @class = "NID dont-process", @id = "note-covercert-id" })%>
<%: Html.Hidden("cover", "covernote", new { @class = "NID dont-process", @id = "note-covercert" })%>
<%: Html.Hidden("note-hidden-type", Model.Type, new { @id = "note-hidden-type", @class = "dont-process" })%>
<%} %>
<% else
    { %>
    <a href="<%= Url.Action("ApproveCert", "VehicleCertificate", new {certificateId = Model.vCertificate.VehicleCertificateID}) %>" class="covercert_remove icon" title="Approve Certificate" >
      <img src="../../Content/more_images/mark_read_icon.png" class="sprite" />
    </a>
<%: Html.HiddenFor(model => model.vCertificate.VehicleCertificateID, new { @class = "NID dont-process", @id = "note-covercert-id" })%>
<%: Html.Hidden("cert", "certificate", new { @class = "NID dont-process", @id = "note-covercert" })%>
<%: Html.Hidden("note-hidden-type", Model.Type, new { @id = "note-hidden-type", @class = "dont-process" })%>
<% } %>
<div class="note_heading"><%: Model.Type %></div> 
<div class="covercert_content"><% if (Model.Type == "Cover")
                             { %> <%: Model.vCoverNote.covernoteid%> <% }
                             else
                             {%><%: Model.vCertificate.CertificateNo%><%} %>
                        
</div>
</div>
 <div class="note-block"></div>
</div>

