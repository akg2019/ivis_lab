﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Notification>" %>

    <div class="notification-s">
        <%: Html.Hidden("note-id-bad", Model.ID, new { @class = "note-id dont-process" })%>
        <div class="line">
            <div class="heading">
                <div class="star">
                <% if (!Model.read)
                   { %>
                    <img src="../../Content/more_images/Star_Unread.png" class="unread-star" />
                <% } %>
                </div>
                <span><%: Model.heading%></span>
            </div>
            <div class="from italic">
                From: <%: Model.NoteFrom.username%>
            </div>
            <div class="date italic">
                Date: <%: Model.timestamp.ToString("MMM d, yyyy - hh:mm tt")%>
            </div>
        </div>
        <div class="line">
            <div class="line2">
                <div class="content italic">
                    <%: Model.content%>
                </div>
                <div class="ellipses italic">...</div>
                <div class="action">
                    <div class="more mail-btn"><img src="../../Content/more_images/Read_More.png" /></div>
                    <% if (ViewData["pagename"].ToString() != "trash") { %> <div class="garbage mail-btn"><img src="../../Content/more_images/To-Trash.png" /></div> <% } %>
                    <div class="less mail-btn"><img src="../../Content/more_images/Read_less.png" /></div>
                </div>
            </div>
        </div>
    </div>