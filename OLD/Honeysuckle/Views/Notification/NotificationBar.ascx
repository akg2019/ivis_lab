﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Notification>>" %>


<div id="notes" class="note-segment">
    <div id="notelists">
        <div id="notesscroll">
        <% Html.RenderPartial("~/Views/Notification/ShowNotification.ascx", new Honeysuckle.Models.Notification()); %>
            <%--<% foreach (var item in Model) { %>
                    <% Html.RenderPartial("~/Views/Notification/ShowNotification.ascx", item); %>
            <% } %>--%>
        </div>
    </div>
    <div id="seefullnotify"><%: Html.ActionLink("All Notifications", "Index", "Notification") %></div>
    <div class="notify-borders"></div>
</div>
    
   
