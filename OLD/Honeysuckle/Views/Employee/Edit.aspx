﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.EmployeeViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Edit</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <legend>Fields</legend>
            
            <%: Html.HiddenFor(model => model.employee.EmployeeID) %>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.employee.person.fname)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.employee.person.fname, new { id = "fname" })%>
                <%: Html.ValidationMessageFor(model => model.employee.person.fname)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.employee.person.mname)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.employee.person.mname, new { id = "mname" })%>
                <%: Html.ValidationMessageFor(model => model.employee.person.mname)%>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.employee.person.lname)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.employee.person.lname, new { id = "lname" })%>
                <%: Html.ValidationMessageFor(model => model.employee.person.lname)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.employee.company.CompanyName)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.employee.company.CompanyName, new { id = "companyName" })%>
                <%: Html.ValidationMessageFor(model => model.employee.company.CompanyName)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.employee.Active)%>
            </div>
            <div class="editor-field">
                <%: Html.CheckBoxFor(model => model.employee.Active, new { id = "active", @disabled = "disabled" })%>
                <%: Html.ValidationMessageFor(model => model.employee.Active)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.employee.person.address.roadnumber)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.employee.person.address.roadnumber, new { id = "roadno" })%>
                <%: Html.ValidationMessageFor(model => model.employee.person.address.roadnumber)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.employee.person.address.road.RoadName)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.employee.person.address.road.RoadName, new { id = "RoadName" })%>
                <%: Html.ValidationMessageFor(model => model.employee.person.address.road.RoadName)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.employee.person.address.roadtype.RoadTypeName)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.employee.person.address.roadtype.RoadTypeName, new { id = "RoadType" })%>
                <%: Html.ValidationMessageFor(model => model.employee.person.address.roadtype.RoadTypeName)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.employee.person.address.zipcode.ZipCodeName)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.employee.person.address.zipcode.ZipCodeName, new { id = "ZipCode" })%>
                <%: Html.ValidationMessageFor(model => model.employee.person.address.zipcode.ZipCodeName)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.employee.person.address.city.CityName)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.employee.person.address.city.CityName, new { id = "CityName" })%>
                <%: Html.ValidationMessageFor(model => model.employee.person.address.city.CityName)%>
            </div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.employee.person.address.country.CountryName)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.employee.person.address.country.CountryName, new { id = "CountryName" })%>
                <%: Html.ValidationMessageFor(model => model.employee.person.address.country.CountryName)%>
            </div>

            <p>
                <input type="submit" value="Save" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Back to List", "Index") %>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftBar" runat="server">
</asp:Content>

