﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Email>" %>

<div class="editor-label">
    <%: Html.LabelFor(model => model.email) %>
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.email) %>
</div>
            
<div class="editor-label">
    <%: Html.LabelFor(model => model.primary) %>
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.primary) %>
</div>
