﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.TemplateWording>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div id="wording-page">
        <div id="page-title">
            <img src="../../Content/more_images/Wording_Header.png" />
            <div class="form-text-links">
                <span class="form-header-text">Template Wording Edit</span>
                <div class="icon">
                    <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
               </div>
            </div>
        </div>
        
        <div class="field-data">
            <%: Html.Hidden("page", "edit", new { @class = "dont-process" })%>
        
            <% using (Html.BeginForm()) {%>
                <%: Html.ValidationSummary(true) %>
        
                <fieldset>

                    <div class="validate-section">
                        <div class="subheader">
                            <div class="arrow right down"></div>
                            Main Template Wording Data
                            <div class="valid"></div>
                        </div>
                        <div class="data">
                            <%: Html.HiddenFor(model => model.ID, new { @class = "dont-process" })%>

                            <div class="editor-label">
                                Certificate Type:
                            </div> 
                            <div class="editor-field input">
                                <%: Html.TextBoxFor(model => model.CertificateType, new { id = "certtype", @required = "required", @class = "certtype" })%>
                            </div> 
                            <div class="editor-label">
                                Certificate Insurance Code:
                            </div> 
                            <div class="editor-field input">
                                <%: Html.HiddenFor(model => model.CertificateInsuredCode, new { id = "realccode", @class = "dont-process" })%>
                                <%: Html.DropDownList("inscode", new SelectList(new List<Object>() { new { value = "indv", text = "indv" }, new { value = "jtin", text = "jtin" }, new { value = "inco", text = "inco" }, new { value = "blnk", text = "blnk" }, new { value = "comp", text = "comp" } }, "value", "text"), new { id = "certinscodedropdown", @required = "required" })%>
                            </div> 
                            <div class="check-field">
                                <div class="editor-label">
                                    Excluded Driver:
                                </div> 
                                <div class="editor-field input">
                                    <%: Html.CheckBoxFor(model => model.ExcludedDriver, new { id = "excdri", @class = "word-check" })%>
                                </div> 
                            </div>
                            <div class="check-field">
                                <div class="editor-label">
                                    Excluded Insured:
                                </div> 
                                <div class="editor-field input">
                                    <%: Html.CheckBoxFor(model => model.ExcludedInsured, new { id = "excins", @class = "word-check" })%>
                                </div> 
                            </div>
                            <div class="check-field">
                                <div class="editor-label">
                                    Multiple Vehicle:
                                </div> 
                                <div class="editor-field input">
                                    <%: Html.CheckBoxFor(model => model.MultipleVehicle, new { id = "mulveh", @class = "word-check" })%>
                                </div> 
                            </div>
                            <div class="check-field">
                                <div class="editor-label">
                                    Registered Owner:
                                </div> 
                                <div class="editor-field input">
                                    <%: Html.CheckBoxFor(model => model.RegisteredOwner, new { id = "regown", @class = "word-check" })%>
                                </div> 
                            </div>
                            <div class="check-field">
                                <div class="editor-label">
                                    Restricted Driver:
                                </div>
                                <div class="editor-field input">
                                    <%: Html.CheckBoxFor(model => model.RestrictedDriver, new { id = "regdri", @class = "word-check" })%>
                                </div> 
                            </div>
                            <div class="check-field">
                                <div class="editor-label">
                                    Scheme:
                                </div> 
                                <div class="editor-field input">
                                    <%: Html.CheckBoxFor(model => model.Scheme, new { id = "sch", @class = "word-check" })%>
                                </div> 
                            </div>
                            <div class="editor-label">
                                Extension Code:
                            </div> 
                            <div class="editor-field input">
                                <%: Html.TextBoxFor(model => model.ExtensionCode, new { id = "extcode", @class = "read-only ext-code", @readonly = "readonly", @required = "required" })%>
                            </div> 
                            <div class="editor-label">
                                Certificate ID:
                            </div> 
                            <div class="editor-field input">
                                <%: Html.TextBoxFor(model => model.CertificateID, new { id = "certid", @class = "read-only certid", @readonly = "readonly", @required = "required" })%>
                            </div>
                        </div>
                    </div>

                    <div class="validate-section">
                        <div class="subheader">
                            <div class="arrow right"></div>
                            Template
                            <div class="valid"></div>
                        </div>
                        <div class="data">
                            <div class="editor-label">
                                Vehicle Registration No:
                            </div> 
                            <div class="editor-field input">
                                <div class="text-area-section">
                                    <%: Html.TextAreaFor(model => model.VehRegNo, new { id = "vehregno", @class = "text-area-cleditor"})%>
                                    <div class="text-area-btns">
                                        <button type="button" onclick="TemplateTags('vehregno',this)">< Vehicle Registration No. ></button>
                                    </div>
                                </div>
                            </div> 
                            <div class="editor-label">
                                Vehicle Description:
                            </div> 
                            <div class="editor-field input">
                                <div class="text-area-section">
                                    <%: Html.TextAreaFor(model => model.VehicleDesc, new { id = "vehdes", @class = "text-area-cleditor"})%>
                                    <div class="text-area-btns">
                                        <button type="button" onclick="TemplateTags('vehmake',this)">< Vehicle Make ></button>
                                        <button type="button" onclick="TemplateTags('vehyear',this)">< Vehicle Year ></button>
                                        <button type="button" onclick="TemplateTags('vehmod',this)">< Vehicle Model ></button>
                                        <button type="button" onclick="TemplateTags('vehmodtype',this)">< Vehicle Model Type ></button>
                                        <button type="button" onclick="TemplateTags('vehext',this)">< Vehicle Extension ></button>
                                        <button type="button" onclick="TemplateTags('vehregno',this)">< Vehicle Registration No ></button>
                                    </div>
                                </div>
                            </div> 
                            <div class="editor-label">
                                Policy Holders: <br > <small><i>Use carriage return <b>_CR_</b> to insert new line </i></small>
                            </div> 
                            <div class="editor-field input">
                                <div class="text-area-section">
                                    <%: Html.TextAreaFor(model => model.PolicyHolders, new { id = "polhol", @class = "text-area-cleditor"})%>
                                    <div class="text-area-btns">
                                        <button type="button" onclick="TemplateTags('ins',this)">< Vehicle Main Insureds ></button>
                                    </div>
                                </div>
                            </div> 
                            <div class="editor-label">
                                Authorized Drivers: <br > <small><i>Use carriage return <b>_CR_</b> to insert new line </i></small>
                            </div> 
                            <div class="editor-field input">
                                <div class="text-area-section">   
                                    <%: Html.TextAreaFor(model => model.AuthorizedDrivers, new { id = "autdri", @class = "text-area-cleditor"})%>
                                    <div class="text-area-btns">
                                        <button type="button" onclick="TemplateTags('names',this)">< Vehicle Driver Names ></button>
                                        <button type="button" onclick="TemplateTags('auth',this)">< Vehicle Authorized Drivers ></button>
                                    </div>
                                </div>
                            </div> 
                            <div class="editor-label">
                                Limits of Use: <br > <small><i>Use carriage return <b>_CR_</b> to insert new line </i></small>
                            </div> 
                            <div class="editor-field input">
                                <%: Html.TextAreaFor(model => model.LimitsOfUse, new { id = "limuse", @class = "text-area-cleditor"})%>
                            </div> 
                        </div>
                    </div>
                    
                    <div class="btnlinks">
                        <% Html.RenderAction("BackBtn", "Back"); %>
                        <input type="submit" value="Submit" class="submitbutton" />
                    </div>
                
                </fieldset>

            <% } %>
    
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/wording.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/wording.js" type="text/javascript"></script>
</asp:Content>


