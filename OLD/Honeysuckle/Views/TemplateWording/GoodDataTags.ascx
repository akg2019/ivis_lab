﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<% if ((bool)ViewData["maplock"])
   { %>
        <%: Html.DropDownList("dd", new SelectList(new[] { new SelectListItem() { Text = "None Selected", Value = "empty"}, 
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Year>", Value = "vy"}, 
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Make>", Value = "vm" }, 
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Model>", Value = "vmo" },
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Model Type>", Value = "vmt" }, 
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Extension>", Value = "ve" }, 
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Reg No>", Value = "vrn" },
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Main Driver Name>", Value = "vmdn" }, 
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Driver Names>", Value = "vdn" },
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Authorized Drivers>", Value = "vad" },
                                                           new SelectListItem() { Text = "<[Policy Info]Main Insured>", Value = "mi" }
                                                        },
                                    "Value", "Text"),
                                    new { @class = "wording_tags", @disabled = "disabled" })%>
<% } %>
<% else
   { %>
        <%: Html.DropDownList("dd", new SelectList(new[] { new SelectListItem() { Text = "None Selected", Value = "empty"}, 
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Year>", Value = "vy"}, 
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Make>", Value = "vm" }, 
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Model>", Value = "vmo" },
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Model Type>", Value = "vmt" }, 
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Extension>", Value = "ve" }, 
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Reg No>", Value = "vrn" },
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Main Driver Name>", Value = "vmdn" }, 
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Driver Names>", Value = "vdn" },
                                                           new SelectListItem() { Text = "<[Risk Items]Veh Authorized Drivers>", Value = "vad" },
                                                           new SelectListItem() { Text = "<[Policy Info]Main Insured>", Value = "mi" }
                                                        },
                                    "Value", "Text"),
                                    new { @class = "wording_tags" })%>
<% } %>