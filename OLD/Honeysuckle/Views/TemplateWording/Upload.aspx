﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Upload
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id= "form-header">
        <img src="../../Content/more_images/Wording_Header.png" />
        <div class="form-text-links">
            <span class="form-header-text">Template Wording Upload</span>
        </div>
    </div>

    <fieldset>
        <div class="field-data">
            <% using (Html.BeginForm("Upload", "TemplateWording", FormMethod.Post, new { enctype = "multipart/form-data" }))
               { %>
               <div id="instructions">File Specifications: Please upload text files under 5MB ONLY. Wording Templates and Cover/Usage Associations must have their specific headings as noted in manual. Each record must be in its own line and each field value of a record must be seperated by a tabular key (TAB).</div>
                <div class="files" ng-app="eGovApp" ng-controller="MyeGovController">
                    <div class="file-info">
                        <b class="btn btn-sm btn-primary" style="float:left!important;">Download Template</b>
                        <label for="file" style="padding-left:5px;padding-top:3px!important;"> Select File Wording Templates :</label>
                        <input type="file" name="file" value="" accept="text/plain" class="fileupload wording" />
                        <span id="wording-filename" class="filename"></span>
                    </div>
                    <div class="file-info">
                        <b class="btn btn-sm btn-primary" style="float:left!important;">Download Template</b>
                        <label for="file" style="padding-left:5px;padding-top:3px!important;" > Select File Cover/Usage Association :</label>
                        <input type="file" name="file" value="" accept="text/plain" class="fileupload usage" />
                        <span id="usage-filename" class="filename"></span>
                    </div>
                     <div class="file-info">
                        <b class="btn btn-sm btn-primary" style="float:left!important;" atl="Download Policy Cover Import Template" ng-click = "GenerateImportTemplates('PolicyCover')">Download Template</b>
                        <label for="file" style="padding-left:5px;padding-top:3px!important;"> Select File Policy Cover :</label>
                        <input type="file" name="file" value="" accept="text/plain" class="fileupload polcover" />
                        <span id="polcover-filename" class="filename"></span>
                    </div>
                </div>
                <%-- <input id="btnAdd" type="submit" class="button" name="Add" value="Upload"/> --%>
            <% } %>

            <div class="response-div">
            
                <p><% try{ViewData["usage_response"].ToString();}catch(Exception){} %></p>
            </div>

            <% if (ViewData["error"] != null) ViewData["error"].ToString(); %>
            <% if (ViewData["response"] != null) Html.RenderAction("ParseResponse", "Response", new { responses = ViewData["response"] }); %>
            <% if (ViewData["usage_response"] != null) Html.RenderAction("ParseResponse", "Response", new { responses = ViewData["usage_response"] }); %>
        </div>
    </fieldset>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">

    <link href="../../Scripts/jquery-upload/css/jquery.fileupload.css" rel="stylesheet" type="text/css" />
    
    <script src="../../Scripts/jquery-upload/js/vendor/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-upload/js/jquery.iframe-transport.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-upload/js/jquery.fileupload.js" type="text/javascript"></script>

    <link href="../../Content/responses.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/responses.js" type="text/javascript"></script>
    <link href="../../Content/wording-upload.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/wording-upload.js" type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
