﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.Vehicle>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm())
       { %>
       <br />
        <div class="search-span">
            Search:
        </div>
        <%: Html.DropDownList("parameter", new SelectList(new List<Object>{
                                                                new { Text = "all", Value = "all" },                                                            
                                                                new { Text = "make", Value = "make" },
                                                                new { Text = "model", Value = "model" },
                                                                new { Text = "model type", Value = "modelType" },
                                                                new { Text = "body type", Value = "bodyType" },
                                                                new { Text = "extension", Value = "extension" },
                                                                new { Text = "vehicle registration no.", Value = "vehicleRegNumber" },
                                                                new { Text = "vin", Value = "VIN" }
                                                                }, 
                                                            "Value", "Text")
                                )%>
        
        <div class="search-text">
            <%: Html.TextBox("search") %>
        </div>
        <div class="search-button">
            <input type="submit" name="submit" value="Search" />
        </div>

        <div id="notActive" style="display:none;"> 
        <%: ViewData["No Active Policy"] %>
        </div>

    <% } %>

    <!-- CHECKS IF THE CURRENT USER BELONGS TO AN INSURANCE COMPANY- As only insurance companies should view the dropdown list-->
   
    <% if (Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID)) { %>
           <h4> View Vehicles made only by: </h4> 
          <% Html.RenderAction("CompanyDropDown", "Company", new {type= "IntermediaryCompanies" }); %>
     <% } %>
     <br /> <br /> <br />


    <table>
        <tr>
            <th></th>
            <th>
                make
            </th>
            <th>
                model
            </th>
            <th>
                modelType
            </th>
            <th>
                bodyType
            </th>
            <th>
                extension
            </th>
            <th>
                vehicleRegNumber
            </th>
            <th>
                VIN
            </th>
            
        </tr>

    <% foreach (var item in Model) { %>
        <tr>
            <td>
                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Vehicles", "Update")))
                  { %> 
                       <%: Html.ActionLink("Edit", "Edit", new {  id=item.ID  }) %> |
               <% } %>


               <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Vehicles", "Update")))
                  { %> 
                       <%: Html.ActionLink("Edit Drivers", "EditDrivers", new {  id=item.ID  }) %> |
               <% } %>


                <%: Html.ActionLink("Details", "Details", new { id=item.ID })%> |

                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Vehicles", "Delete")))
                  { %> 
                        <%: Html.ActionLink("Delete", "Delete", new {  id=item.ID})%> |
               <% } %>


                <% if (Honeysuckle.Models.VehicleCoverNoteModel.IsActiveCoverNoteExist(new Honeysuckle.Models.Vehicle(item.ID))) 
                   { %>
                    <%: Html.ActionLink("View Current Cover Note", "Details", "VehicleCoverNote", new { id = Honeysuckle.Models.VehicleCoverNoteModel.GetCurrentCoverNote(new Honeysuckle.Models.Vehicle(item.ID)).ID }, null)%> | 
                <% } %>
                <% else 
                   { %>
                    <%: Html.ActionLink("Create Cover Note", "Create", "VehicleCoverNote", new { id = item.ID }, null) %> | 
                <% } %>


                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Read")))
                   { %>
                        <% if (Honeysuckle.Models.VehicleCertificateModel.IsVehicleUnderActiveCert(item.ID)) 
                           { %>
                                 <%: Html.ActionLink("View Current Certificate", "Details", "VehicleCertificate", new { id = Honeysuckle.Models.VehicleCertificateModel.GetThisVehicleActiveCertificate(item.ID).VehicleCertificateID }, null)%> | 
                        <% } %>
                        <% else
                           { %>
                                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Create")))
                                   { %> 
                                     <%: Html.ActionLink("Create Certificate", "Create", "VehicleCertificate", new {  id=item.ID}, null) %> 
                                <% } %>
                        <% } %>
                   <% } %>

            </td>
            <td>
                <%: item.make %>
            </td>
            <td>
                <%: item.VehicleModel %>
            </td>
            <td>
                <%: item.modelType %>
            </td>
            <td>
                <%: item.bodyType %>
            </td>
            <td>
                <%: item.extension %>
            </td>
            <td>
                <%: item.vehicleRegNumber %>
            </td>
            <td>
                <%: item.VIN %> 
            </td>
            
        </tr>
    
    <% } %>

    </table>

    <p>
    <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Vehicles", "Create")))
        { %> 
             <%: Html.ActionLink("Create New", "Create") %>
     <% } %>
    </p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/GetIntermediaryData.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/NoActivePolicy.js" type="text/javascript"></script>
    <link href="../../Content/Pages.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

