﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Vehicle>" %>

 <div class="newvehicle">
   <div class="blurDiv">
        <% using (Html.BeginCollectionItem("vehicles")) {%>
            <%: Html.ValidationSummary(true) %>

            <% string RegNo = "Not Available";
               if (Model.VehicleRegNumber != null && Model.VehicleRegNumber != "") RegNo = Model.VehicleRegNumber; %>

            <%: Html.HiddenFor(model => model.ID, new { @class = "vehId dont-process" })%> 
            <%: Html.HiddenFor(model => model.base_id, new { @class = "base_veh_id dont-process" })%>
            <%: Html.HiddenFor(model => model.chassisno, new { @class = "chassis dont-process" })%>
           
            <div class="line">
                <span>Vehicle Information:</span> <%: Html.TextBox("vehicleDataDisplay", ViewData["vehicleInfoDisplay"], new { @readonly = "readonly", @class= "vehData", @atl = "Click to Edit Vehicle Information"})%> 
            </div>
            <div class="line">
                <span>Registration No.:</span> <%: Html.TextBox("vehicleCardRegNo", RegNo , new {  @class = "text-indent vehRegNo", @readonly = "readonly" }) %>
                <%: Html.HiddenFor(model => model.VehicleRegNumber) %>
            </div>
            <div class="line">
                <span>Chassis Number:</span>
                <div class="data-field1">
                    <%: Html.TextBoxFor(model => model.chassisno, new {  @class="chassisNumber",@readonly = "readonly"})%> 
                    <%: Html.HiddenFor(model => model.VIN, new {  @class = "vehicle_vin dont-process" })%> 
                    <%-- %><%: Html.HiddenFor(model => model.usage.ID, new { id = "vehUsageID", @class = "usageIdentity dont-process" })%> --%>
                </div>
            </div>

             
            <div class="line this_vehicle_dropdown dropdown_spacing"> 
                <span>Current Usage:</span>

                <div class="data-field1">
                    <%: Html.TextBoxFor(model => model.usage.usage, new { @readonly = "readonly", @class="form-control"})%> 
                </div>
            </div>

            <div class="line this_vehicle_dropdown dropdown_spacing"> 
                <span>Choose Usage:</span>

                <div class="useDropDown"> 
                    <%: Html.HiddenFor(model => model.usage.ID, new { @class = "vehicle-usage-id", @hidden ="hidden"  }) %>
                    <% Html.RenderAction("UsageDropdown", "Usage", new { coverID = ViewData["PolCoverId"] }); %> 
                </div>
            </div>
            <div class="line mortgagee">
                <span>Mortgagee: </span>
                <div class="actual_mortgagee">
                    <%: Html.TextBoxFor(model => model.mortgagee.mortgagee, new { @class = "vup_mortgagee" }) %>
                </div>
            </div>

           <% if (!(bool)ViewData["Create"] && Honeysuckle.Models.VehicleCoverNoteModel.IsVehUnderActiveNote((int)ViewData["VehicleID"], int.Parse(ViewData["polid"].ToString())))
              { %>
                 <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Read")))
                    { %>
                        <div class="line coverDetails">
                            <span>Active CoverNote:</span>
                            <div class="coverDetail">
                            <%: Html.TextBox("coverDetails", ViewData["CoverDetails"], new { @readonly = "readonly" })%> 
                            </div>
                        </div>
                 <% } %>
             <% } %>
            <%else
              { %>
                 <div class="line coverDetails">
                    <span>Active CoverNote:</span>
                    <div class="coverDetail">
                    <%: Html.TextBox("coverDetails", "No Active Cover Note", new { @readonly = "readonly", @class = "text-indent read-only" })%> 
                    </div>
                </div>
            <%} %>

            <div class="line coverHistory">
                <span>Cover Count:</span>
                <div class="coverDetails">
                <%: Html.TextBox("coverTotal", ViewData["coverHistory"], new { @readonly = "readonly", @id = "coverTotal", @class = "text-indent read-only" })%>
                </div>
            </div>

            <div class = "PrevPolicy">
                <div class="vehicleflag line">
                    <% if ((bool)ViewData["isinsured"] && !((bool)ViewData["isinsuredatmyco"]))
                        { %>
                        
                        <input type="button" value="Message Admin" id = "messageCompAdmin" class="messageCompAdmins" onclick="MessageAdmin();" /> 
                        
                        <span>Vehicle is under a policy at : <%: ViewData["compName"] %></span>
                        <div id="sendNote"> </div>
                    <% } %>
                </div>
                <div class="line cancelbtn">
                    <% if ((bool)ViewData["isinsuredatmyco"]) { %>
                        <% if ((bool)ViewData["details"] && !((bool)ViewData["VehUnderThisPolicy"]))
                           { %> <span class="fullmess">*Not Currently Active Under Policy. Please Edit Policy to Remove This Vehicle</span> <% } %>
                        <% else if (!((bool)ViewData["VehUnderThisPolicy"]))
                           { %> <input type="button" value="Cancel Previous Policy" id = "cancelPol"  class= "buttonCancel" onclick="CancelRiskFromPolicy();" /> <% } %>
                    <% } %>
                </div>
            
                <div class="history line">
                  <% if ((bool)ViewData["History"] && int.Parse(ViewData["polid"].ToString()) != 0 && Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Read")))
                     { %>
                        <button onclick="CoverNoteHistory(<%: int.Parse(ViewData["polid"].ToString()) %>,<%: Model.ID %>)" type="button">View Cover Note History</button>
                  <% } %>
                </div> 
            </div>
            <!-- THIS TEXTBOX IS ONLY SHOWN IN POLICY EDIT/DETAILS, TO ADVISE THE USER THAT EVEN THOUGH THE VEHICLE IS SHOWN UNDER  
                        THAT PARTICULAR POLICY, IT HAS BEEN CANCELLED FROM THE POLICY, AND A 'SAVE' MUST BE DONE TO RE-ADD IT TO THE POLICY-->
      </div>      

        <div class="vehicleFooter line">
            <% bool certExists = false; %>
            <% if (!((bool)ViewData["Create"] || (bool)ViewData["Edit"]))
               { %>
                 <% if (!(bool)Honeysuckle.Models.VehicleCertificateModel.IsVehicleUnderActiveCert(Model.ID, (DateTime)ViewData["PolStart"], (DateTime)ViewData["PolEnd"], int.Parse(ViewData["polid"].ToString()))) { %>
                    <% if ((bool)Honeysuckle.Models.VehicleCertificateModel.IsPolicyCertMade(Model.ID, int.Parse(ViewData["polid"].ToString()))) { %>
                          <% certExists = true; %>
                           <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Read"))) { %>
                                <%: Html.ActionLink("View Certificate", "GetThisInactiveCert", "VehicleCertificate", new { vehID = Model.ID, polID= int.Parse(ViewData["polid"].ToString()) }, null)%> 
                           <% } %>
                    <% } %>
                    <% else 
                       { %>
                         <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Create")))
                            { %>
                              <% if (!(bool)ViewData["expired"] && !(bool)ViewData["imported"])
                                { %>
                                    <%: Html.ActionLink("Create Certificate", "Create", "VehicleCertificate", new { id = Model.ID, polid = int.Parse(ViewData["polid"].ToString()) }, null)%> 
                             <% } %>
                         <% } %> 
                    <% } %>

                    <!--  AN ACTIVE COVER NOTE CAN NOT EXIST/BE MADE WHILE AN ACTIVE CERTIFICATE EXISTS -->
                    <% if (ViewData["pol"] == null) ViewData["pol"] = 0; %>
                        
                    <% if (!certExists){ %>
                        <% if (Honeysuckle.Models.VehicleCoverNoteModel.IsVehUnderActiveNote(Model.ID, int.Parse(ViewData["polid"].ToString())))
                           { %>
                             <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Read"))) { %>
                                  <%: Html.ActionLink("View Cover Note", "GetThisActiveNote", "VehicleCoverNote", new { id = Model.ID, start = ViewData["PolStart"].ToString(), end = ViewData["PolEnd"].ToString(), policyid = int.Parse(ViewData["polid"].ToString()) }, null)%> 
                             <% } %>
                        <% } %>
                        <% else { %>
                            <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Create"))) { %>
                                  <% if (!(bool)ViewData["expired"] && !(bool)ViewData["imported"] && Honeysuckle.Models.VehicleCoverNoteModel.CanIGenerateCoverNote(Model, new Honeysuckle.Models.Policy(int.Parse(ViewData["polid"].ToString()))))
                                     { %>
                                       <%: Html.ActionLink("Create Cover Note", "Create", "VehicleCoverNote", new { id = Model.ID, polid = int.Parse(ViewData["polid"].ToString()) }, null)%> 
                                 <% } %>
                             <% } %>
                         <% } %>
                     <% } %>
                <% } %>
                <% else 
                   { %>
                    <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Certificates", "Read"))) { %>
                          <%: Html.ActionLink("View Certificate", "GetThisActiveCert", "VehicleCertificate", new { id = Model.ID, start = ViewData["PolStart"].ToString(), end = ViewData["PolEnd"].ToString(), policyid = int.Parse(ViewData["polid"].ToString()) }, null)%> 
                    <% } %>
               <% } %>
            <% } %>

            <% if (!(bool)ViewData["details"] && !(bool)ViewData["expired"] && !(bool)ViewData["imported"])
               { %>
                 
                   <a href="#" class="rmvehicle">Delete</a> 
                    <%: Html.ActionLink("EDIT", "UpdateVehicle", "Policy", new { vehicleID = Model.ID }, null)%> 
              <% } %>     

        </div>
       
    <% } %>
</div>
