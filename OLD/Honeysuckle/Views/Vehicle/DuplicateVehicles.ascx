﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Vehicle>>" %>
   <div style="color:Red;">Record has duplicates. Check a row to use the corresponding record or edit it.</div>
   <br />
    <br />
    <%: Html.Hidden("duplicatePartial", null,new{ id="duplicatePartial" }) %>
     <table>
        <tr>
            <th>
                Select
            </th>
            <th>
                Year
            </th>
            <th>
               Make
            </th>
            <th>
                Model
            </th>
            <th>
                Reg No
            </th>
            <th>
                Colour
            </th>
            <th>
                EngineNo
            </th>
            <th>
                Created
            </th>
            
        </tr>

    <% foreach (var item in Model) { %>
    
        <tr>
            <td>
                <%: Html.CheckBox("dupChassisselect", false, new { @class = "dupChassischeck", id = item.ID.ToString(), @OnClick = "selectDuplicateVehicle(this)" })%>
            </td>
            <td>
                <%: item.VehicleYear %>
            </td>
            <td>
                <%: item.make %>
            </td>
            <td>
                <%: item.VehicleModel %>
            </td>
            <td>
                <%: item.VehicleRegNumber %>
            </td>
            <td>
                <%: item.colour %>
            </td>
            <td>
                <%: item.engineNo %>
            </td>
            <td>
                <%: item.createdOn.ToString() %>
            </td>
            
        </tr>
    
    <% } %>

    </table>
    

