﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Vehicle>" %>

<div class="vehicle-row row selected">
    <%  string vehmodel = "Not Available", regNo = "Not Available";
        if (Model.VehicleModel != null && Model.VehicleModel != "") vehmodel = Model.VehicleModel;
        if (Model.VehicleRegNumber != null && Model.VehicleRegNumber != "") regNo = Model.VehicleRegNumber; %>

    <%: Html.Hidden("basid", Model.base_id, new { @class = "baseid dont-process" })%>
    <div class="vehicle-check select item">
        <%: Html.CheckBox("vehcheck", true, new { @class = "vehcheck", id = Model.ID.ToString(), @OnClick = "changeVehicleCheckBox(this)" })%>
    </div>
    <div class="chassisno item">
        <%: Model.chassisno %>
    </div>
    <div class="make item">
        <%: Model.make %>
    </div>
    <div class="model item">
        <%: vehmodel %>
    </div>
    <div class="regno item">
        <%: regNo %>
    </div>
</div>
