﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Vehicle>" %>

 <% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

        <%: Html.HiddenFor(model => model.ID, new { @id = "hidden_vehicle_id", @class = "dont-process" })%>
        <%: Html.Hidden("prevRegNo", "", new { @id = "prevRegNo", @Value = "", @class = "dont-process" })%>
        <%: Html.Hidden("prevChassis", "", new { @id = "prevChassis", @Value = "", @class = "dont-process" })%>
        <div class="editor-label">
            Chassis Number
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => model.chassisno, new { id = "chassis",@class = "chassis" }) %>
            <%: Html.ValidationMessageFor(model => model.chassisno) %>
        </div>

         <div class="editor-label">
            Vehicle Year
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => model.VehicleYear, new { id = "vehicleYear" })%>
            <%: Html.ValidationMessageFor(model => model.VehicleYear) %>
        </div>

        <div class="editor-label">
            VIN 
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => model.VIN, new { id = "vin" }) %>
            <%: Html.ValidationMessageFor(model => model.VIN) %>
        </div>

        <div class="editor-label">
            Make
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => model.make, new { @required = "required", id = "make" }) %>
            <%: Html.ValidationMessageFor(model => model.make) %>
        </div>
            
        <div class="editor-label">
            Model
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => model.VehicleModel, new { id = "vehiclemodel" })%>
            <%: Html.ValidationMessageFor(model => model.VehicleModel)%>
        </div>
            
       

        <div class="editor-label">
            Vehicle Registration Number
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => model.VehicleRegNumber, new { id = "vehicleRegNum" })%>
            <%: Html.ValidationMessageFor(model => model.VehicleRegNumber) %>
        </div>

        <div class="check-div adv-check-div">
            <div class="editor-label">
                Advanced Data
            </div>
            <div class="editor-field">
                <%: Html.CheckBox("ad", false, new { @class ="advanced-check" })%>
            </div>
        </div>

        <div class="vehicle-advanced-data">

            <div class="editor-label">
                Model Type
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.modelType, new { id = "modeltype" }) %>
                <%: Html.ValidationMessageFor(model => model.modelType) %>
            </div>
            
            <div class="editor-label">
                Body Type
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.bodyType, new {id = "bodytype" })%>
                <%: Html.ValidationMessageFor(model => model.bodyType) %>
            </div>

            <div class="editor-label">
                Estimated Value
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.estimatedValue, new {id = "estimated_value",@class="estimatedValue" })%>
                <%: Html.ValidationMessageFor(model => model.estimatedValue) %>
            </div>
            
            <div class="editor-label">
                Extension
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.extension, new {id = "extension" }) %>
                <%: Html.ValidationMessageFor(model => model.extension) %>
            </div>
            
            <div class="editor-label">
                Color
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.colour, new { id = "colour" })%>
                <%: Html.ValidationMessageFor(model => model.colour) %>
            </div>
            
            <div class="editor-label">
                Expiry date of Fitness 
            </div>
            <div class="editor-field">
                    <%: Html.TextBox("expiryDateoffitness", null, new { id = "fitnessExpiry", @readonly = "readonly"})%>
                    <button id="clear-expiryoffitness" onclick="clearExpiryFitness()" type="button">Clear</button>
            </div>

            <div class="editor-label">
                Cylinder Number
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.cylinders, new { id = "cylinders" })%>
                <%: Html.ValidationMessageFor(model => model.cylinders) %>
            </div>
            
            <div class="editor-label">
                Engine Number
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.engineNo, new { id = "engineNo" })%>
                <%: Html.ValidationMessageFor(model => model.engineNo) %>
            </div>
            
            <div class="check-div">
                <div class="editor-label">
                    Engine Modified
                </div>                    
                <div class="editor-field">
                    <%: Html.CheckBoxFor(model => model.engineModified, new {id = "engineModified", Style = "width: 14px; height:14px;"})%>
                </div>
            </div>
            
            <div class="editor-label">
                Engine Type
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.engineType, new {id = "engineType" })%>
                <%: Html.ValidationMessageFor(model => model.engineType) %>
            </div>
            
            <div class="editor-label">
                Estimated Annual Mileage
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.estAnnualMileage, new { id = "mileage" })%>
                <%: Html.ValidationMessageFor(model => model.estAnnualMileage) %>
            </div>
            
            <div class="editor-label">
                HPPCC Unit Type
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.HPCCUnitType, new {id = "hpccUnit" })%>
                <%: Html.ValidationMessageFor(model => model.HPCCUnitType) %>
            </div>
            
            <div class="editor-label">
                Reference Number
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.referenceNo, new {id = "refno" })%>
                <%: Html.ValidationMessageFor(model => model.referenceNo) %>
            </div>
            
            <div class="editor-label">
                Registration Location
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.registrationLocation, new {id = "regLocation" })%>
                <%: Html.ValidationMessageFor(model => model.registrationLocation) %>
            </div>
            
            <div class="editor-label">
                Roof Type
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.roofType, new {id = "roofType" })%>
                <%: Html.ValidationMessageFor(model => model.roofType) %>
            </div>
            
            <div class="editor-label">
                Seating Number
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.seating, new { id = "seating" })%>
                <%: Html.ValidationMessageFor(model => model.seating) %>
            </div>
            
            <div class="editor-label">
                Seating Certificate
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.seatingCert, new {id = "seatingCert" })%>
                <%: Html.ValidationMessageFor(model => model.seatingCert) %>
            </div>
            
            <div class="editor-label">
                Tonnage
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.tonnage, new {id = "tonnage" })%>
                <%: Html.ValidationMessageFor(model => model.tonnage) %>
            </div>

            <div class="editor-label">
                Hand Drive Type 
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.handDrive.handDriveType, new { id = "handDriveType" })%>
                <%: Html.ValidationMessageFor(model => model.handDrive.handDriveType)%>
            </div>

            <div class="editor-label">
                Transmission Type
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.transmissionType.transmissionType, new { id = "tranType" })%>
                <%: Html.ValidationMessageFor(model => model.transmissionType.transmissionType)%>
            </div>

            <div class="editor-label"> 
                Registered Owner
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.RegisteredOwner, new { id = "regOwner" })%>
                <%: Html.ValidationMessageFor(model => model.RegisteredOwner)%>
            </div>

            <div class="check-div">
                <div class="editor-label">
                    Restricted Driver 
                </div>
                <div class="editor-field">
                    <%: Html.CheckBoxFor(model => model.RestrictedDriving, new { id = "restrictedDriver", Style = "width: 14px; height:14px;"})%>
                </div>
            </div>
            
            <div class="subheader"> Main Driver of Vehicle </div><br />

            <% Html.RenderAction("MainDriver", "People"); %>
              

        </div>

        <script src="../../Scripts/ApplicationJS/vehicleDatePicker.js" type="text/javascript"></script>
        <script src="../../Scripts/ApplicationJS/TestChassis-Modal.js" type="text/javascript"></script>
        <script src="../../Scripts/ApplicationJS/TestRegNo.js" type="text/javascript"></script>
        <script src="../../Scripts/ApplicationJS/VehicleAutoComplete.js" type="text/javascript"></script>
        <script src="../../Scripts/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript"></script> <!-- datepicker -->
        <script src="../../Scripts/ApplicationJS/TestVIN-modal.js" type="text/javascript"></script>
<% } %>