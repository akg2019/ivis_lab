﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.pidrop>>" %>

<%: Html.DropDownList(ViewData["type"].ToString(), new SelectList(Model.ToList(), "id", "name"), null, new { @class = "pidrop ignore"}) %>
