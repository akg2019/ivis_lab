﻿<%@ Page Title="Create Policy" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Policy>" %>

    <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        
        <%: Html.Hidden("systemdate", DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt"), new { id = "sysdate", @class = "dont-process" })%>

        <% if (Honeysuckle.Models.Constants.user.newRoleMode) { %> <%: Html.Hidden("compid", Honeysuckle.Models.Constants.user.tempUserGroup.company.CompanyID, new { @class = "dont-process" })%>  <%  } %>
        <% else { %> <%: Html.Hidden("compid", Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID, new { @class = "dont-process" })%> <% } %>

        <% //bool i_am_insurer = Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID); %>
        
        <%
            bool isInsurer = false;
            Honeysuckle.Models.Company thisComp =  Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user);
                            
            if (Honeysuckle.Models.Constants.user.newRoleMode)
            {
                if (Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Honeysuckle.Models.Constants.user.tempUserGroup.company.CompanyID)) isInsurer = true;
            }
            else if (Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID)) isInsurer = true;
        %>

        <%: Html.Hidden("page", "create", new { id="page", @class = "dont-process" })%>
        <% if (isInsurer){ %>
            <%: Html.Hidden("insurer", "ins") %>
        <% } %>

        <div id= "form-header">
            <img src="../../Content/more_images/Policy.png" alt=""/> 
            <div class="form-text-links">
                <span class="form-header-text">Policy Create</span>
                <div class="icon">
                    <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                </div>
            </div>
        </div>
    
        <div class="field-data">

            <!--dialog divs -->
            <div id="compContact"></div> 
            <div id="insured-modal"></div>
            <div id="insured-modal1"></div>
            <div id="company-modal"></div>
            <div id="company-modal1"></div>
            <div id="vehicle-modal"></div>
            <div id="vehicle-modal1"></div>
            <div id="InformationDialog"></div>
            <!-- end dialog divs -->

            <% using (Html.BeginForm()) { %>
                <%: Html.ValidationSummary(true) %>
                
                <fieldset>
                <div class="validate-section">
                    <div class="subheader"> 
                        <div class="arrow right down"></div>
                        Policy Information 
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div id= "policyInformation" class="policy-box" > 
                            <%: Html.Hidden("page", "create", new { @class = "dont-process" })%>
                            <!-- hidden info -->
                            <div id= "SendMail" > </div>
                            <%: Html.Hidden("vehicleID",ViewData["vehID"], new { id = "vehicleID", @class = "dont-process" })%>
                            <%: Html.HiddenFor(model => model.ID, new { id = "hiddenid", @Value = Model.ID, @class = "dont-process" })%> <!-- THIS SHOWS THE ID OF THE POLICY -->
                            <%: Html.HiddenFor(model => model.ID, new { id = "compid", @Value = Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID, @class = "dont-process" })%> 
            
                            <div id= "Alert"> 
                                <%: ViewData["No Vehicle"]  %>
                                <%: ViewData["No Insured"]  %> 
                                <%: ViewData["Date Error"]  %> 
                                <%: ViewData["IAJID"]  %> 
                            </div>
                            <div id= "VehiclePrompt"> 
                                <%: ViewData["VehicleAlert"]%>
                            </div>
                

                            <% if ((bool)ViewData["postBack"])
                                { %>
                                <div id= "postBackVals"> </div>
                            <%  } %>

                            <!-- end of hidden info -->
                            <div class="shown-policy-data policy-box">
                    
                                
                 
                                <% if (!isInsurer)
                                    {  %>
                                    <div class="editor-label">
                                        Insurance Company
                                    </div>
                                    <div class="editor-field">
                                        <%: Html.HiddenFor(model => model.insuredby.CompanyID, new { id = "insID", @class = "dont-process" })%>
                                        <% Html.RenderAction("CompanyDropDown", "Company", new { type = "InsuranceCompanies", notDisabled = true}); %>
                                    </div>
                                <%  } %>
                                <% else { %> <%: Html.Hidden("companies", thisComp.CompanyID, new { @id = "isInsurer", @class = "dont-process" })%> <% } %>

                
                                <div class="editor-label">
                                    Start Date <span class="format">(Day Month Year HH:mm tt)</span>
                                </div>
                                <div class="editor-field">
                                    <%: Html.TextBoxFor(model => model.startDateTime, new { @required = "required", id = "start", @readonly = "readonly", @class = "datetime", @Value = Model.startDateTime.ToString("d MMM, yyyy, HH:mm tt")})%>
                                    <%: Html.Hidden("hidden_start", Model.startDateTime.ToString("MM/dd/yyyy hh:mm:ss tt"), new { id = "hidden_start", @class = "dont-process" })%>
                                </div>
            
                                <div class="editor-field">
                                  Period in Months:
                                   <%: Html.Hidden("systemdate", DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt"), new { id = "sysdate", @class = "dont-process" })%>
                                    <button type="button" class="quickperiod submitbutton" value="1">1</button>
                                    <button type="button" class="quickperiod submitbutton" value="3">3</button>
                                    <button type="button" class="quickperiod submitbutton" value="6">6</button>
                                    <button type="button" class="quickperiod submitbutton" value="12">12</button>
                               </div><br />

                                <div class="editor-label">
                                    End Date <span class="format">(Day Month Year HH:mm tt)</span>
                                </div>
                                <div class="editor-field">
                                    <%: Html.TextBoxFor(model => model.endDateTime,  new { @required = "required", id = "end", @readonly = "readonly", @class = "datetime", @Value = Model.endDateTime.ToString("d MMM, yyyy, HH:mm tt") /*ViewData ["endDate"].ToString()*/})%>
                                    <%: Html.Hidden("hidden_end", Model.endDateTime.ToString("MM/dd/yyyy hh:mm:ss tt"), new { id = "hidden_end", @class = "dont-process" })%>
                                </div>

                                <div class="editor-label">
                                    Policy Cover 
                                </div>
                                <div class="editor-field">  
                                    <%: Html.HiddenFor(model => model.policyCover.ID, new { id = "CoverID", @class = "dont-process" })%>
                                    <%/* if (!(bool)ViewData["postBack"]) { %>  <% Html.RenderAction("CoversDropDown", "PolicyCover"); %> <% } %>
                                    <% else %> <% Html.RenderAction("CoversDropDown", "PolicyCover", new { Compid = Model.insuredby.CompanyID, type = "Edit" }); */%>

                                    <% Html.RenderAction("CoversDropDown", "PolicyCover"); %>
                                </div>

                                <div class="editor-label">
                                    Policy Prefix 
                                </div>
                                <div class="editor-field">
                                    <%: Html.TextBoxFor(model => model.policyCover.prefix, new { @required = "required", id = "policyprefix", @readonly = "readonly", @class = "read-only words" }) %>
                                    <%: Html.ValidationMessageFor(model => model.policyCover.prefix)%>
                                </div>
                                <div class="editor-label">
                                    No Policy Number
                               </div>
                               <div class="editor-field">
                                    <div class="WarningInfo">If you do not have a policy number,</br>this box should be checked</div>
                                    <div><%: Html.CheckBoxFor(model => model.no_policy_no, new { @class = "dont-process no_policy_no_check", title= "No Policy Numberr."}) %> </div>
                                </div>
                                 <div class="editor-label">
                                    Policy Number 
                                </div>
                                <div class="editor-field">
                                    <%: Html.TextBoxFor(model => model.policyNumber, new { @required = "required", id = "policyNum"}) %>
                                    <%: Html.ValidationMessageFor(model => model.policyNumber) %>
                                </div>

                                <%-- 
                                <div class="editor-label">
                                    Broker Number 
                                </div>
                                <div class="editor-field">
                                    <%: Html.TextBoxFor(model => model.BrokerNo, new { @required = "required", id = "policyNum"}) %>
                                    <%: Html.ValidationMessageFor(model => model.BrokerNo) %>
                                </div>
                                --%>

                                <% if(!isInsurer){ %>
                                <div class = "brokerPolicyCreate">
                                    <div class="editor-label">
                                        Broker/Temporary Number 
                                    </div>
                                    <div class="editor-field">
                                        <%: Html.TextBoxFor(model => model.brokerPolicyNumber , new {id = "BrokerPolicyNum"}) %>
                                        <%: Html.ValidationMessageFor(model => model.brokerPolicyNumber) %>
                                    </div>
                                </div>
                                <% } %>

                                
                                <div class="check-field">
                                    <div class="editor-label">
                                        Scheme
                                    </div>
                                    <div class="editor-field">
                                        <%: Html.CheckBoxFor(model => model.Scheme, new { id = "scheme" })%>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                
                <div class="validate-section">
                    <div class="subheader">
                        <div class="arrow right"></div>
                        Source Company Information 
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="SourceComp policy-box">
                            <div class="editor-label">
                                Company Name 
                            </div>
                         
                            <%-- if (isInsurer){ %>
                                <div class="editor-field">
                                    <%: Html.HiddenFor(model => model.compCreatedBy.CompanyID, new { @class = "hidden_company_id dont-process" }) %>
                                    <% Html.RenderAction("CompanyDropDown", "Company", new { type = "IntermediaryCompanies"}); %>
                                </div>
                            <% } %>
                            <% else{ %> --%>
                                <div class="editor-field">
                                    <%: Html.HiddenFor(model => model.compCreatedBy.CompanyID,new{@Value = Honeysuckle.Models.Constants.user.employee.company.CompanyID,@class = "hidden_company_id dont-process"}) %>
                                    <%: Html.TextBox("thisCompName", thisComp.CompanyName, new { @readonly = "readonly", @class = "read-only" })%>
                                </div>

                                <div class="editor-label">
                                    Company TRN 
                                </div>
                                <div class="editor-field">
                                    <%: Html.TextBox("thisCompTRN", thisComp.trn, new { @readonly = "readonly", @class = "read-only" })%>
                                </div>
                            <%-- } --%>
                        </div>
                    </div>
                </div>

                <div class="validate-section policyholders">
                    <div class="subheader"> 
                        <div class="arrow right"></div>
                        Policy Holders
                        <div class="valid"></div>
                    </div>
                    <div id="policyholders" class="data">
                        <div class="editor-label"> 
                            Insured Type
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.insuredType, new { @readonly = "readonly", @class = "read-only", @id = "insuredType" }) %>
                        </div>
                        <div class="policy-holders">
                            <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("PolicyHolder", "Create"))) { %>
                                <div class="addInsured policy-box">
                                    <div id="insured">
                                        <%
                                            if (Model.insured != null)
                                            {
                                                foreach (Honeysuckle.Models.Person person in Model.insured)
                                                {
                                                    Html.RenderAction("personView", "People", new { perId = person.PersonID, type = "details", changeTRN = false, trn = person.TRN }); 
                                                }
                                            }
                                        %>
                                    </div>
                                    <div id="addInsured" class="add-dynamic small"><img src="../../Content/more_images/ivis_add_icon.png" /><span>Add Policy Holder</span></div>
                                </div>
                
                    
                                <div class="addCompany policy-box">
                        
                                    <div id="company">
                                        <%
                                            if (Model.company != null)
                                            { 
                                                foreach(Honeysuckle.Models.Company company in Model.company)
                                                {
                                                    Html.RenderAction("CompanyView", "Company", new { CompanyIdNum = company.CompanyID }); 
                                                }
                                            }   
                                        %>
                                    </div>
                                    <div id="addCompany" class="add-dynamic small"><img src="../../Content/more_images/ivis_add_icon.png" /><span>Add Company Policy Holder</span></div>
                                </div>
                            <% } %>
                            <% else { %>
                                <div class="message">
                                    You require the Policy Holders Create permission to perform this action
                                </div>
                            <% } %>
                        </div>
                    </div>
                </div>

                <div id="LeaveBlank" class="policy-mailing-address address validate-section">
                    <div class="subheader"> 
                        <div class="arrow right"></div>
                        Mailing Address
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="editor-label">
                            Address Auto-Fill Drop Down
                        </div>
                        <div class="editor-field">
                            <%: Html.HiddenFor(model => model.mailingAddressShort, new { @class = "hidden-mail-id dont-process" })%>
                            <% Html.RenderAction("PolicyInsuredDropDown", new { id = 0, type = "mailingaddressshort", selected = Model.mailingAddressShort }); %>
                        </div>

                        <div class="editor-label">
                            Street 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.mailingAddress.roadnumber, new { @class = "padding RoadNumber mailing", @PlaceHolder = " 12", @autocomplete = "off" })%>
                            <%: Html.TextBoxFor(model => model.mailingAddress.road.RoadName, new { @class = "padding roadname mailing", @PlaceHolder = " Short Hill", @autocomplete = "off" })%>
                            <%: Html.TextBoxFor(model => model.mailingAddress.roadtype.RoadTypeName, new { @class = "padding roadtype mailing", @PlaceHolder = " Avenue", @autocomplete = "off" })%>
          
                            <%: Html.ValidationMessageFor(model => model.mailingAddress.roadnumber)%>
                            <%: Html.ValidationMessageFor(model => model.mailingAddress.road.RoadName)%>
                            <%: Html.ValidationMessageFor(model => model.mailingAddress.roadtype.RoadTypeName)%>
                        </div>

                        <div class="editor-label">
                            Building/Apartment No. 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.mailingAddress.ApartmentNumber, new { @class = "aptNumber mailing", @autocomplete = "off" })%>
                            <%: Html.ValidationMessageFor(model => model.mailingAddress.ApartmentNumber)%>
                        </div>

                        <div class="editor-label">
                            City/Town
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.mailingAddress.city.CityName, new { @class = "city mailing more-words",  @autocomplete = "off" })%>
                            <%: Html.ValidationMessageFor(model => model.mailingAddress.city.CityName)%>
                        </div>

                        <div class="editor-label">
                            Parish
                        </div>
                        <div class="editor-field">
                            <%: Html.HiddenFor(model => model.mailingAddress.parish.ID, new { @class = "parish mailing dont-process", @autocomplete = "off" })%>
                            <% Html.RenderAction("ParishList", "Parish"); %>
                           <%-- <%: Html.ValidationMessage("parish") %>--%>
                        </div>

                        <div class="editor-label">
                           Country
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.mailingAddress.country.CountryName, new { @class = "country mailing", @Value = "Jamaica", @autocomplete = "off" })%>
                            <%: Html.ValidationMessageFor(model => model.mailingAddress.country.CountryName)%>
                        </div>
                    </div>
                </div>
                
                <div class="policy-billing-address address validate-section">
                    
                    <div class="subheader"> 
                        <div class="arrow right"></div>
                        <span class="subtext">Billing Address</span>
                        <div class="valid"></div>
                        <div class="bilcheck"> 
                            Use Same As Above: <%: Html.CheckBox("billscheck", true, new { @class = "billing check dont-process"})%> 
                        </div> 
                    </div>
                    <div class="billing-address-section hidden data">
                        <div class="editor-label">
                            Address Auto-Fill Drop Down
                        </div>
                        <div class="editor-field">
                            <%: Html.HiddenFor(model => model.billingAddressShort, new { @class = "hidden-bill-id dont-process" })%>
                            <% Html.RenderAction("PolicyInsuredDropDown", new { id = 0, type = "billingaddressshort", selected = Model.billingAddressShort }); %>
                        </div>
                        <div class="editor-label">
                            Street 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.billingAddress.roadnumber, new { @class = "padding RoadNumber billing", @PlaceHolder = " 12", @autocomplete = "off" })%>
                            <%: Html.TextBoxFor(model => model.billingAddress.road.RoadName, new { @class = "padding roadname billing",  @PlaceHolder = " Short Hill", @autocomplete = "off" })%>
                            <%: Html.TextBoxFor(model => model.billingAddress.roadtype.RoadTypeName, new { @class = "padding roadtype billing", @PlaceHolder = " Avenue", @autocomplete = "off" })%>
          
                            <%: Html.ValidationMessageFor(model => model.billingAddress.roadnumber)%>
                            <%: Html.ValidationMessageFor(model => model.billingAddress.road.RoadName)%>
                            <%: Html.ValidationMessageFor(model => model.billingAddress.roadtype.RoadTypeName)%>
                        </div>

                        <div class="editor-label">
                            Building/Apartment No. 
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.billingAddress.ApartmentNumber, new { @class = "aptNumber billing", @autocomplete = "off" })%>
                            <%: Html.ValidationMessageFor(model => model.billingAddress.ApartmentNumber)%>
                        </div>

                        <div class="editor-label">
                            City/Town
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.billingAddress.city.CityName, new { @class = "city billing", @autocomplete = "off" })%>
                            <%: Html.ValidationMessageFor(model => model.billingAddress.city.CityName)%>
                        </div>

                        <div class="editor-label">
                            Parish
                        </div>
                        <div class="editor-field">
                            <%: Html.HiddenFor(model => model.billingAddress.parish.ID, new { @class = "parish billing dont-process" })%>
                            <% Html.RenderAction("ParishList", "Parish"); %>
                            <%: Html.ValidationMessage("parish") %>
                        </div>

                        <div class="editor-label">
                           Country
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.billingAddress.country.CountryName, new { @class = "country billing", @Value = "Jamaica", @autocomplete = "off" })%>
                            <%: Html.ValidationMessageFor(model => model.billingAddress.country.CountryName)%>
                        </div>
                    </div>
                </div>
                
                <div class="validate-section policy-risk">
                    <div class="subheader"> 
                        <div class="arrow right"></div>
                        Vehicles
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div id="vehicle-box" class="addvehicle policy-box">
                            <div id="drivers"></div>
                            <div id="vehicles">
                                <%
                                    if (Model.vehicles != null)
                                    {
                                        foreach (Honeysuckle.Models.Vehicle vehicle in Model.vehicles)
                                        {
                                            if ((bool)ViewData["postBack"]) Html.RenderAction("Vehicle", "Vehicle", new { test = vehicle.ID, startDate = Model.startDateTime.ToString("yyyy-MM-dd HH:mm:ss"), endDate = Model.endDateTime.ToString("yyyy-MM-dd HH:mm:ss"), type = "Add", polid = Model.ID,VehicleUsage = vehicle.usage.ID, page = "create" });
                                            else Html.RenderAction("Vehicle", "Vehicle", new { test = vehicle.ID, startDate = Model.startDateTime.ToString("yyyy-MM-dd HH:mm:ss"), endDate = Model.endDateTime.ToString("yyyy-MM-dd HH:mm:ss"), type = "Add", CoverID = Model.policyCover.ID, VehicleUsage = vehicle.usage.ID, polid = Model.ID, page = "create" });
                                        }
                                    }
                                %>
                                <div id="addvehicle" class="add-dynamic big"><img src="../../Content/more_images/ivis_add_icon.png" /><span>Add Vehicle(s)</span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <%: Html.Hidden("proceed", "", new { id = "Continue", @class = "dont-process" })%> 

                <div class="btnlinks">
                    <% Html.RenderAction("BackBtn", "Back"); %>
                    <input type="submit" value="Create Policy" name="submit" id="Submit2" class="saveButton" />
                </div>
 
                </fieldset>

            <% } %>

        </div>

    </asp:Content>

    <asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
        <!-- css -->
        <link href="../../Scripts/datetimepicker-master/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
        <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
        <link href="../../Content/Pages.css" rel="stylesheet" type="text/css" />
        <link href="../../Content/Policies.css" rel="stylesheet" type="text/css" />
        
        <!-- JS -->
        
        <script src="../../Scripts/ApplicationJS/usage-pull.js" type="text/javascript"></script> <!-- manages updating of usage everytime someone hits the drop down -->
        <script src="../../Scripts/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript"></script> <!-- datepicker -->
        <script src="../../Scripts/ApplicationJS/policy-create.js" type="text/javascript"></script>
        <script src="../../Scripts/ApplicationJS/multipleVehicles.js" type="text/javascript"></script>
        <script src="../../Scripts/ApplicationJS/multipleInsured.js" type="text/javascript"></script> 
        <script src="../../Scripts/ApplicationJS/multipleCompanies.js" type="text/javascript"></script>
        <script src="../../Scripts/ApplicationJS/policy.js" type="text/javascript"></script>
        <script src="../../Scripts/ApplicationJS/managedrivers.js" type="text/javascript"></script>
        <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>
        <script src="../../Scripts/ApplicationJS/cover-usages-update.js" type="text/javascript"></script> <!-- manages updating of usages everytime someone hits the drop down -->
        <script src="../../Scripts/ApplicationJS/policy-cover-prefix-lookup.js" type="text/javascript"></script> <!-- manages updating of policy prefix everytime someone hits the drop down -->
        <script src="../../Scripts/ApplicationJS/personDetails.js" type="text/javascript"></script>
        <script src="../../Scripts/ApplicationJS/multiple-emails.js" type="text/javascript"></script>
        <script src="../../Scripts/ApplicationJS/multiplePhoneNums.js" type="text/javascript"></script>
        <script src="../../Scripts/ApplicationJS/policyinsureddropdown.js" type="text/javascript"></script> <!-- this manages the mailing address auto fill drop down -->
        <script src="../../Scripts/ApplicationJS/mortgagees.js" type="text/javascript"></script> <!-- this is for the autofill of mortgagees field in the vehicle card -->
        <script src="../../Scripts/ApplicationJS/policy-insured.js" type="text/javascript"></script>
        
    </asp:Content>


