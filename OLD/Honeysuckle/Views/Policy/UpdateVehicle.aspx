﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Vehicle>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	UpdateVehicle
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2 style="text-align:center;">Update Vehicle</h2>

    <div class="newvehicle">
   <div class="blurDiv" style = "margin:7px!mportant; padding-right:25px!important;">
         <% using (Html.BeginForm()) {%>
            <%: Html.ValidationSummary(true) %>

            <%: Html.HiddenFor(model => model.ID)%> 
            <%: Html.HiddenFor(model => model.base_id)%>
           
            <div class="line">
                <p>Year</p>
               <%: Html.TextBoxFor(model => model.VehicleYear, new {@class = "form-control"})%> 
            </div>

             <div class="line">
                <p>Make</p>
               <%: Html.TextBoxFor(model => model.make, new {@class = "form-control"})%> 
            </div>

            <div class="line">
                <p>Model</p>
               <%: Html.TextBoxFor(model => model.VehicleModel, new {@class = "form-control"})%> 
            </div>

            <div class="line">
                <p>Reg</p>
               <%: Html.TextBoxFor(model => model.VehicleRegNumber, new {@class = "form-control"})%> 
            </div>

            <div class="line">
                <p>Chassis</p>
               <%: Html.TextBoxFor(model => model.chassisno, new {@class = "form-control"})%> 
            </div>
              
      </div>      
      <hr />
        <div class="vehicleFooter line">
          <input type = "submit" value = "UPDATE CHANGES" style=" height:30px!important;"/>    

        </div>
       
    <% } %>
</div>




</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
