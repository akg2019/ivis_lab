﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Driver>" %>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
            <%: Html.HiddenFor(model => model.DriverId, new { id = "driverid", @class = "dont-process" })%>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.licenseno) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.licenseno, new { id = "license" })%>
                <%: Html.ValidationMessageFor(model => model.licenseno) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.dateissued) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.dateissued, new { id = "issued", @readonly = "readonly", @Value = Model.dateissued.ToString("d") })%>
                <%: Html.ValidationMessageFor(model => model.dateissued) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.expirydate) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.expirydate, new { id = "expiry", @readonly = "readonly", @Value = Model.expirydate.ToString("d") })%>
                <%: Html.ValidationMessageFor(model => model.expirydate) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.licenseclass) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.licenseclass, new { id = "dclass" })%>
                <%: Html.ValidationMessageFor(model => model.licenseclass) %>
            </div>
    <% } %>