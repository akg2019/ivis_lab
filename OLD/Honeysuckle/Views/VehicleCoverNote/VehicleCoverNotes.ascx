﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.VehicleCoverNote>>" %>

<div id="covernotes">
    <% if(Model.Count() > 0){ %>
        <div class="row menu">
            <div class="item">
                Effective Date
            </div>
            <div class="item">
                Expriy Date
            </div>
            <div class="item">
                Cancelled
            </div>
            <div class="item">
                Company
            </div>
        </div>
        <% foreach (Honeysuckle.Models.VehicleCoverNote vcn in Model){ %>
            <%  if (vcn.effectivedate.Date <= DateTime.Now && vcn.effectivedate.Date.AddDays(vcn.period.Value) >= DateTime.Now)
               { %>
                <div class="row current covernote">
            <% } %>
            <% else
               { %>
                <div class="row history covernote">
            <% } %>
                <div class="item">
                    <%: String.Format("{0:d}",vcn.effective_date_time.Date) %>
                </div>
                <div class="item">
                    <%: vcn.period %>
                </div>
                <div class="item">
                    <%: vcn.cancelled %>
                </div>
                <div class="item">
                    <%: vcn.company.CompanyName %>
                </div>
            </div>
         <% } %>
       </div>
    <% } %>
    
    

</div>