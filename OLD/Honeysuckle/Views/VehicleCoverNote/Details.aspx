﻿<%@ Page Title="Cover Note Details" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.VehicleCoverNote>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Cover Note Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div id= "form-header"><img src="../../Content/more_images/Cover_Note.png" alt="" /> 
      <div class="form-text-links">
        <span class="form-header-text">Cover Note Details </span> 
         <% if (Model.cancelled)
               { %>
                  <div class = "cancelledRecord" title = "Cancelled Policy">Cancelled</div>
            <% } %>
        <div class="icon">
              <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
        </div>
    </div>
 </div> 
     <div class="field-data">
    
        
      <div id = "SendMail"> </div>

    <fieldset>

        <%: Html.HiddenFor(model => model.ID, new { id = "coverNoteID",@class="dont-process" })%>
        <% bool allow = false;
           bool isExpired = (Model.expirydate < DateTime.Now);
           Model.approved = true;
           bool NoteActive = Honeysuckle.Models.VehicleCoverNoteModel.IsCoverNoteActive(Model.ID);
           bool NoteCancelled = Honeysuckle.Models.VehicleCoverNoteModel.IsCoverNoteCancelled(Model.ID);
           
            if (Honeysuckle.Models.Constants.user.newRoleMode)
            {
                if (Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Honeysuckle.Models.Constants.user.tempUserGroup.company.CompanyID))
                    allow = true;
            }
            else if (Honeysuckle.Models.CompanyModel.IsCompanyInsurer(Honeysuckle.Models.UserModel.GetUser(Honeysuckle.Models.Constants.user.ID).employee.company.CompanyID) || (Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user)))
                    allow = true;

            bool canPrint = true;
            int CTID = Honeysuckle.Models.IntermediaryModel.TestIntermediaryToInsurer(Honeysuckle.Models.Constants.user.employee.company.CompanyID, Model.company.CompanyID);
            if (CTID != 0)
            {
                canPrint = Honeysuckle.Models.IntermediaryModel.GetIntermediary(CTID).PrintCoverNote;
            }
        %>

        <div class="validate-section">
           <div class="subheader"> 
           <div class="arrow right down"></div>
                Cover Note Information 
                <div class="valid"></div>
                <% if(Model.Policy.imported){ %>   
                    <img src="../../Content/more_images/imported.png" class ="imported" />
                <% } %>
           </div>
            <div class="data">
                <div class="editor-label">Alterations: </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.alterations, new { @readonly = "readonly", @class = "read-only" })%>
                </div>
                 <%
                    string LastPrintedByName;
                    string FirstPrintedByName;
            
                    if (Model.lastprintflat == null) LastPrintedByName = "";
                        else LastPrintedByName = Model.lastprintflat;
          
                    if (Model.firstprintflat == null) FirstPrintedByName = "";
                        else FirstPrintedByName = Model.firstprintflat;
                %>
                <div class="editor-label">First Printed By: </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => Model.firstprintflat, new { @readonly = "readonly", @class = "read-only first-print", @Value = FirstPrintedByName })%>
                </div>

                <div class="editor-label">Last Printed By: </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.lastprintflat, new { @readonly = "readonly", @class = "read-only last-print", @Value = LastPrintedByName })%>
                </div>

                <div class="editor-label">Cancelled: <%: Html.CheckBox("cancelled", Model.cancelled)%></div><br />
   
                <%
                    string cancelledon;
                    if (Model.cancelledon != new DateTime()) cancelledon = Model.cancelledon.ToString("d MMM, yyyy, HH:mm tt");
                    else cancelledon = "";
                %>
        
                <div class="editor-label">Cancelled On: </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.cancelledon, new { @readonly = "readonly", @class = "read-only", @Value = cancelledon })%>
                </div>
        
                <div class="editor-label">Cover Note Number: </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.covernoteid, new { @readonly = "readonly", @class = "read-only" })%>
                </div>
        
                <div class="editor-label">Effective Date: </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.effectivedate, new { @readonly = "readonly", @class = "read-only", @Value = Model.effectivedate.ToString("d MMM, yyyy, hh:mm tt") })%>
                </div>

                <div class="editor-label">Period: </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.period, new { @readonly = "readonly", @class = "read-only" })%>
                </div>

                <div class="editor-label">Expriy Date: </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.expirydate, new { @readonly = "readonly", @class = "read-only", @Value = Model.expirydate.ToString("d MMM, yyyy, hh:mm tt") })%>
                </div>
        
                <div class="editor-label">Manual Cover Note No.</div>
                <div class="editor-field">
                   <%: Html.TextBoxFor(model => model.covernoteno, new { @readonly = "readonly", @class = "read-only" })%>
                </div>

                <div class="editor-label">Cover Note Wording Template:</div> 
                <div class="editor-field">
                   <%: Html.TextBoxFor(model => model.certificateType, new { @readonly = "readonly", @class = "read-only" })%>
                </div>
        
                <div class="editor-label">Print Count</div>
                <div class="editor-field">
                    <%--<% if (allow)
                       { %>
                            <%: Html.TextBoxFor(model => model.printcountInsurer, new { @readonly = "readonly", @class = "read-only print-count" })%>
                    <% } %>
                    <% else
                       { %>
                         <%: Html.TextBoxFor(model => model.printcount, new { @readonly = "readonly", @class = "read-only print-count" })%>
                    <% } %>--%>
                    <%: Html.TextBoxFor(model => model.printcount, new { @readonly = "readonly", @class = "read-only print-count" })%>
                </div>
            </div> <!-- end of first data section -->
  </div> <!-- end of first validate section -->
         <div class="validate-section" style="float:none !important">
             <div class="subheader"> 
                <div class="arrow right"></div>
                Vehicle Information 
                <div class="valid"></div>
             </div>
             <div class="data">
                    <div id = "company-display"></div> <div id = "person-modal"></div>   <div id = "vehicle-modal"></div>
                    <div class="editor-label"> Chassis No: </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.risk.chassisno, new { @readonly = "readonly", id = "chassis", @class = "read-only" })%>
                    </div>
                    <div class="editor-label"> Make: </div>
                    <div class="editor-field">
                       <%: Html.TextBoxFor(model => model.risk.make, new { @readonly = "readonly", @class = "read-only" })%>
                    </div>
                    <div class="editor-label"> Model: </div>
                    <div class="editor-field">
                       <%: Html.TextBoxFor(model => model.risk.VehicleModel, new { @readonly = "readonly", @class = "read-only" })%>
                    </div>
                </div> <!-- end of first data section -->
      </div> <!-- end of first validate section -->   
       
            <% if (Model.risk.mainDriver != null && Model.risk.mainDriver.person.PersonID != 0) { %>
               <div class="validate-section">
                   <div class="subheader">
                         <div class="arrow right"></div>
                         Main Driver 
                        <div class="valid"></div>
                   </div>
                   <div class="data">
                        <div class="editor-label">
                           TRN: 
                        </div>
                        <div class="editor-field">
                           <%: Html.TextBoxFor(model => model.risk.mainDriver.person.TRN, new { @readonly = "readonly", id = "maindriverTRN", @class = "read-only" })%>
                        </div>

                        <div class="editor-label">
                            First Name: 
                        </div>
                        <div class="editor-field">
                             <%: Html.TextBoxFor(model => model.risk.mainDriver.person.fname, new { @readonly = "readonly", @class = "read-only" })%>
                        </div>
            
                        <div class="editor-label">
                            Last Name: 
                        </div>
                        <div class="editor-field">
                           <%: Html.TextBoxFor(model => model.risk.mainDriver.person.lname, new { @readonly = "readonly", @class = "read-only" })%>
                        </div>

                 </div> <!-- end of first data section -->
        </div> <!-- end of first validate section --> 
            <% } %>

            <div class="validate-section">
                <div class="subheader"> 
                     <div class="arrow right"></div>
                     Insurance Company Information 
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div id="company">
                        <% Html.RenderAction("CompanyRead", "Company", new { id = Model.company.CompanyID }); %>
                    </div>
                </div> <!-- end of first data section -->
        </div> <!-- end of first validate section -->

        <div class="validate-section">
            <div class="subheader">
                <div class="arrow right"></div>
                Policy Information 
                <div class="valid"></div>
            </div>
            <div class="data">
                <div id="policy">
                    <% Html.RenderAction("ViewPolicy", "Policy", new { id = Model.Policy.ID }); %>
                </div>
            </div> <!-- end of first data section -->
        </div> <!-- end of first validate section -->

        <br /><br />
        <!-- APPROVE/CANCEL/HOLD BUTTONS -->
        <div class='btnlinks'>

           <% Html.RenderAction("BackBtn", "Back"); %>

            <% if (!isExpired)  { %>

           <%if (!NoteCancelled)
                       { %>
                            <button id="Button1" onclick="cancelCoverNote()">Cancel Cover Note</button>
                    <% } %>


                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("CoverNotes", "Approve"))) { %>
                    <% if (!NoteActive && !NoteCancelled) { %>
                            <button hidden = "hidden" id="approve" onclick="approveCoverNote()">Approve</button>
                    <%  } %>
                    <% else if (!NoteCancelled)
                       { %>
                            <button id="cancel" onclick="cancelCoverNote()">Cancel Cover Note</button>
                    <% } %>
                <% } %>
                <% else  { %>
                    <% if (!NoteActive && !NoteCancelled)
                       { %>
                            <input hidden = "hidden" type="button" value="Request Approval" id = "reqApproval" onclick="RequestCoverNoteApproval();" /> 
                    <%  } %>
                <%  } %>  
           <%  } %> 
      
         <!---------------- ----------------->
        <% if ((Model.approved || Model.cancelled) && canPrint) { %>
            <button class="printbtn submitbutton" type="button" onclick="PrintNote()">Print Cover Note</button>
        <% } %>  
        <% if (!Model.approved && !isExpired && !Model.cancelled) { %>  
            <button class="editCoverNote">Edit</button>
        <% } %>  
        
        
          </div>

     </fieldset>

    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/vehiclecovernote-details.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/VehicleCoverNote.js" type="text/javascript"></script>
    <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/Policies.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/print.js" type="text/javascript"></script>
</asp:Content>


