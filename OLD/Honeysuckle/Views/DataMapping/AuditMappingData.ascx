﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.MappingData>" %>

<div class="editor-label">
    Table:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.table) %>
</div>
            
<div class="editor-label">
    Reference ID:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.refid) %>
</div>
            
<div class="editor-label">
    Data:
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.data) %>
</div>
            
<div class="editor-label">
    Lock:
</div>
<div class="editor-field">
    <%: Html.CheckBoxFor(model => model.maplock) %>
</div>