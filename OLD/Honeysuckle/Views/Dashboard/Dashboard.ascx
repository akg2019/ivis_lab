﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div id="dashboard">
    <% if (Honeysuckle.Models.Constants.user.username != "not initialized")
        { %>
        <div id="DashboardHeader" style= "padding-bottom:7px!important">
            <img src="../../Content/more_images/Home_Icon.png" />
            <span>Dashboard</span>
        </div>
        <div id="DashboardBody">
            <%: Html.Hidden("pageIndex", "index", new { @id = "startpage", @class = "dont-process" })%>
            <!-- COMPANIES QUICK LIST -->
            <div style ="padding-left:14px; padding-top:7px;"  ng-app="eGovApp" ng-controller="MyeGovController">
                <input id="GenReport" type = "button" class = "btn btn-primary" value = "Export Cover Data Report" ng-click ="DisplayFiler()" />
                <br />

              

                <div id="FilterArea"  hidden = "hidden">
                <br />
                    <lable>Cover Type</lebel>
                    <select style="width:80px!important" ng-model="TypeOfCover">
                        <option selected = "selected">Both</option>
                        <option>Cover Notes</option>
                        <option>Certtificates</option>
                    </select>

                    <label style="padding-left:5px;">Company</label>
                    <select ng-model="Intermediary" ng-selected="GetIntID">
                    <option selected="selected">All</option>
                        <option ng-repeat = "n in IM" id= {{n.CompanyID}} class = "optionCompany"> {{n.CompanyName}}</option>
                       
                    </select>

                     <label style="padding-left:5px;">From</label>
                     <input type="date"  style="width:153px!important"  ng-model="FromDate"/>

                     <label style="padding-left:5px;">To</label>
                     <input type="date"  style="width:152px!important"  ng-model="ToDate" />

                     <br />
                   
                     <div style = "padding:10px; width:320px; margin:auto;">
                     <label id = "loadingmessage" style="color:green;"></label>
                     </div>
                     <div style="padding-right:17px">
                        <input type = "button" class = "btn btn-primary" style="float:right" value = "Generate Report" ng-click = "GenerateCoverReport()" />
                     </div>
                   

                <br />
                </div>

   <!-- <div id="companies" class="table-data"> -->           
     <div class="container big">

    <div class="stripes"></div>
    <div class="dashboard-pad">
        <div class="form-header"><img src="../../Content/more_images/Company.png" />
            <div class="form-text-links">
                <span class="form-header-text">Quick List</span>
            </div>
            
        </div>
        <br>
                <input type ="text" placeholder = "Quick search company here" class = "form-control" style= "width:97%!important; margin:auto" ng-model = "CompanyFilter"  />
        <div id="loadingMessage" style ="text-align:center; width:350px; margin:auto; padding:30px;"> Loading quick list please wait..... </div>
                        <table>
                            <tr class="HeaderTr">
                                <td>Company Name</td>
                                <td>Code</td>
                                <td>Type</td>
                                
                                <td style= "text-align:right!important;">Company Actions</td>
                            </tr>
                            <tr ng-repeat= "item in IM | filter:CompanyFilter" class="innerTd" ng-class="HomeDefaultValues.CompanyID == item.CompanyID ? 'mine' :  (($index + 1) % 2 > 0 ? (item.Enabled == true ? 'oddRow' : 'oddRow disabledz') : (item.Enabled == true ? 'evenRow' : 'evenRow disabledz'))">
                                <td><b class="PolicyNumber">{{item.CompanyName}}</td>
                                <td class="PolicyNumber">{{item.compshort}}</td>
                                <td class="PolicyNumber">{{item.CompanyType}}</td>
                                
                                <td class="PolicyNumber">
                                    <label hidden = "hidden" id = "{{item.CompanyID}}" class = "id dont-process"></label>
                                    <div ng-show = "HomeDefaultValues.company_update && HomeDefaultValues.CompanyID == item.CompanyID || HomeDefaultValues.am_administrator || HomeDefaultValues.am_iaj_administrator">
                                        <button class = "btn btn-xs btn-primary" ng-click = "EditCompany(item.CompanyID)" title="Edit Company" style = "float:right!important; margin-right:3px!important;">Edit</button>
                                        
                                    </div>

                                    <div ng-show = "!item.has_data && HomeDefaultValues.company_delete" title="Delete Company">
                                         <button class = "btn btn-xs btn-danger" ng-click = "DeleteCompany(item.CompanyID)" style = "float:right!important; margin-right:3px!important;">X</button>
                                    </div> 

                                    <div ng-show = "HomeDefaultValues.company_enable">
                                        <div ng-show = "item.Enabled"  title="Deactivate Company">
                                           <button class = "btn btn-xs btn-danger" ng-click = "DisableCompany(item.CompanyID)" style = "float:right!important; margin-right:3px!important;">DA</button>    
                                        </div> 

                                       <div ng-show = "item.Enabled == false || item.Enabled == null" title="Activate Company">
                                           <button class = "btn btn-xs btn-success" ng-click = "EnableCompany(item.CompanyID)" style = "float:right!important; margin-right:3px!important;">AC</button>       
                                       </div> 
                                    </div>

                                   

                                
                                </td>

                            </tr>

                        </table>

                       

    </div>
    </div>
    <div id="legend"><span class="companykey mycomp">My Company</span> <div class="key myCompany"></div><span class="companykey disabled">Disabled Companies</span><div class="key disabledCompany"></div></div>
<!-- <div id="companies" class="table-data"> -->




            </div>

         
            <div hidden= "hidden" class="companies-list.f" >
                <div class="container big">
                    <div class="processing">
                        <span>Quick List Loading</span>
                        <img src="../../Content/more_images/loading.gif" />
                    </div>
                </div>
            </div>
            
           <!-- <% if (!(Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user))) { %>
                COVER LIST
                <div class="cover-list" hidden="hidden">
                    <div class="container big">
                        <div class="processing">
                            <span>Cover List Loading</span>
                            <img src="../../Content/more_images/loading.gif" />
                        </div>
                    </div>
                </div>
            <% } %>
            
            <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("BI", "Read"))) { %>
                 BUSINESS INTELLIGENCE 
                <div class="business-intelligence"  hidden="hidden">
                    <div class="container big">
                        <div class="processing">
                            <span>Business Intelligence Loading</span>
                            <img src="../../Content/more_images/loading.gif" />
                        </div>
                    </div>
                </div>
            <% } %>-->
            
            
        </div>

    <% } %>
</div>
<style type="text/css">
    table {border:none!important; width:100%!important; }
    td{  border:none!important;padding:0,5px,0,5px!important;}
    
    .TextBox{width:85%!important; margin-bottom:0!important;}
    
      .TextBox2{width:98%!important;}
    
    .FilterTitle
    {
        text-align:center;
        color:Orange;
    }
    
    .HeaderTr
    {
        border:solid;
        border-width:thin;
        padding:5px,1px,5px,1px;
        border-color:#e0d5d5;
        background:#bdbab7;
    }
    
    .TdButtons
    {
        float:right;
        padding-right:5px;
    }
    
    
    .innerTd
    {
        border-bottom:solid;
        border-bottom-width:thin;
        border-bottom-color:#e0d5d5;
    }
    
    
    .innerTd:hover
    {
        background:lightgreen;
        cursor:pointer;
    }

    .oddRow{
     background:#ddd;
    }
    .evenRow{
     background:#e6e6e6;
    }
    .disabledz
    {
            background: #950011;
    color: #fff;
    }

    .mine{    background: #8cc63f;
    color: #fff}

    
    .PolicyNumber{font-size:12px;}
    
    .Numbering{color:#ea5e0b;font-weight:bold;}
    
    .odds{background:#e8e8e8;}
    
    #LoadingMessage{color:#ea5e0b;}

</style>