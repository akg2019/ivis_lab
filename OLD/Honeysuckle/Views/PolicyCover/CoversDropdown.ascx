﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.PolicyCover>>" %>

<%: Html.DropDownList("polCovers", new SelectList(Model.ToList(), "ID", "cover"), new { @class = "covers_dropdown", id= "polCoverBox"} )%>