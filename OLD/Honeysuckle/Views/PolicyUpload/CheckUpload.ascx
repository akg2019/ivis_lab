﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.JSON>" %>

<div class="line">
        <div id="searching" class="index-search">
        <%: Html.TextBox("search-json", null, new { @placeholder = "Search Uploads by ID", @autocomplete = "off", @class = "modalSearch jsonSearch" })%> 
            <button id="search-json-btn" type="button" class="quick-search-btn"  ><img src="../../Content/more_images/Search_Icon.png" /></button>
        </div>
    </div>
<div class="ivis-table">
            <div class="covercert menu">
                <div class="jsonID item">ID</div>
                <div class="fileName item">File Name</div>
                <div class="created item">Created</div>
                <div class="status item">Status</div>
                <div class="completed item">Completed On</div>
            </div>
            <div id="lastupload" class="table-data">
                <div class="jsonID item"><%: Model.ID %></div>
                <div class="fileName item"><%: Model.title %> </div>
                <div class="created item"><%: Model.createdTime %></div>
                <div class="status item">
                    <% if (Model.completed)
                       {%>
                           Completed
                    <% }
                       else if (Model.inprogress)
                       { %>
                        Still Processing
                    <%}
                       else
                       {%>
                       Failed
                    <% } %>
       
                </div>
                <div class="completed item"><%: Model.completedTime %></div>
            </div>
</div>

<script src="../../Scripts/ApplicationJS/checkUpload.js" type="text/javascript"></script>

<link href="../../Content/checkUpload.css" rel="stylesheet" type="text/css" />