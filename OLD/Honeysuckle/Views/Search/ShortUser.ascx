﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.User>>" %>

<% //Html.RenderPartial("~/Views/User/ShowShortUser.ascx", Model); %>


<%
    bool amadmin = Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user);
    bool amiajadmin = Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user);
    bool user_update = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "Update"));
    bool user_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "Read"));  
    bool user_delete = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "Delete"));
    bool usergroup_update = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("UserGroups", "Update"));
    bool user_activate = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Users", "Activate"));
%>

    <%: Html.Hidden("count", Model.Count(), new { @class = "dont-process" })%>
    <div class="ivis-table">
        <div class="shortuser menu">
            <div class="rowuser uname item">
                Username
            </div>
            <div class="rowuser compname item">
                Company Name
            </div>
            <div class="rowuser name item">
                Name
            </div>
            <div class="rowuser trn item">
                TRN
            </div>
        </div>
        <div class="scroll-table">
        <% int i = 0; %>
        <% foreach (var item in Model) { %>
            <% i++; %>
            <% if (i%2 == 0) { %>
                <% if (item.active) { %>
                    <div class="shortuser data even row user">
                <% } %>
                <% else { %>
                    <div class="shortuser data even row user disabled">
                <% } %>
            <% } %>
            <% else{ %>
                <% if (item.active) { %>
                    <div class="shortuser data odd row user">
                <% } %>
                <% else { %>
                    <div class="shortuser data odd row user disabled">
                <% } %>
            <% } %>
        
                <%: Html.Hidden("hiddenid", item.ID, new { @class = "userid id dont-process" })%>
                <div class="rowuser uname item">
                    <%: item.username %>
                </div>
                <div class="rowuser compname item">
                    <%: item.employee.company.CompanyName %>
                </div>
                <div class="rowuser name item">
                    <%: item.employee.person.fname + " " + item.employee.person.lname %>
                </div>
                <div class="rowuser trn item">
                    <%: item.employee.person.TRN %>
                </div>
                <div class="rowuser functions item">
                    <% if (user_update || Honeysuckle.Models.UserGroupModel.AmAnAdministrator(new Honeysuckle.Models.User(item.ID))) { %> 
                        <div id="editImage" class="icon edit user" title="Edit User"> 
                            <img src="../../Content/more_images/edit_icon.png" class= "sprite" />
                        </div> 
                    <% } %>
                    <% if (user_read) { %> 
                        <div class="icon view user" title="View User Details"> 
                            <img src="../../Content/more_images/view_icon.png" class="sprite" />
                        </div>
                    <% } %>
                    <% if (user_delete) { // || Honeysuckle.Models.UserGroupModel.AmAnAdministrator(new Honeysuckle.Models.User(item.ID))) { %>  <!-- ADD TEST IF USER CAN BE DELETED -->
                        <div id="deleteImage" class="icon delete user" title="Delete User"> 
                            <img src="../../Content/more_images/delete_icon.png" class="sprite" />
                        </div> 
                    <% } %>
                    <% if ((amadmin || amiajadmin) && item.active) { %> 
                        <div id="resetPassImage" class="icon resetpass user" title="Reset Password"> 
                            <img src="../../Content/more_images/reset_password_icon.png" class="sprite" />
                        </div> 
                    <% } %>
                    <% if ((amadmin || amiajadmin) && item.active) { %> 
                        <div id="resetLoginImage" class="icon resetquest user" title="Reset First Login Time"> 
                            <img src="../../Content/more_images/reset_timer_icon.png" class="sprite" />
                        </div> 
                    <% } %>
                    <% if (amadmin || usergroup_update) { %>
                        <div class="icon assignrole user" title="Manage Roles for User">
                            <img src="../../Content/more_images/user_role_icon.png" class="sprite" />
                        </div>
                    <% } %>
                    <% if (user_activate){ %>
                        <div class="addbtns">
                            <% if(item.active){ %>
                                 <div id="disableImage" class="icon disable user" title="Deactivate User"> 
                                    <img src="../../Content/more_images/disable_icon.png" class="sprite" />
                                </div> 
                            <% } %>
                            <% else{ %>
                                  <div id="enableImage" class="icon enable user" title="Activate User"> 
                                    <img src="../../Content/more_images/enable_icon.png" class="sprite" />
                                </div> 
                            <% } %>
                        </div>
                    <% } %>
                
                </div>
            </div>
    
        <% } %>
       </div>
    </div>

