﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Honeysuckle.Models.Search>>" %>
<%if (!Model.Any())
  { %>
<h2 class="center" style= "text-align:center;">
    <cite>NO RESULTS FOUND</cite>
</h2>
<%}
  else
  { %>
<% if (ViewData.Model.Any(x => x.table.Equals("covernote")))
   {%>
<div id="covernotes" class="search-result">
    <div class="search-headers subheader arrow-click">
        <div class="arrow right">
        </div>
        <img class="normal" src="../../Content/more_images/Cover_Note.png" /><span class="category">Cover
            Notes:</span><span class="count"><%Model.Where(x => x.table.Equals("covernote")).Select(x => x.covernote).Count(); %>
                Found</span></div>
    <div id="search-results-covernotes" class="results">
        <%
            Html.RenderPartial("ShortCoverNote", Model.Where(x => x.table.Equals("covernote")).Select(x => x.covernote).ToList());
        %>
    </div>
</div>
<%}%>
<% if (ViewData.Model.Any(x => x.table.Equals("certificate")))
   {%>
<div id="certificates" class="search-result">
    <div class="search-headers subheader arrow-click">
        <div class="arrow right">
        </div>
        <img class="normal" src="../../Content/more_images/Certificate.png" /><span class="category">Certificates:</span><span
            class="count">Found</span></div>
    <div id="search-results-certificates" class="results">
        <% 
            Html.RenderPartial("ShortCertificate", Model.Where(x => x.table.Equals("certificate")).Select(x => x.certificate).ToList());
        %>
    </div>
</div>
<%} %>
<% if (ViewData.Model.Any(x => x.table.Equals("vehicle")))
   {%>
<div id="vehicles" class="search-result">
    <div class="search-headers subheader arrow-click">
        <div class="arrow right">
        </div>
        <img class="normal" src="../../Content/more_images/Vehicle.png" /><span class="category">Vehicles:</span><span
            class="count">Found</span></div>
    <div id="search-results-vehicles" class="results">
        <% 
            Html.RenderPartial("ShortVehicle", Model.Where(x => x.table.Equals("vehicle")).Select(x => x.vehicle).ToList());
        %>
    </div>
</div>
<%} %>
<% if (ViewData.Model.Any(x => x.table.Equals("policy")))
   {%>
<div id="policies" class="search-result">
    <div class="search-headers subheader arrow-click">
        <div class="arrow right">
        </div>
        <img class="normal" src="../../Content/more_images/Policy.png" /><span class="category">Policies:</span><span
            class="count"><%Model.Where(x => x.table.Equals("policy")).Count(); %>
            Found</span></div>
    <div id="search-results-policies" class="results">
        <% 
            Html.RenderPartial("ShortPolicies", Model.Where(x => x.table.Equals("policy")).Select(x => x.policy).ToList());
        %>
    </div>
</div>
<%} %>
<% if (ViewData.Model.Any(x => x.table.Equals("company")))
   {%>
<div id="companies-search" class="search-result">
    <div class="search-headers subheader arrow-click">
        <div class="arrow right">
        </div>
        <img class="normal" src="../../Content/more_images/Company.png" /><span class="category">Companies:</span><span
            class="count">Found</span></div>
    <div id="search-results-companies" class="results">
        <% 
            Html.RenderPartial("ShortCompany", Model.Where(x => x.table.Equals("company")).Select(x => x.company).ToList());
        %>
    </div>
</div>
<%} %>
<% if (ViewData.Model.Any(x => x.table.Equals("user")))
   {%>
<div id="users" class="search-result">
    <div class="search-headers subheader arrow-click">
        <div class="arrow right">
        </div>
        <img class="normal" src="../../Content/more_images/User.png" /><span class="category">Users:</span><span
            class="count">Found</span></div>
    <div id="search-results-users" class="results">
        <% 
            Html.RenderPartial("ShortUser", Model.Where(x => x.table.Equals("user")).Select(x => x.user).ToList());
        %>
    </div>
</div>
<%} %>
<% if (ViewData.Model.Any(x => x.table.Equals("people")))
   {%>
<div id="people" class="search-result">
    <div class="search-headers subheader arrow-click">
        <div class="arrow right">
        </div>
        <img class="normal" src="../../Content/more_images/User.png" /><span class="category">Policy
            Holders (People):</span><span class="count">Found</span></div>
    <div id="search-results-people" class="results">
        <% Html.RenderPartial("ShortPeople", Model.Where(x => x.table.Equals("people")).Select(x
        => x.user).ToList()); %>
    </div>
</div>
<%} %>
<%} %>