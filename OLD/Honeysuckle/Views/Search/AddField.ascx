﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div class="search-field">
    <%: Html.DropDownList("andor", new SelectList(new List<Object>{new { value = "and", text = "and"}, new { value = "or", text = "or" }}, "value", "text"), new { @class = "andor" } )  %>
    <%: Html.DropDownList("module", new SelectList(new List<Object> { new { value = "all", text = "all" }, new { value = "vehicle", text = "vehicle" }, new { value = "user", text = "user" }, new { value = "policy", text = "policy" }, new { value = "covernote", text = "cover note" }, new { value = "certificate", text = "certificate" } }, "value", "text"), new { @class = "module" })%>
    <%: Html.TextBox("search-add", null, new { @class = "search-add", @PlaceHolder = "Search [RegNo or Chassis]" })%>
    <div class="run"><button class="runbtn search-btn-adv" onclick="updateSearch()" value="Search" ><img src="../../Content/more_images/Search_Icon.png" /></button></div>
    <div class="rm"><input class="rmbtn" type="button" onclick="rmField(this)" value="x" /></div>
</div>