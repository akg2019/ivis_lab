﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<% using (Html.BeginForm("Advanced", "Search", FormMethod.Get)) { %>
    <div class="gen-search">
        <input type="text" id="search" name="search"  placeholder="Search  [RegNo or Chassis]"/>
        <button type="submit" id="gen-search-submit" ><img src="../../Content/more_images/Search_Icon.png" /></button>
    </div>
<% } %>

