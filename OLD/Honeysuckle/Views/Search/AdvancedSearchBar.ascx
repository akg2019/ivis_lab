﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
   
<div id="adv-search-form">
    <% 
        if (ViewData["search"] != null) { %> 
      

         <%: Html.TextBox("advsearchChassis", null, new { id = "advanced-search-Chassis", @class = "form-control", @PlaceHolder = "Search [Chassis]" })%> 
          <%: Html.TextBox("advsearchReg", null, new { id = "advanced-search-Reg", @class = "form-control", @PlaceHolder = "OR Search [Reg Number]" })%> 
          <%: Html.TextBox("advsearch", null, new { id = "advanced-search-bar", @class = "adv-search-text first", @Value = ViewData["search"].ToString(), @PlaceHolder = "Search [RegNo or Chassis]", @hidden = "hidden" } )%> 
           <p class="btn btn-success" onclick="search()"> Search</p>                                                                       
        
      <% } %><% else { %>
       
          <%: Html.TextBox("advsearch", null, new { id = "advanced-search-bar", @class = "adv-search-text additional", @PlaceHolder = "Search [Company]" })%> 
          <%: Html.TextBox("advsearchChassis", null, new { id = "advanced-search-Chassis", @class = "adv-search-text additional", @PlaceHolder = "Search [Chassis]" })%> 
          <%: Html.TextBox("advsearchReg", null, new { id = "advanced-search-Reg", @class = "adv-search-text additional", @PlaceHolder = "OR Search [Reg Number]" })%> 
           <p class="btn btn-success" onclick="search()"> Search</p>
     <% 
      
      } %>
    <button hidden="hidden" id="advsearchbtn" value="Search" onclick="search()" class="search-btn-adv" ><img src="../../Content/more_images/Search_Icon.png" /></button>
    <div id="more-terms"></div> <!-- THIS IS WHERE ALL THE NEW TERMS WOULD GO -->
    <hr />
</div>
<div id="add-search-term" class="add-dynamic small" hidden="hidden">
    <img src="../../Content/more_images/ivis_add_icon.png" />
    <span>Add Search Term</span>
</div>