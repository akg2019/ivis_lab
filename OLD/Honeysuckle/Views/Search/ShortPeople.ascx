﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Person>>" %>
<%
    bool policyholder_read = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("PolicyHolder", "Read"));
    bool policyholder_update = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("PolicyHolder", "Update"));
%>
<%: Html.Hidden("count", Model.Count(), new { @class = "dont-process" })%>
<div class="ivis-table">
    <div class="shortpeople menu">
        <div class="rowpeople fname item">
            First
        </div>
        <div class="rowpeople mname item">
            Middle
        </div>
        <div class="rowpeople lname item">
            Last
        </div>
        <div class="rowpeople tnr item">
            TRN
        </div>
    </div>
    <div class="scroll-table">
        <% int i = 0; %>
        <% foreach (var item in Model)
           { %>
        <% string TRN = "";
           if (item != null) if (item.TRN == "10000000") TRN = "Not Available"; else TRN = item.TRN;
        %>
        <% i++; %>
        <% if (i % 2 == 0)
           { %>
        <div class="person data even row">
            <% } %>
            <% else
                { %>
            <div class="person data odd row">
                <% } %>
                <%: Html.Hidden("did", item.PersonID, new { @class = "hiddenid id dont-process" })%>
                <div class="rowpeople fname item">
                    <%: item.fname %>
                </div>
                <div class="rowpeople mname item">
                    <%: item.mname %>
                </div>
                <div class="rowpeople lname item">
                    <%: item.lname %>
                </div>
                <div class="rowpeople trn item">
                    <%: TRN %>
                </div>
                <div class="rowpeople functions item">
                    <% if (policyholder_read)
                       { %>
                    <div class="icon view" title="View Policy Holder">
                        <img src="../../Content/more_images/View_icon.png" class="sprite" />
                    </div>
                    <% } %>
                    <% if (policyholder_update)
                       { %>
                    <div class="icon edit" title="Edit Policy Holder">
                        <img src="../../Content/more_images/Edit_icon.png" class="sprite" />
                    </div>
                    <% } %>
                </div>
            </div>
            <% } %>
        </div>
    </div>
</div>
