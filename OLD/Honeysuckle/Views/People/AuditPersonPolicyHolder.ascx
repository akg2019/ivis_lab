﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Person>" %>

<div class="editor-label">
    Policy Holder
</div>
<div class="editor-field">
    <%: Html.TextBoxFor(model => model.fname, new { @readonly = "readonly", @class = "read-only", @Value = Model.fname + ' ' + Model.lname })%>
</div>