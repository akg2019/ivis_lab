﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Person>" %>

       
<%: Html.HiddenFor(model => model.PersonID, new { id = "personId", @class = "dont-process" })%>
<% string TRN = "";
     if (Model.TRN == "10000000") TRN = "Not Available"; else TRN = Model.TRN;
     %>
    <div class="container big nomtop">
    <div id="duplicates"></div>
        <div class="stripes"></div>
        <div class="data">
             <% if ((bool)ViewData["IVISRecords"])
                   { %> 
                <div class="icon-record"><img src="../../Content/more_images/data-ivis.png" /></div>
                <% }
                   else
                   { %>
                <div class="icon-record"><img src="../../Content/more_images/data-taj.png" /></div>
                 <% } %>
            <div class="header">TRN:</div>
            <div class="label trn"><%: TRN %></div>
            
         </div>
    </div>
    <%--<div class="headingDisplay"><%: ViewData ["HeadingMessage"] %> </div>--%>
    <div class="container small left">
        <div class="stripes"></div>
        <div class="data">
            <div class="header">Name:</div>
            <% if (Model.lname == "" && Model.fname == "")
               {%>
                 <div class="label">Not Available</div><br /><br /><br /><br />
            <% }
               else
               { %>
                <div class="label"><%: Model.lname%>,</div><br />
                <div class="label"><%: Model.fname%></div><br />
                <div class="label"><%: Model.mname%></div><br /><br /><br />
               <% } %>
        </div>
   </div>
   <div class="container small">
        <div class="stripes"></div>
        <div class="data">
            <div class="header">Address:</div>
            <% if (Model.address.longAddressUsed)
               { %>
               <div class="label"><%: Model.address.longAddress%></div><br /><br /><br /><br /><br />
            <% }
               else
               {%>
               <% if (Model.address.roadnumber != null && Model.address.road.RoadName != null && Model.address.roadtype != null && Model.address.roadtype.RoadTypeName != null)
                  {%>
            <div class="label"><%: Model.address.roadnumber%> <%: Model.address.road.RoadName%> <%: Model.address.roadtype.RoadTypeName%></div><br/>
            <% } %>
            <% if (Model.address.ApartmentNumber != null)
               { %>
            <div class="label"><%: Model.address.ApartmentNumber%></div><br/>
            <% } %>
            <div class="label"><%: Model.address.city.CityName%></div><br/>
            <div class="label"><%: Model.address.parish.parish%></div><br />
            <div class="label"><%: Model.address.country.CountryName%></div>
            <% } %>
        </div>
   </div>
   <% if(Model.hasDuplicates){ %>
    <div style="color:Red;">Record has duplicates</div>
   <% } %>
       


 
  

