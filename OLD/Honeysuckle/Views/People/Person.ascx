﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Person>" %>

<% using (Html.BeginCollectionItem("person")) { %>
    <%: Html.ValidationSummary(true) %>
    
    <% if (!(bool)ViewData["policyInsured"]) { %>
    <div class="validate-section">
    <% } %>
        <div class="subheader">
            <% if (!(bool)ViewData["policyInsured"]) { %>
            <div class="arrow right"></div>
            <% } %>
            Personal Data
            <% if (!(bool)ViewData["policyInsured"]) { %>
            <div class="valid"></div>
            <% } %>
        </div>
    <% if (!(bool)ViewData["policyInsured"]) { %>
        <div class="data">
    <% } %>
            <%: Html.Hidden("trnChange",false, new { id = "trnChange", @class = "dont-process" })%>
            <%: Html.HiddenFor(model => model.PersonID, new { id = "personid", @class = "dont-process" })%>
            <%: Html.Hidden("editingPolHolder",false, new{ id = "editingPolHolder" ,@class="dont-process" }) %>
            <% bool editTRN = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("TRN", "Edit")); %>
            <div class="editor-label hideperson">
                TRN
            </div>
            <div class="editor-field">
            <div class="foreign-div hideperson" style="float: left; width:30%;">
                <span class="foreign-id-label" style="font-size:0.8em; width:30%;">Foreign ID</span>
                <%: Html.CheckBox("foreignID", false, new { id = "foreignIDCheck", style = "float: right;" })%> 
             </div>  
               
              <div class="foreign-div hideperson" style="float: left; width:30%;">
               <span class="foreign-id-label" style="font-size:0.8em; width:30%;">No TRN</span>
                <%: Html.CheckBox("noTRN", false, new { id = "noTRN", style = "float: right;" })%> 
              </div>
              
           
                <%: Html.TextBoxFor(model => model.TRN, new { id = "trn-editPolHol" , @PlaceHolder = " Eg. 154785623", @class = "person-trn hideperson" })%>
                <%: Html.ValidationMessageFor(model => model.TRN) %>
                <% if (ViewData["editButton"] != null && (bool)ViewData["editButton"] && true)
                   {%>
                <button id="edit-trn" class ="editButton hideperson" type="button">Edit</button>
                <% } else {%>
                <span style="color:Red;font-size:0.5em"></span>
                <% } %>
            </div>

            <div class="editor-label">
                First Name
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.fname, new { @required = "required", id = "fname", @class = "name" })%>
                <%: Html.ValidationMessageFor(model => model.fname) %>
            </div>
            
            <div class="editor-label" hidden ="hidden">
                Middle Name
            </div>
            <div class="editor-field" hidden ="hidden">
                <%: Html.TextBoxFor(model => model.mname, new { id = "mname", @class = "name",@required="required" })%>
                <%: Html.ValidationMessageFor(model => model.mname) %>
            </div>
            
            <div class="editor-label">
                Last Name
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.lname, new { @required = "required", id = "lname", @class = "name" })%>
                <%: Html.ValidationMessageFor(model => model.lname) %>
            </div>
    <% if (!(bool)ViewData["policyInsured"]) { %>        
        </div>
    </div>
    <% } %>
    <!-- Full adress layout -->

    <!--////////////////// -->
    <% if (!(bool)ViewData["policyInsured"]) { %>
    <div class="validate-section" hidden ="hidden">
    <% } %>
        <div class="subheader">
            <% if (!(bool)ViewData["policyInsured"]) { %>
            <div class="arrow right"></div>
            <% } %>
            Address
            <% if (!(bool)ViewData["policyInsured"]) { %>
            <div class="valid"></div>
            <% } %>
        </div>
    <% if (!(bool)ViewData["policyInsured"]) { %>
        <div class="data">
    <% } %>
        <div id="perStructuredAddress" class="dont-process" >
            <div class="editor-label">
                Street 
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.address.roadnumber, new { id = "RoadNumber", @PlaceHolder = " 12", @autocomplete = "off"})%>
                <%: Html.TextBoxFor(model => model.address.road.RoadName, new { id = "roadname", @PlaceHolder = " Short Hill", @autocomplete = "off" })%>
                <%: Html.TextBoxFor(model => model.address.roadtype.RoadTypeName, new { id = "roadtype", @PlaceHolder = " Avenue", @autocomplete = "off" })%>
                
                <%: Html.ValidationMessageFor(model => model.address.roadnumber)%>
                <%: Html.ValidationMessageFor(model => model.address.road.RoadName)%>
                <%: Html.ValidationMessageFor(model => model.address.roadtype.RoadTypeName)%>
            </div>

            <div class="editor-label">
                Building/Apartment No. 
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.address.ApartmentNumber, new { id = "aptNumber", @autocomplete = "off" })%>
                <%: Html.ValidationMessageFor(model => model.address.ApartmentNumber)%>
            </div>

            <div class="editor-label">
                City/Town
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.address.city.CityName, new { id = "city", @required = "required", @autocomplete = "off", @class = "city" })%>
                <%: Html.ValidationMessageFor(model => model.address.city.CityName)%>
            </div>
            <div class="editor-label">
                Parish
            </div>
            <div class="editor-field parishm">
                <%: Html.HiddenFor(model => model.address.parish.ID, new { id = "parish", @class = "dont-process" })%>
                <% Html.RenderAction("ParishList", "Parish"); %>
                <%: Html.ValidationMessage("parish") %>
            </div>
            <div class="editor-label">
                Country
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.address.country.CountryName, new { id = "country", @required = "required", @autocomplete = "off", @class = "country" })%>
                <%: Html.ValidationMessageFor(model => model.address.country.CountryName)%>
            </div>
        </div>
            <div id="perCombinedAddress" class="dont-process">
                    <div class="editor-label">
                       <div>Combined Address</div>
                    </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.address.longAddress, new { id = "longAddress", @readonly = "readonly", @autocomplete = "off", @class = "address-details read-only" })%>
                    <%: Html.ValidationMessageFor(model => model.address.longAddress) %>
                </div>
        </div> 
    <% if (!(bool)ViewData["policyInsured"]) { %>
        </div>
    </div>
    <% } %>

    <!-- -->

    <% if (!(bool)ViewData["policyInsured"]) { %>
        <div class="validate-section"  hidden ="hidden">
            <div class="subheader">
                <div class="arrow right"></div>
                Additional Emails
                <div class="valid"></div>
            </div>
            <div class="data">
                <div class="emails">
                    <div class="email-container">
                        <% if (Model.emails != null) { %>
                            <% foreach (Honeysuckle.Models.Email email in Model.emails) { %>
                                <% Html.RenderPartial("~/Views/Email/NewEmail.ascx", email); %>
                            <% } %>
                        <% } %>
                    </div>
                    <% if (ViewData["edit"] != null)
                       { %>
                        <% if ((bool)ViewData["edit"])
                           { %>
                            <div class="addemail">
                                <div id="addNewEmail" class="add-dynamic small">
                                    <img src="../../Content/more_images/ivis_add_icon.png" />
                                    <span>Add Email</span>
                                </div>
                            </div>
                        <% }
                       } %>
                </div>
            </div>
        </div>
    <%  } %> 

    <% if (!(bool)ViewData["policyInsured"]) { %>
        <div class="validate-section"  hidden ="hidden">
            <div class="subheader">
                <div class="arrow right"></div>
                Phone Numbers
                <div class="valid"></div>
            </div>
            <div class="data">
                <div class="phones">
                    <div class="phone-container">
                        <% if (Model.phoneNums != null) { %>
                                    <% foreach (Honeysuckle.Models.Phone phone in Model.phoneNums) { %>
                                        <div class="phone dont-process"><div class="subheader dont-process"><span>Phone Number</span><input type="button" value="Remove" class="remove btnclr rmphone dont-process"/></div>
                                        <% Html.RenderPartial("~/Views/Phone/NewPhone.ascx", phone); %> 
                                        </div>
                                        <br />
                                    <% } %>
                                <% } %>
                    </div>
                    <% if (ViewData["edit"] != null)
                       { %>
                        <% if ((bool)ViewData["edit"])
                           { %>
                            <div class="addNewphone">
                                <div id="addphone" class="add-dynamic small">
                                    <img src="../../Content/more_images/ivis_add_icon.png" />
                                    <span>Add Phone Number</span>
                                </div>
                            </div>
                        <% }
                       } %>
                </div>
            </div>
        </div>
    <%  } %> 
    <script src="../../Scripts/ApplicationJS/parishUpdate.js" type="text/javascript"></script>
    <% if ((bool)ViewData["policyInsured"]) { %> <script src="../../Scripts/ApplicationJS/ExistingPerson.js" type="text/javascript"></script>  <%  } %> 
    <script src="../../Scripts/ApplicationJS/multiple-emails.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/multiplePhoneNums.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/editPolicyHolder-personTRN.js" type="text/javascript"></script>
<% } %>

    

