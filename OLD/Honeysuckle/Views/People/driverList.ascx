﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Person>>" %>

 <div id="insured" class="insured-modal">
 
    <%--<div class="line">
        <span class="mess">Please tick the appropriate person (s):</span>
        <div id="searching" class="index-search">
            <%: Html.TextBox("search-modal", null, new { @placeholder = "Search Drivers", @autocomplete = "off", @class="modalSearch"})%> 
            <button type="button" class="quick-search-btn" onclick="searchPeople(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
        </div>
    </div>--%>

        <div>
             <table style="width:100%!important; border-style:none!important;">
                    <tr>
                        <td>&nbsp;Search By <b><i style="color:Green;">Trn</i> </b><%:Html.RadioButton("search-type","Trn",true) %></td>
                        <td>&nbsp;Search By <b><i style="color:Green;">First Name</i>  </b> <%:Html.RadioButton("search-type","Fn") %></td>
                        <td>&nbsp;Search By <b><i style="color:Green;">Last Name</i> </b> <%:Html.RadioButton("search-type","ln") %></td>
                    </tr>
                   
               </table>
               <br />
               <div style ="height:2px;"></div>
               <%: Html.TextBox("search-modal", null, new { @placeholder = "Search People", @autocomplete = "off", @class = "modalSearch peopleSearch" })%> 

            <button type="button" class="quick-search-btn" onclick="searchPeople(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
            <span class="mess">&nbsp;&nbsp; Please tick the appropriate person(s):</span>
            <div style ="height:10px;"></div>
        </div>

 
     <div class="ivis-table modal-table">
         <div class= "heading menu"> 
            <div class="select item">Select</div>
            <div class="trn item">TRN</div>
            <div class="fname item">First Name</div>
            <div class="lname item">Last Name</div>
        </div>
        <div id="selected-insured" class="static selectedSection"></div>
        <div id="insureds" class="dynamic" >
            <% if(Model.Count() == 0) { %>
                    <div class="rowNone">There Are No Drivers. Please add one.</div>
            <% } %>
            <% else { %>

                <% if(Model.Count() > 50) { %>
                    <div class="rowNone">
                        Please search the system for existing insureds, or add a new insured. 
                    </div>
                <% } %>

                <% int i = 0; %>
                <% foreach (Honeysuckle.Models.Person item in Model) { %>
                    <% if (i%2 == 0) { %> <div class="insured-row row even" > <% } %>
                    <% else { %> <div class="insured-row row odd" > <% } %>
                    <% i++; %>
        
                        <div class="insured-check select item" >
                            <%: Html.CheckBox("insuredcheck", false, new { @class = "insuredcheck", id = item.PersonID.ToString(), @OnClick="changeCheckBox(this)" })%>
                        </div>
                        <div class="insured-trn trn item" >
                            <% if (item.TRN == "10000000")
                               { %>
                                Not Available
                            <% }
                               else
                               {%>
                                <%: item.TRN %>
                            <% } %>
                        </div>
                        <div class="insured-fname fname item" >
                            <%: item.fname %> 
                        </div>
                        <div class="insured-lname lname item" >
                            <%: item.lname %>
                        </div>
        
                    </div>
                 <% } %>
            <% } %>
        </div>
    </div>
   
</div>

</div>