﻿
<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Person>" %>

  <div class="insured">
    <% using (Html.BeginCollectionItem("insured")) {%>
        <%: Html.ValidationSummary(true) %>

        <%: Html.HiddenFor(Model => Model.PersonID, new { @class = "perId dont-process" })%> 
        <%: Html.HiddenFor(model => model.base_id, new { @class = "base_per_id dont-process" })%>
        <%: Html.HiddenFor(Model => Model.ID, new{ @class="perImportID dont-process" }) %>

        <% 
           string PersonName = "";
            try
           {
               PersonName = Model.fname.ToString() + " " + Model.lname.ToString();
               ViewData["personName"] = Model.fname.ToString() + " " + Model.lname.ToString();
           }
           catch (Exception)
           {
            
           } %>

        <div class="label">
            Name:
        </div>
        <div class="data-field">
            <%: Html.HiddenFor(model => model.TRN, new { @readonly = "readonly", @class = "perTRN dont-process" })%>
            <%: Html.TextBox("Names", PersonName, new { @class = "names dont-process", @readonly = "readonly" })%>
        </div>

        <% if (!(bool)ViewData["peopleDetails"]) { %> <input type="button" value="Delete" class="remInsured"/> <% } %>
        <% else { %> <div class="blueSpace"></div> <% } %>
       
    <% } %>
    </div>