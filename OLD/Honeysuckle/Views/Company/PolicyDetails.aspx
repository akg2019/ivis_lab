﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	PolicyDetails
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <%
       int PolicyID = 0;
       try { PolicyID = int.Parse(TempData["PolicyID"].ToString()); }catch(Exception){}
 %>
    <h2>Policy Details</h2>
   
    <div id="DataArea" ng-app="CompanyDataPolicies" ng-controller="CompanyDataController" ng-init="GetDetails(<%: PolicyID%>)">
        
        <b>Policy</b>
        <hr />

        <b>Risk</b>
        <hr />

        <b>Insureds</b>
        <hr />

        <b>Drivers</b>
        <hr />

        <b>Cover Notes</b>
        <hr />

        <b>Certificates</b>
        <hr />
        
    </div>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.js"></script>
  <script src="../../Scripts/ApplicationJS/CompanyDataPolicies.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
