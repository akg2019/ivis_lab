﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Honeysuckle.Models.Employee>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ViewEmployees
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="employees">
        <div class="employee-row menu-row">
            <div class="emp-action"></div>
            <div class="emp-fname">First Name</div>
            <div class="emp-lname">Last Name</div>
            <div class="emp-active">
                Active
            </div>
        </div>

    <% foreach (var item in Model) { %>
    
        <div class="employee-row">
            <div class="emp-action">
                <% if (item.Active)
                   { %>
                    <%: Html.ActionLink("Deactivate", "DeactivateEmployee", new { id = item.EmployeeID })%> |
                <% } %>
                <% else
                   { %>
                    <%: Html.ActionLink("Activate", "ActivateEmployee", new { id = item.EmployeeID })%> |
                <% } %>
                <%// Html.ActionLink("Edit", "Edit", new { /* id=item.PrimaryKey */ }) %> |
                <%// Html.ActionLink("Details", "Details", new { /* id=item.PrimaryKey */ })%> |
                <%// Html.ActionLink("Delete", "Delete", new { /* id=item.PrimaryKey */ })%>
            </div>
            <div class="emp-fname">
                <%: item.person.fname %>
            </div>
            <div class="emp-lname">
                <%: item.person.lname %>
            </div>
            <div class="emp-active">
                <%: item.Active %>
            </div>
        </div>
    
    <% } %>

    </div>

    <p>
        <%: Html.ActionLink("Create New", "Create") %>
    </p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
</asp:Content>

