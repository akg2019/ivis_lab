﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Company>>" %>
<div style="color:Red;">Record has duplicates. Check a row to use the corresponding record or edit it.</div>
   <br />
    <br />
<%: Html.Hidden("duplicatePartial", null,new{ id="duplicatePartial" }) %>
    <table>
        <tr>
            
            <th>
                Select
            </th>
            <th>
                Name
            </th>
            
            <th>
                Created
            </th>
        </tr>

    <% foreach (var item in Model) { %>
    
        <tr>
            <td>
                <%: Html.CheckBox("dupCompTRNselect", false, new { @class = "dupCompTRNcheck", id = item.CompanyID.ToString(), @OnClick = "selectDuplicateComp(this)" })%>
            </td>
            <td>
                <%: item.CompanyName %>
            </td>
            <td>
                <%: item.createdOn %>
            </td>
        </tr>
    
    <% } %>

    </table>
