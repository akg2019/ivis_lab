﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	CompanyPolicies

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <%-- <h2 class="Title">Policies</h2>--%>
  
    <div  ng-app="CompanyDataPolicies" ng-controller="CompanyDataController" ng-init="GetPoliciesInit()" >
       

       <div style="width:95%; margin:auto;">
          
        <table id="MyTable">
                   <tr>
                        <td>Filter</td>
                        <td ng-show="FilterOption =='Policy'">Policy#</td>
                        <td ng-show="FilterOption =='Certificate'"><span style="font-size:12px;">Cert#/Reg#/Chass#/Pol#/TRN</span></td>
                        <td ng-show="FilterOption =='Cover Note'"><span style="font-size:12px;">CN#/Reg#/Chass#/Pol#/TRN</span></td>
                        <td>From Date</td>
                        <td>To Date</td>
                        <td></td>
                   </tr>
                    <tr>
                        <td>
                            <select class="form-control TextBox" ng-model="FilterOption" ng-change="GetFilteredData" style="height:35px!important;">
                                <option>Policy</option>
                                <option>Certificate</option>
                                <option>Cover Note</option>
                            </select>
                        </td>

                        <td ng-show="FilterOption =='Policy'"><input type="text" class="form-control TextBox" ng-model="FilteredText" placeholder="Policy#" /></td>
                        <td ng-show="FilterOption =='Certificate'"><input type="text" class="form-control TextBox" ng-model="FilteredText" placeholder="Cert/Reg/Chass#" /></td>
                        <td ng-show="FilterOption =='Cover Note'"><input type="text" class="form-control TextBox" ng-model="FilteredText" placeholder="Cn/Reg/Chass#" /></td>

                        <td>
                          <input type="date" id = "start" class="form-control TextBox" />
                        </td>
                         <td>
                           <input type="date" id = "end" class="form-control TextBox" />
                        </td>
                        <td>
                            <span ng-click="GetPolicies()" class="btn btn-sm btn-success" style="width:75%!important;">Filter</span>
                        </td>
                    </tr>
               </table>

               <hr />

            <div id="FilteredPanel">
                <div id="PolicyFIlter" ng-show="FilterOption =='Policy'">
                    <p class="FilterTitle"><b>{{FilterOption}}</b></p>
                    <p style="text-align:center;">Search results. Records found <b>{{Policies.length | number}}</b></p>
                    <p style="text-align:center;" id="LoadingMessage">Loading policy records please wait.....</p>
                    <input type="text" class="form-control TextBox2" placeholder="Type search here" ng-model="Pol"/>
                    <div>
                        <table>
                            <tr class="HeaderTr">
                                <td>#</td>
                                <td>Policy No.</td>
                                <td>Date Created</td>
                                <td>Start Date</td>
                                <td>End Date</td>
                                <td style="float:right;"></td>
                            </tr>
                            <tr ng-repeat= "p in Policies | filter:Pol" class="innerTd" ng-class-odd="'odds'">
                                <td class="Numbering">{{$index+1}}</td>
                                <td><b class="PolicyNumber">{{p.Policyno}}  <i style="color:#7bab3c">| {{p.Prefix}}</i></b></td>
                                <td class="PolicyNumber">{{p.CreatedOn | date:'yyyy-MMM-dd hh:mm a'}}</td>
                                <td class="PolicyNumber">{{p.StartDateTime | date:'yyyy-MMM-dd hh:mm a'}}</td>
                                <td class="PolicyNumber">{{p.EndDateTime | date:'yyyy-MMM-dd hh:mm a'}}</td>
                                <td class="TdButtons">
                                    <div class="btn-group">
                                         <span ng-click="GetPolicyView(p.PolicyID)" class="btn btn-sm btn-primary">View</span>
                                         <span ng-click="EditPolicy(p.PolicyID)" class="btn btn-sm btn-info">Edit</span>
                                    </div>  
                                </td>
                            </tr>

                        </table>


                    </div>
                </div>

                 <div id="Div1" ng-show="FilterOption =='Certificate'">
                    <p class="FilterTitle"><b>{{FilterOption}}</b></p>
                    <p style="text-align:center;">Search results. Records found <b>{{CertificateData.length | number}}</b></p>
                    <p style="text-align:center;" id="P1">Loading Certificates records please wait.....</p>
                    <input type="text" class="form-control TextBox2" placeholder="Type search here" ng-model="CertFilter"/>
                    <table>
                        <tr class="HeaderTr">
                                <td>#</td>
                                <td>Date Created</td>
                                <td>Cert No.</td>
                                <td>Cert Type</td>
                                <td>Vehicle Desc</td>
                                <td>Start Date</td>
                                <td>Expiry Date</td>
                                <td></td>
                                <td style="float:right;"></td>
                         </tr>
                        <tr ng-repeat = "c in CertificateData | filter:CertFilter" class="innerTd" ng-class-odd="'odds'">
                                <td class="Numbering">{{$index+1}}</td>
                                <td class="PolicyNumber">{{c.CreatedOn | date:'yyyy-MMM-dd hh:mm:ss a'}}</td>
                                <td class="PolicyNumber"><b>{{c.CovernoteNo}}</b></td>
                                <td class="PolicyNumber">{{c.certificateType}}</td>
                                <td class="PolicyNumber">{{c.VehicleDesc}}</td>
                                <td class="PolicyNumber">{{c.effectivedate | date}}</td>
                                <td class="PolicyNumber">{{c.expirydate | date}}</td>
                                <td><span class="btn btn-sm btn-info" ng-click ="PrintCert(c.GCID)">Print Cert</span></td>
                                <td><span class="btn btn-sm btn-primary" ng-click="GetPolicyView(c.PolicyID)">View Policy</span></td>
                         </tr>
                    </table>


                </div>

                 <div id="Div2" ng-show="FilterOption =='Cover Note'">
                    <p class="FilterTitle"><b>{{FilterOption}}</b></p>
                    <p style="text-align:center;">Search results. Records found <b>{{CoverNoteData.length | number}}</b></p>
                    <p style="text-align:center;" id="P2">Loading Cover Notes records please wait.....</p>
                    <input type="text" class="form-control TextBox2" placeholder="Type search here" ng-model="CnFilter"/>
                     <table>
                        <tr class="HeaderTr">
                                <td>#</td>
                                <td>Date Created</td>
                                <td>Cover Note No.</td>
                                <td>Vehicle Desc</td>
                                <td>Start Date</td>
                                <td>Expiry Date</td>
                                <td></td>
                                <td style="float:right;"></td>
                         </tr>
                        <tr ng-repeat = "c in CoverNoteData | filter:CnFilter" class="innerTd" ng-class-odd="'odds'">
                                <td class="Numbering">{{$index+1}}</td>
                                <td class="PolicyNumber">{{c.CreatedOn | date:'yyyy-MMM-dd hh:mm:ss a'}}</td>
                                <td class="PolicyNumber"><b>{{c.CovernoteNo}}</b></td>
                                <td class="PolicyNumber">{{c.VehicleDesc}}</td>
                                <td class="PolicyNumber">{{c.effectivedate | date}}</td>
                                <td class="PolicyNumber">{{c.expirydate | date}}</td>
                                <td><span class="btn btn-sm btn-info" ng-click="PrintCoverNote(c.ID)">Print Cover</span></td>
                                <td><span class="btn btn-sm btn-primary" ng-click="GetPolicyView(c.PolicyID)">View Policy</span></td>
                         </tr>
                    </table>
                </div>

                 <div id="Div3" ng-show="FilterOption =='Risk/Vehicle'">
                    <p class="FilterTitle"><b>{{FilterOption}}</b></p>
                </div>

                 <div id="Div4" ng-show="FilterOption =='Insured'">
                    <p class="FilterTitle"><b>{{FilterOption}}</b></p>
                </div>
       
       </div>

       </div>


    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">

<style type="text/css">
    table {border:none!important; width:100%!important; }
    td{  border:none!important;padding:0,5px,0,5px!important;}
    
    .TextBox{width:85%!important; margin-bottom:0!important;}
    
      .TextBox2{width:98%!important;}
    
    .FilterTitle
    {
        text-align:center;
        color:Orange;
    }
    
    .HeaderTr
    {
        border:solid;
        border-width:thin;
        padding:5px,1px,5px,1px;
        border-color:#e0d5d5;
        background:#bdbab7;
    }
    
    .TdButtons
    {
        float:right;
        padding-right:5px;
    }
    
    
    .innerTd
    {
        border-bottom:solid;
        border-bottom-width:thin;
        border-bottom-color:#e0d5d5;
    }
    
    
    .innerTd:hover
    {
        background:lightgreen;
    }
    
    .PolicyNumber{font-size:12px;}
    
    .Numbering{color:#ea5e0b;font-weight:bold;}
    
    .odds{background:#e8e8e8;}
    
    #LoadingMessage{color:#ea5e0b;}

</style>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.js"></script>
  <script src="../../Scripts/ApplicationJS/CompanyDataPolicies.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
