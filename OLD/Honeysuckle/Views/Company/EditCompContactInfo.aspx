﻿<%@ Page Title="EditContacts" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit <%: Model.CompanyName %> Company Contact Information: 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm()) { %>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            <div id= "form-header"><img src="../../Content/more_images/Contact.png" alt=""/>
                <div class="form-text-links">
                     <span class="form-header-text">Edit Company Contacts</span>
                </div>
             </div> 
            <div class="field-data">
                <div id="contact-info">
                    <%: Html.HiddenFor(model => model.CompanyID, new { @class = "dont-process" })%>

                    <div class="emails">
                        <div class="email-container">
                         <div class="subheader"><img src="../../Content/more_images/User.png" alt=""/><span>Email Info</span></div>
                           <div class="addemail">
                             <div id="addNewEmail" class="add-dynamic small">
                                 <img src="../../Content/more_images/ivis_add_icon.png" />
                                 <span>Add Email</span>
                              </div>
                           </div>
                            <% if (Model.emails != null && Model.emails.Count != 0) { %>
                                <% foreach (Honeysuckle.Models.Email email in Model.emails) { %>
                                    <% Html.RenderPartial("~/Views/Email/NewEmail.ascx", email); %>
                                <% } %>
                            <% } %>
                        </div>
                    </div>


                    <!-- Phone numbers -->
                    <div class="phones">
                        <div class="phone-container">
                         <div class="subheader"><img src="../../Content/more_images/User.png" alt=""/><span>Phone Info</span></div>
                          <div class="addphone">
                            <div id="addphone" class="add-dynamic small">
                                <img src="../../Content/more_images/ivis_add_icon.png" />
                                <span>Add Phone</span>
                              </div>
                            </div>
                            <% if (Model.phoneNums != null && Model.phoneNums.Count != 0) { %>
                                <% foreach (Honeysuckle.Models.Phone phone in Model.phoneNums) { %>
                                   <div class="phone"><div class="subheader"><span>Phone Number</span><input type="button" value="Remove" class="remove btnclr rmphone"/></div>
                                         <% Html.RenderAction("EditPhone", "Phone", new { phone = phone}); %> 
                                    </div>
                                <% } %>
                            <% } %>
                      </div> 
                  </div>
                <div class="btnlinks"> 
                    <% Html.RenderAction("BackBtn", "Back"); %>
                    <input type="submit" value="Done" id="Submit2" />
                </div>
            </div>
         </div>
        </fieldset>
    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
  <script src="../../Scripts/ApplicationJS/multiple-emails.js" type="text/javascript"></script>
  <script src="../../Scripts/ApplicationJS/multiplePhoneNums.js" type="text/javascript"></script>
  <link href="../../Content/RegularCompContact.css" rel="stylesheet" type="text/css" />
</asp:Content>

