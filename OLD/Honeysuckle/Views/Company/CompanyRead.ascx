﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Company>" %>

    <% using (Html.BeginCollectionItem("company")) { %>
        <%: Html.ValidationSummary(true)%>
        <% if(Model.CompanyID != 0) { %>

            <%: Html.HiddenFor(model => model.CompanyID, new { id = "companyid", @class="dont-process" })%>
            <%: Html.HiddenFor(model => model.IAJID, new { id = "companyid", @class = "dont-process" })%>

            <div class="editor-label">
                Company Name:
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.CompanyName, new { id = "companyname", @readonly = "readonly", @class = "read-only" })%>
                <%: Html.ValidationMessageFor(model => model.CompanyName)%>
            </div>
        <% } %>
    <% } %>