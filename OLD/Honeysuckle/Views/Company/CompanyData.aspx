﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	CompanyData
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <div id="company-data-content">
     <div id="form-header">
            <img src="../../Content/more_images/Company.png"/>
            <div class="form-text-links">
                <span class="form-header-text">
                <% if(ViewData["CompanyName"] != null) {%>
                    <%: ViewData["CompanyName"].ToString() %> Data
                    <% } else { %> Company Data <% } %>
                </span>
            </div>
     </div>
     <div id="add-items">
        <div class="date-pickers-block">
            <span id = "filter-data"><strong>Filter</strong> Type: </span>
           
            <select id="import-dropdown">
                <option value = "all">All</option>
                <option value = "imported">Imported</option>
                <option value = "ximported">Not Imported</option>
            </select>
            <div id="dates">
                    Any Date <%: Html.RadioButton("filter", "all", true, new { @id = "all-data"})%> Date Range <%: Html.RadioButton("filter", "date-range", new { @id = "date-range" })%> </span>
                <%: Html.TextBox("data_created_start", DateTime.Now.AddMonths(-1), new { @class = "datetime datepicker created_data start", @readonly = "readonly", @Value = DateTime.Now.AddMonths(-1).ToString("d MMM, yyyy HH:mm tt") })%> <!-- yyyy MM dd -->
                <strong>-</strong>
                <%: Html.TextBox("data_created_start_end", DateTime.Now, new { @class = "datetime datepicker created_data end", @readonly = "readonly", @Value = DateTime.Now.ToString("d MMM, yyyy HH:mm tt") })%>
              <select id="createdORmodified">
                <option value = "created">Created Date</option>
                <option value = "modified">Modified Date</option>
                <option value = "both">Both</option>
            </select>
                <button class="update-data" onclick="">Update</button>
            </div>

        </div>
      </div>
     <div id="all-data-content">
         <div id="policies" class="data-result">
                <div class="data-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Policy.png" /><span class="category">Policies:</span>
                     <span class="count"></span>
                </div>
                <div id="data-results-policies" class="data-content"></div>
          </div>
        <div id="covernotes" class="data-result">
            <div class="data-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Cover_Note.png" /><span class="category">Cover Notes:</span><span class="count"></span></div>
            <div id="data-results-covernotes" class="data-content"></div>
         </div>
        <div id="certificates" class="data-result">
            <div class="data-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Certificate.png" /><span class="category">Certificates:</span><span class="count"></span></div>
            <div id="data-results-certificates" class="data-content"></div>
        </div>
        <div id="vehicles" class="data-result">
            <div class="data-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Vehicle.png" /><span class="category">Vehicles:</span><span class="count"></span></div>
            <div id="data-results-vehicles" class="data-content"></div>
        </div>
      <div id="companies" class="data-result">
            <div class="data-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/Company.png" /><span class="category">Companies:</span><span class="count"></span></div>
            <div id="data-results-companies" class="data-content"></div>
      </div>
      <div id="people" class="data-result">
            <div class="data-headers subheader arrow-click"><div class="arrow right"></div><img class="normal" src="../../Content/more_images/User.png" /><span class="category">People:</span><span class="count"></span></div>
            <div id="data-results-people" class="data-content"></div>
     </div>
    </div>
 </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
 <link href="../../Content/advanced-search.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/company_data.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.js"></script>
    <link href="../../Scripts/datetimepicker-master/jquery.datetimepicker.css" rel="stylesheet"
        type="text/css" />
    <script src="../../Scripts/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript"></script>
    <link href="../../Content/company_data.css" rel="stylesheet" type="text/css" />
 <%-- %>script src="../../Scripts/ApplicationJS/advanced-search.js" type="text/javascript"></script>--%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
