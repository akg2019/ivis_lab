﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Company>" %>

<script runat="server">

    protected void Button1_Click(object sender, EventArgs e)
    {

    }
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Company Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <form id="form1" runat="server">

   <div id= "form-header"><img src="../../Content/more_images/Company.png" alt=""/>
        <div class="form-text-links">
            <span class="form-header-text">Create Company</span>
            <div class="icon">
                <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
            </div>
       </div>
   </div>
   <div class="field-data">

    <% using (Html.BeginForm()) { %>
        <%: Html.ValidationSummary(true) %>

        <fieldset>

            <%    
                bool isAdmin = Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user);
                bool isIAJAdmin = Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user);
                bool isAdministartive = Honeysuckle.Models.UserGroupModel.IsAdministrative(Honeysuckle.Models.Constants.user);
                int myComp = Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user).CompanyID;
            %>

            <div class="validate-section">
                <div class="subheader"> 
                    <div class="arrow right down"></div>
                    Main Company Information
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div class="editor-label">
                        TRN
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.trn, new { id = "trn", @class = "company-trn",  @required = "required", @PlaceHolder= " Eg. 088776655"})%><br />
                        <%: Html.ValidationMessageFor(model => model.trn)%>
                    </div>

                    <div class="editor-label">
                        IAJID
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.IAJID, new { id = "IAJID", @required = "required", @PlaceHolder = " Eg. 723476655645", @class="IAJID-create" })%><br />
                        <%: Html.ValidationMessageFor(model => model.IAJID) %>
                    </div>

                    <div class="editor-label">
                        Company Name
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.CompanyName, new { id = "CompanyName", @required = "required" })%><br />
                        <%: Html.ValidationMessageFor(model => model.CompanyName) %>
                    </div>
            
                    <% if (isAdmin || isIAJAdmin) 
                        { %>
                        <div class="editor-label">
                            Company Type
                        </div>
                        <div class="editor-field">
                            <%: Html.DropDownListFor(m=> m.CompanyType, new SelectList(new[] {" ", "Insurer", "Broker", "Agent", "Association"}), new { @class = "type", @required = "required"} )%>
                        </div>
                    <% } %>

                    <% if ((Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Enable")))) 
                        { %>
                        <div class="editor-label">
                            Status: 
                        </div>
                        <div class="editor-field">
                            <%: Html.DropDownList("enable", new SelectList(new List<Object>(){new {value = "enabled", text = "Enabled"}, new{value = "disabled", text = "Disabled"}}, "value", "text"), new { id = "enable" }) %>
                        </div>
                    <% } %>
                </div>
            </div>

           
                          
                <div class="validate-section">
                    <% if (isIAJAdmin || isAdmin) { %> 
                        <div class="subheader"> 
                            <div class="arrow right"></div>
                            Shortenings 
                            <div class="valid"></div>
                        </div>
                        <div class="data">
                            <div class="editor-label  compCode">
                                Insurer Code
                            </div>
                            <div class = "editor-field">
                                <%: Html.TextBoxFor(model => model.insurercode, new { id = "compInsCode", @Class= "compCode", @required = "required" })%>
                            </div>
                            <div class="editor-label">
                                Company Shortening Code
                            </div>
                            <div class = "editor-field">
                                <%: Html.TextBoxFor(model => model.compshort, new { id = "ComShortening" })%>
                            </div>
                        </div>
                            
                    <% } %>
                    <% else
                        { %>
                            <%: Html.HiddenFor(model => model.insurercode, new { id = "compInsCode", @class = "dont-process" })%> 
                            <%: Html.HiddenFor(model => model.compshort, new { id = "ComShortening", @class = "dont-process" })%> 
                    <% } %>
                </div>

                <div class="validate-section">
                <% if (isIAJAdmin || isAdministartive)
                   {%>
                    <div class="subheader"> 
                        <div class="arrow right"></div>
                        Admin Settings 
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="editor-label">
                            Password Retries
                        </div>
                        <div class="editor-field">
                            <%: Html.TextBoxFor(model => model.floginlimit, new { @class = "floginlimit", @required = "required", @Value = 0 })%>
                            <%: Html.ValidationMessageFor(model => model.floginlimit)%>
                        </div>
                        <div class="check-div">
                            <div class="editor-label autoCanel">
                                Auto Cancel Policy Risk
                            </div>
                            <div class="editor-field">
                                <%: Html.CheckBoxFor(model => model.autoCancelVehicle, new { @id = "auto-cancel-vehicle", @Class = "autoCanel" })%>
                            </div>
                        </div>
                    </div>
                    <% } %>
                </div>
                <br />

            
                   
            <div class="validate-section">
                <div class="subheader"> 
                    <div class="arrow right"></div>
                    Company Address 
                    <div class="valid"></div>
                </div>
                <div class="data">
                    <div class="editor-label">
                        Road 
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.CompanyAddress.roadnumber, new { id = "RoadNumber", @PlaceHolder = "12" })%>
                        <%: Html.TextBoxFor(model => model.CompanyAddress.road.RoadName, new { id = "roadname", @PlaceHolder = " Short Hill" })%>
                        <%: Html.TextBoxFor(model => model.CompanyAddress.roadtype.RoadTypeName, new { id = "roadtype", @PlaceHolder = " Avenue" })%>
                
                        <%: Html.ValidationMessageFor(model => model.CompanyAddress.roadnumber)%>
                        <%: Html.ValidationMessageFor(model => model.CompanyAddress.road.RoadName)%>
                        <%: Html.ValidationMessageFor(model => model.CompanyAddress.roadtype.RoadTypeName)%>
                    </div>

                    <div class="editor-label">
                        Building/Apartment No. 
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.CompanyAddress.ApartmentNumber, new { id = "aptNumber" })%>
                        <%: Html.ValidationMessageFor(model => model.CompanyAddress.ApartmentNumber)%>
                    </div>

                    <div class="editor-label">
                        City/Town
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.CompanyAddress.city.CityName, new { id = "city", @required = "required", @class = "city" })%>
                        <%: Html.ValidationMessageFor(model => model.CompanyAddress.city.CityName)%>
                    </div>

                    <div class="editor-label">
                        Parish
                    </div>
                    <div class="editor-field">
                        <%: Html.HiddenFor(model => model.CompanyAddress.parish.ID, new { id = "parish", @class="dont-process" })%>
                        <%  Html.RenderAction("ParishList", "Parish"); %>
                        <%: Html.ValidationMessage("parish") %>
                    </div>

                    <div class="editor-label">
                        Country Name
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.CompanyAddress.country.CountryName, new { id = "country", @required = "required", @Value = "Jamaica", @class= "country"}) %>
                        <%: Html.ValidationMessageFor(model => model.CompanyAddress.country.CountryName) %>
                    </div>
                </div>
            </div>
            
            <div class="btnlinks">
                <% Html.RenderAction("BackBtn", "Back"); %>
                <%--<input type="submit" value="Create" id="Submit2" />--%>
                <asp:Button ID="Submit2" runat="server" OnClientClick="this.disabled='true'; this.value='Please wait...'" UseSubmitBehavior="false" Text="Create" />
            </div>
        </fieldset>

      <% } %>

       

    </div>

    </form>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <script src="../../Scripts/ApplicationJS/companyInsuredCodeSet.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/addressManagement.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/CompCreateIAJIDTest.js" type="text/javascript"></script>
    <link href="../../Content/AdressStyle.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/parishUpdate.js" type="text/javascript"></script>
</asp:Content>
