﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Company>" %>


<h4>Company Contact Information: </h4>

<% using (Html.BeginCollectionItem("company")) { %>
    <%: Html.ValidationSummary(true) %>

    <div class="field-data1">
        <div id="contact-info">
            <%: Html.HiddenFor(model => model.base_id, new { id = "compBaseID", @class = "dont-process"})%>
            <%: Html.HiddenFor(model => model.CompanyID, new { id = "compID", @class = "dont-process" })%>

            <div class="emails">
                <div id="email-container">
                    <div class="subheader">
                        <img src="../../Content/more_images/User.png" alt=""/>
                        <span>
                            Email Info
                        </span>
                    </div>
                    <div class="addemail">
                        <div id="addNewEmail" class="add-dynamic small">
                            <img src="../../Content/more_images/ivis_add_icon.png" />
                            <span>
                                Add Email
                            </span>
                        </div>
                    </div>
                    <% if (Model.emails != null) { %>
                        <% foreach (Honeysuckle.Models.Email email in Model.emails) {  %>
                            <% Html.RenderPartial("~/Views/Email/NewEmail.ascx", email); %>
                        <% } %>
                    <% } %>
                </div>
            </div>

            <!-- Phone numbers -->
            <div class="phones">
                <div id="phone-container">
                    <div class="subheader">
                        <img src="../../Content/more_images/User.png" alt=""/>
                        <span>
                            Phone Info
                        </span>
                    </div>
                    <div class="addphone">
                        <div id="addphone" class="add-dynamic small">
                            <img src="../../Content/more_images/ivis_add_icon.png" />
                            <span>
                                Add Phone
                            </span>
                        </div>
                    </div>
                    <% if (Model.phoneNums != null) { %>
                        <% foreach (Honeysuckle.Models.Phone phone in Model.phoneNums) { %>
                            <% Html.RenderPartial("~/Views/Phone/EditPhone.ascx", phone); %>
                        <% } %>
                    <% } %>
                </div>
            </div>
        </div>
    </div>

    <link href="../../Content/RegularCompContact.css" rel="stylesheet" type="text/css" />

<% } %>




