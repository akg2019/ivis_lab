﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	View <%: Model.CompanyName %> Company Contact Information: 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <% using (Html.BeginForm()) { %>
     <%: Html.ValidationSummary(true) %>

      <fieldset>
        <div id= "form-header">  
            <img src="../../Content/more_images/Contact.png" alt=""/>
            <div class="form-text-links">
                     <span class="form-header-text">Company Contact Information </span>
             </div> 
             <ul id="menu-links" class="regularComp">
                   <li>
                        <span class="menu-links-heading">Company</span>
                        <ul>
                            <li>
                                <%: Html.ActionLink("Company Details", "Details", new { id = Model.CompanyID })%>
                            </li>
                           <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company","Update")))
                                { %> 
                                 <li>
                                    <%: Html.ActionLink("Edit", "EditCompContactInfo", new { id = Model.CompanyID })%> 
                                 </li>
                             <% } %>
                        </ul>
                    </li>
                </ul>
            </div>
           <div class="field-data">
              <div id="contact-info">

        <%: Html.HiddenFor(model => model.CompanyID, new { @class = "dont-process" })%>

        <!-- Company emails -->
            <div class="emails">
              <div class="email-container">
                <div class="subheader"><img src="../../Content/more_images/User.png" alt=""/><span>Email Info</span></div>
                 <% if (Model.emails != null && Model.emails.Count != 0) { %>
                        <% foreach (Honeysuckle.Models.Email email in Model.emails) { %>
                            <% Html.RenderPartial("~/Views/Email/NewEmail.ascx", email); %>
                        <% } %>
                 <% } %>
                 <% else 
                    { %> 
                       <div class="addemail">
                          No Emails
                       </div>
                <% } %>
               </div>
            </div>

          <!-- Company phone numbers -->
                    <div class="phones">
                        <div class="phone-container">
                           <div class="subheader"><img src="../../Content/more_images/User.png" alt=""/><span>Phone Info</span></div>
                            <% if (Model.phoneNums != null && Model.phoneNums.Count != 0) { %>
                                <% foreach (Honeysuckle.Models.Phone phone in Model.phoneNums) { %>
                                     <div class="phone"><div class="subheader"><span>Phone Number</span></div>
                                      <% Html.RenderAction("EditPhone", "Phone", new { phone = phone, type="View" }); %>
                                     </div>
                                <% } %>
                           <% } %>
                           <% else 
                             { %> 
                                 <div class="addemail">
                                 No Phone Numbers
                                 </div>
                          <% } %>
                        </div>
                    </div> 

                 </div> 
               <div class="btnlinks">
                   <% Html.RenderAction("BackBtn", "Back"); %>
              </div>
            </div> 

        
        </fieldset>
    
     <% } %>

       

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
  <script src="../../Scripts/ApplicationJS/Hidedata.js" type="text/javascript"></script>
  <link href="../../Content/RegularCompContact.css" rel="stylesheet" type="text/css" />
</asp:Content>

 
