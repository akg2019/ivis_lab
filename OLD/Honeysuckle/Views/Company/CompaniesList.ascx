﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Company>>" %>

<%
    bool company_update = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Update"));
    bool company_delete = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Delete"));
    bool company_enable = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Company", "Enable"));
    if (Honeysuckle.Models.Constants.user.employee == null) Honeysuckle.Models.Constants.user.employee = new Honeysuckle.Models.Employee();
    Honeysuckle.Models.Constants.user.employee.company = Honeysuckle.Models.CompanyModel.GetMyCompany(Honeysuckle.Models.Constants.user);
    bool am_administrator =  Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user);
    bool am_iaj_administrator = Honeysuckle.Models.UserGroupModel.AmAnIAJAdministrator(Honeysuckle.Models.Constants.user);
%>




<div class="container big">

    <div class="stripes"></div>
    <div class="dashboard-pad">
        <div class="form-header"><img src="../../Content/more_images/Company.png" />
            <div class="form-text-links">
                <span class="form-header-text">Quick List</span>
            </div>
        </div>
        <div class="ivis-table">
            <div class="company menu">
                    <div class="name item">Name</div>
                    <div class="short item">Code</div>
                    <div class="type item">Type</div>
                    <div class="contact item">Contact Name</div>
                    <div class="email item">Email</div>
                    <div class="phone item">Phone</div>
            </div>
            <!-- <div id="companies" class="table-data"> -->
    
            <% int i = 0; %>
            <% foreach(var item in Model){ %>
                <% i++; %>
                <% if (i == 1) { %>
                    <div class="company data row mine">
                <% } %>
                <% else if (i % 2 == 1) { %>
                    <% if (i == 2) { %> 
                        <div id="companies" class="table-data">
                    <% } %>
                    <% if (item.Enabled) { %>
                        <div class="company data odd row">
                    <% } %>
                    <% else { %>
                        <div class="company data odd row disabled">
                    <% } %>
                <% } %>
                <% else { %>
                    <% if (i == 2) { %> 
                        <div id="companies" class="table-data">
                    <% } %>
                    <% if (item.Enabled) { %>
                        <div class="company data even row">
                    <% } %>
                    <% else { %>
                        <div class="company data even row disabled">
                    <% } %>
                <% } %>
                        <%: Html.Hidden("ID", item.CompanyID, new { @class = "id dont-process" })%>
                        <div class="name item thisCompName"><%: item.CompanyName%></div>
                        <div class="short item"><%: item.compshort %></div>
                        <div class="type item"><%: item.CompanyType %></div>
                        <div class="contact item">
                            <% if (item.primary_phone != null){ %>
                                <%: item.primary_phone.name %>
                            <% } %>
                        </div>
                        <div class="email item">
                            <% if (item.primary_email != null){ %>
                                <%: item.primary_email.email %>
                            <%} %>
                        </div>
                        <div class="phone item">
                            <% if (item.primary_phone != null){ %>
                                <%: item.primary_phone.PhoneNumber %>
                            <% } %>
                        </div>
                        <div class="functions item">
                            <% if (company_update && Honeysuckle.Models.Constants.user.employee.company.CompanyID == item.CompanyID || am_administrator || am_iaj_administrator)
                                { %>
                                <div class="icon edit" title="Edit Company">
                                    <img src="../../Content/more_images/edit_icon.png" class="sprite" />
                                </div>
                            <% } %>
                            <% if (!item.has_data && company_delete) 
                                { %> 
                                <div class="icon delete" title="Delete Company">
                                    <img src="../../Content/more_images/delete_icon.png" class="sprite" />
                                </div> 
                            <% } %>
                            <% if (company_enable) { %>
                                <% if (item.Enabled)
                                    { %>
                                        <div class="icon disable" title="Deactivate Company">
                                            <img src="../../Content/more_images/disable_icon.png" class="sprite" />
                                        </div> 
                                <% } %>
                                <% else
                                    { %>
                                        <div class="icon enable" title="Activate Company">
                                            <img src="../../Content/more_images/enable_icon.png" class="sprite" />
                                        </div> 
                                <% } %>
                            <% } %>
                   
                        </div>

                    </div>
                <% } %>
            </div>
        </div>
    </div>
    </div>
    <div id="legend"><span class="companykey mycomp">My Company</span> <div class="key myCompany"></div><span class="companykey disabled">Disabled Companies</span><div class="key disabledCompany"></div></div>

