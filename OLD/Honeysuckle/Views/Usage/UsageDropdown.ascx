﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Usage>>" %>

<%: Html.DropDownList("Usage", new SelectList(Model.ToList(), "ID", "Usage"), new { @class = "usage_dropdown", id = "usageMenu", @required = "required"})%>