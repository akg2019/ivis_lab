﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.Usage>" %>

<%
    bool usage_delete = Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("Usages", "Delete"));
%>
        
<%: Html.HiddenFor(model => model.ID, new { @class = "usage-id dont-process" })%>
<%: Html.HiddenFor(model => model.UCID, new { @class = "certUCID dont-process" })%>
 
<div class="item usages">
    <%: Model.usage %> <span> (<%: Model.CertCode %>)</span> <span hidden = "hidden" class ="ucid-field"> <%: Model.UCID %></span>
</div>

<% if(!(bool)ViewData["Details"]){ %>
    <div class="item usageStat">
        <% if (Model.ID != 0) { %>
            <% if (usage_delete && !Model.in_use) { %> 
                <div class="icon delete" title="Delete Usage"> 
                    <img src="../../Content/more_images/delete_icon.png" class="sprite" />
                </div> 
                <div class="icon edit" title="Edit Usage"> 
                    <img src="../../Content/more_images/edit_icon.png" class="sprite" />
                </div> 
            <% } %>
            <% if (Model.in_use) { %> 
                <div class="icon activeunderpol" title="Currently Active Under Policy"> 
                    <img src="../../Content/more_images/Enable_01.png" class="" /> 
                </div> 
            <% } %>
        <% } %>
        <% else { %>
            <div class="icon delete" title="Delete Usage"> 
                <img src="../../Content/more_images/delete_icon.png" class="sprite" />
            </div> 
        <% } %>
    </div>
<% } %>	


