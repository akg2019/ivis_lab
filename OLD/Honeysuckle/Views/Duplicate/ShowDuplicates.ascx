﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.Duplicate>>" %>

<div class="all-duplicates">
    <div class="prev">
        <
    </div>
    <div class="view-window-div">
        <div class="sliding-div">
            <% 
                foreach(var duplicate in Model)
                { 
            %>
                <div class="duplicate">
                    <% 
                        switch (duplicate.type)
                        {
                            case "policy":
                                Html.RenderAction("AuditPolicy", "Policy", new { id = ((Honeysuckle.Models.Policy)duplicate.ref_obj).ID });
                                break;
                            default:
                                break;
                        }          
                    %>
                </div>
            <% 
                } 
            %>
        </div>
    </div>
    <div class="next">
        >
    </div>
</div>