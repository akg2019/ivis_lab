﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.User>>" %>

    <div id="user-search">
        <div class="menu row">
            <div class="item"></div>
            <div class="item">
                username
            </div>
        </div>

    <% foreach (var item in Model) { %>
    
        <div class="row">
            <div class="item">
                <%: Html.ActionLink("Edit", "Edit", new { /* id=item.PrimaryKey */ }) %> |
                <%: Html.ActionLink("Details", "Details", new { /* id=item.PrimaryKey */ })%> |
                <%: Html.ActionLink("Delete", "Delete", new { /* id=item.PrimaryKey */ })%>
            </div>
            <div class="item">
                <%: item.username %>
            </div>
        </div>
    
    <% } %>

    </div>
<!--
    <p>
        <%//: Html.ActionLink("Create New", "Create") %>
    </p>
-->

