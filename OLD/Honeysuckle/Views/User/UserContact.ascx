﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Honeysuckle.Models.User>" %>

<% using (Html.BeginCollectionItem("user")) {%>
    <%: Html.ValidationSummary(true) %>
    <div class="items">
        <div class="subheader"><img src="../../Content/more_images/User.png" alt=""/><span>User Contact</span><input type="button" value="Remove" id= "Rem" class="remove btnclr"/></div>
        <%// Html.RenderAction("UserInfo", "User", new { username = Model.username }); %>

        <%: Html.HiddenFor(model => model.ID, new { @class = "userId dont-process" })%>
        <%: Html.HiddenFor(model => Model.employee.person.PersonID,new { @class = "personID dont-process" })%>
        <div class= "row-field"> 
            <span>Username:</span><%:Html.TextBoxFor(model => model.username, new { @readonly = "readonly", id = "usernameEmail", @class = "read-only" })%>  
        </div>
        <div class= "row-field">  
            <span>Name:</span><%: Html.TextBox("peoplecontact", Model.employee.person.fname + " " + Model.employee.person.lname, new { @readonly = "readonly", id = "name", @class = "read-only" })%>
        </div>

        <% foreach (var email in Model.employee.person.emails)
           {%>
            <% if (email.email != Model.username && email.primary)
               {%>
                <div class= "row-field">  
                    <span>Primary Email:</span><%: Html.TextBox("primary-email", email.email, new { @readonly = "readonly", id = "primary-email", @class = "read-only" })%>
                </div>
        <% } %>
        <%} %>

        <% if (Model.employee.person.phoneNums != null && Model.employee.person.phoneNums.Count() != 0)
           {%>
                <% if (Model.employee.person.phoneNums.Count() > 1)
                   { %>
                    <% foreach (var phone in Model.employee.person.phoneNums)
                           if (phone.primary)%>
                                <% Html.RenderPartial("~/Views/Phone/NewPhone.ascx", phone, new ViewDataDictionary { { "EditCompContact", true } }); %>
                  <% } 
                       else %>
                            <%Html.RenderPartial("~/Views/Phone/NewPhone.ascx", Model.employee.person.phoneNums.ElementAt(0), new ViewDataDictionary { { "EditCompContact", true } }); %>
          <%}
          else %>
            <% Html.RenderAction("NewPhone", "Phone"); %>
        <br />
    </div> 
    <br />
<% } %>

