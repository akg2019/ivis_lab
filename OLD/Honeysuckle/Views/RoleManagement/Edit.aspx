﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.UserGroup>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<%
    bool is_admin = Honeysuckle.Models.UserGroupModel.AmAnAdministrator(Honeysuckle.Models.Constants.user);    
%>
    <div id="emptylist">
        <% if (int.Parse(ViewData["fillpermissions"].ToString()) == 1) { %>
            Please add permissions to your group.
        <%} %>
    </div>

    <%: Html.Hidden("page", "edit", new { @class = "dont-process" }) %>
    
    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            <div id="form-header"><img src="../../Content/more_images/User.png" />
                  <div class="form-text-links">
                    <span class="form-header-text">Edit <%: Model.UserGroupName  %> Permissions</span>
                    <div class="icon">
                        <img src="../../Content/more_images/open-sections.png" title="Open All Sub-Sections" class="open-sections close sprite" />
                    </div>
                </div>
            </div>
            <div class="field-data">

                <div class="validate-section">
                    <%: Html.HiddenFor(model => model.UserGroupID, new { @class = "hidid dont-process" })%>
                    <div class="subheader">
                        <div class="arrow right down"></div>
                        Main User Group Data
                        <div class="valid"></div>
                    </div>
                    <div class="data">
                        <div class="editor-label userGroupName">
                            User Group Name 
                        </div>
                        <div class="editor-field">
                            <%: Html.HiddenFor(model => model.UserGroupID, new { @class = "dont-process" })%>
                            <% if (Model.stock || Model.system) { %> <%: Html.TextBoxFor(model => model.UserGroupName, new { @class = "read-only user-group-name", @readonly = "readonly" }) %> <% } %>
                            <% else { %> <%: Html.TextBoxFor(model => model.UserGroupName, new { @class = "user-group-name read-only", @readonly = "readonly" }) %> <% } %>
                            <%//: Html.ValidationMessageFor(model => model.UserGroupName)%>
                        </div>
                        <div class="check-area">
                        
                            <% if(!Model.SuperGroup) { %>
                                <% if (is_admin) { %>
                        
                                    <div class="stockcheck check">
                                        <div class="editor-label">
                                            Stock Group?:
                                        </div>
                                        <div class="editor-field">
                                            <%: Html.CheckBoxFor(model => model.stock, new { id = "stockcheckbox", @OnChange = "ToggleStock()", disabled = "disabled" })%>
                                        </div>
                                    </div>
                                    <div class="systemcheck check" title="This field allows you create a group which which will be a system group that functions across companies and only a System Administrator (you) can assign and remove from the user.. You cannot have a group be a system group and a super group.">
                                        <div class="editor-label">
                                            System Group?:
                                        </div>
                                        <div class="editor-field">
                                            <%: Html.CheckBoxFor(model => model.system, new { id = "systemcheckbox", @OnChange = "ToggleStockGroups()", disabled = "disabled" })%>
                                        </div>
                                    </div>
                                    <div class="admincheck check" title="This field allows this group to view business intelligence data.">
                                        <div class="editor-label help">
                                            Administrative Group ?:
                                        </div>
                                        <div class="editor-field">
                                            <%: Html.CheckBoxFor(model => model.administrative, new { id = "admincheckbox", disabled = "disabled" })%>
                                        </div>
                                    </div>
                            
                            
                                <% } %>
                            <% } %>
                        </div>
                        <% if (is_admin) {  %>
                           <div class="company">
                                <div class="editor-label">
                                    Company Group is Assigned To:
                                </div>
                                <div class="editor-field">
                                    <%: Html.TextBoxFor(model => model.company.CompanyName, new { @class = "read-only ignore", @readonly = "readonly", disabled = "disabled" })%>
                                </div>
                            </div>
                        <% } %>
                    </div>
                </div>

                <div class="validate-section permission-header">
                    <div class="subheader">
                    <div class="arrow right"></div>
                        Group Permissions
                        <div class="valid"></div>
                        <div class="server-responses"></div>
                    </div>
                    <div class="data">
                        <div class="supergroupcheck  check">
                            <div class="editor-label">
                                Super Group?:
                            </div>
                            <div class="editor-field">
                                <%: Html.HiddenFor(model => model.SuperGroup, new { @class = "dont-process" })%>
                                <%: Html.CheckBoxFor(model => model.SuperGroup, new {id = "supergroupcheckbox", @OnChange = "ToggleSuperGroups()", disabled = "disabled"})%>
                            </div>
                        </div>
                        <div class="permissions">
                            <div class="all-user-permission">
                                <div class="heading">Permissions Not Selected</div>
                                <div class="search-section">
                                    <input id="userpermissions-not" placeholder="Search [Permission]" class="searching quick-search rolemanagement-edit userpermissions-not" autocomplete="off" />
                                    <button type="button" class="quick-search-btn" onclick="clickSearchBtn(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
                                </div>
                                <% Html.RenderAction("ListBoxUserPermissions", "UserPermission", new { id = Model.UserGroupID, system = Model.system }); %>
                            </div>
                            <div class="move-buttons">
                                <button type="button" class="add-button perm move btnclr" onclick="addPermission()" >>></button>
                                <button type="button" class="rm-button perm move btnclr" onclick="rmPermission()" ><<</button>
                                <!--
                                <input type="submit" value=">>" name="submit" class="add-button btnclr" />
                                <input type="submit" value="<<" name="submit" class="rm-button btnclr" />
                                -->
                            </div>
                            <div class="my-user-permission">
                                <div class="heading">Permissions Selected</div>
                                <div class="search-section">
                                    <input id="permissions-is" placeholder="Search [Permission]" class="searching quick-search rolemanagement-edit userpermissions-is" autocomplete="off" />
                                    <button type="button" class="quick-search-btn" onclick="clickSearchBtn(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
                                </div>
                                <% Html.RenderAction("MyListBoxUserPermissions", "UserPermission", new { id = Model.UserGroupID, system = Model.system }); %>
                            </div>
                        </div>

                        <div class="groups hide">
                            <div class="all-user-permission">
                                <div class="heading">Child Groups Not Selected</div>
                                <div class="search-section">
                                    <input id="childgroups-not" placeholder="Search [Group]" class="searching quick-search rolemanagement-edit childgroups-not" autocomplete="off" />
                                    <button type="button" class="quick-search-btn" onclick="clickSearchBtn(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
                                </div>
                                <% Html.RenderAction("SuperGroupListBoxUserGroups", "UserGroup", new { id = Model.UserGroupID, comp = Model.company.CompanyID }); %>
                            </div>
                            <div class="move-buttons">
                                <button type="button" class="add-button super move btnclr" onclick="addGroup()" >>></button>
                                <button type="button" class="rm-button super move btnclr" onclick="rmGroup()" ><<</button>
                                <!--
                                <input type="submit" value=">>" name="submit" class="add-button btnclr" /> 
                                <input type="submit" value="<<" name="submit" class="rm-button btnclr" />
                                -->
                            </div>
                            <div class="my-user-permission">
                                <div class="heading">Child Groups Selected</div>
                                <div class="search-section">
                                    <input id="childgroups-is" placeholder="Search [Group]" class="searching quick-search rolemanagement-edit childgroups-is" autocomplete="off" />
                                    <button type="button" class="quick-search-btn" onclick="clickSearchBtn(this)" ><img src="../../Content/more_images/Search_Icon.png" /></button>
                                </div>
                                <% Html.RenderAction("SuperGroupMyListBoxUserGroups", "UserGroup", new { id = Model.UserGroupID, comp = Model.company.CompanyID }); %>
                            </div>
            
                        </div>
                    </div>
                </div>
            
                
            </div>
            <div class="btnlinks">
                <% Html.RenderAction("BackBtn", "Back"); %>
            </div>
        </fieldset>

    <% } %>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <!--<script src="../../Scripts/ApplicationJS/roleManagementCreate.js" type="text/javascript"></script>-->
    <link href="../../Content/rolemanagement.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/rolemanagement-edit.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ApplicationJS/compDropDown.js" type="text/javascript"></script>
    <script src="../../Scripts/ApplicationJS/rolemanagement.js" type="text/javascript"></script>
    <!--<script src="../../Scripts/ApplicationJS/rolemanagement-name-unique.js" type="text/javascript"></script>-->
</asp:Content>

