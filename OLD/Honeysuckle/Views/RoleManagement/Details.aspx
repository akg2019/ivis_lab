﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Honeysuckle.Models.UserGroup>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <fieldset>
        <legend>Details</legend>
        <div class="field">
            <div class="display-label">UserGroupID</div>
            <div class="display-field"><%: Model.UserGroupID%></div>
        </div>
        <div class="field">
            <div class="display-label">UserGroupName</div>
            <div class="display-field"><%: Model.UserGroupName%></div>
        </div>
    
    <% if (Model.SuperGroup)
       { %>
       <div class="children">
        <% foreach (Honeysuckle.Models.UserGroup child in Model.children)
           { %>
            <div class="child">
                <strong><p>Child Group</p></strong>
                <div class="child-group-name">
                    <span>Group Name:</span> <%: child.UserGroupName%> <br />
                </div>
                <div class="child-permissions">
                    <% child.Permissions = Honeysuckle.Models.UserPermissionModel.GetAllPermissionsForGroup(child); %>
                    <% foreach (Honeysuckle.Models.UserPermission permission in child.Permissions)
                       { %>
                        <div class="perm">
                            <%: permission.actiontable %>
                        </div>
                        <br />
                    <% } %>
                </div>
                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("UserGroups","Update")))
                    { %>
                    <%: Html.ActionLink("Edit This Child Group", "Edit", new { id = child.UserGroupID })%>
                <% } %>
            </div>
            <br />
        <% } %>
        </div>
    <% } %>
    <% else
       { %>
        <div class="permissions">
        <% foreach (Honeysuckle.Models.UserPermission permission in Model.Permissions)
           { %>
            <div class="permission">
                <%: permission.actiontable %>
            </div>
            <br />
        <% } %>
        </div>
    <% } %>
    </fieldset>
    <p>
        <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("UserGroups","Update")))
            { %>
                <% if (Honeysuckle.Models.UserModel.TestUserPermissions(Honeysuckle.Models.Constants.user, new Honeysuckle.Models.UserPermission("UserPermissions","Update")))
                 { %>
                     <%: Html.ActionLink("Edit", "Edit", new { id = Model.UserGroupID })%> |
              <% } %>
         <% } %>

        <%: Html.ActionLink("Back to List", "Index") %>
    </p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="jscss" runat="server">
    <link href="../../Content/rolemanagement-details.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftBar" runat="server">
</asp:Content>

