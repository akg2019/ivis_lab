﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Honeysuckle.Models.VehicleCertificate>>" %>

  <div class="approval-modal">

   <div class="ivis-table">

    <div class="vehicle-check item selectItem selectAll">
        <%: Html.CheckBox("selectAll", false, new { @class = "selectAll", @OnClick = "changeSelectAll(this)" })%>   Select All
    </div>

     <div class= "menu vehicle-row">
        <div class="item selectItem">Select</div> 
        <div class="element item"> Certifictate No. </div>
        <div class="make item">Risk </div> 

     </div>

        <div id="vehiclesDocList">
          <% if(Model.Count() == 0) 
         { %>
               <div class="rowNone">There are No Certificates at this time</div>
       <% } %>

        <% int i = 0; %>
        <% foreach (Honeysuckle.Models.VehicleCertificate item in Model)
           { %>
            <% if (i % 2 == 0) { %><div class="vehicle-row row even"> <% } %>
            <% else { %> <div class="vehicle-row row odd"> <% } %>
                <% i++; %>

                <div class="vehicle-check item selectItem">
                    <%: Html.CheckBox("vehcheck", false, new { @class = "vehcheck", id = item.VehicleCertificateID.ToString()})%>
                </div>
                
                <div class="item element vcPrint ">
                    <%: item.CertificateNo %>
                </div>

                <div class="item make">
                    <%: item.Risk.VehicleYear %> , <%:item.Risk.make  %>  <%:item.Risk.VehicleModel  %>
                </div>
            </div>
        <% } %>
     </div>


        </div> 
   </div> 
</div>
