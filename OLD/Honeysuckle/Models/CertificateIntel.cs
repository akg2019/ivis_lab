﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class CertificateIntel
    {
        public int ID { get; set; }
        public VehicleCertificate certificate { get; set; }
        public Policy policy { get; set; }
        public DateTime date { get; set; }
        public Vehicle risk { get; set; }
        public User user { get; set; }
        public Company company { get; set; }

        public CertificateIntel() { }

        public CertificateIntel(int ID)
        {
            this.ID = ID;
        }
        public CertificateIntel(VehicleCertificate certificate, Policy policy, DateTime date, Vehicle risk, User user, Company company)
        {
            this.certificate = certificate;
            this.policy = policy;
            this.date = date;
            this.risk = risk;
            this.user = user;
            this.company = company;
        }
        public CertificateIntel(int ID, VehicleCertificate certificate, Policy policy, DateTime date, Vehicle risk, User user, Company company)
        {
            this.ID = ID;
            this.certificate = certificate;
            this.policy = policy;
            this.date = date;
            this.risk = risk;
            this.user = user;
            this.company = company;
        }
    }
    public class CertificateIntelModel
    {
        /// <summary>
        /// this function logs the creation of a certificate for business intelligence purposes
        /// </summary>
        /// <param name="certintel"></param>
        //public static void LogCert(CertificateIntel certintel)
        //{
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        string query = "EXEC LogCert " + certintel.policy.ID + "," + certintel.certificate.VehicleCertificateID + ",'" + certintel.date.ToString("yyyy-MM-dd hh:mm:ss") + "'," + certintel.risk.ID + "," + certintel.user.ID + "," + certintel.company.CompanyID;
        //        SqlDataReader reader = sql.QuerySQL(query, "read", "certificateintel", certintel);
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //}

        #region old
        ///// <summary>
        ///// this function returns intelligence data for certificates in a time range
        ///// </summary>
        ///// <param name="start"></param>
        ///// <param name="end"></param>
        ///// <returns></returns>
        //public static List<CertificateIntel> CertIntelForTime(DateTime start, DateTime end)
        //{
        //    List<CertificateIntel> intel = new List<CertificateIntel>();
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        SqlDataReader reader = sql.QuerySQL("EXEC CertIntelForTime '" + start.ToString("yyyy-MM-dd hh:mm:ss") + "','" + end.ToString("yyyy-MM-dd hh:mm:ss") + "'");
        //        while (reader.Read())
        //        {
                    
        //        }
        //        sql.DisconnectSQL();
        //    }
        //    return intel;
        //}
        #endregion
    }
}