﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class Mortgagee
    {
        public int ID { get; set; }
        public string mortgagee { get; set; }

        public Mortgagee() { }
        public Mortgagee(string mortgagee)
        {
            this.mortgagee = mortgagee;
        }
        public Mortgagee(int ID, string mortgagee)
        {
            this.ID = ID;
            this.mortgagee = mortgagee;
        }
    }
    public class MortgageeModel
    {
        /// <summary>
        /// this function adds a mortgagee to the database
        /// this version is to be used by logged in users on the 
        /// website; not the json webservice
        /// </summary>
        /// <param name="mortgagee"></param>
        /// <returns></returns>
        public static Mortgagee AddMortgagee(Mortgagee mortgagee)
        {
            return AddMortgagee(mortgagee, Constants.user.ID);
        }

        /// <summary>
        /// this function adds a mortgagee to the database
        /// this version is to be used by the json webservice
        /// </summary>
        /// <param name="mortgagee"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static Mortgagee AddMortgagee(Mortgagee mortgagee, int uid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC AddMortgagee '" + Constants.CleanString(mortgagee.mortgagee) + "'," + uid;
                SqlDataReader reader = sql.QuerySQL(query, "create", "mortgagee", mortgagee);
                while (reader.Read())
                {
                    mortgagee.ID = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return mortgagee;
        }

        /// <summary>
        /// this function returns all the mortgagees in the database
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAllMortgagees()
        {
            List<string> output = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetAllMortgagees", "read", "mortgagee", new Mortgagee());
                while (reader.Read())
                {
                    output.Add(reader["mortgagee"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return output;
        }

        /// <summary>
        /// this function returns the mortgagee in question 
        /// </summary>
        /// <param name="mortgagee"></param>
        /// <returns></returns>
        public static Mortgagee GetMortgagee(Mortgagee mortgagee)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetMortgageeID '" + Constants.CleanString(mortgagee.mortgagee) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "mortgagee", new Mortgagee());
                while (reader.Read())
                {
                    mortgagee.ID = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return mortgagee;
        }

    }
}