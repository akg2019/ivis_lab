﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Honeysuckle.Models
{
    public class Vehicle
    {
        public int ID { get; set; }
        public int base_id { get; set; }

        public string EDIID { get; set; }
        public string mileage { get; set; }
        public bool RestrictedDriving { get; set; }
        public string RegisteredOwner { get; set; }

        [RegularExpression("(([$])?[1-9][0-9]{0,2}(([,][0-9]{3})*|([0-9]*))([.][0-9]{0,2})?)", ErrorMessage = "Incorrect estimated value format (only numbers allowed) ")]
        public string estimatedValue { get; set; }

        [RegularExpression("([a-zA-Z0-9][a-zA-Z0-9-'.& ]*[a-zA-Z0-9])", ErrorMessage = "Incorrect 'make' format  (no numbers allowed) ")]
        public string make { get; set; }

        [RegularExpression("([a-zA-Z0-9][a-zA-Z0-9-. ]*[a-zA-Z0-9]*)", ErrorMessage = "Incorrect model format (Letters, numbers and hyphens only)")]
        public string VehicleModel { get; set; }

        [RegularExpression("([a-zA-Z0-9][a-zA-Z0-9-./& ]*[a-zA-Z0-9])", ErrorMessage = "Incorrect model type format")]
        public string modelType { get; set; }

        [RegularExpression("([a-zA-Z0-9][a-zA-Z0-9-/,.& ]*[a-zA-Z0-9])", ErrorMessage = "Incorrect body type format")]
        public string bodyType { get; set; }

        [RegularExpression("([a-zA-Z0-9][a-zA-Z0-9-,./& ]*[a-zA-Z0-9])", ErrorMessage = "Incorrect extention format")]
        public string extension { get; set; }

        [RegularExpression("([a-zA-Z0-9]+)", ErrorMessage = "Incorrect registration number format (Letters, numbers and hyphens only)")]
        public string VehicleRegNumber { get; set; }

        [RegularExpression("([a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]+)", ErrorMessage = "Incorrect VIN format (Letters, numbers and hyphens only)")]
        public string VIN { get; set; }

        [RegularExpression("([a-zA-Z0-9]*)", ErrorMessage = "Chassis Numbers can only be made of numbers and letters")]
        public string chassisno { get; set; }

        public HandDriveType handDrive { get; set; }

        [RegularExpression("([a-zA-Z-.,/& ]+)", ErrorMessage = "Incorrect colour format (Letters only)")]
        public string colour { get; set; }

        [RegularExpression("([0-9]+)", ErrorMessage = "Incorrect cylinders format (Numbers only)")]
        public int cylinders { get; set; }

        [RegularExpression("([a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]+)", ErrorMessage = "Incorrect engine number format (Letters, numbers and hyphens only)")]
        public string engineNo { get; set; }

        //[RegularExpression("([a-zA-Z][a-zA-Z]*)", ErrorMessage = "Incorrect engine modified format (Letters, numbers and hyphens only)")]
        public bool engineModified { get; set; }

        [RegularExpression("([a-zA-Z0-9][a-zA-Z0-9-&'-/,. ]*[a-zA-Z0-9]+)", ErrorMessage = "Incorrect engine type format")]
        public string engineType { get; set; }

        [RegularExpression("([0-9]+)", ErrorMessage = "Incorrect mileage format (Numbers only)")]
        public int estAnnualMileage { get; set; }

        public DateTime expiryDateOfFitness { get; set; }

        [RegularExpression("([a-zA-Z0-9][a-zA-Z0-9,.\\-'/ ]*[a-zA-Z0-9]+)", ErrorMessage = "Incorrect HPPCCUnit format (Letters, numbers and hyphens only)")]
        public string HPCCUnitType { get; set; }

        public Driver mainDriver { get; set; }

        [RegularExpression("([a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]*)", ErrorMessage = "Incorrect reference number format (Letters, numbers and hyphens only)")]
        public string referenceNo { get; set; }

        [RegularExpression("([a-zA-Z0-9][a-zA-Z0-9-,&/., #']*[a-zA-Z0-9]+)", ErrorMessage = "Incorrect registration location format (Letters, numbers and hyphens only)")]
        public string registrationLocation { get; set; }

        [RegularExpression("([a-zA-Z0-9][a-zA-Z0-9- ]*[a-zA-Z0-9]+)", ErrorMessage = "Incorrect roof type format (Letters, numbers and hyphens only)")]
        public string roofType { get; set; }

        [RegularExpression("([0-9]+)", ErrorMessage = "Incorrect seating format (Numbers only)")]
        public int seating { get; set; }

        [RegularExpression("([0-9]+)", ErrorMessage = "Incorrect seatingCert format (Numbers only)")]
        public int seatingCert { get; set; }

        [RegularExpression("([0-9]+)", ErrorMessage = "Incorrect tonnage format (Numbers only)")]
        public int tonnage { get; set; }

        public TransmissionType transmissionType { get; set; }

        [RegularExpression("([0-9]+)", ErrorMessage = "Incorrect year format (Numbers only)")]
        public int VehicleYear { get; set; }

        public List<Driver> AuthorizedDrivers { get; set; }
        public List<Driver> ExceptedDrivers { get; set; }
        public List<Driver> ExcludedDrivers { get; set; }

        public string chassisReg { get; set; }
        public Usage usage { get; set; }

        public int risk_item_no { get; set; } //coming from external system
        public Mortgagee mortgagee { get; set; } //coming from vehicles under policy database table
        public AuthorizedDriverWording authorized_driver_wording { get; set; } //coming from vehicles under policy database table

        public bool vehicle_under_pol { get; set; }
        public bool vehicle_has_active_cover_note { get; set; }
        public bool vehicle_has_cover_note_history { get; set; }
        public bool underactivepolicy { get; set; }

        public bool imported { get; set; }

        public bool FSLerror { get; set; }
        public bool hasDuplicates { get; set; }
        public bool IsTAJ { get; set; }

        public DateTime createdOn { get; set; }
        public int createdBy { get; set; }

        public Vehicle() { }
        public Vehicle(string chassisno)
        {
            this.chassisno = chassisno;
        }
        public Vehicle(int ID)
        {
            this.ID = ID;
        }
        public Vehicle(int ID, string VIN)
        {
            this.ID = ID;
            this.VIN = VIN;
        }
        public Vehicle(int ID, string Make, string Model, string modelType, string bodyType, string extension, string VehicleRegNumber, string VIN)
        {
            this.ID = ID;
            this.make = Make;
            this.VehicleModel = Model;
            this.modelType = modelType;
            this.bodyType = bodyType;
            this.extension = extension;
            this.VehicleRegNumber = VehicleRegNumber;
            this.VIN = VIN;

        }
        public Vehicle(int ID, string Make, string Model, string modelType, string bodyType, string extension, string VehicleRegNumber, string VIN, string chassisno)
        {
            this.ID = ID;
            this.make = Make;
            this.VehicleModel = Model;
            this.modelType = modelType;
            this.bodyType = bodyType;
            this.extension = extension;
            this.VehicleRegNumber = VehicleRegNumber;
            this.VIN = VIN;
            this.chassisno = chassisno;
        }
        public Vehicle(int ID, string Make, string Model, string modelType, string bodyType, string extension, string VIN, string ChassisReg, int seating)
        {
            this.ID = ID;
            this.make = Make;
            this.VehicleModel = Model;
            this.modelType = modelType;
            this.bodyType = bodyType;
            this.extension = extension;
            this.VehicleRegNumber = VehicleRegNumber;
            this.VIN = VIN;
            this.chassisReg = ChassisReg;

        }

        public Vehicle(int ID, string VIN, string Make, string Model)
        {
            this.ID = ID;
            this.VIN = VIN;
            this.make = Make;
            this.VehicleModel = Model;
        }

        public Vehicle(int ID, string VIN, string chassis, string Make, string Model, int year, string usage, string vehRegNumber)
        {
            this.ID = ID;
            this.VIN = VIN;
            this.make = Make;
            this.VehicleModel = Model;
            this.chassisno = chassis;
            this.VehicleYear = year;
            this.usage = new Usage(usage);
            this.VehicleRegNumber = vehRegNumber;
        }
        public Vehicle(int ID, string VIN, string VehicleRegNumber, string Make, string Model)
        {
            this.ID = ID;
            this.VIN = VIN;
            this.VehicleRegNumber = VehicleRegNumber;
            this.make = Make;
            this.VehicleModel = Model;
        }
        public Vehicle(int ID, string make, string model, string modelType, string BodyType, string Extension, string VehicleRegNumber, string VIN, string chassisno,
                       HandDriveType handDriveType, string colour, int cylinders, string engineNo, bool engineModified, string engineType, int estAnnualMileage,
                       DateTime expiryDateOfFitness, string HPCCUnitType, Driver MainDriver, string ReferenceNo, string RegistrationLocation,
                       string RoofType, int seating, int seatingCert, int tonnage, TransmissionType transmissionType, int VehicleYear)
        {

            this.ID = ID;
            this.make = make;
            this.VehicleModel = model;
            this.modelType = modelType;
            this.bodyType = BodyType;
            this.extension = Extension;
            this.VehicleRegNumber = VehicleRegNumber;
            this.VIN = VIN;
            this.chassisno = chassisno;
            this.handDrive = handDriveType;
            this.colour = colour;
            this.cylinders = cylinders;
            this.engineNo = engineNo;
            this.engineModified = engineModified;
            this.engineType = engineType;
            this.estAnnualMileage = estAnnualMileage;
            this.expiryDateOfFitness = expiryDateOfFitness;
            this.HPCCUnitType = HPCCUnitType;
            this.mainDriver = MainDriver;
            this.referenceNo = ReferenceNo;
            this.registrationLocation = RegistrationLocation;
            this.roofType = RoofType;
            this.seating = seating;
            this.seatingCert = seatingCert;
            this.tonnage = tonnage;
            this.transmissionType = transmissionType;
            this.VehicleYear = VehicleYear;
        }

        public Vehicle(string VehicleRegNumber, int ID = 0)
        {
            this.ID = ID;
            this.VehicleRegNumber = VehicleRegNumber;
        }

        public Vehicle(int ID, string chassisno, string regno)
        {
            this.ID = ID;
            this.chassisno = chassisno;
            this.VehicleRegNumber = regno;
            this.chassisReg = "Chassis No: " + this.chassisno + "; RegNo: " + this.VehicleRegNumber;
        }

        public Vehicle(int ID, string chassisno, string regno, string make, string model, bool underPol, bool coverHis, bool activeCover, bool imported)
        {
            this.ID = ID;
            this.chassisno = chassisno;
            this.VehicleRegNumber = regno;
            this.make = make;
            this.VehicleModel = model;
            this.vehicle_under_pol = underPol;
            this.vehicle_has_cover_note_history = coverHis;
            this.vehicle_has_active_cover_note = activeCover;
            this.imported = imported;
        }

        public static List<int> toInt(List<Vehicle> vehicles)
        {
            List<int> result = new List<int>();
            foreach (Vehicle vehicle in vehicles)
            {
                result.Add(vehicle.ID);
            }
            return result;
        }

    }

    public class VehicleModel
    {

        /// <summary>
        /// this function returns all the vehicles that a company has and currently covers
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public static List<Vehicle> VehiclesMyCompanyCovered(Company company)
        {
            List<Vehicle> vehicles = new List<Vehicle>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC VehiclesMyCompanyCovered " + company.CompanyID;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    Vehicle veh = new Vehicle();
                    veh.ID = int.Parse(reader["ID"].ToString());
                    veh.chassisno = reader["chassis"].ToString();
                    veh.make = reader["make"].ToString();
                    veh.VehicleModel = reader["model"].ToString();
                    veh.VehicleRegNumber = reader["regno"].ToString();
                    veh.VIN = reader["vin"].ToString();
                    vehicles.Add(veh);
                }
                sql.DisconnectSQL();
            }
            return vehicles;
        }

        /// <summary>
        /// Gets vehicle by chassis or reg number for current insurer/broker/agent
        /// </summary>
        /// <param name="company">Company object</param>
        /// <param name="SearchType">Type of search Chassis/Reg</param>
        /// <param name="SearchValue">Search value</param>
        /// <returns>A list of Insured vehicles matching the chassis or reg number</returns>
        public static List<Vehicle> VehiclesMyCompanyCovered(Company company, string SearchType, string SearchValue)
        {
            List<Vehicle> vehicles = new List<Vehicle>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC VehiclesMyCompanyCovered_1 " + company.CompanyID + ", '" + SearchType + "','" + SearchValue + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    Vehicle veh = new Vehicle();
                    veh.ID = int.Parse(reader["ID"].ToString());
                    veh.chassisno = reader["chassis"].ToString();
                    veh.make = reader["make"].ToString();
                    veh.VehicleModel = reader["model"].ToString();
                    veh.VehicleRegNumber = reader["regno"].ToString();
                    veh.VIN = reader["vin"].ToString();
                    vehicles.Add(veh);
                }
                sql.DisconnectSQL();
            }
            return vehicles;
        }

        /// <summary>
        /// this function returns all the vehicles covered by a policy
        /// </summary>
        /// <param name="id">policy id</param>
        /// <returns></returns>
        public static bool VehicleCoveredByMyPolicy(int id)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC VehicleCoveredByMyPolicy " + id + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function returns all the vehicles that can have a certificate
        /// or a cover note created for right now
        /// </summary>
        /// <returns></returns>
        public static List<Vehicle> GetVehiclesCanBeCertOrCNote()
        {
            List<Vehicle> Vehicles = new List<Vehicle>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "";
                if (Constants.user.newRoleMode) query = "EXEC GetVehiclesCanBeCertOrCNote " + Constants.user.tempUserGroup.company.CompanyID;
                else query = "EXEC GetVehiclesCanBeCertOrCNote " + CompanyModel.GetMyCompany(Constants.user).CompanyID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    Vehicle vehicle = new Vehicle(int.Parse(reader["ID"].ToString()), reader["vin"].ToString(), reader["regno"].ToString(), reader["make"].ToString(), reader["model"].ToString());
                    vehicle.chassisno = reader["chassis"].ToString();
                    Vehicles.Add(vehicle);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return Vehicles;
        }

        /// <summary>
        /// this function returns all vehicles which do not have any certificates currently
        /// </summary>
        /// <returns></returns>
        public static List<Vehicle> GetVehiclesWithoutCerts()
        {
            List<Vehicle> Vehicles = new List<Vehicle>();
            //sql sql = new sql();
            //if (sql.ConnectSQL())
            //{
            //    string query = "EXEC GetVehiclesWithoutCerts " + CompanyModel.GetMyCompany(Constants.user).CompanyID;
            //    SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
            //    //SqlDataReader reader = sql.QuerySQL(query);
            //    while (reader.Read())
            //    {
            //        Vehicle vehicle = new Vehicle(int.Parse(reader["ID"].ToString()), reader["vin"].ToString(), reader["regno"].ToString(), reader["make"].ToString(), reader["model"].ToString());
            //        vehicle.chassisno = reader["chassis"].ToString();
            //        Vehicles.Add(vehicle);
            //    }
            //    reader.Close();
            //    sql.DisconnectSQL();
            //}
            return Vehicles;
        }

        /// <summary>
        /// this function returns all vehicles that do not have a cover note currently
        /// </summary>
        /// <returns></returns>
        public static List<Vehicle> GetVehiclesWithoutCover()
        {

            List<Vehicle> Vehicles = new List<Vehicle>();
            //sql sql = new sql();
            //if (sql.ConnectSQL())
            //{
            //    string query = "EXEC GetVehiclesWithoutCover " + CompanyModel.GetMyCompany(Constants.user).CompanyID;
            //    SqlDataReader reader = sql.QuerySQL(query);
            //    //SqlDataReader reader = sql.QuerySQL(query);
            //    while (reader.Read())
            //    {
            //        Vehicle vehicle = new Vehicle(int.Parse(reader["ID"].ToString()), reader["vin"].ToString(), reader["regno"].ToString(), reader["make"].ToString(), reader["model"].ToString());
            //        vehicle.chassisno = reader["chassis"].ToString();
            //        Vehicles.Add(vehicle);
            //    }
            //    reader.Close();
            //    sql.DisconnectSQL();
            //}
            return Vehicles;
        }

        /// <summary>
        /// this function returns a short pack of data related to a vehicle
        /// </summary>
        /// <param name="id">vehicle id</param>
        /// <param name="cmp">company id</param>
        /// <returns></returns>
        private static Vehicle GetShortVehicle(int id, int cmp = 0)
        {
            Vehicle Risk = new Vehicle(id);
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetShortVehicle " + id;
                if (cmp != 0) query = query + "," + cmp;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    Risk = new Vehicle(id,
                        reader["VIN"].ToString(),
                        reader["chassis"].ToString(),
                        reader["make"].ToString(),
                        reader["model"].ToString(),
                        0,
                        null,
                        reader["regno"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return Risk;
        }

        /// <summary>
        /// this function returns all vehicles in short data
        /// </summary>
        /// <param name="ids">list of vehicle ids</param>
        /// <param name="cmp">company id</param>
        /// <returns></returns>
        public static List<Vehicle> GetShortVehicles(List<int> ids, int cmp = 0)
        {
            List<Vehicle> Vehicles = new List<Vehicle>();
            foreach (int i in ids)
            {
                Vehicle v = GetShortVehicle(i, cmp);
                //if (v.make != null )
                Vehicles.Add(v);
            }

            return Vehicles;
        }

        /// <summary>
        /// This function tests if a Risk is already covered under this specific policy
        /// </summary>
        /// <param name="Risk">Valid Risk already in the system</param>
        /// <returns>boolean</returns>
        public static bool IsVehicleUnderPolicy(int VehicleId, int policyid, DateTime start, DateTime end)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsVehicleUnderPolicy '" + start.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                             + end.ToString("yyyy-MM-dd HH:mm:ss") + "',"
                                                             + VehicleId + ","
                                                             + policyid;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function tests if a Risk is already covered under an existing policy
        /// </summary>
        /// <param name="Risk">Valid Risk already in the system</param>
        /// <returns>boolean</returns>
        public static bool IsVehicleUnderPolTimeFrame(int VehicleId)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsVehicleUnderPol " + VehicleId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function tests if a Risk is already covered under an existing policy at my company
        /// </summary>
        /// <param name="Risk">valid Risk already in the system</param>
        /// <returns>boolean</returns>
        public static bool IsVehicleUnderPolicyAtMyCompany(int VehicleId, DateTime start, DateTime end)
        {
            bool result = false;
            sql sql = new sql();
            int companyId;

            if (Constants.user.newRoleMode) companyId = Constants.user.tempUserGroup.company.CompanyID;
            else companyId = CompanyModel.GetMyCompany(Constants.user).CompanyID;

            if (sql.ConnectSQL())
            {

                string query = "EXEC IsVehicleUnderPolicyAtMyCompany '" + start.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                                        + end.ToString("yyyy-MM-dd HH:mm:ss") + "',"
                                                                        + VehicleId + ","
                                                                        + companyId;

                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
                //SqlDataReader reader = sql.QuerySQL(query);

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        public static bool IsVehicleUnderPolicyAtMyCompanyCurrent(int VehicleId)
        {
            bool result = false;
            sql sql = new sql();
            int companyId;

            if (Constants.user.newRoleMode) companyId = Constants.user.tempUserGroup.company.CompanyID;
            else companyId = CompanyModel.GetMyCompany(Constants.user).CompanyID;

            if (sql.ConnectSQL())
            {

                string query = "EXEC IsVehicleUnderPolicyAtMyCompanyCurrent " + VehicleId + ","
                                                                                + companyId;

                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
                //SqlDataReader reader = sql.QuerySQL(query);

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function adds a vehicle to the database
        /// </summary>
        /// <param name="Risk"></param>
        /// <returns></returns>
        public static Vehicle addVehicle(Vehicle Risk, bool update = false, Policy Pol = null)
        {
            return addVehicle(Risk, Constants.user.ID, update, false, Pol);
        }

        /// <summary>
        /// This function deals with the adding a new Risk to the system
        /// </summary>
        /// <param name="Risk">Complete Risk model-Contains all the data for the new Risk </param>
        public static Vehicle addVehicle(Vehicle Risk, int uid, bool update = false, bool importing = false, Policy Pol = null)
        {
            List<User> Admins = new List<User>();
            Notification note = new Notification();
            List<string> to = new List<string>();
            note.NoteTo = new List<User>();
            List<string> emails = new List<string>();

            Driver d = new Driver();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                if (Risk.mainDriver != null)
                {
                    if (Risk.mainDriver.person != null)
                    {
                        //Adding the address of the main driver to the system (or retrieving the existing id if the address previously existed in the system)
                        if (Risk.mainDriver.person.address != null) Risk.mainDriver.person.address.ID = AddressModel.AddAddress(Risk.mainDriver.person.address);

                        bool personUpdate = false;
                        if (importing && PersonModel.GetPersonByTRN(Risk.mainDriver.person.TRN).PersonID != 0) personUpdate = true;

                        //Checking to see if the person already exists
                        if (Risk.mainDriver.person.PersonID == 0 && Risk.mainDriver.person.TRN != null && Risk.mainDriver.person.TRN != "") PersonModel.AddPerson(Risk.mainDriver.person, uid, false, importing, personUpdate);

                    }
                }
                else Risk.mainDriver = new Driver(); //do i need this?


                //Testing to see if the person is already a driver in the system
                int driverId = DriverModel.getDriver(Risk);

                if (driverId == 0)
                {
                    if (Risk.mainDriver.person != null && Risk.mainDriver.person.PersonID != 0) Risk.mainDriver.DriverId = DriverModel.addDriver(Risk.mainDriver.person.PersonID, uid);
                }
                else Risk.mainDriver.DriverId = driverId;

                if (Risk.mainDriver.person != null && (Risk.mainDriver.person.TRN == null || Risk.mainDriver.person.TRN == "")) Risk.mainDriver.DriverId = 0;

                if (Risk.handDrive == null)
                {
                    Risk.handDrive = new HandDriveType();
                    Risk.handDrive.handDriveType = "";
                }

                if (Risk.transmissionType == null)
                {
                    Risk.transmissionType = new TransmissionType();
                    Risk.transmissionType.transmissionType = "";
                }

                if (Risk.usage == null) Risk.usage = new Usage();
                if (Risk.colour != null) Risk.colour = Risk.colour.Trim();

                int IntermediaryID = 0; int CompanyID = 0;

                //Gets Company ID and Intermediary IDs to be added to the vehicle and Vehicle MetaData tables.
                try
                {
                    if (Pol != null)
                    {
                        CompanyID = Pol.insuredby.CompanyID;

                        IntermediaryID = Pol.compCreatedBy.CompanyID;

                        if (IntermediaryID == 0) IntermediaryID = CompanyID;
                    }
                    else 
                    { 
                        CompanyID = CompanyModel.GetMyCompany(new User(uid)).CompanyID;
                        try { IntermediaryID = Pol.compCreatedBy.CompanyID; }catch(Exception){}
                    
                    }


                    try { if (CompanyID == 0) CompanyID = CompanyModel.GetCompanyByTRN(Pol.insuredby.trn).CompanyID; }catch(Exception){}
                    try { if (IntermediaryID == 0) IntermediaryID = CompanyModel.GetCompanyByTRN(Pol.compCreatedBy.trn).CompanyID; }catch(Exception){}

                    if (IntermediaryID == 0) IntermediaryID = CompanyID;
                                         
                }
                catch (Exception)
                {
                  
                }


                if (IntermediaryID == 0) IntermediaryID = CompanyID;
                try { if (CompanyID == 0) CompanyID = CompanyModel.GetCompanyByTRN(Pol.insuredby.trn).CompanyID; }
                catch (Exception) { }
                try { if (IntermediaryID == 0) IntermediaryID = CompanyModel.GetCompanyByTRN(Pol.compCreatedBy.trn).CompanyID; }
                catch (Exception) { }

                if (IntermediaryID == 0) IntermediaryID = CompanyID;

                string query = "EXECUTE AddVehicle '" + Constants.CleanString(Risk.EDIID) + "','"
                                                      + Constants.CleanString(Risk.make) + "','"
                                                      + Constants.CleanString(Risk.VehicleModel) + "', '"
                                                      + Constants.CleanString(Risk.modelType) + "','"
                                                      + Constants.CleanString(Risk.bodyType) + "','"
                                                      + Constants.CleanString(Risk.extension) + "','"
                                                      + Constants.CleanString(Risk.VehicleRegNumber) + "','"
                                                      + Constants.CleanString(Risk.VIN) + "','"
                                                      + Constants.CleanString(Risk.chassisno) + "','"
                                                      + Constants.CleanString(Risk.handDrive.handDriveType) + "','"
                                                      + Constants.CleanString(Risk.colour) + "','"
                                                      + Constants.CleanString(Risk.engineNo) + "','"
                                                      + Risk.engineModified + "','"
                                                      + Constants.CleanString(Risk.engineType) + "','"
                                                      + Constants.CleanString(Risk.HPCCUnitType) + "','"
                                                      + Constants.CleanString(Risk.registrationLocation) + "','"
                                                      + Constants.CleanString(Risk.roofType) + "','"
                                                      + Risk.expiryDateOfFitness.ToString("yyyy-MM-dd") + "','"
                                                      + Constants.CleanString(Risk.RegisteredOwner) + "',"
                                                      + Risk.RestrictedDriving + ",'"
                                                      + Constants.CleanString(Risk.transmissionType.transmissionType) + "',"
                                                      + Risk.seating + ","
                                                      + Risk.seatingCert + ","
                                                      + Risk.tonnage + ","
                                                      + Risk.VehicleYear + ","
                                                      + Risk.cylinders + "," +
                                                      +Risk.estAnnualMileage + ","
                                                      + Risk.mainDriver.DriverId + ",'"
                                                      + Constants.CleanString(Risk.referenceNo) + "','"
                                                      + Constants.CleanString(Risk.estimatedValue) + "',"
                                                      + uid + ","
                                                      + update + "," + CompanyID + "," + IntermediaryID;

                SqlDataReader reader = sql.QuerySQL(query, "create", "vehicle", Risk, uid);
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    Risk.ID = int.Parse(reader["id"].ToString());
                    Risk.transmissionType.ID = int.Parse(reader["tid"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();

                try
                {
                    //List<Vehicle> vehicle = getDuplicateVehicles(Risk.chassisno);
                    //if (vehicle.Count > 1)
                    //{
                    //    note.heading = "Vehicle - Duplicates";
                    //    note.content = "The following duplicates are in the system now for Chassis - " + Risk.chassisno + ": \n";


                    //    foreach (Vehicle v in vehicle)
                    //    {
                    //        Company comp = CompanyModel.GetMyCompany(new User(v.createdBy));
                    //        List<User> u = CompanyModel.GetCompanyAdmin(new Company(comp.CompanyID));
                    //        List<User> uAdd = new List<User>();
                    //        foreach (User ua in u)
                    //        {
                    //            if (!emails.Contains(ua.username)) uAdd.Add(ua);
                    //            emails.Add(ua.username);
                    //        }
                    //        Admins.AddRange(uAdd);
                    //        note.content += v.VehicleYear + " " + v.make + " " + v.VehicleModel + " " + v.VehicleRegNumber + " , Created On: " + v.createdOn + " at " + comp.CompanyName; //+ ". View at: " + System.Web.HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + System.Web.HttpContext.Current.Request.Url.Host + (System.Web.HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + System.Web.HttpContext.Current.Request.Url.Port) + "/Vehicle/Details/" + v.ID + " \n";
                    //    }
                    //    for (int p = 0; p < Admins.Count(); p++)
                    //    {
                    //        note.NoteTo.Add(Admins[p]);
                    //    }

                    //    for (int x = 0; x < Admins.Count(); x++)
                    //        to.Add(Admins[x].username);

                    //    //MailModel.SendMailWithThisFunction(Admins, new Mail(to, note.heading, note.content));

                    //}
                }
                catch (Exception e)
                {
                }
            }
            return Risk;
        }

        /// <summary>
        /// this retrieves the mortgagee of the risk on a particular policy
        /// </summary>
        /// <param name="riskid"></param>
        /// <param name="polid"></param>
        /// <returns></returns>
        public static Mortgagee getMortgagee(int riskid, int polid)
        {
            Mortgagee mortgagee = new Mortgagee();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetMortgagee " + riskid + "," + polid;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    if ((bool)reader["result"]) mortgagee = new Mortgagee(int.Parse(reader["ID"].ToString()), reader["mortgagee"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return mortgagee;
        }

        /// <summary>
        /// this function returnes the current policy that a risk is under
        /// </summary>
        /// <param name="vehicle">risk</param>
        /// <returns>policy id</returns>
        private static int GetCurrentPolicy(Vehicle vehicle)
        {
            int result = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetVehiclePolicy " + vehicle.ID;
                SqlDataReader reader = sql.QuerySQL(query);
                int rows = 0;
                while (reader.Read())
                {
                    rows++;
                    if (rows > 1)
                    {
                        result = 0;
                        break;
                    }
                    result = int.Parse(reader["PolicyID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function retrieves all the information about a particular Risk
        /// </summary>
        /// <param name="ID">Risk's ID </param>
        /// <returns>Risk</returns>
        public static Vehicle GetVehicle(int ID, bool old = false)
        {
            Vehicle v = new Vehicle();
            HandDriveType hdt = new HandDriveType();
            Person per = new Person();
            Email em = new Email();
            TransmissionType tran = new TransmissionType();
            Driver d = new Driver();
            //Usage u = new Usage();


            Address add = new Address();
            Road road = new Road();
            RoadType road1 = new RoadType();
            ZipCode zip = new ZipCode();
            City city = new City();
            Country country = new Country();
            Parish parish = new Parish();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE GetVehicle " + ID + ",'" + old + "'";
                SqlDataReader reader = sql.QuerySQL("EXECUTE GetVehicle " + ID, "read", "vehicle", new Vehicle());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    v.ID = ID;
                    v.base_id = int.Parse(reader["base_id"].ToString());
                    v.make = reader["Make"].ToString();
                    v.VehicleModel = reader["Model"].ToString();
                    v.modelType = reader["ModelType"].ToString();
                    if (DBNull.Value != reader["BodyType"]) v.bodyType = reader["BodyType"].ToString();
                    if (DBNull.Value != reader["Extension"]) v.extension = reader["Extension"].ToString();
                    if (DBNull.Value != reader["VehicleRegistrationNo"]) v.VehicleRegNumber = reader["VehicleRegistrationNo"].ToString();
                    if (DBNull.Value != reader["VIN"]) v.VIN = reader["VIN"].ToString();
                    v.chassisno = reader["ChassisNo"].ToString();
                    if (DBNull.Value != reader["Colour"]) v.colour = reader["Colour"].ToString();
                    if (DBNull.Value != reader["Cylinders"]) v.cylinders = int.Parse(reader["Cylinders"].ToString());
                    if (DBNull.Value != reader["EngineNo"]) v.engineNo = reader["EngineNo"].ToString();
                    if (DBNull.Value != reader["EngineModified"]) v.engineModified = (bool)reader["EngineModified"];
                    if (DBNull.Value != reader["EngineType"]) v.engineType = reader["EngineType"].ToString();
                    if (DBNull.Value != reader["EstimatedAnnualMileage"]) v.estAnnualMileage = int.Parse(reader["EstimatedAnnualMileage"].ToString());
                    if (DBNull.Value != reader["EstimatedValue"]) v.estimatedValue = reader["EstimatedValue"].ToString();
                    if (DBNull.Value != reader["ExpiryDateOfFitness"]) v.expiryDateOfFitness = DateTime.Parse(reader["ExpiryDateOfFitness"].ToString());
                    if (DBNull.Value != reader["HPCCUnitType"]) v.HPCCUnitType = reader["HPCCUnitType"].ToString();
                    if (DBNull.Value != reader["MainDriver"]) d.DriverId = int.Parse(reader["MainDriver"].ToString());
                    v.mainDriver = d;
                    if (DBNull.Value != reader["ReferenceNo"]) v.referenceNo = reader["ReferenceNo"].ToString();
                    if (DBNull.Value != reader["RegistrationLocation"]) v.registrationLocation = reader["RegistrationLocation"].ToString();
                    if (DBNull.Value != reader["RoofType"]) v.roofType = reader["RoofType"].ToString();
                    if (DBNull.Value != reader["Seating"]) v.seating = int.Parse(reader["Seating"].ToString());
                    if (DBNull.Value != reader["SeatingCert"]) v.seatingCert = int.Parse(reader["SeatingCert"].ToString());
                    if (DBNull.Value != reader["Tonnage"]) v.tonnage = int.Parse(reader["Tonnage"].ToString());
                    if (DBNull.Value != reader["VehicleYear"]) v.VehicleYear = int.Parse(reader["VehicleYear"].ToString());
                    if (DBNull.Value != reader["RegisteredOwner"]) v.RegisteredOwner = reader["RegisteredOwner"].ToString();
                    if (DBNull.Value != reader["RestrictedDriving"]) v.RestrictedDriving = (bool)reader["RestrictedDriving"];
                    //if (DBNull.Value != reader["usage"]) u.usage = reader["usage"].ToString();
                    //u.ID = int.Parse(reader["ID"].ToString());
                    //v.usage = u;

                    if (DBNull.Value != reader["HandDrive"]) hdt.ID = int.Parse(reader["HandDrive"].ToString());
                    if (DBNull.Value != reader["HDType"]) hdt.handDriveType = reader["HDType"].ToString();

                    if (DBNull.Value != reader["TransmissionType"]) tran.transmissionType = reader["TransmissionType"].ToString();

                    if (DBNull.Value != reader["PeopleID"]) per.PersonID = int.Parse(reader["PeopleID"].ToString());
                    if (DBNull.Value != reader["TRN"]) per.TRN = reader["TRN"].ToString();
                    if (DBNull.Value != reader["FName"]) per.fname = reader["FName"].ToString();

                    if (DBNull.Value != reader["MName"]) per.mname = reader["MName"].ToString();
                    if (DBNull.Value != reader["LName"]) per.lname = reader["LName"].ToString();

                    if (DBNull.Value != reader["AID"]) add.ID = int.Parse(reader["AID"].ToString());

                    if (DBNull.Value != reader["RoadNumber"]) add.roadnumber = reader["RoadNumber"].ToString();
                    if (DBNull.Value != reader["AptNumber"]) add.ApartmentNumber = reader["AptNumber"].ToString();

                    if (DBNull.Value != reader["RoadName"]) road.RoadName = reader["RoadName"].ToString();
                    if (DBNull.Value != reader["RoadType"]) road1.RoadTypeName = reader["RoadType"].ToString();

                    if (DBNull.Value != reader["Zipcode"]) zip.ZipCodeName = reader["Zipcode"].ToString();
                    if (DBNull.Value != reader["City"]) city.CityName = reader["City"].ToString();
                    if (DBNull.Value != reader["CountryName"]) country.CountryName = reader["CountryName"].ToString();

                    if (DBNull.Value != reader["PID"])
                    {
                        parish.ID = int.Parse(reader["PID"].ToString());
                        parish.parish = reader["parish"].ToString();
                    }


                    add.road = road;
                    add.roadtype = road1;
                    add.zipcode = zip;
                    add.city = city;
                    add.country = country;
                    add.parish = parish;

                    //Placing all the data sent from the various models into the user model
                    per.address = add;
                    d.person = per;
                    //v.mainDriver = new Driver(per);
                    v.mainDriver = d;
                    v.transmissionType = tran;
                    v.handDrive = hdt;
                    v.chassisReg = "Chassis No: " + v.chassisno + "; RegNo: " + v.VehicleRegNumber;
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return v;
        }

        /// <summary>
        /// this function returns the vehicle with the chassis no in question
        /// </summary>
        /// <param name="chassisno"></param>
        /// <returns></returns>
        public static Vehicle GetVehicleByChassis(string chassisno)
        {
            return GetVehicleByChassis(chassisno, Constants.user.ID);
        }

        /// <summary>
        /// this function returns the vehicle with the chassis no in question
        /// </summary>
        /// <param name="chassisno"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static Vehicle GetVehicleByChassis(string chassisno, int uid)
        {
            Vehicle v = new Vehicle();
            v.chassisno = chassisno;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetVehicleByChassis '" + Constants.CleanString(chassisno) + "'," + uid;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    try
                    {
                        v.ID = int.Parse(reader["ID"].ToString());
                        if (DBNull.Value != reader["Make"]) v.make = reader["Make"].ToString();
                        if (DBNull.Value != reader["Model"]) v.VehicleModel = reader["Model"].ToString();
                        if (DBNull.Value != reader["ModelType"]) v.modelType = reader["ModelType"].ToString();
                        if (DBNull.Value != reader["BodyType"]) v.bodyType = reader["BodyType"].ToString();
                        if (DBNull.Value != reader["Extension"]) v.extension = reader["Extension"].ToString();
                        if (DBNull.Value != reader["VehicleRegistrationNo"]) v.VehicleRegNumber = reader["VehicleRegistrationNo"].ToString();
                        if (DBNull.Value != reader["VIN"]) v.VIN = reader["VIN"].ToString();
                        if (DBNull.Value != reader["ChassisNo"]) v.chassisno = reader["ChassisNo"].ToString();
                        if (DBNull.Value != reader["Colour"]) v.colour = reader["Colour"].ToString();
                        if (DBNull.Value != reader["VehicleYear"]) v.VehicleYear = int.Parse(reader["VehicleYear"].ToString());
                        if (DBNull.Value != reader["PeopleID"]) v.mainDriver = new Driver(new Person(int.Parse(reader["PeopleID"].ToString())));
                    }
                    catch (Exception)
                    {
                        
                       
                    }
                    

                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return v;
        }

        /// <summary>
        /// this function returns a vehicle with a reginstration no
        /// </summary>
        /// <param name="licenseNo"></param>
        /// <returns></returns>
        public static Vehicle GetVehicleByLicenseNo(string licenseNo)
        {
            Vehicle v = new Vehicle();
            v.VehicleRegNumber = licenseNo;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetVehicleByRegNo '" + Constants.CleanString(licenseNo) + "'";
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {

                    try
                    {
                        v.ID = int.Parse(reader["ID"].ToString());
                        v.base_id = int.Parse(reader["base_id"].ToString());
                        if (DBNull.Value != reader["Make"]) v.make = reader["Make"].ToString();
                        if (DBNull.Value != reader["Model"]) v.VehicleModel = reader["Model"].ToString();
                        if (DBNull.Value != reader["ModelType"]) v.modelType = reader["ModelType"].ToString();
                        if (DBNull.Value != reader["BodyType"]) v.bodyType = reader["BodyType"].ToString();
                        if (DBNull.Value != reader["Extension"]) v.extension = reader["Extension"].ToString();
                        if (DBNull.Value != reader["VIN"]) v.VIN = reader["VIN"].ToString();
                        if (DBNull.Value != reader["ChassisNo"]) v.chassisno = reader["ChassisNo"].ToString();
                        if (DBNull.Value != reader["Colour"]) v.colour = reader["Colour"].ToString();
                        if (DBNull.Value != reader["VehicleYear"]) v.VehicleYear = int.Parse(reader["VehicleYear"].ToString());
                    }
                    catch (Exception)
                    {
                        
                       
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return v;
        }

        /// <summary>
        /// This function adds drivers (authorized, excepted, excluded) to a Risk
        /// </summary>
        /// <param name="vin">Risk's VIN </param>
        /// <param name="PersonID">Person ID of person to be added </param>
        /// <param name="type">Type of driver (1 is authorized driver, 2 is excepted driver, 3 is excluded driver)</param>
        /// <returns></returns>
        public static bool addDriversToVehicle(string chassis, int PersonID, int type, int policyID)
        {
            return addDriversToVehicle(chassis, PersonID, type, policyID, Constants.user.ID);
        }

        /// <summary>
        /// This function adds drivers (authorized, excepted, excluded) to a Risk
        /// </summary>
        /// <param name="vin">Risk's VIN </param>
        /// <param name="PersonID">Person ID of person to be added </param>
        /// <param name="type">Type of driver (1 is authorized driver, 2 is excepted driver, 3 is excluded driver)</param>
        /// <param name="uid">user id that is calling this function</param>
        /// <returns></returns>
        public static bool addDriversToVehicle(string chassis, int PersonID, int type, int policyID, int uid, string Trn = "")
        {
            bool result = false;
            int DriverID = 0;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                //check if person is a driver already and gets driver id- otherwise add driver

                string query = "EXECUTE GetDriverFromPersonID " + PersonID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "driver", new Driver());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    if ((bool)reader["result"]) { DriverID = int.Parse(reader["did"].ToString()); }
                }
                reader.Close();

                if (DriverID == 0)
                {
                    //adding a driver
                    reader = sql.QuerySQL("EXECUTE AddDriver " + PersonID + "," + uid, "create", "driver", new Driver(PersonModel.GetPerson(PersonID)));

                    while (reader.Read())
                    {
                        DriverID = int.Parse(reader["ID"].ToString());
                    }
                    reader.Close();
                }

                query = "EXECUTE AddDriversToVehicle '" + chassis + "'," + policyID + "," + DriverID + "," + type + "," + uid;
                reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
                //reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function tests the existence of a Risk in the system, using the VIN
        /// </summary>
        /// <param name="Risk">The Risk to test/search for</param>
        /// <returns>Risk</returns>
        public static Vehicle TestVehicleWithChassis(Vehicle Risk, int vehID = 0)
        {
            if (Risk != null)
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    string query = "EXEC GetVehicleWithChassis '" + Constants.CleanString(Risk.chassisno) + "'," + vehID; ;
                    SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
                    //SqlDataReader reader = sql.QuerySQL(query);
                    while (reader.Read())
                    {
                        if ((bool)reader["result"])
                        {
                            //THERE IS A RESULT
                            if ((bool)reader["vehicle"])
                            {
                                //THE Risk IS ALREADY IN THE SYSTEM
                                Risk.ID = int.Parse(reader["VehicleID"].ToString());
                            }
                        }

                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            return Risk;
        }

        /// <summary>
        /// this function tests if a vin is available for use
        /// </summary>
        /// <param name="vin"></param>
        /// <param name="vehID"></param>
        /// <returns></returns>
        public static int TestVehicleVIN(string vin, int vehID = 0)
        {
            int result = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetVehicleWithVIN '" + Constants.CleanString(vin) + "'," + vehID;

                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());

                while (reader.Read())
                {
                    result = int.Parse(reader["VehicleID"].ToString());
                }

                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function deletes drivers (authorized, excepted, excluded) from a Risk
        /// </summary>
        /// <param name="vin">Risk's VIN </param>
        /// <param name="PersonID">Person to be deleted </param>
        /// <param name="type">Type of driver (1 is authorized driver, 2 is excepted driver, 3 is excluded driver)</param>
        public static void deleteDrivers(string chassis, string perId, string type, string polId)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DeleteDriversFromVehicle '" + Constants.CleanString(chassis) + "',"
                                                                 + int.Parse(perId) + ","
                                                                 + int.Parse(type) + ","
                                                                 + int.Parse(polId) + ","
                                                                 + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "policy", new Policy(int.Parse(polId)), "remove drivers");
                //SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function gets a list of drivers based on the type
        /// </summary>
        /// <param name="VehicleId">valid id of a Risk in the database</param>
        /// <param name="type">either 1,2,3 (1 is authorized drivers, 2 is excepted drivers, 3 is excluded drivers)</param>
        /// <returns>list of drivers</returns>
        public static List<Driver> GetDriverList(int VehicleId, int policyid, int type)
        {
            List<Driver> drivers = new List<Driver>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC GetMyDrivers " + VehicleId + "," + policyid + "," + type;
                SqlDataReader reader = sql.QuerySQL(query, "read", "driver", new Driver());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    Driver driver = new Driver();
                    Person person = new Person();

                    person.PersonID = int.Parse(reader["PersonID"].ToString());
                    person.TRN = reader["TRN"].ToString();
                    driver.relation = reader["Relation"].ToString();
                    if (DBNull.Value != reader["FName"]) person.fname = reader["FName"].ToString();
                    if (DBNull.Value != reader["LName"]) person.lname = reader["LName"].ToString();
                    driver.person = person;


                    drivers.Add(driver);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return drivers;
        }

        /// <summary>
        /// This function gets the TRN of the main driver of a Risk
        /// </summary>
        /// <param name="VehId">Risk's ID</param>
        /// <returns>integer</returns>
        public static string GetMainDriver(int VehId)
        {
            string TRN = null;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE GetMainDriver " + VehId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "driver", new Driver());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    TRN = reader["TRN"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return TRN;

        }

        /// <summary>
        /// This function udates mainDriver
        /// </summary>
        /// <param name="vehId">Risk's ID</param>
        /// <param name="trn">person TRN</param>
        public static void UpdateMainDriver(int vehID, string trn, int perId)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC updateMainDriver " + vehID + ",'" + trn + "'," + perId;
                SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function Adds the relation of a driver, to the main driver of a Risk
        /// </summary>
        /// <param name="vin">Risk's VIN</param>
        /// <param name="personId">Person Id of the driver</param>
        /// <param name="relation">The driver's relation to the main-driver</param>
        public static void AddDriverRelation(string chassis, int personId, string relation, int polID)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXECUTE AddDriverRelation " + personId + ",'"
                                                            + Constants.CleanString(chassis) + "',"
                                                            + polID + ",'"
                                                            + Constants.CleanString(relation) + "',"
                                                            + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "create", "driver", new Driver(PersonModel.GetPerson(personId)));
                //SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }

        }


        /// <summary>
        /// This function cancels a Risk under a particular policy
        /// </summary>
        /// <param name="vehId">Risk's VIN</param>
        public static void CancelVehicleToPolicyAssoc(int vehId, int polid, DateTime start, DateTime end)
        {
            int PolicyId = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE CancelVehicleToPolicy  " + vehId + ","
                                                                 + polid + ",'"
                                                                 + start.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                                 + end.ToString("yyyy-MM-dd HH:mm:ss") + "',"
                                                                 + Constants.user.ID;

                SqlDataReader reader = sql.QuerySQL(query, "update", "vehicle", new Vehicle(vehId));
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    if (!(bool)reader["result"]) // If the policy from which the Risk has been cancelled, has no more active Vehicles, then the policy is cancelled
                        PolicyId = int.Parse(reader["policyId"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }

            if (PolicyId != 0)
            {
                if (sql.ConnectSQL())
                {

                    int PolicyCreator = 0;
                    Notification note = new Notification();
                    note.NoteTo = new List<User>();

                    string query = "EXECUTE CancelPolicy " + PolicyId + "," + Constants.user.ID;
                    SqlDataReader reader = sql.QuerySQL(query, "update", "policy", new Policy(PolicyId));
                    reader.Close();
                    sql.DisconnectSQL();

                    //A notification is sent to the creator of the policy, upon cancellation
                    PolicyCreator = PolicyModel.GetPolicyCreator(PolicyId);

                    if (PolicyCreator != Constants.user.ID)
                    {
                        note.NoteTo.Add(new User(PolicyCreator));
                        note.NoteFrom = new User(Constants.user.ID);
                        note.heading = "Policy Cancelled";
                        note.content = "The Policy: PolicyId " + PolicyId + " was cancelled in the system";

                        NotificationModel.CreateNotification(note);
                    }
                }
            }
        }

        /// <summary>
        /// This function checks if a Risk is under a particular policy
        /// </summary>
        /// <param name="polId">Policy Id</param>
        /// <param name="vehId">Risk Id</param>
        public static bool isVehUnderThisPolicy(int polId, int vehId)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXECUTE isVehUnderThisPolicy " + polId + "," + vehId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
                //SqlDataReader reader = sql.QuerySQL(query);

                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function checks if a Risk is under any policy that is not the one stipulated, under a particular timeframe
        /// </summary>
        /// <param name="vehId">Risk Id</param>
        public static bool isVehUnderActivePolicy(int vehId, DateTime startDate, DateTime endDate, int PolId = 0)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXECUTE IsVehicleUnderActivePolicy '" + startDate.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                                        + endDate.ToString("yyyy-MM-dd HH:mm:ss") + "',"
                                                                        + vehId + ","
                                                                        + PolId;

                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function returns the usage of a Risk , under a given policy
        /// </summary>
        /// <param name="vehId">Risk Id</param>
        /// <param name="polId">Policy Id</param>
        /// <returns> integer (usage ID)</returns>
        public static int GetUsage(int vehId, int polId)
        {
            int use = 0;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXECUTE GetVehUsage " + polId + "," + vehId;

                SqlDataReader reader = sql.QuerySQL(query, "read", "usage", new Usage());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    use = int.Parse(reader["VehicleUsage"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return use;
        }

        /// <summary>
        /// This function returns a list of vehicle makes
        /// used by JavaScript for autocomplete
        /// </summary>
        public static List<string> GetVehicleMakes()
        {
            List<string> result = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE GetVehicleMakes";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result.Add(reader["Make"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function returns a list of vehicle models
        /// </summary>
        public static List<string> GetVehicleModels()
        {
            List<string> result = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE GetVehicleModels";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result.Add(reader["Model"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function returns a list of vehicle model types
        /// </summary>
        public static List<string> GetVehicleModelTypes()
        {
            List<string> result = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE GetVehicleModelTypes";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result.Add(reader["ModelType"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function returns a list of vehicle body types
        /// </summary>
        public static List<string> GetVehicleBodyTypes()
        {
            List<string> result = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE GetVehicleBodyTypes";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result.Add(reader["BodyType"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function is in use on short vehicle search page partial (DONT REMOVE)
        /// </summary>
        /// <param name="vehid"></param>
        /// <returns></returns>
        public static bool HasCoverNoteHistory(int vehid)
        {
            bool result = false;
            if (IsVehicleUnderPolTimeFrame(vehid) && (GetCurrentPolicy(new Vehicle(vehid)) != 0)) result = HasCoverNoteHistory(vehid, GetCurrentPolicy(new Vehicle(vehid)));
            return result;
        }

        /// <summary>
        /// This function tests if a vehicle has past cover notes
        /// </summary>
        public static bool HasCoverNoteHistory(int vehId, int PolId)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE HasCoverNoteHistory " + vehId + "," + PolId;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
                //SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }
        #region new old
        /// <summary>
        /// this function which returns the company id of any company that is covering a vehicle
        /// </summary>
        /// <param name="vehID">vehicle id</param>
        /// <returns>company id</returns>
        //public static int GetCompanyUsingVehicleID(int vehID)
        //{
        //    sql sql = new sql();
        //    string query = "";
        //    int compID = 0;

        //    if (sql.ConnectSQL())
        //    {
        //        query = "EXEC GetCompanyUsingVehicleID "+ vehID;

        //        SqlDataReader reader = sql.QuerySQL(query, "read", "company", new Company());
        //        //SqlDataReader reader = sql.QuerySQL(query);
        //        while (reader.Read())
        //        {
        //            compID = (int) reader["ID"];
        //        }
        //    }
        //    return compID;
        //}
        #endregion
        /// <summary>
        /// thie function cancels all vehicle/policy relationships of policies that are active under a specified time frame
        /// </summary>
        /// <param name="vehId">vehicle id</param>
        /// <param name="polid">policy id of the policy not to be affected</param>
        /// <param name="start">start time stamp</param>
        /// <param name="end">end time stamp</param>
        public static void CancelVehicleUnderPolicy(int vehId, int polid, DateTime start, DateTime end, int ID = 0)
        {


            sql sql = new sql();
            int userID = 0;
            if (ID != 0) userID = ID; else userID = Constants.user.ID;
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE CancelVehicleToPolicy  " + vehId + ","
                                                                 + polid + ",'"
                                                                 + start.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                                 + end.ToString("yyyy-MM-dd HH:mm:ss") + "',"
                                                                 + userID;

                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());
                //SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }

        }


        /// <summary>
        /// this function checks if the Vehicle Registration No is is in the system
        /// </summary>
        /// <param name="RegNo">Registration Number</param>
        public static bool DoesRegNoExist(string RegNo = null, int ID = 0)
        {

            bool exists = false;
            sql sql = new sql();

            if (sql.ConnectSQL())
            {
                string query = "EXECUTE DoesRegNoExist  '" + RegNo + "'";
                if (ID != 0) query += "," + ID;

                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());

                while (reader.Read())
                {
                    exists = (bool)reader["Result"];
                }

                reader.Close();
                sql.DisconnectSQL();
            }

            return exists;

        }


        /// <summary>
        /// this function gets the number of doubled insured vehicles for a company
        /// </summary>
        /// <param name="company">The company</param>
        public static int GetNumberOfDoubleInsured(Company company)
        {

            int count = 0;
            sql sql = new sql();

            if (sql.ConnectSQL())
            {
                string query = "EXECUTE GetNumberOfDoubleInsured " + company.CompanyID;
                SqlDataReader reader = sql.QuerySQL(query);

                while (reader.Read())
                {
                    count = int.Parse(reader["NumOfRows"].ToString());
                }

                reader.Close();
                sql.DisconnectSQL();
            }

            return count;
        }

        /// <summary>
        /// this function gets doubled insured vehicles for a company
        /// </summary>The company's ID</param>
        public static List<Vehicle> GetDoubleInsuredVehicles(int compid)
        {
            List<Vehicle> vehicles = new List<Vehicle>();
            List<int> IDs = new List<int>();
            sql sql = new sql();

            if (sql.ConnectSQL())
            {
                string query = "EXEC GetDoubleInsuredVehicle " + compid;
                SqlDataReader reader = sql.QuerySQL(query, "read", "vehicle", new Vehicle());

                while (reader.Read())
                {
                    IDs.Add(int.Parse(reader["VehicleID"].ToString()));
                }

                foreach (int id in IDs)
                {
                    vehicles.Add(GetVehicle(id));
                }
            }

            return vehicles;
        }

        public static bool IsVehUnderActiveNote(int vehID, int polID)
        {
            bool active = false;
            sql sql = new sql();
            string query = "";
            if (sql.ConnectSQL())
            {
                query = "EXEC IsVehUnderActiveNote " + vehID + "," + polID;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    active = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return active;
        }

        public static List<Vehicle> GetCompanyVehicles(int compID, DateTime start, DateTime end, string typeOfDate = null)
        {
            List<Vehicle> vehicles = new List<Vehicle>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                if (typeOfDate == null) typeOfDate = "created";
                string query = "EXEC GetCompanyVehicles " + compID + ",'" + typeOfDate + "'";
                if ((start == DateTime.Parse("1969-12-31 00:00:00") || (start == DateTime.Parse("1969-12-31 19:00:00")) && (end == DateTime.Parse("1969-12-31 00:00:00") || end == DateTime.Parse("1969-12-31 19:00:00"))))
                {
                    query += ",'" + null + "','" + null + "'";
                }
                else
                    query += ",'" + start.ToString("yyyy-MM-dd HH:mm:ss") + "','" + end.ToString("yyyy-MM-dd HH:mm:ss") + "'";

                SqlDataReader reader = sql.QuerySQL(query);
                //Vehicle vehicle = new Vehicle();
                while (reader.Read())
                {
                    //vehicle.ID = int.Parse(reader["ID"].ToString());
                    //vehicle.chassisno = reader["ChassisNo"].ToString();
                    //vehicle.VehicleRegNumber = reader["VehicleRegistrationNo"].ToString();
                    //vehicle.make = reader["Make"].ToString();
                    //vehicle.VehicleModel = reader["Model"].ToString();
                    //vehicle.vehicle_under_pol = IsVehicleUnderPolicyAtMyCompanyCurrent(int.Parse(reader["ID"].ToString()));
                    //vehicle.vehicle_has_cover_note_history = HasCoverNoteHistory(int.Parse(reader["ID"].ToString()));
                    //vehicle.vehicle_has_active_cover_note = IsVehUnderActiveNote(int.Parse(reader["ID"].ToString()), 0);
                    //vehicle.imported = (bool)reader["imported"];

                    vehicles.Add(new Vehicle(
                                                int.Parse(reader["ID"].ToString()),
                                                reader["ChassisNo"].ToString(),
                                                reader["VehicleRegistrationNo"].ToString(),
                                                reader["Make"].ToString(),
                                                reader["Model"].ToString(),
                                                false,//IsVehicleUnderPolicyAtMyCompanyCurrent(int.Parse(reader["ID"].ToString())),
                                                false,//HasCoverNoteHistory(int.Parse(reader["ID"].ToString())),
                                                false,//IsVehUnderActiveNote(int.Parse(reader["ID"].ToString()), 0),
                                                (bool)reader["imported"]
                                              )
                                );

                    //vehicles.Add(vehicle);
                    //vehicle = new Vehicle();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return vehicles;
        }

        public static void MarkVehicleImported(Vehicle veh)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                try
                {
                    string query = "EXEC MarkVehicleImported " + veh.ID;
                    SqlDataReader reader = sql.QuerySQL(query);
                    reader.Close();
                    sql.DisconnectSQL();
                }
                catch (Exception)
                {
                    
                }
               
            }
        }


        public static List<Company> CheckForDuplicate(string chassis)
        {
            List<Company> companies = new List<Company>();
            Company comp = new Company();
            comp.duplicate = false;

            if (!string.IsNullOrEmpty(chassis.Trim()))
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    string query = "EXEC CheckVehicleDuplicate '" + chassis + "'";
                    SqlDataReader reader = sql.QuerySQL(query);

                    while (reader.Read())
                    {
                        if (int.Parse(reader["vnum"].ToString()) > 1)
                        {
                            comp.duplicate = true;
                            comp.CompanyID = int.Parse(reader["CompanyID"].ToString());
                            comp.CompanyName = reader["CompanyName"].ToString();
                        }
                        else
                        {
                            comp.duplicate = false;
                        }

                        companies.Add(comp);
                    }
                }

            }

            return companies;
        }

        public static List<Vehicle> getDuplicateVehicles(string chassis)
        {
            List<Vehicle> vehicles = new List<Vehicle>();
            Vehicle v;

            if (!string.IsNullOrEmpty(chassis.Trim()))
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    string query = "EXEC getDuplicateVehicles '" + chassis + "'";
                    SqlDataReader reader = sql.QuerySQL(query);

                    while (reader.Read())
                    {
                        v = new Vehicle();
                        v.ID = int.Parse(reader["ID"].ToString());
                        v.chassisno = reader["ChassisNo"].ToString();
                        v.make = reader["Make"].ToString();
                        v.VehicleModel = reader["Model"].ToString();
                        v.VehicleYear = int.Parse(reader["VehicleYear"].ToString());
                        v.VehicleRegNumber = reader["VehicleRegistrationNo"].ToString();
                        v.colour = reader["Colour"].ToString();
                        v.engineNo = reader["EngineNo"].ToString();
                        v.createdOn = DateTime.Parse(reader["CreatedOn"].ToString());
                        v.createdBy = int.Parse(reader["CreatedBy"].ToString());
                        vehicles.Add(v);

                    }
                }

            }

            return vehicles;
        }

    }
}

