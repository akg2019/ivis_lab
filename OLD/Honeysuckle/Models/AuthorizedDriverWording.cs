﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class AuthorizedDriverWording
    {
        public int ID { get; set; }
        public string wording { get; set; }
        public Company company { get; set; }
        public bool can_delete { get; set; }

        public AuthorizedDriverWording() { }
        public AuthorizedDriverWording(int ID)
        {
            this.ID = ID;
        }
        public AuthorizedDriverWording(string wording)
        {
            this.wording = wording;
        }
        public AuthorizedDriverWording(int ID, string wording)
        {
            this.ID = ID;
            this.wording = wording;
        }
        public AuthorizedDriverWording(int ID, string wording, Company company)
        {
            this.ID = ID;
            this.wording = wording;
            this.company = company;
        }
        public AuthorizedDriverWording(int ID, string wording, Company company, bool can_delete)
        {
            this.ID = ID;
            this.wording = wording;
            this.company = company;
            this.can_delete = can_delete;
        }
    }
    public class AuthorizedDriverWordingModel
    {
        /// <summary>
        /// this function returns a specified insurer's list of authorized driver wordings
        /// </summary>
        /// <param name="company">an insuerer as a company object with a valid id</param>
        /// <returns>a list of authorized driver wordings</returns>
        public static List<AuthorizedDriverWording> get_all_wording_for_insurer(Company company)
        {
           List<AuthorizedDriverWording> wordings = new List<AuthorizedDriverWording>();
                sql sql = new sql();
            
            try
            {

                
                if (sql.ConnectSQL())
                {
                    SqlDataReader reader = sql.QuerySQL("EXEC get_all_wording_for_insurer " + company.CompanyID);
                    while (reader.Read())
                    {
                        wordings.Add(new AuthorizedDriverWording(int.Parse(reader["id"].ToString()),
                                                                 reader["wording"].ToString(),
                                                                 new Company(int.Parse(reader["compid"].ToString()), reader["cname"].ToString()),
                                                                 can_be_deleted(int.Parse(reader["id"].ToString()))));
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }

            }
            catch (Exception e)
                
            { 
            
            }
            return wordings;
        }

        /// <summary>
        /// this function returns a specified wording for view
        /// </summary>
        /// <param name="id">valid id of a wording in the IVIS database</param>
        /// <returns>a fully realized authorized wording object</returns>
        public static AuthorizedDriverWording get_wording(int id)
        {
            AuthorizedDriverWording word = new AuthorizedDriverWording(id);
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC get_wording " + id);
                while (reader.Read())
                {
                    word.ID = id;
                    word.wording = reader["wording"].ToString();
                    word.company = new Company(int.Parse(reader["compid"].ToString()), reader["cname"].ToString());
                    word.can_delete = can_be_deleted(id);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return word;
        }

        /// <summary>
        /// this function returns the wording that has been selected for a vehicle/policy combination in the system
        /// </summary>
        /// <param name="policy">the policy object with a valid id</param>
        /// <param name="vehicle">the vehicle object with a valid id that is under the policy given</param>
        /// <returns>the string (content) of the wording selected currently</returns>
        public static string get_wording(Policy policy, Vehicle vehicle)
        {
            string wording = "";
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC get_wording_under_vehicle " + policy.ID + "," + vehicle.ID);
                while (reader.Read())
                {
                    if (reader["word"] != DBNull.Value) wording = reader["word"].ToString();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return wording;
        }

        /// <summary>
        /// this function returns the fully realized authorized driver wording object for a vehicle on a policy
        /// </summary>
        /// <param name="polID">integer of the policy on the database</param>
        /// <param name="vehID">integer of the vehicle on the database that is under the policy</param>
        /// <returns>Authorized Driver Wording data object filled with data</returns>
        public static AuthorizedDriverWording get_wording(int polID, int vehID)
        {
            AuthorizedDriverWording wording = new AuthorizedDriverWording();
            string word = "";
            int id = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC get_wording_under_vehicle " + polID + "," + vehID);
                while (reader.Read())
                {
                    if (reader["word"] != DBNull.Value)  word = reader["word"].ToString();
                    if (reader["id"] != DBNull.Value) id = int.Parse(reader["id"].ToString());
                }

                wording.wording = word;
                wording.ID = id;

                reader.Close();
                sql.DisconnectSQL();
            }
            return wording;
        }

        /// <summary>
        /// this function gets the database id of the authorized driver wording in question
        /// </summary>
        /// <param name="adw">this object requires the company object with a valid id as well as the content text of the wording</param>
        /// <returns>the authorized driver wording object with the databse id of that record</returns>
        public static AuthorizedDriverWording get_wording(AuthorizedDriverWording adw)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC get_wording_by_word '" + adw.wording + "'," + adw.company.CompanyID);
                while (reader.Read())
                {
                    if ((bool)reader["result"]) adw.ID = int.Parse(reader["id"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return adw;
        }

        /// <summary>
        /// this function creates a wording in the database
        /// </summary>
        /// <param name="adw">an authorized wording driver object with a corelating company id, and woding text</param>
        /// <returns>the authorized wording driver object with the recorded database id</returns>
        public static AuthorizedDriverWording create_wording(AuthorizedDriverWording adw)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC create_wording '" + Constants.CleanString(adw.wording) + "'," + adw.company.CompanyID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "create", "authorizeddriverwording", adw);
                while (reader.Read())
                {
                    adw.ID = int.Parse(reader["id"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return adw;
        }

        /// <summary>
        /// this function tests if a specified authorized wording driver record can be deleted
        /// </summary>
        /// <param name="id">the database id of the authorized wording driver record</param>
        /// <returns>boolean stating whether the record can be deleted or not</returns>
        private static bool can_be_deleted(int id)
        {
            bool can_delete = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC authorized_drivers_wording_can_be_deleted " + id);
                while (reader.Read())
                {
                    can_delete = (bool)reader["result"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return can_delete;
        }

        /// <summary>
        /// this function deletes the record from the database
        /// </summary>
        /// <param name="adw"></param>
        /// <returns></returns>
        public static bool delete(AuthorizedDriverWording adw)
        {
            bool result = false;
            if (can_be_deleted(adw.ID))
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    //SqlDataReader reader = sql.QuerySQL("EXEC auth_word_delete " + adw.ID, "delete", "authorizeddriverwording", adw);
                    SqlDataReader reader = sql.QuerySQL("EXEC auth_word_delete " + adw.ID);
                    result = true;
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            return result;
        }

        /// <summary>
        /// this function attributes a authorized wording driver record to a specified vehicle of a policy.
        /// </summary>
        /// <param name="adwID">authorized wording id</param>
        /// <param name="polID">policy id</param>
        /// <param name="vehID">vehicle id</param>
        public static void updateRiskADWording(int adwID, int polID, int vehID)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC UpdateRiskADWording "  + adwID + ","
                                                            + polID + ","
                                                            + vehID;

                //SqlDataReader reader = sql.QuerySQL(query, "create", "authorizeddriverwording", new AuthorizedDriverWording());
                SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        public static void updateAuthorizedWording(int ID, string wording)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC updateAuthorizedDriverWording " + ID + ",'" + wording + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

    }
}
	
