﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class Phone
    {
        public int PhoneNumberId { get; set; }

        [RegularExpression("([(][0-9]{3}[)][0-9]{3}[-][0-9]{4})", ErrorMessage = "Invalid phone number format: only numbers, hyphens, brackets and the plus sign (+) allowed")]
        public string PhoneNumber { get; set; }
        public bool primary { get; set; }
        [RegularExpression("([0-9-()+ ]*)", ErrorMessage = "Invalid phone Extension format: only numbers, hyphens, brackets and the plus sign (+) allowed")]
        public string Extension {get; set; }
        public string name { get; set; } //for company list
      

        public Phone() { }
        public Phone(string phoneNum)
        {
            this.PhoneNumber = phoneNum;
        }
        
        public Phone( int PhoneId, string PhoneNum)
        {
            this.PhoneNumberId = PhoneId;
            this.PhoneNumber = PhoneNum;
        }

        public Phone(int PhoneId, string PhoneNum, string extension,  bool primary)
        {
            this.PhoneNumberId = PhoneId;
            this.PhoneNumber = PhoneNum;
            this.Extension = extension;
            this.primary = primary;
        }

        public Phone(string PhoneNum, string extension)
        {
            this.PhoneNumber = PhoneNum;
            this.Extension = extension;
          
        }

        public Phone(string PhoneNum, string extension, bool primary)
        {
            this.PhoneNumber = PhoneNum;
            this.Extension = extension;
            this.primary = primary;
        }

        public Phone(int PhoneId, string PhoneNum, string extension)
        {
            this.PhoneNumberId = PhoneId;
            this.PhoneNumber = PhoneNum;
            this.Extension = extension;
            
        }
    }

    public class PhoneModel
    {

        /// <summary>
        /// This function adds phone numbers to the system (if it isn't already there) and then associates them with a company
        /// </summary>
        /// <param name="company">The complete company Model, with its phone numbers</param>
        public static void AddPhoneNumToComp(Company company)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                if (company.phoneNums != null)
                    foreach (Phone phone in company.phoneNums)
                    {
                        string Query = "EXEC AddPhoneNumberToCompany " + company.CompanyID + ",'"
                                                                       + Constants.CleanString(phone.PhoneNumber) + "','"
                                                                       + Constants.CleanString(phone.Extension) + "','"
                                                                       + phone.primary + "',"
                                                                       + Constants.user.ID;


                        SqlDataReader reader = sql.QuerySQL(Query, "create", "phone", phone);
                        reader.Close();
                    }

                sql.DisconnectSQL();
            }
        }


        /// <summary>
        /// This function adds phone numbers to the system (if it isn't already there) and then associates them with a user of a company
        /// </summary>
        /// <param name="company">The complete company Model, with its phone numbers</param>
        public static void AddPhoneToCompUser(Company company, List<User> user = null)
        {
            int UserCount = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                if (company.phoneNums != null)
                    foreach (Phone phone in company.phoneNums)
                    {
                        string Query = "EXEC AddPhoneNumberToCompUsers " + company.CompanyID + ",'"
                                                                       + Constants.CleanString(phone.PhoneNumber) + "','"
                                                                       + Constants.CleanString(phone.Extension) + "',"
                                                                       + phone.primary + "," +
                                                                       + user[UserCount].ID + ","+
                                                                       + Constants.user.ID;


                        SqlDataReader reader = sql.QuerySQL(Query, "create", "phone", phone);
                        reader.Close();
                        UserCount++;
                    }

                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function returns all the phone Numbers associated to a company
        /// </summary>
        /// <param name="company">Company model</param>
        /// <returns>list of phone numbers</returns>
        public static List<Phone> GetCompanyPhones(Company company)
        {
            List<Phone> phoneNums = new List<Phone>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string Query = "EXEC GetCompanyPhoneNums " + company.CompanyID;
                SqlDataReader reader = sql.QuerySQL(Query, "read", "phone", new Phone());

                while (reader.Read())
                {
                    
                    Phone phone = new Phone(int.Parse(reader["PID"].ToString()), reader["PhoneNo"].ToString(),
                                            reader["Ext"].ToString());
                   
                    if (DBNull.Value == reader["primary"]) phone.primary = false;
                    else phone.primary = (bool)reader["primary"];
                    phoneNums.Add(phone);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return phoneNums;
        }


        /// <summary>
        /// This function removes the association of all phones from a specific company in the system
        /// </summary>
        /// <param name="company">The company for which the phone numbers are to be dropped</param>
        public static void DropPhonesFromCompany(Company company)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC [DropPhonesNumsFromComp] " + company.CompanyID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "phone", new Phone());
                reader.Close();
                sql.DisconnectSQL();
            }
        }
        #region old
        /// <summary>
        /// This function returns a phone number model (for insurance companies/brokers/agents
        /// </summary>
        /// <param name="id">phone id</param>
        /// <returns>Phone model</returns>
        //public static Phone GetPhone(int id)
        //{
        //    Phone phoneNums = new Phone();
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {

        //        string Query = "EXEC GetPhone " + id;
        //        SqlDataReader reader = sql.QuerySQL(Query, "read", "phone", new Phone());

        //        while (reader.Read())
        //        {
        //            phoneNums = new Phone(reader["PhoneNo"].ToString(),
        //                                    reader["Ext"].ToString());
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return phoneNums;
        //}

        /// <summary>
        /// This function returns a phone number model (for regular companies)
        /// </summary>
        /// <param name="id">phone id</param>
        /// <returns>Phone model</returns>
        //public static Phone GetCompPhone(int id, int compID)
        //{
        //    Phone phone = new Phone();
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {

        //        string Query = "EXEC GetCompPhone " + id +","+ compID;
        //        SqlDataReader reader = sql.QuerySQL(Query, "read", "phone", new Phone());

        //        while (reader.Read())
        //        {
        //            phone = new Phone(reader["PhoneNo"].ToString(),
        //                                    reader["Ext"].ToString());

        //            if (DBNull.Value == reader["primary"]) phone.primary = false;
        //            else phone.primary = (bool)reader["primary"];
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return phone;
        //}
        #endregion
        /// <summary>
        /// this function associates all phone numbers in a user object to that user
        /// </summary>
        /// <param name="user"></param>
        public static void AddPhoneToPerson(User user)
        {
            sql sql = new sql();

            if (sql.ConnectSQL() && user.employee.person.phoneNums != null)
            {
                foreach(Phone phone in user.employee.person.phoneNums){
                    string query = "EXEC AddPhoneNumberToPeople " + user.employee.person.PersonID + ",'"
                                                                  + Constants.CleanString(phone.PhoneNumber) + "','"
                                                                  + Constants.CleanString(phone.Extension) + "',"
                                                                  + phone.primary + ","
                                                                  + Constants.user.ID;

                    SqlDataReader reader = sql.QuerySQL(query, "create", "phone", phone);
                    reader.Close();
                }

                sql.DisconnectSQL();

            }
        }

        /// <summary>
        /// this function retrieves all the phone numbers associated to a specific person
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        public static List<Phone> GetMyPhone(Person person)
        {
            List<Phone> phones = new List<Phone>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string Query = "EXEC GetMyPhone " + person.PersonID;
                SqlDataReader reader = sql.QuerySQL(Query, "read", "phone", new Phone());
                while (reader.Read())
                {
                    phones.Add(new Phone(int.Parse(reader["PID"].ToString()), reader["PhoneNo"].ToString(),reader["Ext"].ToString(), (bool)reader["Primary"]));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return phones;
        }

        /// <summary>
        /// this function deletes the association of all phones to a specified person
        /// </summary>
        /// <param name="person"></param>
        public static void DropPhoneFromPerson(Person person)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DropPhoneFromPerson " + person.PersonID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "phone", new Phone());
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function updates the phone number for a specific person
        /// </summary>
        /// <param name="person"></param>
        /// <param name="phone"></param>
        public static void EditPersonPhone(Person person, Phone phone)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC EditUserPhone '" + Constants.CleanString(phone.PhoneNumber) + "','"
                                                      + Constants.CleanString(phone.Extension) + "',"
                                                      + person.PersonID + ","
                                                      + Constants.user.ID;

                SqlDataReader reader = sql.QuerySQL(query, "update", "phone", phone);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function retrieves the phone number based on the content of a phone number
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static Phone GetPhone(Phone phone)
        {
            Phone returnPhone = new Phone();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetPhoneNumber '" + Constants.CleanString(phone.PhoneNumber) + "','"
                                                       + Constants.CleanString(phone.Extension) + "'";

                SqlDataReader reader = sql.QuerySQL(query, "read", "phone", new Phone());

                while (reader.Read())
                {
                    returnPhone = new Phone(int.Parse(reader["PID"].ToString()), reader["PhoneNo"].ToString(), reader["Ext"].ToString());
                }

                reader.Close();
                sql.DisconnectSQL();
            }

            return returnPhone;
        }


    }
}