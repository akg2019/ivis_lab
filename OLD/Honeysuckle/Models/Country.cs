﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;

namespace Honeysuckle.Models
{
    public class Country
    {

        public int CountryID { get; set; }

        [RegularExpression("[a-zA-Z](([a-zA-Z0-9.& '-])*[a-zA-Z])?", ErrorMessage = "Incorrect country name format (some symbols are not allowed)")]
        public string CountryName { get; set; }

        public Country() { }
        public Country(int CountryID1, string CountryName1)
        {
            this.CountryID = CountryID1;
            this.CountryName = CountryName1;

        }
        public Country(string CountryName)
        {
            this.CountryName = CountryName;
        }
        
    }
        public class CountryModel
        {

            /// <summary>
            /// this function returns a list of countries in the system
            /// </summary>
            /// <returns>list of strings</returns>
            public static List<string> GetCountryList()
            {
                List<string> result = new List<string>();
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    SqlDataReader reader = sql.QuerySQL("EXECUTE GetCountries", "read", "country", new Country());
                    while (reader.Read())
                    {
                        result.Add(reader["Country"].ToString());
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
                return result;
            }
        }
    }

