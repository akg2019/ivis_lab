﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class MappingData
    {
        public int id { get; set; }
        public string table { get; set; }
        public int refid { get; set; }
        public string data { get; set; }
        public string correct_data { get; set; }
        public Company company { get; set; }
        public bool maplock { get; set; }

        public MappingData() { }
        public MappingData(int id)
        {
            this.id = id;
        }
        public MappingData(string table, string data)
        {
            this.table = table;
            this.data = data;
        }
        public MappingData(int id, string table, string data)
        {
            this.id = id;
            this.table = table;
            this.data = data;
        }
        public MappingData(int id, string table, int refid, string data)
        {
            this.id = id;
            this.table = table;
            this.refid = refid;
            this.data = data;
        }
        public MappingData(int id, string table, int refid, string data, bool maplock)
        {
            this.id = id;
            this.table = table;
            this.refid = refid;
            this.data = data;
            this.maplock = maplock;
        }
        public MappingData(int id, string table, int refid, string data, Company company)
        {
            this.id = id;
            this.table = table;
            this.refid = refid;
            this.data = data;
            this.company = company;
        }
        public MappingData(int id, string table, int refid, string data, Company company, bool maplock)
        {
            this.id = id;
            this.table = table;
            this.refid = refid;
            this.data = data;
            this.company = company;
            this.maplock = maplock;
        }
    }
    public class MappingDataModel
    {
        /// <summary>
        /// this function adds mapping data to the database
        /// </summary>
        /// <param name="map"></param>
        /// <returns></returns>
        public static MappingData AddMappingData(MappingData map)
        {
            return AddMappingData(map, Constants.user.ID);
        }

        /// <summary>
        /// this function adds mapping data to the database
        /// this version is to be used by the json webservice
        /// </summary>
        /// <param name="map"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static MappingData AddMappingData(MappingData map, int uid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC AddMappingData '"  + Constants.CleanString(map.table) + "','"
                                                        + Constants.CleanString(map.data) + "',"
                                                        + CompanyModel.GetMyCompany(new User(uid)).CompanyID + ","
                                                        + uid;
                SqlDataReader reader = sql.QuerySQL(query, "create", "mappingdata", map);
                while (reader.Read())
                {
                    map.id = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return map;
        }

        /// <summary>
        /// this function returns all the mapping data of the user's logged in company
        /// </summary>
        /// <returns></returns>
        public static List<MappingData> GetAllMappingData()
        {
            List<MappingData> maps = new List<MappingData>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                int cid;
                if (UserGroupModel.AmAnAdministrator(Constants.user)) cid = 0;
                else cid = CompanyModel.GetMyCompany(Constants.user).CompanyID;
                SqlDataReader reader = sql.QuerySQL("EXEC GetAllMappingData " + cid);
                while (reader.Read())
                {
                    MappingData map = new MappingData(int.Parse(reader["ID"].ToString()), reader["table"].ToString(), reader["data"].ToString());
                    if (!(DBNull.Value.Equals(reader["refid"]))) map.refid = int.Parse(reader["refid"].ToString());
                    map.maplock = (bool)reader["lock"];
                    if (!(DBNull.Value.Equals(reader["correct_data"]))) map.correct_data = reader["correct_data"].ToString();
                    maps.Add(map);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return maps;
        }

        /// <summary>
        /// this function updates the mapping data for on the database
        /// </summary>
        /// <param name="map"></param>
        /// <returns></returns>
        public static bool UpdateMappingData(MappingData map)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = ""; bool go = true;
                if (map.table == "parish") query = "EXEC UpdateMappingData " + map.id + "," + map.refid + "," + Constants.user.ID;
                else if (map.table == "wordings") query = "EXEC update_mapping_wording_tags " + map.id + "," + Constants.user.ID + ",'" + map.correct_data + "'";
                else go = false;
                if (go)
                {
                    SqlDataReader reader = sql.QuerySQL(query, "update", "mappingdata", map);
                    while (reader.Read())
                    {
                        result = (bool)reader["result"];
                    }
                    reader.Close();
                }
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function locks a record so that it can't be updated until it's unlocked
        /// </summary>
        /// <param name="map"></param>
        public static void LockMappingData(MappingData map)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC LockMappingData " + map.id + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "mappingdata", map);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function unlocks a locked record and makes it editable again
        /// </summary>
        /// <param name="map"></param>
        public static void UnLockMappingData(MappingData map)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC UnLockMappingData " + map.id + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "mappingdata", map);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function retrieves a single record of mapping data from the database
        /// </summary>
        /// <param name="map"></param>
        /// <returns></returns>
        public static MappingData GetMappingData(MappingData map)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetMappingData " + map.id;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    map = new MappingData(int.Parse(reader["ID"].ToString()), reader["table"].ToString(), reader["data"].ToString());
                    if (!(DBNull.Value.Equals(reader["refid"]))) map.refid = int.Parse(reader["refid"].ToString());
                    if (!(DBNull.Value.Equals(reader["correct_data"]))) map.correct_data = reader["correct_data"].ToString();
                    map.maplock = (bool)reader["lock"];
                }
            }
            return map;
        }
    }
}