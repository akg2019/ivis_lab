﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Globalization;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

namespace Honeysuckle.Models
{
    public class Policy
    {

        public int ID { get; set; }
        [RegularExpression("[0-9]+", ErrorMessage = "Incorrect EDIID format  (Only numbers allowed) ")]
        public string EDIID { get; set; }
        public DateTime CreatedOn { get; set; }
        public Company insuredby { get; set; }
        public DateTime startDateTime { get; set; }
        public DateTime endDateTime { get; set; }
        public List<Person> insured { get; set; }
        public List<Company> company { get; set; }
        public List<Vehicle> vehicles { get; set; }
        public PolicyCover policyCover { get; set; }
        public string insuredType { get; set; }
        public bool Scheme { get; set; }
        public Company compCreatedBy { get; set; }
        //public string BrokerNo { get; set; }

        public string mailingAddressShort { get; set; }
        public Address mailingAddress { get; set; }
        public string billingAddressShort { get; set; }
        public Address billingAddress { get; set; }

        public List<Driver> AuthorizedDrivers { get; set; }
        public List<Driver> ExceptedDrivers { get; set; }
        public List<Driver> ExcludedDrivers { get; set; }

        [RegularExpression("[a-zA-Z0-9][a-zA-Z0-9- ]*[a-zA-Z0-9]", ErrorMessage = "Incorrect policy number format  (Only letters, numbers and hyphens allowed) ")]
        public string policyNumber { get; set; }

        public bool no_policy_no { get; set; }
        public string internal_policy_number { get; set; }

        public bool imported { get; set; }
        public bool cancelled { get; set; }
        public bool billCheck { get; set; }
        public string brokerPolicyNumber { get; set; }
        public bool expired { get; set; }


        public Policy() { }
        public Policy(int ID)
        {
            this.ID = ID;
        }
        public Policy(int ID, PolicyCover cover)
        {
            this.ID = ID;
            this.policyCover = cover;
        }

        public Policy(int ID, PolicyCover cover, string policyNumber)
        {
            this.ID = ID;
            this.policyCover = cover;
            this.policyNumber = policyNumber;
        }
        public Policy(int ID, string policyNumber, PolicyCover cover)
        {
            this.ID = ID;
            this.policyCover = cover;
            this.policyNumber = policyNumber;
        }
        public Policy(int ID, PolicyCover cover, string Policyno, string EDIID)
        {
            this.ID = ID;
            this.policyCover = cover;
            this.EDIID = EDIID;
            this.policyNumber = Policyno;
        }
        public Policy(int ID, Company insuredby, DateTime startdate, DateTime enddate, string policyNumber)
        {
            this.ID = ID;
            this.insuredby = insuredby;
            this.startDateTime = startdate;
            this.endDateTime = enddate;
            this.policyNumber = policyNumber;
        }
        public Policy(int ID, DateTime startDateTime, DateTime endDateTime, string insuredType, PolicyCover policyCover, string policyNumber)
        {
            this.ID = ID;
            this.startDateTime = startDateTime;
            this.endDateTime = endDateTime;
            this.insuredType = insuredType;
            this.policyCover = policyCover;
            this.policyNumber = policyNumber;
        }

        public Policy(string json)
        {
            JObject obj = JObject.Parse(json);
            JToken ptoken = obj["policy"];

        }

        public Policy(string policyno, bool havePolicyNo)
        {
            this.policyNumber = policyno;
        }

        public Policy(int ID, string policyNumber, DateTime startDateTime, DateTime endDateTime)
        {
            this.ID = ID;
            this.policyNumber = policyNumber;
            this.startDateTime = startDateTime;
            this.endDateTime = endDateTime;
        }
        public Policy(int ID, DateTime startDateTime, DateTime endDateTime, Company insuredBy)
        {
            this.ID = ID;
            this.startDateTime = startDateTime;
            this.endDateTime = endDateTime;
            this.insuredby = insuredBy;
        }

    }

    public class PolicyModel
    {
        #region old
        //public static void update_usage_on_risk(Policy policy, Vehicle vehicle)
        //{
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        string query = "EXEC update_usage_on_risk " + policy.ID + "," + vehicle.ID + "," + vehicle.usage.ID + "," + Constants.user.ID;
        //        SqlDataReader reader = sql.QuerySQL(query, "update", "policy", policy);
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //}

        //public static void update_mortgagee_on_risk(Policy policy, Vehicle vehicle)
        //{
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        string query = "EXEC update_mortgagee_on_risk " + policy.ID + "," + vehicle.ID + ",'" + vehicle.mortgagee.mortgagee + "'," + Constants.user.ID;
        //        SqlDataReader reader = sql.QuerySQL(query, "update", "policy", policy);
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //}

        ///// <summary>
        ///// this function determines if this policy is viewable by the user currently logged in
        ///// </summary>
        ///// <param name="policy"></param>
        ///// <returns></returns>
        //public static bool is_my_policy(Policy policy)
        //{
        //    bool result = false;
        //    sql sql = new sql();
        //    if (sql.ConnectSQL())
        //    {
        //        string query = "EXEC IsMyPolicy " + policy.ID + "," + Constants.user.ID;
        //        SqlDataReader reader = sql.QuerySQL(query);
        //        while (reader.Read())
        //        {
        //            result = (bool)reader["result"];
        //        }
        //        reader.Close();
        //        sql.DisconnectSQL();
        //    }
        //    return result;
        //}
        #endregion


        private static string generate_internal_policy_number(Policy policy)
        {
            string insurer_code = CompanyModel.GetInsurerCode(policy.insuredby.CompanyID);
            return insurer_code + "-" + (get_highest_private_code_count(policy.insuredby) + 1).ToString();
        }

        private static int get_highest_private_code_count(Company insurer)
        {
            int result = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC get_highest_private_code_count " + insurer.CompanyID);
                while (reader.Read())
                {
                    result = int.Parse(reader["result"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function is used to test if a Risk is under a policy, which has multiple Vehicles
        /// </summary>
        /// <param name="polID">Policy Id</param>
        /// <returns>boolean</returns>
        public static bool HasMultipleVehicle(int polID)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsMultipleRisk " + polID;
                //SqlDataReader reader = sql.QuerySQL(query, "read", "policy", new Policy());
                SqlDataReader reader = sql.QuerySQL(query);

                while (reader.Read())
                {
                    result = bool.Parse(reader["result"].ToString());
                }
                reader.Close();
            }
            return result;
        }

        /// <summary>
        /// this function marks a policy as imported
        /// </summary>
        /// <param name="pol"></param>
        public static void MarkPolicyImported(Policy pol)
        {
            try
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    SqlDataReader reader = sql.QuerySQL("EXEC MarkPolicyImported " + pol.ID, "update", "policy", pol);
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            catch (Exception)
            {
               
            }
        }

        /// <summary>
        /// this function retreives all the policy holders for a policy
        /// </summary>
        /// <param name="pol"></param>
        /// <returns></returns>
        public static List<string> GetPolicyHolders(Policy pol)
        {
            List<string> trns = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "";
                query = "EXEC GetPolicyHolders " + pol.ID;

                //SqlDataReader reader = sql.QuerySQL(query, "read", "policy", new Policy());
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    trns.Add(reader["trn"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return trns;
        }

        /// <summary>
        /// this function returns the company who created (broker/agent) this policy
        /// </summary>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static Company GetCompanyCreatedByPolicy(Policy policy)
        {
            Company company = new Company();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetCompanyCreatedByPolicy " + policy.ID, "read", "company", new Company());
                while (reader.Read())
                {
                    company.CompanyID = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return company;
        }

        /// <summary>
        /// this function tests if the id relates to a real policy in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool IsRealPolicyID(int id = 0)
        {
            bool result = false;
            if (id != 0)
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    SqlDataReader reader = sql.QuerySQL("EXEC IsRealPolicyID " + id, "read", "policy", new Policy());
                    while (reader.Read())
                    {
                        result = bool.Parse(reader["result"].ToString());
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            return result;
        }

        /// <summary>
        /// this function determins whether the user logged into the system is able to see the policy in question
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool CanIViewPolicy(int id = 0)
        {
            bool result = false;
            if (id != 0)
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    SqlDataReader reader = sql.QuerySQL("EXEC CanIViewPolicy " + id + "," + Constants.user.ID, "read", "policy", new Policy());
                    while (reader.Read())
                    {
                        result = bool.Parse(reader["result"].ToString());
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            return result;
        }

        /// <summary>
        /// this function returns if the vehicle in question is under cover
        /// </summary>
        /// <param name="chassis"></param>
        /// <param name="license"></param>
        /// <returns></returns>
        public static bool IsVehicleCovered(string chassis = null, string license = null)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL() && ((chassis != null && chassis != "") || (license != null && license != "")))
            {
                string query = "EXEC IsVehicleCovered ";
                if (chassis != null && chassis != "") query += " '" + Constants.CleanString(chassis) + "',";
                else query += " null,";
                if (license != null && license != "") query += "'" + Constants.CleanString(license) + "'";
                else query += "null";
                //SqlDataReader reader = sql.QuerySQL(query, "read", "policy", new Policy());
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = bool.Parse(reader["result"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }


        /// <summary>
        /// This function performs a vehicle cover validation
        /// </summary>
        /// <param name="chassis"></param>
        /// <param name="license"></param>
        /// <returns></returns>
        public static JArray IsVehicleCoveredDetails(string chassis = null, string license = null)
        {
            string sqlConnectionString = "";
            #if DEBUG
                        sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;//ApplicationServices
            #else
                                        sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ExternalConnection"].ConnectionString; 
            #endif

            JArray result = new JArray();

            string query = "EXEC IsVehicleCoveredDetails ";

            if (chassis != null && chassis != "") query += " '" + Constants.CleanString(chassis) + "',";
            else query += " null,";
            if (license != null && license != "") query += "'" + Constants.CleanString(license) + "'";
            else query += "null";

            DataTable dt = (DataTable)OmaxFramework.Utilities.OmaxData.HandleQuery(query, sqlConnectionString, OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.ScalarQuery);

            result = JArray.Parse(JsonConvert.SerializeObject(dt));

            return result;
        }

        //Get Vehicle status
        public static JArray IsVehicleCoveredDetailsUpdated(string chassis = null, string license = null)
        {
            string sqlConnectionString = "";
            #if DEBUG
                        sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;//ApplicationServices
            #else
                                                    sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ExternalConnection"].ConnectionString; 
            #endif

            JArray result = new JArray();

            string query = "EXEC CoverStatusCheck ";

            if (chassis != null && chassis != "") query += " '" + Constants.CleanString(chassis) + "',";
            else query += " null,";
            if (license != null && license != "") query += "'" + Constants.CleanString(license) + "'";
            else query += "null";

            DataTable dt = (DataTable)OmaxFramework.Utilities.OmaxData.HandleQuery(query, sqlConnectionString, OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.ScalarQuery);

            result = JArray.Parse(JsonConvert.SerializeObject(dt));

            return result;
        }


        public static JArray IsVehicleCoveredDetails2(string chassis = null, string license = null)
        {
                        string sqlConnectionString = "";
            #if DEBUG
                        sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;//ApplicationServices
            #else
                                        sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ExternalConnection"].ConnectionString; 
            #endif

            JArray result = new JArray();

            //string query = "EXEC CoverStatusCheck ";

            //string query = "EXEC VechicleCoverStatusCheckFlat ";
            string query = "EXEC VechicleCoverStatusCheckFlat_Mod ";

            if (chassis != null && chassis != "") query += " '" + Constants.CleanString(chassis) + "',";
            else query += " null,";
            if (license != null && license != "") query += "'" + Constants.CleanString(license) + "'";
            else query += "null";

            DataTable dt = (DataTable)OmaxFramework.Utilities.OmaxData.HandleQuery(query, sqlConnectionString, OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.ScalarQuery);

            result = JArray.Parse(JsonConvert.SerializeObject(dt));

            return result;
        }


        /// <summary>
        /// this function tests if a vehicle is covered under a certain time period
        /// </summary>
        /// <param name="VehicleID"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <returns></returns>
        public static bool IsVehicleCovered(int VehicleID, DateTime startdate, DateTime enddate)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC IsVehicleCoveredByID " + VehicleID + ",'" + startdate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + enddate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                //SqlDataReader reader = sql.QuerySQL(query, "read", "policy", new Policy());
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = bool.Parse(reader["result"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function returns the policy that a vehicle is under during a specified time frame
        /// </summary>
        /// <param name="VehicleID"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <returns></returns>
        public static Policy GetPolicyCoveredUnder(int VehicleID, DateTime startdate, DateTime enddate)
        {
            Policy policy = new Policy();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetPolicyCoveredUnder " + VehicleID + ",'" + startdate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + enddate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                //SqlDataReader reader = sql.QuerySQL(query, "read", "policy", new Policy());
                SqlDataReader reader = sql.QuerySQL(query);

                while (reader.Read())
                {
                    policy.ID = int.Parse(reader["PolicyID"].ToString());
                    policy.compCreatedBy = new Company(int.Parse(reader["CompanyCreatedBy"].ToString()));
                    policy.insuredby = new Company(int.Parse(reader["insurer"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return policy;
        }

        /// <summary>
        /// this function searches through the entire policy module 
        /// and annexed sections of the application
        /// </summary>
        /// <param name="search">a string that will be searched</param>
        /// <returns>list of policies that match the search</returns>
        public static List<Policy> PolicySearch(string search)
        {
            List<Policy> policies = new List<Policy>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC PolicySearch '" + Constants.CleanString(search) + "'";
                //SqlDataReader reader = sql.QuerySQL(query, "read", "policy", new Policy());
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    policies.Add(new Policy(int.Parse(reader["ID"].ToString()),
                                            new Company(int.Parse(reader["CompanyID"].ToString()), reader["CompanyName"].ToString(), reader["IAJID"].ToString()),
                                            DateTime.Parse(reader["start"].ToString()),
                                            DateTime.Parse(reader["end"].ToString()),
                                            reader["pno"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return policies;
        }

        /// <summary>
        /// this function returns all the usages is use in this policy
        /// </summary>
        /// <param name="pol"></param>
        /// <returns></returns>
        public static List<Usage> getAllUsagesinPolicy(Policy pol)
        {
            List<Usage> usages = new List<Usage>();
            foreach (Vehicle v in pol.vehicles)
            {
                usages.Add(v.usage);
            }
            return usages;
        }

        /// <summary>
        /// this function adds a policy to a system;
        /// this function is be used by users logged in on the website
        /// </summary>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static int addPolicy(Policy policy)
        {
            return addPolicy(policy, Constants.user.ID);
        }

        /// <summary>
        /// This function adds a policy to the system
        /// </summary>
        /// <param name="policy">Policy- complete policy model with all the data about the policy</param>
        /// <returns>Integer- Policy Id</returns>
        public static int addPolicy(Policy policy, int uid, bool import = false)
        {
            EmailModel em = new EmailModel();
            EmployeeModel emp = new EmployeeModel();
            Employee empp = new Employee();
            AddressModel add = new AddressModel();
            int policyId = 0;
            int companyCreatedByID = 0;


            sql sql = new sql();
            if (sql.ConnectSQL() && policy.insuredby != null)
            {

                string DriverNames = "";
                string PolicyHolderNames = "";

                string CompanyNames = "";
                try { foreach (var N in policy.company) { CompanyNames += N.CompanyName + " and "; } }
                catch (Exception) { }
                try { CompanyNames = CompanyNames.Substring(0, CompanyNames.Length - 4); }
                catch (Exception) { }
                PolicyHolderNames += CompanyNames;
                string AuthDrivers = "";

                try
                {
                    foreach (var N in policy.insured)
                    {
                        if (N.TRN != null || N.TRN != "")
                        {

                            var P = PersonModel.GetPersonByTRN(N.TRN);
                            PolicyHolderNames += P.fname + " " + P.mname + " " + P.lname + " and ";
                        }
                        else {
                            PolicyHolderNames += N.fname + " " + N.mname + " " + N.lname + " and ";
                        }
                        
                    }
                }
                catch (Exception) { }


                try { foreach (var N in policy.vehicles) { foreach (var d in N.AuthorizedDrivers) { AuthDrivers += d.person.fname + " " + d.person.mname + " " + d.person.lname + " and "; } } }
                catch (Exception) { }
               
                try { PolicyHolderNames = PolicyHolderNames.Substring(0, PolicyHolderNames.Length - 4); }
                catch (Exception) { }
                try { DriverNames = DriverNames.Substring(0, DriverNames.Length - 4); }
                catch (Exception) { }

                try { AuthDrivers = AuthDrivers.Substring(0, AuthDrivers.Length - 4); }
                catch (Exception) { }
                try { AuthDrivers = AuthDrivers.Replace("'", ""); }
                catch (Exception) { }

                PolicyHolderNames = PolicyHolderNames.Replace("'", "");

                try { PolicyHolderNames = PolicyHolderNames.Replace("'", ""); }
                catch (Exception) { }
                try { DriverNames = DriverNames.Replace("'", ""); }
                catch (Exception) { }

                //testing if the cover already exists in the system
                policy.billingAddress.ID = AddressModel.AddAddress(policy.billingAddress, uid);
                policy.mailingAddress.ID = AddressModel.AddAddress(policy.mailingAddress, uid);
                if (policy.insuredby.CompanyID == 0)
                {
                    CompanyModel.AddCompany(policy.insuredby);
                    policy.insuredby = CompanyModel.GetCompanyByTRN(policy.insuredby.trn);
                }

                if (policy.compCreatedBy == null)
                {
                    policy.compCreatedBy = CompanyModel.GetCompanyByTRN(policy.compCreatedBy.trn);
                    companyCreatedByID = policy.compCreatedBy.CompanyID;
                }

                if (policy.policyCover.ID == 0) policy.policyCover = PolicyCoverModel.addPolicyCover(policy.policyCover, uid, policy.insuredby.CompanyID);

                //policy insured type automation
                int people_count = 0; int company_count = 0;
                if (policy.insured != null) people_count = policy.insured.Count();
                if (policy.company != null) company_count = policy.company.Count();

                if (people_count == 1 && company_count == 0) policy.insuredType = "Individual";
                else if (people_count == 0 && company_count == 1) policy.insuredType = "Company";
                else if ((people_count > 1 && company_count == 0) || (people_count == 0 && company_count > 1)) policy.insuredType = "Joint Insured";
                else if (people_count == 1 && company_count == 1) policy.insuredType = "Individual/Company";
                else if ((people_count > 1 && company_count > 0) || (people_count > 0 && company_count > 1)) policy.insuredType = "Blanket";
                else policy.insuredType = "No Insured Type";

                if (import && (policy.CreatedOn == null || policy.CreatedOn.ToString("yyyy-MM-dd HH:mm:ss") == "0001-01-01 00:00:00")) policy.CreatedOn = DateTime.Now;

                var PolPrefix = PolicyCoverModel.GetCoverByID(policy.policyCover);

                string query = "EXECUTE AddPolicy '" + policy.startDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                     + policy.endDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                     + policy.CreatedOn.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                     + Constants.CleanString(policy.policyNumber) + "',"
                                                     + uid + ","
                                                     + policy.insuredby.CompanyID + ",'"
                                                     + Constants.CleanString(policy.EDIID) + "',"
                                                     + policy.policyCover.ID + ",'"
                                                     + Constants.CleanString(policy.insuredType) + "',"
                                                     + policy.Scheme + ","
                                                     + policy.billingAddress.ID + ","
                                                     + policy.mailingAddress.ID + ",'"
                                                     + Constants.CleanString(policy.mailingAddressShort) + "','"
                                                     + Constants.CleanString(policy.billingAddressShort) + "',"
                                                     + policy.compCreatedBy.CompanyID + ",'"
                                                     + policy.billCheck + "','"
                                                     + policy.no_policy_no + "',"
                                                     + import + ",NULL,'" + PolPrefix.prefix + "','" + PolicyHolderNames + "'";

                if (policy.brokerPolicyNumber != null && policy.brokerPolicyNumber != "") query += ",'" + policy.brokerPolicyNumber + "'";

                //Policy pol = new Policy();
                //pol = policy;
                //SqlDataReader reader = sql.QuerySQL(query);

                SqlDataReader reader = sql.QuerySQL(query, "create", "policy", policy);
                while (reader.Read())
                {
                    policyId = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return policyId;

        }

        /// <summary>
        /// this function returned a policy id based on an edi system id
        /// </summary>
        /// <param name="ediid"></param>
        /// <returns></returns>
        public static int get_policy_by_edi(string ediid, int CompanyID = 0)
        {
            int result = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC get_policy_by_edi '" + ediid + "'," + CompanyID.ToString());
                while (reader.Read())
                {
                    result = int.Parse(reader["id"].ToString());
                }
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function adds the policy insureds to a policy
        /// </summary>
        /// <param name="policy"></param>
        public static void addPolicyInsured(Policy policy)
        {
            addPolicyInsured(policy, Constants.user.ID);
        }

        /// <summary>
        /// This function adds all the information about a policy (including insured companies, individuals and vehicles associated with the policy)
        /// </summary>
        /// <param name="policy">Policy object containing policy information</param>
        public static void addPolicyInsured(Policy policy, int uid, bool import = false, bool cancelled = false)
        {
            sql sql = new sql();
            //string query= "";
            if (policy.EDIID == null) policy.EDIID = "0";

            //adds the companies and people to the policy 
            if (policy.company != null)
            {
                for (int x = 0; x < policy.company.Count; x++)
                {
                    add_company_policy_insured(policy, policy.company[x], uid);
                }
            }
            if (policy.insured != null)
            {
                for (int x = 0; x < policy.insured.Count; x++)
                {
                    add_person_policy_insured(policy, policy.insured[x], uid);
                }
            }

            //Adding the vehicles to the policy
            for (int x = 0; x < policy.vehicles.Count; x++) PolicyModel.addVehicleToPolicy(policy, policy.vehicles[x], uid, import, cancelled);
        }

        /// <summary>
        /// this function removes a vehicle from specified policy
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="vehicle"></param>
        /// <param name="uid"></param>
        public static void rm_vehicle_from_policy(Policy policy, Vehicle vehicle, int uid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC RemoveVehicleFromPolicy " + policy.ID + ","
                                                               + vehicle.ID + ","
                                                               + uid;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "vehicle", policy, "policyholder");
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function will add a specific company as a policy holder to a policy
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="company"></param>
        /// <param name="uid"></param>
        public static void add_company_policy_insured(Policy policy, Company company, int uid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                int IntermediaryID = 0; int CompanyID = 0;

                //Gets Company ID and Intermediary IDs to be added to the vehicle and Vehicle MetaData tables.
                if (policy != null)
                {
                    CompanyID = policy.insuredby.CompanyID;
                    IntermediaryID = policy.compCreatedBy.CompanyID;
                    if (IntermediaryID == 0) IntermediaryID = CompanyID;
                }

                try { if (CompanyID == 0) CompanyID = CompanyModel.GetCompanyByTRN(policy.insuredby.trn).CompanyID; }
                catch (Exception) { }
                try { if (IntermediaryID == 0) IntermediaryID = CompanyModel.GetCompanyByTRN(policy.compCreatedBy.trn).CompanyID; }
                catch (Exception) { }

                if (IntermediaryID == 0) IntermediaryID = CompanyID;

                //THIS IS ADDING A PERSON TO THE POLICY
                string query = "EXECUTE AddInsuredToPolicy '" + Constants.CleanString(policy.EDIID) + "',"
                                                             + uid + ","
                                                             + policy.ID + ","
                                                             + company.CompanyID + ","
                                                             + 0 + "," + CompanyID + "," + IntermediaryID;
                SqlDataReader reader = sql.QuerySQL(query);//, "create", "company", policy, "policyholder");
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function will delete a specific company as a policy holder to a policy
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="company"></param>
        /// <param name="uid"></param>
        public static void rm_company_policy_insured(Policy policy, Company company, int uid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                //THIS IS ADDING A PERSON TO THE POLICY
                string query = "EXECUTE RemoveInsuredFromPolicy " + policy.ID + ","
                                                                  + company.CompanyID + ","
                                                                  + uid + ","
                                                                  + 0;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "company", policy, "policyholder");
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function will add a specific person as a policy holder to a policy
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="company"></param>
        /// <param name="uid"></param>
        public static void add_person_policy_insured(Policy policy, Person person, int uid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                int IntermediaryID = 0; int CompanyID = 0;

                //Gets Company ID and Intermediary IDs to be added to the vehicle and Vehicle MetaData tables.
                if (policy != null)
                {
                    CompanyID = policy.insuredby.CompanyID;
                    IntermediaryID = policy.compCreatedBy.CompanyID;

                    if (IntermediaryID == 0) IntermediaryID = CompanyID;
                }

                try { if (CompanyID == 0) CompanyID = CompanyModel.GetCompanyByTRN(policy.insuredby.trn).CompanyID; }
                catch (Exception) { }
                try { if (IntermediaryID == 0) IntermediaryID = CompanyModel.GetCompanyByTRN(policy.compCreatedBy.trn).CompanyID; }
                catch (Exception) { }

                if (IntermediaryID == 0) IntermediaryID = CompanyID;


                //THIS IS ADDING A PERSON TO THE POLICY
                string query = "EXECUTE AddInsuredToPolicy '" + Constants.CleanString(policy.EDIID) + "',"
                                                             + uid + ","
                                                             + policy.ID + ","
                                                             + person.PersonID + ","
                                                             + 1 + "," + CompanyID + "," + IntermediaryID + "," + person.PersonID;
                SqlDataReader reader = sql.QuerySQL(query);//, "create", "person", policy, "policyholder");
                //SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function will remove a specific person as a policy holder to a policy
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="company"></param>
        /// <param name="uid"></param>
        public static void rm_person_policy_insured(Policy policy, Person person, int uid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                //THIS IS ADDING A PERSON TO THE POLICY
                string query = "EXECUTE RemoveInsuredFromPolicy " + policy.ID + ","
                                                                  + person.PersonID + ","
                                                                  + uid + ","
                                                                  + 1;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "person", policy, "policyholder");
                //SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function deals with the adding of vehicles to a policy
        /// </summary>
        /// <returns>boolean</returns>
        public static bool addVehicleToPolicy(Policy policy, Vehicle Risk, int uid, bool import = false, bool cancelled = false)
        {

            if (Risk.ID == 0) Risk = VehicleModel.GetVehicleByChassis(Risk.chassisno, uid);
            else //Retrieving the main driver ID of the vehicle
            {
                Risk.mainDriver = VehicleModel.GetVehicleByChassis(Risk.chassisno, uid).mainDriver;
            }

            if (Risk.mortgagee == null || Risk.mortgagee.mortgagee == "") Risk.mortgagee = new Mortgagee();
            else Risk.mortgagee = MortgageeModel.AddMortgagee(Risk.mortgagee, uid);

            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                int IntermediaryID = 0; int CompanyID = 0;

                //Gets Company ID and Intermediary IDs to be added to the vehicle and Vehicle MetaData tables.
                //Gets Company ID and Intermediary IDs to be added to the vehicle and Vehicle MetaData tables.
                try
                {
                    if (policy != null)
                    {
                        CompanyID = CompanyModel.GetMyCompany(new User(uid)).CompanyID; 

                        IntermediaryID = policy.compCreatedBy.CompanyID;

                        if (IntermediaryID == 0) IntermediaryID = CompanyID;
                    }
                    else
                    {
                        CompanyID = CompanyModel.GetMyCompany(new User(uid)).CompanyID;
                        try { IntermediaryID = policy.compCreatedBy.CompanyID; }
                        catch (Exception) { }

                    }

                    if (IntermediaryID == 0) IntermediaryID = CompanyID;
                    try { if (CompanyID == 0) CompanyID = CompanyModel.GetCompanyByTRN(policy.insuredby.trn).CompanyID; }
                    catch (Exception) { }
                    try { if (IntermediaryID == 0) IntermediaryID = CompanyModel.GetCompanyByTRN(policy.compCreatedBy.trn).CompanyID; }
                    catch (Exception) { }

                    if (IntermediaryID == 0) IntermediaryID = CompanyID;

                }
                catch (Exception)
                {

                }


                string query = "EXECUTE AddVehicleToPolicy " + Risk.ID + ","
                                                             + policy.ID + ","
                                                             + Risk.usage.ID + ","
                                                             + Risk.mortgagee.ID + ","
                                                             + uid + ","
                                                             + import + ","
                                                             + cancelled + "," + CompanyID + "," + IntermediaryID;


                SqlDataReader reader = sql.QuerySQL(query);//, "create", "vehicle", policy, "policyholder");
                while (reader.Read())
                {
                    result = bool.Parse(reader["result"].ToString());
                }
                reader.Close();

                sql.DisconnectSQL();
            }

            //Setting the vehicle's main driver as an authorized driver of that vehicle (under a given policy)
            if (Risk.mainDriver != null) VehicleModel.addDriversToVehicle(Risk.chassisno, Risk.mainDriver.person.PersonID, 1, policy.ID, uid);

            return result;
        }
                

        /// <summary>
        /// this function retrieves a policy id of any policy that has a specific policy no
        /// </summary>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static Policy GetPolicyByNo(string policy_no)
        {
            Policy policy = new Policy();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetPolicyByPolicyNo '" + Constants.CleanString(policy_no) + "'";
                //SqlDataReader reader = sql.QuerySQL(query, "read", "policy", new Policy());
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    policy.ID = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            if (policy.ID != 0) policy = getPolicyFromID(policy.ID);
            return policy;
        }

         public static Policy GetPolicyByNo(string policy_no, int CompanyID)
        {
            Policy policy = new Policy();
           
            DataTable dt = (DataTable)OmaxFramework.Utilities.OmaxData.HandleQuery("SELECT MAX(POLICYID)[ID] FROM POLICIES WHERE COMPANYID = " + CompanyID + " AND POLICYNO = '" + policy_no + "'", sql.GetCurrentCOnnectionString(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.ScalarQuery);

            try { policy.ID = int.Parse(dt.Rows[0].ItemArray[0].ToString()); } catch (Exception) { }

            if (policy.ID != 0) policy = getPolicyFromID(policy.ID);

            return policy;
        }

        /// <summary>
        /// this function updates a policy's data, this version should be used only by the website and not the import module
        /// </summary>
        /// <param name="policy"></param>
        public void modifyPolicy(Policy policy)
        {
            modifyPolicy(policy, Constants.user.ID);
        }

        /// <summary>
        /// This function modifies an existing policy
        /// </summary>
        /// <param name="policy">Policy object, with policy information</param>
        public void modifyPolicy(Policy policy, int id)
        {

            try
            {
                foreach(var v in policy.vehicles)
                {
                    try
                    {
                        string con = new sql().SqlConn();
                        string Query = @"UPDATE VehiclesUnderPolicy SET VehicleUsage = " + v.usage.ID + "  WHERE PolicyID = " + policy.ID; //+ " AND VehicleID = " + v.ID;
                        OmaxFramework.Utilities.OmaxData.HandleQuery(Query, con, OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.NoneQuery);
                    }
                    catch (Exception)
                    {
                     
                    }
                   
                }

               
            }
            catch (Exception)
            {
              
            }

            string DriverNames = "";
            string PolicyHolderNames = "";

            string CompanyNames = "";
            try { foreach (var N in policy.company) { CompanyNames += N.CompanyName + " and "; } }
            catch (Exception) { }
            try { CompanyNames = CompanyNames.Substring(0, CompanyNames.Length - 4); }
            catch (Exception) { }
            PolicyHolderNames += CompanyNames;

            string AuthDrivers = "";

            try
            {
                foreach (var N in policy.insured)
                {
                    if (N.TRN != null || N.TRN != "")
                    {

                        var P = PersonModel.GetPersonByTRN(N.TRN);
                        PolicyHolderNames += P.fname + " " + P.mname + " " + P.lname + " and ";
                    }
                    else {
                        PolicyHolderNames += N.fname + " " + N.mname + " " + N.lname + " and ";
                    }

                }
            }
            catch (Exception) { }


            try { foreach (var N in policy.vehicles) { foreach (var d in N.AuthorizedDrivers) { AuthDrivers += d.person.fname + " " + d.person.mname + " " + d.person.lname + " and "; } } }
            catch (Exception) { }

             try { AuthDrivers = AuthDrivers.Substring(0, AuthDrivers.Length - 4); }catch(Exception){}
             try { AuthDrivers = AuthDrivers.Replace("'",""); }catch(Exception){}

            try { PolicyHolderNames = PolicyHolderNames.Substring(0, PolicyHolderNames.Length - 4); }
            catch (Exception) { }
            try { DriverNames = DriverNames.Substring(0, DriverNames.Length - 4); }
            catch (Exception) { }

             try { PolicyHolderNames = PolicyHolderNames.Replace("'", ""); }catch(Exception){}
             try { DriverNames = DriverNames.Replace("'", ""); }catch(Exception){}

            var PolPrefix = PolicyCoverModel.GetCoverByID(policy.policyCover);


            try
            {
             sql sql = new sql();
                        if (sql.ConnectSQL())
                        {
                            string BrokerNo = "";
                            if (policy.brokerPolicyNumber != null && policy.brokerPolicyNumber != "") BrokerNo = policy.brokerPolicyNumber;
                            //if(policy
                            //modifying a policy 
                            if (policy.policyCover.ID == 0) policy.policyCover = PolicyCoverModel.addPolicyCover(policy.policyCover, id, policy.insuredby.CompanyID);
                            policy.billingAddress.ID = AddressModel.AddAddress(policy.billingAddress, id);
                            policy.mailingAddress.ID = AddressModel.AddAddress(policy.mailingAddress, id);
                            string query = "EXECUTE ModifyPolicy '" + policy.startDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                                    + policy.endDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "','"
                                                                    + Constants.CleanString(policy.insuredType) + "','"
                                                                    + Constants.CleanString(policy.policyNumber) + "',"
                                                                    + policy.policyCover.ID + ","
                                                                    + policy.Scheme + ","
                                                                    + policy.billingAddress.ID + ","
                                                                    + policy.mailingAddress.ID + ","
                                                                    + policy.ID + ",'"
                                                                    + Constants.CleanString(policy.mailingAddressShort) + "','"
                                                                    + Constants.CleanString(policy.billingAddressShort) + "',"
                                                                    + id + ","
                                                                    + policy.billCheck + ","
                                                                    + policy.no_policy_no + ",'" + BrokerNo + "','" + PolPrefix.prefix + "','" + PolicyHolderNames.Replace("'","") + "'";


                            try
                            {
                                query += "," + (policy.cancelled ? "1" : "0");

                            }
                            catch (Exception)
                            {
                                query += ",0";
                            }

                            try { query += "," + (policy.compCreatedBy != null ? policy.compCreatedBy.CompanyID : 0); }
                            catch (Exception) { }

                

                try
                {
                   // SqlDataReader reader = sql.QuerySQL(query, "update", "policy", policy);
                    //reader.Close();

                    sql.QuerySQL(query, "update", "policy", policy);
                    sql.DisconnectSQL();
                }
                catch (Exception)
                {
                  
                }
            }
            }
            catch (Exception)
            {
               
            }


        }

        /// <summary>
        /// This function retrieves information about a policy, given the policy id
        /// </summary>
        /// <param name="id">Policy id</param>
        /// <returns>Policy object- storing policy infromation</returns>
        public static Policy getPolicyFromID(int id)
        {
            Policy pm = new Policy();
            User us = new User();
            PolicyCover pc = new PolicyCover();
            Person per = new Person();
            Email em = new Email();


            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXECUTE GetPolicyFromId " + id;
                SqlDataReader reader = sql.QuerySQL(query);
                //SqlDataReader reader = sql.QuerySQL(query, "read", "policy", new Policy());

                while (reader.Read())
                {
                    pm.ID = id;

                   
                    try { pm.no_policy_no = bool.Parse(reader["no_policy_no"].ToString()); }catch(Exception){}

                    if (DBNull.Value != reader["EDIID"]) pm.EDIID = reader["EDIID"].ToString();

                    if (DBNull.Value != reader["CreatedOn"]) pm.CreatedOn = DateTime.Parse(reader["CreatedOn"].ToString());

                    
                    try {us.ID = int.Parse(reader["CreatedBy"].ToString()); }catch(Exception){}

                    pm.insuredType = reader["InsuredType"].ToString();

                    
                    try {if (DBNull.Value != reader["StartDateTime"]) pm.startDateTime = DateTime.Parse(reader["StartDateTime"].ToString()); }catch(Exception){}
                    try {if (DBNull.Value != reader["EndDateTime"]) pm.endDateTime = DateTime.Parse(reader["endDateTime"].ToString()); }catch(Exception){}

                    if (DBNull.Value != reader["Policyno"]) pm.policyNumber = reader["Policyno"].ToString();
                    if (DBNull.Value != reader["BrokerNo"]) pm.brokerPolicyNumber = reader["BrokerNo"].ToString();
                    
                    try {pm.insuredby = new Company(int.Parse(reader["CompanyID"].ToString()), reader["CompanyName"].ToString()); }catch(Exception){}
                    try {pm.imported = bool.Parse(reader["imported"].ToString()); }catch(Exception){}
                    
                    try {pc.ID = int.Parse(reader["PolicyCover"].ToString()); }catch(Exception){}
                    pc.cover = reader["Cover"].ToString();
                    pm.Scheme = bool.Parse(reader["Scheme"].ToString());
                   
                    try { pm.compCreatedBy = new Company(int.Parse(reader["CompanyCreatedBy"].ToString())); }catch(Exception){}
                    if (DBNull.Value != reader["creatorCompTRN"]) pm.compCreatedBy.trn = reader["creatorCompTRN"].ToString();
                    if (DBNull.Value != reader["creatorCompName"]) pm.compCreatedBy.CompanyName = reader["creatorCompName"].ToString();

                    pc.prefix = reader["PolicyPrefix"].ToString();
                    pm.policyCover = pc;

                    Parish billparish = new Parish();
                    Parish mailparish = new Parish();
                    try {if (reader["mpid"] != DBNull.Value) mailparish = new Parish(int.Parse(reader["mpid"].ToString()), reader["mp"].ToString()); }catch(Exception){}
                    try {if (reader["PID"] != DBNull.Value) billparish = new Parish(int.Parse(reader["PID"].ToString()), reader["Parish"].ToString()); }catch(Exception){}
                    
                    try
                    {
                        pm.billingAddressShort = reader["billshort"].ToString();
                        pm.billingAddress = new Address(int.Parse(reader["AID"].ToString()),
                                                        reader["RoadNumber"].ToString(),
                                                        reader["AptNumber"].ToString(),
                                                        new Road(reader["RoadName"].ToString()),
                                                        new RoadType(reader["RoadType"].ToString()),
                                                        new ZipCode(reader["ZipCode"].ToString()),
                                                        new City(reader["City"].ToString()),
                                                        billparish,
                                                        new Country(reader["CountryName"].ToString()));
                    }
                    catch (Exception)
                    {
                      
                    }
                    
                    try
                    {
                        pm.mailingAddressShort = reader["mailshort"].ToString();
                        pm.mailingAddress = new Address(int.Parse(reader["maid"].ToString()),
                                                        reader["marn"].ToString(),
                                                        reader["mapt"].ToString(),
                                                        new Road(reader["mrn"].ToString()),
                                                        new RoadType(reader["mrt"].ToString()),
                                                        new ZipCode(reader["mzc"].ToString()),
                                                        new City(reader["mc"].ToString()),
                                                        mailparish,
                                                        new Country(reader["mco"].ToString()));
                    }
                    catch (Exception)
                    {

                    }

                    try {pm.cancelled = bool.Parse(reader["cancelled"].ToString()); }catch(Exception){}
                    try {pm.expired = bool.Parse(reader["expired"].ToString()); }catch(Exception){}
                    
                    if (reader["BillCheck"] != DBNull.Value)
                        try {pm.billCheck = bool.Parse(reader["BillCheck"].ToString()); }catch(Exception){}
                        
                    else
                        pm.billCheck = true;

                }
                reader.Close();
                sql.DisconnectSQL();
            }

            //getting lists of insured companies, individuals and vehicles associated with a policy
            pm.insured = GetInsuredList(pm.ID);
            pm.company = GetInsuredCompanies(pm.ID);
            pm.vehicles = GetInsuredVehicles(pm.ID);

            return pm;

        }

        /// <summary>
        /// This function deletes an existing policy from the system
        /// </summary>
        /// <param name="id">Existing policy id</param>
        /// <returns>boolean</returns>
        public static bool deletePolicy(int id)
        {
            bool result = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXECUTE DeletePolicy " + id + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "update", "policy", new Policy(id));

                while (reader.Read())
                {
                    result = bool.Parse(reader["result"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function retrieves all the policies in the system
        /// </summary>
        /// <returns>List of policies</returns>
        public static List<Policy> GetPolicies()
        {
            List<Policy> result = new List<Policy>();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXECUTE GetPolicies", "read", "policy", new Policy());

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        int ID = int.Parse(reader["PolicyID"].ToString());
                        string policyNo = reader["Policyno"].ToString();
                        string EDIID = reader["EDIID"].ToString();

                        result.Add(new Policy(ID, new PolicyCover(int.Parse(reader["polcovid"].ToString()), reader["prefix"].ToString(), reader["cover"].ToString()), policyNo, EDIID));
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function retrieves all the insured individuals associated with a particular policy
        /// </summary>
        /// <param name="Id">Policy ID</param>
        /// <returns>A list of people insured to a Risk- and policy</returns>
        public static List<Person> GetInsuredList(int PolicyId)
        {
            List<Person> persons = new List<Person>();
            Address add = new Address();
            try
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    SqlDataReader reader = sql.QuerySQL("EXEC GetInsured " + PolicyId, "read", "person", new Person());
                    while (reader.Read())
                    {
                        Person per = new Person();
                        per.PersonID = int.Parse(reader["PeopleID"].ToString());
                        per.fname = reader["FName"].ToString();
                        per.lname = reader["LName"].ToString();
                        per.TRN = reader["TRN"].ToString();

                        add.city = new City(reader["City"].ToString());
                        add.roadnumber = reader["RoadNumber"].ToString();
                        add.road = new Road(reader["RoadName"].ToString());
                        add.country = new Country(reader["CountryName"].ToString());

                        per.address = add;

                        persons.Add(per);
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }


            }
            catch (Exception)
            {
              
            }

            try
            {
                //Runs backup function
                if (persons.Count <= 0) 
                {

                    sql sql = new sql();
                    if (sql.ConnectSQL())
                    {
                        SqlDataReader reader = sql.QuerySQL("EXEC GetInsured2 " + PolicyId, "read", "person", new Person());
                        while (reader.Read())
                        {
                            Person per = new Person();
                            per.PersonID = int.Parse(reader["PeopleID"].ToString());
                            per.fname = reader["FName"].ToString();
                            per.lname = reader["LName"].ToString();
                            per.TRN = reader["TRN"].ToString();

                            try
                            {
                                add.city = new City(reader["City"].ToString());
                                add.roadnumber = reader["RoadNumber"].ToString();
                                add.road = new Road(reader["RoadName"].ToString());
                                add.country = new Country(reader["CountryName"].ToString());

                                per.address = add;
                            }
                            catch (Exception)
                            {
                              
                            } 

                            persons.Add(per);
                        }
                        reader.Close();
                        sql.DisconnectSQL();
                    }

                
                }
                

            }
            catch (Exception)
            {

            }


            return persons;
        }

        /// <summary>
        /// This function retrieves all the insured companies associated with a particular policy
        /// </summary>
        /// <param name="Id">Policy ID</param>
        /// <returns>A list of companies insured to a Risk- and policy</returns>
        public static List<Company> GetInsuredCompanies(int PolicyId)
        {
            List<Company> companies = new List<Company>();
            Address add = new Address();

            //New line

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetInsuredCompanies " + PolicyId, "read", "company", new Company());
                while (reader.Read())
                {
                    Company com = new Company();
                    com.CompanyID = int.Parse(reader["CompanyID"].ToString());
                    com.CompanyName = reader["CompanyName"].ToString();
                    com.IAJID = reader["IAJID"].ToString();

                    add.city = new City(reader["City"].ToString());
                    add.roadnumber = reader["RoadNumber"].ToString();
                    add.road = new Road(reader["RoadName"].ToString());
                    add.country = new Country(reader["CountryName"].ToString());

                    com.CompanyAddress = add;
                    companies.Add(com);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return companies;
        }

        /// <summary>
        /// This function calls the appropriate functions to update a policy
        /// </summary>
        /// <param name="policy"></param>
        ///<param name="uid"></param>
        public static void UpdatePolicyData(Policy policy, int uid)
        {
            List<Company> current_insured_companies = GetInsuredCompanies(policy.ID);
            List<Person> current_insured_people = GetInsuredList(policy.ID);
            List<Vehicle> current_insured_vehicles = GetInsuredVehicles(policy.ID);

            Tuple<List<int>, List<int>> tuple = new Tuple<List<int>, List<int>>(new List<int>(), new List<int>());
            //company
            if (policy.company == null) policy.company = new List<Company>();
            tuple = find_diff(Company.toInt(current_insured_companies), Company.toInt(policy.company));
            foreach (int i in tuple.Item1)
            {
                rm_company_policy_insured(policy, new Company(i), uid);
            }
            foreach (int i in tuple.Item2)
            {
                add_company_policy_insured(policy, new Company(i), uid);
            }
            //people
            if (policy.insured == null) policy.insured = new List<Person>();
            tuple = find_diff(Person.toInt(current_insured_people), Person.toInt(policy.insured));
            foreach (int i in tuple.Item1)
            {
                rm_person_policy_insured(policy, new Person(i), uid);
            }
            foreach (int i in tuple.Item2)
            {
                add_person_policy_insured(policy, new Person(i), uid);
            }
            //vehicle
            if (policy.vehicles == null) policy.vehicles = new List<Vehicle>();
            tuple = find_diff(Vehicle.toInt(current_insured_vehicles), Vehicle.toInt(policy.vehicles));
            foreach (int i in tuple.Item1)
            {
                rm_vehicle_from_policy(policy, new Vehicle(i), uid);
            }
            foreach (int i in tuple.Item2)
            {
                if ((VehicleModel.isVehUnderActivePolicy(i, policy.startDateTime, policy.endDateTime)) &&
                     !(CompanyModel.CanAutoCancelVehiclePolicy(PolicyModel.GetPolicyCoveredUnder(i, policy.startDateTime, policy.endDateTime).insuredby.CompanyID)))
                    RiskDoubleInsuredEmail(i, policy, false);
                else if ((VehicleModel.isVehUnderActivePolicy(i, policy.startDateTime, policy.endDateTime)) &&
                     (CompanyModel.CanAutoCancelVehiclePolicy(PolicyModel.GetPolicyCoveredUnder(i, policy.startDateTime, policy.endDateTime).insuredby.CompanyID)))
                    RiskDoubleInsuredEmail(i, policy, true);

                addVehicleToPolicy(policy, get_vehicle_from_list(policy.vehicles, i), uid);
            }
            //this is to update any information (change in usage/mortgagee)
            foreach (Vehicle vehicle in policy.vehicles)
            {
                //addVehicleToPolicy(policy, vehicle, uid);
            }

        }

        /// <summary>
        /// this function returns the vehicle with its id matching the sent id
        /// </summary>
        /// <param name="vehicles"></param>
        /// <param name="id"></param>
        private static Vehicle get_vehicle_from_list(List<Vehicle> vehicles, int id)
        {
            Vehicle vehicle = new Vehicle();
            for (int i = 0; i < vehicles.Count(); i++)
            {
                if (vehicles[i].ID == id)
                {
                    vehicle = vehicles[i];
                    break;
                }
            }
            return vehicle;
        }

        /// <summary>
        /// takes two lists of integers and returns two lists that shows 
        /// what's in list 1 that isn't in list 2 and the opposite
        /// </summary>
        /// <param name="current_list"></param>
        /// <param name="new_list"></param>
        /// <returns></returns>
        private static Tuple<List<int>, List<int>> find_diff(List<int> current_list, List<int> new_list)
        {
            List<int> to_be_added = new List<int>();
            List<int> to_be_removed = new List<int>();

            foreach (int i in current_list)
            {
                if (!new_list.Contains(i)) to_be_added.Add(i);
            }

            foreach (int i in new_list)
            {
                if (!current_list.Contains(i)) to_be_removed.Add(i);
            }

            Tuple<List<int>, List<int>> result = new Tuple<List<int>, List<int>>(to_be_added, to_be_removed);
            return result;
        }

        /// <summary>
        /// This function retrieves all the vehicles insured under a particular policy
        /// </summary>
        /// <param name="PolicyId">Policy ID</param>
        /// <returns>A list of vehicles</returns>
        public static List<Vehicle> GetInsuredVehicles(int PolicyId)
        {
            List<Vehicle> vehicles = new List<Vehicle>();
            Usage usage = new Usage();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetInsuredVehicles " + PolicyId, "read", "vehicle", new Vehicle());
                while (reader.Read())
                {
                    Vehicle veh = new Vehicle();
                    veh.ID = int.Parse(reader["VehicleID"].ToString());
                    veh.VIN = reader["VIN"].ToString();
                    veh.chassisno = reader["ChassisNo"].ToString();
                    veh.make = reader["Make"].ToString();
                    veh.VehicleModel = reader["Model"].ToString();
                    if (DBNull.Value != reader["VehicleYear"]) veh.VehicleYear = int.Parse(reader["VehicleYear"].ToString());
                    if (DBNull.Value != reader["VehicleUsage"]) veh.usage = new Usage(int.Parse(reader["VehicleUsage"].ToString()), reader["usage"].ToString());
                    if (DBNull.Value != reader["BodyType"]) veh.bodyType = reader["BodyType"].ToString();
                    veh.VehicleRegNumber = reader["regno"].ToString();
                    if (reader["mid"] != DBNull.Value && reader["mortgagee"] != DBNull.Value) veh.mortgagee = new Mortgagee(int.Parse(reader["mid"].ToString()), reader["mortgagee"].ToString());
                    else veh.mortgagee = new Mortgagee();

                    veh.AuthorizedDrivers = VehicleModel.GetDriverList(veh.ID, PolicyId, 1);
                    veh.ExcludedDrivers = VehicleModel.GetDriverList(veh.ID, PolicyId, 3);
                    veh.ExceptedDrivers = VehicleModel.GetDriverList(veh.ID, PolicyId, 2);

                    vehicles.Add(veh);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return vehicles;
        }

        /// <summary>
        /// This function retrieves all the policies in the system associated with the company of the current user
        /// </summary>
        /// <returns>List of policies</returns>
        public static List<Policy> GetMyCompPolicies()
        {
            List<Policy> result = new List<Policy>();
            int CompanyId = UserModel.GetUser(Constants.user.ID).employee.company.CompanyID;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXECUTE GetMyCompPolicies " + CompanyId, "read", "policy", new Policy());
                while (reader.Read())
                {
                    int ID = int.Parse(reader["PolicyID"].ToString());
                    string cov = reader["Cover"].ToString();
                    string policyPrefix = reader["PolicyPrefix"].ToString();
                    string policyNo = reader["Policyno"].ToString();
                    string EDIID = reader["EDIID"].ToString();
                    result.Add(new Policy(ID, new PolicyCover(policyPrefix, cov), policyNo, EDIID));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function retrieves all the policies in the system associated with the company of the current user, and made by a particular intermediary
        /// </summary>
        /// <param name="id">An insurance company , associated with the intermediary, Id</param>
        /// <returns>List of policies</returns>
        public static List<Policy> GetThisIntermediaryPolicies(int id = 0)
        {
            List<Policy> result = new List<Policy>();
            int company_id = UserModel.GetUser(Constants.user.ID).employee.company.CompanyID;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE GetMyIntermediaryPolicies " + company_id + "," + id;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    int ID = int.Parse(reader["PolicyID"].ToString());
                    string cov = reader["Cover"].ToString();
                    string policyPrefix = reader["PolicyPrefix"].ToString();
                    string policyNo = reader["Policyno"].ToString();
                    string EDIID = reader["EDIID"].ToString();

                    result.Add(new Policy(ID, new PolicyCover(policyPrefix, cov), policyNo, EDIID));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function retrieves all the policies in the system made with the company of the current user- not including those made by the company's intermediaries
        /// </summary>
        /// <param name="id">The company's id</param>
        /// <returns>List of policies</returns>
        public static List<Policy> GetMyPolicies(int id = 0)
        {
            List<Policy> result = new List<Policy>();
            int CompanyId = UserModel.GetUser(Constants.user.ID).employee.company.CompanyID;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE GetMyCompPoliciesOnly " + id;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    int ID = int.Parse(reader["PolicyID"].ToString());
                    string cov = reader["Cover"].ToString();
                    string policyPrefix = reader["PolicyPrefix"].ToString();
                    string policyNo = reader["Policyno"].ToString();
                    string EDIID = reader["EDIID"].ToString();

                    result.Add(new Policy(ID, new PolicyCover(policyPrefix, cov), policyNo, EDIID));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// This function retrieves all the emails of the company administrators, of a particular company, under which a Risk is insured
        /// </summary>
        /// <param name="id">Risk Id</param>
        /// <returns>List of usernames- string</returns>
        public static List<string> getAdmins(int id, string type = null, string start = null, string end = null)
        {
            List<string> admins = new List<string>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader; string query = "";
                switch (type)
                {
                    case "Certificate":
                        query = "EXECUTE GetAdminsWithCertificate " + id;
                        reader = sql.QuerySQL(query);
                        while (reader.Read())
                        {
                            admins.Add(reader["username"].ToString());
                        }
                        reader.Close();
                        break;
                    case "CoverNote":
                        query = "EXECUTE GetAdminsWithCoverNote " + id;
                        reader = sql.QuerySQL(query);
                        while (reader.Read())
                        {
                            admins.Add(reader["username"].ToString());
                        }
                        reader.Close();
                        break;
                    default:
                        DateTime startDate = DateTime.Parse(start); DateTime endDate = DateTime.Parse(end);
                        query = "EXECUTE GetAdmins " + id + ",'" + startDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + endDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                        reader = sql.QuerySQL(query);
                        while (reader.Read())
                        {
                            admins.Add(reader["username"].ToString());
                        }
                        reader.Close();
                        break;
                }
                sql.DisconnectSQL();
            }
            return admins;
        }

        /// <summary>
        /// This function cancels a policy, given the policy Id
        /// </summary>
        /// <param name="id">Policy Id</param>
        /// <returns>boolean value</returns>
        public static bool CancelPolicy(int id, int uid = 0)
        {
            bool result = false;
            sql sql = new sql();

            if (uid == 0) uid = Constants.user.ID;

            if (sql.ConnectSQL())
            {
                //Cancels the policy record
                SqlDataReader reader = sql.QuerySQL("EXECUTE CancelPolicy " + id + "," + uid, "update", "policy", new Policy(id));
                while (reader.Read())
                {
                    result = bool.Parse(reader["result"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();

                try
                {
                    //Cancels all risks that are under the policy being cancelled
                    sql.QuerySQL("EXECUTE CancelPolicyRisks " + id + "," + uid + "," + true);
                    //Cancels all covers that are in force
                    sql.QuerySQL("EXECUTE CancelPolicyCoverandCerts " + id + "," + uid + "," + true);
                }
                catch (Exception)
                {

                }

            }

            try
            {
                CancelPolicyCoverandCerts(id, uid);
            }
            catch (Exception e) { }

            return result;
        }

        public static void Uncancel(int p)
        {

            sql sql = new sql();



            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXECUTE UnCancelPolicy " + p, "update", "policy", new Policy(p));
                while (reader.Read())
                {
                    bool result = bool.Parse(reader["result"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
        }


        /// <summary>
        /// This function gets the creator of a policy
        /// </summary>
        /// <param name="id">Policy Id</param>
        /// <returns>integer - user Id of policy creator</returns>
        public static int GetPolicyCreator(int id)
        {
            int UserID = 0;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXECUTE GetPolicyCreator " + id);
                while (reader.Read())
                {
                    UserID = int.Parse(reader["UserId"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return UserID;
        }

        /// <summary>
        /// this function returns the policy that this vehicle is currently under
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Policy GetVehiclePolicy(int id)
        {
            Policy policy = new Policy(id);
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetVehiclePolicy " + id;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    policy = new Policy(int.Parse(reader["PolicyID"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return policy;
        }

        /// <summary>
        /// this function sends a notification to the administrators of the company
        /// that insured the vehicle in question under another policy
        /// </summary>
        /// <param name="vehId"></param>
        /// <param name="policy"></param>
        public static void RiskDoubleInsuredEmail(int vehId, Policy policy, bool autoCancel, bool import = false, string insuredAt = "")
        {
            List<User> Admins = new List<User>();

            int CurrentCreator = CompanyModel.GetMyCompany(Constants.user).CompanyID;
            int PreviousCreator = PolicyModel.GetPolicyCoveredUnder(vehId, policy.startDateTime, policy.endDateTime).compCreatedBy.CompanyID;
            int PreviousInsurer = CompanyModel.GetInsuranceCo(vehId, policy.startDateTime, policy.endDateTime).CompanyID;

            //The insurance company that insures the new policy being made
            Admins.AddRange(CompanyModel.GetCompanyAdmin(policy.insuredby));

            //So that duplicate messages aren't sent (if the new policy and previous policy were both insured by the same insurer)
            if (PreviousInsurer != policy.insuredby.CompanyID) Admins.AddRange(CompanyModel.GetCompanyAdmin(new Company(PreviousInsurer)));

            //The intermediary creating the new policy (if applicable)
            if (CompanyModel.IsCompanyRegular(CompanyModel.GetMyCompany(Constants.user).CompanyID)) Admins.AddRange(CompanyModel.GetCompanyAdmin(new Company(CurrentCreator)));

            //The intermediary creator of the previous policy (if applicable), also checking that if the previous creator was an intermediary,
            // it isn't the same one that is currently creating the policy (so that duplicates aren't sent)
            if (CompanyModel.IsCompanyRegular(PreviousCreator) && PreviousCreator != CurrentCreator) Admins.AddRange(CompanyModel.GetCompanyAdmin(new Company(PreviousCreator)));

            //SENDING THE MESSAGES TO ALL THE ADMINS INVOLVED
            Notification note = new Notification();
            List<string> to = new List<string>();
            note.NoteTo = new List<User>();
            Vehicle veh = new Vehicle();
            veh = VehicleModel.GetVehicle(vehId);

            for (int p = 0; p < Admins.Count(); p++)
            {
                note.NoteTo.Add(Admins[p]);

                //int userID = UserModel.GetUser(AdminEmails[p].email.ToString()).ID;
                //if (userID != 0)
                //note.NoteTo.Add(new User(userID));
            }

            string companyDets = "";
            if (insuredAt != "") companyDets = "Insured at " + insuredAt;

            if (autoCancel)
            {
                note.heading = "Risk - Auto Cancelled Previous Policy";
                if (import)
                {
                    note.content = "The vehicle, " + veh.VehicleYear + " " + veh.make + " " + veh.VehicleModel + ", with chassis no. '" + veh.chassisno + "' has been automatically cancelled under a previous policy (" + companyDets + ") and is now active under a new policy.\n" +
                                   "Vist  Policy/Details/" + policy.ID + " in the system to see the policy.";
                }
                else
                {
                    note.content = "The vehicle, " + veh.VehicleYear + " " + veh.make + " " + veh.VehicleModel + ", with chassis no. '" + veh.chassisno + "' has been automatically cancelled under a previous policy (" + companyDets + ") and is now active under a new policy.\n" +
                                    "Vist " + System.Web.HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + System.Web.HttpContext.Current.Request.Url.Host + (System.Web.HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + System.Web.HttpContext.Current.Request.Url.Port) + "/Policy/Details/" + policy.ID +
                                    " to see the policy.";
                }
            }
            else
            {
                note.heading = "Risk - Double Insured";
                if (import)
                {
                    note.content = "The vehicle, " + veh.VehicleYear + " " + veh.make + " " + veh.VehicleModel + ", with chassis no. '" + veh.chassisno + "' is now under more than one policy, within the time frame: " + policy.startDateTime.ToString("yyyy-MM-dd") + " to: " + policy.endDateTime.ToString("yyyy-MM-dd") +
                                   "\n" + companyDets + "\nVisit  Policy/Details/" + policy.ID + " in the system to see the policy.";
                }
                else
                {

                    note.content = "The vehicle, " + veh.VehicleYear + " " + veh.make + " " + veh.VehicleModel + ", with chassis no. '" + veh.chassisno + "' is now under more than one policy, within the time frame: " + policy.startDateTime.ToString("yyyy-MM-dd") + " to: " + policy.endDateTime.ToString("yyyy-MM-dd") +
                                    "\n" + companyDets + "\nVisit " + System.Web.HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + System.Web.HttpContext.Current.Request.Url.Host + (System.Web.HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + System.Web.HttpContext.Current.Request.Url.Port) + "/Policy/Details/" + policy.ID +
                                    " to see the policy.";
                }
            }
            note.NoteFrom = new User(Constants.user.ID);
            for (int x = 0; x < Admins.Count(); x++)
                to.Add(Admins[x].username);

           //MailModel.SendMailWithThisFunction(Admins, new Mail(to, note.heading, note.content));
        }

        /// <summary>
        /// this function activates a policy after it was cancelled before
        /// </summary>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static bool activate_policy(Policy policy)
        {
            if (CanIViewPolicy(policy.ID))
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    SqlDataReader reader = sql.QuerySQL("EXEC activate_policy " + policy.ID + "," + Constants.user.ID, "update", "policy", policy);
                    reader.Close();
                    sql.DisconnectSQL();
                    return true;
                }
                else return false;
            }
            else return false;
        }

        /// <summary>
        /// This function determines if a policy number already exists in the system
        /// </summary>
        /// <param name="id">Policy Number</param>
        /// <returns>boolean </returns>
        public static bool PolicyNoExist(string PolicyNumber, int insurance_company, int polId = 0)
        {
            bool found = false;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE DoesPolicyNoExist '" + Constants.CleanString(PolicyNumber) + "'," + polId + "," + insurance_company;
                //if (polId != 0) query += "," + polId;
                //SqlDataReader reader = sql.QuerySQL(query, "read", "policy", new Policy());
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    found = bool.Parse(reader["result"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return found;

        }


        /// <summary>
        /// this function gets broker usernames
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>List</returns>
        public static List<List<string>> getBrokers(int id, string type = null, string start = null, string end = null)
        {
            List<List<string>> brokers = new List<List<string>>();


            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader; string query = "";
                switch (type)
                {
                    case "Certificate":
                        query = "EXECUTE GetBrokersWithCertificates " + id;
                        reader = sql.QuerySQL(query);

                        while (reader.Read())
                        {
                            List<string> people = new List<string>();
                            people.Add(reader["username"].ToString());
                            people.Add(reader["name"].ToString());

                            brokers.Add(people);

                        }
                        reader.Close();
                        break;
                    case "CoverNote":
                        query = "EXECUTE GetBrokersWithCoverNote " + id;
                        reader = sql.QuerySQL(query);

                        while (reader.Read())
                        {
                            List<string> people = new List<string>();
                            people.Add(reader["username"].ToString());
                            people.Add(reader["name"].ToString());

                            brokers.Add(people);

                        }
                        reader.Close();
                        break;
                    default:
                        DateTime startDate = DateTime.Parse(start); DateTime endDate = DateTime.Parse(end);

                        query = "EXECUTE GetAdmins " + id + ",'" + startDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + endDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                        reader = sql.QuerySQL(query);

                        while (reader.Read())
                        {
                            List<string> people = new List<string>();
                            people.Add(reader["username"].ToString());
                            people.Add(reader["name"].ToString());
                            brokers.Add(people);
                        }
                        reader.Close();
                        break;
                }
                sql.DisconnectSQL();
            }
            return brokers;

        }

        public static List<Policy> GetPoliciesWithCompAndVehicle(int compid, int vehid)
        {
            List<Policy> policies = new List<Policy>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetPoliciesWithCompAndVehicle " + compid + "," + vehid;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    Policy p = new Policy(int.Parse(reader["PolicyID"].ToString()), reader["Policyno"].ToString(), DateTime.Parse(reader["StartDateTime"].ToString()), DateTime.Parse(reader["endDateTime"].ToString()));
                    p.insuredby = new Company();
                    p.insuredby.compshort = reader["cshort"].ToString();
                    policies.Add(p);
                }
            }
            return policies;
        }

        public static bool PolicyExists(Policy pol)
        {
            bool result = false;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC PolicyExists '" + Constants.CleanString(pol.EDIID) + "','"
                                                     + Constants.CleanString(pol.policyNumber) + "',"
                                                     + pol.policyCover.ID;
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    result = bool.Parse(reader["result"].ToString());
                }
            }
            return result;
        }

        /// <summary>
        /// this function returns the billing address of a policy
        /// </summary>
        /// <param name="policy_id"></param>
        /// <returns></returns>
        public static Address get_policy_billing_address(int policy_id)
        {
            Address address = new Address();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC get_policy_billing_address " + policy_id);
                while (reader.Read())
                {
                    if (bool.Parse(reader["exists"].ToString()))
                    {
                        address.ID = int.Parse(reader["address_id"].ToString());
                        if (DBNull.Value != reader["billing_address_short"]) address.short_code = reader["billing_address_short"].ToString();
                        if (DBNull.Value != reader["road_number"]) address.roadnumber = reader["road_number"].ToString();
                        if (DBNull.Value != reader["apt_number"]) address.ApartmentNumber = reader["apt_number"].ToString();
                        if (DBNull.Value != reader["road_name"]) address.road = new Road(reader["road_name"].ToString());
                        else address.road = new Road();
                        if (DBNull.Value != reader["road_type"]) address.roadtype = new RoadType(reader["road_type"].ToString());
                        else address.roadtype = new RoadType();
                        address.city = new City(reader["city"].ToString());
                        if (DBNull.Value != reader["pid"]) address.parish = new Parish(int.Parse(reader["pid"].ToString()), reader["parish"].ToString());
                        else address.parish = new Parish();
                        address.country = new Country(reader["country"].ToString());
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return address;
        }

        /// <summary>
        /// This function determines if a broker number(number from broker on import) already exists in the system
        /// </summary>
        /// <param name="id">Broker Policy Number</param>
        /// <returns>PolicyID</returns>
        public static int BrokerNoExist(string BrokerNumber, int insurance_company, int polId = 0)
        {
            int polID = 0;

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXECUTE DoesBrokerNoExist '" + Constants.CleanString(BrokerNumber) + "'," + polId + "," + insurance_company;
                //if (polId != 0) query += "," + polId;
                //SqlDataReader reader = sql.QuerySQL(query, "read", "policy", new Policy());
                SqlDataReader reader = sql.QuerySQL(query);
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        if (DBNull.Value != reader["PolicyID"])
                            polID = int.Parse(reader["PolicyID"].ToString());
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return polID;

        }

        public static List<Policy> GetCompanyPolicies(int compID, DateTime start, DateTime end, string typeOfDate = null)
        {
            List<Policy> policies = new List<Policy>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                if (typeOfDate == null) typeOfDate = "created";
                //string query = "EXEC GetCompanyPolicies " + compID + ",'" + typeOfDate + "'";
                string query = "EXEC GetCompanyPoliciesNew " + compID + ",'" + typeOfDate + "'";
                if ((start == DateTime.Parse("1969-12-31 00:00:00") || (start == DateTime.Parse("1969-12-31 19:00:00")) && (end == DateTime.Parse("1969-12-31 00:00:00") || end == DateTime.Parse("1969-12-31 19:00:00"))))
                {
                    query += ",'" + null + "','" + null + "'";
                }
                else
                    query += ",'" + start.ToString("yyyy-MM-dd HH:mm:ss") + "','" + end.ToString("yyyy-MM-dd HH:mm:ss") + "'";

                SqlDataReader reader = sql.QuerySQL(query);
                Policy pol = new Policy();
                while (reader.Read())
                {
                    pol = new Policy(int.Parse(reader["PolicyID"].ToString()),
                                    reader["Policyno"].ToString(),
                                    DateTime.Parse(reader["StartDateTime"].ToString()),
                                    DateTime.Parse(reader["EndDateTime"].ToString()));
                    pol.insuredType = reader["InsuredType"].ToString();
                    pol.policyCover = new PolicyCover();
                    pol.policyCover.prefix = reader["Prefix"].ToString();
                    pol.imported = bool.Parse(reader["imported"].ToString());



                    policies.Add(pol);
                    pol = new Policy();
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return policies;
        }


        public static void CancelPolicyCoverandCerts(int polID, int uid = 0)
        {
            sql sql = new sql();
            string query = "";

            if (uid == 0) uid = Constants.user.ID;
            if (sql.ConnectSQL())
            {
                query = "EXEC CancelPolicyCoverandCerts " + polID + "," + uid;
                SqlDataReader reader = sql.QuerySQL(query);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        public static List<int> GetPolicyFromChassis(string chassiss, string trn)
        {
            List<int> policyIDs = new List<int>();
            sql sql = new sql();
            string query = "";

            if (sql.ConnectSQL())
            {
                query = "EXEC GetPolicyFromChassis '" + chassiss + "','" + trn + "'";
                SqlDataReader reader = sql.QuerySQL(query);
                while (reader.Read())
                {
                    policyIDs.Add(int.Parse(reader["PolicyID"].ToString()));
                }
                reader.Close();
                sql.DisconnectSQL();
            }


            return policyIDs;
        }

    }
}


/// <summary>
/// Policy Record Object
/// </summary>
public class PolicyCheckRecord
{
    public int PolRecID { set; get; }
    public int CompanyID { set; get; }
    public string InsurerID { set; get; }
    public string CompanyName { set; get; }
    public string PolicyID { set; get; }
    public string RegNo { set; get; }
    public string ChassisNo { set; get; }
    public string ExpirationDate { set; get; }
    public bool? IsActive { set; get; }
    public string CoverType { set; get; }
    public string UniqueCoverNumber { set; get; }
    public string CreatedByUser { set; get; }
    public string UserID { set; get; }

    public static string GetConn()
    {
        #if DEBUG
           return System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;//ApplicationServices
        #else
           return     System.Configuration.ConfigurationManager.ConnectionStrings["ExternalConnection"].ConnectionString; 
        #endif
    }

}

