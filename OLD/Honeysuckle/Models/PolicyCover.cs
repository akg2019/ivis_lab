﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;

namespace Honeysuckle.Models
{
    public class PolicyCover
    {
        public int ID { get; set; }

        [RegularExpression("[a-zA-Z0-9 ][ a-zA-Z0-9-]*[a-zA-Z0-9 ]", ErrorMessage = "Incorrect policy cover format  (Only letters, numbers and hyphens allowed). Do not start or end the cover with a hypen please.")]
        public string prefix { get; set; }
        public string cover { get; set; }
        public bool isCommercialBusiness { get; set; }
        public List<Usage> usages { get; set; }
        public bool can_be_deleted { get; set; }
        public bool IsMotorTrade { get; set; }
        public bool IsActive { get; set; }

        public PolicyCover() { }

        public PolicyCover(string cover) 
        {
            this.cover = cover;
        }
        public PolicyCover(string prefix, string cover)
        {
            this.prefix = prefix;
            this.cover = cover;
        }
        public PolicyCover(int ID)
        {
            this.ID = ID;
        }
        public PolicyCover(int ID, string cover)
        {
            this.ID = ID;
            this.cover = cover;
        }
        public PolicyCover(int ID, string prefix, string cover)
        {
            this.ID = ID;
            this.prefix = prefix;
            this.cover = cover;
        }
        public PolicyCover(string cover, bool isCommercialBusiness)
        {
            this.cover = cover;
            this.isCommercialBusiness = isCommercialBusiness;
        }
        public PolicyCover(int Id, string cover, bool isCommercialBusiness)
        {
            this.ID = Id;
            this.cover = cover;
            this.isCommercialBusiness = isCommercialBusiness;
        }
        public PolicyCover(int Id, string prefix, string cover, bool isCommercialBusiness)
        {
            this.ID = Id;
            this.prefix = prefix;
            this.cover = cover;
            this.isCommercialBusiness = isCommercialBusiness;
        }
        public PolicyCover(int ID, string cover, bool isCommmercialBusiness, List<Usage> usages)
        {
            this.ID = ID;
            this.cover = cover;
            this.isCommercialBusiness = isCommercialBusiness;
            this.usages = usages;
        }
        public PolicyCover(int ID, string prefix, string cover, bool isCommmercialBusiness, List<Usage> usages)
        {
            this.ID = ID;
            this.prefix = prefix;
            this.cover = cover;
            this.isCommercialBusiness = isCommercialBusiness;
            this.usages = usages;
        }
        public PolicyCover(int ID, string prefix, string cover, bool isCommmercialBusiness, bool IsMotorTrade)
        {
            this.ID = ID;
            this.prefix = prefix;
            this.cover = cover;
            this.isCommercialBusiness = isCommercialBusiness;
            this.usages = usages;
            this.IsMotorTrade = IsMotorTrade;
        }
        public PolicyCover(int ID, string prefix, string cover, bool isCommmercialBusiness, bool IsMotorTrade,bool IsActive)
        {
            this.ID = ID;
            this.prefix = prefix;
            this.cover = cover;
            this.isCommercialBusiness = isCommmercialBusiness;
            this.usages = usages;
            this.IsMotorTrade = IsMotorTrade;
            this.IsActive = IsActive;
        }

    }


    public class PolicyCoverModel
    {
        /// <summary>
        /// this is on view (DONT DELETE)
        /// this function checks to see if a policy cover can be deleted
        /// </summary>
        /// <param name="cover"></param>
        /// <returns></returns>
        public static bool CanCoverBeDeleted(PolicyCover cover)
        {
            sql sql = new sql();
            bool result = false;
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC CanCoverBeDeleted " + cover.ID);
                while (reader.Read())
                {
                    result = (bool)reader["result"];
                }
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function updates the usages as it relates to the a specific cover
        /// </summary>
        /// <param name="cover"></param>
        public static void UpdateUsagesInCover(PolicyCover cover)
        {
            UpdateUsagesInCover(cover, Constants.user.ID, "",0);
        }

        /// <summary>
        /// this function updates the usages as it relates to the a specific cover
        /// </summary>
        /// <param name="cover"></param>
        /// <param name="uid"></param>
        public static void UpdateUsagesInCover(PolicyCover cover, int uid, string CertType, int COmpanyID)
        {
            List<int> systemUsageID = new List<int>();
            List<int> newUsageID = new List<int>();
            
            //DeleteAllUsagesToCovers(cover, uid); // to be changed

            systemUsageID = GetUsagesWithCover(cover);
            foreach (Usage u in cover.usages)
                newUsageID.Add(u.ID);


            Tuple<List<int>, List<int>> difference = find_diff(systemUsageID, newUsageID);
            //DeleteAllUsagesToCovers()
            int count = 0;
            foreach (Usage u in cover.usages)
            {
                if (newUsageID.Count() != 0)
                {
                    if(difference.Item2.Count != 0)
                    AssociateUsageToCover(cover.ID, difference.Item2.ElementAt(count), uid, CertType,COmpanyID);
                }
                else AssociateUsageToCover(cover.ID, UsageModel.CreateUsage(u).ID, uid, CertType, COmpanyID);
            }
        }

        /// <summary>
        /// this function adds a cover/usage association
        /// </summary>
        /// <param name="coverid"></param>
        /// <param name="usageid"></param>
        public static void AssociateUsageToCover(int coverid, int usageid, string CertType, int COmpanyID)
        {
            AssociateUsageToCover(coverid, usageid, Constants.user.ID, CertType,COmpanyID);
        }
        
        /// <summary>
        /// this function adds a cover/usage association
        /// </summary>
        /// <param name="coverid"></param>
        /// <param name="usageid"></param>
        public static void AssociateUsageToCover(int coverid, int usageid, int uid, string CertType, int COmpanyID)
        {
            //sql sql = new sql();
            //if (sql.ConnectSQL() && usageid != 0)
            //{
              
            //}
              if (COmpanyID == 0)
                {
                    string Query = "EXEC AssociateUsageToCover " + coverid + "," + usageid + "," + uid + ",'" + CertType + "'";
                    OmaxFramework.Utilities.OmaxData.HandleQuery(Query, sql.GetCurrentCOnnectionString(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.NoneQuery);
                   
                
                }
                else {
                    string Query5 = "EXEC AssociateUsageToCover1 " + coverid + "," + usageid + "," + uid + ",'" + CertType + "'," + COmpanyID;
                    OmaxFramework.Utilities.OmaxData.HandleQuery(Query5, sql.GetCurrentCOnnectionString(), OmaxFramework.Utilities.OmaxData.DataBaseType.Sql, OmaxFramework.Utilities.OmaxData.QueryType.NoneQuery);
                   
                
                }
               
        }

        /// <summary>
        /// this function removes all usages from a specific cover
        /// </summary>
        /// <param name="cover"></param>
        /// <param name="uid"></param>
        public static void DeleteAllUsagesToCovers(PolicyCover cover, int uid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DeleteAllUsagesToCovers " + cover.ID + "," + uid;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "usage", cover);
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// this function receive usage IDs 
        /// </summary>
        /// <param name="cover"></param>
        public static List<int> GetUsagesWithCover(PolicyCover cover)
        {
            List<int> UsageIdList = new List<int>();
       
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string query = "EXEC GetUsagesWithCover " + cover.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "usage", cover);

                while (reader.Read())
                {
                    try { UsageIdList.Add(int.Parse(reader["UsageID"].ToString())); }
                    catch (Exception) { }
                }

            }

            return UsageIdList;
        }

        /// <summary>
        /// this function removes a cover from the system
        /// </summary>
        /// <param name="cover"></param>
        /// <returns></returns>
        public static bool DeleteCover(PolicyCover cover)
        {
            bool result = false;
            sql sql = new sql();
            if (CanCoverBeDeleted(cover))
            {
                if (sql.ConnectSQL())
                {
                    SqlDataReader reader = sql.QuerySQL("EXEC DeleteCover " + cover.ID + "," + Constants.user.ID, "delete", "policycover", cover);
                    while (reader.Read())
                    {
                        result = (bool)reader["result"];
                    }
                    reader.Close();
                    sql.DisconnectSQL();
                }
            }
            return result;
        }
        
        /// <summary>
        /// this function updates all cover information in the database
        /// </summary>
        /// <param name="cover"></param>
        public static void UpdateCover(PolicyCover cover)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                try
                {
                    string query = "EXEC UpdateCover " + cover.ID + ",'"
                                                 + Constants.CleanString(cover.prefix) + "','"
                                                 + Constants.CleanString(cover.cover) + "',"
                                                 + cover.isCommercialBusiness + ","
                                                 + Constants.user.ID.ToString() + "," + cover.IsMotorTrade + "," + cover.IsActive;
                    SqlDataReader reader = sql.QuerySQL(query, "update", "policycover", cover);

                    try
                    {
                        reader.Close();
                        sql.DisconnectSQL();
                    }
                    catch (Exception)
                    {

                    }
                }
                catch (Exception)
                {
                  
                }
               
            }
        }

        /// <summary>
        /// this function retrieves all the policy covers in the system
        /// </summary>
        /// <returns></returns>
        public static List<PolicyCover> GetAllPolicyCovers()
        {
            List<PolicyCover> covers = new List<PolicyCover>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetAllPolicyCovers", "read", "policycover", new PolicyCover());
                while (reader.Read())
                {
                    PolicyCover cover = new PolicyCover(int.Parse(reader["ID"].ToString()), reader["prefix"].ToString(), reader["cover"].ToString(), (bool)reader["IsCommercialBusiness"]);
                    cover.can_be_deleted = CanCoverBeDeleted(cover);
                    covers.Add(cover);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return covers;
        }

        /// <summary>
        /// this function retrieves all the policy covers in the system for your company
        /// </summary>
        /// <returns></returns>
        public static List<PolicyCover> GetAllPolicyCoversForCompany()
        {
            List<PolicyCover> covers = new List<PolicyCover>();
            sql sql = new sql();
            SqlDataReader reader;
            if (sql.ConnectSQL() && UserModel.TestGUID(Constants.user))
            {
                if (Constants.user.newRoleMode) reader = sql.QuerySQL("EXEC GetAllPolicyCoversForCompany " + Constants.user.tempUserGroup.company.CompanyID, "read", "policycover", new PolicyCover());
                else reader = sql.QuerySQL("EXEC GetAllPolicyCoversForCompany " + CompanyModel.GetMyCompany(Constants.user).CompanyID, "read", "policycover", new PolicyCover());

                while (reader.Read())
                {
                        bool IsMotorTrade = false;
                        bool IsCommercial = false;
                        bool IsActive = false;

                        try{bool.TryParse(reader["IsMotorTrade"].ToString(), out IsMotorTrade);}catch(Exception){}
                        try{bool.TryParse(reader["IsCommercialBusiness"].ToString(), out IsCommercial);}catch(Exception){}
                        try { bool.TryParse(reader["IsActive"].ToString(), out IsActive); }catch(Exception){}

                        PolicyCover cover = new PolicyCover(int.Parse(reader["ID"].ToString()), reader["prefix"].ToString(), reader["cover"].ToString(), IsCommercial, IsMotorTrade, IsActive);
                        cover.can_be_deleted = CanCoverBeDeleted(cover);
                        covers.Add(cover);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return covers.OrderBy(p => p.prefix).ToList();
        }

        /// <summary>
        /// This function adds a new policy cover to the system
        /// </summary>
        /// <param name="policyCover">Policy cover name </param>
        public static PolicyCover addPolicyCover(PolicyCover policyCover)
        {
            return addPolicyCover(policyCover, Constants.user.ID, CompanyModel.GetMyCompany(Constants.user).CompanyID);
        }

        /// <summary>
        /// this function adds a new policy cover to the system
        /// </summary>
        /// <param name="policyCover"></param>
        /// <param name="uid"></param>
        /// <param name="companyid"></param>
        /// <returns></returns>
        public static PolicyCover addPolicyCover(PolicyCover policyCover, int uid, int companyid)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                //If statements to determine if isCommercialBusiness is true or false
                //policyCover.isCommercialBusiness = true; // will change depending on cover

                string query = "EXECUTE AddPolicyCover2 '" + Constants.CleanString(policyCover.prefix) + "','"
                                                            + Constants.CleanString(policyCover.cover) + "',"
                                                            + policyCover.isCommercialBusiness + ","
                                                            + uid + ","
                                                            + companyid.ToString() + "," + policyCover.IsMotorTrade + "," + policyCover.IsActive;
                SqlDataReader reader = sql.QuerySQL(query, "create", "policycover", policyCover);

                while (reader.Read())
                {
                    policyCover.ID = int.Parse(reader["ID"].ToString());
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return policyCover;
        }

        /// <summary>
        /// This function gets a list of all existing policy covers in the system
        /// </summary>
        /// <returns>List of policy covers</returns>
        public static List<PolicyCover> GetPolicyCovers(int compID, bool All = false)
        {
            List<PolicyCover> result = new List<PolicyCover>();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string Query = "";
                if (All == true) Query = "EXECUTE GetCoversList2 " + compID; else Query = "EXECUTE GetCoversList " + compID;
                SqlDataReader reader = sql.QuerySQL(Query);
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        string cov = (reader["Cover"].ToString() + " ( " + reader["Prefix"].ToString() + " )");
                        int Id = int.Parse(reader["ID"].ToString());
                        result.Add(new PolicyCover(Id, cov));
                    }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return result;
        }

        /// <summary>
        /// this function retrieves a policy cover based on it's database id
        /// </summary>
        /// <param name="cover"></param>
        /// <returns></returns>
        public static PolicyCover GetCoverByID(PolicyCover cover)
        {

            try
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    SqlDataReader reader = sql.QuerySQL("EXEC GetCoverByID " + cover.ID, "read", "policycover", new PolicyCover());
                    while (reader.Read())
                    {
                        bool IsMotorTrade = false;
                        bool IsCommercial = false;
                        bool IsActive = false;

                        try { bool.TryParse(reader["IsMotorTrade"].ToString(), out IsMotorTrade); }catch(Exception){}
                        try { bool.TryParse(reader["IsCommercialBusiness"].ToString(), out IsCommercial); }catch(Exception){}
                        try { bool.TryParse(reader["IsActive"].ToString(), out IsActive); }catch(Exception){}

                        cover = new PolicyCover(cover.ID, reader["prefix"].ToString(), reader["cover"].ToString(), IsCommercial, IsMotorTrade, IsActive);
                    }
                    reader.Close();
                    cover.usages = UsageModel.GetPolicyCoverUsages(cover);
                    sql.DisconnectSQL();
                }
            }
            catch (Exception)
            {

            }

            return cover;
        }


        /// <summary>
        /// Gets policy cover by description
        /// </summary>
        /// <param name="Description"></param>
        /// <param name="CompanyID"></param>
        /// <returns></returns>
        public static PolicyCover GetCoverByCoverDescription(string Description, int CompanyID)
        {
            PolicyCover cover = new PolicyCover();
            try
            {
                sql sql = new sql();
                if (sql.ConnectSQL())
                {
                    SqlDataReader reader = sql.QuerySQL("select top 1 * from PolicyCovers p where p.Prefix = '" + Description + "' and p.CompanyID = " + CompanyID, "read", "policycover", new PolicyCover());
                    while (reader.Read())
                    {
                        bool IsMotorTrade = false;
                        bool IsCommercial = false;
                        bool IsActive = false;
                        int ID = 0;

                        bool.TryParse(reader["IsMotorTrade"].ToString(), out IsMotorTrade);
                        bool.TryParse(reader["IsCommercialBusiness"].ToString(), out IsCommercial);
                        bool.TryParse(reader["IsActive"].ToString(), out IsActive);
                        int.TryParse(reader["ID"].ToString(), out ID);

                        cover = new PolicyCover(ID, reader["Prefix"].ToString(), reader["Cover"].ToString(), IsCommercial, IsMotorTrade, IsActive);
                    }
                    reader.Close();
                    cover.usages = UsageModel.GetPolicyCoverUsages(cover);
                    sql.DisconnectSQL();
                }
            }
            catch (Exception)
            {

            }

            return cover;
        }

        /// <summary>
        /// this function retrieves the policy prefix of a cover by it's database id
        /// </summary>
        /// <param name="cover"></param>
        /// <returns></returns>
        public static string GetPolicyPrefix(PolicyCover cover)
        {
            string prefix = null;
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetPolicyPrefix " + cover.ID, "read", "policycover", new PolicyCover());
                while (reader.Read())
                {
                    prefix = reader["prefix"].ToString();
                }
                sql.DisconnectSQL();
            }
            return prefix;
        }

        /// <summary>
        /// this function discovers the difference between two lists of integers
        /// </summary>
        /// <param name="current_list"></param>
        /// <param name="new_list"></param>
        /// <param name="imported"></param>
        /// <returns></returns>
        private static Tuple<List<int>, List<int>> find_diff(List<int> current_list, List<int> new_list, bool imported = false)
        {
            List<int> to_be_added = new List<int>();
            List<int> to_be_removed = new List<int>();

            foreach (int i in current_list)
            {
                if (!new_list.Contains(i)) to_be_added.Add(i);
            }

            if (!imported)
            {
                foreach (int i in new_list)
                {
                    if (!current_list.Contains(i)) to_be_removed.Add(i);
                }
            }

            Tuple<List<int>, List<int>> result = new Tuple<List<int>, List<int>>(to_be_added, to_be_removed);
            return result;
        }

    }
}
