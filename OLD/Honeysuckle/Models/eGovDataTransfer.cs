﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using OmaxFramework;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class eGovDataTransfer
    {

        /// <summary>
        /// Gets exposes the database connectionstring
        /// </summary>
        /// <returns></returns>
        public string ConnectionString()
        {
            string sqlConnectionString = "";
            #if DEBUG
               return sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;//ApplicationServices
            #else
               return sqlConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ExternalConnection"].ConnectionString;
            #endif
        }

        /// <summary>
        /// Policy Main Object
        /// </summary>
        public class PolicyData
        {
            public string edi_system_id { set; get; }
            public string insurer_trn { set; get; }
            public string broker_trn { set; get; }
            public string start_date_time { set; get; }
            public string created_on { set; get; }
            public string end_date_time { set; get; }
            public string policy_prefix { set; get; }
            public string broker_policy_number { set; get; }
            public string policy_number { set; get; }
            public string policy_cover { set; get; }
            public bool empty_policy_number { set; get; }
            public bool is_commercial_business { set; get; }
            public string address_on_policy { set; get; }
            public string InsuredType { set; get; }
            public int policyID { set; get; }
            public int CompanyID { set; get; }
            public string LastModifiedOn { set; get; }
            public string mailing_address { set; get; }
            public string billing_address { set; get; }
        }

        /// <summary>
        /// Policy Company Insured Object
        /// </summary>
        public class policyInsuredCompany
        {
            public string edi_system_id { set; get; }
            public string name { set; get; }
            public string trn { set; get; }
            public string address_on_policy { set; get; }

        }

        /// <summary>
        /// Policy Person Insured Object
        /// </summary>
        public class policyInsuredIndvidual
        {
            public string edi_system_id { set; get; }
            public string first_name { set; get; }
            public string middle_name { set; get; }
            public string last_name { set; get; }
            public string trn { set; get; }
            public string address_on_policy { set; get; }

        }

        /// <summary>
        /// Risk Drivers
        /// </summary>
        public class RiskDrivers
        {
            public string edi_system_id { set; get; }
            public string first_name { set; get; }
            public string middle_name { set; get; }
            public string last_name { set; get; }
            public string date_of_birth { set; get; }
            public string date_license_issued { set; get; }
            public string type_of_license { set; get; }
            public string id { set; get; }
            public string trn { set; get; }
        }

        /// <summary>
        /// Authorized Drivers
        /// </summary>
        public class authorized_drivers
        {
            public string edi_system_id { set; get; }
            public string first_name { set; get; }
            public string middle_name { set; get; }
            public string last_name { set; get; }
            public string date_of_birth { set; get; }
            public string date_license_issued { set; get; }
            public string type_of_license { set; get; }
            public string id { set; get; }
            public string trn { set; get; }
        }

        /// <summary>
        /// Excluded Drivers
        /// </summary>
        public class excluded_drivers
        {
            public string edi_system_id { set; get; }
            public string first_name { set; get; }
            public string middle_name { set; get; }
            public string last_name { set; get; }
            public string date_of_birth { set; get; }
            public string date_license_issued { set; get; }
            public string type_of_license { set; get; }
            public string id { set; get; }
            public string trn { set; get; }
        }

        /// <summary>
        /// Exceptedt Drivers
        /// </summary>
        public class excepted_drivers
        {
            public string edi_system_id { set; get; }
            public string first_name { set; get; }
            public string middle_name { set; get; }
            public string last_name { set; get; }
            public string date_of_birth { set; get; }
            public string date_license_issued { set; get; }
            public string type_of_license { set; get; }
            public string id { set; get; }
            public string trn { set; get; }
        }


        /// <summary>
        /// Main Driver Trn
        /// </summary>
        public class main_driver_trns
        {
            public string id { set; get; }
            public string trn { set; get; }
        }

        /// <summary>
        /// Risk Vehicle
        /// </summary>
        public class RsikVehicle
        {
            public string edi_system_id { set; get; }
            public string make { set; get; }
            public string model { set; get; }
            public string model_type { set; get; }
            public string colour { set; get; }
            public string extension { set; get; }
            public string reference_number { set; get; }
            public string transmission_type { set; get; }
            public string roof_type { set; get; }
            public string tonnage { set; get; }
            public string body_type { set; get; }
            public string registration_number { set; get; }
            public string chassis_number { set; get; }
            public string left_hand_drive { set; get; }
            public string right_hand_drive { set; get; }
            public string engine_modified { set; get; }
            public string engine_number { set; get; }
            public main_driver_trns main_driver_trn { set; get; }
            public string main_driver { set; get; }
            public string hpcc { set; get; }
            public string usage { set; get; }
            public string vin { set; get; }
            public string restricted_driving { set; get; }
            public string seating { set; get; }
            public string year { set; get; }

        }


        /// <summary>
        /// Risk Cover Notes
        /// </summary>
        public class RiskCoverNote
        {
            public string edi_system_id { set; get; }
            public string alterations { set; get; }
            public string cancelled { set; get; }
            public string printed_paper_number { set; get; }
            public string manual_cover_note_id { set; get; }
            public string endorsement_number { set; get; }
            public string mortgagees { set; get; }
            public string limitations_of_use { set; get; }
            public string date_first_printed { set; get; }
            public string last_printed_on_at { set; get; }
            public string last_printed_by_plain_text { set; get; }
            public string cover_note_id { set; get; }
            public string effective_date_time { set; get; }
            public string expiry_date { set; get; }
            public string days_effective { set; get; }
            public string authorized_drivers_wording { set; get; }
            public string printed_count { set; get; }
            public string chassis_number { set; get; }
        }


        /// <summary>
        /// Risk Certificates
        /// </summary>
        public class RiskCertificate
        {
            public string edi_system_id { set; get; }
            public string alterations { set; get; }
            public bool cancelled { set; get; }
            public string printed_paper_number { set; get; }
            public string endorsement_number { set; get; }
            public string limitations_of_use { set; get; }
            public string date_first_printed { set; get; }
            public string date_time_last_printed_on { set; get; }
            public string last_printed_by_plain_text { set; get; }
            public string certificate_no { set; get; }
            public string certificate_type { set; get; }
            public string effective_date { set; get; }
            public string expiry_date { set; get; }
            public string authorized_drivers_wording { set; get; }
            public string chassis_number { set; get; }
            public string marks_and_registration_no { set; get; }
        }

        /// <summary>
        /// This function retrieves all the insured individuals associated with a particular policy
        /// </summary>
        /// <param name="Id">Policy ID</param>
        /// <returns>A list of people insured to a Risk- and policy</returns>
        public static List<Person> GetInsuredList(int PolicyId)
        {
            List<Person> persons = new List<Person>();
            Address add = new Address();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetInsured " + PolicyId, "read", "person", new Person());
                while (reader.Read())
                {
                    Person per = new Person();
                    per.PersonID = int.Parse(reader["PeopleID"].ToString());
                    per.fname = reader["FName"].ToString();
                    per.lname = reader["LName"].ToString();
                    per.TRN = reader["TRN"].ToString();

                    add.city = new City(reader["City"].ToString());
                    add.roadnumber = reader["RoadNumber"].ToString();
                    add.road = new Road(reader["RoadName"].ToString());
                    add.country = new Country(reader["CountryName"].ToString());

                    per.address = add;

                    persons.Add(per);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return persons;
        }

        /// <summary>
        /// This function retrieves all the insured companies associated with a particular policy
        /// </summary>
        /// <param name="Id">Policy ID</param>
        /// <returns>A list of companies insured to a Risk- and policy</returns>
        public static List<Company> GetInsuredCompanies(int PolicyId)
        {
            List<Company> companies = new List<Company>();
            Address add = new Address();

            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetInsuredCompanies " + PolicyId, "read", "company", new Company());
                while (reader.Read())
                {
                    Company com = new Company();
                    com.CompanyID = int.Parse(reader["CompanyID"].ToString());
                    com.CompanyName = reader["CompanyName"].ToString();
                    com.IAJID = reader["IAJID"].ToString();

                    add.city = new City(reader["City"].ToString());
                    add.roadnumber = reader["RoadNumber"].ToString();
                    add.road = new Road(reader["RoadName"].ToString());
                    add.country = new Country(reader["CountryName"].ToString());

                    com.CompanyAddress = add;
                    companies.Add(com);
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return companies;
        }

        public JObject GeteGovPolicyData(string Fromdate = "", string Todate = "", int PageSize = 100, int PageNumber = 1)
        {
            JObject PolicyDataPayload = new JObject();
            JArray Policies = new JArray();

            //Build Payload
            List<PolicyData> pol = new List<PolicyData>();

            DateTime F, T;
            DateTime.TryParse(Fromdate, out F);
            DateTime.TryParse(Todate, out T);

            int TotalPages = 0;
            
            //Get policy Data
            JArray dt = JArray.Parse(JsonConvert.SerializeObject((DataTable)Utilities.OmaxData.HandleQuery("Exec epic_eGovGetPoliciesModified '" + F.ToString() + "', '" + T.ToString() + "'," + PageNumber.ToString() + "," + PageSize.ToString(), ConnectionString(), Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery)));

            int.TryParse(dt[0]["TotalPages"].ToString(), out TotalPages);

            if (dt.Count > 0)
            {
                foreach (var R in dt.Children())
                {
                    Policy CurrentPolicy = PolicyModel.getPolicyFromID(int.Parse(R["PolicyID"].ToString()));

                    string BillingAddress = ""; string MailingAddress ="";
                    try 
	                {
                        MailingAddress = CurrentPolicy.mailingAddress.ApartmentNumber.ToString() + " " + CurrentPolicy.mailingAddress.road.RoadName.ToString() + ", " + CurrentPolicy.mailingAddress.city.CityName.ToString() + " " + CurrentPolicy.mailingAddress.country.CountryName.ToString();
	                }
	                catch (Exception)
	                {
		
	                }

                    try 
	                {
                        BillingAddress = CurrentPolicy.billingAddress.ApartmentNumber.ToString() + " " + CurrentPolicy.billingAddress.road.RoadName.ToString() + ", " + CurrentPolicy.billingAddress.city.CityName.ToString() + " " + CurrentPolicy.billingAddress.country.CountryName.ToString();
	                }
	                catch (Exception)
	                {
		 
	                }

                    try
                    {
                        pol.Add(new PolicyData
                        {
                            edi_system_id = CurrentPolicy.EDIID ,
                            insurer_trn = CurrentPolicy.company.Count > 0 ? CurrentPolicy.company[0].trn : CurrentPolicy.compCreatedBy != null ? CurrentPolicy.compCreatedBy.trn : "",
                            broker_trn = CurrentPolicy.compCreatedBy != null ? CurrentPolicy.compCreatedBy.trn : "",
                            start_date_time = CurrentPolicy.startDateTime.ToString(),
                            created_on = CurrentPolicy.CreatedOn.ToString(),
                            end_date_time = CurrentPolicy.endDateTime.ToString(),
                            policy_prefix = CurrentPolicy.policyCover!= null ? CurrentPolicy.policyCover.prefix:"",
                            policy_number = CurrentPolicy.policyNumber,
                            policy_cover = CurrentPolicy.policyCover!= null ? CurrentPolicy.policyCover.cover:"",
                            is_commercial_business = CurrentPolicy.policyCover!= null ? CurrentPolicy.policyCover.isCommercialBusiness:false ,
                            InsuredType = CurrentPolicy.insuredType,
                            policyID = CurrentPolicy.ID,
                            CompanyID = CurrentPolicy.company.Count > 0 ? CurrentPolicy.company[0].CompanyID : 0 ,
                            LastModifiedOn = R["LastModifiedOn"].ToString(),
                            address_on_policy = MailingAddress,
                            billing_address = BillingAddress,
                            broker_policy_number = CurrentPolicy.policyNumber,
                            empty_policy_number = false,
                            mailing_address = MailingAddress
                        });
                    }
                    catch (Exception)
                    {
                      
                    }
                }


                try
                {
                    //Get Insured Data
                    foreach (var P in pol)
                    {

                        //Certificate Data
                        List<RiskCertificate> RC = new List<RiskCertificate>();
                        JArray Certs = JArray.Parse(JsonConvert.SerializeObject((DataTable)Utilities.OmaxData.HandleQuery("Exec Epic_eGovGetCertificates NULL,NULL,NULL," + P.policyID.ToString(), ConnectionString(), Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery)));
                        if (Certs.Count > 0)
                        {
                            var S = Certs.Where(i => !string.IsNullOrWhiteSpace(i["PolicyHolderAddress"].ToString().ToString()) || i["PolicyHolderAddress"].ToString().ToString() != "  , , , ").Take(1).FirstOrDefault();

                            foreach (var c in Certs.GroupBy(p => p["expirydate"].ToString()))
                            {
                                try
                                {
                                    RC.Add(new RiskCertificate
                                    {
                                        edi_system_id = P.edi_system_id,
                                        alterations = "",
                                        authorized_drivers_wording = S["authorizeddrivers"].ToString(),
                                        cancelled = S["Cancelled"] != null ? bool.Parse(S["Cancelled"].ToString()) : false,
                                        certificate_no = S["certitificateNo"].ToString(),
                                        certificate_type = S["certificateType"].ToString(),
                                        chassis_number = S["chassisNo"].ToString(),
                                        date_first_printed = S["DateFirstPrinted"].ToString(),
                                        effective_date = S["effectivedate"].ToString(),
                                        endorsement_number = S["endorsementNo"].ToString(),
                                        expiry_date = S["expirydate"].ToString(),
                                        last_printed_by_plain_text = S["LastPrintedByFlat"].ToString(),
                                        date_time_last_printed_on = S["LastPrintedOn"].ToString(),
                                        limitations_of_use = S["LimitsOfUse"].ToString(),
                                        marks_and_registration_no = S["MarksRegNo"].ToString(),
                                        printed_paper_number = S["PrintedPaperNo"].ToString(),
                                    });
                                }
                                catch (Exception)
                                {

                                }

                            }

                        }
                        
                        //Cover Note Data
                        List<RiskCoverNote> CN = new List<RiskCoverNote>();
                        JArray CNotes = JArray.Parse(JsonConvert.SerializeObject((DataTable)Utilities.OmaxData.HandleQuery("Exec Epic_eGovGetCoverNotes  NULL,NULL,NULL," + P.policyID.ToString(), ConnectionString(), Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery)));
                        if (CNotes.Count > 0)
                        {

                            var C = CNotes.Where(i => !string.IsNullOrWhiteSpace(i["PolicyHolderAddress"].ToString().ToString()) || i["PolicyHolderAddress"].ToString().ToString() != "  , , , ").Take(1).FirstOrDefault();
                                                      
                            try
                            {
                                foreach (var c in CNotes)
                                {
                                    try
                                    {
                                        CN.Add(
                                        new RiskCoverNote
                                        {
                                            edi_system_id = P.edi_system_id,
                                            alterations = "",
                                            authorized_drivers_wording = C["authorizeddrivers"].ToString(),
                                            cancelled = C["Cancelled"].ToString(),
                                            chassis_number = C["chassisNo"].ToString(),
                                            cover_note_id = C["NoteNo"].ToString(),
                                            date_first_printed = C["DateFirstPrinted"].ToString(),
                                            days_effective = C["Period"].ToString(),
                                            effective_date_time = C["effectiveTime"].ToString(),
                                            endorsement_number = C["endorsementNo"].ToString(),
                                            expiry_date = C["expirydate"].ToString(),
                                            last_printed_by_plain_text = C["LastPrintedByFlat"].ToString(),
                                            last_printed_on_at = C["PolicyHolderAddress"].ToString(),
                                            limitations_of_use = C["LimitsOfUse"].ToString(),
                                            manual_cover_note_id = C["ManualCoverNoteNo"].ToString(),
                                            mortgagees = C["mortgagees"].ToString(),
                                            printed_count = C["PrintCount"].ToString(),
                                            printed_paper_number = C["PrintedPaperNo"].ToString(),
                                        }
                                        );
                                    }
                                    catch (Exception)
                                    {

                                    }

                                }
                            }
                            catch (Exception)
                            {

                            }

                        }

                        //Insured Data
                        List<policyInsuredCompany> CompI = new List<policyInsuredCompany>();
                        List<policyInsuredIndvidual> IndivI = new List<policyInsuredIndvidual>();

                        //JArray insureds = JArray.Parse(JsonConvert.SerializeObject((DataTable)Utilities.OmaxData.HandleQuery("Exec epic_eGovGetPolicyInsureds " + P.policyID.ToString() + ", " + P.CompanyID.ToString(), ConnectionString(), Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery)));


                        //Individuals
                        foreach (var i in GetInsuredList(P.policyID))
                        {
                            try
                            {
                                IndivI.Add(new policyInsuredIndvidual
                                {
                                    first_name = i.fname,
                                    last_name = i.lname,
                                    middle_name = i.mname,
                                    trn = i.TRN,
                                    address_on_policy = (i.address.roadnumber.ToString() + " " + i.address.road.RoadName + ", " + i.address.city.CityName + " " + i.address.country.CountryName),
                                    edi_system_id = P.edi_system_id
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }


                        //Companies
                        foreach (var i in GetInsuredCompanies(P.policyID))
                        {
                            try
                            {
                                CompI.Add(new policyInsuredCompany
                                {
                                    name = i.CompanyName,
                                    address_on_policy = (i.CompanyAddress.roadnumber.ToString() + " " + i.CompanyAddress.road.RoadName + ", " + i.CompanyAddress.city.CityName + " " + i.CompanyAddress.country.CountryName),
                                    edi_system_id = P.edi_system_id,
                                    trn = i.trn
                                });
                            }
                            catch (Exception)
                            {

                            }
                        }


                        //Vehicles
                        JArray vehicles = JArray.Parse(JsonConvert.SerializeObject((DataTable)Utilities.OmaxData.HandleQuery("Exec Epic_eGovGetVehicle NULL,NULL,NULL," + P.policyID.ToString(), ConnectionString(), Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery)));
                        //do logic here
                        JArray FilteredVehicle = new JArray();

                        //Driver Data

                        //Drivers
                        JArray Drivers = JArray.Parse(JsonConvert.SerializeObject((DataTable)Utilities.OmaxData.HandleQuery("Exec Epic_eGovGetDrivers NULL,NULL,NULL," + P.policyID.ToString(), ConnectionString(), Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery)));
                        // Athorized Drivers
                        JArray AuthorizedDrivers = JArray.Parse(JsonConvert.SerializeObject((DataTable)Utilities.OmaxData.HandleQuery("Exec Epic_eGovGetAuthorizedDriversDrivers NULL,NULL,NULL," + P.policyID.ToString(), ConnectionString(), Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery)));
                        //Excepted Drivers
                        JArray ExceptedDrivers = JArray.Parse(JsonConvert.SerializeObject((DataTable)Utilities.OmaxData.HandleQuery("Exec Epic_eGovGetExceptedDriversDrivers NULL,NULL,NULL," + P.policyID.ToString(), ConnectionString(), Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery)));
                        //Excluded Drivers
                        JArray ExcludedDrivers = JArray.Parse(JsonConvert.SerializeObject((DataTable)Utilities.OmaxData.HandleQuery("Exec Epic_eGovGetExcludedDriversDrivers NULL,NULL,NULL," + P.policyID.ToString(), ConnectionString(), Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery)));


                        //Build complete payload
                        JObject Policy = new JObject();
                        JObject PolicyData = new JObject();
                        JObject PolInsured = new JObject();
                        JObject Risk = new JObject();
                        JObject Cover = new JObject();
                        JArray CoverArray = new JArray();

                        if (Drivers.Count <= 0) Drivers = JArray.Parse(JsonConvert.SerializeObject(IndivI));

                        P.CompanyID = 0;
                        P.policyID = 0;

                        PolicyData.Add("data", JObject.Parse(JsonConvert.SerializeObject(P)));
                        Policy.Add("policy", PolicyData);

                        if (IndivI.Count > 0) { PolInsured["person"] = JArray.Parse(JsonConvert.SerializeObject(IndivI.Where(y => y.trn != ""))); }
                        if (CompI.Count > 0) { PolInsured["company"] = JArray.Parse(JsonConvert.SerializeObject(CompI.Where(y => y.trn != ""))); }

                        Policy.Add("insured", PolInsured);

                        Risk.Add("drivers", Drivers);
                        Risk.Add("authorized_drivers", AuthorizedDrivers);
                        Risk.Add("excepted_drivers", ExceptedDrivers);
                        Risk.Add("excluded_drivers", ExcludedDrivers);

                        //JArray veh = JArray.Parse(JsonConvert.SerializeObject(vehicles.GroupBy(p => p["chassis_number"].ToString())));

                        vehicles.GroupBy(p => p["chassis_number"].ToString(), p => p["registration_number"].ToString());

                        Risk.Add("vehicles", vehicles);

                        if (Certs.Count > 0)
                        {
                            Cover.Add("certificate", JArray.Parse(JsonConvert.SerializeObject(RC)));
                        }

                        if (CNotes.Count > 0)
                        {
                            Cover.Add("cover_note", JArray.Parse(JsonConvert.SerializeObject(CN)));
                        }

                        CoverArray.Add(Cover);
                        Risk.Add("cover", CoverArray);

                        Policy.Add("risk", Risk);
                        Policies.Add(Policy);

                    }
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                }

               

            }

            try
            {
                PolicyDataPayload["total_pages"] = TotalPages.ToString();
                PolicyDataPayload["records_per_page"] = PageSize.ToString();
                PolicyDataPayload["current_page_number"] = PageNumber.ToString();
                PolicyDataPayload["search_range"] = "from " + Fromdate + " to " + Todate;
                PolicyDataPayload["details"] = "returning page " + PageNumber.ToString() + " of " + TotalPages.ToString();
                PolicyDataPayload.Add("policy_data", Policies);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
                        
            return PolicyDataPayload;

        }


    }
}