﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace Honeysuckle.Models
{
    public class Email
    {
        public int emailID { get; set; }
        [RegularExpression("([a-zA-Z0-9.-_]+[@][a-zA-Z0-9]+([.][a-z]+)+)" , ErrorMessage="Invalid email address, please retry")]
        public string email { get; set; }
        public bool primary { get; set; }

        public Email() { }
        public Email(string email)
        {
            this.email = email;
        }
        public Email(int emailID, string email, bool primary)
        {
            this.emailID = emailID;
            this.email = email;
            this.primary = primary;
        }

        public Email(int emailID, string email)
        {
            this.emailID = emailID;
            this.email = email;
           
        }

        public Email(string email, bool primary)
        {
            this.email = email;
            this.primary = primary;
        }
    }

    public class EmailModel
    {

        /// <summary>
        /// This function adds an email/many emails to the person specified
        /// </summary>
        /// <param name="person">Personal data with emails associated</param>
        public static void AddEmailtoPerson(Person person)
        {
            AddEmailtoPerson(person, Constants.user.ID);
        }
        
        /// <summary>
        /// This function adds an email to the system (if it isn't already there) and then associates it with the person in question
        /// </summary>
        /// <param name="person">the person we are adding the emails to</param>
        public static void AddEmailtoPerson (Person person, int uid, bool policy_holder = false)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                if (person.emails != null)
                    foreach (Email email in person.emails)
                    {
                        string query = "EXEC AddEmailToPerson " + person.PersonID + ",'"
                                                                + Constants.CleanString(email.email) + "',"
                                                                + email.primary + ","
                                                                + uid + ","
                                                                + policy_holder;
                        SqlDataReader reader = sql.QuerySQL(query, "update:add email " + Constants.CleanString(email.email), "person", person);
                        reader.Close();
                    }

                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function returns all the emails associated to a person
        /// </summary>
        /// <param name="person">The person in question</param>
        /// <returns>List of emails</returns>
        public static List<Email> GetMyEmails(Person person)
        {
            List<Email> emails = new List<Email>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {

                string Query = "EXEC GetMyEmails " + person.PersonID;
                SqlDataReader reader = sql.QuerySQL(Query, "read", "email", new Email());
                while (reader.Read())
                {
                    try { emails.Add(new Email(int.Parse(reader["EID"].ToString()), reader["email"].ToString(), (bool)reader["Primary"])); }
                    catch (Exception) { }
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return emails;
        }

        /// <summary>
        /// This function returns all email data, based on an email Id, in the system
        /// </summary>
        /// <param name="email">The email object with a valid ID in the system</param>
        /// <returns>a fully realised email object</returns>
        public static Email GetEmail(Email email)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                SqlDataReader reader = sql.QuerySQL("EXEC GetEmail " + email.emailID, "read", "email", new Email());
                while (reader.Read())
                {
                    email.email = reader["email"].ToString();
                    email.primary = (bool)reader["Primary"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return email;
        }

        /// <summary>
        /// This function updates a person's emails in the system
        /// </summary>
        /// <param name="person">the person we're updating</param>
        public static void UpdateEmails(Person person)
        {
            DropEmailsFromPerson(person);
            AddEmailtoPerson(person);
        }

        /// <summary>
        /// This function removes the association of all emails from a specific person in the system
        /// </summary>
        /// <param name="person">this is the person we're dropping emails from</param>
        public static void DropEmailsFromPerson(Person person)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DropEmailsFromPerson " + person.PersonID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "email", new Email());
                reader.Close();
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function adds a username (an email, not a random string) to a list of emails
        /// </summary>
        /// <param name="personemails">list of emails</param>
        /// <param name="username">username/email to be added to list</param>
        /// <returns>full list including username/email</returns>
        public static List<Email> AddUsernameEmailToPerson(List<Email> personemails, string username)
        {
            bool isUserNamePrime = true;
            foreach (Email email in personemails)
            {
                if (email.primary) isUserNamePrime = false;
            }
            personemails.Add(new Email(username, isUserNamePrime));
            return personemails;
        }

        /// <summary>
        /// This function removes the username from a list of emails (for presentation purposes)
        /// </summary>
        /// <param name="personemails">this is a list of emails (including username)</param>
        /// <param name="username">a username</param>
        /// <returns>list of emails (not including username)</returns>
        public static List<Email> RmUsernameEmailToPerson(List<Email> personemails, string username)
        {
            if (personemails != null)
            {
                for (int i = personemails.Count - 1; i >= 0; i--)
                {
                    if (personemails[i].email == username)
                    {
                        personemails.RemoveAt(i);
                    }
                }
                return personemails;
            }
            else return new List<Email>();
        }

        /// <summary>
        /// This function looks at a list of emails and returns the email that is selected as the primary email
        /// </summary>
        /// <param name="personemails">list of emails</param>
        /// <returns>primary email</returns>
        public static Email GetPrimaryEmail(List<Email> personemails)
        {
            Email email = new Email();
            foreach (Email e in personemails)
            {
                if (e.primary)
                {
                    email = e;
                    break;
                }
            }
            return email;
        }

        /// <summary>
        /// Overloaded version of GetPrimaryEmail that goes to database and retrieves user's
        /// primary email from user id rather than from person or list of emails
        /// </summary>
        /// <param name="user">User object with valid user id</param>
        /// <returns>Email object</returns>
        public static Email GetPrimaryEmail(User user)
        {
            Email email = new Email();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC GetPrimaryEmail " + user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "read", "email", new Email());
                while (reader.Read())
                {
                    email.emailID = int.Parse(reader["EID"].ToString());
                    email.email = reader["email"].ToString();
                    email.primary = (bool)reader["primary"];
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return email;
        }

        /// <summary>
        /// this returns a list of emails which are the primary emails for all the users in the input list of users
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        public static List<Email> GetPrimaryEmail(List<User> users)
        {
            List<Email> emails = new List<Email>();
            foreach (User user in users)
            {
                emails.Add(GetPrimaryEmail(user));
            }
            return emails;
        }

        /// <summary>
        /// this function converts my email objects to a list of real email addresses
        /// </summary>
        /// <param name="emails"></param>
        /// <returns></returns>
        public static List<string> ConvertEmailtoString(List<Email> emails)
        {
            List<string> string_emails = new List<string>();
            foreach (Email email in emails)
            {
                string_emails.Add(email.email);
            }
            return string_emails;
        }

        /// <summary>
        /// This function adds an email to the system (if it isn't already there) and then associates it with the company in question
        /// </summary>
        /// <param name="company">The company to which emails are being associated</param>
        public static void AddEmailtoCompany(Company company)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                if (company.emails != null)
                    foreach (Email email in company.emails)
                    {
                        string query = "EXEC AddEmailToCompany " + company.CompanyID + ",'"
                                                                 + Constants.CleanString(email.email) + "','"
                                                                 + email.primary + "',"
                                                                 + Constants.user.ID;
                        SqlDataReader reader = sql.QuerySQL(query, "create", "email", email);
                        reader.Close();
                    }
                sql.DisconnectSQL();
            }
        }

        /// <summary>
        /// This function returns all the emails associated to a company
        /// </summary>
        /// <param name="company">The company in question</param>
        /// <returns>List of emails</returns>
        public static List<Email> GetCompanyEmails(Company company)
        {
            List<Email> emails = new List<Email>();
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string Query = "EXEC GetCompanyEmails " + company.CompanyID;
                SqlDataReader reader = sql.QuerySQL(Query, "read", "email", new Email());
                while (reader.Read())
                {
                    emails.Add(new Email(int.Parse(reader["EID"].ToString()), reader["email"].ToString(), (bool)reader["PrimaryEmail"]));
                }
                reader.Close();
                sql.DisconnectSQL();
            }
            return emails;
        }

        /// <summary>
        /// This function removes the association of all emails from a specific company in the system
        /// </summary>
        /// <param name="company">The company for which the emails are to be dropped</param>
        public static void DropEmailsFromCompany(Company company)
        {
            sql sql = new sql();
            if (sql.ConnectSQL())
            {
                string query = "EXEC DropEmailsFromCompany " + company.CompanyID + "," + Constants.user.ID;
                SqlDataReader reader = sql.QuerySQL(query, "delete", "email", new Email());
                reader.Close();
                sql.DisconnectSQL();
            }
        }

    }
}
