﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Honeysuckle.Models
{
    public class FileControl
    {
        public static List<HttpPostedFileBase> files { get; set; }
        public string filePath { get; set; }
        public string fileName { get; set; }
        public string status { get; set; }


        public FileControl() 
        {
            this.filePath = AppDomain.CurrentDomain.BaseDirectory + "uploads\\"; 
        }
        public FileControl(string fileName, string status)
        {
            this.fileName = fileName;
            this.status = status;
        }

        public static List<FileControl> storeFiles(List<HttpPostedFileBase> files,int kid = 0, string key = null)
        {
            List<FileControl> fcs = new List<FileControl>();
            string status = " has been uploaded.You will be notified of the outcome when processing is done.";
            foreach (HttpPostedFileBase file in files)
            {
                if (file.ContentType.ToLower() == "text/plain" && file != null)
                {
                    try
                    {
                        
                        string nameOfFile = Constants.user.ID + "_" + kid + "_"  + DateTime.Now.ToString("yyyyMMddHHmmss") +"_" + ToSafeFileName(file.FileName.Substring(file.FileName.LastIndexOf("\\") + 1));
                        FileControl filecontrol = new FileControl();
                        string p = System.IO.Path.Combine(filecontrol.filePath,nameOfFile);
                        file.SaveAs(p);
                    }
                    catch (Exception e)
                    {
                        Log.LogRecord("Error with file upload", e.Message);
                        status = " failed to upload. Please check the file and structure and try again.";
                    }
                }
                else status = " is an invalid txt file.";

                fcs.Add(new FileControl(file.FileName,status));
            }
                
           return fcs;     
          }

        public static string ToSafeFileName(string s)
        {
            return s
                .Replace("\\", "")
                .Replace("/", "")
                .Replace("\"", "")
                .Replace("*", "")
                .Replace(":", "")
                .Replace("?", "")
                .Replace("<", "")
                .Replace(">", "")
                .Replace("|", "");
        }
            
        
    }
}