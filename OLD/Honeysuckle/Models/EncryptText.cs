﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Honeysuckle.Models
{
    public class EncryptText
    {
        /// <summary>
        /// this function encrypts text with random hash
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Encrypt(string text)
        {
            string hash = BCrypt.GenerateSalt(); 
            string hashText = BCrypt.HashPassword(text, hash);
            return hashText;
        }

        /// <summary>
        /// this function checks if text matches hashed text
        /// </summary>
        /// <param name="plain"></param>
        /// <param name="hashed"></param>
        /// <returns></returns>
        public static bool TestPassword(string plain, string hashed)
        {
            return BCrypt.CheckPassword(plain, hashed);
        }
    }
}