﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PdfSharp.Pdf;
using PdfSharp.Fonts;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf.IO;
using System.IO;
using System.Diagnostics;
using MigraDoc.DocumentObjectModel;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf.Printing;

namespace Honeysuckle.Models
{
    public class PDF
    {
        ///<summary>
        /// This function is used to convert pdf data to bytes
        /// </summary>
        public static byte[] ConvertToDataBytes(PdfDocument pdf)
        {
            byte[] data;
            using (MemoryStream stream = new MemoryStream())
            {
                pdf.Save(stream, false);
                data = stream.ToArray();
            }
            return data;
        }

        /// <summary>
        /// Splits up sentences into new lines
        /// </summary>
        /// <param name="Words"></param>
        /// <returns></returns>
        public static List<String> SplitWords(string Sentence)
        {
            List<string> Words = new List<string>();
            try
            {
                Words.AddRange(Sentence.Split('('));
            }
            catch (Exception)
            {

            }
                       
            if (Words.Count < 0)
            {
                Words.Add(Sentence);

            }
            else 
            {
                try
                {
                    Words = Words.Where(p => p.Trim().Length > 0).ToList();
                    List<String> DressLine = new List<string>();
                    foreach (string line in Words)
                    {
                        DressLine.Add("(" + line);
                    }

                    Words.Clear();
                    Words.AddRange(DressLine);
                }
                catch (Exception)
                {
                  
                }
                
            }

            return Words;
        }


        ///<summary>
        /// This function calls the repsective functions of companies to print certificates to pdf
        /// </summary>
        public static PdfDocument PrintCertToPDF(List<VehicleCoverNoteGenerated> generetedCerts = null, int companyID = 0)
        {
            //IF/CASE CHECKS TO DECIDE WHICH ONE OF THE FORMATS TO PRINT IN (IF NECESSARY)

            /*
             * List of Insurer Codes FYI
            
             * JIIC - Jamaica International Insurance Co. Ltd.
             * GGJ  - Guardian General Insurance Company (Jamaic)
             * KEY  - Key Insurance Company Ltd.
             * JNGI - JN General Insurance Company Ltd.
             * GA   - General Accident Insurance Co. Ltd.
             * BCIC - British Caribbean Insurance Co. Ltd.
             * ICWI - The Insurance Company of the West Indies
             * AGI  - Advantage General Insurance Co.
             * AHAC - American Home Insurance Co. Ltd

             */
            PdfDocument pdf = new PdfDocument();
            if (companyID == 0) return pdf; //If no insurer is attached to the policy

            switch (CompanyModel.GetInsurerCode(companyID))
            {
                case "GKGI":
                    return JIICprint(generetedCerts);
                case "GGJ":
                    return GGJprint(generetedCerts);
                case "KEY":
                    return KEYprint(generetedCerts);
                case "JNGI":
                    return JNGIprint(generetedCerts);
                case "GA":
                    return GAprint(generetedCerts);
                case "AS":
                    return GAprint(generetedCerts);
                case "BCIC":
                    return BCICprint(generetedCerts);
                case "ICWI":
                    return ICWIprint(generetedCerts);
                case "AGIC":
                    return AGIprint(generetedCerts);
                case "IRJ":
                    return IRJprint(generetedCerts);
                case "NO RESULT":
                    return pdf;
                default:
                    pdf.Tag = "NO CODE";
                    return pdf;

            }
        }

        ///<summary>
        ///This function calls the repsective functions of companies to print cover notes to pdf
        /// </summary>
        public static PdfDocument PrintCoverNoteToPDF(List<VehicleCoverNoteGenerated> generatedNotes = null, int companyID = 0)
        {
            PdfDocument pdf = new PdfDocument();
            if (companyID == 0) return pdf; //If no insurer is attached to the policy

            switch (CompanyModel.GetInsurerCode(companyID))
            {
                case "GKGI":
                    return JIICcovernote(generatedNotes);
                case "GGJ":
                    return GGJcovernote(generatedNotes);
                case "KEY":
                    return KEYcovernote(generatedNotes);
                case "JNGI":
                    return JNGIcovernote(generatedNotes);
                case "GA":
                    return GAcovernote(generatedNotes);
                case "AS":
                    return GAcovernote(generatedNotes);
                case "BCIC":
                    return BCICcovernote(generatedNotes);
                case "ICWI":
                    return ICWIcovernote(generatedNotes);
                case "AGIC":
                    return AGIcovernote(generatedNotes);
                case "IRJ":
                    return IRJcovernote(generatedNotes);
                case "NO RESULT":
                    return pdf;
                default:
                    pdf.Tag = "NO CODE";
                    return pdf;
            }
        }


        ////////////////************************* COVER NOTES *********************************///////////////

        ///<summary>
        ///This function is used to create the pdf for JNGI's covers (for printing)
        /// </summary>
        private static PdfDocument JNGIcovernote(List<VehicleCoverNoteGenerated> generatedNotes)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();

                pdf.Info.Title = "Print of Cover Note";

                foreach (VehicleCoverNoteGenerated VcnGenerated in generatedNotes)
                {

                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));
                    XPen pen2 = new XPen(XColor.FromName("Black"), 1.6);
                    XPen pen3 = new XPen(XColor.FromName("Black"), 0.5);

                    // Create a font
                    XFont fontBold = new XFont("Arial", 14, XFontStyle.Bold);

                    ///////DRAWING//////
                    gfx.DrawString("COVER NOTE", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(260, 75, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawLine(pen2, 260, 90, 355, 90);

                    gfx.DrawString("JAMAICA", new XFont("Times New Roman", 13, XFontStyle.Bold), XBrushes.Black, new XRect(510, 78, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("JN General Insurance Company Limited", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(395, 95, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Date created    ", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(10, 95, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(System.DateTime.Now.ToString("MMMM dd, yyyy, H:mm tt"), new XFont("Times New Roman", 10), XBrushes.Black, new XRect(85, 95, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Control No.: -" + VcnGenerated.CovernoteNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(465, 110, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW. CAP, 257", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(100, 125, 20, 40), XStringFormats.TopLeft);

                    tf.DrawString("The undernamed having proposed for insurance in respect of the Motor Vehicle described in the Schedule below, the risk is hereby held covered in terms of the Company's usual form of Policy applicable thereto with the alterations indicated, for the period shown, unless the cover be terminated by the Company by notice in writing, in which case the insurance will thereupon cease and a proportionate part of the annual premium otherwise applicable for such insurance will be charged for the time the Company has been on risk.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(10, 155, 560, 100), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 10, 207, 580, 207);

                    gfx.DrawString("POLICY NUMBER  " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(10, 209, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("POLICY COVER  ", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(290, 209, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.cover, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(380, 209, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawLine(pen3, 10, 224, 580, 224);


                    gfx.DrawString("Insured ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 229, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(60, 229, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Address ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 265, 20, 40), XStringFormats.TopLeft);
                    tf.DrawString(VcnGenerated.PolicyHoldersAddress, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(60, 265, 250, 40), XStringFormats.TopLeft);

                    gfx.DrawString("PERIOD OF INSURANCE: " + VcnGenerated.Period + " Day(s)", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(300, 230, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 277, 235, 295, 235);
                    gfx.DrawLine(pen3, 480, 235, 580, 235);

                    gfx.DrawLine(pen3, 277, 235, 277, 315);
                    gfx.DrawLine(pen3, 500, 235, 500, 315);
                    gfx.DrawLine(pen3, 580, 235, 580, 315);

                    gfx.DrawLine(pen3, 277, 315, 580, 315);

                    gfx.DrawString("Effective From", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(283, 255, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Expires At", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(283, 275, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(VcnGenerated.effectiveTime, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(345, 255, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.expiryTime, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(345, 275, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(385, 255, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(385, 275, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Reference No.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(515, 255, 20, 40), XStringFormats.TopLeft);
                    if (VcnGenerated.referenceNo != null)
                        gfx.DrawString(VcnGenerated.referenceNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(525, 275, 20, 40), XStringFormats.TopLeft);


                    //First rectangle                         
                    gfx.DrawString("Year, Make, Model and Type of Body", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(15, 327, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Chassis No/", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(215, 317, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Engine No.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(215, 327, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("C.C./", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(295, 317, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("H.P.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(295, 327, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Seating", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(355, 317, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Capacity", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(355, 327, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Registration", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(425, 317, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Mark", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(425, 327, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Estimated", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(510, 317, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Value", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(510, 327, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawRectangle(pen3, 10, 315, 570, 25);


                    //Second rectangle
                    if (VcnGenerated.bodyType == null) VcnGenerated.bodyType = " ";
                    gfx.DrawString(VcnGenerated.vehdesc, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 345, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(215, 345, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.HPCC != null)
                        gfx.DrawString(VcnGenerated.HPCC, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(295, 345, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.seating != null)
                    {
                        if (VcnGenerated.seating == "0") VcnGenerated.seating = "";
                        gfx.DrawString(VcnGenerated.seating, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(355, 345, 20, 40), XStringFormats.TopCenter);
                    }
                    if (VcnGenerated.vehregno != null)
                        gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(425, 345, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawRectangle(pen3, 10, 340, 570, 33);


                    //Third 'rectangle'
                    if (VcnGenerated.limitsofuse == null) VcnGenerated.limitsofuse = "";

                    try 
	                {	        
		                if(!string.IsNullOrEmpty(VcnGenerated.limitsofuse))
                        {
                            var Lim = VcnGenerated.limitsofuse.Split('\n');
                            VcnGenerated.limitsofuse = "";
                            foreach (string Line in Lim) { VcnGenerated.limitsofuse += (Line + Environment.NewLine); }
                        }
	                }
	                catch (Exception)
	                {
		
	                }
                   
                    gfx.DrawString("Limitations as to use    ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(15, 378, 550, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.limitsofuse.Trim(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 388, 350, 40), XStringFormats.TopLeft);
                    //gfx.DrawRectangle(pen3, 10, 373, 570, 50);

                    gfx.DrawLine(pen3, 10, 373, 10, 423);
                    gfx.DrawLine(pen3, 580, 373, 580, 423);
                    gfx.DrawLine(pen3, 10, 423, 160, 423);
                    gfx.DrawLine(pen3, 577, 423, 580, 423);

                    gfx.DrawString("Authorized Drivers", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 426, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Mortgagees", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(402, 425, 20, 40), XStringFormats.TopLeft);

                    //Fourth rectangle
                    if (VcnGenerated.authorizedwording != null)
                        tf.DrawString(VcnGenerated.authorizedwording.Trim(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 445, 350, 40), XStringFormats.TopLeft);
                    if (VcnGenerated.mortgagees != null)
                        tf.DrawString(VcnGenerated.mortgagees.Trim(), new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(410, 445, 100, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 440, 570, 110);
                    gfx.DrawLine(pen3, 400, 440, 400, 550);

                    //Fifth rectangle
                    gfx.DrawString("Provided that the person driving holds a license to drive this vehicle or has held and is not disqualified from holding or obtaining such a license.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 555, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 550, 570, 20);

                    //Sixth rectangle
                    tf.DrawString("Alterations to form of Policy Applicable", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(15, 575, 100, 40), XStringFormats.TopLeft);
                    gfx.DrawString("AS PER POLICY.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(130, 575, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawRectangle(pen3, 10, 570, 570, 30);


                    gfx.DrawString("Issued by ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 605, 90, 40), XStringFormats.TopLeft);
                    tf.DrawString(VcnGenerated.companyCreatedBy == null ? VcnGenerated.CompanyName.ToString() : VcnGenerated.companyCreatedBy.ToString(), new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(65, 605, 150, 80), XStringFormats.TopLeft);


                    gfx.DrawString("I/We Hereby Certify that this Cover Note is ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(420, 605, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("issued  in accordance with the provision of the ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(420, 615, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("above mentioned law.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(420, 625, 90, 40), XStringFormats.TopCenter);

                    gfx.DrawString("JN GENERAL INSURNCE COMPANY LIMITED", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(420, 640, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawLine(pen3, 360, 689, 580, 689);


                    gfx.DrawString("For the company", new XFont("Times New Roman", 9, XFontStyle.Italic), XBrushes.Black, new XRect(420, 690, 90, 40), XStringFormats.TopCenter);
                    //gfx.DrawString("This Cover Note is only valid if printed on Security Paper.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(420, 705, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Copies should not be accepted.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(460, 715, 90, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Printed On" + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("H:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(380, 780, 95, 40), XStringFormats.TopCenter);


                    /////////////////////////////// WaterMark ///////////////////////////

                    if (VcnGenerated.cancelled)
                    {
                        // Get the size (in point) of the text
                        XSize size = gfx.MeasureString("Cancelled", new XFont("Times New Roman", 150, XFontStyle.Italic));

                        // Define a rotation transformation at the center of the page
                        gfx.TranslateTransform(page.Width / 2, page.Height / 2);
                        gfx.RotateTransform(-Math.Atan(page.Height / page.Width) * 180 / Math.PI);
                        gfx.TranslateTransform(-page.Width / 2, -page.Height / 2);

                        // Create a string format
                        XStringFormat format = new XStringFormat();
                        format.Alignment = XStringAlignment.Near;
                        format.LineAlignment = XLineAlignment.Near;

                        // Create a dimmed red brush
                        XBrush brush = new XSolidBrush(XColor.FromArgb(150, 200, 200, 200));

                        // Draw the string
                        gfx.DrawString("Cancelled", new XFont("Times New Roman", 150, XFontStyle.Italic), brush,
                        new XPoint((page.Width - size.Width) / 2, (page.Height - size.Height) / 2),
                        format);
                    }
                }

                ///////////////////////////////////////////////////////////////////

                return pdf;

            }
            catch (Exception)
            {
                return new PdfDocument();
            }


        }

        ///<summary>
        ///This function is used to create the pdf for KEY Insurance's covers (for printing)
        /// </summary>.
        private static PdfDocument KEYcovernote(List<VehicleCoverNoteGenerated> generatedNotes)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();


                pdf.Info.Title = "Print of Cover Notes";

                foreach (VehicleCoverNoteGenerated VcnGenerated in generatedNotes)
                {

                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));
                    XPen pen2 = new XPen(XColor.FromName("Black"), 1.6);
                    XPen pen3 = new XPen(XColor.FromName("Black"), 0.5);

                    // Create a font
                    XFont fontBold = new XFont("Arial", 14, XFontStyle.Bold);

                    ///////DRAWING//////
                    gfx.DrawString("Jamaica", new XFont("Arial", 13, XFontStyle.Bold), XBrushes.Black, new XRect(300, 40, 20, 40), XStringFormats.TopCenter);

                    gfx.DrawString("COVER NOTE", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(300, 67, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawLine(pen2, 262, 83, 357, 83);

                    gfx.DrawString("(Provisional) Certificate of Insurance" + VcnGenerated.CovernoteNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(300, 87, 20, 40), XStringFormats.TopCenter);

                    gfx.DrawString("Cover Note No." + VcnGenerated.CovernoteNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(460, 87, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("[" + VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode + "] KEY", new XFont("Times New Roman", 13, XFontStyle.Bold), XBrushes.Black, new XRect(460, 106, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("The Motor Vehicles Insurance (Third Party Risks) Act", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(300, 127, 20, 40), XStringFormats.TopCenter);

                    tf.DrawString("The undernamed having proposed for insurance in respect of the Motor Vehicle described in the Schedule below, the risk is hereby held covered in terms of the Company's usual form of Policy applicable thereto with the alterations indicated, for the period shown, unless the cover be terminated by the Company by notice in writing, in which case the insurance will thereupon cease and a proportionate part of the annual premium otherwise applicable for such insurance will be charged for the time the Company has been on risk.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(10, 148, 585, 100), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 10, 203, 585, 203);

                    gfx.DrawString("POLICY NO.           " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 210, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("POLICY COVER  ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(290, 210, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.cover, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(380, 210, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 10, 225, 585, 225);


                    gfx.DrawString("Insured ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 237, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(60, 237, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Address ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 272, 20, 40), XStringFormats.TopLeft);
                    tf.DrawString(VcnGenerated.PolicyHoldersAddress, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(60, 272, 250, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Premium ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 307, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("PERIOD OF INSURANCE: " + VcnGenerated.Period + " Day(s)", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(303, 235, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 275, 242, 295, 242);
                    gfx.DrawLine(pen3, 480, 242, 585, 242);

                    gfx.DrawLine(pen3, 275, 242, 275, 322);
                    gfx.DrawLine(pen3, 495, 242, 495, 322);
                    gfx.DrawLine(pen3, 585, 242, 585, 322);

                    gfx.DrawLine(pen3, 275, 322, 585, 322);


                    gfx.DrawString("effective From", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(283, 260, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Expires At", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(283, 280, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(VcnGenerated.effectiveTime, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(348, 260, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.expiryTime, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(348, 280, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(390, 260, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(390, 280, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Endorsement", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(510, 260, 20, 40), XStringFormats.TopLeft);
                    if (VcnGenerated.endorsementNo != null)
                        gfx.DrawString(VcnGenerated.endorsementNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(520, 280, 20, 40), XStringFormats.TopLeft);


                    //First rectangle                         
                    gfx.DrawString("Year, Make, Model and Type of Body", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(15, 339, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Chassis No./", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(215, 329, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Engine No/VIN", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(215, 339, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("C.C./", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(295, 329, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("H.P.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(295, 339, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Seating", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(355, 329, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Capacity", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(355, 339, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Registration", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(425, 329, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Mark", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(425, 339, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Estimated", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(510, 329, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Value", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(510, 339, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawRectangle(pen3, 10, 327, 575, 27);


                    //Second rectangle
                    gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.bodyType.ToString(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 359, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(215, 359, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.HPCC != null)
                        gfx.DrawString(VcnGenerated.HPCC, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(295, 359, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.seating != null)
                    {
                        if (VcnGenerated.seating == "0") VcnGenerated.seating = "";
                        gfx.DrawString(VcnGenerated.seating, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(355, 359, 20, 40), XStringFormats.TopCenter);
                    }
                    if (VcnGenerated.vehregno != null)
                        gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(425, 359, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawRectangle(pen3, 10, 354, 575, 30);


                    //Third rectangle
                    gfx.DrawString("USE PERMITTED    " + VcnGenerated.Usage, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(15, 389, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 384, 575, 25);

                    gfx.DrawString("Person or classes of persons entitled to drive:", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 420, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Mortgagees", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(395, 420, 100, 40), XStringFormats.TopLeft);

                    //Fourth rectangle
                    if (VcnGenerated.authorizedwording != null)
                        tf.DrawString("(a) " + VcnGenerated.authorizedwording, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 437, 350, 40), XStringFormats.TopLeft);

                    if (VcnGenerated.mortgagees != null)
                        tf.DrawString(VcnGenerated.mortgagees, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(400, 437, 100, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 435, 575, 110);
                    gfx.DrawLine(pen3, 390, 435, 390, 545);

                    //Fifth rectangle
                    gfx.DrawString("Provided that the person driving holds a license to drive this vehicle or has held and is not disqualified from holding or obtaining such a license", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 550, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 545, 575, 20);


                    //gfx.DrawString("This Cover Note is only valid if printed on Security Paper. Copies should not be accepted.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(18, 590, 90, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Date Issued on This", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(18, 635, 95, 40), XStringFormats.TopLeft);
                    gfx.DrawString(System.DateTime.Now.ToString("MMMM dd, yyyy, H:mm tt") + " By: ", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(108, 635, 95, 40), XStringFormats.TopLeft);

                    gfx.DrawString("I/We Hereby Certify that the policy to which this ", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(440, 590, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("provisional certificate relates is issued in accordance ", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(440, 600, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("with provisions of the above mentioned act.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(440, 610, 90, 40), XStringFormats.TopCenter);

                    gfx.DrawString("KEY INSURANCE COMPANY LIMITED", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(440, 635, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawLine(pen3, 360, 700, 580, 700);


                    gfx.DrawString("For the company", new XFont("Times New Roman", 10, XFontStyle.Italic), XBrushes.Black, new XRect(440, 705, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("(Authorized Signature)", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(440, 717, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("H:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(400, 745, 95, 40), XStringFormats.TopCenter);


                    //////////////////////  Adding an image  //////////////////

                    if (VcnGenerated.LogoLocation != "" && VcnGenerated.LogoLocation != null)
                    {
                        try
                        {
                            string Paths = "";
                            Paths = VcnGenerated.LogoLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                            PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);

                            gfx.DrawImage(ximg, 18, 690, 120, 90);
                        }
                        catch (Exception)
                        {
                           
                        }
                        
                    }

                    ////////////////////////////////////////////////////////////

                    /////////////////////////////// WaterMark ///////////////////////////

                    if (VcnGenerated.cancelled)
                    {
                        try
                        {
                            XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                            gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                        }
                        catch (Exception)
                        {

                        }
                       
                    }
                }

                ///////////////////////////////////////////////////////////////////

                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }


        }


        ///<summary>
        ///This function is used to create the pdf for GA's covers (for printing)
        /// </summary>
        private static PdfDocument GAcovernote(List<VehicleCoverNoteGenerated> generatedNotes)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();

                pdf.Info.Title = "Print of Certificate";

                foreach (VehicleCoverNoteGenerated VcnGenerated in generatedNotes)
                {

                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));
                    XPen pen2 = new XPen(XColor.FromName("Black"), 1.6);
                    XPen pen3 = new XPen(XColor.FromName("Black"), 0.5);

                    // Create a font
                    XFont fontBold = new XFont("Arial", 14, XFontStyle.Bold);


                    //////////////////////  Adding an image  //////////////////

                    if (VcnGenerated.HeaderImgLocation != "" && VcnGenerated.HeaderImgLocation != null)
                    {

                        string Paths = "";
                        Paths = VcnGenerated.HeaderImgLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                        PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);

                        gfx.DrawImage(ximg, 1, 1, 100, 90);
                    }

                    ////////////////////////////////////////////////////////////


                    ///////DRAWING//////
                    gfx.DrawString("COVER NOTE", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(260, 75, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawLine(pen2, 260, 90, 355, 90);

                    gfx.DrawString("JAMAICA", new XFont("Times New Roman", 14, XFontStyle.Bold), XBrushes.Black, new XRect(490, 78, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("General Accident Insurance Company Jamaica Ltd.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(360, 95, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Date created    ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 95, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(System.DateTime.Now.ToString("MMMM dd, yyyy, H:mm tt"), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(85, 95, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Control No.: -" + VcnGenerated.CovernoteNo.Trim(), new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(486, 110, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW. CAP, 257", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(100, 130, 20, 40), XStringFormats.TopLeft);

                    tf.DrawString("The undernamed having proposed for insurance in respect of the Motor Vehicle described in the Schedule below, the risk is hereby held covered in terms of the Company's usual form of Policy applicable thereto with the alterations indicated, for the period shown, unless the cover be terminated by the Company by notice in writing, in which case the insurance will thereupon cease and a proportionate part of the annual premium otherwise applicable for such insurance will be charged for the time the Company has been on risk.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(10, 160, 560, 100), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 10, 212, 580, 212);

                    gfx.DrawString("POLICY NO.           " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 215, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("POLICY COVER  ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(290, 215, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.cover, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(380, 215, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawLine(pen3, 10, 230, 580, 230);


                    gfx.DrawString("Insured ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 230, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(60, 230, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Address ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 270, 20, 40), XStringFormats.TopLeft);
                    tf.DrawString(VcnGenerated.PolicyHoldersAddress, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(60, 270, 250, 40), XStringFormats.TopLeft);

                    gfx.DrawString("PERIOD OF INSURANCE: " + VcnGenerated.Period + " Day(s)", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(303, 233, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 277, 235, 295, 235);
                    gfx.DrawLine(pen3, 455, 235, 580, 235);

                    gfx.DrawLine(pen3, 277, 235, 277, 315);
                    gfx.DrawLine(pen3, 475, 235, 475, 315);
                    gfx.DrawLine(pen3, 580, 235, 580, 315);

                    gfx.DrawLine(pen3, 277, 315, 580, 315);


                    gfx.DrawString("Effective From", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(283, 255, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Expires At", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(283, 275, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(VcnGenerated.effectiveTime, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(345, 255, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.expiryTime, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(345, 275, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(385, 255, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(385, 275, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Endorsement", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(500, 245, 20, 40), XStringFormats.TopLeft);
                    if (VcnGenerated.endorsementNo != null)
                        gfx.DrawString(VcnGenerated.endorsementNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(515, 275, 20, 40), XStringFormats.TopLeft);


                    //First rectangle                         
                    gfx.DrawString("Year, Make, Model and Type of Body", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(15, 327, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Chassis No./", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(215, 317, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Engine No.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(215, 327, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("CC", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(295, 317, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Seating", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(355, 317, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Capacity", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(355, 327, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Registration", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(425, 317, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Mark", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(425, 327, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Estimated", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(510, 317, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Value", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(510, 327, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.estimatedValue != null)
                    {

                        gfx.DrawString(VcnGenerated.estimatedValue, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(560, 323, 20, 40), XStringFormats.TopLeft);
                    }
                    gfx.DrawRectangle(pen3, 10, 315, 570, 25);


                    //Second rectangle
                    if (VcnGenerated.bodyType == null) VcnGenerated.bodyType = " ";
                    gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.bodyType.ToString(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 348, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(215, 348, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.HPCC != null)
                        gfx.DrawString(VcnGenerated.HPCC, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(295, 348, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.seating != null)
                    {
                        if (VcnGenerated.seating == "0") VcnGenerated.seating = "";
                        gfx.DrawString(VcnGenerated.seating, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(355, 348, 20, 40), XStringFormats.TopCenter);
                    }
                    if (VcnGenerated.vehregno != null)
                        gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(425, 348, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawRectangle(pen3, 10, 340, 570, 30);


                    //Third rectangle
                    if (VcnGenerated.limitsofuse == null) VcnGenerated.limitsofuse = "";
                    gfx.DrawString("Limitations as to use    ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(15, 375, 20, 40), XStringFormats.TopLeft);
                    tf.DrawString(VcnGenerated.limitsofuse + ". The Policy does not cover use for hire or reward or commercial travelling racing pace-making reliability trial speed-testing the carriage of goods or samples in connection with any trade or business or use for any purpose in connection with the Motor Trade.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(100, 375, 470, 50), XStringFormats.TopLeft);

                    gfx.DrawRectangle(pen3, 10, 370, 570, 50);

                    gfx.DrawString("Authorized Drivers", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 425, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Mortgagees", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(390, 425, 20, 40), XStringFormats.TopLeft);

                    //Fourth rectangle
                    if (VcnGenerated.authorizedwording != null)
                        tf.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 445, 350, 40), XStringFormats.TopLeft);

                    if (VcnGenerated.mortgagees != null)
                        tf.DrawString(VcnGenerated.mortgagees, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(410, 445, 100, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 440, 570, 110);
                    gfx.DrawLine(pen3, 400, 440, 400, 550);

                    //Fifth rectangle
                    gfx.DrawString("Provided that the person driving holds a license to drive this vehicle or has held and is not disqualified from holding or obtaining such a license.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 555, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 550, 570, 20);

                    //Sixth rectangle
                    tf.DrawString("Alterations to form of Policy Applicable", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(15, 575, 100, 40), XStringFormats.TopLeft);
                    gfx.DrawString("AS PER POLICY.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(130, 578, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawLine(pen3, 10, 573, 580, 573);
                    gfx.DrawLine(pen3, 10, 573, 10, 603);
                    gfx.DrawLine(pen3, 580, 573, 580, 603);
                    gfx.DrawLine(pen3, 10, 603, 20, 603);
                    gfx.DrawLine(pen3, 10, 603, 580, 603);


                    gfx.DrawString("I/We Hereby Certify that this Cover Note is ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(420, 605, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("issued  in accordance with the provision of the ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(420, 615, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("above mentioned law.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(420, 625, 90, 40), XStringFormats.TopCenter);

                    gfx.DrawString("General Accident Insurance Company Jamaica Ltd.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(420, 643, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawLine(pen3, 360, 689, 580, 689);


                    gfx.DrawString("For the company", new XFont("Times New Roman", 9, XFontStyle.Italic), XBrushes.Black, new XRect(420, 690, 90, 40), XStringFormats.TopCenter);
                    //gfx.DrawString("This Cover Note is only valid if printed on Security Paper.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(420, 705, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Copies should not be accepted.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(460, 715, 90, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Printed On" + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("H:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(400, 730, 95, 40), XStringFormats.TopCenter);


                    /////////////////////////////// WaterMark ///////////////////////////

                    if (VcnGenerated.cancelled)
                    {
                        try
                        {
                            XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                            gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                        }
                        catch (Exception)
                        {

                        }
                        
                    }
                }
                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }
        }


        /// <summary>
        /// This function is used to create the pdf for AGI's cover notes (for printing)
        /// </summary>
        private static PdfDocument GGJcovernote(List<VehicleCoverNoteGenerated> generatedNotes)
        {

            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();


                pdf.Info.Title = "Print of Cover Note";

                foreach (VehicleCoverNoteGenerated VcnGenerated in generatedNotes)
                {

                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));
                    XPen pen2 = new XPen(XColor.FromName("Black"), 1.6);
                    XPen pen3 = new XPen(XColor.FromName("Black"), 0.5);

                    // Create a font
                    XFont fontBold = new XFont("Arial", 14, XFontStyle.Bold);

                    ///////DRAWING///////
                    gfx.DrawString("JAMAICA", new XFont("Arial", 13), XBrushes.Black, new XRect(275, 60, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW 1939 AND AMENDED THERETO", new XFont("Arial", 11), XBrushes.Black, new XRect(70, 90, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("COVER NOTE", new XFont("Arial", 16, XFontStyle.Bold), XBrushes.Black, new XRect(250, 120, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawRectangle(pen3, 80, 150, 180, 22);
                    if (VcnGenerated.CovernoteNo != null) gfx.DrawString("Cover Note Number     " + VcnGenerated.CovernoteNo, new XFont("Arial", 9, XFontStyle.Bold), XBrushes.Black, new XRect(82, 155, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawRectangle(pen3, 365, 150, 180, 22);
                    if (VcnGenerated.PolicyNo != null) gfx.DrawString("Policy Number     " + VcnGenerated.PolicyNo, new XFont("Arial", 9, XFontStyle.Bold), XBrushes.Black, new XRect(367, 155, 20, 40), XStringFormats.TopLeft);

                    //policy cover description
                    gfx.DrawString("Policy Cover", new XFont("Arial", 9, XFontStyle.Bold), XBrushes.Black, new XRect(35, 200, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.cover) ? VcnGenerated.cover : "", new XFont("Arial", 9), XBrushes.Black, new XRect(280, 200, 20, 40), XStringFormats.TopLeft);


                    gfx.DrawString("1. ", new XFont("Arial", 9), XBrushes.Black, new XRect(35, 220, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Make of Vehicle", new XFont("Arial", 9, XFontStyle.Bold), XBrushes.Black, new XRect(60, 220, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.vehdesc, new XFont("Arial", 9), XBrushes.Black, new XRect(280, 220, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("1a.", new XFont("Arial", 9), XBrushes.Black, new XRect(35, 245, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Index mark and registration number of vehicle", new XFont("Arial", 9, XFontStyle.Bold), XBrushes.Black, new XRect(60, 245, 122, 40), XStringFormats.TopLeft);
                    if (VcnGenerated.vehregno != null) gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Arial", 9), XBrushes.Black, new XRect(280, 245, 20, 40), XStringFormats.TopLeft);


                    gfx.DrawString("1b.", new XFont("Arial", 9), XBrushes.Black, new XRect(35, 270, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Chassis Number", new XFont("Arial", 9, XFontStyle.Bold), XBrushes.Black, new XRect(60, 270, 122, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Arial", 9), XBrushes.Black, new XRect(280, 270, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("2.", new XFont("Arial", 9), XBrushes.Black, new XRect(35, 300, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Name of Policyholder", new XFont("Arial", 9, XFontStyle.Bold), XBrushes.Black, new XRect(60, 300, 122, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Arial", 9), XBrushes.Black, new XRect(180, 300, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("3.", new XFont("Arial", 9), XBrushes.Black, new XRect(35, 320, 200, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Effective Date of the Commencement", new XFont("Arial", 9, XFontStyle.Bold), XBrushes.Black, new XRect(60, 320, 400, 40), XStringFormats.TopLeft);
                    gfx.DrawString("of the Insurance for purposes of the Ordinances", new XFont("Arial", 9, XFontStyle.Bold), XBrushes.Black, new XRect(60, 332, 400, 40), XStringFormats.TopLeft);
                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy  hh:mm ttt") : "", new XFont("Arial", 9), XBrushes.Black, new XRect(285, 332, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("4.", new XFont("Arial", 9), XBrushes.Black, new XRect(35, 345, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Date of Expiry of this Cover Note", new XFont("Arial", 9, XFontStyle.Bold), XBrushes.Black, new XRect(60, 345, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy  hh:mm ttt") : "", new XFont("Arial", 9), XBrushes.Black, new XRect(285, 345, 20, 40), XStringFormats.TopLeft);

                    //gfx.DrawString("Expiry Time : ", new XFont("Arial", 9, XFontStyle.Bold), XBrushes.Black, new XRect(365,345, 20, 40), XStringFormats.TopLeft);
                    //gfx.DrawString(VcnGenerated.expiryTime, new XFont("Arial", 9), XBrushes.Black, new XRect(430, 345, 20, 40), XStringFormats.TopLeft);


                    gfx.DrawString("5.", new XFont("Arial", 9), XBrushes.Black, new XRect(35,365, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Limitations as to use", new XFont("Arial", 9, XFontStyle.Bold), XBrushes.Black, new XRect(60, 365, 20, 40), XStringFormats.TopLeft);
                    tf.DrawString(VcnGenerated.limitsofuse, new XFont("Arial", 9), XBrushes.Black, new XRect(60, 380, 530, 170), XStringFormats.TopLeft);


                    tf.DrawString("The Policyholder having proposed for insurance in respect of the Motor Vehicle described above, the risk is hereby held covered in terms of the Company's usual form of Policy applicable thereto with the alterations indicated, for the period shown, unless the cover be terminated by the Company by notice in writing, in which case the insurance will thereupon cease and a proportionate part of the annual premium otherwise applicable for such insurance will be charged for the time the Company has been on risk.", 
                        new XFont("Arial", 9), XBrushes.Black, new XRect(60, 535, 530, 70), XStringFormats.TopLeft);

                    gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW 1939 AND AMENDED THERETO", new XFont("Arial", 9), XBrushes.Black, new XRect(90, 585, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Arial", 9), XBrushes.Black, new XRect(230, 605, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("I/We hereby Certify that this Cover Note is issued in accordance with the above-mentioned Act", new XFont("Arial", 9), XBrushes.Black, new XRect(60, 620, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Hour of Issue ", new XFont("Arial", 9), XBrushes.Black, new XRect(365, 645, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(DateTime.Now.ToString("H:mm tt"), new XFont("Arial", 9), XBrushes.Black, new XRect(430, 640, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawLine(pen3, 425, 648, 570, 648);

                    gfx.DrawString("Date of Issue ", new XFont("Arial", 9), XBrushes.Black, new XRect(365, 658, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(AddOrdinal(DateTime.Now.Day) + " " + DateTime.Now.ToString("MMMM yyyy"), new XFont("Arial", 9), XBrushes.Black, new XRect(430, 658, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawLine(pen3, 425, 667, 570, 667);

                    gfx.DrawLine(pen3, 365, 715, 570, 715);
                    gfx.DrawString("Authorized Representative", new XFont("Arial", 10), XBrushes.Black, new XRect(385, 717, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(55, 780, 95, 40), XStringFormats.TopLeft);

                    //////////////////////  Adding an image  ////////////////////

                    if (VcnGenerated.FooterImgLocation != "" && VcnGenerated.FooterImgLocation != null)
                    {
                        try
                        {
                            string Paths = "";
                            Paths = VcnGenerated.FooterImgLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                            PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);
                            gfx.DrawImage(ximg, 60, 660, 120, 70);
                        }
                        catch (Exception)
                        {

                        }
                       
                    }

                    ////////////////////////// WaterMark ///////////////////////////

                    if (VcnGenerated.cancelled)
                    {
                        // Get the size (in point) of the text
                        XSize size = gfx.MeasureString("Cancelled", new XFont("Times New Roman", 150, XFontStyle.Italic));

                        // Define a rotation transformation at the center of the page
                        gfx.TranslateTransform(page.Width / 2, page.Height / 2);
                        gfx.RotateTransform(-Math.Atan(page.Height / page.Width) * 180 / Math.PI);
                        gfx.TranslateTransform(-page.Width / 2, -page.Height / 2);

                        // Create a string format
                        XStringFormat format = new XStringFormat();
                        format.Alignment = XStringAlignment.Near;
                        format.LineAlignment = XLineAlignment.Near;

                        // Create a dimmed red brush
                        XBrush brush = new XSolidBrush(XColor.FromArgb(150, 200, 200, 200));

                        // Draw the string
                        gfx.DrawString("Cancelled", new XFont("Times New Roman", 150, XFontStyle.Italic), brush,
                        new XPoint((page.Width - size.Width) / 2, (page.Height - size.Height) / 2),
                        format);
                    }
                }
                ///////////////////////////////////////////////////////////////////

                return pdf;

            }
            catch (Exception)
            {
                return new PdfDocument();
            }

        }

        /// <summary>
        /// This function is used to create the pdf for BCIC's cover notes (for printing)
        /// </summary>
        private static PdfDocument BCICcovernote(List<VehicleCoverNoteGenerated> generatedNotes)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();

                pdf.Info.Title = "Print of Cover Notes";

                foreach (VehicleCoverNoteGenerated VcnGenerated in generatedNotes)
                {

                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));
                    XPen pen2 = new XPen(XColor.FromName("Black"), 1.6);
                    XPen pen3 = new XPen(XColor.FromName("Black"), 0.5);

                    // Create a font
                    XFont fontBold = new XFont("Arial", 14, XFontStyle.Bold);


                    //////////////////////  Adding an image  //////////////////

                    if (VcnGenerated.HeaderImgLocation != "" && VcnGenerated.HeaderImgLocation != null)
                    {
                        string Paths = "";
                        Paths = VcnGenerated.HeaderImgLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                        PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);
                       
                        gfx.DrawImage(ximg, 15, 10, 510, 70);
                    }

                    ////////////////////////////////////////////////////////////


                    ///////DRAWING//////
                    gfx.DrawString("COVER NOTE", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(250, 90, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawLine(pen2, 250, 105, 350, 105);

                    gfx.DrawString("Cover Note No. " + VcnGenerated.CovernoteNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(450, 115, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Date of Issue", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(15, 130, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(System.DateTime.Now.ToString("yyyy, MMMM dd, h:mm tt"), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(95, 130, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Certificate Code. " + VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 13, XFontStyle.Bold), XBrushes.Black, new XRect(420, 130, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW, CAP 257", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(80, 150, 20, 40), XStringFormats.TopLeft);

                    tf.DrawString("The undernamed having proposed for insurance in respect of the Motor Vehicle described in the Schedule below, the risk is hereby held covered (subject to the relevant premium being paid) in terms of the Company's usual form of Policy applicable thereto with the alterations indicated, for the period shown, unless the cover be terminated by the Company by notice in writing, in which case the insurance will thereupon cease and a proportionate part of the annual premium otherwise applicable for such insurance will be charged for the time the Company has been on risk.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(10, 175, 565, 100), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 10, 225, 570, 225);

                    gfx.DrawString("POLICY NO.             " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 235, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("POLICY COVER  ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(290, 235, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.cover, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(380, 236, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawLine(pen3, 10, 248, 570, 248);

                    gfx.DrawString("Insured", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 260, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(50, 260, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Address", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 295, 20, 40), XStringFormats.TopLeft);
                    tf.DrawString(VcnGenerated.PolicyHoldersAddress, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(50, 295, 250, 40), XStringFormats.TopLeft);

                    gfx.DrawString("PERIOD OF INSURANCE: " + VcnGenerated.Period + " Day(s)", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(300, 260, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("TRANSACTION TYPE", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(475, 260, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 277, 264, 290, 264);
                    gfx.DrawLine(pen3, 455, 264, 470, 264);
                    gfx.DrawLine(pen3, 573, 264, 575, 264);

                    gfx.DrawLine(pen3, 277, 264, 277, 335);
                    gfx.DrawLine(pen3, 468, 264, 468, 335);
                    gfx.DrawLine(pen3, 575, 264, 575, 335);

                    gfx.DrawLine(pen3, 277, 335, 575, 335);

                    gfx.DrawString("Effective From", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(283, 285, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Expires At", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(283, 300, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(VcnGenerated.effectiveTime, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(345, 285, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.expiryTime, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(345, 300, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(385, 285, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(385, 300, 20, 40), XStringFormats.TopLeft);

                    //First rectangle
                    gfx.DrawString("Year, Make, Model and Type of Body:", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(20, 350, 20, 40), XStringFormats.TopLeft);
                    if (VcnGenerated.bodyType == null) VcnGenerated.bodyType = " ";
                    gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.bodyType.ToString(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(180, 350, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Chassis", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(380, 342, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("No", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(380, 352, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(455, 348, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawRectangle(pen3, 10, 340, 565, 25);


                    //Second rectangle
                    gfx.DrawString("Engine No:", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(20, 375, 20, 40), XStringFormats.TopLeft);
                    if (VcnGenerated.engineNo != null)
                        gfx.DrawString(VcnGenerated.engineNo.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(80, 375, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Registration", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(185, 367, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Mark", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(185, 377, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.vehregno != null)
                        gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(230, 373, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("C.C./", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(280, 367, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("H.P.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(280, 377, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.HPCC != null)
                        gfx.DrawString(VcnGenerated.HPCC, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(310, 373, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Seating", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(363, 367, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Capacity", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(363, 377, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.seating != null)
                    {
                        if (VcnGenerated.seating == "0") VcnGenerated.seating = "";
                        gfx.DrawString(VcnGenerated.seating, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(410, 373, 20, 40), XStringFormats.TopLeft);
                    }
                    gfx.DrawString("Estimated", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(460, 367, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Value", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(460, 377, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.estimatedValue != null)
                    {

                        gfx.DrawString(VcnGenerated.estimatedValue, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(500, 373, 20, 40), XStringFormats.TopLeft);
                    }
                    gfx.DrawRectangle(pen3, 10, 365, 565, 25);

                    //Third rectangle
                    gfx.DrawString("USE PERMITTED", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(20, 400, 20, 40), XStringFormats.TopLeft);
                    if (VcnGenerated.Usage != null)
                        gfx.DrawString(VcnGenerated.Usage, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(110, 400, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 390, 565, 25);

                    gfx.DrawString("Persons or classes of persons entitled to drive:", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 425, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Mortgagees", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(310, 425, 20, 40), XStringFormats.TopLeft);

                    //Fourth rectangle
                    if (VcnGenerated.authorizedwording != null)
                        tf.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 445, 270, 200), XStringFormats.TopLeft);

                    if (VcnGenerated.mortgagees != null)
                        tf.DrawString(VcnGenerated.mortgagees, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(310, 445, 100, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 435, 565, 100);
                    gfx.DrawLine(pen3, 300, 435, 300, 535);

                    //Fifth rectangle
                    gfx.DrawString("Provided that the person driving holds a license to drive this vehicle or has held and is not disqualified from holding or obtaining such a license.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 540, 350, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 535, 565, 20);


                    //gfx.DrawString("This Cover Note is only valid if printed on security paper. Copies should not be accepted.", new XFont("Times New Roman", 8), XBrushes.Black, new XRect(25, 575, 180, 40), XStringFormats.TopLeft);
                    gfx.DrawString("I/We Hereby Certify that his Cover Note is issued ", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(375, 575, 180, 40), XStringFormats.TopLeft);
                    gfx.DrawString("in accordance with the provision of the above", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(385, 585, 180, 40), XStringFormats.TopLeft);
                    gfx.DrawString("mentioned law. ", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(435, 595, 180, 40), XStringFormats.TopLeft);


                    gfx.DrawString("BRITISH CARIBBEAN INSURANCE", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(385, 625, 180, 40), XStringFormats.TopLeft);
                    gfx.DrawString("COMPANY LIMITED", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(415, 635, 180, 40), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 355, 680, 565, 680);
                    gfx.DrawString("For the Company", new XFont("Times New Roman", 10, XFontStyle.Italic), XBrushes.Black, new XRect(430, 685, 95, 40), XStringFormats.TopLeft);

                    gfx.DrawString("PrintedOn: " + System.DateTime.Now.ToString("yyyy, MMMM dd,") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(10, 725, 95, 40), XStringFormats.TopLeft);

                    /////////////////////////////// WaterMark ///////////////////////////

                    if (VcnGenerated.cancelled)
                    {
                        try
                        {
                            XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                            gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                        }
                        catch (Exception)
                        {

                        }
                        
                    }
                }
                ///////////////////////////////////////////////////////////////////

                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }
        }


        /// <summary>
        /// This function is used to create the pdf for ICWI's cover notes (for printing)
        /// </summary>
        private static PdfDocument ICWIcovernote(List<VehicleCoverNoteGenerated> generatedNotes)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();

                pdf.Info.Title = "Print of Cover Note";

                foreach (VehicleCoverNoteGenerated VcnGenerated in generatedNotes)
                {

                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));
                    XPen pen2 = new XPen(XColor.FromName("Black"), 1.6);
                    XPen pen3 = new XPen(XColor.FromName("Black"), 0.5);

                    // Create a font
                    XFont fontBold = new XFont("Arial", 14, XFontStyle.Bold);

                    //////////////////////  Adding an image  ////////////////////

                    if (VcnGenerated.HeaderImgLocation != "" && VcnGenerated.HeaderImgLocation != null)
                    {
                        string Paths = "";
                        Paths = VcnGenerated.HeaderImgLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                        PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);
                        gfx.DrawImage(ximg, 20, 10, 70, 70);
                    }

                    ////////////////////////////////////////////////////////////

                    ///////DRAWING//////
                    
                    //Company Address

                    gfx.DrawString("THE INSURANCE COMPANY OF THE WEST INDIES LIMITED ", new XFont("Arial", 12, XFontStyle.Bold), XBrushes.Black, new XRect(140, 5, 300, 40), XStringFormats.TopLeft);
                    gfx.DrawString("2 St. Lucia Avenue, Kingston 5  ", new XFont("Arial", 9, XFontStyle.Regular), XBrushes.Black, new XRect(250, 20, 300, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Tel: (876) 926-9040-7, (876) 926-9182-91, Email: direct@icwi.com ", new XFont("Arial", 9, XFontStyle.Regular), XBrushes.Black, new XRect(170, 35, 300, 40), XStringFormats.TopLeft);
                   
                    
                    gfx.DrawString("COVER NOTE", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(250, 90, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawLine(pen2, 250, 105, 350, 105);

                    gfx.DrawString("JAMAICA", new XFont("Times New Roman", 15, XFontStyle.Bold), XBrushes.Black, new XRect(505, 95, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("The Insurance Company Of The West Indies Limited", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(348, 110, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Date created    ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(20, 110, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(System.DateTime.Now.ToString("dd MMMM, yyyy, H:mm tt"), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(95, 110, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Control No.: -" + VcnGenerated.CovernoteNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(490, 125, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) ACT", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(125, 150, 20, 40), XStringFormats.TopLeft);

                    tf.DrawString("The undernamed having proposed for insurance in respect of the Motor Vehicle described in the Schedule below, the risk is hereby held covered in terms of the Company's usual form of Policy applicable thereto with the alterations indicated, for the period shown, unless the cover be terminated by the Company by notice in writing, in which case the insurance will thereupon cease and a proportionate part of the annual premium otherwise applicable for such insurance will be charged for the time the Company has been on risk.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(20, 175, 565, 100), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 20, 225, 580, 225);

                    gfx.DrawString("POLICY NO.             " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(20, 228, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("POLICY COVER  ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(290, 228, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.cover, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(380, 229, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawLine(pen3, 20, 243, 580, 243);

                    gfx.DrawString("Insured ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(20, 245, 20, 40), XStringFormats.TopLeft);
                    tf.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(70, 245, 200, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Address ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(20, 270, 20, 40), XStringFormats.TopLeft);
                    tf.DrawString(VcnGenerated.PolicyHoldersAddress.Replace(",,",""), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(70, 270, 200, 40), XStringFormats.TopLeft);

                    gfx.DrawString("PERIOD OF INSURANCE:" + (!string.IsNullOrEmpty(VcnGenerated.Period) ? VcnGenerated.Period : "30") + " Day(s)", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(303, 250, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 277, 254, 295, 254);
                    gfx.DrawLine(pen3, 455, 254, 580, 254);

                    gfx.DrawLine(pen3, 277, 254, 277, 330);
                    gfx.DrawLine(pen3, 475, 254, 475, 330);
                    gfx.DrawLine(pen3, 580, 254, 580, 330);

                    gfx.DrawString("Effective From", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(283, 270, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Expires At", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(283, 285, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(VcnGenerated.effectiveTime !="" ? Convert.ToDateTime(VcnGenerated.effectiveTime).ToString("hh:mm ttt"):"" , new XFont("Times New Roman", 9), XBrushes.Black, new XRect(345, 270, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.expiryTime != "" ? Convert.ToDateTime(VcnGenerated.expiryTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(345, 285, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(385, 270, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(385, 285, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Endorsement", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(500, 270, 20, 40), XStringFormats.TopLeft);
                    if (VcnGenerated.endorsementNo != null)
                        gfx.DrawString(VcnGenerated.endorsementNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(515, 285, 20, 40), XStringFormats.TopLeft);

                    //First rectangle
                    gfx.DrawString("Year, Make, Model and Type of Body", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(25, 340, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Chassis No.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(225, 330, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("C.C.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(305, 330, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Seating", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(365, 330, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Capacity", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(365, 340, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Registration", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(435, 330, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Mark", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(435, 340, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Estimated", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(520, 330, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Value", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(520, 340, 20, 40), XStringFormats.TopCenter);
                    if (! string.IsNullOrEmpty(VcnGenerated.estimatedValue))
                    {
                        decimal EstVale = 0;
                        decimal.TryParse(VcnGenerated.estimatedValue, out EstVale);
                        gfx.DrawString(EstVale.ToString("c2"), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(515, 358, 20, 40), XStringFormats.TopLeft);
                    }
                    gfx.DrawRectangle(pen3, 20, 330, 560, 25);


                    //Second rectangle
                    gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.bodyType.ToString() + " " + (!string.IsNullOrEmpty(VcnGenerated.vehmodeltype) ? VcnGenerated.vehmodeltype : ""), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(25, 358, 20, 40), XStringFormats.TopLeft);
                    //if (VcnGenerated.bodyType == null) VcnGenerated.bodyType = " ";
                    //gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.vehmodeltype, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(25, 358, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(225, 358, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.HPCC != null)
                        gfx.DrawString(VcnGenerated.HPCC, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(305, 358, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.seating != null)
                    {
                        if (VcnGenerated.seating == "0") VcnGenerated.seating = "";
                        gfx.DrawString(VcnGenerated.seating, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(365, 358, 20, 40), XStringFormats.TopCenter);
                    }
                    if (VcnGenerated.vehregno != null)
                        gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(435, 358, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawRectangle(pen3, 20, 355, 560, 30);


                    //Third rectangle
                    gfx.DrawString("Limitations as to use", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(25, 390, 20, 40), XStringFormats.TopLeft);

                    if (VcnGenerated.limitsofuse != null)
                    {
                        string LimUse = "";
                        var W = VcnGenerated.limitsofuse.Split(',');
                        foreach (string i in W) { if (i.Length > 2) { LimUse += (i.Trim() + Environment.NewLine); } }
                        tf.DrawString(LimUse.Replace("\r\n", Environment.NewLine).Replace("\"",""), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(25, 400, 460, 90), XStringFormats.TopLeft);
                    
                    }
                   
                    gfx.DrawRectangle(pen3, 20, 510, 560, 80);

                    gfx.DrawString("Authorized Drivers", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(20, 495, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Mortgagees", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(400, 495, 20, 40), XStringFormats.TopLeft);


                    //Fourth rectangle
                    if (VcnGenerated.authorizedwording != null)
                    {
                        string AuthDriving = "";
                        var W = VcnGenerated.authorizedwording.Split(',');
                        foreach (string i in W) { if (i.Length > 2) { AuthDriving += (i.Trim() + Environment.NewLine); } }
                        tf.DrawString(AuthDriving.Replace("\"",""), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(25, 515, 350, 70), XStringFormats.TopLeft);
                    }
                    if (VcnGenerated.mortgagees != null)
                        tf.DrawString(VcnGenerated.mortgagees, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(410, 515, 150, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 20, 385, 560, 100);
                    gfx.DrawLine(pen3, 400, 510, 400, 590);

                    //Fifth rectangle
                    gfx.DrawString("Provided that the person driving holds a license to drive this vehicle or has held and is not disqualified from holding or obtaining such a license", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(25, 593, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 20, 590, 560, 20);

                    //Sixth rectangle
                    tf.DrawString("Alterations to form of Policy Applicable", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(25, 616, 90, 40), XStringFormats.TopLeft);
                    gfx.DrawString("AS PER POLICY.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(130, 616, 90, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 20, 613, 560, 30);


                    gfx.DrawString("I/We Hereby Certify that this Cover Note is", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(420, 644, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("issued in accordance with the provision of the", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(420, 654, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("above mentioned law.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(420, 664, 90, 40), XStringFormats.TopCenter);

                    gfx.DrawString("The Insurance Company Of The West Indies", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(420, 680, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Limited", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(420, 690, 90, 40), XStringFormats.TopCenter);

                    gfx.DrawLine(pen3, 360, 725, 560, 725);
                    gfx.DrawString("For the Company", new XFont("Times New Roman", 9, XFontStyle.Italic), XBrushes.Black, new XRect(410, 730, 95, 40), XStringFormats.TopCenter);

                    //gfx.DrawString("This Cover Note is only valid if printed on Security Paper.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(350, 740, 95, 40), XStringFormats.TopLeft);
                    //gfx.DrawString("Copies should not be accepted.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(455, 750, 95, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Printed On " + System.DateTime.Now.ToString("dd MMMM, yyyy") + " @ " + System.DateTime.Now.ToString("H:mm tt") + "By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(359, 760, 95, 40), XStringFormats.TopLeft);

                    /////////////////////////////// WaterMark ///////////////////////////

                    if (VcnGenerated.cancelled)
                    {
                        try
                        {
                            XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                            gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                        }
                        catch (Exception)
                        {

                        }
                        
                    }
                }
                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }
        }


        /// <summary>
        /// This function is used to create the pdf for AGI's cover notes (for printing)
        /// </summary>
        private static PdfDocument AGIcovernote(List<VehicleCoverNoteGenerated> generatedNotes)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();


                pdf.Info.Title = "Print of Cover Note";

                foreach (VehicleCoverNoteGenerated VcnGenerated in generatedNotes)
                {

                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));
                    XPen pen2 = new XPen(XColor.FromName("Black"), 1.6);
                    XPen pen3 = new XPen(XColor.FromName("Black"), 0.5);

                    // Create a font
                    XFont fontBold = new XFont("Arial", 14, XFontStyle.Bold);


                    //////////////////////  Adding an image  //////////////////

                    if (VcnGenerated.HeaderImgLocation != "" && VcnGenerated.HeaderImgLocation != null)
                    {
                        string Paths = "";
                        Paths = VcnGenerated.HeaderImgLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                        PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);

                        gfx.DrawImage(ximg, 30, 3, 550, 80);
                    }

                    ////////////////////////////////////////////////////////////



                    ///////DRAWING//////
                    gfx.DrawString("Jamaica", new XFont("Arial", 12, XFontStyle.Bold), XBrushes.Black, new XRect(300, 90, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("COVER NOTE", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(280, 105, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawLine(pen2, 280, 120, 375, 120);

                    gfx.DrawString("Cover Note No.  " + VcnGenerated.CovernoteNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(470, 110, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Date created    ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(30, 130, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(System.DateTime.Now.ToString("MMMM dd, yyyy, h:mm tt"), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(105, 130, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("[" + VcnGenerated.certificateType + "] AGIC", new XFont("Times New Roman", 13, XFontStyle.Bold), XBrushes.Black, new XRect(470, 130, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW. CAP. 257", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(160, 155, 20, 40), XStringFormats.TopLeft);

                    tf.DrawString("The undernamed having proposed for insurance in respect of the Motor Vehicle described in the Schedule below, the risk is hereby held covered in terms of the Company's usual form of Policy applicable thereto with the alterations indicated, for the period shown, unless the cover be terminated by the Company by notice in writing, in which case the insurance will thereupon cease and a proportionate part of the annual premium otherwise applicable for such insurance will be charged for the time the Company has been on risk.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(30, 175, 550, 100), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 30, 228, 590, 228);

                    gfx.DrawString("POLICY NO.             " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(30, 232, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("POLICY COVER  ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(300, 232, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.cover, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(390, 233, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawLine(pen3, 30, 248, 590, 248);

                    gfx.DrawString("Insured ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(30, 255, 20, 40), XStringFormats.TopLeft);
                    tf.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(70, 255, 200, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Address ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(30, 290, 20, 40), XStringFormats.TopLeft);
                    tf.DrawString(VcnGenerated.PolicyHoldersAddress, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(70, 290, 200, 40), XStringFormats.TopLeft);

                    gfx.DrawString("PERIOD OF INSURANCE: " + VcnGenerated.Period + " Day(s)", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(313, 255, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 307, 259, 305, 259);
                    gfx.DrawLine(pen3, 465, 259, 590, 259);

                    gfx.DrawLine(pen3, 307, 259, 307, 335);
                    gfx.DrawLine(pen3, 505, 259, 505, 335);
                    gfx.DrawLine(pen3, 590, 259, 590, 335);

                    gfx.DrawLine(pen3, 307, 335, 590, 335);


                    gfx.DrawString("Effective From", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(313, 277, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Expires At", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(313, 297, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(VcnGenerated.effectiveTime, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(375, 277, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.expiryTime, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(375, 297, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(415, 277, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(415, 297, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Endorsement", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(510, 277, 20, 40), XStringFormats.TopLeft);
                    if (VcnGenerated.endorsementNo != null)
                        gfx.DrawString(VcnGenerated.endorsementNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(525, 297, 20, 40), XStringFormats.TopLeft);

                    //First rectangle
                    gfx.DrawString("Year, Make, Model and Type of Body", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(35, 350, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Chassis ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(235, 340, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("No.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(235, 350, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("C.C./", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(315, 340, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("H.P.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(315, 350, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Seating", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(375, 340, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Capacity", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(375, 350, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Registration", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(445, 340, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Mark", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(445, 350, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Estimated", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(530, 340, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Value", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(530, 350, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.estimatedValue != null)
                    {
                        //if (VcnGenerated.seating == "0") VcnGenerated.seating = "";
                        gfx.DrawString(VcnGenerated.estimatedValue, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(570, 346, 20, 40), XStringFormats.TopLeft);
                    }
                    gfx.DrawRectangle(pen3, 30, 340, 560, 25);


                    //Second rectangle
                    if (VcnGenerated.bodyType == null) VcnGenerated.bodyType = " ";
                    gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.bodyType.ToString(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(35, 368, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(235, 368, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.HPCC != null)
                        gfx.DrawString(VcnGenerated.HPCC, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(315, 368, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.seating != null)
                    {
                        if (VcnGenerated.seating == "0") VcnGenerated.seating = "";
                        gfx.DrawString(VcnGenerated.seating, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(375, 368, 20, 40), XStringFormats.TopCenter);
                    }
                    if (VcnGenerated.vehregno != null)
                        gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(445, 368, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawRectangle(pen3, 30, 365, 560, 30);


                    //Third rectangle
                    gfx.DrawString("USE PERMITTED    " + VcnGenerated.Usage, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(35, 400, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 30, 395, 560, 25);

                    gfx.DrawString("Persons or classes of persons entitled to drive:", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(30, 430, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Mortgagees", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(430, 430, 20, 40), XStringFormats.TopLeft);

                    //Fourth rectangle
                    if (VcnGenerated.authorizedwording != null)
                        tf.DrawString("A) " + VcnGenerated.authorizedwording, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(35, 450, 350, 40), XStringFormats.TopLeft);

                    if (VcnGenerated.mortgagees != null)
                        tf.DrawString(VcnGenerated.mortgagees, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(420, 450, 100, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 30, 443, 560, 100);
                    gfx.DrawLine(pen3, 410, 443, 410, 543);

                    //Fifth rectangle
                    gfx.DrawString("Provided that the person driving holds a license to drive this vehicle or has held and is not disqualified from holding or obtaining such a license", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(35, 548, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 30, 543, 560, 20);

                    //gfx.DrawString("This Cover Note is only valid if printed on Security Paper. Copies should not be accepted.", new XFont("Times New Roman", 8), XBrushes.Black, new XRect(40, 585, 95, 40), XStringFormats.TopLeft);

                    gfx.DrawString("I/We Hereby Certify that this Cover Note is issued ", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(430, 585, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("in accordance with the provision of the above", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(430, 595, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("mentioned law.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(430, 605, 90, 40), XStringFormats.TopCenter);

                    gfx.DrawString("ADVANTAGE GENERAL INSURANCE COMPANY LIMITED", new XFont("Arial", 9, XFontStyle.Bold), XBrushes.Black, new XRect(320, 635, 90, 40), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 355, 685, 535, 685);
                    gfx.DrawString("For the Company", new XFont("Times New Roman", 9, XFontStyle.Italic), XBrushes.Black, new XRect(420, 690, 95, 40), XStringFormats.TopCenter);

                    gfx.DrawString("Issued on: " + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("h:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(35, 720, 95, 40), XStringFormats.TopLeft);

                    /////////////////////////////// WaterMark ///////////////////////////

                    if (VcnGenerated.cancelled)
                    {
                        try
                        {
                            XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                            gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                        }
                        catch (Exception)
                        {

                        }
                        
                    }
                }

                ///////////////////////////////////////////////////////////////////

                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }


        }

        /// <summary>
        /// This function is used to create the pdf for JIIC's cover notes (for printing)
        /// </summary>
        private static PdfDocument JIICcovernote(List<VehicleCoverNoteGenerated> generatedNotes)
        {

            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();

                pdf.Info.Title = "Print of Cover Notes";

                foreach (VehicleCoverNoteGenerated VcnGenerated in generatedNotes)
                {

                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));
                    XPen pen2 = new XPen(XColor.FromName("Black"), 1.6);
                    XPen pen3 = new XPen(XColor.FromName("Black"), 0.5);




                    if (VcnGenerated.HeaderImgLocation != "" && VcnGenerated.HeaderImgLocation != null)
                    {
                        string Paths = "";
                        Paths = VcnGenerated.HeaderImgLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                        PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);
                        gfx.DrawImage(ximg, 20, 5, 600, 70);
                    }



                    // Create a font
                    XFont fontBold = new XFont("Arial", 14, XFontStyle.Bold);

                    ///////DRAWING///////
                    gfx.DrawString("COVER NOTE", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(300, 113, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawLine(pen2, 262, 128, 357, 128);

                    gfx.DrawString("JAMAICA", new XFont("Arial", 13, XFontStyle.Bold), XBrushes.Black, new XRect(520, 118, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Date Created", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(10, 140, 95, 40), XStringFormats.TopLeft);
                    gfx.DrawString(System.DateTime.Now.ToString("MMMM dd, yyyy, H:mm tt"), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(90, 140, 95, 40), XStringFormats.TopLeft);
                    gfx.DrawString("GK General Insurance Company Limited", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(400, 138, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Control No. : " + VcnGenerated.CovernoteNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(460, 150, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("THE MOTOR VEHICLE INSURANCE (THIRD PARTY RISKS) LAW, CAP 257", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(300, 165, 20, 40), XStringFormats.TopCenter);

                    tf.DrawString("The undernamed having proposed for insurance in respect of the Motor Vehicle described in the Schedule below, the risk is hereby held covered in terms of the Company's usual form of Policy applicable thereto with the alterations indicated, for the period shown, unless the cover be terminated by the Company by notice in writing, in which case the insurance will thereupon cease and a proportionate part of the annual premium otherwise applicable for such insurance will be charged for the time the Company has been on risk.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(10, 190, 585, 100), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 10, 238, 585, 238);

                    gfx.DrawString("POLICY NUMBER  " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 240, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("POLICY COVER  ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(290, 240, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.cover, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(380, 240, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 10, 252, 585, 252);


                    gfx.DrawString("Insured ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 260, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(60, 260, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Address ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 285, 20, 40), XStringFormats.TopLeft);
                    tf.DrawString(VcnGenerated.PolicyHoldersAddress, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(60, 285, 250, 40), XStringFormats.TopLeft);

                    gfx.DrawString("PERIOD OF INSURANCE: " + VcnGenerated.Period + " Day(s)", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(283, 265, 20, 40), XStringFormats.TopLeft);
                    //gfx.DrawString("TRANSACTION TYPE ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(490, 254, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 275, 260, 585, 260);
                    //gfx.DrawLine(pen3, 480, 260, 490, 260);

                    gfx.DrawLine(pen3, 275, 260, 275, 322);
                    gfx.DrawLine(pen3, 487, 260, 487, 322);
                    gfx.DrawLine(pen3, 585, 260, 585, 322);

                    gfx.DrawLine(pen3, 275, 322, 585, 322);


                    gfx.DrawString("Effective From", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(283, 278, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Expires At", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(283, 292, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(VcnGenerated.effectiveTime, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(348, 278, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.expiryTime, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(348, 292, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(388, 278, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(390, 292, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Reference No.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(510, 270, 20, 40), XStringFormats.TopLeft);
                    if (VcnGenerated.endorsementNo != null)
                        gfx.DrawString(VcnGenerated.endorsementNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(520, 290, 20, 40), XStringFormats.TopLeft);


                    //First rectangle                         
                    gfx.DrawString("Year, Make, Model and Type of Body", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(15, 339, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Chassis No./", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(215, 329, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Engine No/VIN", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(215, 339, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("C.C./", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(295, 329, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("H.P.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(295, 339, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Seating", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(355, 329, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Capacity", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(355, 339, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Registration", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(425, 329, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Mark", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(425, 339, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Estimated", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(510, 329, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Value", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(510, 339, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawRectangle(pen3, 10, 327, 575, 27);


                    //Second rectangle
                    gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.bodyType.ToString(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 359, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(215, 359, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.HPCC != null)
                        gfx.DrawString(VcnGenerated.HPCC, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(295, 359, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.seating != null)
                    {
                        if (VcnGenerated.seating == "0") VcnGenerated.seating = "";
                        gfx.DrawString(VcnGenerated.seating, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(355, 359, 20, 40), XStringFormats.TopCenter);
                    }
                    if (VcnGenerated.vehregno != null)
                        gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(425, 359, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawRectangle(pen3, 10, 354, 575, 30);

                    if (VcnGenerated.estimatedValue != null)
                    {

                        gfx.DrawString(VcnGenerated.estimatedValue, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(460, 359, 20, 40), XStringFormats.TopLeft);
                    }

                    //Third rectangle
                    gfx.DrawString("Limitations as to use    " + VcnGenerated.Usage, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(15, 389, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 384, 575, 20);

                    gfx.DrawString("Authorized Drivers", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(10, 422, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Mortgagees", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(290, 422, 100, 40), XStringFormats.TopLeft);

                    //Fourth rectangle
                    if (VcnGenerated.authorizedwording != null)
                        tf.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 437, 280, 90), XStringFormats.TopLeft);

                    if (VcnGenerated.mortgagees != null)
                        tf.DrawString(VcnGenerated.mortgagees, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(400, 437, 100, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 435, 575, 90);
                    gfx.DrawLine(pen3, 290, 435, 290, 525);

                    //Fifth rectangle
                    gfx.DrawString("Provided that the person driving holds a license to drive this vehicle or has held and is not disqualified from holding or obtaining such a license", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 528, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 525, 575, 20);

                    //Sixth rectangle
                    gfx.DrawString("Alterations to form of", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(15, 546, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Policy Applicable", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(15, 557, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("AS PER POLICY", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(130, 546, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 545, 575, 25);

                    gfx.DrawString("Issue by:   ", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(15, 572, 90, 40), XStringFormats.TopLeft);
                    tf.DrawString(VcnGenerated.companyCreatedBy == null ? VcnGenerated.CompanyName.ToString() : VcnGenerated.companyCreatedBy.ToString(), new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(65, 572, 150, 80), XStringFormats.TopLeft);

                    gfx.DrawString("I/We Hereby Certify that this Cover Note is ", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(440, 572, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("issued in accordance with provisions of the", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(440, 584, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("above mentioned law.", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(440, 595, 90, 40), XStringFormats.TopCenter);

                    gfx.DrawString("GK GENERAL INSURANCE COMPANY", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(440, 615, 90, 40), XStringFormats.TopCenter);
                    gfx.DrawString("LIMITED", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(440, 625, 90, 40), XStringFormats.TopCenter);

                    gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("H:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(90, 735, 95, 40), XStringFormats.TopCenter);

                    //////////////////////  Adding an image  ////////////////////

                    if (VcnGenerated.LogoLocation != "" && VcnGenerated.LogoLocation != null)
                    {
                        try
                        {
                            string Paths = "";
                            Paths = VcnGenerated.LogoLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                            PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);
                            //gfx.DrawImage(ximg, 450, 642, 90, 80);
                        }
                        catch (Exception)
                        {

                        }
                       
                    }

                    ////////////////////////////////////////////////////////////


                    gfx.DrawLine(pen3, 375, 735, 580, 735);
                    gfx.DrawString("For the company", new XFont("Times New Roman", 11), XBrushes.Black, new XRect(440, 735, 90, 40), XStringFormats.TopCenter);

                    /////////////////////////////// WaterMark ///////////////////////////

                    if (VcnGenerated.cancelled)
                    {
                        try
                        {
                            XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                            gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                        }
                        catch (Exception)
                        {

                        }
                       
                    }
                }

                ///////////////////////////////////////////////////////////////////

                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }

        }

        /// <summary>
        /// IronRock covernote Printed Template
        /// </summary>
        /// <param name="generatedNotes"></param>
        /// <returns></returns>
        private static PdfDocument IRJcovernote(List<VehicleCoverNoteGenerated> generatedNotes)
        {

            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();

                pdf.Info.Title = "Print of Cover Notes";

                foreach (VehicleCoverNoteGenerated VcnGenerated in generatedNotes)
                {

                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));
                    XPen pen2 = new XPen(XColor.FromName("Black"), 1.6);
                    XPen pen3 = new XPen(XColor.FromName("Black"), 0.5);
                                        

                    if (VcnGenerated.HeaderImgLocation != "" && VcnGenerated.HeaderImgLocation != null)
                    {
                        string Paths = "";
                        Paths = VcnGenerated.HeaderImgLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                        PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);
                        gfx.DrawImage(ximg, 10, 15, 170, 70);
                    }
                    

                    // Create a font
                    XFont fontBold = new XFont("Arial", 14, XFontStyle.Bold);

                    ///////DRAWING///////
                    gfx.DrawString("COVER NOTE", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(300, 113, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawLine(pen2, 262, 128, 357, 128);
                                     
                    gfx.DrawString("Date Created", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(10, 140, 95, 40), XStringFormats.TopLeft);
                    gfx.DrawString(System.DateTime.Now.ToString("MMMM dd, yyyy, H:mm tt"), new XFont("Times New Roman", 11), XBrushes.Black, new XRect(90, 140, 95, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Cover Note No. " + VcnGenerated.CovernoteNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(460, 125, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Certificate Code. " + VcnGenerated.certificateType, new XFont("Times New Roman", 13, XFontStyle.Bold), XBrushes.Black, new XRect(443, 140, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("THE MOTOR VEHICLE INSURANCE (THIRD PARTY RISKS) ACT", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(300, 165, 20, 40), XStringFormats.TopCenter);

                    tf.DrawString("The undernamed having proposed for insurance in respect of the Motor Vehicle described in the Schedule below, the risk is hereby held covered in terms of the Company's usual form of Policy applicable thereto with the alterations indicated, for the period shown, unless the cover be terminated by the Company by notice in writing, in which case the insurance will thereupon cease and a proportionate part of the annual premium otherwise applicable for such insurance will be charged for the time the Company has been on risk.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(10, 190, 585, 100), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 10, 238, 585, 238);

                    gfx.DrawString("POLICY NO.     " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 240, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("POLICY COVER  ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(290, 240, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.cover, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(380, 240, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawLine(pen3, 10, 252, 585, 252);


                    gfx.DrawString("Insured ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 260, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(60, 260, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Address ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(10, 285, 20, 40), XStringFormats.TopLeft);
                    tf.DrawString(VcnGenerated.PolicyHoldersAddress, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(60, 285, 250, 40), XStringFormats.TopLeft);

                    gfx.DrawString("PERIOD OF INSURANCE: " + VcnGenerated.Period + " Day(s)", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(310, 254, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("TRANSACTION TYPE ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(490, 254, 20, 40), XStringFormats.TopLeft);


                    gfx.DrawLine(pen3, 275, 260, 300, 260); //First horizontal line
                    gfx.DrawLine(pen3, 465, 260, 487, 260); //Second horizontal line

                    //gfx.DrawLine(pen3, 480, 260, 490, 260);

                    gfx.DrawLine(pen3, 275, 260, 275, 322); //First vertical line
                    gfx.DrawLine(pen3, 487, 260, 487, 322); //Second rertical Line
                    gfx.DrawLine(pen3, 585, 260, 585, 322); //Third vertical line

                    gfx.DrawLine(pen3, 275, 322, 585, 322); //Third horizontal line


                    gfx.DrawString("Effective From", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(283, 278, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Expires At", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(283, 300, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(VcnGenerated.effectiveTime, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(348, 278, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.expiryTime, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(348, 300, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(388, 278, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(390, 300, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Reference No.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(510, 270, 20, 40), XStringFormats.TopLeft);
                    if (VcnGenerated.endorsementNo != null)
                        gfx.DrawString(VcnGenerated.endorsementNo, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(520, 290, 20, 40), XStringFormats.TopLeft);


                    //First rectangle                         
                    gfx.DrawString("Year, Make, Model and Type of Body: ", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(15, 339, 20, 40), XStringFormats.TopLeft);

                    gfx.DrawString("Engine No:   " + VcnGenerated.engineNo , new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(15, 362, 20, 40), XStringFormats.TopLeft);
                  
                    gfx.DrawString("Chassis", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(345, 330, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("No", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(345, 340, 20, 40), XStringFormats.TopCenter);
                 
                    gfx.DrawString("C.C./", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(295, 359, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("H.P.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(295, 370, 20, 40), XStringFormats.TopCenter);

                    gfx.DrawString("Seating", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(395, 359, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Capacity", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(395, 370, 20, 40), XStringFormats.TopCenter);

                    gfx.DrawString("Registration", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(195, 359, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Mark", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(195, 370, 20, 40), XStringFormats.TopCenter);

                    gfx.DrawString("Estimated", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(475, 359, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawString("Value", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(475, 370, 20, 40), XStringFormats.TopCenter);
                    
                    gfx.DrawRectangle(pen3, 10, 327, 575, 27);

                    //Second rectangle
                    gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.bodyType.ToString(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(170, 339, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(430, 335, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.HPCC != null)
                        gfx.DrawString(VcnGenerated.HPCC , new XFont("Times New Roman", 9), XBrushes.Black, new XRect(330, 365, 20, 40), XStringFormats.TopCenter);
                    if (VcnGenerated.seating != null)
                    {
                        if (VcnGenerated.seating == "0") VcnGenerated.seating = "";
                        gfx.DrawString(VcnGenerated.seating , new XFont("Times New Roman", 9), XBrushes.Black, new XRect(430, 365, 20, 40), XStringFormats.TopCenter);
                    }
                    if (VcnGenerated.vehregno != null)
                        gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 9), XBrushes.Black, new XRect(240, 365, 20, 40), XStringFormats.TopCenter);
                    gfx.DrawRectangle(pen3, 10, 354, 575, 30);

                    if (VcnGenerated.estimatedValue != null)
                    {
                        gfx.DrawString(VcnGenerated.estimatedValue, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(520, 365, 20, 40), XStringFormats.TopLeft);
                    }

                   
                    //Third rectangle
                    gfx.DrawString("USE PERMITTED    " + VcnGenerated.Usage, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(15, 389, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 384, 575, 20);

                    gfx.DrawString("Authorized Drivers", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(10, 422, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Mortgagees", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(290, 422, 100, 40), XStringFormats.TopLeft);

                    //Fourth rectangle
                    if (VcnGenerated.authorizedwording != null)
                        tf.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 437, 270, 90), XStringFormats.TopLeft);

                    if (VcnGenerated.mortgagees != null)
                        tf.DrawString(VcnGenerated.mortgagees, new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(400, 437, 100, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 435, 575, 90);
                    gfx.DrawLine(pen3, 290, 435, 290, 525);

                    //Fifth rectangle
                    gfx.DrawString("Provided that the person driving holds a license to drive this vehicle or has held and is not disqualified from holding or obtaining such a license", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(15, 528, 20, 40), XStringFormats.TopLeft);
                    gfx.DrawRectangle(pen3, 10, 525, 575, 20);

                    gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("H:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(55, 780, 95, 40), XStringFormats.TopCenter);

                    gfx.DrawString("I/WE HEREBY CERTIFY", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(10, 575, 20, 40), XStringFormats.TopLeft);
                    tf.DrawString("that the Policy to which this Certificate relates is issued in accordance with the provisions of", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(156, 575, 400, 40), XStringFormats.TopLeft);
                    tf.DrawString(" the above-mentioned Law", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(10, 595, 400, 40), XStringFormats.TopLeft);
                    gfx.DrawString("Issued on " + DateTime.Now.ToString("MMMM dd, yyyy, hh:mm ttt"), new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(10, 615, 20, 40), XStringFormats.TopLeft);

                    ////////////////////////////////////////////////////////////

                    gfx.DrawString("IRONROCK INSURANCE COMPANY LIMITED", new XFont("Times New Roman", 11), XBrushes.Black, new XRect(265, 645, 95, 40), XStringFormats.TopCenter);
                    gfx.DrawLine(pen3, 180, 700, 455, 700);
                    gfx.DrawString("For the company", new XFont("Times New Roman", 11), XBrushes.Black, new XRect(265, 700, 95, 40), XStringFormats.TopCenter);

                                
                    /////////////////////////////// WaterMark ///////////////////////////

                    if (VcnGenerated.cancelled)
                    {
                        try
                        {
                            XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                            gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                        }
                        catch (Exception)
                        {

                        }

                    }
                }

                ///////////////////////////////////////////////////////////////////

                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }

        }


        //////////************************* CERTIFICATES*********************************////////////


        /// <summary>
        /// This function is used to create the pdf for KEY's certificates (for printing)
        /// </summary>
        private static PdfDocument KEYprint(List<VehicleCoverNoteGenerated> generatedCerts)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();


                pdf.Info.Title = "Print of Certificate";

                foreach (VehicleCoverNoteGenerated VcnGenerated in generatedCerts)
                {

                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));

                    // Create a font
                    XFont fontBold = new XFont("Times New Roman", 14, XFontStyle.Bold);

                    //if no Registration Number is provided the generate a certificate that has no mention of Registration Number

                    if (VcnGenerated.vehregno != null || VcnGenerated.vehregno != "")
                    {
                        // Draw the text 
                        gfx.DrawString("Jamaica", new XFont("Arial", 12, XFontStyle.Bold), XBrushes.Black, new XRect(270, 50, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(185, 75, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW, CAP 257", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(180, 100, 200, 40), XStringFormats.TopCenter);

                        gfx.DrawString("Certificate No.  " + VcnGenerated.certitificateNo, new XFont("Times New Roman", 13, XFontStyle.Bold), XBrushes.Black, new XRect(40, 140, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Policy No: " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(225, 141, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(455, 130, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(40, 168, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Year & Make of Vehicle", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 168, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.vehmodeltype, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(225, 168, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1a.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(40, 184, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Index Mark and Registration", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 184, 20, 40), XStringFormats.TopLeft);
                        if (VcnGenerated.vehregno != null)
                            gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(225, 184, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Number of Vehicle Insured", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 196, 122, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1b.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(40, 212, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Chassis Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 212, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(225, 212, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1c.  ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(40, 229, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Engine Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 229, 20, 40), XStringFormats.TopLeft);
                        if (VcnGenerated.engineNo != null)
                            gfx.DrawString(VcnGenerated.engineNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(225, 229, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1d. ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(40, 246, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("VIN/Serial No", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 246, 20, 40), XStringFormats.TopLeft);
                        if (VcnGenerated.VIN != null)
                            gfx.DrawString(VcnGenerated.VIN, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(225, 246, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("2. ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(40, 261, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Name of Policyholder", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 261, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(225, 261, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(40, 283, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Effective date of the commencement of Insurance ", new XFont("Times New Roman", 11), XBrushes.Black, new XRect(68, 283, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString("for purposes of the Law", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 295, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(225, 295, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.effectiveTime != "" ? Convert.ToDateTime(VcnGenerated.effectiveTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(355, 295, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4.  ", new XFont("Times New Roman", 11), XBrushes.Black, new XRect(40, 313, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Date of Expiry of Insurance", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 313, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(225, 313, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.expiryTime != "" ? Convert.ToDateTime(VcnGenerated.expiryTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(355, 313, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("5.        Persons or classes of persons entitled to drive", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(40, 333, 20, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.authorizedwording != null)
                            tf.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 345, 400, 40), XStringFormats.TopLeft);

                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive" + " the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any" + " enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 439, 500, 40), XStringFormats.TopLeft);

                        gfx.DrawString("6.       Limitations as to use", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(40, 493, 20, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.limitsofuse != null)
                            tf.DrawString(VcnGenerated.limitsofuse.ToUpper(), new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 506, 510, 40), XStringFormats.TopLeft);

                        tf.DrawString("The Policy does not cover use for hire or reward or commercial travelling racing pace-making reliability trial speed-testing the carriage of goods or samples in connection with any trade or business or use for any purpose in connection with the Motor Trade.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 547, 510, 40), XStringFormats.TopLeft);

                        gfx.DrawString("(Limitations rendered inoperative by Section 8 (2) of the Act are not to be included under this heading).", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(68, 625, 270, 40), XStringFormats.TopLeft);

                        tf.DrawString("I/WE HEREBY CERTIFY that the Policy to which this Certificate relates is issued in accordance with the provisions of the above-mentioned Law.", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(40, 637, 500, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Issued on This     " + AddOrdinal(System.DateTime.Now.Day) + " day of " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(40, 673, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("KEY INSURANCE COMPANY LIMITED", new XFont("Times New Roman", 11), XBrushes.Black, new XRect(305, 700, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawLine(pen, 300, 743, 520, 743);
                        gfx.DrawString("(Authorized Signature)", new XFont("Times New Roman", 11, XFontStyle.Italic), XBrushes.Black, new XRect(350, 750, 95, 40), XStringFormats.TopLeft);
                        //////////////////////  Adding an image  //////////////////

                        if (VcnGenerated.LogoLocation != "" && VcnGenerated.LogoLocation != null)
                        {
                            try
                            {
                                string Paths = "";
                                Paths = VcnGenerated.LogoLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                                PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);

                                gfx.DrawImage(ximg, 18, 690, 120, 90);
                            }
                            catch (Exception)
                            {

                            }
                           
                        }

                        ////////////////////////////////////////////////////////////

                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcnGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {

                            }
                           
                        }
                    }
                    else
                    {
                        // Draw the text 
                        gfx.DrawString("Jamaica", new XFont("Arial", 12, XFontStyle.Bold), XBrushes.Black, new XRect(270, 50, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(185, 75, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW, CAP 257", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(180, 100, 200, 40), XStringFormats.TopCenter);

                        gfx.DrawString("Certificate No.  " + VcnGenerated.certitificateNo, new XFont("Times New Roman", 13, XFontStyle.Bold), XBrushes.Black, new XRect(40, 140, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Policy No: " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(225, 141, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(455, 130, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(40, 168, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Year & Make of Vehicle", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 168, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.vehmodeltype, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(225, 168, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Number of Vehicle Insured", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 196, 122, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1a.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(40, 212, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Chassis Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 212, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(225, 212, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1b.  ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(40, 229, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Engine Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 229, 20, 40), XStringFormats.TopLeft);
                        if (VcnGenerated.engineNo != null)
                            gfx.DrawString(VcnGenerated.engineNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(225, 229, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1c. ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(40, 246, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("VIN/Serial No", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 246, 20, 40), XStringFormats.TopLeft);
                        if (VcnGenerated.VIN != null)
                            gfx.DrawString(VcnGenerated.VIN, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(225, 246, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("2. ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(40, 261, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Name of Policyholder", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 261, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(225, 261, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(40, 283, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Effective date of the commencement of Insurance ", new XFont("Times New Roman", 11), XBrushes.Black, new XRect(68, 283, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString("for purposes of the Law", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 295, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(225, 295, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.effectiveTime != "" ? Convert.ToDateTime(VcnGenerated.effectiveTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(355, 295, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4.  ", new XFont("Times New Roman", 11), XBrushes.Black, new XRect(40, 313, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Date of Expiry of Insurance", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 313, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(225, 313, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.expiryTime != "" ? Convert.ToDateTime(VcnGenerated.expiryTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(355, 313, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("5.        Persons or classes of persons entitled to drive", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(40, 333, 20, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.authorizedwording != null)
                            tf.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 345, 400, 40), XStringFormats.TopLeft);

                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive" + " the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any" + " enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 439, 500, 40), XStringFormats.TopLeft);

                        gfx.DrawString("6.       Limitations as to use", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(40, 493, 20, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.limitsofuse != null)
                            tf.DrawString(VcnGenerated.limitsofuse.ToUpper(), new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 506, 510, 40), XStringFormats.TopLeft);

                        tf.DrawString("The Policy does not cover use for hire or reward or commercial travelling racing pace-making reliability trial speed-testing the carriage of goods or samples in connection with any trade or business or use for any purpose in connection with the Motor Trade.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(68, 547, 510, 40), XStringFormats.TopLeft);

                        gfx.DrawString("(Limitations rendered inoperative by Section 8 (2) of the Act are not to be included under this heading).", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(68, 625, 270, 40), XStringFormats.TopLeft);

                        tf.DrawString("I/WE HEREBY CERTIFY that the Policy to which this Certificate relates is issued in accordance with the provisions of the above-mentioned Law.", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(40, 637, 500, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Issued on This     " + AddOrdinal(System.DateTime.Now.Day) + " day of " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(40, 673, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("KEY INSURANCE COMPANY LIMITED", new XFont("Times New Roman", 11), XBrushes.Black, new XRect(305, 700, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawLine(pen, 300, 743, 520, 743);
                        gfx.DrawString("(Authorized Signature)", new XFont("Times New Roman", 11, XFontStyle.Italic), XBrushes.Black, new XRect(350, 750, 95, 40), XStringFormats.TopLeft);
                        //////////////////////  Adding an image  //////////////////

                        if (VcnGenerated.LogoLocation != "" && VcnGenerated.LogoLocation != null)
                        {
                            try
                            {
                                string Paths = "";
                                Paths = VcnGenerated.LogoLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                                PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);

                                gfx.DrawImage(ximg, 18, 690, 120, 90);
                            }
                            catch (Exception)
                            {

                            }
                            
                        }

                        ////////////////////////////////////////////////////////////

                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcnGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {

                            }
                           
                        }
                    }

                    
                }
                ///////////////////////////////////////////////////////////////////

                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }


        }


        /// <summary>
        /// This function is used to create the pdf for BCIC's certificates  (for printing)
        /// </summary>
        /// 
        private static PdfDocument JNGIprint(List<VehicleCoverNoteGenerated> generatedCerts)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();


                pdf.Info.Title = "Print of Certificate";

                foreach (VehicleCoverNoteGenerated VcnGenerated in generatedCerts)
                {

                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));

                    // Create a font
                    XFont fontBold = new XFont("Arial", 14, XFontStyle.Bold);


                    /*************************** JNGI'S CERTIFICATE LAYOUT *****************************/

                    //if no Registration Number is provided the generate a certificate that has no mention of Registration Number

                    if (VcnGenerated.vehregno != null || VcnGenerated.vehregno != "")
                    {
                        gfx.DrawString(VcnGenerated.certificateType.ToUpper(), new XFont("Times New Roman", 16, XFontStyle.Bold), XBrushes.Black, new XRect(550, 40, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("JAMAICA", new XFont("Times New Roman", 20, XFontStyle.Bold), XBrushes.Black, new XRect(272, 40, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("CERTIFICATE OF MOTOR INSURANCE", new XFont("Times New Roman", 16, XFontStyle.Bold), XBrushes.Black, new XRect(165, 70, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("THE MOTOR VEHICLES INSUANCE (THIRD PARTY RISK) ACT", new XFont("Times New Roman", 8, XFontStyle.Bold), XBrushes.Black, new XRect(195, 95, 200, 40), XStringFormats.TopLeft);


                        //gfx.DrawString("CERTIFICATE No. ", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(140, 115, 30, 40), XStringFormats.TopLeft);
                        //gfx.DrawString(VcnGenerated.certitificateNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 115, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1  POLICY #: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 145, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.PolicyNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 145, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("2  VEHICLE DESCRIPTION: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 165, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.vehdesc, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 165, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3  REGISTRATION #: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 185, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 185, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4  CHASSIS #: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 205, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 205, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("5  ENGINE #: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 225, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.engineNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 225, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("6  NAME OF POLICY HOLDER: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 245, 165, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.PolicyHolders.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 245, 300, 10), XStringFormats.TopLeft);

                        gfx.DrawString("7  EFFECTIVE DATE OF THE COMMENCEMENT OF INSURANCE FOR THE ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 265, 400, 40), XStringFormats.TopLeft);
                        gfx.DrawString("PURPOSE THE LAW", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(60, 278, 400, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("dd MMMM yyyy") : VcnGenerated.effectivedate.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 278, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("8  DATE OF EXPIRY OF INSURANCE", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 295, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("dd MMMM yyyy") : VcnGenerated.expirydate.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 295, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("9  PERSONS OR CLASSES OF PERSONS ENTITLED TO DRIVE", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 325, 200, 40), XStringFormats.TopLeft);


                        if (VcnGenerated.authorizedwording != null)
                        {
                            List<string> Words = new List<string>();
                            Words.AddRange(VcnGenerated.authorizedwording.Split('.'));

                            int x = 70;
                            int y = 350;

                            foreach (var w in Words)
                            {
                                tf.DrawString(w.Trim(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(x, y, 570, 50), XStringFormats.TopLeft);
                                y += 10;
                            }



                        }



                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(60, 400, 510, 50), XStringFormats.TopLeft);

                        gfx.DrawString("10 LIMITATIONS AS TO USE*", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(47, 455, 20, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.limitsofuse != null)
                            tf.DrawString(VcnGenerated.limitsofuse, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(60, 472, 510, 50), XStringFormats.TopLeft);


                      //  gfx.DrawString("BUT SUBJECT TO THE EXCLUSIONS AS REFERRED TO BELOW", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(60, 520, 570, 40), XStringFormats.TopLeft);

                     //   gfx.DrawString("Use for racing, pace-making, reliability trial, and speed testing", new XFont("Times New Roman", 8), XBrushes.Black, new XRect(60, 535, 570, 40), XStringFormats.TopLeft);

                    //    tf.DrawString("Use for hire or reward, commercial travelling, the carriage of goods (other than samples )in connection with any trade or business or for any", new XFont("Times New Roman", 8), XBrushes.Black, new XRect(60, 550, 570, 40), XStringFormats.TopLeft);

                     //   tf.DrawString("purpose in connection with the Motor Trade.", new XFont("Times New Roman", 8), XBrushes.Black, new XRect(60, 560, 570, 40), XStringFormats.TopLeft);


                        gfx.DrawLine(pen, 45, 590, 570, 590);

                        tf.DrawString("*Limitations rendered inoperative by section 8(2) of the law are not to be included under this heading.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(60, 592, 570, 40), XStringFormats.TopLeft);

                        tf.DrawString("I/WE HEREBY CERTIFY", new XFont("Times New Roman", 14, XFontStyle.Bold), XBrushes.Black, new XRect(60, 630, 570, 40), XStringFormats.TopLeft);

                        tf.DrawString("that the Policy to which this Certificate relates is issued in accordance with", new XFont("Times New Roman", 8), XBrushes.Black, new XRect(260, 629, 250, 40), XStringFormats.TopLeft);

                        tf.DrawString("the provision of the above mentioned law", new XFont("Times New Roman", 8), XBrushes.Black, new XRect(260, 637, 250, 40), XStringFormats.TopLeft);


                        tf.DrawString("JN GENERAL INSURANCE COMPANY LIMITED", new XFont("Times New Roman", 12), XBrushes.Black, new XRect(295, 665, 300, 40), XStringFormats.TopLeft);


                        gfx.DrawLine(pen, 285, 730, 570, 730);


                        gfx.DrawString(Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 8), XBrushes.LightSlateGray, new XRect(440, 760, 95, 40), XStringFormats.TopLeft);

                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcnGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {

                            }
                           
                        }
                    }
                    else
                    {
                        gfx.DrawString(VcnGenerated.certificateType.ToUpper(), new XFont("Times New Roman", 13, XFontStyle.Bold), XBrushes.Black, new XRect(400, 20, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Jamaica", new XFont("Times New Roman", 16, XFontStyle.Bold), XBrushes.Black, new XRect(270, 30, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW, 1939", new XFont("Times New Roman", 8), XBrushes.Black, new XRect(180, 55, 200, 40), XStringFormats.TopLeft);

                        gfx.DrawString("CERTIFICATE OF MOTOR INSURANCE", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 85, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("CERTIFICATE No. ", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(140, 115, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.certitificateNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 115, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1  POLICY #: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 145, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.PolicyNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 145, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("2  VEHICLE DESCRIPTION: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 165, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.vehdesc, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 165, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3  CHASSIS #: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 195, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 195, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4  ENGINE #: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 215, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.engineNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 215, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("5  NAME OF POLICY HOLDER: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 235, 165, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.PolicyHolders.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 235, 300, 10), XStringFormats.TopLeft);

                        gfx.DrawString("6  EFFECTIVE DATE OF THE COMMENCEMENT OF INSURANCE FOR THE ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 255, 400, 40), XStringFormats.TopLeft);
                        gfx.DrawString("PURPOSE THE LAW", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(150, 268, 400, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.effectivedate.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 268, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("7  DATE OF EXPIRY OF INSURANCE", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 285, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.expirydate.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 285, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("8  PERSONS OR CLASSES OF PERSONS ENTITLED TO DRIVE", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 315, 200, 40), XStringFormats.TopLeft);


                        if (VcnGenerated.authorizedwording != null)
                        {
                            List<string> Words = new List<string>();
                            Words.AddRange(VcnGenerated.authorizedwording.Split('.'));

                            int x = 145;
                            int y = 340;

                            foreach (var w in Words)
                            {
                                tf.DrawString(w, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(x, y, 470, 50), XStringFormats.TopLeft);
                            }

                            x += 10; y += 10;

                        }



                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 390, 350, 50), XStringFormats.TopLeft);

                        gfx.DrawString("9  LIMITATIONS AS TO USE*", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 445, 20, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.limitsofuse != null)
                            tf.DrawString(VcnGenerated.limitsofuse, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(150, 462, 270, 50), XStringFormats.TopLeft);


                        gfx.DrawString("BUT SUBJECT TO THE EXCLUSIONS AS REFERRED TO BELOW", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(145, 505, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Use for racing, pace-making, reliability trial, and speed testing", new XFont("Times New Roman", 8, XFontStyle.Bold), XBrushes.Black, new XRect(150, 525, 20, 40), XStringFormats.TopLeft);

                        tf.DrawString("Use for Hire or reward or for commercial travelling or racing pace-making reliability trial speed-testing the carriage of goods or samples in connection with any trade or business or use for any purpose in connection with the Motor Trade.", new XFont("Times New Roman", 8, XFontStyle.Bold), XBrushes.Black, new XRect(150, 543, 290, 40), XStringFormats.TopLeft);

                        gfx.DrawString(Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 8), XBrushes.LightSlateGray, new XRect(440, 760, 95, 40), XStringFormats.TopLeft);

                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcnGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {

                            }
                           
                        }
                    }

                }

                ///////////////////////////////////////////////////////////////////

                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }

        }


        //private static PdfDocument JNGIprint(List<VehicleCoverNoteGenerated> generatedCerts)
        //{
        //    try
        //    {
        //        PdfDocument pdf = new PdfDocument();
        //        Document doc = new Document();


        //        pdf.Info.Title = "Print of Certificate";

        //        foreach (VehicleCoverNoteGenerated VcnGenerated in generatedCerts)
        //        {

        //            PdfPage page = pdf.AddPage();
        //            page.Size = PageSize.Letter;

        //            // Get an XGraphics object for drawing
        //            XGraphics gfx = XGraphics.FromPdfPage(page);
        //            XTextFormatter tf = new XTextFormatter(gfx);
        //            XPen pen = new XPen(XColor.FromName("Black"));

        //            // Create a font
        //            XFont fontBold = new XFont("Arial", 14, XFontStyle.Bold);


        //            /*************************** JNGI'S CERTIFICATE LAYOUT *****************************/

        //            //if no Registration Number is provided the generate a certificate that has no mention of Registration Number

        //            if (VcnGenerated.vehregno != null || VcnGenerated.vehregno != "")
        //            {
        //                gfx.DrawString(VcnGenerated.certificateType.ToUpper(), new XFont("Times New Roman", 13, XFontStyle.Bold), XBrushes.Black, new XRect(400, 20, 20, 40), XStringFormats.TopLeft);
        //                gfx.DrawString("Jamaica", new XFont("Times New Roman", 16, XFontStyle.Bold), XBrushes.Black, new XRect(270, 30, 20, 40), XStringFormats.TopLeft);
        //                gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW, 1939", new XFont("Times New Roman", 8), XBrushes.Black, new XRect(180, 55, 200, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("CERTIFICATE OF MOTOR INSURANCE", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 85, 30, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("CERTIFICATE No. ", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(140, 115, 30, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.certitificateNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 115, 30, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("1  POLICY #: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 145, 30, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.PolicyNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 145, 30, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("2  VEHICLE DESCRIPTION: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 165, 30, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.vehdesc, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 165, 30, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("3  REGISTRATION #: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 185, 30, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 185, 30, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("4  CHASSIS #: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 205, 200, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 205, 30, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("5  ENGINE #: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 225, 200, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.engineNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 225, 30, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("6  NAME OF POLICY HOLDER: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 245, 165, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.PolicyHolders.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 245, 300, 10), XStringFormats.TopLeft);

        //                gfx.DrawString("7  EFFECTIVE DATE OF THE COMMENCEMENT OF INSURANCE FOR THE ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 265, 400, 40), XStringFormats.TopLeft);
        //                gfx.DrawString("PURPOSE THE LAW", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(150, 278, 400, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.effectivedate.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 278, 20, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("8  DATE OF EXPIRY OF INSURANCE", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 295, 200, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.expirydate.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 295, 20, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("9  PERSONS OR CLASSES OF PERSONS ENTITLED TO DRIVE", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 325, 200, 40), XStringFormats.TopLeft);


        //                if (VcnGenerated.authorizedwording != null)
        //                {
        //                    List<string> Words = new List<string>();
        //                    Words.AddRange(VcnGenerated.authorizedwording.Split('.'));

        //                    int x = 145;
        //                    int y = 350;

        //                    foreach (var w in Words)
        //                    {
        //                        tf.DrawString(w, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(x, y, 470, 50), XStringFormats.TopLeft);
        //                    }

        //                    x += 10; y += 10;

        //                }



        //                tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 400, 350, 50), XStringFormats.TopLeft);

        //                gfx.DrawString("10  LIMITATIONS AS TO USE*", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 455, 20, 40), XStringFormats.TopLeft);

        //                if (VcnGenerated.limitsofuse != null)
        //                    tf.DrawString(VcnGenerated.limitsofuse, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(150, 472, 270, 50), XStringFormats.TopLeft);


        //                gfx.DrawString("BUT SUBJECT TO THE EXCLUSIONS AS REFERRED TO BELOW", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(145, 515, 20, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("Use for racing, pace-making, reliability trial, and speed testing", new XFont("Times New Roman", 8, XFontStyle.Bold), XBrushes.Black, new XRect(150, 535, 20, 40), XStringFormats.TopLeft);

        //                tf.DrawString("Use for Hire or reward or for commercial travelling or racing pace-making reliability trial speed-testing the carriage of goods or samples in connection with any trade or business or use for any purpose in connection with the Motor Trade.", new XFont("Times New Roman", 8, XFontStyle.Bold), XBrushes.Black, new XRect(150, 553, 290, 40), XStringFormats.TopLeft);

        //                gfx.DrawString(Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 8), XBrushes.LightSlateGray, new XRect(440, 760, 95, 40), XStringFormats.TopLeft);

        //                /////////////////////////////// WaterMark ///////////////////////////

        //                if (VcnGenerated.cancelled)
        //                {
        //                    XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
        //                    gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
        //                }
        //            }
        //            else
        //            {
        //                gfx.DrawString(VcnGenerated.certificateType.ToUpper(), new XFont("Times New Roman", 13, XFontStyle.Bold), XBrushes.Black, new XRect(400, 20, 20, 40), XStringFormats.TopLeft);
        //                gfx.DrawString("Jamaica", new XFont("Times New Roman", 16, XFontStyle.Bold), XBrushes.Black, new XRect(270, 30, 20, 40), XStringFormats.TopLeft);
        //                gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW, 1939", new XFont("Times New Roman", 8), XBrushes.Black, new XRect(180, 55, 200, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("CERTIFICATE OF MOTOR INSURANCE", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 85, 30, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("CERTIFICATE No. ", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(140, 115, 30, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.certitificateNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 115, 30, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("1  POLICY #: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 145, 30, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.PolicyNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 145, 30, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("2  VEHICLE DESCRIPTION: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 165, 30, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.vehdesc, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 165, 30, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("3  CHASSIS #: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 195, 200, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 195, 30, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("4  ENGINE #: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 215, 200, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.engineNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 215, 30, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("5  NAME OF POLICY HOLDER: ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 235, 165, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.PolicyHolders.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 235, 300, 10), XStringFormats.TopLeft);

        //                gfx.DrawString("6  EFFECTIVE DATE OF THE COMMENCEMENT OF INSURANCE FOR THE ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 255, 400, 40), XStringFormats.TopLeft);
        //                gfx.DrawString("PURPOSE THE LAW", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(150, 268, 400, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.effectivedate.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 268, 20, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("7  DATE OF EXPIRY OF INSURANCE", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 285, 200, 40), XStringFormats.TopLeft);
        //                gfx.DrawString(VcnGenerated.expirydate.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 285, 20, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("8  PERSONS OR CLASSES OF PERSONS ENTITLED TO DRIVE", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 315, 200, 40), XStringFormats.TopLeft);


        //                if (VcnGenerated.authorizedwording != null)
        //                {
        //                    List<string> Words = new List<string>();
        //                    Words.AddRange(VcnGenerated.authorizedwording.Split('.'));

        //                    int x = 145;
        //                    int y = 340;

        //                    foreach (var w in Words)
        //                    {
        //                        tf.DrawString(w, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(x, y, 470, 50), XStringFormats.TopLeft);
        //                    }

        //                    x += 10; y += 10;

        //                }



        //                tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 390, 350, 50), XStringFormats.TopLeft);

        //                gfx.DrawString("9  LIMITATIONS AS TO USE*", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(140, 445, 20, 40), XStringFormats.TopLeft);

        //                if (VcnGenerated.limitsofuse != null)
        //                    tf.DrawString(VcnGenerated.limitsofuse, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(150, 462, 270, 50), XStringFormats.TopLeft);


        //                gfx.DrawString("BUT SUBJECT TO THE EXCLUSIONS AS REFERRED TO BELOW", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(145, 505, 20, 40), XStringFormats.TopLeft);

        //                gfx.DrawString("Use for racing, pace-making, reliability trial, and speed testing", new XFont("Times New Roman", 8, XFontStyle.Bold), XBrushes.Black, new XRect(150, 525, 20, 40), XStringFormats.TopLeft);

        //                tf.DrawString("Use for Hire or reward or for commercial travelling or racing pace-making reliability trial speed-testing the carriage of goods or samples in connection with any trade or business or use for any purpose in connection with the Motor Trade.", new XFont("Times New Roman", 8, XFontStyle.Bold), XBrushes.Black, new XRect(150, 543, 290, 40), XStringFormats.TopLeft);

        //                gfx.DrawString(Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 8), XBrushes.LightSlateGray, new XRect(440, 760, 95, 40), XStringFormats.TopLeft);

        //                /////////////////////////////// WaterMark ///////////////////////////

        //                if (VcnGenerated.cancelled)
        //                {
        //                    XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
        //                    gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
        //                }
        //            }

        //        }

        //        ///////////////////////////////////////////////////////////////////

        //        return pdf;
        //    }
        //    catch (Exception)
        //    {
        //        return new PdfDocument();
        //    }

        //}

        /// <summary>
        /// This function is used to create the pdf for GGJ's certificates  (for printing)
        /// </summary>
        private static PdfDocument 
        GGJprint(List<VehicleCoverNoteGenerated> generatedCerts)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();


                pdf.Info.Title = "Print of Certificate";

                foreach (VehicleCoverNoteGenerated VcnGenerated in generatedCerts)
                {

                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"), 0.5);

                    // Create a font
                    XFont fontBold = new XFont("Times New Roman", 14, XFontStyle.Bold);


                    //if no Registration Number is provided the generate a certificate that has no mention of Registration Number

                    if (VcnGenerated.vehregno != null || VcnGenerated.vehregno != "")
                    {
                        // Draw the text 
                        gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(230, 60, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("The Motor Vehicles Insurance (THIRD PARTY RISKS) Act", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(170, 82, 200, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Certificate Number:        " + VcnGenerated.certitificateNo, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 115, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Policy No:        " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(270, 115, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(510, 135, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1.        Make of Vehicle", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 155, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.vehmodeltype, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(270, 155, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1a.      Index Mark and Registration", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 167, 20, 40), XStringFormats.TopLeft);
                        if (VcnGenerated.vehregno != null)
                            gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(270, 167, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Number of Vehicle Insured", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(63, 178, 122, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1b.      Chassis Number", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 190, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(270, 190, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("2.        Name of Policyholder", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 200, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(270, 200, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3.        Effective date of the commencement of", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 230, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Insurance for purposes of the Act", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(63, 242, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy hh:mm ttt"):"", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(270, 242, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4.        Date of Expiry of Insurance", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 254, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy hh:mm ttt") : "", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(270, 254, 20, 40), XStringFormats.TopLeft);


                        gfx.DrawString("5.        Persons or classes of persons entitled to drive*", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 280, 20, 40), XStringFormats.TopLeft);

                        int y_axis = 0;
                        //if (VcnGenerated.authorizedwording != null) 
                        //{
                        //    foreach(string Line in SplitWords(VcnGenerated.authorizedwording))
                        //    {
                        //        tf.DrawString(Line, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63 , 293 + y_axis, 500, 40), XStringFormats.TopLeft);
                        //        y_axis += 20;
                        //    }
                           
                        //}

                        tf.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 293, 500, 40), XStringFormats.TopLeft);
                          
                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 365, 500, 40), XStringFormats.TopLeft);

                        gfx.DrawString("6.        Limitations as to use*", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 425, 20, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.limitsofuse != null)
                            tf.DrawString(VcnGenerated.limitsofuse, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 445, 460, 160), XStringFormats.TopLeft);

                        //tf.DrawString("THE POLICY DOES NOT COVER use for hire or reward or commercial travelling racing pace-making reliability trial speed-testing the carriage of goods or samples in connection with any trade or business or use for any purpose in connection with the Motor Trade.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 610, 510, 50), XStringFormats.TopLeft);

                        gfx.DrawString("*(Limitations rendered inoperative by Section 8 (2) of the Act are not to be included under this heading).", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 650, 20, 40), XStringFormats.TopLeft);

                        //gfx.DrawString("I/WE HEREBY CERTIFY", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(63, 660, 20, 40), XStringFormats.TopLeft);
                        //gfx.DrawString("that the Policy to which this Certificate relates is issued in " + "accordance with the ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 670, 260, 40), XStringFormats.TopLeft);
                        //gfx.DrawString("provisions of the abovementioned Act.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 680, 20, 40), XStringFormats.TopLeft);


                      
                        gfx.DrawString("I/WE HEREBY CERTIFY", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(63, 665, 20, 40), XStringFormats.TopLeft);
                        tf.DrawString("that the Policy to which this Certificate relates is issued in ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(205, 665, 265, 40), XStringFormats.TopLeft);
                       
                        tf.DrawString("accordance with the provisions of the above-mentioned Act.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 680, 265, 40), XStringFormats.TopLeft);



                        gfx.DrawString("Issued on         " + AddOrdinal(System.DateTime.Now.Day) + " day of " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(63, 700, 20, 40), XStringFormats.TopLeft);
    
                        //gfx.DrawString("Issued this   " + AddOrdinal(System.DateTime.Now.Day) + " day of  " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 700, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Signed on behalf of the Company", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(360, 720, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawLine(pen, 360, 760, 520, 760);
                        gfx.DrawString("Authorized Signature", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(390, 760, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(55, 780, 95, 40), XStringFormats.TopLeft);

                        //////////////////////  Adding an image  ////////////////////

                        if (VcnGenerated.FooterImgLocation != "" && VcnGenerated.FooterImgLocation != null)
                        {
                            try
                            {
                                string Paths = "";
                                Paths = VcnGenerated.FooterImgLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                                PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);
                                gfx.DrawImage(ximg, 60, 710, 120, 70);
                            }
                            catch (Exception)
                            {
                             
                            }
                           
                        }

                        ////////////////////////////////////////////////////////////


                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcnGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {
                           
                            }
                           
                        }
                    }
                    else
                    {
                        // Draw the text 
                        gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(230, 60, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("The Motor Vehicles Insurance (THIRD PARTY RISKS) Act", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(170, 82, 200, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Certificate Number:        " + VcnGenerated.certitificateNo, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 115, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Policy No:        " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(270, 115, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(510, 135, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1.        Make of Vehicle", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 155, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.vehmodeltype, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(270, 155, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1a.      Index Mark and Registration", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 167, 20, 40), XStringFormats.TopLeft);
                        if (VcnGenerated.vehregno != null)
                            gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(270, 167, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Number of Vehicle Insured", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(63, 178, 122, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1b.      Chassis Number", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 190, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(270, 190, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("2.        Name of Policyholder", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 200, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(270, 200, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3.        Effective date of the commencement of", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 230, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Insurance for purposes of the Act", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(63, 242, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(270, 242, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4.        Date of Expiry of Insurance", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 254, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(270, 254, 20, 40), XStringFormats.TopLeft);


                        gfx.DrawString("5.        Persons or classes of persons entitled to drive*", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 280, 20, 40), XStringFormats.TopLeft);

                        int y_axis = 0;
                        //if (VcnGenerated.authorizedwording != null)
                        //{
                        //    foreach (string Line in SplitWords(VcnGenerated.authorizedwording))
                        //    {
                        //        tf.DrawString(Line, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 293 + y_axis, 500, 40), XStringFormats.TopLeft);
                        //        y_axis += 20;
                        //    }

                        //}

                        tf.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 293 + y_axis, 500, 40), XStringFormats.TopLeft);

                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 365, 500, 40), XStringFormats.TopLeft);

                        gfx.DrawString("6.        Limitations as to use*", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 425, 20, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.limitsofuse != null)
                            tf.DrawString(VcnGenerated.limitsofuse, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 445, 406, 60), XStringFormats.TopLeft);

                        tf.DrawString("THE POLICY DOES NOT COVER use for hire or reward or commercial travelling racing pace-making reliability trial speed-testing the carriage of goods or samples in connection with any trade or business or use for any purpose in connection with the Motor Trade.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 540, 510, 50), XStringFormats.TopLeft);

                        gfx.DrawString("*(Limitations rendered inoperative by Section 8 (2) of the Act are not to be included under this heading).", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 575, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("I/WE HEREBY CERTIFY", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(63, 595, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("that the Policy to which this Certificate relates is issued in " + "accordance with the ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 585, 260, 40), XStringFormats.TopLeft);
                        gfx.DrawString("provisions of the abovementioned Act.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 608, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Issued this   " + AddOrdinal(System.DateTime.Now.Day) + " day of  " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 630, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Signed on behalf of the Company", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(360, 660, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawLine(pen, 360, 720, 520, 720);
                        gfx.DrawString("Authorized Signature", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(400, 720, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(55, 780, 95, 40), XStringFormats.TopLeft);

                        //////////////////////  Adding an image  ////////////////////

                        if (VcnGenerated.FooterImgLocation != "" && VcnGenerated.FooterImgLocation != null)
                        {
                            string Paths = "";
                            Paths = VcnGenerated.FooterImgLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");
                            try
                            {
                                PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);
                                gfx.DrawImage(ximg, 15, 650, 120, 70);
                            }
                            catch (Exception)
                            {
                            
                            }
                           
                        }

                        ////////////////////////////////////////////////////////////


                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcnGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {

                            }
                           
                        }

                        #region OLD Without REG
                        //// Draw the text 
                        //gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(230, 60, 30, 40), XStringFormats.TopLeft);
                        //gfx.DrawString("The Motor Vehicles Insurance (THIRD PARTY RISKS) Act", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(170, 82, 200, 40), XStringFormats.TopLeft);

                        //gfx.DrawString("Certificate Number:        " + VcnGenerated.certitificateNo, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 115, 30, 40), XStringFormats.TopLeft);
                        //gfx.DrawString("Policy No:        " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(270, 115, 30, 40), XStringFormats.TopLeft);
                        //gfx.DrawString(VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(510, 135, 20, 40), XStringFormats.TopLeft);

                        //gfx.DrawString("1.        Make of Vehicle", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 155, 20, 40), XStringFormats.TopLeft);
                        //gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.vehmodeltype, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(270, 155, 20, 40), XStringFormats.TopLeft);

                        //gfx.DrawString("Number of Vehicle Insured", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(63, 178, 122, 40), XStringFormats.TopLeft);

                        //gfx.DrawString("1a.      Chassis Number", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 190, 20, 40), XStringFormats.TopLeft);
                        //gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(270, 190, 20, 40), XStringFormats.TopLeft);

                        //gfx.DrawString("2.        Name of Policyholder", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 200, 20, 40), XStringFormats.TopLeft);
                        //gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(270, 200, 20, 40), XStringFormats.TopLeft);

                        //gfx.DrawString("3.        Effective date of the commencement of", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 230, 200, 40), XStringFormats.TopLeft);
                        //gfx.DrawString("Insurance for purposes of the Act", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(63, 242, 200, 40), XStringFormats.TopLeft);
                        //gfx.DrawString(VcnGenerated.effectivedate, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(270, 242, 20, 40), XStringFormats.TopLeft);

                        //gfx.DrawString("4.        Date of Expiry of Insurance", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 254, 20, 40), XStringFormats.TopLeft);
                        //gfx.DrawString(VcnGenerated.expirydate, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(270, 254, 20, 40), XStringFormats.TopLeft);


                        //gfx.DrawString("5.        Persons or classes of persons entitled to drive*", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 280, 20, 40), XStringFormats.TopLeft);

                        //if (VcnGenerated.authorizedwording != null)
                        //    tf.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 293, 400, 40), XStringFormats.TopLeft);

                        //tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 365, 500, 40), XStringFormats.TopLeft);

                        //gfx.DrawString("6.        Limitations as to use*", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 445, 20, 40), XStringFormats.TopLeft);

                        //if (VcnGenerated.limitsofuse != null)
                        //    tf.DrawString(VcnGenerated.limitsofuse, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 455, 400, 40), XStringFormats.TopLeft);

                        //tf.DrawString("THE POLICY DOES NOT COVER use for hire or reward or commercial travelling racing pace-making reliability trial speed-testing the carriage of goods or samples in connection with any trade or business or use for any purpose in connection with the Motor Trade.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 490, 510, 50), XStringFormats.TopLeft);

                        //gfx.DrawString("*(Limitations rendered inoperative by Section 8 (2) of the Act are not to be included under this heading).", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 550, 20, 40), XStringFormats.TopLeft);

                        //gfx.DrawString("I/WE HEREBY CERTIFY", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(63, 580, 20, 40), XStringFormats.TopLeft);
                        //gfx.DrawString("that the Policy to which this Certificate relates is issued in " + "accordance with the ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(207, 580, 260, 40), XStringFormats.TopLeft);
                        //gfx.DrawString("provisions of the abovementioned Act.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 593, 20, 40), XStringFormats.TopLeft);

                        //gfx.DrawString("Issued this   " + AddOrdinal(System.DateTime.Now.Day) + " day of  " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 620, 20, 40), XStringFormats.TopLeft);

                        //gfx.DrawString("Signed on behalf of the Company", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(360, 660, 95, 40), XStringFormats.TopLeft);

                        //gfx.DrawLine(pen, 360, 720, 520, 720);
                        //gfx.DrawString("Authorized Signature", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(400, 720, 95, 40), XStringFormats.TopLeft);

                        //gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(55, 780, 95, 40), XStringFormats.TopLeft);

                        ////////////////////////  Adding an image  ////////////////////

                        //if (VcnGenerated.FooterImgLocation != "" && VcnGenerated.FooterImgLocation != null)
                        //{
                        //    string Paths = "";
                        //    Paths = VcnGenerated.FooterImgLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                        //    PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);
                        //    gfx.DrawImage(ximg, 15, 650, 250, 70);
                        //}

                        //////////////////////////////////////////////////////////////


                        ///////////////////////////////// WaterMark ///////////////////////////

                        //if (VcnGenerated.cancelled)
                        //{
                        //    XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                        //    gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                        //}

                        #endregion
                    }

                    
                }
                ///////////////////////////////////////////////////////////////////

                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }

        }

        /// <summary>
        /// This function is used to create the pdf for BCIC's certificates  (for printing)
        /// </summary>
        private static PdfDocument BCICprint(List<VehicleCoverNoteGenerated> generatedCerts)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();


                pdf.Info.Title = "Print of Certificate";

                int x = 0; int y = 0;

                foreach (VehicleCoverNoteGenerated VcnGenerated in generatedCerts)
                {

                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));

                    // Create a font
                    XFont fontBold = new XFont("Arial", 14, XFontStyle.Bold);


                    /*************************** BCIC'S CERTIFICATE LAYOUT *****************************/


                    //////////////////////  Adding an image  ////////////////////
                    if (VcnGenerated.LogoLocation != "" && VcnGenerated.LogoLocation != null)
                    {
                        string Paths = "";
                        Paths = VcnGenerated.LogoLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");
                        
                        PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);

                        gfx.DrawImage(ximg, 40, 8, 80, 40);
                    }

                    ////////////////////////////////////////////////////////////

                    //if no Registration Number is provided the generate a certificate that has no mention of Registration Number

                    if (VcnGenerated.vehregno != null || VcnGenerated.vehregno != "")
                    {
                        if (VcnGenerated.certificateCode.Trim() != "" || VcnGenerated.certificateCode.Trim() != null)
                        {
                            gfx.DrawString(VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(480, 10, 20, 40), XStringFormats.TopLeft);
                        }
                        else
                        {
                            gfx.DrawString(VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(510, 10, 20, 40), XStringFormats.TopLeft);
                        }

                        gfx.DrawString("Jamaica", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(270, 30, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW, CAP 257", new XFont("Arial", 12), XBrushes.Black, new XRect(90, 55, 200, 40), XStringFormats.TopLeft);

                        gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(185, 75, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Certificate No.            " + VcnGenerated.certitificateNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(25, 110, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Policy No: " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(340, 105, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1         Make of Vehicle  ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 135, 140, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.vehdesc.TrimStart(), new XFont("Times New Roman", 10), XBrushes.Black, new XRect(300, 135, 160, 40), XStringFormats.TopLeft);


                        gfx.DrawString("           Index Mark and Registration  ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 155, 140, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.vehregno != null)
                            gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Regular), XBrushes.Black, new XRect(300, 155, 140, 40), XStringFormats.TopLeft);



                        gfx.DrawString("           Number of Vehicle Insured  ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 175, 122, 40), XStringFormats.TopLeft);


                        gfx.DrawString("           Chassis Number  ", new XFont("Times New Roman", 10, XFontStyle.Regular), XBrushes.Black, new XRect(25, 195, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Regular), XBrushes.Black, new XRect(300, 195, 200, 40), XStringFormats.TopLeft);


                        gfx.DrawString("           Engine Number  ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 215, 122, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.engineNo, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(300, 215, 140, 40), XStringFormats.TopLeft);


                        gfx.DrawString("2.        Name of Policy Holder        ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 240, 165, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(300, 240, 160, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3.        Effective date of the commencement of Insurance for purposes of the Law", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 265, 298, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(395, 265, 298, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.effectiveTime != "" ? Convert.ToDateTime(VcnGenerated.effectiveTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(510, 265, 298, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4.        Date of Expiry of Insurance", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 285, 95, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(395, 285, 298, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.expiryTime != "" ? Convert.ToDateTime(VcnGenerated.expiryTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(510, 285, 298, 40), XStringFormats.TopLeft);


                        gfx.DrawString("5.        Persons or classes of persons entitled to drive", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 305, 175, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.authorizedwording != null)
                        {
                            //VcnGenerated.authorizedwording.Replace("\n", " ");
                            string NewData = "";

                            string[] TempData = VcnGenerated.authorizedwording.Split('(');

                            foreach (string Line in TempData) { NewData += Line + Environment.NewLine; }


                            tf.DrawString(NewData, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(53, 320, 500, 60), XStringFormats.TopLeft);
                        }
                        // if (VcnGenerated.authorizedwording != null)
                        // gfx.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(53, 275, 400, 40), XStringFormats.TopLeft);

                        int Heights = 110;
                        gfx.DrawString("", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 480, 95, (60 + Heights)), XStringFormats.TopLeft);
                        //The text formatter is used because it wraps the text
                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(53, 440, 500, 80), XStringFormats.TopLeft);

                        gfx.DrawString("6.        Limitiations as to use", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 480, 95, 60), XStringFormats.TopLeft);

                        if (VcnGenerated.limitsofuse != null)
                            tf.DrawString(VcnGenerated.limitsofuse, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(53, 490, 500, 40), XStringFormats.TopLeft);


                        gfx.DrawString("THE POLICY DOES NOT COVER", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(53, 530, 95, 40), XStringFormats.TopLeft);

                        tf.DrawString("Use for hire or reward or commercial travelling racing pace-making reliability trial speed-testing the carriage of\n goods or samples in connection with any trade or business or use for any purpose in connection with the Motor Trade.", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(53, 545, 500, 10), XStringFormats.TopLeft);

                        gfx.DrawString("( Limitations rendered inoperative by Section 6 (2) of the Law are not to be included under this heading)", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(53, 640, 95, 40), XStringFormats.TopLeft);

                        tf.DrawString("I/WE HEREBY CERTIFY ", new XFont("Arial", 11, XFontStyle.Bold), XBrushes.Black, new XRect(53, 660, 500, 40), XStringFormats.TopLeft);
                        tf.DrawString("that the Policy to which this Certificate relates is issued in accordance with the", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(185, 660, 400, 40), XStringFormats.TopLeft);

                        tf.DrawString(" provisions of the above-mentioned Law.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(53, 670, 400, 40), XStringFormats.TopLeft);


                        gfx.DrawString("Issued the   " + AddOrdinal(System.DateTime.Now.Day) + " day of " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 685, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawString("BRITISH CARIBBEAN INSURANCE COMPANY LIMITED", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(240, 685, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawLine(pen, 270, 725, 500, 725);
                        gfx.DrawString("For the Company", new XFont("Times New Roman", 10, XFontStyle.Italic), XBrushes.Black, new XRect(350, 750, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("yyyy, MMMM dd,") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(55, 780, 95, 40), XStringFormats.TopLeft);

                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcnGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {

                            }
                           
                        }

                        x = 0; y = 0;
                    }
                    else
                    {
                        if (VcnGenerated.certificateCode.Trim() != "" || VcnGenerated.certificateCode.Trim() != null)
                        {
                            gfx.DrawString(VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(480, 10, 20, 40), XStringFormats.TopLeft);
                        }
                        else
                        {
                            gfx.DrawString(VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(510, 10, 20, 40), XStringFormats.TopLeft);
                        }

                        gfx.DrawString("Jamaica", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(270, 30, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW, CAP 257", new XFont("Arial", 12), XBrushes.Black, new XRect(90, 55, 200, 40), XStringFormats.TopLeft);

                        gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(185, 75, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Certificate No.            " + VcnGenerated.certitificateNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(25, 110, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Policy No: " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(340, 105, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1         Make of Vehicle  ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 135, 140, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.vehdesc.TrimStart(), new XFont("Times New Roman", 10), XBrushes.Black, new XRect(300, 135, 160, 40), XStringFormats.TopLeft);


                        gfx.DrawString("           Number of Vehicle Insured  ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 175, 122, 40), XStringFormats.TopLeft);


                        gfx.DrawString("           Chassis Number  ", new XFont("Times New Roman", 10, XFontStyle.Regular), XBrushes.Black, new XRect(25, 195, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Regular), XBrushes.Black, new XRect(300, 195, 200, 40), XStringFormats.TopLeft);


                        gfx.DrawString("           Engine Number  ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 215, 122, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.engineNo, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(300, 215, 140, 40), XStringFormats.TopLeft);


                        gfx.DrawString("2.        Name of Policy Holder        ", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 240, 165, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(300, 240, 160, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3.        Effective date of the commencement of Insurance for purposes of the Law", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 265, 298, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(395, 265, 298, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.effectiveTime != "" ? Convert.ToDateTime(VcnGenerated.effectiveTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(510, 265, 298, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4.        Date of Expiry of Insurance", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 285, 95, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(395, 285, 298, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.expiryTime != "" ? Convert.ToDateTime(VcnGenerated.expiryTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(510, 285, 298, 40), XStringFormats.TopLeft);


                        gfx.DrawString("5.        Persons or classes of persons entitled to drive", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 305, 175, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.authorizedwording != null)
                        {
                            //VcnGenerated.authorizedwording.Replace("\n", " ");
                            string NewData = "";

                            string[] TempData = VcnGenerated.authorizedwording.Split('(');

                            foreach (string Line in TempData) { NewData += Line + Environment.NewLine; }


                            tf.DrawString(NewData, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(53, 320, 500, 60), XStringFormats.TopLeft);
                        }
                        // if (VcnGenerated.authorizedwording != null)
                        // gfx.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(53, 275, 400, 40), XStringFormats.TopLeft);

                        int Heights = 110;
                        gfx.DrawString("", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 480, 95, (60 + Heights)), XStringFormats.TopLeft);
                        //The text formatter is used because it wraps the text
                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(53, 440, 500, 80), XStringFormats.TopLeft);

                        gfx.DrawString("6.        Limitiations as to use", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 480, 95, 60), XStringFormats.TopLeft);

                        if (VcnGenerated.limitsofuse != null)
                            tf.DrawString(VcnGenerated.limitsofuse, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(53, 490, 500, 40), XStringFormats.TopLeft);


                        gfx.DrawString("THE POLICY DOES NOT COVER", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(53, 530, 95, 40), XStringFormats.TopLeft);

                        tf.DrawString("Use for hire or reward or commercial travelling racing pace-making reliability trial speed-testing the carriage of\n goods or samples in connection with any trade or business or use for any purpose in connection with the Motor Trade.", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(53, 545, 500, 10), XStringFormats.TopLeft);

                        gfx.DrawString("( Limitations rendered inoperative by Section 6 (2) of the Law are not to be included under this heading)", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(53, 640, 95, 40), XStringFormats.TopLeft);

                        tf.DrawString("I/WE HEREBY CERTIFY ", new XFont("Arial", 11, XFontStyle.Bold), XBrushes.Black, new XRect(53, 660, 500, 40), XStringFormats.TopLeft);
                        tf.DrawString("that the Policy to which this Certificate relates is issued in accordance with the", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(185, 660, 400, 40), XStringFormats.TopLeft);

                        tf.DrawString(" provisions of the above-mentioned Law.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(53, 670, 400, 40), XStringFormats.TopLeft);


                        gfx.DrawString("Issued the   " + AddOrdinal(System.DateTime.Now.Day) + " day of " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10), XBrushes.Black, new XRect(25, 685, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawString("BRITISH CARIBBEAN INSURANCE COMPANY LIMITED", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(240, 685, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawLine(pen, 270, 725, 500, 725);
                        gfx.DrawString("For the Company", new XFont("Times New Roman", 10, XFontStyle.Italic), XBrushes.Black, new XRect(350, 750, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("yyyy, MMMM dd,") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(55, 780, 95, 40), XStringFormats.TopLeft);

                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcnGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {

                            }
                           
                        }

                        x = 0; y = 0;
                    }

                }

                ///////////////////////////////////////////////////////////////////
               
                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }

        }


        /// <summary>
        /// This function is used to create the pdf for GA's certificates (for printing)
        /// </summary>
        private static PdfDocument GAprint(List<VehicleCoverNoteGenerated> generatedCerts)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();


                pdf.Info.Title = "Print of Certificate";

                foreach (VehicleCoverNoteGenerated VcnGenerated in generatedCerts)
                {

                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));

                    // Create a font
                    XFont fontBold = new XFont("Arial", 14, XFontStyle.Bold);



                    /*************************** GENERAL ACCIDENT'S CERTIFICATE LAYOUT *****************************/

                    //if no Registration Number is provided the generate a certificate that has no mention of Registration Number

                    if (VcnGenerated.vehregno != null || VcnGenerated.vehregno != "")
                    {
                        gfx.DrawString("Jamaica", new XFont("Times New Roman", 13, XFontStyle.Bold), XBrushes.Black, new XRect(270, 50, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Times New Roman", 14, XFontStyle.Bold), XBrushes.Black, new XRect(190, 70, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW, CAP 257", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(90, 90, 200, 40), XStringFormats.TopLeft);


                        gfx.DrawString("Certificate No.", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(35, 135, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.certitificateNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(120, 135, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Policy No:    " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(220, 135, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(510, 135, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1.        Make of Vehicle", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 155, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.vehmodeltype, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 155, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1a.      Index Mark and Registration", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 173, 20, 40), XStringFormats.TopLeft);
                        if (VcnGenerated.vehregno != null)
                            gfx.DrawString(VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 177, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Number of Vehicle Insured", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 185, 122, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1b.      Chassis Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 203, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 203, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1c.      Engine Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 222, 20, 40), XStringFormats.TopLeft);
                        if (VcnGenerated.engineNo != null)
                            gfx.DrawString(VcnGenerated.engineNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 222, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("2.        Name of Policyholder", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 240, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 240, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3.        Effective date of the commencement of Insurance for purposes of", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 265, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString("the Law", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 278, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 278, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.effectiveTime != "" ? Convert.ToDateTime(VcnGenerated.effectiveTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 278, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4.        Date of Expiry of Insurance", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 295, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 295, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.expiryTime != "" ? Convert.ToDateTime(VcnGenerated.expiryTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 298, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("5.        Persons or classes of persons entitled to drive", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 315, 20, 40), XStringFormats.TopLeft);

                        //if (VcnGenerated.authorizedwording != null)
                        //    tf.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(64, 328, 300, 40), XStringFormats.TopLeft);

                        //Fix overflow issues
                        int Q = 0;
                        if (VcnGenerated.authorizedwording != null)
                        {
                            Q = 40;
                            List<string> W = new List<string>();
                            try { W.AddRange(VcnGenerated.authorizedwording.Split('(')); }
                            catch (Exception) { }

                            int P = 0;
                            if (W.Count > 0)
                            {
                                foreach (var r in W.ToList())
                                {
                                    tf.DrawString(r, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(64, 308 + P, 300, 40), XStringFormats.TopLeft);
                                    P += 25;
                                }
                            }
                            else
                            {
                                Q = 0;
                                tf.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(64, 328, 300, 40), XStringFormats.TopLeft);
                            }
                        }



                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive" + " the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any" + " enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(64, 420 + Q, 490, 40), XStringFormats.TopLeft);

                        gfx.DrawString("6.        Limitations as to use", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 465 + Q, 20, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.limitsofuse != null)
                            tf.DrawString(VcnGenerated.limitsofuse, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(64, 480 + Q, 500, 80), XStringFormats.TopLeft);

                        //tf.DrawString("The Policy does not cover use for hire or reward or commercial travelling racing pace-making reliability trial speed-testing the carriage of goods or samples in connection with any trade or business or use for any purpose in connection with the Motor Trade.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(64, 505, 490, 50), XStringFormats.TopLeft);

                        gfx.DrawString("I/WE HEREBY CERTIFY", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(66, 595, 20, 40), XStringFormats.TopLeft);
                        tf.DrawString("that the Policy to which this Certificate relates is issued in " + "accordance with the provisions of the above-mentioned Law.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(240, 595, 250, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Issued on         " + AddOrdinal(System.DateTime.Now.Day) + " day of " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 620, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("GENERAL ACCIDENT INSURANCE COMPANY JAMAICA LTD.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(240, 650, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawLine(pen, 290, 705, 500, 705);
                        gfx.DrawString("For the Company", new XFont("Times New Roman", 10, XFontStyle.Italic), XBrushes.Black, new XRect(350, 705, 95, 40), XStringFormats.TopLeft);
                        gfx.DrawString("PrintedOn: " + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(55, 780, 95, 40), XStringFormats.TopLeft);

                        //////////////////////  Adding an image  //////////////////
                        if (VcnGenerated.LogoLocation != "" && VcnGenerated.LogoLocation != null)
                        {
                            string Paths = "";
                            Paths = VcnGenerated.LogoLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                            PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);

                            gfx.DrawImage(ximg, 45, 640, 100, 90);
                        }

                        ////////////////////////////////////////////////////////////

                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcnGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {

                            }
                            
                        }
                    }
                    else
                    {
                        gfx.DrawString("Jamaica", new XFont("Times New Roman", 13, XFontStyle.Bold), XBrushes.Black, new XRect(270, 50, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Times New Roman", 14, XFontStyle.Bold), XBrushes.Black, new XRect(190, 70, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW, CAP 257", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(90, 90, 200, 40), XStringFormats.TopLeft);


                        gfx.DrawString("Certificate No.", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(35, 135, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.certitificateNo, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(120, 135, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Policy No:    " + VcnGenerated.PolicyNo, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(220, 135, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(510, 135, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1.        Make of Vehicle", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 155, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.vehmodeltype, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 155, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Number of Vehicle Insured", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 185, 122, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1a.      Chassis Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 203, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 203, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1b.      Engine Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 222, 20, 40), XStringFormats.TopLeft);
                        if (VcnGenerated.engineNo != null)
                            gfx.DrawString(VcnGenerated.engineNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 222, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("2.        Name of Policyholder", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 240, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 240, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3.        Effective date of the commencement of Insurance for purposes of", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 265, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString("the Law", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 278, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 278, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.effectiveTime != "" ? Convert.ToDateTime(VcnGenerated.effectiveTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 278, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4.        Date of Expiry of Insurance", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 295, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 295, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.expiryTime != "" ? Convert.ToDateTime(VcnGenerated.expiryTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 298, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("5.        Persons or classes of persons entitled to drive", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 315, 20, 40), XStringFormats.TopLeft);

                        //if (VcnGenerated.authorizedwording != null)
                        //    tf.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(64, 328, 300, 40), XStringFormats.TopLeft);

                        //Fix overflow issues
                        int Q = 0;
                        if (VcnGenerated.authorizedwording != null)
                        {
                            Q = 40;
                            List<string> W = new List<string>();
                            try { W.AddRange(VcnGenerated.authorizedwording.Split('(')); }
                            catch (Exception) { }

                            int P = 0;
                            if (W.Count > 0)
                            {
                                foreach (var r in W.ToList())
                                {
                                    tf.DrawString(r, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(64, 308 + P, 300, 40), XStringFormats.TopLeft);
                                    P += 25;
                                }
                            }
                            else
                            {
                                Q = 0;
                                tf.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(64, 328, 300, 40), XStringFormats.TopLeft);
                            }
                        }



                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive" + " the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any" + " enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(64, 420 + Q, 490, 40), XStringFormats.TopLeft);

                        gfx.DrawString("6.        Limitations as to use", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 465 + Q, 20, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.limitsofuse != null)
                            tf.DrawString(VcnGenerated.limitsofuse, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(64, 480 + Q, 500, 80), XStringFormats.TopLeft);

                        //tf.DrawString("The Policy does not cover use for hire or reward or commercial travelling racing pace-making reliability trial speed-testing the carriage of goods or samples in connection with any trade or business or use for any purpose in connection with the Motor Trade.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(64, 505, 490, 50), XStringFormats.TopLeft);

                        gfx.DrawString("I/WE HEREBY CERTIFY", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(66, 595, 20, 40), XStringFormats.TopLeft);
                        tf.DrawString("that the Policy to which this Certificate relates is issued in " + "accordance with the provisions of the above-mentioned Law.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(240, 595, 250, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Issued on         " + AddOrdinal(System.DateTime.Now.Day) + " day of " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 620, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("GENERAL ACCIDENT INSURANCE COMPANY JAMAICA LTD.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(240, 650, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawLine(pen, 290, 705, 500, 705);
                        gfx.DrawString("For the Company", new XFont("Times New Roman", 10, XFontStyle.Italic), XBrushes.Black, new XRect(350, 705, 95, 40), XStringFormats.TopLeft);
                        gfx.DrawString("PrintedOn: " + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(55, 780, 95, 40), XStringFormats.TopLeft);

                        //////////////////////  Adding an image  //////////////////
                        if (VcnGenerated.LogoLocation != "" && VcnGenerated.LogoLocation != null)
                        {
                            string Paths = "";
                            Paths = VcnGenerated.LogoLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                            PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);

                            gfx.DrawImage(ximg, 45, 640, 100, 90);
                        }

                        ////////////////////////////////////////////////////////////

                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcnGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {

                            }
                            
                        }
                    }

                }

                ///////////////////////////////////////////////////////////////////

                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }

        }


        /// <summary>
        /// This function is used to create the pdf for ICWI's certificates (for printing)
        /// </summary>
        private static PdfDocument ICWIprint(List<VehicleCoverNoteGenerated> generatedCerts)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();


                pdf.Info.Title = "Print of Certificate";

                foreach (VehicleCoverNoteGenerated VcnGenerated in generatedCerts)
                {

                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));
                    XPen pen1 = new XPen(XColor.FromName("Black"), 1.2);

                    // Create a font
                    XFont fontBold = new XFont("Arial", 14, XFontStyle.Bold);

                    //if no Registration Number is provided the generate a certificate that has no mention of Registration Number

                    if (VcnGenerated.vehregno != null || VcnGenerated.vehregno != "")
                    {
                        // Draw the text 
                        gfx.DrawString("Jamaica", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(270, 30, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) ACT", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(100, 55, 200, 40), XStringFormats.TopLeft);

                        gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Times New Roman", 16), XBrushes.Black, new XRect(180, 75, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString(VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(470, 80, 20, 40), XStringFormats.TopLeft);

                        //Rectangular box surrounding certificate information
                        gfx.DrawRectangle(pen1, new XRect(35, 100, 520, 485));

                        gfx.DrawString("Certificate No.            " + VcnGenerated.certitificateNo, new XFont("Arial", 13, XFontStyle.Bold), XBrushes.Black, new XRect(50, 105, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Policy No: " + VcnGenerated.PolicyNo, new XFont("Arial", 13, XFontStyle.Bold), XBrushes.Black, new XRect(350, 105, 30, 40), XStringFormats.TopLeft);
                        //gfx.DrawString(VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(480, 135, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 130, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Make of Vehicle", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(80, 128, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.vehmodeltype, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 128, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Index Mark and Registration", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(80, 138, 30, 40), XStringFormats.TopLeft);
                        if (VcnGenerated.vehregno != null)
                            gfx.DrawString("Registration # " + VcnGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 138, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Number of Vehicle Insured", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(80, 150, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Chassis # " + VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 150, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Type of Body: " + VcnGenerated.bodyType, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 160, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("2.         Name of Policyholder", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 182, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 182, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3.         Effective date of the commencement of", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 210, 230, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Insurance for purposes of the Law", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(80, 222, 230, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 222, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.effectiveTime != "" ? Convert.ToDateTime(VcnGenerated.effectiveTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(370, 222, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4.         Date of Expiry of Insurance", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 240, 230, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 240, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.expiryTime != "" ? "11:59 PM" : "11:59 PM" , new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(370, 240, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("5.        Persons or classes of persons entitled to drive", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(50, 270, 230, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.authorizedwording != null)
                        {
                            string AuthDriving = "";
                            var W = VcnGenerated.authorizedwording.Split(',');
                            foreach (string i in W) { if (i.Length > 2) { AuthDriving += (i.Trim() + Environment.NewLine); } }
                            
                            tf.DrawString(AuthDriving.Replace("\"",""), new XFont("Times New Roman", 10), XBrushes.Black, new XRect(80, 280, 420, 120), XStringFormats.TopLeft);
                        }

                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(80, 380, 430, 40), XStringFormats.TopLeft);

                        gfx.DrawString("6.         Limitations as to use", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(50, 420, 230, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.limitsofuse != null)
                        {
                            string LimUse = "";
                            var W = VcnGenerated.limitsofuse.Split(',');
                            foreach (string i in W) { if (i.Length > 2) { LimUse += (i.Trim() + Environment.NewLine); } }
                            tf.DrawString(LimUse.Replace("\"", ""), new XFont("Times New Roman", 10), XBrushes.Black, new XRect(80, 435, 470, 200), XStringFormats.TopLeft);
                        }
                        tf.DrawString("WE HEREBY CERTIFY that the Policy to which this Certificate relates is issued in accordance with the provisions of the above-mentioned Act.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(50, 590, 470, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Issued on this " + AddOrdinal(System.DateTime.Now.Day) + " day of " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(50, 620, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("THE INSURANCE COMPANY OF THE WEST INDIES LIMITED", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(240, 670, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawLine(pen, 240, 720, 500, 720);
                        gfx.DrawString("Authorised Signature", new XFont("Times New Roman", 10, XFontStyle.Italic), XBrushes.Black, new XRect(350, 720, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("dd MMMM, yyyy") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Arial", 10), XBrushes.Black, new XRect(55, 770, 95, 40), XStringFormats.TopLeft);


                        //////////////////////  Adding an image  ////////////////////

                        if (VcnGenerated.LogoLocation != "" && VcnGenerated.LogoLocation != null)
                        {
                            string Paths = "";
                            Paths = VcnGenerated.LogoLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                            PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);
                            gfx.DrawImage(ximg, 50, 660, 70, 70);
                        }

                        ////////////////////////////////////////////////////////////

                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcnGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {

                            }
                           
                        }
                    }
                    else
                    {
                        // Draw the text 
                        gfx.DrawString("Jamaica", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(270, 30, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(100, 55, 200, 40), XStringFormats.TopLeft);

                        gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Times New Roman", 16), XBrushes.Black, new XRect(180, 75, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString(VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(470, 80, 20, 40), XStringFormats.TopLeft);

                        //Rectangular box surrounding certificate information
                        gfx.DrawRectangle(pen1, new XRect(35, 100, 520, 485));

                        gfx.DrawString("Certificate No.            " + VcnGenerated.certitificateNo, new XFont("Arial", 13, XFontStyle.Bold), XBrushes.Black, new XRect(50, 105, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Policy No: " + VcnGenerated.PolicyNo, new XFont("Arial", 13, XFontStyle.Bold), XBrushes.Black, new XRect(350, 105, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.certificateType + ":" + VcnGenerated.certificateCode, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(480, 135, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 130, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Make of Vehicle", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(80, 128, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.vehyear + " " + VcnGenerated.vehmake + " " + VcnGenerated.vehmodel + " " + VcnGenerated.vehmodeltype, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 128, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Number of Vehicle Insured", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(80, 150, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Chassis # " + VcnGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 150, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Type of Body: " + VcnGenerated.vehmodel, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 160, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("2.         Name of Policyholder", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 182, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.PolicyHolders, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 182, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3.         Effective date of the commencement of", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 210, 230, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Insurance for purposes of the Law", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(80, 222, 230, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.effectivedate) ? DateTime.Parse(VcnGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 222, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.effectiveTime != "" ? Convert.ToDateTime(VcnGenerated.effectiveTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(370, 222, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4.         Date of Expiry of Insurance", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(50, 240, 230, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcnGenerated.expirydate) ? DateTime.Parse(VcnGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(250, 240, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcnGenerated.expiryTime != "" ? Convert.ToDateTime(VcnGenerated.effectiveTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(370, 240, 30, 40), XStringFormats.TopLeft);

                        gfx.DrawString("5.        Persons or classes of persons entitled to drive", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(50, 270, 230, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.authorizedwording != null)
                            tf.DrawString(VcnGenerated.authorizedwording, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(80, 280, 200, 40), XStringFormats.TopLeft);

                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 9), XBrushes.Black, new XRect(80, 380, 430, 40), XStringFormats.TopLeft);

                        gfx.DrawString("6.         Limitations as to use", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(50, 420, 230, 40), XStringFormats.TopLeft);

                        if (VcnGenerated.limitsofuse != null)
                            tf.DrawString(VcnGenerated.limitsofuse, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(80, 435, 470, 200), XStringFormats.TopLeft);

                        tf.DrawString("WE HEREBY CERTIFY that the Policy to which this Certificate relates is issued in accordance with the provisions of the above-mentioned Act.", new XFont("Times New Roman", 9, XFontStyle.Bold), XBrushes.Black, new XRect(50, 590, 470, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Issued on this " + AddOrdinal(System.DateTime.Now.Day) + " day of " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(50, 620, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("THE INSURANCE COMPANY OF THE WEST INDIES LIMITED", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(240, 670, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawLine(pen, 240, 720, 500, 720);
                        gfx.DrawString("Authorised Signature", new XFont("Times New Roman", 10, XFontStyle.Italic), XBrushes.Black, new XRect(350, 720, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("dd MMMM, yyyy") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Arial", 10), XBrushes.Black, new XRect(55, 770, 95, 40), XStringFormats.TopLeft);


                        //////////////////////  Adding an image  ////////////////////

                        if (VcnGenerated.LogoLocation != "" && VcnGenerated.LogoLocation != null)
                        {
                            try
                            {
                                string Paths = "";
                                Paths = VcnGenerated.LogoLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                                PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);
                                gfx.DrawImage(ximg, 50, 660, 70, 70);
                            }
                            catch (Exception)
                            {

                            }
                           
                        }

                        ////////////////////////////////////////////////////////////

                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcnGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);

                            }
                            catch (Exception)
                            {

                            }
                           
                        }
                    }

                    
                }

                ///////////////////////////////////////////////////////////////////

                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }

        }

        /// <summary>
        /// This function is used to create the pdf for JIIC's certificates (for printing)
        /// </summary>
        private static PdfDocument JIICprint(List<VehicleCoverNoteGenerated> generatedCerts)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();


                pdf.Info.Title = "Print of Certificate";

                foreach (VehicleCoverNoteGenerated VcGenerated in generatedCerts)
                {
                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));

                    // Create a font
                    XFont fontBold = new XFont("Times New Roman", 14, XFontStyle.Bold);

                    //If no registration number is provided then generate a certificate that has no mention of registration number

                    if (VcGenerated.vehregno != null || VcGenerated.vehregno != "")
                    {
                        // Draw the text 
                        gfx.DrawString("Jamaica", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(270, 50, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Times New Roman", 14, XFontStyle.Bold), XBrushes.Black, new XRect(185, 70, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW, CAP 257", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(90, 92, 200, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Certificate No.  " + VcGenerated.certitificateNo, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(35, 135, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Policy No: " + VcGenerated.PolicyNo, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(220, 135, 30, 40), XStringFormats.TopLeft);
                        //gfx.DrawString(VcGenerated.PolicyTypes.Trim() == "Individual" ? VcGenerated.certificateType + " :indv:" : VcGenerated.certificateType, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(510, 135, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.certificateType + ":" + VcGenerated.certificateCode, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(455, 130, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1.        Make of Vehicle", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 155, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.vehyear + " " + VcGenerated.vehmake + " " + VcGenerated.vehmodel + " " + VcGenerated.vehmodeltype, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 155, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1a.      Index Mark and Registration", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 173, 20, 40), XStringFormats.TopLeft);
                        if (VcGenerated.vehregno != null)
                            gfx.DrawString(VcGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 177, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Number of Vehicle Insured", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 185, 122, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1b.      Chassis Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 203, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 203, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1c.      Engine Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 222, 20, 40), XStringFormats.TopLeft);
                        if (VcGenerated.engineNo != null)
                            gfx.DrawString(VcGenerated.engineNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 222, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("2.        Name of Policyholder", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 240, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.PolicyHolders, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 240, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3.        Effective date of the commencement of Insurance for purposes of", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 265, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString("the Law", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 278, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcGenerated.effectivedate) ? DateTime.Parse(VcGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 278, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.effectiveTime != "" ? Convert.ToDateTime(VcGenerated.effectiveTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 278, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4.        Date of Expiry of Insurance", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 295, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcGenerated.expirydate) ? DateTime.Parse(VcGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 295, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.expiryTime != "" ? Convert.ToDateTime(VcGenerated.expiryTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 298, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("5.        Persons or classes of persons entitled to drive", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 315, 20, 40), XStringFormats.TopLeft);

                        if (VcGenerated.authorizedwording != null)
                            tf.DrawString(VcGenerated.authorizedwording, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 328, 400, 40), XStringFormats.TopLeft);

                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive" + " the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any" + " enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 410, 480, 40), XStringFormats.TopLeft);

                        gfx.DrawString("6.        Limitations as to use", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 460, 20, 40), XStringFormats.TopLeft);

                        if (VcGenerated.limitsofuse != null)
                            tf.DrawString(VcGenerated.limitsofuse.Replace("\n", "").ToUpper(), new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 480, 450, 100), XStringFormats.TopLeft);

                        gfx.DrawString("I/WE HEREBY CERTIFY", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(66, 590, 20, 40), XStringFormats.TopLeft);
                        tf.DrawString("that the Policy to which this Certificate relates is issued in " + "accordance with the provisions of the above-mentioned Law.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(230, 590, 260, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Issued on         " + AddOrdinal(System.DateTime.Now.Day) + " day of " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 617, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("GK GENERAL INSURANCE COMPANY", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(240, 660, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawLine(pen, 290, 710, 500, 710);
                        gfx.DrawString("For the Company", new XFont("Times New Roman", 10, XFontStyle.Italic), XBrushes.Black, new XRect(350, 710, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(55, 780, 95, 40), XStringFormats.TopLeft);


                        //////////////////////  Adding an image  ////////////////////

                        if (VcGenerated.LogoLocation != "" && VcGenerated.LogoLocation != null)
                        {
                            try
                            {
                                string Paths = "";
                                Paths = VcGenerated.LogoLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                                PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);
                                gfx.DrawImage(ximg, 36, 640, 170, 80);
                            }
                            catch (Exception)
                            {

                            }
                           
                        }

                        ////////////////////////////////////////////////////////////


                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {

                            }
                           
                        }
                    }
                    else
                    {
                        // Draw the text 
                        gfx.DrawString("Jamaica", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(270, 50, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Times New Roman", 14, XFontStyle.Bold), XBrushes.Black, new XRect(185, 70, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW, CAP 257", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(90, 92, 200, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Certificate No.  " + VcGenerated.certitificateNo, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(35, 135, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Policy No: " + VcGenerated.PolicyNo, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(220, 135, 30, 40), XStringFormats.TopLeft);
                        //gfx.DrawString(VcGenerated.PolicyTypes.Trim() == "Individual" ? VcGenerated.certificateType + " :indv:" : VcGenerated.certificateType, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(510, 135, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.certificateType + ":" + VcGenerated.certificateCode, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(455, 130, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1.        Make of Vehicle", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 155, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.vehyear + " " + VcGenerated.vehmake + " " + VcGenerated.vehmodel + " " + VcGenerated.vehmodeltype, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 155, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Number of Vehicle Insured", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 185, 122, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1a.      Chassis Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 203, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 203, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1b.      Engine Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 222, 20, 40), XStringFormats.TopLeft);
                        if (VcGenerated.engineNo != null)
                            gfx.DrawString(VcGenerated.engineNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 222, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("2.        Name of Policyholder", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 240, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.PolicyHolders, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 240, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3.        Effective date of the commencement of Insurance for purposes of", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 265, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString("the Law", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 278, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcGenerated.effectivedate) ? DateTime.Parse(VcGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 278, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.effectiveTime != "" ? Convert.ToDateTime(VcGenerated.effectiveTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 278, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4.        Date of Expiry of Insurance", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 295, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcGenerated.expirydate) ? DateTime.Parse(VcGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 295, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.expiryTime != "" ? Convert.ToDateTime(VcGenerated.expiryTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 298, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("5.        Persons or classes of persons entitled to drive", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 315, 20, 40), XStringFormats.TopLeft);

                        if (VcGenerated.authorizedwording != null)
                            tf.DrawString(VcGenerated.authorizedwording, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 328, 400, 40), XStringFormats.TopLeft);

                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive" + " the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any" + " enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 410, 480, 40), XStringFormats.TopLeft);

                        gfx.DrawString("6.        Limitations as to use", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 460, 20, 40), XStringFormats.TopLeft);

                        if (VcGenerated.limitsofuse != null)
                            tf.DrawString(VcGenerated.limitsofuse.Replace("\n", "").ToUpper(), new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 480, 450, 100), XStringFormats.TopLeft);

                        gfx.DrawString("I/WE HEREBY CERTIFY", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(66, 590, 20, 40), XStringFormats.TopLeft);
                        tf.DrawString("that the Policy to which this Certificate relates is issued in " + "accordance with the provisions of the above-mentioned Law.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(230, 590, 260, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Issued on         " + AddOrdinal(System.DateTime.Now.Day) + " day of " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 617, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("JAMAICA INTERNATIONAL INSURANCE COMPANY LIMITED", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(240, 660, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawLine(pen, 290, 710, 500, 710);
                        gfx.DrawString("For the Company", new XFont("Times New Roman", 10, XFontStyle.Italic), XBrushes.Black, new XRect(350, 710, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(55, 780, 95, 40), XStringFormats.TopLeft);


                        //////////////////////  Adding an image  ////////////////////

                        if (VcGenerated.LogoLocation != "" && VcGenerated.LogoLocation != null)
                        {
                            try
                            {
                                string Paths = "";
                                Paths = VcGenerated.LogoLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                                PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);
                                gfx.DrawImage(ximg, 36, 640, 170, 80);
                            }
                            catch (Exception)
                            {

                            }
                           
                        }

                        ////////////////////////////////////////////////////////////


                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {

                            }
                            
                        }
                    }
                }
                ///////////////////////////////////////////////////////////////////

                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }


        }


        /// <summary>
        /// Prints a certificate for IRONROCK
        /// </summary>
        /// <param name="generatedCerts"></param>
        /// <returns></returns>
        private static PdfDocument IRJprint(List<VehicleCoverNoteGenerated> generatedCerts)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();


                pdf.Info.Title = "Print of Certificate";

                foreach (VehicleCoverNoteGenerated VcGenerated in generatedCerts)
                {
                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));

                    // Create a font
                    XFont fontBold = new XFont("Times New Roman", 14, XFontStyle.Bold);

                    //If no registration number is provided then generate a certificate that has no mention of registration number

                    if (VcGenerated.vehregno != null || VcGenerated.vehregno != "")
                    {
                        //////////////////////  Adding an image  ////////////////////

                        if (VcGenerated.LogoLocation != "" && VcGenerated.LogoLocation != null)
                        {
                            try
                            {
                                string Paths = "";
                                Paths = VcGenerated.LogoLocation.Replace(System.Web.HttpContext.Current.Server.MapPath("~"), "");

                                PdfSharp.Drawing.XImage ximg = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("~") + Paths);
                                gfx.DrawImage(ximg, 35, 5, 170, 70);
                            }
                            catch (Exception)
                            {

                            }

                        }

                        // Draw the text 
                        gfx.DrawString("Jamaica", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(270, 60, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Times New Roman", 14, XFontStyle.Bold), XBrushes.Black, new XRect(185, 80, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("THE MOTOR VEHICLE INSURANCE (THIRD PARTY RISKS) ACT", new XFont("Times New Roman", 11, XFontStyle.Bold), XBrushes.Black, new XRect(122, 102, 200, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Certificate No.  " + VcGenerated.certitificateNo, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(35, 140, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Policy No: " + VcGenerated.PolicyNo, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(220, 135, 30, 40), XStringFormats.TopLeft);
                        //gfx.DrawString(VcGenerated.PolicyTypes.Trim() == "Individual" ? VcGenerated.certificateType + " :indv:" : VcGenerated.certificateType, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(510, 135, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.certificateType + ":" + VcGenerated.certificateCode, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(455, 135, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1.        Make of Vehicle", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 155, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.vehyear + " " + VcGenerated.vehmake + " " + VcGenerated.vehmodel + " " + VcGenerated.vehmodeltype, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 155, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1a.      Index Mark and Registration", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 173, 20, 40), XStringFormats.TopLeft);
                        if (VcGenerated.vehregno != null)
                            gfx.DrawString(VcGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 177, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Number of Vehicle Insured", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 185, 122, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1b.      Chassis Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 203, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 203, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1c.      Engine Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 222, 20, 40), XStringFormats.TopLeft);
                        if (VcGenerated.engineNo != null)
                            gfx.DrawString(VcGenerated.engineNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 222, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("2.        Name of Policyholder", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 240, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.PolicyHolders, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 240, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3.        Effective date of the commencement of Insurance for purposes of", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 265, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString("the Law", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 278, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcGenerated.effectivedate) ? DateTime.Parse(VcGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 278, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.effectiveTime != "" ? Convert.ToDateTime(VcGenerated.effectiveTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 278, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4.        Date of Expiry of Insurance", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 295, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcGenerated.expirydate) ? DateTime.Parse(VcGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 295, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.expiryTime != "" ? Convert.ToDateTime(VcGenerated.expiryTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 298, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("5.        Persons or classes of persons entitled to drive", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 315, 20, 40), XStringFormats.TopLeft);

                        if (VcGenerated.authorizedwording != null)
                            tf.DrawString(VcGenerated.authorizedwording, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 328, 400, 40), XStringFormats.TopLeft);

                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive" + " the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any" + " enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 410, 480, 40), XStringFormats.TopLeft);

                        gfx.DrawString("6.        Limitations as to use", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 460, 20, 40), XStringFormats.TopLeft);

                        if (VcGenerated.limitsofuse != null)
                            tf.DrawString(VcGenerated.limitsofuse, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 480, 450, 100), XStringFormats.TopLeft);

                        gfx.DrawString("*(Limitations rendered inoperative by section 6 (2) of the Law are not to be included under this heading).", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 580, 450, 40), XStringFormats.TopLeft);
                        gfx.DrawString("I/WE HEREBY CERTIFY", new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(66, 610, 20, 40), XStringFormats.TopLeft);
                        tf.DrawString("that the Policy to which this Certificate relates is issued in accordance with the", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(212, 610, 400, 40), XStringFormats.TopLeft);
                        tf.DrawString("provisions of the above-mentioned Law", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(66, 625, 400, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Issued on " + AddOrdinal(System.DateTime.Now.Day)  + " day of " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(66, 640, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("IRONROCK INSURANCE COMPANY LIMITED", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(200, 670, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawLine(pen, 200, 730, 410, 730);
                        gfx.DrawString("For the Company", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(265, 730, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(55, 780, 95, 40), XStringFormats.TopLeft);


                        ////////////////////////////////////////////////////////////


                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {

                            }

                        }
                    }
                }
                ///////////////////////////////////////////////////////////////////

                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }


        }


        /// <summary>
        /// This function is used to create the pdf for AGI's certificates (for printing)
        /// </summary>
        private static PdfDocument AGIprint(List<VehicleCoverNoteGenerated> generatedCerts)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                Document doc = new Document();

                pdf.Info.Title = "Print of Certificate";

                foreach (VehicleCoverNoteGenerated VcGenerated in generatedCerts)
                {
                    PdfPage page = pdf.AddPage();
                    page.Size = PageSize.Letter;

                    // Get an XGraphics object for drawing
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XTextFormatter tf = new XTextFormatter(gfx);
                    XPen pen = new XPen(XColor.FromName("Black"));

                    // Create a font
                    XFont fontBold = new XFont("Times New Roman", 14, XFontStyle.Bold);


                    //if no Registration Number is provided the generate a certificate that has no mention of Registration Number

                    if (VcGenerated.vehregno != null || VcGenerated.vehregno != "")
                    {
                        // Draw the text 
                        gfx.DrawString("Jamaica", new XFont("Arial", 12, XFontStyle.Bold), XBrushes.Black, new XRect(270, 55, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(185, 75, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW, CAP 257", new XFont("Arial", 11), XBrushes.Black, new XRect(90, 97, 200, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Certificate No.  " + VcGenerated.certitificateNo, new XFont("Times New Roman", 13, XFontStyle.Bold), XBrushes.Black, new XRect(35, 135, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Policy No: " + VcGenerated.PolicyNo, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(220, 135, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.certificateType + ":" + VcGenerated.certificateCode, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(510, 135, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1.        Make of Vehicle", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 160, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.vehyear + " " + VcGenerated.vehmake + " " + VcGenerated.vehmodel + " " + VcGenerated.vehmodeltype, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 160, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1a.      Index Mark and Registration", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 173, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.vehregno.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 177, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Number of Vehicle Insured", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 184, 122, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1b.      Chassis Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 203, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 203, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("2.        Name of Policyholder", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 222, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.PolicyHolders, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 222, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3.        Effective date of the commencement of Insurance", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 240, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString("for purposes of the Law", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 253, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcGenerated.effectivedate) ? DateTime.Parse(VcGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 253, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.effectiveTime != "" ? Convert.ToDateTime(VcGenerated.effectiveTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 253, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4.        Date of Expiry of Insurance", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 278, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcGenerated.expirydate) ? DateTime.Parse(VcGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 278, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.expiryTime != "" ? Convert.ToDateTime(VcGenerated.expiryTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 278, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("5.        Persons or classes of persons entitled to drive", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 298, 20, 40), XStringFormats.TopLeft);

                        if (VcGenerated.authorizedwording != null)
                            tf.DrawString(VcGenerated.authorizedwording, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 315, 400, 40), XStringFormats.TopLeft);

                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive" + " the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any" + " enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 395, 480, 40), XStringFormats.TopLeft);

                        gfx.DrawString("6.        Limitations as to use", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 435, 20, 40), XStringFormats.TopLeft);

                        if (VcGenerated.limitsofuse != null)
                            tf.DrawString(VcGenerated.limitsofuse, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 450, 520, 40), XStringFormats.TopLeft);

                        tf.DrawString("I/WE HEREBY CERTIFY that the Policy to which this Certificate relates is issued in accordance with the provisions of the above-mentioned Law.", new XFont("Arial", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 593, 500, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Issued on         " + AddOrdinal(System.DateTime.Now.Day) + " day of " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 623, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("ADVANTAGE GENERAL INSURANCE COMPANY LIMITED", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(240, 640, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawLine(pen, 280, 690, 500, 690);
                        gfx.DrawString("For the Company", new XFont("Times New Roman", 10, XFontStyle.Italic), XBrushes.Black, new XRect(350, 690, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(55, 780, 95, 40), XStringFormats.TopLeft);


                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {

                            }
                           
                        }

                        ///////////////////////////////////////////////////////////////////
                    }
                    else
                    {
                        // Draw the text 
                        gfx.DrawString("Jamaica", new XFont("Arial", 12, XFontStyle.Bold), XBrushes.Black, new XRect(270, 55, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString("CERTIFICATE OF INSURANCE", new XFont("Arial", 14, XFontStyle.Bold), XBrushes.Black, new XRect(185, 75, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("THE MOTOR VEHICLES INSURANCE (THIRD PARTY RISKS) LAW, CAP 257", new XFont("Arial", 11), XBrushes.Black, new XRect(90, 97, 200, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Certificate No.  " + VcGenerated.certitificateNo, new XFont("Times New Roman", 13, XFontStyle.Bold), XBrushes.Black, new XRect(35, 135, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString("Policy No: " + VcGenerated.PolicyNo, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(220, 135, 30, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.certificateType + ":" + VcGenerated.certificateCode, new XFont("Times New Roman", 12, XFontStyle.Bold), XBrushes.Black, new XRect(510, 135, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1.        Make of Vehicle", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 160, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.vehyear + " " + VcGenerated.vehmake + " " + VcGenerated.vehmodel + " " + VcGenerated.vehmodeltype, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 160, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Number of Vehicle Insured", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 184, 122, 40), XStringFormats.TopLeft);

                        gfx.DrawString("1a.      Chassis Number", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 203, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.chassisNo.ToUpper(), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 203, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("2.        Name of Policyholder", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 222, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.PolicyHolders, new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 222, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("3.        Effective date of the commencement of Insurance", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 240, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString("for purposes of the Law", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 253, 200, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcGenerated.effectivedate) ? DateTime.Parse(VcGenerated.effectivedate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 253, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.effectiveTime != "" ? Convert.ToDateTime(VcGenerated.effectiveTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 253, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("4.        Date of Expiry of Insurance", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(35, 278, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(!string.IsNullOrEmpty(VcGenerated.expirydate) ? DateTime.Parse(VcGenerated.expirydate).ToString("MMMM dd, yyyy") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(220, 278, 20, 40), XStringFormats.TopLeft);
                        gfx.DrawString(VcGenerated.expiryTime != "" ? Convert.ToDateTime(VcGenerated.expiryTime).ToString("hh:mm ttt") : "", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(350, 278, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("5.        Persons or classes of persons entitled to drive", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 298, 20, 40), XStringFormats.TopLeft);

                        if (VcGenerated.authorizedwording != null)
                            tf.DrawString(VcGenerated.authorizedwording, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 315, 400, 40), XStringFormats.TopLeft);

                        tf.DrawString("Provided that the person driving is permitted in accordance with the licensing or other laws or regulations to drive" + " the Motor Vehicle or has been so permitted and is not disqualified by order of a Court of Law or by reason of any" + " enactment or regulation in that behalf from driving the Motor Vehicle.", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 395, 480, 40), XStringFormats.TopLeft);

                        gfx.DrawString("6.        Limitations as to use", new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 435, 20, 40), XStringFormats.TopLeft);

                        if (VcGenerated.limitsofuse != null)
                            tf.DrawString(VcGenerated.limitsofuse, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(63, 450, 520, 40), XStringFormats.TopLeft);

                        tf.DrawString("I/WE HEREBY CERTIFY that the Policy to which this Certificate relates is issued in accordance with the provisions of the above-mentioned Law.", new XFont("Arial", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 593, 500, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Issued on         " + AddOrdinal(System.DateTime.Now.Day) + " day of " + System.DateTime.Now.ToString("MMMM yyyy"), new XFont("Times New Roman", 10, XFontStyle.Bold), XBrushes.Black, new XRect(35, 623, 20, 40), XStringFormats.TopLeft);

                        gfx.DrawString("ADVANTAGE GENERAL INSURANCE COMPANY LIMITED", new XFont("Times New Roman", 10), XBrushes.Black, new XRect(240, 640, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawLine(pen, 280, 690, 500, 690);
                        gfx.DrawString("For the Company", new XFont("Times New Roman", 10, XFontStyle.Italic), XBrushes.Black, new XRect(350, 690, 95, 40), XStringFormats.TopLeft);

                        gfx.DrawString("Printed On: " + System.DateTime.Now.ToString("MMMM dd, yyyy") + " @ " + System.DateTime.Now.ToString("HH:mm tt") + " By: " + Constants.user.employee.person.fname + " " + Constants.user.employee.person.lname, new XFont("Times New Roman", 10), XBrushes.Black, new XRect(55, 780, 95, 40), XStringFormats.TopLeft);


                        /////////////////////////////// WaterMark ///////////////////////////

                        if (VcGenerated.cancelled)
                        {
                            try
                            {
                                XImage cancelled_image = PdfSharp.Drawing.XImage.FromFile(System.Web.HttpContext.Current.Server.MapPath("/Content/more_images/cancelled-watermark.png"));
                                gfx.DrawImage(cancelled_image, 0, 0, page.Width, page.Height);
                            }
                            catch (Exception)
                            {

                            }
                           
                        }

                        ///////////////////////////////////////////////////////////////////
                    }

                }

                return pdf;
            }
            catch (Exception)
            {
                return new PdfDocument();
            }
        }


        /// <summary>
        /// This function is used to pull the ordinal for the date display in each certificate    
        /// </summary>
        public static string AddOrdinal(int num)
        {
            if (num <= 0) return num.ToString();

            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num + "th";
            }

            switch (num % 10)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }
        }


    }
}
