﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Threading;

namespace Honeysuckle.Models
{
    public class ApplicationConfiguration
    {
        public int ID { get; set; }
        public ConfigurationType Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public bool ToggleValue { get; set; }
        public string LiteralValue { get; set; }
        public bool Active { get; set; }

        sql sql;

        public ApplicationConfiguration()
        {
        }

        public bool IsEnabled(string applicationCode)
        {
            bool isEnabled = false;
            if (string.IsNullOrEmpty(applicationCode))
            {
                throw new ArgumentException("ApplicationCode was not specified");
            }

            sql = new sql();

            if (!sql.ConnectSQL()){
                throw new ApplicationException("Unable to Open Database Connection");
            }

            string query = string.Format("EXEC GetApplicationConfigurationByID '{0}'", applicationCode);

            try
            {
                SqlDataReader reader = sql.QuerySQL(query);

                if(reader != null){
                    while (reader.Read())
                    {
                        isEnabled = Boolean.Parse(reader["ToggleValue"].ToString());

                    }

                    reader.Close();
                    sql.DisconnectSQL();
                }
                
                
            }
            catch (SqlException sqlException)
            {
                Log.LogRecord("SQL Exception", sqlException.Procedure + sqlException.LineNumber + sqlException.Message);
                //throw;
            }

            return isEnabled;
        }          
    }  
}