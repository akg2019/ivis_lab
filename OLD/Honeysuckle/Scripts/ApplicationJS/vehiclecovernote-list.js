﻿var showing = 0;
$(function () {
    $("#covernotes").children(".history").hide();
    $("#covernotes .current").on("click",function () {
        if (showing == 0) {
            $("#covernotes").children(".history").show(400);
            showing = 1; 
        }
        else {
            $("#covernotes").children(".history").hide(400);
            showing = 0; 
        }
    });
});   //end of MAIN Function

$(function () {
    $("#btn_EditVehicle").click(function () {
        window.location.replace("/Vehicle/Edit/" + $(".vehdetails").val());
    });
});   //end of MAIN Function