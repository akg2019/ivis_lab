﻿var url = "";

$(function () {

    url = window.location.href;

    $("#auditlink").on('click', function () {
        if (getAuditPage() != "") {
            $($("#log-data-modal").load('/Audit/AuditLog/' + getID() + '?location=' + getAuditPage(), function (value) {
                $(".log").perfectScrollbar({
                    wheelSpeed: 0.3,
                    wheelPropagation: true,
                    minScrollbarLength: 10,
                    suppressScrollX: true
                });
            })).dialog({
                title: capitalizeFirstLetter(getAuditPage()) + ' Audit Log',
                modal: true,
                width: 450,
                height: 400,
                buttons: {
                    Close: function () {
                        $(this).dialog('close');
                        $("#log-data-modal").html("");
                    }
                }
            });
        }
    });

    $("body").on("click", ".audit", function () {
        var id = $(this).find(".id").val();
        $($("#log-data-modal-diff").load('/Audit/Audit/' + id, function (value) {
            $(".log-partial").perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });
        })).dialog({
            title: capitalizeFirstLetter(getAuditPage()) + ' Audit Diff',
            modal: true,
            width: 600,
            height: 500,
            buttons: {
                Close: function () {
                    $(this).dialog('close');
                    $("#log-data-modal-diff").html("");
                }
            }
        });
    });

});

function getAuditPage() {
    var this_url = url.replace(/^.*\/\/[^\/]+/, '');
    this_url = this_url.substr(this_url.indexOf('/') + 1, this_url.length - 1 - this_url.indexOf('/')).substr(0, this_url.substr(this_url.indexOf('/') + 1, this_url.length - 1 - this_url.indexOf('/')).indexOf('/'));
    switch (this_url.toLowerCase()) {
        case "vehicle":
            this_url = "vehicles";
            break;
        case "user":
            this_url = "user"
            break;
        case "vehiclecertificate":
            this_url = "vehiclecertificates";
            break;
        case "policy":
            this_url = "policies";
            break;
        case "people":
        case "company":
        case "vehiclecovernote":
            break;
        default:
            this_url = "";
            break;
    }
    return this_url;
}

function getID() {
    var this_url = url.replace(/^.*\/\/[^\/]+/, '');
    this_url = this_url.substr(this_url.indexOf('/') + 1, this_url.length - 1 - this_url.indexOf('/'));
    this_url = this_url.substr(this_url.indexOf('/') + 1, this_url.length - 1 - this_url.indexOf('/'));
    this_url = this_url.substr(this_url.indexOf('/') + 1, this_url.length - 1 - this_url.indexOf('/'));
    if (this_url.indexOf('?') > 0)  this_url = this_url.substr(0, this_url.indexOf('?'));
    this_url = this_url.toLowerCase();
    return this_url;
}

function capitalizeFirstLetter(string){
    return string.substr(0,1).toUpperCase() + string.substr(1,string.length - 1);
}