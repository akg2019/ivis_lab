﻿

var trnFocus = false;
var trnSet = false;
var selectedId = "";

$(function () {
    if (typeof $("#trn").val() != 'undefined')
    if( $("#trn").val().trim().length > 0 ) {
        trnSet = true;
    }
});


$(function () {
    $("#trn").focus(function () {
        trnFocus = true;
    })
});

$(function () {
    $("#trn").blur(function () {
        if (trnFocus) {
            var test = $("#trn").val();
            var result = false;
            if(test.trim())
                $("body").append('<div class="ui-widget-overlay ui-front temp"><div class="tempmessage"><img src="../../Content/more_images/loading.gif" /><span>Processing Data</span></div></div>');
            $.getJSON("/Vehicle/GetUserByTRN/" + test.trim(), function (data) {
                if (test.trim())
                    $(".ui-front.temp").remove();
                if (!data.error) {
                    if (data.result) {
                        //something exists
                        if (data.person) {

                            var buttons = {
                                Ok: function () {

                                    if ($('.dupTRNcheck:checked').length <= 0 && $('#duplicatePartial').length > 0) {
                                        alert('Please select a record or cancel');
                                        return false;
                                    }

                                    if ($('#duplicatePartial').length > 0) {

                                        $.ajax({
                                            type: "POST",
                                            traditional: true,
                                            url: "/People/GetFullPersonDetails/?personId=" + selectedId,
                                            async: false,
                                            dataType: "json",
                                            success: function (rdata) {
                                                data = rdata;
                                            },
                                            error: function () {
                                                alert('Error receiving data');
                                            }
                                        });
                                    }
                                    $(this).dialog("close");
                                    $(this).html("");
                                    $(this).dialog("destroy");
                                    $("#InformationDialog").html("");

                                    // Adding the selected driver to the drivers list
                                    AddPersonSelected(data.baseid, 1);

                                    $(".ui-dialog-content").each(function () {

                                        if ($(this).attr('id') == "createDriver") {
                                            $(this).dialog("close");
                                            $(this).html("");
                                            $(this).dialog("destroy");
                                            $("#InformationDialog").html("");
                                        }

                                    });


                                    //$($(".ui-dialog-content")[1]).html("");
                                    // $($(".ui-dialog-content")[1]).dialog("destroy"); //Removing the driver create modal

                                },

                                Edit: function () {

                                    if ($('.dupTRNcheck:checked').length <= 0 && $('#duplicatePartial').length > 0) {
                                        alert('Please select a record or cancel');
                                        return false;
                                    }

                                    if ($('#duplicatePartial').length > 0) {

                                        $.ajax({
                                            type: "POST",
                                            traditional: true,
                                            url: "/People/GetFullPersonDetails/?personId=" + selectedId,
                                            async: false,
                                            dataType: "json",
                                            success: function (rdata) {
                                                data = rdata;
                                            },
                                            error: function () {
                                                alert('Error receiving data');
                                            }
                                        });
                                    }

                                    $('#fname').val(data.fname);
                                    $('#mname').val(data.mname);
                                    $('#lname').val(data.lname);

                                    $('#RoadNumber').val(data.roadno);
                                    $('#roadname').val(data.roadname);
                                    $('#roadtype').val(data.roadtype);
                                    $('#aptNumber').val(data.aptNumber);

                                    $('#city').val(data.city);
                                    $('.parishes#parishes').val(data.parishid);
                                    $('.parish#parish').val(data.parishid);
                                    $('#country').val(data.country);

                                    if (data.longAddressUsed) $('#longAddress').val(data.longAddress);

                                    $('input').trigger('change');
                                    $('.parishes#parishes').trigger('change');

                                    

                                    $(this).dialog("close");
                                    $("#InformationDialog").html('');
                                },
                                Cancel: function () {
                                    //$("#trn").val("");
                                    $(this).dialog("close");
                                    $(this).html("");
                                    $(this).dialog("destroy");
                                    $("#InformationDialog").html("");
                                }
                            };
//                            if (data.duplicate) buttons['View Duplicates'] = function () {
//                                $('#duplicates').load('/People/GetPersonDuplicates/?trn=' + test.trim());
//                                $('#duplicates').dialog({
//                                    modal: true,
//                                    width: 470,
//                                    height: 380,
//                                    title: 'Duplicates',
//                                    buttons: {
//                                        OK: function () {
//                                            $('#duplicates').html('');
//                                            $(this).dialog('close');
//                                        }
//                                    }
//                                });
//                                
//                            };
                            //There is a person
                            $("#InformationDialog").dialog({
                                modal: true,
                                width: 600,
                                autoOpen: false,
                                title: "Person Exists",
                                height: 390,
                                buttons: buttons
                            });

                            if (data.duplicate) {
                                $("#InformationDialog").load('/People/GetPersonDuplicates/?trn=' + test.trim(), function (value) { }).dialog('open');

                            }
                            else {
                                $("#InformationDialog").load('/People/FullDriverDetails/' + data.personid + "?create=" + true + "&duplicate=" + data.duplicate, function (value) { }).dialog('open');

                            }

                            

                        }
                    }

                    else {
                        //TRN FREE
                        if (test.trim().length != 0) trnSet = true;
                        $("#country").val("Jamaica");
                        $("#country").trigger('change');
                    }
                }
                else {
                    //ERROR
                    $("<div>" + data.message + "</div>").dialog({
                        modal: true,
                        Title: "Alert",
                        buttons: {
                            Ok: function () {
                                $("#trn").val("");
                                $(this).dialog("close");
                            }
                        }
                    });
                }

            });
            trnFocus = false;
        }
    })
});
function selectDuplicateVeh(e) {

    selectedId = $(e).prop('id');
    $('.dupTRNcheck:not(#' + selectedId + ')').prop('checked', false);
}

