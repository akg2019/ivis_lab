function noDeleteUser() {

        $("<div></div>").dialog({
            title: 'Alert!',
            modal:true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                }
            }
        });
};
