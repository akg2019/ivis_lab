﻿
$(function () {
    $("#addphone").click(function () {
        if ($(".phone-container").length > 0) {
            $.ajax({
                //  url: '/Company/Phone/',
                url: '/Phone/NewPhone',
                cache: false,
                success: function (html) {
                    // var header = "<div class=\"phone\"><div class=\"subheader\"><img src=\"../../Content/more_images/User.png\" alt=\"\"/><span>Phone Number</span><input type=\"button\" value=\"Remove\" class=\"remove btnclr rmphone\"/></div>";
                    //var header = "<div class=\"phone\"><div class=\"phoneheader\"><span><strong>Phone Number</strong></span><a href=\"#\" class=\"icon\" title=\"Remove Phone Number\"><img src=\"../../Content/more_images/delete_icon.png\" class=\"remove btnclr rmphone dont-process sprite\" /></a></div>";
                    var header = "<div class=\"phone dont-process\"><div class=\"subheader dont-process\"><span>Phone Number</span><input type=\"button\" value=\"Remove\" class=\"remove btnclr rmphone dont-process\"/></div>";
                    var end = "</div>";
                    html = header + html + end;
                    $(".phone-container").append(html);
                    $('.extNum').trigger('keyup')

                    var checked = false;
                    $(".phonePrimary").each(function () {

                        if ($(this).prop('checked'))
                            checked = true;
                    });

                    if (!checked)
                        $(".phonePrimary:first").prop('checked', true);
                }
            });
            return false;
        }
    });
});


$(function () {
    $(document).on("click", ".rmphone", function () {

        var deleteItem = $(this).parents(".phone");

        $("<div>You are about to delete this phone number. Are you sure you want to do this? </div>").dialog({
            modal: true,
            title: 'Alert!',
            buttons: {
                Ok: function () {
                    deleteItem.remove();
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");

                    var checked = false;
                    $(".phonePrimary").each(function () {

                        if ($(this).prop('checked'))
                            checked = true;
                    });

                    if (!checked)
                        $(".phonePrimary:first").prop('checked', true);
                },
                Cancel: function () {
                    $(this).dialog("close");
                    $(this).dialog().dialog("destroy");
                }
            }
        });

    });
});

var checkedPrimary = false;

$(document).ready(function () {
    $("body").on("change", ".phonePrimary", function (event) {

        if ($("#" + event.target.id).is(':checked')) 
            {
                checkedPrimary = true;
            }
        else 
            {
                checkedPrimary = false;
            }

      $(".phonePrimary").prop('checked', false);
      $("#" + event.target.id).prop('checked', checkedPrimary);
    });
});


$(document).on("click", "#addphone", function (e) {

    var Phones = $(e.target).parents(".phones").find('#phone-container');
    if (Phones.length > 0) {
        $.ajax({
            // url: '/Company/Phone/',
            url: '/Phone/NewPhone',
            cache: false,
            success: function (html) {

                var header = "<div class=\"phone\"><div class=\"subheader\"><img src=\"../../Content/more_images/User.png\" alt=\"\"/><span>Phone Info</span><input type=\"button\" value=\"Remove\" class=\"remove btnclr rmphone\"/></div>";
                var end = "</div>";
                html = header + html + end;

                Phones.append(html);
                $('.extNum').trigger('keyup');
            }
        });
        return false;
    }
});

$(function () {

    //if ($(".phone-container").length > 0 || $(".newphone").length > 0 || $("#phone-container").length > 0) {

        $('html').on('keyup', '.telNum', function (e) {

            if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
                $(this).val($(this).val().replace(/^[(]?(\d{3})[)]?/g, '($1)'));
                $(this).val($(this).val().replace(/[)](\d{3})\-?/g, ')$1-'));
                
                return true;
            }
            $(this).val($(this).val().replace(/[^\-()0-9]/g, ''));
        });


    //}

});