﻿$(function () {

    $(".coverdays").click(function () {

        UpdateCoverPeriod($(this));

        //Higlights the selected cover note quick option
        try {
            //Remove highlited class from all buttons because at this point we do not know which button is being clicked
            $("#quickperiod-coverdays-7").removeClass("SelectedPeriod");
            $("#quickperiod-coverdays-14").removeClass("SelectedPeriod");
            $("#quickperiod-coverdays-30").removeClass("SelectedPeriod");

            //Add highlighted class to the button that was clicked
            $(this).addClass("SelectedPeriod");
           
        } catch (e) {

        }


    });

});

function UpdateCoverPeriod(periodObj) {
    
    var period = periodObj.val();
    var DAY = 1000 * 60 * 60 * 24;
    var expiry = new Date(periodObj.parents('.vehicle-row').find('.expiryPicker').val());
    var effective = new Date(periodObj.parents('.vehicle-row').find('.effective').val());
    var expElem = periodObj.parents('.vehicle-row').find('.expiryPicker');

    if (period != "" && period != 'undefined') {
        if ((effective.getTime() + (parseInt(period) * DAY)) > get_expiry_date().getTime()) {

            var Message = '';
            var Dt = new Date();
            if (get_expiry_date().getTime() < Dt.getTime()) {
                Message = '<div><b style="color:red;">Epired Policy</b><br/><br/>The days effective period cannot go beyond the policy expiry date. </div>';
            } else {
                Message = '<div>The  days effective period cannot go beyond the policy expiry date. </div>';
            }

            $(Message).dialog({
                title: 'Alert!',
                modal: true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                        $(this).dialog().dialog("destroy");
                    }
                }
            });

        }
        else {
            expiry = new Date();
            expiry.setDate(effective.getDate() + parseInt(period));
            expiry.setHours(23); expiry.setMinutes(59);
            expElem.val(expiry.dateFormat('d M Y h:i A'));
            expElem.trigger("change");
        }
    }

}