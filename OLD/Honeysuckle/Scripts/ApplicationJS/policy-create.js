﻿//SETTING THE USAGE DROPDOWN TO THE CORRECT USAGE OF THE VEHICLE UNDER THE POLICY
$(document).ready(function () {

    //if (typeof $("#hiddenid").val() !== 'undefined') {
    if($("#hiddenid").val() != 0 && $("#hiddenid").val() != "") {
        //THIS FUNCTION ONLY HAPPENS ON POLICY EDIT
        $(".chassisNumber").each(function () {

            var usagemodel = $(this).parent().children("#vehUsageID");
            var polID = $("#hiddenid").val();
            var dropdownbox = $(this).parent().parent().parent().children(".this_vehicle_dropdown").children(".useDropDown").children(".usage_dropdown");
            var usageID = usagemodel.val();
            $("#compDropDown").prop('disabled', true);

            dropdownbox.children().each(function () {
                if ($(this).val() == usageID) {
                    $(this).prop('selected', true);
                    return;
                }
            });

        }); //END OF Vehicle -chassis each block
    }
    else { //   ELSE STATEMENT- IF IT AS NOT POLICY EDIT, HENCE, POLICY CREATE
        if (typeof $('#postBackVals').val() != 'undefined') {
            $(".chassisNumber").each(function () {
                var usagemodel = $(this).parent().children("#vehUsageID");
                var Usedropdownbox = $(this).parent().parent().parent().children(".this_vehicle_dropdown").children(".useDropDown").children(".usage_dropdown");
                var usageID = usagemodel.val();
                Usedropdownbox.children().each(function () {
                    if ($(this).val() == usageID) {
                        $(this).prop('selected', true);
                        return;
                    }
                });
            });  //END OF VEHICLE CHASSIS-EACH FUNCTINO
         }
     }  //END OF ELSE STATEMENT
 });  //END OF DOCUMENT-READY FUNCTION


///// ADJUSTING THE COVER NOTE DROPDOWN , BASED ON COMPANY SELECTION ///
 $(function () {
     if ($("#insurer").length == 0) {
         $("html").on("change", ".company_dropdown", function () {
             var dropBox = document.getElementById("polCoverBox");
             var dropdownUsages = document.getElementById("usageMenu");
             $.ajax({
                 type: "POST",
                 url: '/PolicyCover/CoversDropDownJSON/?Compid=' + $(this).val(),
                 cache: false,
                 success: function (data) {
                     $.each(data, function (i, v) {
                         v = JSON.parse(v);
                         //Clearing the data from the dropdown 
                         for (i = dropBox.options.length - 1; i >= 0; i--) {
                             dropBox.remove(i);
                         }
                         // Placing the new option in the dropdown
                         for (c = 0; c < v.length; c++) {
                             var option = document.createElement("option");
                             option.text = v[c].cover;
                             option.value = v[c].ID;
                             dropBox.add(option);
                         }
                         if (typeof v[0] == 'undefined') $("#CoverID").val(0);
                         else $("#CoverID").val(dropBox.value);
                         ////CHANGE ANY USAGE DROPDOWNS MAY BE LOADED////
                         $.ajax({
                             type: "POST",
                             url: '/Usage/UsageDropdownOptions/?coverID=' + $(".covers_dropdown").val(),  //$("#CoverID").val(), //
                             cache: false,
                             success: function (data) {
                                 $.each(data, function (i, v) {
                                     v = JSON.parse(v);
                                     var elems = document.getElementsByClassName('usage_dropdown');
                                     for (var i = 0; i < elems.length; i++) {
                                         //clearing the data from the dropdowns
                                         if (typeof elems[i].options !== 'undefined') {
                                             for (p = elems[i].options.length - 1; p >= 0; p--) {
                                                 elems[i].remove(p);
                                             }
                                         }
                                         // Placing the new options in the dropdown (if any)
                                         for (c = 0; c < v.length; c++) {
                                             var option = document.createElement("option");
                                             option.text = v[c].usage;
                                             option.value = v[c].ID;
                                             elems[i].add(option);
                                         }
                                         if (elems[i].value != "") $(".usageIdentity").val(elems[i].value);
                                         else $(".usageIdentity").val("");
                                     } //END OF DROPDOWN FOR LOOP
                                 }) //END OF FOREACH USAGE ITEM
                             } // END OF SUCCESS FUNCTION
                         }); // END OF AJAX CALL FOR VEHICLE USAGES  
                     })
                 }
             });
         });
     }
     else {
         $(".company_dropdown option").each(function () {
             if ($(this).val() == $(".hidden_company_id").val()) {
                 $(this).prop('selected', true);
                 return true;
             }
         });
     }
 });


 $(function () {

     $('.usage_dropdown option:selected').each( function() {
        $(this).parent().parent().find('.vehicle-usage-id').val($(this).val());
     });


     $(document).on("change", ".usage_dropdown", function () {
         var selectedUse = $(this).val();
         $(this).parent().parent().parent().find("#vehUsageID").val(selectedUse);
     });

 });


function TrueDelete(e) {
    $("#policyprefix").prop("required",false);
    $("#policyNum").prop("required",false);
    $("#policycover").prop("required",false);
    var id = $("#hiddenid").val();
    if (id != 0) {
        //DO STUFF
        $.ajax({
            url: '/Policy/DeleteTrue/' + id,
            cache: false,
            success: function () {
                window.location.replace("/Home/Index/");
            }
        });
    }
    else window.location.replace("/Home/Index/");
}


$(function () {
    if ($("#insurer").length == 0) {
        var Polcover = $("#CoverID").val();
        if (Polcover != 0) {
            $(".covers_dropdown").children().each(function () {
                if ($(this).val() == Polcover) {
                    $(this).prop('selected', true);
                    return;
                }
            });
        }
    
        var insurer = $("#insID").val();
        if (insurer != 0) {
            $(".company_dropdown").children().each(function () {
                if ($(this).val() == insurer) {
                    $(this).prop('selected', true);
                    return;
                }
            });
        }
    }
});
$(function () {
    $("form").on('submit',function(){
        StartSpinner();
    });
});