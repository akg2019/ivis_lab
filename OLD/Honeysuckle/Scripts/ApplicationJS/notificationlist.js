﻿var pages = 0;
var current_page = 1;

var error = "<div>There was an error. If the problem persists please contact your administrator</div>";

$(function () {

    $("body").on('click', '.more', function () {
        $(this).parents(".notification-s").find(".more,.ellipses").hide();
        $(this).parents(".notification-s").find(".less,.garbage").show();
        $(this).parents(".notification-s").find(".content").toggleClass("wide").toggleClass("high", 1000);
    });

    $("body").on('click', '.less', function () {
        $(this).parents(".notification-s").find(".more,.ellipses").show();
        $(this).parents(".notification-s").find(".less,.garbage").hide();
        $(this).parents(".notification-s").find(".content").toggleClass("high", 1000, function () {
            $(this).toggleClass("wide");
        })
    });

    $("#notification-notes").perfectScrollbar({
        wheelSpeed: 0.3,
        wheelPropagation: true,
        minScrollbarLength: 10,
        suppressScrollX: true
    });


    if ($.trim($("#sent").html()).length > 0) {
        var modal = "<div>Your Message Was Sent</div>";
        $(modal).dialog();
    }
    if ($.trim($("#senderror").html()).length > 0) {
        var html = '<div>' + $("#senderror").html() + '</div>';
        $(html).dialog();
    }

    $(".data .heading span").each(function () {
        if ($.trim($(this).html()).length > 60) {
            $(this).html($.trim($(this).html()).substring(0, 60) + "...");
        }
    });

    $("#unread").on('click', function () {
        window.location.replace("/Notification/Index");
    });
    $("#all").on('click', function () {
        window.location.replace("/Notification/AllMail");
    });
    $("#trash").on('click', function () {
        window.location.replace("/Notification/Trash");
    });

    $("body").on('click', '.unread-star,.more', function () {
        var id = $(this).parents(".notification-s").find(".note-id").val();
        $(this).parents(".notification-s").find(".unread-star").toggleClass('remove');
        var unread = $(this).parents(".notification-s").find(".star").html().trim().length > 0;
        if (unread) {
            $.ajax({
                url: '/Notification/Read/' + id,
                cache: false,
                success: function (data) {
                    if (data.result) {
                        $(document).find('.unread-star.remove').remove();
                        if (unread) $(document).find('#unread .mail-count').html(parseInt($(document).find('#unread .mail-count').text().trim()) - 1);
                    }
                    else {
                        $(document).find('.unread-star.remove').toggleClass("remove");
                        $(error).dialog({
                            title: 'Error',
                            modal:true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                },
                error: function () {
                    $(error).dialog({
                        title: 'Error',
                        modal:true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                }
            });
        }
    });

    $("body").on('click', '.garbage', function () {
        var id = $(this).parents(".notification-s").find(".note-id").val();
        $(this).parents(".notification-s").toggleClass("trashed");
        var pname = $("#pagename").val();
        var unread = $(this).parents(".notification-s").find(".star").html().trim().length > 0;
        $.ajax({
            url: '/Notification/Delete/' + id,
            cache: false,
            success: function (data) {
                if (data.result) {
                    if (pname == 'all') {
                        $(document).find('#all .mail-count').html(parseInt($(document).find('#all .mail-count').text().trim()) - 1);
                        if (unread) $(document).find('#unread .mail-count').html(parseInt($(document).find('#unread .mail-count').text().trim()) - 1);

                        var i = parseInt($(document).find('#all .mail-count').text().trim());
                        var pcount = parseInt($("#pagecount").val());

                        if (i <= pcount * 10) $(".nextpage").remove();
                    }
                    if (pname == 'unread') {
                       // $(document).find('#unread .mail-count').html(parseInt($(document).find('#unread .mail-count').text().trim()) - 1);
                        $(document).find('#all .mail-count').html(parseInt($(document).find('#all .mail-count').text().trim()) - 1);
                    }
                    $(document).find('#trash .mail-count').html(parseInt($(document).find('#trash .mail-count').text().trim()) + 1);
                    $(document).find(".trashed").slideUp();
                    $(document).find(".trashed").remove();

                    if ($(".more").text().length > 0) {
                        $.ajax({
                            url: '/Notification/AdditionalNote/' + $("#pagecount").val() + "/?page=" + pname,
                            cache: false,
                            success: function (data) {
                                $("#notification-notes").append(data);
                            }
                        });
                    }


                }
                else {
                    $(document).find('.unread-star.remove').toggleClass("remove");
                    $(error).dialog({
                        title: 'Error',
                        modal:true,
                        buttons: {
                            Ok: function () {
                                $(document).find(".trashed").toggleClass("trashed");
                                $(this).dialog('close');
                            }
                        }
                    });
                }
            },
            error: function () {
                $(error).dialog({
                    title: 'Error',
                    modal:true,
                    buttons: {
                        Ok: function () {
                            $(document).find(".trashed").toggleClass("trashed");
                            $(this).dialog('close');
                        }
                    }
                });
            }
        });

    });

});

function updatePages() {
    var i = 0;
    $(".note-page").each(function () {
        i++;
        if (i == current_page) $(this).show();
        else {
            if ($(this).is(":visible")) $(this).hide();
        }
    });
}

function markAsRead(e) {

}

function prevpage(i, s) {
    goToPage(i - 1, s);
}

function nextpage(i, s) {
    goToPage(i + 1);
}

function goToPage(i, s) {
    
    var pname = $("#pagename").val();
    var url = '';

    switch (pname)
    {
        case 'unread':
            url = '/Notification/Index/?page=' + i;
            break;
        case 'all':
            url = '/Notification/AllMail/?page=' + i;
            break;
        case 'trash':
            url = '/Notification/Trash/?page=' + i;
            break;
    }
    
    if (s != undefined) if (s.length > 0) url  += '&search=' + s
    window.location.replace(url);
}



//Find urls

$(function () {

    var regex1; var thisText;
    regex1 = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;

    $(".content").each(function () {

        thisText = $(this).text().replace(regex1, "<a href='$1'>$1</a>");

        $(this).html(""); $(this).append(thisText);

        thisText = "";
    });

});



