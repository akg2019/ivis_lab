﻿document.getElementById("heading").style.width = "400px";
document.getElementById("sendto").style.width = "300px";


$("#sendto").prop('readonly', true);
$("#heading").val("Vehicle required to be cancelled, for new policy.");
$("#content").val("The owner of the vehicle: " + $("#vehInfo").val() + " would like this vehicle to be placed under a new policy.");
$("#heading").prop('readonly', true);

var vehicleId = $("#veh").val();


var PolicyStartDate = $("#start").val();
var PolicyEndDate = $("#end").val();

$("#veh").hide();
var admins = [];

$.getJSON("/Policy/CompanyAdmins/" + vehicleId,
 {
   start: PolicyStartDate , end: PolicyEndDate
}, 

 function (data) {
    if (data.result) {
        admins = data.admins;

        var selectbox = $("#sendto");
        var options = "";
        selectbox.empty();

        for (var i = 0; i < admins.length; i++) {
            options += "<option value='" + admins[i] + "'>" + admins[i] + "</option>";
        }
        selectbox.append(options);

        selectbox.chosen();

       // $("#sendto").val(admins);

    }
});
