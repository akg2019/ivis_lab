﻿$(function () {

    $('#search-json-btn').click(function () {
        var url = "/PolicyUpload/SearchUpload/" + $('#search-json').val();

        $.ajax({

            type: "POST",
            url: url,
            cache: false,
            success: function (result) {
                if (result.haveData) {

                    var status = "";

                    if (result.js.completed) status = "Completed";
                    else if (result.js.inprogress) status = "Still Processing";
                    else status = "Failed";

                    $('.table-data .jsonID.item').html(result.js.ID);
                    $('.table-data .fileName.item').html(result.js.title);
                    $('.table-data .created.item').html(result.createdTime);
                    $('.table-data .status.item').html(status);
                    $('.table-data .completed.item').html(result.completedTime);


                }
                else {
                    $('.table-data .jsonID.item').html('');
                    $('.table-data .fileName.item').html('');
                    $('.table-data .created.item').html('');
                    $('.table-data .status.item').html('');
                    $('.table-data .completed.item').html('');
                    $('.table-data .created.item').html('No results');
                }
            },
            error: function () {
                alert("error finding data");
            }

        });
    });
});