﻿$(function () {
    $(".billing.check").on("change", function () {
        if ($(this).prop('checked')) copyMailingBilling();
        else clearBlling();
        $(".billing-address-section").toggleClass("hidden");
    });


    $(".RoadNumber.mailing").on("keyup", function () {
        if ($(".billing.check").prop('checked')) $(".billing.RoadNumber").val($(this).val());
    });

    $(".roadname.mailing").on("keyup", function () {
        if ($(".billing.check").prop('checked')) $(".billing.roadname").val($(this).val());
    });

    $(".roadtype.mailing").on("keyup", function () {
        if ($(".billing.check").prop('checked')) $(".billing.roadtype").val($(this).val());
    });

    $(".city.mailing").on("keyup", function () {
        if ($(".billing.check").prop('checked')) $(".billing.city").val($(this).val());
    });

    $(".aptNumber.mailing").on("keyup", function () {
        if ($(".billing.check").prop('checked')) $(".billing.aptNumber").val($(this).val());
    });

    $(".parish.mailing").on("change", function () {
        if ($(".billing.check").prop('checked')) {
            $(".billing.parish").val($(this).val());
            $(".billing-address-section").find(".parishes").children("option").each(function () {
                if ($(this).val() == $(".billing.parish").val()) {
                    $(this).prop('selected', true);
                    $(".billing-address-section").find(".parishes").trigger("change");
                    return false;
                }
            });

        }
    });

    if ($(".parish.mailing").val() > 0) {
        $(".policy-mailing-address").find(".parishes").prop('disabled', true);
        $(".policy-mailing-address").find(".parishes").toggleClass('read-only');
        $(".policy-mailing-address").find(".parishes").children("option").each(function () {
            if ($(this).val() == $(".parish.mailing").val()) {
                $(this).prop('selected', true);
                $(".policy-mailing-address").find(".parishes").trigger("change");
                return false;
            }
        });
    }

    if ($(".parish.billing").val() > 0) {
        $(".policy-billing-address").find(".parishes").prop('disabled', true);
        $(".policy-billing-address").find(".parishes").toggleClass('read-only');
        $(".policy-billing-address").find(".parishes").children("option").each(function () {
            if ($(this).val() == $(".parish.billing").val()) {
                $(this).prop('selected', true);
                $(".policy-billing-address").find(".parishes").trigger("change");
                return false;
            }
        });
    }

    $(".country.mailing").on("keyup", function () {
        if ($(".billing.check").prop('checked')) $(".billing.country").val($(this).val());
    });


    $(".pidrop").on("change", function () {
        var val = $(this).val();
        var mailing = $(this).is("#mailingaddressshort");
        var billing = $(this).is("#billingaddressshort");

        if (mailing) $(".hidden-mail-id").val($(this).val());
        if (billing) $(".hidden-bill-id").val($(this).val());

        if (val == 0) {
            if (($(".billing.check").prop('checked') && mailing) || billing) clearBlling();
            if (mailing) clearMailing();
            $('.city').trigger('keyup');
            $('.country').trigger('keyup');
            $('.parishes').trigger('change');
        }
        else {
            $.ajax({
                url: '/Address/GetHolderAddress/' + val,
                cache: false,
                success: function (json) {
                    if (json.success) {
                        if (mailing) updateMailing(json.roadno, json.roadname, json.roadtype, json.apt, json.city, json.parishid, json.country);
                        if (billing) updateBilling(json.roadno, json.roadname, json.roadtype, json.apt, json.city, json.parishid, json.country);
                    }
                    else if (json.longAddressUsed) {
                        $('<div>The form cannot be filled because this person\'s address is in a free text format as follows: </br></br><strong>' + json.longAddress + '</strong></br></br>Please use the structured form provided.</div>').dialog({
                            modal: true,
                            title: 'Alert!',
                            buttons: {
                                OK: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                    else {
                        alert("failed getting address");
                    }
                }
            })
        }
    });

    $("body").on("change", "#maindriver-dropdown", function () {
        var val = $(this).val();
        if (val == 0) clear_driving();
        else update_driving(val);
    });

    UpdateDropDown();

    if ($("#page").val() == "details") $(".parishes").prop('disabled', true).toggleClass('read-only');
});

function clear_driving() {
    $(".driver-details, .driver-details .address-details").each(function () {
        $(this).val("");
        $(this).trigger("change");
    });
    $(".driver-details #parish").val(0);
    $(".driver-details #parishes option").each(function () {
        if ($(this).val() == 0) {
            $(this).prop('selected', true);
            return false;
        }
    });
    $(".driver-details #parishes").trigger("change");
}

function update_driving(id) {
    $.ajax({
        url: '/People/person_json/' + id,
        cache: false,
        success: function (json) {
            if (json.result) {
                $(".driver-details#trn").val(json.person.TRN);
                $(".driver-details#trn").trigger("change");

                $(".driver-details#fname").val(json.person.fname);
                $(".driver-details#fname").trigger("change");

                $(".driver-details#mname").val(json.person.mname);
                $(".driver-details#mname").trigger("change");

                $(".driver-details#lname").val(json.person.lname);
                $(".driver-details#lname").trigger("change");

                $(".driver-details .address-details#RoadNumber").val(json.person.address.roadnumber);
                $(".driver-details .address-details#RoadNumber").trigger("change");

                $(".driver-details .address-details#roadname").val(json.person.address.road.RoadName);
                $(".driver-details .address-details#roadname").trigger("change");

                $(".driver-details .address-details#roadtype").val(json.person.address.roadtype.RoadTypeName);
                $(".driver-details .address-details#roadtype").trigger("change");

                $(".driver-details .address-details#city").val(json.person.address.city.CityName);
                $(".driver-details .address-details#city").trigger("change");

                $(".driver-details .address-details#parish").val(json.person.address.parish.ID);
                $(".driver-details #parishes option").each(function () {
                    if ($(this).val() == json.person.address.parish.ID) {
                        $(this).prop('selected', true);
                        return false;
                    }
                });
                $(".driver-details #parishes").trigger("change");

                $(".driver-details .address-details#country").val(json.person.address.country.CountryName);
                $(".driver-details .address-details#country").trigger("change");
            }
            else {
                $("<div>There was an error in acquiring the driver's information</div>").dialog({
                    modal: true,
                    title: "Error",
                    buttons: {
                        Ok: function () {
                            $(this).dialog('close');
                        }
                    }
                });
            }
        }
    });
}

function DefaultInsuredsDropDown() {
    $(".insuredsdropdown option").each(function () {
        if ($(this).val() == 0) {
            $(this).prop('selected', true);
            return false;
        }
    });
    clearBlling();
    clearMailing();
}

function trigger_mailing() {
    $(".mailing").each(function () {
        $(this).trigger("change");
    });
}

function trigger_billing() {
    $(".billing").each(function () {
        $(this).trigger("change");
    });
}

function updateMailing(roadnum, roadname, roadtype, aptnum, city, pid, country) {
    var message = "";
    
    $(".mailing.RoadNumber").val(roadnum);
    $(".mailing.RoadNumber").prop('readonly', true);
    if (!($(".mailing.RoadNumber").hasClass('read-only'))) $(".mailing.RoadNumber").toggleClass('read-only');


    $(".mailing.roadname").val(roadname);
    $(".mailing.roadname").prop('readonly', true);
    if (!($(".mailing.roadname").hasClass('read-only'))) $(".mailing.roadname").toggleClass('read-only');


    $(".mailing.roadtype").val(roadtype);
    $(".mailing.roadtype").prop('readonly', true);
    if (!($(".mailing.roadtype").hasClass('read-only'))) $(".mailing.roadtype").toggleClass('read-only');

    $(".mailing.aptNumber").val(aptnum);
    $(".mailing.aptNumber").prop('readonly', true);
    if (!($(".mailing.aptNumber").hasClass('read-only'))) $(".mailing.aptNumber").toggleClass('read-only');

    if (city != '') {
        $(".mailing.city").val(city);
        $(".mailing.city").prop('readonly', true);
        if (!($(".mailing.city").hasClass('read-only'))) $(".mailing.city").toggleClass('read-only');
        $(".mailing.city").addClass("ok");
    }
    else {
        $(".mailing.city").val('');
        $(".mailing.city").prop('readonly', false);
        $(".mailing.city").removeClass('read-only');
        message += "city/town,";
    }

    if (pid != 0) {
        $(".mailing.parish").val(pid);
        $(".parishes").addClass("ok");
        $(".policy-mailing-address").find(".parishes option").each(function () {
            if ($(this).val() == pid) {
                $(this).prop('selected', true);
                return false;
            }
        });
        $(".policy-mailing-address").find(".parishes").prop('disabled', true);
        if (!($(".policy-mailing-address").find(".parishes").hasClass('read-only'))) $(".policy-mailing-address").find(".parishes").toggleClass('read-only');
    }
    else {
        message += "parish,";
        $(".mailing.country").val('');
        $(".policy-mailing-address").find(".parishes").prop('disabled', false);
        $(".policy-mailing-address").find(".parishes").removeClass('read-only');
    }
    
    if (country != '') {
        $(".mailing.country").val(country);
        $(".mailing.country").prop('readonly', true);
        if (!($(".mailing.country").hasClass('read-only'))) $(".mailing.country").toggleClass('read-only');
        if (!($(".mailing.country").hasClass('ok'))) $(".mailing.country").toggleClass("ok");
    }
    else {
        message += "country";
        $(".mailing.country").val('');
        $(".mailing.country").prop('readonly', false);
        $(".mailing.country").removeClass('read-only');
    }

    if ($(".billing.check").prop('checked')) copyMailingBilling();

    if (message != '') {
        message = "<div>The " + message + " peices of the address of the policy holder selected were not available from the database. Please fill in the blanks for the system or select another policy holder.</div>";
        $(message).dialog({
            title: 'Address Issue',
            modal:true,
            buttons: {
                Ok: function () {
                    $(this).dialog('close');
                }
            }
        });
    }

    //trigger_mailing();
    //trigger_billing();
}

function updateBilling(roadnum, roadname, roadtype, aptnum, city, pid, country) {
    var message = "";

    $(".billing.RoadNumber").val(roadnum);
    $(".billing.RoadNumber").trigger("change");
    $(".billing.RoadNumber").prop('readonly', true);
    if (!($(".billing.RoadNumber").hasClass('read-only'))) $(".billing.RoadNumber").toggleClass('read-only');

    
    $(".billing.roadname").val(roadname);
    $(".billing.roadname").trigger("change");
    $(".billing.roadname").prop('readonly', true);
    if (!($(".billing.roadname").hasClass('read-only'))) $(".billing.roadname").toggleClass('read-only');
   

    
    $(".billing.roadtype").val(roadtype);
    $(".billing.roadtype").trigger("change");
    $(".billing.roadtype").prop('readonly', true);
    if (!($(".billing.roadtype").hasClass('read-only'))) $(".billing.roadtype").toggleClass('read-only');
   

    $(".billing.aptNumber").val(aptnum);
    $(".billing.aptNumber").trigger("change");
    $(".billing.aptNumber").prop('readonly', true);
    if (!($(".billing.aptNumber").hasClass('read-only'))) $(".billing.aptNumber").toggleClass('read-only');

    if (city != '') {
        $(".billing.city").val(city);
        $(".billing.city").trigger("change");
        $(".billing.city").prop('readonly', true);
        if (!($(".billing.city").hasClass('read-only'))) $(".billing.city").toggleClass('read-only');
    }
    else {
        message += "city/town,";
        $(".billing.city").prop('readonly', false);
        $(".billing.city").removeClass('read-only');
    }

    if (pid != 0) {
        $(".billing.parish").val(pid);
        $(".policy-billing-address .parishes").addClass('ok');
        $(".billing.parish").trigger("change");
        $(".policy-billing-address").find(".parishes option").each(function () {
            if ($(this).val() == pid) {
                $(this).prop('selected', true);
                return false;
            }
        });
        $(".policy-billing-address").find(".parishes").prop('disabled', true);
        if (!($(".policy-billing-address").find(".parishes").hasClass('read-only'))) $(".policy-billing-address").find(".parishes").toggleClass('read-only');
    }
    else {
        $(".policy-billing-address").find(".parishes").prop('disabled', false);
        $(".policy-billing-address").find(".parishes").removeClass('read-only');
        message += "parish,";
    }

    if (country != '') {
        $(".billing.country").val(country);
        $(".billing.country").trigger("change");
        $(".billing.country").prop('readonly', true);
        if (!($(".billing.country").hasClass('read-only'))) $(".billing.country").toggleClass('read-only');
    }
    else {
        message += "country,";
        $(".billing.country").val('');
        $(".billing.country").trigger("change");
        $(".billing.country").prop('readonly', false);
        $(".billing.country").removeClass('read-only');
    }

    //if ($(".billing.check").prop('checked')) copyMailingBilling();

    if (message != '') {
        message = "<div>The " + message + " peices of the address of the policy holder selected were not available from the database. Please fill in the blanks for the system or select another policy holder. Thank you.</div>";
        $(message).dialog({
            title: 'Address Issue',
            modal:true,
            buttons: {
                Ok: function () {
                    $(this).dialog('close');
                }
            }
        });
    }
}

function copyMailingBilling() {
    $(".billing.RoadNumber").val($(".mailing.RoadNumber").val());
    $(".billing.RoadNumber").trigger("change");
    
    $(".billing.roadname").val($(".mailing.roadname").val());
    $(".billing.roadname").trigger("change");
    
    $(".billing.roadtype").val($(".mailing.roadtype").val());
    $(".billing.roadtype").trigger("change");
    
    $(".billing.aptNumber").val($(".mailing.aptNumber").val());
    $(".billing.aptNumber").trigger("change");
    
    $(".billing.city").val($(".mailing.city").val());
    $(".billing.city").trigger("change");
    
    $(".billing.parish").val($(".mailing.parish").val());
    $(".billing.parish").trigger("change");
    
    $(".billing.country").val($(".mailing.country").val());
    $(".billing.country").trigger("change");
    
    $(".policy-billing-address").find(".parishes option").each(function () {
        if ($(this).val() == $(".mailing.parish").val()) {
            $(this).prop('selected', true);
            return false;
        }
    });
}

function clearBlling() {

//    if ($("#page").val() == "edit") {
//        $.ajax({
//            url: '/policy/get_last_billing_address/' + $("#hiddenid").val(),
//            cache: false,
//            success: function (json) {
//                if (json.result) {
//                    updateBilling(json.address.roadnumber,
//                                  json.address.road.RoadName,
//                                  json.address.roadtype.RoadTypeName,
//                                  json.address.ApartmentNumber,
//                                  json.address.city.CityName,
//                                  json.address.parish.ID,
//                                  json.address.country.CountryName);
//                    if (json.address.short_code != "") {
//                        $("#billingaddressshort option").each(function () {
//                            if ($(this).val() == json.address.short_code) {
//                                $(this).prop('selected', true);
//                                return true;
//                            }
//                        });

//                    }
//                }
//                else proper_clear_billing();
//            }
//        });
//    }
//    else
     proper_clear_billing();
}

function proper_clear_billing() {
    $(".billing.RoadNumber").val("");
    $(".billing.RoadNumber").trigger("change");
    $(".billing.RoadNumber").prop('readonly', false);
    if (($(".billing.RoadNumber").hasClass('read-only'))) $(".billing.RoadNumber").toggleClass('read-only');

    $(".billing.roadname").val("");
    $(".billing.roadname").trigger("change");
    $(".billing.roadname").prop('readonly', false);
    if (($(".billing.roadname").hasClass('read-only'))) $(".billing.roadname").toggleClass('read-only');

    $(".billing.roadtype").val("");
    $(".billing.roadtype").trigger("change");
    $(".billing.roadtype").prop('readonly', false);
    if (($(".billing.roadtype").hasClass('read-only'))) $(".billing.roadtype").toggleClass('read-only');

    $(".billing.aptNumber").val("");
    $(".billing.aptNumber").trigger("change");
    $(".billing.aptNumber").prop('readonly', false);
    if (($(".billing.aptNumber").hasClass('read-only'))) $(".billing.aptNumber").toggleClass('read-only');

    $(".billing.city").val("");
    $(".billing.city").trigger("change");
    $(".billing.city").prop('readonly', false);
    if (($(".billing.city").hasClass('read-only'))) $(".billing.city").toggleClass('read-only');

    $(".billing.parish").val("");
    $(".billing.parish").trigger("change");
    $(".policy-billing-address").find(".parishes").prop('disabled', false);
    if (($(".policy-billing-address").find(".parishes").hasClass('read-only'))) $(".policy-billing-address").find(".parishes").toggleClass('read-only');
    if (($(".policy-billing-address .parishes").hasClass('ok'))) $(".policy-billing-address .parishes").toggleClass("ok");

    $(".billing.country").val("");
    $(".billing.country").trigger("change");
    $(".billing.country").prop('readonly', false);
    if (($(".billing.country").hasClass('read-only'))) $(".billing.country").toggleClass('read-only');

    $(".policy-billing-address").find(".parishes option").each(function () {
        if ($(this).val() == 0) {
            $(this).prop('selected', true);
            return false;
        }
    });
}

function clearMailing() {
    $(".mailing.RoadNumber").val("");
    $(".mailing.RoadNumber").trigger("change");
    $(".mailing.RoadNumber").prop('readonly', false);
    if (($(".mailing.RoadNumber").hasClass('read-only'))) $(".mailing.RoadNumber").toggleClass('read-only');

    $(".mailing.roadname").val("");
    $(".mailing.roadname").trigger("change");
    $(".mailing.roadname").prop('readonly', false);
    if (($(".mailing.roadname").hasClass('read-only'))) $(".mailing.roadname").toggleClass('read-only');

    $(".mailing.roadtype").val("");
    $(".mailing.roadtype").trigger("change");
    $(".mailing.roadtype").prop('readonly', false);
    if (($(".mailing.roadtype").hasClass('read-only'))) $(".mailing.roadtype").toggleClass('read-only');

    $(".mailing.aptNumber").val("");
    $(".mailing.aptNumber").trigger("change");
    $(".mailing.aptNumber").prop('readonly', false);
    if (($(".mailing.aptNumber").hasClass('read-only'))) $(".mailing.aptNumber").toggleClass('read-only');

    $(".mailing.city").val("");
    $(".mailing.city").trigger("change");
    $(".mailing.city").prop('readonly', false);
    if (($(".mailing.city").hasClass('read-only'))) $(".mailing.city").toggleClass('read-only');
    if (($(".mailing.city").hasClass('ok'))) $(".mailing.city").toggleClass("ok");

    $(".mailing.parish").val("");
    $(".mailing.parish").trigger("change");
    $(".policy-mailing-address").find(".parishes").prop('disabled', false);
    if (($(".policy-mailing-address").find(".parishes").hasClass('read-only'))) $(".policy-mailing-address").find(".parishes").toggleClass('read-only');
    if (($(".parishes").hasClass('ok'))) $(".parishes").toggleClass("ok");

    $(".mailing.country").val("");
    $(".mailing.country").trigger("change");
    $(".mailing.country").prop('readonly', false);
    if (($(".mailing.country").hasClass('read-only'))) $(".mailing.country").toggleClass('read-only');
    if (($(".mailing.country").hasClass('ok'))) $(".mailing.country").toggleClass("ok");

    $(".policy-mailing-address").find(".parishes option").each(function () {
        if ($(this).val() == 0) {
            $(this).prop('selected', true);
            return false;
        }
    });

    if ($(".billing.check").prop('checked')) copyMailingBilling();
}

function UpdateDropDown() {

    $(".insured").each(function () {
        $(".pidrop").append("<option value=\"" + "p" + $(this).find(".perId").val() + "\">" + $(this).find(".names").val() + "</option>");
    });

    $(".company").each(function () {
        $(".pidrop").append("<option value=\"" + "c" + $(this).find(".comId").val() + "\">" + $(this).find(".companyNames").val() + "</option>");
    });

    //test if this is post back
    $("#mailingaddressshort option").each(function () {
        if ($(this).val() == $(".hidden-mail-id").val()) {
            $(this).prop('selected', true);
            return false;
        }
    });

    $("#billingaddressshort option").each(function () {
        if ($(this).val() == $(".hidden-bill-id").val()) {
            $(this).prop('selected', true);
            return false;
        }
    });

    if ($(".hidden-mail-id").val() != "0" && $(".hidden-mail-id").val() != "") {
        $(".mailing.RoadNumber").prop('readonly', true);
        if (!($(".mailing.RoadNumber").hasClass('read-only'))) $(".mailing.RoadNumber").toggleClass('read-only');
        $(".mailing.roadname").prop('readonly', true);
        if (!($(".mailing.roadname").hasClass('read-only'))) $(".mailing.roadname").toggleClass('read-only');
        $(".mailing.roadtype").prop('readonly', true);
        if (!($(".mailing.roadtype").hasClass('read-only'))) $(".mailing.roadtype").toggleClass('read-only');
        $(".mailing.aptNumber").prop('readonly', true);
        if (!($(".mailing.aptNumber").hasClass('read-only'))) $(".mailing.aptNumber").toggleClass('read-only');
        $(".mailing.city").prop('readonly', true);
        if (!($(".mailing.city").hasClass('read-only'))) $(".mailing.city").toggleClass('read-only');
        $(".policy-mailing-address").find(".parishes").prop('disabled', true);
        if (!($(".policy-mailing-address").find(".parishes").hasClass('read-only'))) $(".policy-mailing-address").find(".parishes").toggleClass('read-only');
        $(".mailing.country").prop('readonly', true);
        if (!($(".mailing.country").hasClass('read-only'))) $(".mailing.country").toggleClass('read-only');
    }
    else {
        $(".mailing.RoadNumber").prop('readonly', false);
        $(".mailing.RoadNumber").removeClass('read-only');
        $(".mailing.roadname").prop('readonly', false);
        $(".mailing.roadname").removeClass('read-only');
        $(".mailing.roadtype").prop('readonly', false);
        $(".mailing.roadtype").removeClass('read-only');
        $(".mailing.aptNumber").prop('readonly', false);
        $(".mailing.aptNumber").removeClass('read-only');
        $(".mailing.city").prop('readonly', false);
        $(".mailing.city").removeClass('read-only');
        $(".policy-mailing-address").find(".parishes").prop('disabled', false);
        $(".policy-mailing-address").find(".parishes").removeClass('read-only');
        $(".mailing.country").prop('readonly', false);
        $(".mailing.country").removeClass('read-only');
    }

    if ($(".hidden-bill-id").val() != "0" && $(".hidden-bill-id").val() != "") {
        $(".billing.RoadNumber").prop('readonly', true);
        if (!($(".billing.RoadNumber").hasClass('read-only'))) $(".billing.RoadNumber").toggleClass('read-only');
        $(".billing.roadname").prop('readonly', true);
        if (!($(".billing.roadname").hasClass('read-only'))) $(".billing.roadname").toggleClass('read-only');
        $(".billing.roadtype").prop('readonly', true);
        if (!($(".billing.roadtype").hasClass('read-only'))) $(".billing.roadtype").toggleClass('read-only');
        $(".billing.aptNumber").prop('readonly', true);
        if (!($(".billing.aptNumber").hasClass('read-only'))) $(".billing.aptNumber").toggleClass('read-only');
        $(".billing.city").prop('readonly', true);
        if (!($(".billing.city").hasClass('read-only'))) $(".billing.city").toggleClass('read-only');
        $(".policy-billing-address").find(".parishes").prop('disabled', true);
        if (!($(".policy-billing-address").find(".parishes").hasClass('read-only'))) $(".policy-billing-address").find(".parishes").toggleClass('read-only');
        $(".billing.country").prop('readonly', true);
        if (!($(".billing.country").hasClass('read-only'))) $(".billing.country").toggleClass('read-only');
    }
    else {
        $(".billing.RoadNumber").prop('readonly', false);
        $(".billing.RoadNumber").removeClass('read-only');
        $(".billing.roadname").prop('readonly', false);
        $(".billing.roadname").removeClass('read-only');
        $(".billing.roadtype").prop('readonly', false);
        $(".billing.roadtype").removeClass('read-only');
        $(".billing.aptNumber").prop('readonly', false);
        $(".billing.aptNumber").removeClass('read-only');
        $(".billing.city").prop('readonly', false);
        $(".billing.city").removeClass('read-only');
        $(".policy-billing-address").find(".parishes").prop('disabled', false);
        $(".policy-billing-address").find(".parishes").removeClass('read-only');
        $(".billing.country").prop('readonly', false);
        $(".billing.country").removeClass('read-only');
    }
}