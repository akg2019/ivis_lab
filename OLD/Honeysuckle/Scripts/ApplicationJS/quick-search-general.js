﻿var text;
var page;
var section;

var allowSuper = true;
var is_safari = false; //navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0;
var options;
var allpermissions;
var mypermissions;
var allgroups;
var mygroups;


$(function () {

    is_safari = (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0);

    page = "default";
    //test what page we're at
    if ($(".quick-search").hasClass("userlist")) page = "userlist";
    if ($(".quick-search").hasClass("wording")) page = "wording";
    if ($(".quick-search").hasClass("rolemanagement-index")) page = "rolemanagement-index";
    if ($(".quick-search").hasClass("intermediary-index")) page = "intermediary-index";
    if ($(".quick-search").hasClass("policycover-index")) page = "policycover-index";
    if ($(".quick-search").hasClass("mapping")) page = "mapping";
    if ($(".quick-search").hasClass("rolemanagement-edit")) { page = "rolemanagement-edit"; setup() }
    if ($(".quick-search").hasClass("rolemanagement-create")) { page = "rolemanagement-create"; setup() }
    if ($(".quick-search").hasClass("rolemanagement-assign")) page = "rolemanagement-assign";
    if ($(".quick-search").hasClass("assign-group")) page = "assign-group";
    if ($(".quick-search").hasClass("cover-note")) page = "cover-note";
    if ($(".quick-search").hasClass("auth-wording")) page = "auth-wording";

    $(".quick-search").bind('keyup', function (event) {
        text = $(".quick-search").val();

        if (event.which == 13) {
            if (page == "rolemanagement-create" || page == "rolemanagement-edit" || page == "rolemanagement-assign") {
                if ($(this).hasClass("userpermissions-not")) {
                    section = "userpermissions-not";
                    text = $(".userpermissions-not").val();
                }
                if ($(this).hasClass("userpermissions-is")) {
                    section = "userpermissions-is";
                    text = $(".userpermissions-is").val();
                }
                if ($(this).hasClass("childgroups-not")) {
                    section = "childgroups-not";
                    text = $(".childgroups-not").val();
                }
                if ($(this).hasClass("childgroups-is")) {
                    section = "childgroups-is";
                    text = $(".childgroups-is").val();
                }
                if ($(this).hasClass("not-in-group")) {
                    section = "not-in-group";
                    text = $(".out").val();
                }
                if ($(this).hasClass("in-group")) {
                    section = "in-group";
                    text = $(".in").val();
                }
            }
            if (page == "assign-group") {
                if ($(this).parent().find(".quick-search").hasClass("not")) {
                    section = "group-out";
                    text = $(".not").val();
                }
                if ($(this).parent().find(".quick-search").hasClass("is")) {
                    section = "group-in";
                    text = $(".is").val();
                }
            }
            filter(text, page);
        }
    });

    $("form").on('keydown', function (event) {
        if (event.which == 13 && $(".quick-search").is(':focus')) {
            event.preventDefault();
            return false;
        }
    });

});

function clickSearchBtn(e) {

    

    if (page == "rolemanagement-create" || page == "rolemanagement-edit" || page == "rolemanagement-assign") {
        if ($(e).parent().find(".quick-search").hasClass("userpermissions-not")) {
            section = "userpermissions-not";
            text = $(".userpermissions-not").val();
        }
        if ($(e).parent().find(".quick-search").hasClass("userpermissions-is")) {
            section = "userpermissions-is";
            text = $(".userpermissions-is").val();
        }
        if ($(e).parent().find(".quick-search").hasClass("childgroups-not")) {
            section = "childgroups-not";
            text = $(".childgroups-not").val();
        }
        if ($(e).parent().find(".quick-search").hasClass("childgroups-is")) {
            section = "childgroups-is";
            text = $(".childgroups-is").val();
        }
        if ($(e).parent().find(".quick-search").hasClass("not-in-group")) {
            section = "not-in-group";
            text = $(".out.searching").val();
        }
        if ($(e).parent().find(".quick-search").hasClass("in-group")) {
            section = "in-group";
            text = $(".in.searching").val();
        }
    }
    if (page == "assign-group") {
        if ($(e).parent().find(".quick-search").hasClass("not")) {
            section = "group-out";
            text = $(".not").val();
        }
        if ($(e).parent().find(".quick-search").hasClass("is")) {
            section = "group-in";
            text = $(".is").val();
        }
    }
    filter(text, page);
}

function filter(text, page) {
    switch (page) {
       
        case "wording":
            filterList(text);
            redrawList();
            $(".word-count .count").html(count_words());
            break;
        case "userlist":
        case "mapping":
        case "policycover-index":
        case "rolemanagement-index":
        case "intermediary-index":
        case "cover-note":
        case "auth-wording":
            filterList(text);
            redrawList();
            //$(".word-count .count").html(count_words());
            break;
        case "rolemanagement-edit":
        case "rolemanagement-create":
        case "rolemanagement-assign":
        case "assign-group":
            switch (section) {
                case "userpermissions-not":
                    filterUserPermissionNot(text);
                    break;
                case "userpermissions-is":
                    filterUserPermissionIs(text);
                    break;
                case "childgroups-not":
                    filterChildGroupsNot(text);
                    break;
                case "childgroups-is":
                    filterChildGroupsIs(text);
                    break;
                case "not-in-group":
                    filterInGroup(text);
                    break;
                case "in-group":
                    filterOutGroup(text);
                    break;
                case "group-out":
                    filterOutGroups(text);
                    break;
                case "group-in":
                    filterInGroups(text);
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }

}

function filterInGroups(text) {
    if (is_safari) {

        $("#groups").children("option").remove();
        if (text.trim() == "") {
            $("#groups").append(buildHtmlFromList(groups));
        }
        else {
            var list = [];
            var i = 0;
            $(groups).each(function () {
                if ($(this).text().trim().toLowerCase().indexOf(text.toLowerCase()) > -1) {
                    list[i] = $(this);
                    i++;
                }
            });
            $("#groups").append(buildHtmlFromList(list));
        }
    }
    else {
        $("#groups").children("option").each(function () {
            if (text.trim() == "") {
                $(this).show();
            }
            else {
                if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            }
        });
    }
}

function filterOutGroups(text) {
    if (is_safari) {

        $("#notgroups").children("option").remove();
        if (text.trim() == "") {
            $("#notgroups").append(buildHtmlFromList(groups));
        }
        else {
            var list = [];
            var i = 0;
            $(groups).each(function () {
                if ($(this).text().trim().toLowerCase().indexOf(text.toLowerCase()) > -1) {
                    list[i] = $(this);
                    i++;
                }
            });
            $("#notgroups").append(buildHtmlFromList(list));
        }
    }
    else {
        $("#notgroups").children("option").each(function () {
            if (text.trim() == "") {
                $(this).show();
            }
            else {
                if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            }
        });
    }
}

function filterList(e) {
    if (e.length == 0) {
        $(".table .row, .ivis-table .row").each(function () {
            $(this).show();
        });
    }
    else {
        $(".table .row, .ivis-table .row").each(function () {
            if (page == "mapping") {
                if ($(this).find(".datamap").html().trim().toLowerCase().indexOf(e.trim().toLowerCase()) > -1 || $(this).find(".tablename").html().trim().toLowerCase().indexOf(e.trim().toLowerCase()) > -1) $(this).show();
                else $(this).hide();
            }
            else {
                $(this).hide();
                $(this).children(".item").each(function () {
                    if (!($(this).hasClass("functions") || $(this).hasClass("check"))) {
                        if ($(this).html().trim().toLowerCase().indexOf(e.trim().toLowerCase()) > -1) {
                            $(this).parents('.row').show();
                            return false;
                        }
                    }
                });
            }
        });
    }
}

function filterListRoleManagementIndex(e) {
    var filter = e;
    var len = $(".companygroup").length;
    var i = 0;
    var ind = 0;
    $(".companygroup").each(function (index, element) {
        ind = index;
        $(this).children(".user-group").children(".user-group-name").each(function (index, element) {
            if (filter.trim() == "") {
                $(this).parent().show();
            }
            else {
                if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    $(this).parent().show();
                }
                else {
                    $(this).parent().hide();
                }
            }
        });
        if ($(this).children(".groupname").text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
            $(this).children(".user-group").show();
        }
        if (ind == len - 1) reDrawOddEven();
    });
}




function redrawList() {
    var i = 0;
    $(".table .row, .ivis-table .row").each(function (index, value) {
        //remove even/odd from divs
        if ($(this).hasClass("even")) $(this).toggleClass("even");
        if ($(this).hasClass("odd")) $(this).toggleClass("odd");
        //add it back
        if ($(this).is(':visible')) {
            if (i % 2 == 0) $(this).toggleClass("even"); //even
            else $(this).toggleClass("odd");
            i++;
        }
    }); 
    //var Rec = document.getElementById("RecordsReturned");

   // if (i > 0) {
      //  Rec.value = i + " Record(s) found ";
    //}

//    var RoleRecord = document.getElementById("RoleRecords");
//    RoleRecord.value = 40 + " Role(s) found ";

}

function redrawListRoleManagementIndex() {
    var i = 0;
    $(".companygroup").each(function () {
        i = 0;
        $(this).children(".user-group").each(function () {
            if ($(this).is(":visible")) {
                //remove odd/even from classes
                if ($(this).hasClass("odd")) $(this).toggleClass("odd");
                if ($(this).hasClass("even")) $(this).toggleClass("even");
                //add back
                if (i % 2 == 0) $(this).toggleClass("odd");
                else $(this).toggleClass("even");

                i++;


            }
        });
    });
    
   
}


/* THIS SECTION FOR ROLEMANAGEMENT CREATE, EDIT AND ASSIGN */


function filterInGroup(filter) {
    if (is_safari) {

        $("#groupusers,#groups").children("option").remove();
        if (filter.trim() == "") {
            $("#groupusers,#groups").append(buildHtmlFromList(groups));
        }
        else {
            var list = [];
            var i = 0;
            $(groups).each(function () {
                if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    list[i] = $(this);
                    i++;
                }
            });
            $("#groupusers,#groups").append(buildHtmlFromList(list));
        }
    }
    else {
        $("#groupusers,#groups").children("option").each(function () {
            if (filter.trim() == "") {
                $(this).show();
            }
            else {
                if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            }
        });
    }
}

function filterOutGroup(filter) {
    if (is_safari) {

        $("#notgroupusers,#notgroups").children("option").remove();
        if (filter.trim() == "") {
            $("#notgroupusers,#notgroups").append(buildHtmlFromList(notgroups));
        }
        else {
            var list = [];
            var i = 0;
            $(notgroups).each(function () {
                if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    list[i] = $(this);
                    i++;
                }
            });
            $("#notgroupusers,#notgroups").append(buildHtmlFromList(list));
        }
    }
    else {
        $("#notgroupusers,#notgroups").children("option").each(function () {
            if (filter.trim() == "") {
                $(this).show();
            }
            else {
                if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            }
        });
    }
}

function buildHtmlFromList(list) {
    var string = "";
    $(list).each(function () {
        string += "<option value=\"" + $(this).val() + "\">" + $(this).text() + "</option>";
    });
    return string;
}

function setup() {
    allpermissions = $("#allpermissions").children("option");
    mypermissions = $("#mypermissions").children("option");
    allgroups = $("#allgroups").children("option");
    mygroups = $("#mygroups").children("option");
}

function filterUserPermissionNot(filter) {
    if (is_safari) {

        $("#allpermissions").children("option").remove();
        if (filter.trim() == "") {
            $("#allpermissions").append(buildHtmlFromList(allpermissions));
        }
        else {
            var list = [];
            var i = 0;
            $(allpermissions).each(function () {
                if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    list[i] = $(this);
                    i++;
                }
            });
            $("#allpermissions").append(buildHtmlFromList(list));
        }
    }
      else {
         $("#allpermissions").children("option").each(function () {
            if (filter.trim() == "") {
                $(this).show();
            }
            else {
                if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            }
        });
    } 
}

function filterUserPermissionIs(filter){
    if (is_safari) {

        $("#mypermissions").children("option").remove();
        if (filter.trim() == "") {
            $("#mypermissions").append(buildHtmlFromList(mypermissions));
        }
        else {
            var list = [];
            var i = 0;
            $(mypermissions).each(function () {
                if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    list[i] = $(this);
                    i++;
                }
            });
            $("#mypermissions").append(buildHtmlFromList(list));
        }
    }
    else {
        $("#mypermissions").children("option").each(function () {
            if (filter.trim() == "") {
                $(this).show();
            }
            else {
                if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            }
        });
    }
}

function filterChildGroupsNot(filter) {
    if (is_safari) {

        $("#allgroups").children("option").remove();
        if (filter.trim() == "") {
            $("#allgroups").append(buildHtmlFromList(allgroups));
        }
        else {
            var list = [];
            var i = 0;
            $(allgroups).each(function () {
                if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    list[i] = $(this);
                    i++;
                }
            });
            $("#allgroups").append(buildHtmlFromList(list));
        }
    }
    else {
        $("#allgroups").children("option").each(function () {
            if (filter.trim() == "") {
                $(this).show();
            }
            else {
                if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            }
        });
    }
}

function filterChildGroupsIs(filter) {
    if (is_safari) {

        $("#mygroups").children("option").remove();
        if (filter.trim() == "") {
            $("#mygroups").append(buildHtmlFromList(mygroups));
        }
        else {
            var list = [];
            var i = 0;
            $(mygroups).each(function () {
                if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    list[i] = $(this);
                    i++;
                }
            });
            $("#mygroups").append(buildHtmlFromList(list));
        }
    }
    else {
        $("#mygroups").children("option").each(function () {
            if (filter.trim() == "") {
                $(this).show();
            }
            else {
                if ($(this).text().trim().toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            }
        });
    }
}


/* END SECTION */
