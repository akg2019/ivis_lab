﻿document.getElementById("heading").style.width = "400px";
document.getElementById("sendto").style.width = "300px";
$("#sendto").prop('readonly', true);
$("#heading").val("Vehicle Certificate approval required.");

//This part is needed for when there are more certs than one to be approved
var certsList = $("#veh").val().split(',');
var message = "Approval request for the following certificate(s): ";

for (var i = 0; i < certsList.length; i++) {
    message = message + window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + "/VehicleCertificate/Details/" + certsList[i];
    if (i !== certsList.length - 1) message = message + ",";
}

$("#content").val(message);
$("#heading").prop('readonly', true);

var vehicleId = $("#veh").val();

$("#veh").hide();
var admins = [];

if (certsList != null && certsList!="") {
    //The first certificate Id in the list can be sent (if it is a lone 
    $.getJSON("/VehicleCertificate/CompanyBrokers/" + certsList[0], function (data) {
        if (data.result) {
            admins = data.admins;
            var selectbox = $("#sendto");
            var options = "";
            selectbox.empty();
            for (var i = 0; i < admins.length; i++) {
                options += "<option value='" + admins[i][0] + "'>" + admins[i][1] + "</option>";
            }
            selectbox.append(options);
            selectbox.chosen();
        }
    });
}