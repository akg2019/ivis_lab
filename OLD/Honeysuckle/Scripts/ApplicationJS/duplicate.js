﻿$(function () {
    $(".data.row").on('click', function () {
        var ref_no = $(this).find(".ref").text().trim();
        var type = $(this).find(".type").text().trim();
        $("<div></div>").load("/Duplicate/ShowDuplicates/?type=" + type + "&refid=" + ref_no, function () {
            $(".sliding-div").perfectScrollbar({
                wheelSpeed: 0.3,
                wheelPropagation: true,
                minScrollbarLength: 10,
                suppressScrollX: true
            });
        }).dialog({
            modal: true,
            height: 550,
            width: 650,
            title: 'Duplicate Data',
            buttons: {
                'close': function () {
                    $(this).dialog('close');
                    $(this).dialog('destroy');
                }
            }
        });
    });

    $("html").on("click", ".prev", function () {
        push_left();
    });

    $("html").on("click", ".next", function () {
        push_right();
    });
});

function push_left() {
    $(".sliding-div").animate({
        left: "-=290"
    }, 2000, function () {

    });
}

function push_right() {
    $(".sliding-div").animate({
        left: "+=290"
    }, 2000, function () {

    });
}