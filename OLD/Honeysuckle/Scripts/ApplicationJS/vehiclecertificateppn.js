﻿var ppnfocus = false;
var ppnExistsMessage = "<div>The Printed Paper No. you've just entered already exists in the database. Please enter one that does not already exist</div>"

$(function () {
    $("#ppn").focus(function () {
        ppnfocus = true;
    });
    $("#ppn").blur(function () {
        if (ppnfocus) {
            var ppn = $("#ppn").val().trim();
            if (ppn.length > 0) {

                var regexNumbers = /^[0-9]+$/;
                if (!regexNumbers.test(ppn)) {
                    alert('Please enter a valid Printed Paper Number (numbers only)! ');
                    return false;
                }

                else {
                    $.getJSON("/VehicleCertificate/TestPrintedPaperNo/" + ppn, function (data) {
                        if (data.result) {
                            $(ppnExistsMessage).dialog({
                                modal: true,
                                title: "Alert",
                                buttons: {
                                    Ok: function () {
                                        $("#ppn").val("");
                                        $(".ui-dialog-content").dialog("close");
                                    }
                                }
                            });
                        }
                    }); //END OF JSON REQUEST
                }//END OF ELSE
            }
        }
        ppnfocus = false;
    });
});
