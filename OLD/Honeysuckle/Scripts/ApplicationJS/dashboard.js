﻿$(function () {

function GetCompanyLits() {
    $.ajax({
            url: '/Company/CompaniesList/',
            cache: false,
            success: function (html) {
                $(".companies-list").html(html);
                update_scroll();
            },
            error: function () {
                $("<div></div>").text("There was an error in loading this partial").dialog({
                    modal: true,
                    title: "Error Loading Partial",
                    buttons: {
                        Ok: function () {
                            $(this).dialog('close');
                        }
                    }
                })
            }
        });
}


//GetCompanyLits();
   

   
});

function update_scroll() {
    $('.table-data').perfectScrollbar({
        wheelSpeed: 0.3,
        wheelPropagation: true,
        minScrollbarLength: 10,
        suppressScrollX: true
    });
}