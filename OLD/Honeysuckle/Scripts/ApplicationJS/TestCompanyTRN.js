﻿

var compTRNFocus = false;
var compTRNSet = false;
var TRN1 = $("#trn").val();
var selectedId = "";

$(function () {
    if (typeof $("#trn").val() != 'undefined')
    if ($("#trn").val().trim().length > 0) {
        compTRNSet = true;
    }
});

$(function () {
    $("#trn").focus(function () {
        compTRNFocus = true;
    })
});

$(function () {
    $("#trn").blur(function () {
        if (compTRNFocus) {
            var result = false;
            var regexTRN = /^(([ ]*[01][0-9]{12}[ ]*)|([ ]*[0][0-9]{8}[ ]*))$/;
            var regexComTRN1 = /^[ ]*[0][0-9]{8}[ ]*$/;  //regex to ensure that the TRN is an integer, that starts with a 0 before checking for its existence in the system

            if (($("#trn").val() == '' || (!regexTRN.test($("#trn").val()) && !$('#foreignIDCheck').is(':checked')) /*&& !regexComTRN1.test($("#trn").val())*/)) {
                alert('Please enter a valid Company TRN. The Company TRN must be 13 digits long when \'Trading As\' applies, or begin with a 0 when 9 digits long');
                return false;
            }

            else
                $.getJSON("/Company/GetCompanyTRN/?TRN=" + $("#trn").val() + "&test=" + true, function (data) {
                    $("body").append('<div class="ui-widget-overlay ui-front temp"><div class="tempmessage"><img src="../../Content/more_images/loading.gif" /><span>Processing Data</span></div></div>');
                    $(".ui-front.temp").remove();
                    if (!data.error) {
                        if (data.result) {
                            //company already exists

                            $("#InformationDialog").dialog({
                                modal: true,
                                autoOpen: false,
                                width: 600,
                                title: "Company Exists",
                                height: 370,
                                buttons: {
                                    Ok: function () {

                                        if ($('.dupCompTRNcheck:checked').length <= 0 && $('#duplicatePartial').length > 0) {
                                            alert('Please select a record or cancel');
                                            return false;
                                        }

                                        if ($('#duplicatePartial').length > 0) {

                                            $.ajax({
                                                type: "POST",
                                                traditional: true,
                                                url: "/Company/GetFullCompanyDetails/?compId=" + selectedId,
                                                async: false,
                                                dataType: "json",
                                                success: function (rdata) {
                                                    data = rdata;
                                                },
                                                error: function () {
                                                    alert('Error receiving data');
                                                }
                                            });
                                        }

                                        $(this).dialog("close");
                                        $("#InformationDialog").html("");
                                        $("#company-modal1").dialog("close");
                                        $("#company-modal1").html("");
                                        AddCompanySelected(data.compId, 1);

                                    },
                                    Edit: function () {

                                        if ($('.dupCompTRNcheck:checked').length <= 0 && $('#duplicatePartial').length > 0) {
                                            alert('Please select a record or cancel');
                                            return false;
                                        }

                                        if ($('#duplicatePartial').length > 0) {

                                            $.ajax({
                                                type: "POST",
                                                traditional: true,
                                                url: "/Company/GetFullCompanyDetails/?compId=" + selectedId,
                                                async: false,
                                                dataType: "json",
                                                success: function (rdata) {
                                                    data = rdata;
                                                },
                                                error: function () {
                                                    alert('Error receiving data');
                                                }
                                            });
                                        }

                                        $('#CompanyName').val(data.compname);
                                        $('#trn').val(data.comptrn);
                                        $('#RoadNumber').val(data.roadno);
                                        $('#roadname').val(data.roadname);
                                        $('#roadtype').val(data.roadtype);
                                        $('#city').val(data.city);
                                        $("#aptNumber").val(data.aptNumber);
                                        $("#parish").val(data.parishid);
                                        $("#country").val(data.country);
                                        $("#editingPolCompany").val(true);

                                        $('#CompanyName').prop('disabled', true);
                                        $('#CompanyName').toggleClass("read-only");

                                        var thisparishId = data.parishid;
                                        if (thisparishId != 0) {
                                            $(".parishes").children().each(function () {
                                                if ($(this).val() == thisparishId) {
                                                    $(this).prop('selected', true);
                                                    return;
                                                }
                                            });
                                        }
                                        $('#CompanyName,#trn,#RoadNumber,#roadname,#roadtype,#city,#aptNumber,#country').trigger('keyup');
                                        $('.parishes').trigger('change');
                                        $(this).dialog("close");
                                        $("#InformationDialog").html("");
                                    },
                                    Cancel: function () {
                                       // $("#trn").val("");
                                        $(this).dialog("close");
                                        $("#InformationDialog").html("");
                                    }
                                }
                            });

                            if (data.duplicate) {
                                $("#InformationDialog").load('/Company/GetCompanyDuplicates/?trn=' + $("#trn").val(), function (value) { }).dialog('open');
                            }
                            else {
                                $("#InformationDialog").load('/Company/FullCompanyDetails/' + data.compId + "?create=" + true, function (value) { }).dialog('open');
                            }

                            
                        }

                        else {
                            //TRN Not in the system yet
                            if (TRN1.trim().length != 0) compTRNSet = true;

                        }
                    }
                    else if (data.invalid) {
                        $("<div>This is not a valid TRN!</div>").dialog({
                            modal: true,
                            title: 'Alert!',
                            buttons: {
                                OK: function () {
                                    $(this).dialog('close');
                                    $("#trn").val("");
                                    $("#trn").focus();
                                    $("#trn").trigger('keyup');
                                }
                            }
                        });
                    }
                    else {
                        //ERROR
                        $("<div>" + data.message + "</div>").dialog({
                            modal: true,
                            title: "Alert",
                            buttons: {
                                Ok: function () {
                                    $("#trn").val("");
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }

                });
            compTRNFocus = false;
        }
    })
});

$(function () {

    $('#foreignIDCheck').change(function () {

        if ($(this).is(':checked')) {
            $('#trn').removeClass('company-trn');
            //alert('hmmm');
            //$('.parisho').find('#parishes').removeClass('parishes');
            $('.parisho').find('#parishes').addClass('foreignID');
            $('#trn').addClass('foreignID');
            $("#company-modal1").find(".parishes").trigger('change');
            $('#trn').trigger('keyup');
        }
        else {
            $('#trn').addClass('company-trn');
            $('#trn').removeClass('foreignID');
           // $('.parisho').find('#parishes').addClass('parishes');
            $('.parisho').find('#parishes').removeClass('foreignID');
            $("#company-modal1").find(".parishes").trigger('change');
            $('#trn').trigger('keyup');
        }
    });

    $('#noTRN').change(function () {
        if ($(this).is(':checked')) {
            $('#trn').removeClass('person-trn');
            //alert('hmmm');
            $('#trn').addClass('blank-trn');
            $('#trn').addClass('read-only');
            $('#trn').prop('readonly', true);

            $('#trn').trigger('keyup');
        }
        else {
            $('#trn').addClass('person-trn');
            $('#trn').removeClass('blank-trn');
            $('#trn').removeClass('read-only');
            $('#trn').prop('readonly', false);
            $('#trn').trigger('keyup');
        }
    });
});
function selectDuplicateComp(e) {

    selectedId = $(e).prop('id');
    $('.dupCompTRNcheck:not(#' + selectedId + ')').prop('checked', false);
}