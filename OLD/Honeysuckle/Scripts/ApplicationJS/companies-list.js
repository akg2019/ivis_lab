﻿var disablecode = "<div id=\"disableImage\" class=\"icon disable company\" title=\"Deactivate Company\"><img src=\"../../Content/more_images/disable_icon.png\" class=\"sprite\"/></div> ";
var enablecode = "<div id=\"enableImage\" class=\"icon enable company\" title=\"Activate Company\"><img src=\"../../Content/more_images/enable_icon.png\" class=\"sprite\"/></div>";
var errormessage = "<div>There was a problem in your request. Please contact your administrator if this error persists</div>";
var cantdelmessage = "<div>This Company cannot be deleted. Please contact your administrator and let him know of this error.</div>";

function redrawCompaniesList() {
    $("#companies .data").each(function (index, value) {
        //remove even/odd from divs
        if ($(this).hasClass("even")) $(this).toggleClass("even");
        if ($(this).hasClass("odd")) $(this).toggleClass("odd");
        //add it back
        if (index % 2 == 0) $(this).toggleClass("even"); //even
        else $(this).toggleClass("odd");
    });
}


$(function () {
    
    $('#companies').perfectScrollbar({
        wheelSpeed: 0.3,
        wheelPropagation: true,
        minScrollbarLength: 10,
        suppressScrollX: true
    });
    
    $("body").on("click", ".company.menu .item", function () {
        var type = "null";
        if ($(this).hasClass("iajid")) type = "iajid";
        else $(".company.menu .iajid").removeClass("ordered");

        if ($(this).hasClass("name")) type = "name";
        else $(".company.menu .name").removeClass("ordered");

        if ($(this).hasClass("type")) type = "type";
        else $(".company.menu .type").removeClass("ordered");

        if ($(this).hasClass("empcount")) type = "empcount";
        else $(".company.menu .empcount").removeClass("ordered");

        if ($(this).hasClass("address")) type = "address";
        else $(".company.menu .address").removeClass("ordered");


        if ($(this).hasClass("short")) type = "short";
        else $(".company.menu .short").removeClass("ordered");

        if ($(this).hasClass("contact")) type = "contact";
        else $(".company.menu .contact").removeClass("ordered");

        if ($(this).hasClass("email")) type = "email";
        else $(".company.menu .email").removeClass("ordered");

        if ($(this).hasClass("phone")) type = "phone";
        else $(".company.menu .phone").removeClass("ordered");


        if (type == "null") alert("error");
        else sortData(type);

        $(this).toggleClass("ordered");
    });

});

function sortData(type) {
    var companies = $(".company.row");
    
    switch (type) {
        case "iajid":
            if ($(".company.menu .iajid").hasClass("ordered")) {
                companies = $(companies).sort(function (a, b) {
                    return $(b).find(".hidiaj").val() - $(a).find(".hidiaj").val();
                });
            }
            else {
                companies = $(companies).sort(function (a, b) {
                    return $(a).find(".hidiaj").val() - $(b).find(".hidiaj").val();
                });
            }
            break;
        case "name":
            if ($(".company.menu .name").hasClass("ordered")) {
                companies = $(companies).sort(function (a, b) {
                    if ($(a).find(".name").html().toLowerCase() < $(b).find(".name").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".name").html().toLowerCase() > $(b).find(".name").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            else {
                companies = $(companies).sort(function (a, b) {
                    if ($(a).find(".name").html().toLowerCase() > $(b).find(".name").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".name").html().toLowerCase() < $(b).find(".name").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            break;
        case "type":
            if ($(".company.menu .type").hasClass("ordered")) {
                companies = $(companies).sort(function (a, b) {
                    if ($(a).find(".type").html().toLowerCase() < $(b).find(".type").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".type").html().toLowerCase() > $(b).find(".type").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            else {
                companies = $(companies).sort(function (a, b) {
                    if ($(a).find(".type").html().toLowerCase() > $(b).find(".type").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".type").html().toLowerCase() < $(b).find(".type").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            break;
        case "empcount":
            if ($(".company.menu .empcount").hasClass("ordered")) {
                companies = $(companies).sort(function (a, b) {
                    return parseInt($(b).find(".empcount").html()) - parseInt($(a).find(".empcount").html());
                });
            }
            else {
                companies = $(companies).sort(function (a, b) {
                    return parseInt($(a).find(".empcount").html()) - parseInt($(b).find(".empcount").html());
                });
            }
            break;
        case "address":
            if ($(".company.menu .address").hasClass("ordered")) {
                companies = $(companies).sort(function (a, b) {
                    if ($(a).find(".address").html().toLowerCase() < $(b).find(".address").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".address").html().toLowerCase() > $(b).find(".address").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            else {
                companies = $(companies).sort(function (a, b) {
                    if ($(a).find(".address").html().toLowerCase() > $(b).find(".address").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".address").html().toLowerCase() < $(b).find(".address").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            break;
        case "short":
            if ($(".company.menu .short").hasClass("ordered")) {
                companies = $(companies).sort(function (a, b) {
                    if ($(a).find(".short").html().toLowerCase() < $(b).find(".short").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".short").html().toLowerCase() > $(b).find(".short").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            else {
                companies = $(companies).sort(function (a, b) {
                    if ($(a).find(".short").html().toLowerCase() > $(b).find(".short").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".short").html().toLowerCase() < $(b).find(".short").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            break;
        case "contact":
            if ($(".company.menu .contact").hasClass("ordered")) {
                companies = $(companies).sort(function (a, b) {
                    if ($(a).find(".contact").html().toLowerCase() < $(b).find(".contact").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".contact").html().toLowerCase() > $(b).find(".contact").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            else {
                companies = $(companies).sort(function (a, b) {
                    if ($(a).find(".contact").html().toLowerCase() > $(b).find(".contact").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".contact").html().toLowerCase() < $(b).find(".contact").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            break;
        case "email":
            if ($(".company.menu .email").hasClass("ordered")) {
                companies = $(companies).sort(function (a, b) {
                    if ($(a).find(".email").html().toLowerCase() < $(b).find(".email").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".email").html().toLowerCase() > $(b).find(".email").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            else {
                companies = $(companies).sort(function (a, b) {
                    if ($(a).find(".email").html().toLowerCase() > $(b).find(".email").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".email").html().toLowerCase() < $(b).find(".email").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            break;
        case "phone":
            if ($(".company.menu .phone").hasClass("ordered")) {
                companies = $(companies).sort(function (a, b) {
                    if ($(a).find(".phone").html().toLowerCase() < $(b).find(".phone").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".phone").html().toLowerCase() > $(b).find(".phone").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            else {
                companies = $(companies).sort(function (a, b) {
                    if ($(a).find(".phone").html().toLowerCase() > $(b).find(".phone").html().toLowerCase()) {
                        return 1;
                    }
                    if ($(a).find(".phone").html().toLowerCase() < $(b).find(".phone").html().toLowerCase()) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
            break;
        default:
            break;
    }

    rePrintCompanyDivs(companies);

}

function rePrintCompanyDivs(companies) {
    var company_list_html = "";
    for (var x = 0; x < companies.length; x++) {
        var comp = companies[x];
        if (($(companies[x]).hasClass("odd") && x % 2 == 1) || ($(companies[x]).hasClass("even") && x % 2 == 0)) company_list_html += comp.outerHTML;
        if (($(companies[x]).hasClass("odd") && x % 2 == 0) || ($(companies[x]).hasClass("even") && x % 2 == 1)) {
            comp = $(comp.outerHTML).toggleClass("odd").toggleClass("even")[0];
            company_list_html += comp.outerHTML;
        }
    }
    $("#companies").html(company_list_html);
}