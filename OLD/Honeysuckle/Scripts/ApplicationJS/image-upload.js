﻿$(function () {

    $(".fileupload").fileupload({
        url: "/TemplateWording/JSONImageUpload/",
        type: "POST",
        dataType: 'json',
        add: function (e, data) {
            if (data.files[0].type == "image/jpg" ||
                data.files[0].type == "image/jpeg" ||
                data.files[0].type == "image/pjpeg" ||
                data.files[0].type == "image/gif" ||
                data.files[0].type == "image/x-png" ||
                data.files[0].type == "image/png") {
                if (data.files[0].size < 1000000) {
                    data.formData = { position: get_position($(e.target)), type: get_type($(e.target)) };
                    data.submit();
                }
                else {
                    $("<div>Please pick an image that is less than 1MB</div>").dialog({
                        title: "Image Size Error!",
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            }
            else {
                $("<div>Please upload only image file types</div>").dialog({
                    title: "File Type Error!",
                    modal: true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }
        },
        done: function (e, data) {
            if (data.result.result) {
                $.ajax({
                    url: '/TemplateWording/Image/' + data.result.id + '?post=' + true,
                    cache: false,
                    success: function (html) {
                        $(e.target).parents(".image-parent").find(".fileupload-parent").hide();
                        $(e.target).parents(".image-parent").find(".image").replaceWith(html);
                        $('<div>The image was successfully uploaded and saved.</div>').dialog({
                            modal: true,
                            title: 'Image Saved!',
                            buttons: {
                                OK: function () {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    },
                    error: function () {
                        $("<div>There was an error. Please refresh the page</div>").insertBefore($(e.target).parents(".image-parent").find(".fileupload-parent"));
                    }
                });
            }
            else $("<div>There was an error. Please refresh the page</div>").insertBefore($(e.target).parents(".image-parent").find(".fileupload-parent"));
        }
    });

    $("body").on("click", ".fileupload-parent .click-this-to-work", function () {
        $(this).parent().find(".fileupload").trigger("click");
    });

    $(".fileupload-parent.hide").hide();

});

function get_position(e) {
    if ($(e).hasClass("logo")) return "logo";
    if ($(e).hasClass("header")) return "header";
    if ($(e).hasClass("footer")) return "footer";
    return "bad";
}

function get_type(e) {
    if ($(e).hasClass("certificate")) return "certificate";
    if ($(e).hasClass("covernote")) return "cover note";
    return "bad";
}
