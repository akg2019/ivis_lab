﻿   /* $(function () {
        $("#addemail").click(function () {
            $.ajax({
                url: this.href,
                cache: false,
                success: function (html) { $("#email-container").append(html); }
            });
            return false;
        });
    });*/

    $(function () {
        $(document).on("click", "a.rmemail", function () {

        var deleteItem =  $(this).parents("div.newemail:first");

        $("<div>You are about to delete this email. Are you sure you want to do this? </div>").dialog({
                modal:true,
                title: 'Alert!',
                buttons: {
                  Ok: function () {
                      deleteItem.remove();
                      $(this).dialog("close");
                      $(this).dialog().dialog("destroy");
                    },
                  Cancel: function () {
                     $(this).dialog("close");
                     $(this).dialog().dialog("destroy");
                  }
                }
            });

        });
    });

    var checked = false;

    $(document).ready(function () {
        $("body").on("click", ".emailprimary", function (event) {
            if ($("#" + event.target.id).is(':checked')) {
                checked = true;
            }
            else {
                checked = false;
            }
            $(".emailprimary").prop('checked', false);
            $("#" + event.target.id).prop('checked', checked);
        });
    });


    $(function () {
        $("#addNewEmail").click(function () {
            if ($(".email-container").length > 0) {
                $.ajax({
                    url: '/Company/Email/',
                    cache: false,
                    success: function (html) {

                        $(".email-container").append(html);
                    }
                });
                return false;
            }
        });
    });

    $(function () {
        $(document).on("click", ".removeemail", function () {

            var deleteItem = $(this).parents("div.items:first");

            $("<div>You are about to delete this element. Are you sure you want to do this? </div>").dialog({
                modal:true,
                title: 'Alert!',
                buttons: {
                    Ok: function () {
                        deleteItem.remove();
                        $(this).dialog("close");
                        $(this).dialog().dialog("destroy");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                        $(this).dialog().dialog("destroy");
                    }
                }
            });
        });
    });



    $(document).on("click", "#addNewEmail", function (e) {
        
            var Emails = $(e.target).parents(".emails").find('#email-container');
      if (Emails.length > 0) {
            $.ajax({
                url: '/Company/Email/',
                cache: false,
                success: function (html) {
                    Emails.append(html);
                }
            });
            return false;
        }
    });