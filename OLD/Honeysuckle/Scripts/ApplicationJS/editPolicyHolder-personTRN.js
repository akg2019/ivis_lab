﻿var TRNmessage = "<div>You are about to edit the TRN of this user. Are you certain you want to do this?</div>";
var TRNbadmessage = "<div>The TRN you have selected is already taken (Please enter a valid TRN) and it must have exactly nine digits</div>";
var trnEdit = false;
var origTRN = "";
var trn = "";
var selectedId = "";

$(function () {


    $('#edit-trn').click(function () {
        origTRN = $("#trn-editPolHol.person-trn, #trn-editPolHol.foreignID").val();
        $(TRNmessage).dialog({
            title: 'Edit TRN Dialog',
            resizable: false,
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    $("#trn-editPolHol.person-trn, #trn-editPolHol.foreignID").removeClass('read-only');
                    $("#trn-editPolHol.person-trn, #trn-editPolHol.foreignID").prop('disabled', false);
                    $("#trn-editPolHol").focus();
                    origTRN = $("#trn-editPolHol.person-trn, #trn-editPolHol.foreignID").val().trim();
                    $('#edit-trn').prop('disabled', true);
                    trnEdit = true;
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });

    });

    $("#trn-editPolHol.person-trn, #trn-editPolHol.foreignID").focus(function () { trnEdit = true });


    $("#trn-editPolHol.person-trn, #trn-editPolHol.foreignID").blur(function () {
        trn = $("#trn-editPolHol.person-trn, #trn-editPolHol.foreignID").val().trim();
        if (trnEdit) {
            trnEdit = false;

            if (origTRN != $("#trn-editPolHol.person-trn, #trn-editPolHol.foreignID").val().trim()) {
                if($("#trn-editPolHol.person-trn, #trn-editPolHol.foreignID").val().trim().length > 0)
                    $("body").append('<div class="ui-widget-overlay ui-front temp"><div class="tempmessage"><img src="../../Content/more_images/loading.gif" /><span>Processing Data</span></div></div>');
                // alert('hit');
                $.ajax({
                    url: '/Vehicle/GetUserByTRN/?id=' + $("#trn-editPolHol.person-trn, #trn-editPolHol.foreignID").val().trim() + '&insured=true',
                    cache: false,
                    success: function (data) {
                        if ($("#trn-editPolHol.person-trn, #trn-editPolHol.foreignID").val().trim().length > 0)
                            $(".ui-front.temp").remove();
                        if (data.result) {

                            if (data.tajerror) {

                                $("<div>TAJ service is down</div>").dialog({
                                    modal: true,
                                    title: 'Notice',
                                    buttons: {
                                        Ok: function () {
                                            $(this).dialog("close");
                                        }
                                    }
                                });
                            }
                            var buttons = {
                                OK: function () {

                                    if ($('.dupTRNcheck:checked').length <= 0 && $('#duplicatePartial').length > 0) {
                                        alert('Please select a record or cancel');
                                        return false;
                                    }

                                    if ($('#duplicatePartial').length > 0) {

                                        $.ajax({
                                            type: "POST",
                                            traditional: true,
                                            url: "/People/GetFullPersonDetails/?personId=" + selectedId,
                                            async: false,
                                            dataType: "json",
                                            success: function (rdata) {
                                                data = rdata;
                                            },
                                            error:function(){
                                                alert('Error receiving data');
                                            }
                                        });
                                    }




                                    $("#InformationDialog").html("");
                                    $("#InformationDialog").dialog("destroy");

                                    AddPersonSelected(data.personid, 1);

                                    $("#insured-modal1").html("");
                                    $("#insured-modal1").dialog("close");

//                                    $('#fname').val(data.fname);
//                                    $('#mname').val(data.mname);
//                                    $('#lname').val(data.lname);

//                                    $('#RoadNumber').val(data.roadno);
//                                    $('#roadname').val(data.roadname);
//                                    $('#roadtype').val(data.roadtype);
//                                    $('#aptNumber').val(data.aptNumber);

//                                    $('#city').val(data.city);
//                                    $('.parishm #parishes').val(data.parishid);
//                                    $('.parishm #parish').val(data.parishid);
//                                    $('#country').val(data.country);

//                                    if (data.longAddressUsed) $('#longAddress').val(data.longAddress);

//                                    $('input').trigger('change');
//                                    $('.parishm select').trigger('change');

//                                    $("#trn-editPolHol").prop('readonly', true);
//                                    $('#fname').prop('readonly', true);
//                                    $('#mname').prop('readonly', true);
//                                    $('#lname').prop('readonly', true);

//                                    $("#trn-editPolHol").addClass('read-only');
//                                    $('#fname').addClass('read-only');
//                                    $('#mname').addClass('read-only');
//                                    $('#lname').addClass('read-only');

//                                    $('#RoadNumber').prop('readonly', true);
//                                    $('#roadname').prop('readonly', true);
//                                    $('#roadtype').prop('readonly', true);
//                                    $('#aptNumber').prop('readonly', true);

//                                    $('#city').prop('readonly', true);
//                                    $('.parishm #parishes').prop('readonly', true);
//                                    $('#country').prop('readonly', true);

//                                    $('#RoadNumber').addClass('read-only');
//                                    $('#roadname').addClass('read-only');
//                                    $('#roadtype').addClass('read-only');
//                                    $('#aptNumber').addClass('read-only');

//                                    $('#city').addClass('read-only');
//                                    $('.parishm #parishes').addClass('read-only');
//                                    $('#country').addClass('read-only');


//                                    $(this).dialog("close");
//                                    $("#InformationDialog").html('');

                                },
                                Edit: function () {

                                    if ($('.dupTRNcheck:checked').length <= 0 && $('#duplicatePartial').length > 0) {
                                        alert('Please select a record or cancel');
                                        return false;
                                    }

                                    if ($('#duplicatePartial').length > 0) {

                                        $.ajax({
                                            type: "POST",
                                            traditional: true,
                                            url: "/People/GetFullPersonDetails/?personId=" + selectedId,
                                            async: false,
                                            dataType: "json",
                                            success: function (rdata) {
                                                data = rdata;
                                            },
                                            error: function () {
                                                alert('Error receiving data');
                                            }
                                        });
                                    }

                                    $('#fname').val(data.fname);
                                    $('#mname').val(data.mname);
                                    $('#lname').val(data.lname);

                                    $('#RoadNumber').val(data.roadno);
                                    $('#roadname').val(data.roadname);
                                    $('#roadtype').val(data.roadtype);
                                    $('#aptNumber').val(data.aptNumber);

                                    $('#city').val(data.city);
                                    $('.parishm #parishes').val(data.parishid);
                                    $('.parishm #parish').val(data.parishid);
                                    $('#country').val(data.country);

                                    if (data.longAddressUsed) $('#longAddress').val(data.longAddress);

                                    $('input').trigger('change');
                                    $('.parishm select').trigger('change');



                                    $(this).dialog("close");
                                    $("#InformationDialog").html('');
                                },
                                Cancel: function () {
                                    $(this).dialog("close");
                                    $("#InformationDialog").html('');
                                    //$("#trn-editPolHol").val('');
                                    //$("#trn-editPolHol").focus();
                                    $("#trn-editPolHol.person-trn, #trn-editPolHol.foreignID").trigger('change');
                                    trnEdit = true;
                                }
                            };
                            //                            if (data.duplicate) buttons['View Duplicates'] = function () {
                            //                                $('#duplicates').load('/People/GetPersonDuplicates/?trn=' + trn);
                            //                                $('#duplicates').dialog({
                            //                                    modal: true,
                            //                                    width: 470,
                            //                                    height: 380,
                            //                                    title: 'Duplicates',
                            //                                    buttons: {
                            //                                        OK: function(){
                            //                                            $('#duplicates').html('');
                            //                                            $(this).dialog('close');
                            //                                            $(this).dialog('destroy');
                            //                                        }
                            //                                    }
                            //                                });
                            //                                
                            //                            };
                            $('#InformationDialog').dialog({

                                resizable: false,
                                modal: true,
                                autoOpen: false,
                                width: 600,
                                title: "Person Exists",
                                height: 380,
                                buttons: buttons

                            });
                            if (data.duplicate) {
                                $("#InformationDialog").load('/People/GetPersonDuplicates/?trn=' + trn, function (value) { }).dialog('open');
                            }
                            else {
                                $("#InformationDialog").load('/People/FullPersonDetails/' + data.personid + "?create=" + true + "&taj=" + data.taj + "&duplicates=" + data.duplicate, function (value) { }).dialog('open');
                            }

                            //                            $("#InformationDialog").load('/People/FullPersonDetails/' + data.personid + "?create=" + true + "&taj=" + data.taj + "&duplicates=" + data.duplicate, function (value) { }).dialog('open');
                        }
                        else if (!data.error && data.tajerror) {
                            $("<div>TAJ service is down</div>").dialog({
                                modal: true,
                                title: 'Notice',
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        }
                    },
                    error: function (data) {
                        // $("#trn-editPolHol.person-trn, #trn-editPolHol.foreignID").val('');
                        //alert('Error validating data');
                    }
                });
            } //else alert('yoooo');
        }

        if (origTRN != $("#trn-editPolHol.person-trn, #trn-editPolHol.foreignID").val().trim()) $('#trnChange').val(true); else $('#trnChange').val(false);

    });

});

$(function () {

    $('#foreignIDCheck').change(function () {

        if ($(this).is(':checked')) {
            $('#trn-editPolHol').removeClass('person-trn');
            //alert('hmmm');
            $('#trn-editPolHol').addClass('foreignID');
            $('.parishm').find('#parishes').removeClass('parishes');
            $('.parishm').find('#parishes').addClass('foreignID');
            $('#trn-editPolHol').trigger('keyup');
            $('.parishm').find('#parishes').trigger('change');
        }
        else {
            $('#trn-editPolHol').addClass('person-trn');
            $('#trn-editPolHol').removeClass('foreignID');
            $('.parishm').find('#parishes').addClass('parishes');
            $('.parishm').find('#parishes').removeClass('foreignID');
            $('#trn-editPolHol').trigger('keyup');
            $('.parishm').find('#parishes').trigger('change');
        }
    });

    $('#noTRN').change(function () {
        if ($(this).is(':checked')) {
            $('#trn-editPolHol').removeClass('person-trn');
            //alert('hmmm');
            $('#trn-editPolHol').addClass('blank-trn');
            $('#trn-editPolHol').addClass('read-only');
            $('#trn-editPolHol').prop('readonly', true);

            $('#trn-editPolHol').trigger('keyup');
        }
        else {
            $('#trn-editPolHol').addClass('person-trn');
            $('#trn-editPolHol').removeClass('blank-trn');
            $('#trn-editPolHol').removeClass('read-only');
            $('#trn-editPolHol').prop('readonly', false);
            $('#trn-editPolHol').trigger('keyup');
        }
    });
});

function selectDuplicateVeh(e) {

   selectedId = $(e).prop('id');
   $('.dupTRNcheck:not(#'+ selectedId +')').prop('checked',false);
}

//$('.parisho').find('#parishes').removeClass('parishes');
//$('.parisho').find('#parishes').addClass('foreignID');
