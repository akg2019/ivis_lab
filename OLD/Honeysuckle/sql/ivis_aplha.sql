USE [IVIS_demo]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Address](
	[AID] [int] IDENTITY(1,1) NOT NULL,
	[RoadName] [int] NULL,
	[RoadType] [int] NULL,
	[ZipCode] [int] NULL,
	[City] [int] NULL,
	[Country] [int] NULL,
	[RoadNumber] [varchar](50) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[LastModifiedBy] [int] NOT NULL,
	[Parish] [int] NULL,
	[ParishMap] [int] NULL,
	[AptNumber] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[AID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[AID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Alert]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Alert](
	[AID] [int] IDENTITY(1,1) NOT NULL,
	[AlertDesc] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[AID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[AID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AuthorizedDrivers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AuthorizedDrivers](
	[EDID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleID] [int] NOT NULL,
	[DriverID] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (''),
	[LastModifiedBy] [int] NOT NULL,
	[Relation] [varchar](250) NULL,
	[PolicyID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[EDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CertIntel]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CertIntel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[date] [datetime] NOT NULL DEFAULT (getdate()),
	[certID] [int] NOT NULL,
	[policyID] [int] NOT NULL,
	[riskID] [int] NOT NULL,
	[userID] [int] NOT NULL,
	[companyID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cities]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cities](
	[CID] [int] IDENTITY(1,1) NOT NULL,
	[City] [varchar](250) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[LastModifiedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[CID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Company]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Company](
	[CompanyID] [int] IDENTITY(1,1) NOT NULL,
	[IAJID] [varchar](max) NULL,
	[CompanyName] [varchar](max) NULL,
	[faultyloginlimit] [int] NULL,
	[Type] [int] NULL,
	[CompanyAddress] [int] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__Company__Created__5070F446]  DEFAULT (getdate()),
	[LastModifiedBy] [int] NOT NULL,
	[TRN] [varchar](14) NOT NULL,
	[CoverNotePrintLimit] [int] NOT NULL CONSTRAINT [DF__Company__CoverNo__5165187F]  DEFAULT ((0)),
	[Enabled] [bit] NOT NULL CONSTRAINT [DF__Company__Enabled__62CF9BA3]  DEFAULT ((0)),
	[EDIID] [varchar](max) NULL,
	[InsurerCode] [varchar](20) NULL,
	[CompShortening] [varchar](20) NULL,
	[iaj] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [PK__Company__2D971C4C0DC89152] PRIMARY KEY CLUSTERED 
(
	[CompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [TRN_Uniq] UNIQUE NONCLUSTERED 
(
	[TRN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__Company__2D971C4DC661DE3D] UNIQUE NONCLUSTERED 
(
	[CompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CompanyCoverNoteCount]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompanyCoverNoteCount](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[count] [int] NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[CompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CompanyToEmails]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompanyToEmails](
	[CTEID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[EmailID] [int] NOT NULL,
	[PrimaryEmail] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastModifiedBy] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CompanyToPhones]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompanyToPhones](
	[CTPID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[PhoneID] [int] NOT NULL,
	[NumberType] [int] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastModifiedBy] [int] NOT NULL,
	[Primary] [bit] NULL,
	[UserID] [int] NULL,
 CONSTRAINT [PK__CompanyT__D023105BC1FCE9CC] PRIMARY KEY CLUSTERED 
(
	[CTPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__CompanyT__D023105A6989792A] UNIQUE NONCLUSTERED 
(
	[CTPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CompanyTypes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompanyTypes](
	[ID] [int] NOT NULL,
	[Type] [varchar](50) NULL,
 CONSTRAINT [PK__CompanyT__3214EC27673E6981] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__CompanyT__3214EC26CB8AB821] UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactGroups]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactGroups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[Owner] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactUserAssoc]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactUserAssoc](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[contactgroup] [int] NOT NULL,
	[UID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Countries]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Countries](
	[CID] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [varchar](250) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[LastModifiedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[CID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Drivers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Drivers](
	[DriverID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (''),
	[LastModifiedBy] [int] NOT NULL,
	[LicenseNo] [varchar](100) NULL,
	[DateLicenseIssued] [date] NULL,
	[DateLicenseExpires] [date] NULL,
	[Class] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[DriverID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[PersonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[DriverID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmailFailures]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailFailures](
	[AuditID] [int] IDENTITY(1,1) NOT NULL,
	[AuditDesc] [text] NULL,
	[AuditType] [varchar](250) NULL,
	[UserLogged] [int] NOT NULL,
	[TimeCreated] [datetime] NOT NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[AuditID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[AuditID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Emails]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Emails](
	[EID] [int] IDENTITY(1,1) NOT NULL,
	[email] [varchar](250) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (''),
	[LastModifiedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[EID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Employees]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[EmployeeID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NOT NULL,
	[CompanyID] [int] NOT NULL,
	[Active] [bit] NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Enquiries]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Enquiries](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[license] [varchar](max) NULL,
	[chassis] [varchar](max) NULL,
	[RequestedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[risk] [int] NULL,
	[policy] [int] NULL,
	[valid] [bit] NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[error]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[error](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[error_type] [varchar](max) NOT NULL,
	[edi_system_id] [varchar](max) NOT NULL,
	[reqid] [int] NOT NULL,
	[subid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ExceptedDrivers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExceptedDrivers](
	[EDID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleID] [int] NOT NULL,
	[DriverID] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (''),
	[LastModifiedBy] [int] NOT NULL,
	[Relation] [varchar](250) NULL,
	[PolicyID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[EDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ExcludedDrivers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExcludedDrivers](
	[EDID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleID] [int] NOT NULL,
	[DriverID] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (''),
	[LastModifiedBy] [int] NOT NULL,
	[Relation] [varchar](250) NULL,
	[PolicyID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[EDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GeneratedCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GeneratedCertificate](
	[GCID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleCertificateID] [int] NOT NULL,
	[VehicleYear] [varchar](max) NULL,
	[VehicleMake] [varchar](max) NULL,
	[VehicleRegNo] [varchar](max) NULL,
	[VehExtension] [varchar](max) NULL,
	[MainInsured] [varchar](max) NULL,
	[VehMainDriver] [varchar](max) NULL,
	[VehDrivers] [varchar](max) NULL,
	[VehicleDesc] [varchar](max) NULL,
	[LimitsOfUse] [varchar](max) NULL,
	[PolicyCover] [varchar](max) NULL,
	[VehModelType] [varchar](max) NULL,
	[VehicleModel] [varchar](max) NULL,
	[Usage] [varchar](max) NULL,
	[PolicyHolderAddress] [varchar](max) NULL,
	[CovernoteNo] [varchar](max) NULL,
	[Period] [varchar](max) NULL,
	[effectiveTime] [varchar](max) NULL,
	[expiryTime] [varchar](max) NULL,
	[effectivedate] [varchar](max) NULL,
	[expirydate] [varchar](max) NULL,
	[endorsementNo] [varchar](max) NULL,
	[bodyType] [varchar](max) NULL,
	[seating] [varchar](max) NULL,
	[chassisNo] [varchar](max) NULL,
	[certificateType] [varchar](max) NULL,
	[certitificateNo] [varchar](max) NULL,
	[engineNo] [varchar](max) NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL CONSTRAINT [DF_GenCert_CreatedOn]  DEFAULT (getdate()),
	[authorizeddrivers] [varchar](max) NULL,
	[PolicyNo] [varchar](max) NULL,
	[mortgagees] [varchar](max) NULL,
	[HPCC] [varchar](250) NULL,
	[CertificateCode] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GeneratedCoverNotes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GeneratedCoverNotes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[VehicleYear] [varchar](max) NULL,
	[VehicleMake] [varchar](max) NULL,
	[VehicleRegNo] [varchar](max) NULL,
	[VehExtension] [varchar](max) NULL,
	[MainInsured] [varchar](max) NULL,
	[VehMainDriver] [varchar](max) NULL,
	[VehDrivers] [varchar](max) NULL,
	[VehicleDesc] [varchar](max) NULL,
	[LimitsOfUse] [varchar](max) NULL,
	[PolicyCover] [varchar](max) NULL,
	[VehModelType] [varchar](max) NULL,
	[VehicleModel] [varchar](max) NULL,
	[VehicleCoverNoteID] [int] NOT NULL,
	[authorizeddrivers] [varchar](max) NULL,
	[mortgagees] [varchar](max) NULL,
	[Usage] [varchar](max) NULL,
	[PolicyNo] [varchar](max) NULL,
	[PolicyHolderAddress] [varchar](max) NULL,
	[CovernoteNo] [varchar](max) NULL,
	[Period] [varchar](max) NULL,
	[effectiveTime] [varchar](max) NULL,
	[expiryTime] [varchar](max) NULL,
	[effectivedate] [varchar](max) NULL,
	[expirydate] [varchar](max) NULL,
	[endorsementNo] [varchar](max) NULL,
	[bodyType] [varchar](max) NULL,
	[seating] [varchar](max) NULL,
	[chassisNo] [varchar](max) NULL,
	[certificateType] [varchar](max) NULL,
	[certitificateNo] [varchar](max) NULL,
	[engineNo] [varchar](max) NULL,
	[companyCreatedBy] [varchar](max) NULL,
	[HPCC] [varchar](250) NULL,
	[CertificateCode] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InsurerToIntermediary]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InsurerToIntermediary](
	[CTID] [int] IDENTITY(1,1) NOT NULL,
	[Insurer] [int] NOT NULL,
	[Intermediary] [int] NOT NULL,
	[Certificate] [bit] NOT NULL DEFAULT ((0)),
	[CoverNote] [bit] NOT NULL DEFAULT ((0)),
	[PrintLimit] [int] NOT NULL DEFAULT ((0)),
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[LastModifiedBy] [int] NOT NULL,
	[enabled] [bit] NULL DEFAULT ((1)),
	[keep] [bit] NULL DEFAULT ((0))
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[JsonAudit]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JsonAudit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedTimeStamp] [datetime] NOT NULL DEFAULT (getdate()),
	[CreatedBy] [int] NOT NULL,
	[method] [varchar](50) NULL,
	[json] [nvarchar](max) NULL,
	[failure] [bit] NOT NULL DEFAULT ((0)),
	[key] [uniqueidentifier] NULL,
	[inprogress] [bit] NOT NULL DEFAULT ((1)),
	[completed] [bit] NOT NULL DEFAULT ((0)),
	[processedjson] [nvarchar](max) NULL,
	[CompletedTimeStamp] [datetime] NULL,
	[count] [int] NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KPI]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mode] [int] NOT NULL,
	[type] [int] NOT NULL,
	[kpi] [int] NOT NULL,
	[company] [int] NOT NULL,
	[datemode] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[KPIDateMode]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KPIDateMode](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[datemode] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KPImode]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KPImode](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mode] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KPItype]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KPItype](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[type] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoggedOutLog]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoggedOutLog](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
	[MarkedTime] [datetime] NOT NULL,
 CONSTRAINT [PK_LoggedOutLog] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MappingTable]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MappingTable](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Table] [varchar](max) NULL,
	[RefID] [int] NULL,
	[data] [varchar](max) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[lock] [bit] NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mortgagees]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mortgagees](
	[MID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Mort_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [int] NOT NULL,
	[Address] [int] NULL,
	[Mortgagee] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[MID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MortgageesToEmail]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MortgageesToEmail](
	[MTPID] [int] IDENTITY(1,1) NOT NULL,
	[MID] [int] NOT NULL,
	[EID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MTPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[MTPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MortgageesToPhone]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MortgageesToPhone](
	[MTPID] [int] IDENTITY(1,1) NOT NULL,
	[MID] [int] NOT NULL,
	[PID] [int] NOT NULL,
	[PhoneType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MTPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[MTPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Notifications]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notifications](
	[NID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[NoteFrom] [int] NOT NULL,
	[Heading] [text] NULL,
	[Content] [text] NULL,
	[Seen] [bit] NOT NULL DEFAULT ((0)),
	[Read] [bit] NOT NULL DEFAULT ((0)),
	[Deleted] [bit] NOT NULL DEFAULT ((0)),
	[New] [bit] NULL DEFAULT ((0)),
	[ParentMessage] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[NID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[NID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NotificationsTo]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationsTo](
	[NTID] [int] IDENTITY(1,1) NOT NULL,
	[Notification] [int] NOT NULL,
	[UID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[NTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Parishes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Parishes](
	[PID] [int] IDENTITY(1,1) NOT NULL,
	[Parish] [varchar](250) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[LastModifiedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Parish] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[PID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[People]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[People](
	[PeopleID] [int] IDENTITY(1,1) NOT NULL,
	[FName] [varchar](250) NULL,
	[MName] [varchar](250) NULL,
	[LName] [varchar](250) NULL,
	[Address] [int] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (''),
	[LastModifiedBy] [int] NOT NULL,
	[TRN] [varchar](15) NOT NULL,
	[DOB] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[PeopleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [TRN_Uniq_People] UNIQUE NONCLUSTERED 
(
	[TRN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[PeopleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PeopleToEmails]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PeopleToEmails](
	[PTEID] [int] IDENTITY(1,1) NOT NULL,
	[PeopleID] [int] NOT NULL,
	[EmailID] [int] NOT NULL,
	[Primary] [bit] NULL CONSTRAINT [FK_PeopleToEmails_Primary_Default]  DEFAULT ((0)),
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (''),
	[LastModifiedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PTEID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[PTEID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PeopleToPhones]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PeopleToPhones](
	[PTPID] [int] IDENTITY(1,1) NOT NULL,
	[PeopleID] [int] NOT NULL,
	[PhoneID] [int] NOT NULL,
	[NumberType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PTPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[PTPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PermissionGroupAssoc]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionGroupAssoc](
	[PGAID] [int] IDENTITY(1,1) NOT NULL,
	[UPID] [int] NOT NULL,
	[UGID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PGAID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[PGAID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PhoneNumbers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhoneNumbers](
	[PID] [int] IDENTITY(1,1) NOT NULL,
	[PhoneNo] [varchar](250) NULL,
	[Ext] [varchar](250) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK__PhoneNum__C577552093425216] PRIMARY KEY CLUSTERED 
(
	[PID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__PhoneNum__C5775521D4B64707] UNIQUE NONCLUSTERED 
(
	[PID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhoneType]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhoneType](
	[PTID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[PTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Policies]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Policies](
	[PolicyID] [int] IDENTITY(1,1) NOT NULL,
	[EDIID] [varchar](max) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CompanyID] [int] NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NOT NULL,
	[Policyno] [varchar](50) NOT NULL,
	[PolicyCover] [int] NULL,
	[InsuredType] [int] NOT NULL,
	[LastModifiedBy] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[Cancelled] [bit] NOT NULL,
	[CancelledBy] [int] NULL,
	[Deleted] [bit] NOT NULL DEFAULT ((0)),
	[Scheme] [bit] NULL,
	[imported] [bit] NOT NULL DEFAULT ((0)),
	[locked] [bit] NOT NULL DEFAULT ((0)),
	[CompanyCreatedBy] [int] NOT NULL,
	[BillingAddress] [int] NOT NULL,
	[MailingAddress] [int] NOT NULL,
	[billing_address_short] [varchar](100) NULL,
	[mailing_address_short] [varchar](100) NULL,
 CONSTRAINT [PK__Policies__2E13394401B3C4E5] PRIMARY KEY CLUSTERED 
(
	[PolicyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__Policies__2E13394535FA60DC] UNIQUE NONCLUSTERED 
(
	[PolicyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PolicyCovers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PolicyCovers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Cover] [varchar](250) NOT NULL,
	[IsCommercialBusiness] [bit] NULL DEFAULT ((0)),
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastModifiedBy] [int] NOT NULL,
	[CompanyID] [int] NOT NULL,
	[Deleted] [bit] NOT NULL DEFAULT ((0)),
	[Prefix] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PolicyInsured]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PolicyInsured](
	[PolicyInsuredID] [int] IDENTITY(1,1) NOT NULL,
	[EDIID] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[PersonID] [int] NULL,
	[PolicyID] [int] NOT NULL,
	[CompanyID] [int] NULL,
	[CreatedBy] [int] NOT NULL,
	[LastModifiedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PolicyInsuredID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[PolicyInsuredID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PolicyInsuredType]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PolicyInsuredType](
	[PITID] [int] IDENTITY(1,1) NOT NULL,
	[InsuredType] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[PITID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[PITID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RequireAddressPerCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequireAddressPerCompany](
	[RAPCID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[RoadNumber] [bit] NOT NULL,
	[RoadName] [bit] NOT NULL,
	[RoadType] [bit] NOT NULL,
	[ZipCode] [bit] NOT NULL,
	[Country] [bit] NOT NULL,
	[City] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastModifiedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RAPCID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[RAPCID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[CompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoadName]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RoadName](
	[RID] [int] IDENTITY(1,1) NOT NULL,
	[RoadName] [varchar](250) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[LastModifiedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RoadType]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RoadType](
	[RTID] [int] IDENTITY(1,1) NOT NULL,
	[RoadType] [varchar](50) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[LastModifiedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[RTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RSAKeys]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RSAKeys](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[session] [varchar](max) NOT NULL,
	[request] [varchar](max) NULL,
	[uid] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SuperGroupMapping]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SuperGroupMapping](
	[SGID] [int] IDENTITY(1,1) NOT NULL,
	[PGID] [int] NOT NULL,
	[CGID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SGID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[SGID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemAuditLog]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SystemAuditLog](
	[AuditID] [int] IDENTITY(1,1) NOT NULL,
	[Action] [text] NULL,
	[ActionUserID] [int] NULL,
	[ActionTimeStamp] [datetime] NOT NULL CONSTRAINT [DF_ActionTimeStamp]  DEFAULT (getdate()),
	[ActionDetails] [text] NULL,
	[Location] [varchar](max) NULL,
	[RecordID] [int] NULL,
	[OrigData] [text] NULL,
	[NewData] [text] NULL,
	[AuditType] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[AuditID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[AuditID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SystemLoginLimit]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemLoginLimit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[faultyloginlimit] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemLogonLimitTime]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemLogonLimitTime](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[hours] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TemplateImages]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TemplateImages](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[template_type] [varchar](50) NOT NULL,
	[image_position] [varchar](50) NOT NULL,
	[image_location] [varchar](250) NOT NULL,
	[companyid] [int] NOT NULL,
	[userid] [int] NOT NULL,
	[createdon] [datetime] NOT NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransmissionTypeLookUp]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransmissionTypeLookUp](
	[TransmisionTypeID] [int] IDENTITY(1,1) NOT NULL,
	[TransmissionType] [varchar](50) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (''),
	[LastModifiedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TransmisionTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[TransmissionType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[TransmisionTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usages]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usages](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[usage] [varchar](max) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UsagesToCovers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsagesToCovers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CoverID] [int] NOT NULL,
	[UsageID] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserGroups]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserGroups](
	[UGID] [int] IDENTITY(1,1) NOT NULL,
	[UserGroup] [varchar](250) NOT NULL,
	[SuperGroup] [bit] NULL DEFAULT ((0)),
	[CreatedAt] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[LastModifiedBy] [int] NOT NULL,
	[Company] [int] NULL,
	[system] [bit] NOT NULL DEFAULT ((0)),
	[stock] [bit] NULL DEFAULT ((0)),
	[CopyFrom] [int] NULL,
	[administrative] [bit] NULL DEFAULT ((0)),
UNIQUE NONCLUSTERED 
(
	[UGID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserGroupUserAssoc]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroupUserAssoc](
	[UGUAID] [int] IDENTITY(1,1) NOT NULL,
	[UGID] [int] NOT NULL,
	[UID] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[LastModifiedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UGUAID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[UGUAID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserPermissions]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserPermissions](
	[UPID] [int] IDENTITY(1,1) NOT NULL,
	[Table] [varchar](255) NOT NULL,
	[Action] [varchar](50) NOT NULL,
	[system] [bit] NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[UPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[UPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](55) NOT NULL,
	[password] [varchar](max) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[reset] [bit] NOT NULL CONSTRAINT [DF_Reset_Users]  DEFAULT ((0)),
	[question] [varchar](max) NULL,
	[response] [varchar](max) NULL,
	[faultylogins] [int] NOT NULL CONSTRAINT [DF_Flogins_Users]  DEFAULT ((0)),
	[lastloginattempt] [datetime] NULL,
	[password_reset_force] [bit] NOT NULL DEFAULT ('false'),
	[passwordresetrequesttime] [datetime] NULL DEFAULT (getdate()),
	[active] [bit] NOT NULL CONSTRAINT [User_ative_default]  DEFAULT ((0)),
	[firstlogin] [bit] NOT NULL CONSTRAINT [User_firstlogin_default]  DEFAULT ((1)),
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[CreatedBy] [int] NOT NULL,
	[FirstLoginDateTime] [datetime] NOT NULL DEFAULT (getdate()),
	[resetguid] [uniqueidentifier] NULL,
	[LastModifiedBy] [int] NOT NULL DEFAULT ((0)),
	[LoggedIn] [bit] NOT NULL DEFAULT ((0)),
	[LastSuccessful] [datetime] NULL,
	[LoginGUID] [uniqueidentifier] NULL,
	[Filter] [bit] NOT NULL DEFAULT ((0)),
	[Deleted] [bit] NULL CONSTRAINT [DF_deleted]  DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VehicleCertificates]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VehicleCertificates](
	[VehicleCertificateID] [int] IDENTITY(1,1) NOT NULL,
	[EDIID] [varchar](max) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CompanyID] [int] NULL,
	[EffectiveDate] [date] NULL,
	[ExpiryDate] [date] NULL,
	[DateFirstPrinted] [date] NULL,
	[LastPrintedOn] [datetime] NULL,
	[EndorsementNo] [varchar](max) NULL,
	[PrintedPaperNo] [varchar](100) NULL,
	[RiskItemNo] [int] NOT NULL,
	[approvedon] [datetime] NULL,
	[approvedby] [int] NULL,
	[MarksRegNo] [varchar](200) NULL,
	[Cancelled] [bit] NULL CONSTRAINT [DEF_can]  DEFAULT ((0)),
	[CancelledBy] [int] NULL,
	[CancelledOn] [datetime] NULL,
	[Usage] [varchar](max) NULL,
	[Scheme] [varchar](max) NULL,
	[LastModifiedBy] [int] NOT NULL,
	[approved] [bit] NOT NULL,
	[PrintCount] [int] NULL,
	[LastPrintedBy] [int] NULL,
	[CertificateType] [varchar](250) NULL,
	[InsuredCode] [varchar](250) NULL,
	[ExtensionCode] [int] NULL,
	[UniqueNum] [int] NULL,
	[PolicyID] [int] NOT NULL,
	[Printed] [bit] NOT NULL,
	[CertificateNo] [varchar](100) NULL,
	[CancelledReason] [varchar](200) NULL,
	[Generated] [bit] NOT NULL,
	[WordingTemplate] [int] NULL,
	[RequestPrint] [bit] NULL,
	[PrintLimitOverride] [bit] NOT NULL CONSTRAINT [DF_VehicleCertificates_PrintLimitOverride]  DEFAULT ((0)),
	[PrintCountInsurer] [int] NULL,
	[LastPrintedByFlat] [varchar](max) NULL,
	[FirstPrintedByFlat] [varchar](max) NULL,
 CONSTRAINT [PK__VehicleC__6EDAC6860BC5C063] PRIMARY KEY CLUSTERED 
(
	[VehicleCertificateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__VehicleC__6EDAC687EB0BE420] UNIQUE NONCLUSTERED 
(
	[VehicleCertificateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VehicleCoverNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VehicleCoverNote](
	[VehicleCoverNoteID] [int] IDENTITY(1,1) NOT NULL,
	[EDIID] [varchar](max) NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [int] NOT NULL,
	[CompanyID] [int] NULL,
	[Alterations] [text] NULL,
	[Cancelled] [bit] NOT NULL CONSTRAINT [DF_Cancelled]  DEFAULT ((0)),
	[CancelledOn] [datetime] NULL,
	[CancelledBy] [int] NULL,
	[DateFirstPrinted] [datetime] NULL,
	[LastPrinted] [datetime] NULL,
	[LastPrintedBy] [int] NULL,
	[CoverNoteID] [varchar](100) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[EndorsementNo] [varchar](100) NULL,
	[ManualCoverNoteNo] [varchar](200) NULL,
	[PrintCount] [int] NULL CONSTRAINT [DF_PC]  DEFAULT ((0)),
	[RiskItemNo] [int] NOT NULL,
	[PrintLimitOverride] [bit] NOT NULL CONSTRAINT [DF__VehicleCo__Print__44CA3770]  DEFAULT ((0)),
	[Printable] [bit] NOT NULL CONSTRAINT [DF__VehicleCo__Print__40257DE4]  DEFAULT ((1)),
	[LimitsOfUse] [varchar](max) NULL,
	[period] [int] NOT NULL,
	[PrintedPaperNo] [varchar](max) NULL,
	[serial] [int] NULL,
	[approved] [bit] NOT NULL CONSTRAINT [DF__VehicleCo__appro__636EBA21]  DEFAULT ((0)),
	[approvedon] [datetime] NULL,
	[approvedby] [int] NULL,
	[PolicyID] [int] NOT NULL,
	[Printed] [bit] NOT NULL,
	[CancelledReason] [varchar](200) NULL,
	[ExpiryDateTime] [datetime] NOT NULL,
	[NoteNo] [int] NULL,
	[RequestPrint] [bit] NULL,
	[WordingTemplate] [int] NULL,
	[Generated] [bit] NULL,
	[CancelByFlat] [varchar](max) NULL,
	[LastPrintedByFlat] [varchar](max) NULL,
	[LastModifiedOn] [datetime] NOT NULL CONSTRAINT [DF_LastModify]  DEFAULT (getdate()),
	[mortgagee] [varchar](max) NULL,
	[PrintCountInsurer] [int] NULL,
	[FirstPrintedByFlat] [varchar](max) NULL,
 CONSTRAINT [PK__VehicleC__C6D392FCF29916EB] PRIMARY KEY CLUSTERED 
(
	[VehicleCoverNoteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__VehicleC__C6D392FDEA0199E0] UNIQUE NONCLUSTERED 
(
	[VehicleCoverNoteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VehicleCoverNoteState]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VehicleCoverNoteState](
	[VCNSID] [int] NOT NULL,
	[state] [varchar](50) NOT NULL,
 CONSTRAINT [PK__VehicleC__891993F4C5DE4430] PRIMARY KEY CLUSTERED 
(
	[VCNSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__VehicleC__891993F55ACF1ADD] UNIQUE NONCLUSTERED 
(
	[VCNSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VehicleHandDriveLookUp]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VehicleHandDriveLookUp](
	[VHDLID] [int] IDENTITY(1,1) NOT NULL,
	[HDType] [varchar](50) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (''),
	[LastModifiedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[VHDLID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[VHDLID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[HDType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Vehicles]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Vehicles](
	[VehicleID] [int] IDENTITY(1,1) NOT NULL,
	[Make] [varchar](250) NULL,
	[Model] [varchar](250) NULL,
	[ModelType] [varchar](50) NULL,
	[BodyType] [varchar](50) NULL,
	[Extension] [varchar](50) NULL,
	[VehicleRegistrationNo] [varchar](50) NULL,
	[VIN] [varchar](50) NULL,
	[HandDrive] [int] NULL,
	[Colour] [varchar](50) NULL,
	[Cylinders] [int] NULL,
	[EngineNo] [varchar](50) NULL,
	[EngineModified] [bit] NULL,
	[EngineType] [varchar](5) NULL,
	[EstimatedAnnualMileage] [int] NULL,
	[ExpiryDateofFitness] [date] NULL,
	[HPCCUnitType] [varchar](50) NULL,
	[MainDriver] [int] NULL,
	[ReferenceNo] [varchar](100) NULL,
	[RegistrationLocation] [varchar](250) NULL,
	[RoofType] [varchar](50) NULL,
	[Seating] [int] NULL,
	[SeatingCert] [int] NULL,
	[Tonnage] [int] NULL,
	[TransmissionType] [int] NULL,
	[VehicleYear] [int] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (''),
	[LastModifiedBy] [int] NOT NULL,
	[ChassisNo] [varchar](50) NOT NULL,
	[Mileage] [varchar](max) NULL,
	[RestrictedDriving] [bit] NULL DEFAULT ((0)),
	[RegisteredOwner] [varchar](max) NULL,
	[Usage] [int] NULL,
	[riskItemNo] [int] NULL,
	[ediid] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UN_Chassis] UNIQUE NONCLUSTERED 
(
	[ChassisNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[VehicleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VehiclesUnderPolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VehiclesUnderPolicy](
	[VUPID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleID] [int] NOT NULL,
	[PolicyID] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (''),
	[LastModifiedBy] [int] NOT NULL,
	[Cancelled] [bit] NOT NULL CONSTRAINT [DF_VUP_can]  DEFAULT ((0)),
	[CancelledBy] [int] NULL,
	[VehicleUsage] [int] NULL,
	[mortgagee] [int] NULL,
	[covercount] [int] NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[VUPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[VUPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Wording]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Wording](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__Wording__Created__3296789C]  DEFAULT (getdate()),
	[CreatedBy] [int] NOT NULL,
	[CertificateType] [varchar](30) NULL,
	[ExtensionCode] [varchar](250) NULL,
	[PolicyCover] [int] NULL,
	[ExcludedDriver] [bit] NOT NULL CONSTRAINT [DF__Wording__Exclude__338A9CD5]  DEFAULT ((0)),
	[ExcludedInsured] [bit] NOT NULL CONSTRAINT [DF__Wording__Exclude__347EC10E]  DEFAULT ((0)),
	[MultipleVehicle] [bit] NOT NULL CONSTRAINT [DF__Wording__Multipl__3572E547]  DEFAULT ((0)),
	[RegisteredOwner] [bit] NOT NULL CONSTRAINT [DF__Wording__Registe__36670980]  DEFAULT ((0)),
	[RestrictedDriver] [bit] NOT NULL CONSTRAINT [DF__Wording__Restric__375B2DB9]  DEFAULT ((0)),
	[Scheme] [bit] NOT NULL CONSTRAINT [DF__Wording__Scheme__384F51F2]  DEFAULT ((0)),
	[AuthorizedDrivers] [varchar](max) NULL,
	[LimitsOfUse] [varchar](max) NULL,
	[VehicleDesc] [varchar](max) NULL,
	[VehRegNo] [varchar](max) NULL,
	[PolicyHolders] [varchar](max) NULL,
	[Imported] [bit] NOT NULL CONSTRAINT [DF__Wording__Importe__3C1FE2D6]  DEFAULT ((0)),
	[CertificateID] [varchar](30) NULL,
	[CertificateInsuredCode] [varchar](max) NULL,
	[LastModifiedBy] [int] NULL,
	[LastModified] [datetime] NULL,
	[VehicleMake] [varchar](max) NULL,
	[VehicleModel] [varchar](max) NULL,
	[VehicleModelType] [varchar](max) NULL,
	[Usage] [varchar](max) NULL,
	[usageid] [int] NOT NULL,
 CONSTRAINT [PK__Wording__3214EC270CB65E63] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__Wording__3214EC26B5B62EDB] UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ZipCodes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZipCodes](
	[ZCID] [int] IDENTITY(1,1) NOT NULL,
	[ZipCode] [varchar](50) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[LastModifiedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ZCID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ZCID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[RequireAddressPerCompany] ADD  DEFAULT ((0)) FOR [RoadNumber]
GO
ALTER TABLE [dbo].[RequireAddressPerCompany] ADD  DEFAULT ((0)) FOR [RoadName]
GO
ALTER TABLE [dbo].[RequireAddressPerCompany] ADD  DEFAULT ((0)) FOR [RoadType]
GO
ALTER TABLE [dbo].[RequireAddressPerCompany] ADD  DEFAULT ((0)) FOR [ZipCode]
GO
ALTER TABLE [dbo].[RequireAddressPerCompany] ADD  DEFAULT ((0)) FOR [Country]
GO
ALTER TABLE [dbo].[RequireAddressPerCompany] ADD  DEFAULT ((0)) FOR [City]
GO
ALTER TABLE [dbo].[RequireAddressPerCompany] ADD  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[SystemLogonLimitTime] ADD  DEFAULT ((2)) FOR [hours]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([City])
REFERENCES [dbo].[Cities] ([CID])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([City])
REFERENCES [dbo].[Cities] ([CID])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([City])
REFERENCES [dbo].[Cities] ([CID])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([Country])
REFERENCES [dbo].[Countries] ([CID])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([Country])
REFERENCES [dbo].[Countries] ([CID])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([Country])
REFERENCES [dbo].[Countries] ([CID])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([RoadName])
REFERENCES [dbo].[RoadName] ([RID])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([RoadName])
REFERENCES [dbo].[RoadName] ([RID])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([RoadName])
REFERENCES [dbo].[RoadName] ([RID])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([RoadType])
REFERENCES [dbo].[RoadType] ([RTID])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([RoadType])
REFERENCES [dbo].[RoadType] ([RTID])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([RoadType])
REFERENCES [dbo].[RoadType] ([RTID])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([ZipCode])
REFERENCES [dbo].[ZipCodes] ([ZCID])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([ZipCode])
REFERENCES [dbo].[ZipCodes] ([ZCID])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD FOREIGN KEY([ZipCode])
REFERENCES [dbo].[ZipCodes] ([ZCID])
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Add_Parish_Map] FOREIGN KEY([ParishMap])
REFERENCES [dbo].[MappingTable] ([ID])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Add_Parish_Map]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_LastModifiedBy_Users] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_LastModifiedBy_Users]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_CreatedBy_User] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_CreatedBy_User]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Parish] FOREIGN KEY([Parish])
REFERENCES [dbo].[Parishes] ([PID])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Parish]
GO
ALTER TABLE [dbo].[AuthorizedDrivers]  WITH CHECK ADD  CONSTRAINT [ep] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[AuthorizedDrivers] CHECK CONSTRAINT [ep]
GO
ALTER TABLE [dbo].[AuthorizedDrivers]  WITH CHECK ADD  CONSTRAINT [epe] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[AuthorizedDrivers] CHECK CONSTRAINT [epe]
GO
ALTER TABLE [dbo].[AuthorizedDrivers]  WITH CHECK ADD FOREIGN KEY([DriverID])
REFERENCES [dbo].[Drivers] ([DriverID])
GO
ALTER TABLE [dbo].[AuthorizedDrivers]  WITH CHECK ADD FOREIGN KEY([DriverID])
REFERENCES [dbo].[Drivers] ([DriverID])
GO
ALTER TABLE [dbo].[AuthorizedDrivers]  WITH CHECK ADD FOREIGN KEY([DriverID])
REFERENCES [dbo].[Drivers] ([DriverID])
GO
ALTER TABLE [dbo].[AuthorizedDrivers]  WITH CHECK ADD FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[AuthorizedDrivers]  WITH CHECK ADD FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[AuthorizedDrivers]  WITH CHECK ADD FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[AuthorizedDrivers]  WITH CHECK ADD  CONSTRAINT [FK_Pol_Auth] FOREIGN KEY([PolicyID])
REFERENCES [dbo].[Policies] ([PolicyID])
GO
ALTER TABLE [dbo].[AuthorizedDrivers] CHECK CONSTRAINT [FK_Pol_Auth]
GO
ALTER TABLE [dbo].[CertIntel]  WITH CHECK ADD  CONSTRAINT [FK__CertIntel__certI__611C5D5B] FOREIGN KEY([certID])
REFERENCES [dbo].[VehicleCertificates] ([VehicleCertificateID])
GO
ALTER TABLE [dbo].[CertIntel] CHECK CONSTRAINT [FK__CertIntel__certI__611C5D5B]
GO
ALTER TABLE [dbo].[CertIntel]  WITH CHECK ADD FOREIGN KEY([companyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[CertIntel]  WITH CHECK ADD FOREIGN KEY([policyID])
REFERENCES [dbo].[Policies] ([PolicyID])
GO
ALTER TABLE [dbo].[CertIntel]  WITH CHECK ADD FOREIGN KEY([riskID])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[CertIntel]  WITH CHECK ADD FOREIGN KEY([userID])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Cities]  WITH CHECK ADD  CONSTRAINT [FK_Cities_CreatedBy_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Cities] CHECK CONSTRAINT [FK_Cities_CreatedBy_Users]
GO
ALTER TABLE [dbo].[Cities]  WITH CHECK ADD  CONSTRAINT [FK_Cities_LastModifiedBy_Users] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Cities] CHECK CONSTRAINT [FK_Cities_LastModifiedBy_Users]
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Address_Company_LookUp] FOREIGN KEY([CompanyAddress])
REFERENCES [dbo].[Address] ([AID])
GO
ALTER TABLE [dbo].[Company] CHECK CONSTRAINT [FK_Address_Company_LookUp]
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_CreatedBy_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Company] CHECK CONSTRAINT [FK_Company_CreatedBy_Users]
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_LastModifiedBy_Users] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Company] CHECK CONSTRAINT [FK_Company_LastModifiedBy_Users]
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_CompanyType] FOREIGN KEY([Type])
REFERENCES [dbo].[CompanyTypes] ([ID])
GO
ALTER TABLE [dbo].[Company] CHECK CONSTRAINT [FK_CompanyType]
GO
ALTER TABLE [dbo].[CompanyCoverNoteCount]  WITH CHECK ADD FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[CompanyToEmails]  WITH CHECK ADD  CONSTRAINT [FK_CompanyToEmails_Company] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[CompanyToEmails] CHECK CONSTRAINT [FK_CompanyToEmails_Company]
GO
ALTER TABLE [dbo].[CompanyToEmails]  WITH CHECK ADD  CONSTRAINT [FK_CompanyToEmails_Emails] FOREIGN KEY([EmailID])
REFERENCES [dbo].[Emails] ([EID])
GO
ALTER TABLE [dbo].[CompanyToEmails] CHECK CONSTRAINT [FK_CompanyToEmails_Emails]
GO
ALTER TABLE [dbo].[CompanyToEmails]  WITH CHECK ADD  CONSTRAINT [FK_CompanyToEmails_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[CompanyToEmails] CHECK CONSTRAINT [FK_CompanyToEmails_Users]
GO
ALTER TABLE [dbo].[CompanyToEmails]  WITH CHECK ADD  CONSTRAINT [FK_CompanyToEmails_Users1] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[CompanyToEmails] CHECK CONSTRAINT [FK_CompanyToEmails_Users1]
GO
ALTER TABLE [dbo].[CompanyToPhones]  WITH CHECK ADD  CONSTRAINT [FK__CompanyTo__Compa__17236851] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[CompanyToPhones] CHECK CONSTRAINT [FK__CompanyTo__Compa__17236851]
GO
ALTER TABLE [dbo].[CompanyToPhones]  WITH CHECK ADD  CONSTRAINT [FK__CompanyTo__Compa__4277DAAA] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[CompanyToPhones] CHECK CONSTRAINT [FK__CompanyTo__Compa__4277DAAA]
GO
ALTER TABLE [dbo].[CompanyToPhones]  WITH CHECK ADD  CONSTRAINT [FK__CompanyTo__Compa__5C02A283] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[CompanyToPhones] CHECK CONSTRAINT [FK__CompanyTo__Compa__5C02A283]
GO
ALTER TABLE [dbo].[CompanyToPhones]  WITH CHECK ADD  CONSTRAINT [FK__CompanyTo__Numbe__18178C8A] FOREIGN KEY([NumberType])
REFERENCES [dbo].[PhoneType] ([PTID])
GO
ALTER TABLE [dbo].[CompanyToPhones] CHECK CONSTRAINT [FK__CompanyTo__Numbe__18178C8A]
GO
ALTER TABLE [dbo].[CompanyToPhones]  WITH CHECK ADD  CONSTRAINT [FK__CompanyTo__Numbe__436BFEE3] FOREIGN KEY([NumberType])
REFERENCES [dbo].[PhoneType] ([PTID])
GO
ALTER TABLE [dbo].[CompanyToPhones] CHECK CONSTRAINT [FK__CompanyTo__Numbe__436BFEE3]
GO
ALTER TABLE [dbo].[CompanyToPhones]  WITH CHECK ADD  CONSTRAINT [FK__CompanyTo__Numbe__5CF6C6BC] FOREIGN KEY([NumberType])
REFERENCES [dbo].[PhoneType] ([PTID])
GO
ALTER TABLE [dbo].[CompanyToPhones] CHECK CONSTRAINT [FK__CompanyTo__Numbe__5CF6C6BC]
GO
ALTER TABLE [dbo].[CompanyToPhones]  WITH CHECK ADD  CONSTRAINT [FK__CompanyTo__Phone__190BB0C3] FOREIGN KEY([PhoneID])
REFERENCES [dbo].[PhoneNumbers] ([PID])
GO
ALTER TABLE [dbo].[CompanyToPhones] CHECK CONSTRAINT [FK__CompanyTo__Phone__190BB0C3]
GO
ALTER TABLE [dbo].[CompanyToPhones]  WITH CHECK ADD  CONSTRAINT [FK__CompanyTo__Phone__4460231C] FOREIGN KEY([PhoneID])
REFERENCES [dbo].[PhoneNumbers] ([PID])
GO
ALTER TABLE [dbo].[CompanyToPhones] CHECK CONSTRAINT [FK__CompanyTo__Phone__4460231C]
GO
ALTER TABLE [dbo].[CompanyToPhones]  WITH CHECK ADD  CONSTRAINT [FK__CompanyTo__Phone__5DEAEAF5] FOREIGN KEY([PhoneID])
REFERENCES [dbo].[PhoneNumbers] ([PID])
GO
ALTER TABLE [dbo].[CompanyToPhones] CHECK CONSTRAINT [FK__CompanyTo__Phone__5DEAEAF5]
GO
ALTER TABLE [dbo].[CompanyToPhones]  WITH CHECK ADD  CONSTRAINT [FK_CompanyToPhones_CompanyToPhones] FOREIGN KEY([CTPID])
REFERENCES [dbo].[CompanyToPhones] ([CTPID])
GO
ALTER TABLE [dbo].[CompanyToPhones] CHECK CONSTRAINT [FK_CompanyToPhones_CompanyToPhones]
GO
ALTER TABLE [dbo].[CompanyToPhones]  WITH CHECK ADD  CONSTRAINT [FK_CompanyToPhones_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[CompanyToPhones] CHECK CONSTRAINT [FK_CompanyToPhones_Users]
GO
ALTER TABLE [dbo].[CompanyToPhones]  WITH CHECK ADD  CONSTRAINT [FK_CompanyToPhones_Users1] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[CompanyToPhones] CHECK CONSTRAINT [FK_CompanyToPhones_Users1]
GO
ALTER TABLE [dbo].[CompanyToPhones]  WITH CHECK ADD  CONSTRAINT [FK_CompanyToPhones_Users2] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[CompanyToPhones] CHECK CONSTRAINT [FK_CompanyToPhones_Users2]
GO
ALTER TABLE [dbo].[ContactGroups]  WITH CHECK ADD  CONSTRAINT [FK_Owner_User] FOREIGN KEY([Owner])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[ContactGroups] CHECK CONSTRAINT [FK_Owner_User]
GO
ALTER TABLE [dbo].[ContactUserAssoc]  WITH CHECK ADD FOREIGN KEY([contactgroup])
REFERENCES [dbo].[ContactGroups] ([ID])
GO
ALTER TABLE [dbo].[ContactUserAssoc]  WITH CHECK ADD FOREIGN KEY([contactgroup])
REFERENCES [dbo].[ContactGroups] ([ID])
GO
ALTER TABLE [dbo].[ContactUserAssoc]  WITH CHECK ADD FOREIGN KEY([contactgroup])
REFERENCES [dbo].[ContactGroups] ([ID])
GO
ALTER TABLE [dbo].[ContactUserAssoc]  WITH CHECK ADD FOREIGN KEY([UID])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[ContactUserAssoc]  WITH CHECK ADD FOREIGN KEY([UID])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[ContactUserAssoc]  WITH CHECK ADD FOREIGN KEY([UID])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Countries]  WITH CHECK ADD  CONSTRAINT [FK_Countries_CreatedBy_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Countries] CHECK CONSTRAINT [FK_Countries_CreatedBy_Users]
GO
ALTER TABLE [dbo].[Countries]  WITH CHECK ADD  CONSTRAINT [FK_Countries_LastModifiedBy_Users] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Countries] CHECK CONSTRAINT [FK_Countries_LastModifiedBy_Users]
GO
ALTER TABLE [dbo].[Drivers]  WITH CHECK ADD  CONSTRAINT [e] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Drivers] CHECK CONSTRAINT [e]
GO
ALTER TABLE [dbo].[Drivers]  WITH CHECK ADD  CONSTRAINT [es] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Drivers] CHECK CONSTRAINT [es]
GO
ALTER TABLE [dbo].[Drivers]  WITH CHECK ADD FOREIGN KEY([PersonID])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[Drivers]  WITH CHECK ADD FOREIGN KEY([PersonID])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[Drivers]  WITH CHECK ADD FOREIGN KEY([PersonID])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[Emails]  WITH CHECK ADD  CONSTRAINT [ek] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Emails] CHECK CONSTRAINT [ek]
GO
ALTER TABLE [dbo].[Emails]  WITH CHECK ADD  CONSTRAINT [p] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Emails] CHECK CONSTRAINT [p]
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK__Employees__Compa__2F2FFC0C] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK__Employees__Compa__2F2FFC0C]
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK__Employees__Compa__6A50C1DA] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK__Employees__Compa__6A50C1DA]
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD FOREIGN KEY([PersonID])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD FOREIGN KEY([PersonID])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD FOREIGN KEY([PersonID])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[Enquiries]  WITH CHECK ADD FOREIGN KEY([policy])
REFERENCES [dbo].[Policies] ([PolicyID])
GO
ALTER TABLE [dbo].[Enquiries]  WITH CHECK ADD FOREIGN KEY([risk])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[error]  WITH CHECK ADD FOREIGN KEY([reqid])
REFERENCES [dbo].[JsonAudit] ([ID])
GO
ALTER TABLE [dbo].[error]  WITH CHECK ADD  CONSTRAINT [FK_subid] FOREIGN KEY([subid])
REFERENCES [dbo].[error] ([ID])
GO
ALTER TABLE [dbo].[error] CHECK CONSTRAINT [FK_subid]
GO
ALTER TABLE [dbo].[ExceptedDrivers]  WITH CHECK ADD  CONSTRAINT [epee] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[ExceptedDrivers] CHECK CONSTRAINT [epee]
GO
ALTER TABLE [dbo].[ExceptedDrivers]  WITH CHECK ADD  CONSTRAINT [epp] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[ExceptedDrivers] CHECK CONSTRAINT [epp]
GO
ALTER TABLE [dbo].[ExceptedDrivers]  WITH CHECK ADD FOREIGN KEY([DriverID])
REFERENCES [dbo].[Drivers] ([DriverID])
GO
ALTER TABLE [dbo].[ExceptedDrivers]  WITH CHECK ADD FOREIGN KEY([DriverID])
REFERENCES [dbo].[Drivers] ([DriverID])
GO
ALTER TABLE [dbo].[ExceptedDrivers]  WITH CHECK ADD FOREIGN KEY([DriverID])
REFERENCES [dbo].[Drivers] ([DriverID])
GO
ALTER TABLE [dbo].[ExceptedDrivers]  WITH CHECK ADD FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[ExceptedDrivers]  WITH CHECK ADD FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[ExceptedDrivers]  WITH CHECK ADD FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[ExceptedDrivers]  WITH CHECK ADD  CONSTRAINT [FK_Pol_Exce] FOREIGN KEY([PolicyID])
REFERENCES [dbo].[Policies] ([PolicyID])
GO
ALTER TABLE [dbo].[ExceptedDrivers] CHECK CONSTRAINT [FK_Pol_Exce]
GO
ALTER TABLE [dbo].[ExcludedDrivers]  WITH CHECK ADD FOREIGN KEY([DriverID])
REFERENCES [dbo].[Drivers] ([DriverID])
GO
ALTER TABLE [dbo].[ExcludedDrivers]  WITH CHECK ADD FOREIGN KEY([DriverID])
REFERENCES [dbo].[Drivers] ([DriverID])
GO
ALTER TABLE [dbo].[ExcludedDrivers]  WITH CHECK ADD FOREIGN KEY([DriverID])
REFERENCES [dbo].[Drivers] ([DriverID])
GO
ALTER TABLE [dbo].[ExcludedDrivers]  WITH CHECK ADD FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[ExcludedDrivers]  WITH CHECK ADD FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[ExcludedDrivers]  WITH CHECK ADD FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[ExcludedDrivers]  WITH CHECK ADD  CONSTRAINT [FK_Pol_Exclu] FOREIGN KEY([PolicyID])
REFERENCES [dbo].[Policies] ([PolicyID])
GO
ALTER TABLE [dbo].[ExcludedDrivers] CHECK CONSTRAINT [FK_Pol_Exclu]
GO
ALTER TABLE [dbo].[ExcludedDrivers]  WITH CHECK ADD  CONSTRAINT [pe] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[ExcludedDrivers] CHECK CONSTRAINT [pe]
GO
ALTER TABLE [dbo].[ExcludedDrivers]  WITH CHECK ADD  CONSTRAINT [pp] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[ExcludedDrivers] CHECK CONSTRAINT [pp]
GO
ALTER TABLE [dbo].[GeneratedCertificate]  WITH CHECK ADD  CONSTRAINT [FK_GeneratedCertificate_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[GeneratedCertificate] CHECK CONSTRAINT [FK_GeneratedCertificate_Users]
GO
ALTER TABLE [dbo].[GeneratedCertificate]  WITH CHECK ADD  CONSTRAINT [FK_GeneratedCertificate_VehicleCertificates] FOREIGN KEY([VehicleCertificateID])
REFERENCES [dbo].[VehicleCertificates] ([VehicleCertificateID])
GO
ALTER TABLE [dbo].[GeneratedCertificate] CHECK CONSTRAINT [FK_GeneratedCertificate_VehicleCertificates]
GO
ALTER TABLE [dbo].[GeneratedCoverNotes]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[GeneratedCoverNotes]  WITH CHECK ADD  CONSTRAINT [FK_Cnote] FOREIGN KEY([VehicleCoverNoteID])
REFERENCES [dbo].[VehicleCoverNote] ([VehicleCoverNoteID])
GO
ALTER TABLE [dbo].[GeneratedCoverNotes] CHECK CONSTRAINT [FK_Cnote]
GO
ALTER TABLE [dbo].[InsurerToIntermediary]  WITH CHECK ADD  CONSTRAINT [FK_Ins_CreatedBy_User] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[InsurerToIntermediary] CHECK CONSTRAINT [FK_Ins_CreatedBy_User]
GO
ALTER TABLE [dbo].[InsurerToIntermediary]  WITH CHECK ADD  CONSTRAINT [FK_Ins_LastModifiedBy_User] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[InsurerToIntermediary] CHECK CONSTRAINT [FK_Ins_LastModifiedBy_User]
GO
ALTER TABLE [dbo].[InsurerToIntermediary]  WITH CHECK ADD  CONSTRAINT [FK_InsurerToIntermediary_Company] FOREIGN KEY([Insurer])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[InsurerToIntermediary] CHECK CONSTRAINT [FK_InsurerToIntermediary_Company]
GO
ALTER TABLE [dbo].[InsurerToIntermediary]  WITH CHECK ADD  CONSTRAINT [FK_InsurerToIntermediary_Company1] FOREIGN KEY([Intermediary])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[InsurerToIntermediary] CHECK CONSTRAINT [FK_InsurerToIntermediary_Company1]
GO
ALTER TABLE [dbo].[JsonAudit]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[KPI]  WITH CHECK ADD FOREIGN KEY([company])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[KPI]  WITH CHECK ADD FOREIGN KEY([mode])
REFERENCES [dbo].[KPImode] ([id])
GO
ALTER TABLE [dbo].[KPI]  WITH CHECK ADD FOREIGN KEY([type])
REFERENCES [dbo].[KPItype] ([id])
GO
ALTER TABLE [dbo].[KPI]  WITH CHECK ADD  CONSTRAINT [FK_Date_Mode] FOREIGN KEY([datemode])
REFERENCES [dbo].[KPIDateMode] ([id])
GO
ALTER TABLE [dbo].[KPI] CHECK CONSTRAINT [FK_Date_Mode]
GO
ALTER TABLE [dbo].[MappingTable]  WITH CHECK ADD  CONSTRAINT [FK_Map_Company] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[MappingTable] CHECK CONSTRAINT [FK_Map_Company]
GO
ALTER TABLE [dbo].[Mortgagees]  WITH CHECK ADD  CONSTRAINT [FK__Mortgagee__Creat__74444068] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Mortgagees] CHECK CONSTRAINT [FK__Mortgagee__Creat__74444068]
GO
ALTER TABLE [dbo].[Mortgagees]  WITH CHECK ADD  CONSTRAINT [FK__Mortgagee__Creat__753864A1] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Mortgagees] CHECK CONSTRAINT [FK__Mortgagee__Creat__753864A1]
GO
ALTER TABLE [dbo].[Mortgagees]  WITH CHECK ADD  CONSTRAINT [FK__Mortgagee__Creat__762C88DA] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Mortgagees] CHECK CONSTRAINT [FK__Mortgagee__Creat__762C88DA]
GO
ALTER TABLE [dbo].[Mortgagees]  WITH CHECK ADD  CONSTRAINT [FK_Address_Mortgagees_LookUp] FOREIGN KEY([Address])
REFERENCES [dbo].[Address] ([AID])
GO
ALTER TABLE [dbo].[Mortgagees] CHECK CONSTRAINT [FK_Address_Mortgagees_LookUp]
GO
ALTER TABLE [dbo].[MortgageesToEmail]  WITH CHECK ADD FOREIGN KEY([EID])
REFERENCES [dbo].[Emails] ([EID])
GO
ALTER TABLE [dbo].[MortgageesToEmail]  WITH CHECK ADD FOREIGN KEY([EID])
REFERENCES [dbo].[Emails] ([EID])
GO
ALTER TABLE [dbo].[MortgageesToEmail]  WITH CHECK ADD FOREIGN KEY([EID])
REFERENCES [dbo].[Emails] ([EID])
GO
ALTER TABLE [dbo].[MortgageesToEmail]  WITH CHECK ADD FOREIGN KEY([MID])
REFERENCES [dbo].[Mortgagees] ([MID])
GO
ALTER TABLE [dbo].[MortgageesToEmail]  WITH CHECK ADD FOREIGN KEY([MID])
REFERENCES [dbo].[Mortgagees] ([MID])
GO
ALTER TABLE [dbo].[MortgageesToEmail]  WITH CHECK ADD FOREIGN KEY([MID])
REFERENCES [dbo].[Mortgagees] ([MID])
GO
ALTER TABLE [dbo].[MortgageesToPhone]  WITH CHECK ADD FOREIGN KEY([PhoneType])
REFERENCES [dbo].[PhoneType] ([PTID])
GO
ALTER TABLE [dbo].[MortgageesToPhone]  WITH CHECK ADD FOREIGN KEY([PhoneType])
REFERENCES [dbo].[PhoneType] ([PTID])
GO
ALTER TABLE [dbo].[MortgageesToPhone]  WITH CHECK ADD FOREIGN KEY([PhoneType])
REFERENCES [dbo].[PhoneType] ([PTID])
GO
ALTER TABLE [dbo].[MortgageesToPhone]  WITH CHECK ADD FOREIGN KEY([MID])
REFERENCES [dbo].[Mortgagees] ([MID])
GO
ALTER TABLE [dbo].[MortgageesToPhone]  WITH CHECK ADD FOREIGN KEY([MID])
REFERENCES [dbo].[Mortgagees] ([MID])
GO
ALTER TABLE [dbo].[MortgageesToPhone]  WITH CHECK ADD FOREIGN KEY([MID])
REFERENCES [dbo].[Mortgagees] ([MID])
GO
ALTER TABLE [dbo].[MortgageesToPhone]  WITH CHECK ADD  CONSTRAINT [FK__MortgageesT__PID__21A0F6C4] FOREIGN KEY([PID])
REFERENCES [dbo].[PhoneNumbers] ([PID])
GO
ALTER TABLE [dbo].[MortgageesToPhone] CHECK CONSTRAINT [FK__MortgageesT__PID__21A0F6C4]
GO
ALTER TABLE [dbo].[MortgageesToPhone]  WITH CHECK ADD  CONSTRAINT [FK__MortgageesT__PID__5D2BD0E6] FOREIGN KEY([PID])
REFERENCES [dbo].[PhoneNumbers] ([PID])
GO
ALTER TABLE [dbo].[MortgageesToPhone] CHECK CONSTRAINT [FK__MortgageesT__PID__5D2BD0E6]
GO
ALTER TABLE [dbo].[MortgageesToPhone]  WITH CHECK ADD  CONSTRAINT [FK__MortgageesT__PID__668030F6] FOREIGN KEY([PID])
REFERENCES [dbo].[PhoneNumbers] ([PID])
GO
ALTER TABLE [dbo].[MortgageesToPhone] CHECK CONSTRAINT [FK__MortgageesT__PID__668030F6]
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD FOREIGN KEY([NoteFrom])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD FOREIGN KEY([NoteFrom])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD FOREIGN KEY([NoteFrom])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD  CONSTRAINT [FK_ParentMsg] FOREIGN KEY([ParentMessage])
REFERENCES [dbo].[Notifications] ([NID])
GO
ALTER TABLE [dbo].[Notifications] CHECK CONSTRAINT [FK_ParentMsg]
GO
ALTER TABLE [dbo].[NotificationsTo]  WITH CHECK ADD FOREIGN KEY([Notification])
REFERENCES [dbo].[Notifications] ([NID])
GO
ALTER TABLE [dbo].[NotificationsTo]  WITH CHECK ADD FOREIGN KEY([Notification])
REFERENCES [dbo].[Notifications] ([NID])
GO
ALTER TABLE [dbo].[NotificationsTo]  WITH CHECK ADD FOREIGN KEY([Notification])
REFERENCES [dbo].[Notifications] ([NID])
GO
ALTER TABLE [dbo].[NotificationsTo]  WITH CHECK ADD FOREIGN KEY([UID])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[NotificationsTo]  WITH CHECK ADD FOREIGN KEY([UID])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[NotificationsTo]  WITH CHECK ADD FOREIGN KEY([UID])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Parishes]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Parishes]  WITH CHECK ADD FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[People]  WITH CHECK ADD  CONSTRAINT [fk] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[People] CHECK CONSTRAINT [fk]
GO
ALTER TABLE [dbo].[People]  WITH CHECK ADD  CONSTRAINT [FK_Address_LookUp] FOREIGN KEY([Address])
REFERENCES [dbo].[Address] ([AID])
GO
ALTER TABLE [dbo].[People] CHECK CONSTRAINT [FK_Address_LookUp]
GO
ALTER TABLE [dbo].[People]  WITH CHECK ADD  CONSTRAINT [foreignKey] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[People] CHECK CONSTRAINT [foreignKey]
GO
ALTER TABLE [dbo].[People]  WITH CHECK ADD  CONSTRAINT [forKey] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[People] CHECK CONSTRAINT [forKey]
GO
ALTER TABLE [dbo].[PeopleToEmails]  WITH CHECK ADD FOREIGN KEY([EmailID])
REFERENCES [dbo].[Emails] ([EID])
GO
ALTER TABLE [dbo].[PeopleToEmails]  WITH CHECK ADD FOREIGN KEY([EmailID])
REFERENCES [dbo].[Emails] ([EID])
GO
ALTER TABLE [dbo].[PeopleToEmails]  WITH CHECK ADD FOREIGN KEY([EmailID])
REFERENCES [dbo].[Emails] ([EID])
GO
ALTER TABLE [dbo].[PeopleToEmails]  WITH CHECK ADD FOREIGN KEY([PeopleID])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[PeopleToEmails]  WITH CHECK ADD FOREIGN KEY([PeopleID])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[PeopleToEmails]  WITH CHECK ADD FOREIGN KEY([PeopleID])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[PeopleToEmails]  WITH CHECK ADD  CONSTRAINT [oek] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[PeopleToEmails] CHECK CONSTRAINT [oek]
GO
ALTER TABLE [dbo].[PeopleToEmails]  WITH CHECK ADD  CONSTRAINT [oo] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[PeopleToEmails] CHECK CONSTRAINT [oo]
GO
ALTER TABLE [dbo].[PeopleToPhones]  WITH CHECK ADD FOREIGN KEY([NumberType])
REFERENCES [dbo].[PhoneType] ([PTID])
GO
ALTER TABLE [dbo].[PeopleToPhones]  WITH CHECK ADD FOREIGN KEY([NumberType])
REFERENCES [dbo].[PhoneType] ([PTID])
GO
ALTER TABLE [dbo].[PeopleToPhones]  WITH CHECK ADD FOREIGN KEY([NumberType])
REFERENCES [dbo].[PhoneType] ([PTID])
GO
ALTER TABLE [dbo].[PeopleToPhones]  WITH CHECK ADD FOREIGN KEY([PeopleID])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[PeopleToPhones]  WITH CHECK ADD FOREIGN KEY([PeopleID])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[PeopleToPhones]  WITH CHECK ADD FOREIGN KEY([PeopleID])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[PeopleToPhones]  WITH CHECK ADD  CONSTRAINT [FK__PeopleToP__Phone__247D636F] FOREIGN KEY([PhoneID])
REFERENCES [dbo].[PhoneNumbers] ([PID])
GO
ALTER TABLE [dbo].[PeopleToPhones] CHECK CONSTRAINT [FK__PeopleToP__Phone__247D636F]
GO
ALTER TABLE [dbo].[PeopleToPhones]  WITH CHECK ADD  CONSTRAINT [FK__PeopleToP__Phone__62E4AA3C] FOREIGN KEY([PhoneID])
REFERENCES [dbo].[PhoneNumbers] ([PID])
GO
ALTER TABLE [dbo].[PeopleToPhones] CHECK CONSTRAINT [FK__PeopleToP__Phone__62E4AA3C]
GO
ALTER TABLE [dbo].[PeopleToPhones]  WITH CHECK ADD  CONSTRAINT [FK__PeopleToP__Phone__695C9DA1] FOREIGN KEY([PhoneID])
REFERENCES [dbo].[PhoneNumbers] ([PID])
GO
ALTER TABLE [dbo].[PeopleToPhones] CHECK CONSTRAINT [FK__PeopleToP__Phone__695C9DA1]
GO
ALTER TABLE [dbo].[PermissionGroupAssoc]  WITH CHECK ADD FOREIGN KEY([UGID])
REFERENCES [dbo].[UserGroups] ([UGID])
GO
ALTER TABLE [dbo].[PermissionGroupAssoc]  WITH CHECK ADD FOREIGN KEY([UGID])
REFERENCES [dbo].[UserGroups] ([UGID])
GO
ALTER TABLE [dbo].[PermissionGroupAssoc]  WITH CHECK ADD FOREIGN KEY([UGID])
REFERENCES [dbo].[UserGroups] ([UGID])
GO
ALTER TABLE [dbo].[PermissionGroupAssoc]  WITH CHECK ADD FOREIGN KEY([UPID])
REFERENCES [dbo].[UserPermissions] ([UPID])
GO
ALTER TABLE [dbo].[PermissionGroupAssoc]  WITH CHECK ADD FOREIGN KEY([UPID])
REFERENCES [dbo].[UserPermissions] ([UPID])
GO
ALTER TABLE [dbo].[PermissionGroupAssoc]  WITH CHECK ADD FOREIGN KEY([UPID])
REFERENCES [dbo].[UserPermissions] ([UPID])
GO
ALTER TABLE [dbo].[PhoneNumbers]  WITH CHECK ADD  CONSTRAINT [FK_PhoneNumbers_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[PhoneNumbers] CHECK CONSTRAINT [FK_PhoneNumbers_Users]
GO
ALTER TABLE [dbo].[PhoneNumbers]  WITH CHECK ADD  CONSTRAINT [FK_PhoneNumbers_Users1] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[PhoneNumbers] CHECK CONSTRAINT [FK_PhoneNumbers_Users1]
GO
ALTER TABLE [dbo].[Policies]  WITH CHECK ADD  CONSTRAINT [FK__Policies__Compan__01342732] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[Policies] CHECK CONSTRAINT [FK__Policies__Compan__01342732]
GO
ALTER TABLE [dbo].[Policies]  WITH CHECK ADD  CONSTRAINT [FK__Policies__Compan__282DF8C2] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[Policies] CHECK CONSTRAINT [FK__Policies__Compan__282DF8C2]
GO
ALTER TABLE [dbo].[Policies]  WITH CHECK ADD  CONSTRAINT [FK__Policies__Compan__46136164] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[Policies] CHECK CONSTRAINT [FK__Policies__Compan__46136164]
GO
ALTER TABLE [dbo].[Policies]  WITH CHECK ADD  CONSTRAINT [FK_insuredtype_policy] FOREIGN KEY([InsuredType])
REFERENCES [dbo].[PolicyInsuredType] ([PITID])
GO
ALTER TABLE [dbo].[Policies] CHECK CONSTRAINT [FK_insuredtype_policy]
GO
ALTER TABLE [dbo].[Policies]  WITH CHECK ADD  CONSTRAINT [FK_Policies_CoverConstraint] FOREIGN KEY([PolicyCover])
REFERENCES [dbo].[PolicyCovers] ([ID])
GO
ALTER TABLE [dbo].[Policies] CHECK CONSTRAINT [FK_Policies_CoverConstraint]
GO
ALTER TABLE [dbo].[Policies]  WITH CHECK ADD  CONSTRAINT [FK_Policies_Policies] FOREIGN KEY([PolicyID])
REFERENCES [dbo].[Policies] ([PolicyID])
GO
ALTER TABLE [dbo].[Policies] CHECK CONSTRAINT [FK_Policies_Policies]
GO
ALTER TABLE [dbo].[Policies]  WITH CHECK ADD  CONSTRAINT [FK_Policies_Users] FOREIGN KEY([CancelledBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Policies] CHECK CONSTRAINT [FK_Policies_Users]
GO
ALTER TABLE [dbo].[Policies]  WITH CHECK ADD  CONSTRAINT [FKMA] FOREIGN KEY([MailingAddress])
REFERENCES [dbo].[Address] ([AID])
GO
ALTER TABLE [dbo].[Policies] CHECK CONSTRAINT [FKMA]
GO
ALTER TABLE [dbo].[Policies]  WITH CHECK ADD  CONSTRAINT [FKPA] FOREIGN KEY([BillingAddress])
REFERENCES [dbo].[Address] ([AID])
GO
ALTER TABLE [dbo].[Policies] CHECK CONSTRAINT [FKPA]
GO
ALTER TABLE [dbo].[Policies]  WITH CHECK ADD  CONSTRAINT [sec] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Policies] CHECK CONSTRAINT [sec]
GO
ALTER TABLE [dbo].[Policies]  WITH CHECK ADD  CONSTRAINT [seek] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Policies] CHECK CONSTRAINT [seek]
GO
ALTER TABLE [dbo].[PolicyCovers]  WITH CHECK ADD  CONSTRAINT [CoverToCompany] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[PolicyCovers] CHECK CONSTRAINT [CoverToCompany]
GO
ALTER TABLE [dbo].[PolicyCovers]  WITH CHECK ADD  CONSTRAINT [PolicyCovers_CreatedBy_User] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[PolicyCovers] CHECK CONSTRAINT [PolicyCovers_CreatedBy_User]
GO
ALTER TABLE [dbo].[PolicyCovers]  WITH CHECK ADD  CONSTRAINT [PolicyCovers_LastModifiedBy_User] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[PolicyCovers] CHECK CONSTRAINT [PolicyCovers_LastModifiedBy_User]
GO
ALTER TABLE [dbo].[PolicyInsured]  WITH CHECK ADD  CONSTRAINT [eys] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[PolicyInsured] CHECK CONSTRAINT [eys]
GO
ALTER TABLE [dbo].[PolicyInsured]  WITH CHECK ADD FOREIGN KEY([PersonID])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[PolicyInsured]  WITH CHECK ADD FOREIGN KEY([PersonID])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[PolicyInsured]  WITH CHECK ADD FOREIGN KEY([PersonID])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[PolicyInsured]  WITH CHECK ADD  CONSTRAINT [FK__PolicyIns__Polic__031C6FA4] FOREIGN KEY([PolicyID])
REFERENCES [dbo].[Policies] ([PolicyID])
GO
ALTER TABLE [dbo].[PolicyInsured] CHECK CONSTRAINT [FK__PolicyIns__Polic__031C6FA4]
GO
ALTER TABLE [dbo].[PolicyInsured]  WITH CHECK ADD  CONSTRAINT [FK__PolicyIns__Polic__2CF2ADDF] FOREIGN KEY([PolicyID])
REFERENCES [dbo].[Policies] ([PolicyID])
GO
ALTER TABLE [dbo].[PolicyInsured] CHECK CONSTRAINT [FK__PolicyIns__Polic__2CF2ADDF]
GO
ALTER TABLE [dbo].[PolicyInsured]  WITH CHECK ADD  CONSTRAINT [FK__PolicyIns__Polic__47FBA9D6] FOREIGN KEY([PolicyID])
REFERENCES [dbo].[Policies] ([PolicyID])
GO
ALTER TABLE [dbo].[PolicyInsured] CHECK CONSTRAINT [FK__PolicyIns__Polic__47FBA9D6]
GO
ALTER TABLE [dbo].[PolicyInsured]  WITH CHECK ADD  CONSTRAINT [FK_Company_PolicyInsured] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[PolicyInsured] CHECK CONSTRAINT [FK_Company_PolicyInsured]
GO
ALTER TABLE [dbo].[PolicyInsured]  WITH CHECK ADD  CONSTRAINT [keys] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[PolicyInsured] CHECK CONSTRAINT [keys]
GO
ALTER TABLE [dbo].[RequireAddressPerCompany]  WITH CHECK ADD  CONSTRAINT [FK__RequireAd__Compa__1446FBA6] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[RequireAddressPerCompany] CHECK CONSTRAINT [FK__RequireAd__Compa__1446FBA6]
GO
ALTER TABLE [dbo].[RequireAddressPerCompany]  WITH CHECK ADD  CONSTRAINT [FK__RequireAd__Compa__1E6F845E] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[RequireAddressPerCompany] CHECK CONSTRAINT [FK__RequireAd__Compa__1E6F845E]
GO
ALTER TABLE [dbo].[RequireAddressPerCompany]  WITH CHECK ADD  CONSTRAINT [FK__RequireAd__Compa__592635D8] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[RequireAddressPerCompany] CHECK CONSTRAINT [FK__RequireAd__Compa__592635D8]
GO
ALTER TABLE [dbo].[RequireAddressPerCompany]  WITH CHECK ADD  CONSTRAINT [FK_RequireAddress_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[RequireAddressPerCompany] CHECK CONSTRAINT [FK_RequireAddress_CreatedBy]
GO
ALTER TABLE [dbo].[RequireAddressPerCompany]  WITH CHECK ADD  CONSTRAINT [FK_RequireAddress_LastModifiedBy] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[RequireAddressPerCompany] CHECK CONSTRAINT [FK_RequireAddress_LastModifiedBy]
GO
ALTER TABLE [dbo].[RoadName]  WITH CHECK ADD  CONSTRAINT [FK_Road_CreatedBy_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[RoadName] CHECK CONSTRAINT [FK_Road_CreatedBy_Users]
GO
ALTER TABLE [dbo].[RoadName]  WITH CHECK ADD  CONSTRAINT [FK_Road_LastModifiedBy_Users] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[RoadName] CHECK CONSTRAINT [FK_Road_LastModifiedBy_Users]
GO
ALTER TABLE [dbo].[RoadType]  WITH CHECK ADD  CONSTRAINT [FK_RoadType_CreatedBy_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[RoadType] CHECK CONSTRAINT [FK_RoadType_CreatedBy_Users]
GO
ALTER TABLE [dbo].[RoadType]  WITH CHECK ADD  CONSTRAINT [FK_RoadType_LastModifiedBy_Users] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[RoadType] CHECK CONSTRAINT [FK_RoadType_LastModifiedBy_Users]
GO
ALTER TABLE [dbo].[RSAKeys]  WITH CHECK ADD FOREIGN KEY([uid])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[SuperGroupMapping]  WITH CHECK ADD FOREIGN KEY([CGID])
REFERENCES [dbo].[UserGroups] ([UGID])
GO
ALTER TABLE [dbo].[SuperGroupMapping]  WITH CHECK ADD FOREIGN KEY([CGID])
REFERENCES [dbo].[UserGroups] ([UGID])
GO
ALTER TABLE [dbo].[SuperGroupMapping]  WITH CHECK ADD FOREIGN KEY([CGID])
REFERENCES [dbo].[UserGroups] ([UGID])
GO
ALTER TABLE [dbo].[SuperGroupMapping]  WITH CHECK ADD FOREIGN KEY([PGID])
REFERENCES [dbo].[UserGroups] ([UGID])
GO
ALTER TABLE [dbo].[SuperGroupMapping]  WITH CHECK ADD FOREIGN KEY([PGID])
REFERENCES [dbo].[UserGroups] ([UGID])
GO
ALTER TABLE [dbo].[SuperGroupMapping]  WITH CHECK ADD FOREIGN KEY([PGID])
REFERENCES [dbo].[UserGroups] ([UGID])
GO
ALTER TABLE [dbo].[SystemAuditLog]  WITH CHECK ADD FOREIGN KEY([ActionUserID])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[SystemAuditLog]  WITH CHECK ADD FOREIGN KEY([ActionUserID])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[TemplateImages]  WITH CHECK ADD FOREIGN KEY([companyid])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[TemplateImages]  WITH CHECK ADD FOREIGN KEY([userid])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[TransmissionTypeLookUp]  WITH CHECK ADD  CONSTRAINT [ee] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[TransmissionTypeLookUp] CHECK CONSTRAINT [ee]
GO
ALTER TABLE [dbo].[TransmissionTypeLookUp]  WITH CHECK ADD  CONSTRAINT [ss] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[TransmissionTypeLookUp] CHECK CONSTRAINT [ss]
GO
ALTER TABLE [dbo].[Usages]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[UsagesToCovers]  WITH CHECK ADD FOREIGN KEY([CoverID])
REFERENCES [dbo].[PolicyCovers] ([ID])
GO
ALTER TABLE [dbo].[UsagesToCovers]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[UsagesToCovers]  WITH CHECK ADD FOREIGN KEY([UsageID])
REFERENCES [dbo].[Usages] ([ID])
GO
ALTER TABLE [dbo].[UserGroups]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[UserGroups]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[UserGroups]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[UserGroups]  WITH CHECK ADD  CONSTRAINT [FK_Copy] FOREIGN KEY([CopyFrom])
REFERENCES [dbo].[UserGroups] ([UGID])
GO
ALTER TABLE [dbo].[UserGroups] CHECK CONSTRAINT [FK_Copy]
GO
ALTER TABLE [dbo].[UserGroups]  WITH CHECK ADD  CONSTRAINT [FK_Group_Company] FOREIGN KEY([Company])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[UserGroups] CHECK CONSTRAINT [FK_Group_Company]
GO
ALTER TABLE [dbo].[UserGroups]  WITH CHECK ADD  CONSTRAINT [FK_Modified_UserGroups] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[UserGroups] CHECK CONSTRAINT [FK_Modified_UserGroups]
GO
ALTER TABLE [dbo].[UserGroupUserAssoc]  WITH CHECK ADD FOREIGN KEY([UGID])
REFERENCES [dbo].[UserGroups] ([UGID])
GO
ALTER TABLE [dbo].[UserGroupUserAssoc]  WITH CHECK ADD FOREIGN KEY([UID])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[UserGroupUserAssoc]  WITH CHECK ADD  CONSTRAINT [UGA_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[UserGroupUserAssoc] CHECK CONSTRAINT [UGA_CreatedBy]
GO
ALTER TABLE [dbo].[UserGroupUserAssoc]  WITH CHECK ADD  CONSTRAINT [UGA_LastModifiedBy] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[UserGroupUserAssoc] CHECK CONSTRAINT [UGA_LastModifiedBy]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([EmployeeID])
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([EmployeeID])
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([EmployeeID])
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_createdby_uid] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_createdby_uid]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_LastModify_User] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_LastModify_User]
GO
ALTER TABLE [dbo].[VehicleCertificates]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCe__Cance__1AD3FDA4] FOREIGN KEY([CancelledBy])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[VehicleCertificates] CHECK CONSTRAINT [FK__VehicleCe__Cance__1AD3FDA4]
GO
ALTER TABLE [dbo].[VehicleCertificates]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCe__Compa__0CA5D9DE] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[VehicleCertificates] CHECK CONSTRAINT [FK__VehicleCe__Compa__0CA5D9DE]
GO
ALTER TABLE [dbo].[VehicleCertificates]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCe__Compa__51851410] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[VehicleCertificates] CHECK CONSTRAINT [FK__VehicleCe__Compa__51851410]
GO
ALTER TABLE [dbo].[VehicleCertificates]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCe__Compa__58D1301D] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[VehicleCertificates] CHECK CONSTRAINT [FK__VehicleCe__Compa__58D1301D]
GO
ALTER TABLE [dbo].[VehicleCertificates]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCe__Creat__17036CC0] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[VehicleCertificates] CHECK CONSTRAINT [FK__VehicleCe__Creat__17036CC0]
GO
ALTER TABLE [dbo].[VehicleCertificates]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCe__RiskI__0D99FE17] FOREIGN KEY([RiskItemNo])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[VehicleCertificates] CHECK CONSTRAINT [FK__VehicleCe__RiskI__0D99FE17]
GO
ALTER TABLE [dbo].[VehicleCertificates]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCe__RiskI__52793849] FOREIGN KEY([RiskItemNo])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[VehicleCertificates] CHECK CONSTRAINT [FK__VehicleCe__RiskI__52793849]
GO
ALTER TABLE [dbo].[VehicleCertificates]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCe__RiskI__59C55456] FOREIGN KEY([RiskItemNo])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[VehicleCertificates] CHECK CONSTRAINT [FK__VehicleCe__RiskI__59C55456]
GO
ALTER TABLE [dbo].[VehicleCertificates]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCe__Signe__19DFD96B] FOREIGN KEY([approvedby])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[VehicleCertificates] CHECK CONSTRAINT [FK__VehicleCe__Signe__19DFD96B]
GO
ALTER TABLE [dbo].[VehicleCertificates]  WITH CHECK ADD  CONSTRAINT [FK_LastModefiedBy_User] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[VehicleCertificates] CHECK CONSTRAINT [FK_LastModefiedBy_User]
GO
ALTER TABLE [dbo].[VehicleCertificates]  WITH CHECK ADD  CONSTRAINT [FK_LastPrintBy_User] FOREIGN KEY([LastPrintedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[VehicleCertificates] CHECK CONSTRAINT [FK_LastPrintBy_User]
GO
ALTER TABLE [dbo].[VehicleCertificates]  WITH CHECK ADD  CONSTRAINT [FK_Policy_Cert] FOREIGN KEY([PolicyID])
REFERENCES [dbo].[Policies] ([PolicyID])
GO
ALTER TABLE [dbo].[VehicleCertificates] CHECK CONSTRAINT [FK_Policy_Cert]
GO
ALTER TABLE [dbo].[VehicleCertificates]  WITH CHECK ADD  CONSTRAINT [FK_VehicleCertificates_VehicleCertificates] FOREIGN KEY([VehicleCertificateID])
REFERENCES [dbo].[VehicleCertificates] ([VehicleCertificateID])
GO
ALTER TABLE [dbo].[VehicleCertificates] CHECK CONSTRAINT [FK_VehicleCertificates_VehicleCertificates]
GO
ALTER TABLE [dbo].[VehicleCertificates]  WITH CHECK ADD  CONSTRAINT [FK_VehicleCertificates_Wording] FOREIGN KEY([WordingTemplate])
REFERENCES [dbo].[Wording] ([ID])
GO
ALTER TABLE [dbo].[VehicleCertificates] CHECK CONSTRAINT [FK_VehicleCertificates_Wording]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCo__Cance__05F8DC4F] FOREIGN KEY([CancelledBy])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK__VehicleCo__Cance__05F8DC4F]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCo__Compa__06ED0088] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK__VehicleCo__Compa__06ED0088]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCo__Compa__46B27FE2] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK__VehicleCo__Compa__46B27FE2]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCo__Compa__4BCC3ABA] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([CompanyID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK__VehicleCo__Compa__4BCC3ABA]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCo__Creat__07E124C1] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK__VehicleCo__Creat__07E124C1]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCo__Creat__47A6A41B] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK__VehicleCo__Creat__47A6A41B]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCo__Creat__4CC05EF3] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK__VehicleCo__Creat__4CC05EF3]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCo__LastP__08D548FA] FOREIGN KEY([LastPrintedBy])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK__VehicleCo__LastP__08D548FA]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCo__LastP__489AC854] FOREIGN KEY([LastPrintedBy])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK__VehicleCo__LastP__489AC854]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCo__LastP__4DB4832C] FOREIGN KEY([LastPrintedBy])
REFERENCES [dbo].[People] ([PeopleID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK__VehicleCo__LastP__4DB4832C]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCo__RiskI__09C96D33] FOREIGN KEY([RiskItemNo])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK__VehicleCo__RiskI__09C96D33]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCo__RiskI__498EEC8D] FOREIGN KEY([RiskItemNo])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK__VehicleCo__RiskI__498EEC8D]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK__VehicleCo__RiskI__4EA8A765] FOREIGN KEY([RiskItemNo])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK__VehicleCo__RiskI__4EA8A765]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK_approve_user] FOREIGN KEY([approvedby])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK_approve_user]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK_Policy_CNote] FOREIGN KEY([PolicyID])
REFERENCES [dbo].[Policies] ([PolicyID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK_Policy_CNote]
GO
ALTER TABLE [dbo].[VehicleCoverNote]  WITH CHECK ADD  CONSTRAINT [FK_VehicleCoverNote_Wording] FOREIGN KEY([WordingTemplate])
REFERENCES [dbo].[Wording] ([ID])
GO
ALTER TABLE [dbo].[VehicleCoverNote] CHECK CONSTRAINT [FK_VehicleCoverNote_Wording]
GO
ALTER TABLE [dbo].[VehicleHandDriveLookUp]  WITH CHECK ADD  CONSTRAINT [FKeeys] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[VehicleHandDriveLookUp] CHECK CONSTRAINT [FKeeys]
GO
ALTER TABLE [dbo].[VehicleHandDriveLookUp]  WITH CHECK ADD  CONSTRAINT [FKeeyss] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[VehicleHandDriveLookUp] CHECK CONSTRAINT [FKeeyss]
GO
ALTER TABLE [dbo].[Vehicles]  WITH CHECK ADD FOREIGN KEY([HandDrive])
REFERENCES [dbo].[VehicleHandDriveLookUp] ([VHDLID])
GO
ALTER TABLE [dbo].[Vehicles]  WITH CHECK ADD FOREIGN KEY([HandDrive])
REFERENCES [dbo].[VehicleHandDriveLookUp] ([VHDLID])
GO
ALTER TABLE [dbo].[Vehicles]  WITH CHECK ADD FOREIGN KEY([HandDrive])
REFERENCES [dbo].[VehicleHandDriveLookUp] ([VHDLID])
GO
ALTER TABLE [dbo].[Vehicles]  WITH CHECK ADD FOREIGN KEY([MainDriver])
REFERENCES [dbo].[Drivers] ([DriverID])
GO
ALTER TABLE [dbo].[Vehicles]  WITH CHECK ADD FOREIGN KEY([MainDriver])
REFERENCES [dbo].[Drivers] ([DriverID])
GO
ALTER TABLE [dbo].[Vehicles]  WITH CHECK ADD FOREIGN KEY([MainDriver])
REFERENCES [dbo].[Drivers] ([DriverID])
GO
ALTER TABLE [dbo].[Vehicles]  WITH CHECK ADD FOREIGN KEY([TransmissionType])
REFERENCES [dbo].[TransmissionTypeLookUp] ([TransmisionTypeID])
GO
ALTER TABLE [dbo].[Vehicles]  WITH CHECK ADD FOREIGN KEY([TransmissionType])
REFERENCES [dbo].[TransmissionTypeLookUp] ([TransmisionTypeID])
GO
ALTER TABLE [dbo].[Vehicles]  WITH CHECK ADD FOREIGN KEY([TransmissionType])
REFERENCES [dbo].[TransmissionTypeLookUp] ([TransmisionTypeID])
GO
ALTER TABLE [dbo].[Vehicles]  WITH CHECK ADD  CONSTRAINT [FK_Vehicles_Usages] FOREIGN KEY([Usage])
REFERENCES [dbo].[Usages] ([ID])
GO
ALTER TABLE [dbo].[Vehicles] CHECK CONSTRAINT [FK_Vehicles_Usages]
GO
ALTER TABLE [dbo].[Vehicles]  WITH CHECK ADD  CONSTRAINT [foreignK] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Vehicles] CHECK CONSTRAINT [foreignK]
GO
ALTER TABLE [dbo].[Vehicles]  WITH CHECK ADD  CONSTRAINT [forKeys] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Vehicles] CHECK CONSTRAINT [forKeys]
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy]  WITH CHECK ADD  CONSTRAINT [eee] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy] CHECK CONSTRAINT [eee]
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy]  WITH CHECK ADD  CONSTRAINT [eyss] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy] CHECK CONSTRAINT [eyss]
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy]  WITH CHECK ADD  CONSTRAINT [FK__VehiclesU__Polic__0ABD916C] FOREIGN KEY([PolicyID])
REFERENCES [dbo].[Policies] ([PolicyID])
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy] CHECK CONSTRAINT [FK__VehiclesU__Polic__0ABD916C]
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy]  WITH CHECK ADD  CONSTRAINT [FK__VehiclesU__Polic__4E53A1AA] FOREIGN KEY([PolicyID])
REFERENCES [dbo].[Policies] ([PolicyID])
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy] CHECK CONSTRAINT [FK__VehiclesU__Polic__4E53A1AA]
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy]  WITH CHECK ADD  CONSTRAINT [FK__VehiclesU__Polic__4F9CCB9E] FOREIGN KEY([PolicyID])
REFERENCES [dbo].[Policies] ([PolicyID])
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy] CHECK CONSTRAINT [FK__VehiclesU__Polic__4F9CCB9E]
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy]  WITH CHECK ADD FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy]  WITH CHECK ADD FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy]  WITH CHECK ADD FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicles] ([VehicleID])
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy]  WITH CHECK ADD  CONSTRAINT [FK_mort] FOREIGN KEY([mortgagee])
REFERENCES [dbo].[Mortgagees] ([MID])
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy] CHECK CONSTRAINT [FK_mort]
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy]  WITH CHECK ADD  CONSTRAINT [FK_VehiclesUnderPolicy_Usages] FOREIGN KEY([VehicleUsage])
REFERENCES [dbo].[Usages] ([ID])
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy] CHECK CONSTRAINT [FK_VehiclesUnderPolicy_Usages]
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy]  WITH CHECK ADD  CONSTRAINT [FK_VehiclesUnderPolicy_Users] FOREIGN KEY([CancelledBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[VehiclesUnderPolicy] CHECK CONSTRAINT [FK_VehiclesUnderPolicy_Users]
GO
ALTER TABLE [dbo].[Wording]  WITH CHECK ADD  CONSTRAINT [FK__Wording__Created__3943762B] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Wording] CHECK CONSTRAINT [FK__Wording__Created__3943762B]
GO
ALTER TABLE [dbo].[Wording]  WITH CHECK ADD  CONSTRAINT [FK_ModifiedBy_User] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[Wording] CHECK CONSTRAINT [FK_ModifiedBy_User]
GO
ALTER TABLE [dbo].[Wording]  WITH CHECK ADD  CONSTRAINT [FK_UsageID] FOREIGN KEY([usageid])
REFERENCES [dbo].[Usages] ([ID])
GO
ALTER TABLE [dbo].[Wording] CHECK CONSTRAINT [FK_UsageID]
GO
ALTER TABLE [dbo].[Wording]  WITH CHECK ADD  CONSTRAINT [FK_WOrd_PCover] FOREIGN KEY([PolicyCover])
REFERENCES [dbo].[PolicyCovers] ([ID])
GO
ALTER TABLE [dbo].[Wording] CHECK CONSTRAINT [FK_WOrd_PCover]
GO
ALTER TABLE [dbo].[ZipCodes]  WITH CHECK ADD  CONSTRAINT [FK_ZipCodes_CreatedBy_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[ZipCodes] CHECK CONSTRAINT [FK_ZipCodes_CreatedBy_Users]
GO
ALTER TABLE [dbo].[ZipCodes]  WITH CHECK ADD  CONSTRAINT [FK_ZipCodes_LastModifiedBy_Users] FOREIGN KEY([LastModifiedBy])
REFERENCES [dbo].[Users] ([UID])
GO
ALTER TABLE [dbo].[ZipCodes] CHECK CONSTRAINT [FK_ZipCodes_LastModifiedBy_Users]
GO
/****** Object:  StoredProcedure [dbo].[ActivateEmployee]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ActivateEmployee]
	-- Add the parameters for the stored procedure here
	@empid int,
	@updateuid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF NOT EXISTS (SELECT EmployeeID FROM Employees WHERE PersonID = (SELECT PersonID FROM Employees WHERE EmployeeID = @empid) AND Active = 1)
	BEGIN
		DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES
		SELECT @origdata = 'Employee:' +  CONVERT(varchar(MAX),@empid) +  ';PersonID:' + CONVERT(varchar(MAX),People.PeopleID) + ';Person:' + People.FName + ' ' + People.MName + ' '  + People.LName  + ';CompanyID:' + CONVERT(varchar(MAX),Company.CompanyID) + ';CompanyName:' + Company.CompanyName
		FROM Employees	JOIN Company ON Employees.CompanyID = Company.CompanyID
						JOIN People ON Employees.PersonID = People.PeopleID	
		WHERE Employees.EmployeeID = @empid;

		UPDATE Employees SET Active = 1 WHERE EmployeeID = @empid;

		SELECT @details = 'The record ' + CONVERT(varchar(MAX),@empid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@updateuid),
			   @newdata = 'Employee:' +  CONVERT(varchar(MAX),@empid) +  ';PersonID:' + CONVERT(varchar(MAX),People.PeopleID) + ';Person:' + People.FName + ' ' + People.MName + ' '  + People.LName  + ';CompanyID:' + CONVERT(varchar(MAX),Company.CompanyID) + ';CompanyName:' + Company.CompanyName
		FROM Employees	JOIN Company ON Employees.CompanyID = Company.CompanyID
						JOIN People ON Employees.PersonID = People.PeopleID	
		WHERE Employees.EmployeeID = @empid;
		EXEC LogAudit 'update', @updateuid , @details, 'Employees', @empid, @origdata, @newdata, 'update';

		SELECT CAST(1 as bit) as result;
		
	END
	ELSE SELECT CAST(0 as bit) as result;

END


GO
/****** Object:  StoredProcedure [dbo].[ActivateUser]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ActivateUser]
	-- Add the parameters for the stored procedure here
	@UID int,
	@editedBy int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	DECLARE @trn varchar(MAX);
	SELECT @trn =  p.TRN 
	FROM Users u LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID
				 LEFT JOIN People p ON e.PersonID = p.PeopleID
	WHERE u.UID = @UID;

	IF NOT EXISTS (SELECT [UID] FROM Users u LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID LEFT JOIN People p ON e.PersonID = p.PeopleID WHERE p.TRN = @trn AND u.UID != @UID AND u.active = 1)
	BEGIN		
		DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES
		SELECT @origdata = 'username:' +  username +  ';active:' + CONVERT(varchar(MAX),active) FROM Users WHERE [UID] = @UID;

		UPDATE Users SET active = 1, LastModifiedBy = @editedBy, faultylogins = 0 WHERE [UID] = @UID;

		SELECT @details = 'The record ' + CONVERT(varchar(MAX),[UID]) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy),
			   @newdata = 'username:' +  username +  ';active:' + CONVERT(varchar(MAX),active)
		FROM Users WHERE [UID] = @UID;
		
		EXEC LogAudit 'update', @editedBy , @details, 'Users', @UID, @origdata, @newdata, 'update';

		SELECT CAST(1 as bit) as result;
	END
	ELSE
	BEGIN
		SELECT CAST(1 as bit) as result;
	END
END


GO
/****** Object:  StoredProcedure [dbo].[AddAddressToSystem]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 03/06/14
-- Description:	Adding a new address to the database
-- =============================================
CREATE PROCEDURE [dbo].[AddAddressToSystem]
	-- Add the parameters for the stored procedure here

	@road Varchar(250),
	@roadtype Varchar(50),
	@zip Varchar(50) = null,
	@city Varchar(250),
	@parish int = null,
	@country Varchar(250),
	@roadNum Varchar (50),
	@aptNumber varchar (250),
	@parishmap int = null,
	@createdby int = null

   AS
   BEGIN
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	
	DECLARE @newRoadNum varchar(50), @newRoadID int, @newRoadTypeID int, @newZipID int, @newCityID int, @newCountryID int;
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	
	IF @parish = 0
		SET @parish = null;

	IF EXISTS (SELECT RID FROM RoadName WHERE RoadName = @road)   -- The road has been entered before
	  BEGIN
	    SELECT @newRoadID = RID FROM RoadName WHERE RoadName = @road;
	  END
	ELSE    --the road is not in the system as yet
		BEGIN
			INSERT INTO RoadName (RoadName, CreatedBy, LastModifiedBy) VALUES (@road, @createdby, @createdby);
			SET @newRoadID = @@IDENTITY;     
			SELECT @details = 'The record ' + CONVERT(varchar(MAX),@newRoadID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby),
				   @origdata = null,
				   @newdata = 'RoadNumber:' + CONVERT(varchar(MAX),@newRoadID);
			EXEC LogAudit 'insert', @createdby , @details, 'RoadName', @newRoadID, @origdata, @newdata, 'insert';
		END


	IF EXISTS (SELECT RTID FROM RoadType WHERE RoadType = @roadtype)
	  BEGIN
	     SELECT @newRoadTypeID = RTID FROM RoadType WHERE RoadType = @roadtype;  
	  END

   ELSE
	    BEGIN
	       
		   INSERT INTO RoadType (RoadType, CreatedBy, LastModifiedBy) VALUES (@roadtype, @createdby, @createdby);
	       SET @newRoadTypeID = @@IDENTITY;   
			  SELECT @details = ('The record ' + CONVERT(varchar(MAX),@newRoadTypeID) + ' was modified from the database by ' +CONVERT(varchar(MAX),@createdby)),
				   @origdata = null,
				   @newdata = ('RoadType:' + CONVERT(varchar(MAX),@newRoadTypeID));
			EXEC LogAudit 'insert', @createdby , @details, 'RoadType', @newRoadTypeID, @origdata, @newdata, 'insert';
		END

	
	IF EXISTS (SELECT ZCID FROM ZipCodes WHERE ZipCode = @zip)
	   BEGIN
	      SELECT @newZipID  = ZCID FROM ZipCodes WHERE ZipCode = @zip;
	   END

	ELSE
	    BEGIN
			INSERT INTO ZipCodes (ZipCode, CreatedBy, LastModifiedBy) VALUES (@zip, @createdby, @createdby);
			SET @newZipID  = @@IDENTITY;   

			SELECT @details = ('The record ' + CONVERT(varchar(MAX),@newZipID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby)),
				   @origdata = null,
				   @newdata = ('ZipCode:' + CONVERT(varchar(MAX),@newZipID));
			EXEC LogAudit 'insert', @createdby , @details, 'ZipCodes', @newZipID, @origdata, @newdata, 'insert';
		END
	

	IF EXISTS (SELECT CID FROM  Cities WHERE City = @city)
	  BEGIN
	      SELECT @newCityID = CID FROM Cities WHERE  City = @city;
	  END

  ELSE
	  BEGIN
	      INSERT INTO Cities (City, CreatedBy, LastModifiedBy) VALUES (@city, @createdby, @createdby);
          SET @newCityID = @@IDENTITY;  

		 SELECT @details = ('The record ' + CONVERT(varchar(MAX),@newCityID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby)),
				@origdata = null,
				@newdata = ('City:' + CONVERT(varchar(MAX),@newCityID));
		 EXEC LogAudit 'insert', @createdby , @details, 'Cities', @newCityID, @origdata, @newdata, 'insert';
	  END

	IF EXISTS (SELECT CID FROM Countries WHERE CountryName = @country)
	  BEGIN
	      SELECT @newCountryID = CID FROM Countries WHERE CountryName= @country
	  END

   ELSE 
	  BEGIN
	    INSERT INTO Countries (CountryName, CreatedBy, LastModifiedBy) VALUES (@country, @createdby, @createdby);
	    SET @newCountryID = @@IDENTITY;  

		SELECT @details = ('The record ' + CONVERT(varchar(MAX),@newCountryID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby)),
				@origdata = null,
				@newdata = ('Country:' + CONVERT(varchar(MAX),@newCountryID));
		EXEC LogAudit 'insert', @createdby , @details, 'Countries', @newCountryID, @origdata, @newdata, 'insert';
	  END

	--checking to see if such an address already exixts in the system

	 IF EXISTS (SELECT AID FROM [Address] WHERE RoadName= @newRoadID 
	                                      AND RoadType= @newRoadTypeID  
										  AND ZipCode= @newZipID  
										  AND City = @newCityID 
										  AND Parish = @parish
										  AND Country = @newCountryID
										  AND RoadNumber = @roadNum 
										  AND AptNumber = @aptNumber)

	BEGIN  
		 SELECT AID FROM [Address] WHERE RoadName= @newRoadID 
	                                      AND RoadType= @newRoadTypeID  
										  AND ZipCode= @newZipID  
										  AND City = @newCityID 
										  AND Parish = @parish
										  AND Country = @newCountryID
										  AND RoadNumber = @roadNum
										  AND AptNumber = @aptNumber ;

	END


	ELSE --that specific address is yet to be added to the system
	BEGIN
	   INSERT INTO [Address] (RoadName, RoadType, ZipCode, City, Country, RoadNumber, CreatedBy, LastModifiedBy, Parish, ParishMap, AptNumber) 
		VALUES (@newRoadID, @newRoadTypeID, @newZipID , @newCityID, @newCountryID, @roadNum, @createdby, @createdby, 
				(CASE WHEN (@parish IS NOT NULL AND @parish != 0) THEN @parish
					  WHEN (@parish IS NULL AND (@parishmap = 0 OR @parishmap IS NULL)) THEN NULL
					  WHEN (@parish IS NULL AND (@parishmap != 0 AND @parishmap IS NOT NULL) AND (SELECT COUNT(*) FROM MappingTable WHERE ID = @parishmap AND RefID IS NOT NULL) > 0) THEN (SELECT RefID FROM MappingTable WHERE ID = @parishmap)
					  ELSE NULL
				END),
				(CASE WHEN (@parishmap = 0 OR @parishmap IS NULL) THEN NULL ELSE @parishmap END), @aptNumber);
	  SELECT @@IDENTITY as AID;
  
	   
	   
	   SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby)),
			  @origdata = null,
			  @newdata = ('ApartmentNumber:' + CONVERT(varchar(MAX),@aptNumber) + ';RoadNumber:' + CONVERT(varchar(MAX),@roadNum) + ';RoadName:' + CONVERT(varchar(MAX),@newRoadID) + ';RoadType:' + CONVERT(varchar(MAX),@newRoadTypeID) + ';ZipCode:' + CONVERT(varchar(MAX),@newZipID) + ';City:' + CONVERT(varchar(MAX),@newCityID) + ';Country:' + CONVERT(varchar(MAX),@newCountryID));
	   EXEC LogAudit 'insert', @createdby , @details, 'Address', @@IDENTITY, @origdata, @newdata, 'insert';
	   
	END

END
GO
/****** Object:  StoredProcedure [dbo].[AddAuthorizedDriver]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddAuthorizedDriver]
	-- Add the parameters for the stored procedure here
	@polID int,
	@drivID int,
	@vehID int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO AuthorizedDrivers (VehicleID, DriverID, CreatedBy, LastModifiedBy, PolicyID) VALUES (@vehID, @drivID, @uid, @uid, @polID);
END


GO
/****** Object:  StoredProcedure [dbo].[AddChildToSuperGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddChildToSuperGroup]
	-- Add the parameters for the stored procedure here
	@PGID int,
	@CGID int,
	@createdby int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF NOT EXISTS (SELECT SGID FROM SuperGroupMapping WHERE PGID = @PGID AND CGID = @CGID)
	BEGIN
		INSERT INTO SuperGroupMapping (PGID, CGID) VALUES (@PGID, @CGID);
		INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
		SELECT 'insert', @createdby, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby),
				'SuperGroupMapping', @@IDENTITY, null, 'PGID:' + CONVERT(varchar(MAX),@PGID) + ';CGID:' + CONVERT(varchar(MAX),@CGID), 'insert';
	END
END


GO
/****** Object:  StoredProcedure [dbo].[AddCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: <Create Date,,>
-- Description:	Adding a new company to the system
-- =============================================
CREATE PROCEDURE [dbo].[AddCompany]
	-- Add the parameters for the stored procedure here
	@IAJID varchar (max) = null,
	@ediid varchar(max) = null,
	@CompanyName varchar(250) = null,
	@floginlimit int = null,
	@CompanyType varchar(50) = null,
	@AddressID int = null,
	@trn varchar(13),
	@enable bit = null,
	@createdby int
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @result bit; DECLARE @wording int; 	DECLARE  @compType int;
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	-- These tests are for companies that are not insurers/brokers/agents (regular companies added to the system)-which will have no company type

	IF (@CompanyType = ' ')
	BEGIN 
		SET @compType = null;
	END
	ELSE
	BEGIN
		SELECT @compType = ID FROM CompanyTypes WHERE [Type] = @CompanyType;
	END


	--IF EXISTS (SELECT AID FROM [Address] WHERE AID = @AddressID)
	--BEGIN

		IF NOT EXISTS(SELECT CompanyID FROM Company WHERE IAJID = @IAJID OR CompanyName = @CompanyName OR TRN = @trn)
		BEGIN
			INSERT INTO Company (IAJID, EDIID, CompanyName, faultyloginlimit, [Type], CompanyAddress, CreatedBy, LastModifiedBy, TRN, [Enabled])
					VALUES ((CASE WHEN(@IAJID IS NULL OR @IAJID = '') THEN NULL ELSE @IAJID END), 
							(CASE WHEN(@ediid IS NULL OR @ediid = '') THEN NULL ELSE @ediid END), 
							(CASE WHEN(@CompanyName IS NULL OR @CompanyName = '') THEN NULL ELSE @CompanyName END), 
							(CASE WHEN (@floginlimit IS NULL) THEN 0 ELSE @floginlimit END), 
							(CASE WHEN (@compType IS NULL OR @compType = 0) THEN NULL ELSE @compType END), 
							(CASE WHEN (@AddressID IS NULL OR @AddressID = 0) THEN NULL ELSE @AddressID END), 
							@createdby, 
							@createdby, 
							@trn, 
							@enable); 
			

			SET @result = 1;
			SELECT @@IDENTITY AS [ID], @result as result;


			SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby),
					@origdata = null,
					@newdata = 'IAJID: ' + @IAJID + ';CompanyName: ' + CONVERT(varchar(MAX),@CompanyName) + ';CompanyType: ' + CONVERT(varchar(MAX),@CompanyType)
								+'FailedLoginLimit: ' + CONVERT(varchar(MAX),@floginlimit) + ';AddressId: '+ CONVERT(varchar(MAX),@AddressID);
							
			EXEC LogAudit 'insert', @createdby , @details, 'Company', @@IDENTITY, @origdata, @newdata, 'insert';

		END

		ELSE
		BEGIN
			SET @result = 0;
			SELECT @result as result;
		END

	--END
	--ELSE
	--BEGIN
	--	SET @result = 0;
	--	SELECT @result as result;
	--END
END

GO
/****** Object:  StoredProcedure [dbo].[AddCountry]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddCountry]
	-- Add the parameters for the stored procedure here
	@countryname varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS (SELECT CID FROM Countries WHERE CountryName = @countryname)
	BEGIN
		SELECT 0 AS result;
	END
	ELSE
	BEGIN
		INSERT INTO Countries (CountryName) VALUES (@countryname);
		SELECT 1 AS result;
	END
	
END


GO
/****** Object:  StoredProcedure [dbo].[AddDriver]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 23/06/2014
-- Description:	Adding a driver to the system
-- =============================================

CREATE PROCEDURE [dbo].[AddDriver]
	-- Add the parameters for the stored procedure here
	@personID int,
	@createdBy int,
	@dateissued Date = null,
	@expirydate Date = null,
	@class varchar(100) = null,
	@licenseno varchar(100) = null

AS
BEGIN
	SET NOCOUNT ON;

		--IF NOT EXISTS (SELECT DriverID FROM Drivers WHERE PersonID = @personID)
		
		DECLARE @did int;
		SELECT @did = DriverID FROM Drivers WHERE PersonID = @personID;
		IF NOT EXISTS (SELECT DriverID FROM Drivers WHERE PersonID = @personID)
			BEGIN
				DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
		
				INSERT INTO Drivers (PersonID, CreatedBy, CreatedOn, LastModifiedBy, DateLicenseIssued, DateLicenseExpires, Class, LicenseNo) 
				VALUES (@personID, 
						@createdBy, 
						CURRENT_TIMESTAMP, 
						@createdBy, 
						(CASE WHEN(@dateissued IS NULL) THEN NULL ELSE @dateissued END),
						(CASE WHEN(@expirydate IS NULL) THEN NULL ELSE @expirydate END),
						(CASE WHEN(@class IS NULL OR @class = '') THEN NULL ELSE @class END),
						(CASE WHEN(@licenseno IS NULL OR @licenseno = '') THEN NULL ELSE @licenseno END));

				SELECT @@IDENTITY AS [ID];

				SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy),
					   @origdata = null,  
					   @newdata = 'PersonID: '+ CONVERT(varchar(MAX),@personID) +';DriverId: '+ CONVERT(varchar(MAX),@@IDENTITY)+ +';dateLicenseissue: ' +CONVERT(varchar(MAX),(CASE WHEN (DateLicenseIssued = '' OR DateLicenseIssued IS NULL) THEN '' ELSE DateLicenseIssued END))
					              +';LicenseExpirydate: ' +CONVERT(varchar(MAX),(CASE WHEN (DateLicenseExpires = '' OR DateLicenseExpires IS NULL) THEN '' ELSE DateLicenseExpires END))
								  +';class: ' +CONVERT(varchar(MAX),Class) +';licenseno: ' +CONVERT(varchar(MAX),LicenseNo)
					FROM Drivers WHERE DriverID = @@IDENTITY;
				EXEC LogAudit 'insert', @createdBy , @details, 'Drivers', @@IDENTITY, @origdata, @newdata, 'insert';
			END
		ELSE
			BEGIN 

			    SELECT @origdata = 'PersonID: '+ CONVERT(varchar(MAX),PersonID) +';DriverId: '+ CONVERT(varchar(MAX),DriverID)+';dateissue: ' +CONVERT(varchar(MAX),DateLicenseIssued)+
					               ';expirydate: ' +CONVERT(varchar(MAX),DateLicenseExpires) +';class: ' +CONVERT(varchar(MAX),Class) +';licenseno: ' +CONVERT(varchar(MAX),LicenseNo)
				FROM Drivers WHERE DriverID = @did;


				IF (@dateissued IS NOT NULL)						UPDATE Drivers SET DateLicenseIssued = @dateissued WHERE DriverID = @did;
				IF (@expirydate IS NOT NULL)						UPDATE Drivers SET DateLicenseExpires = @expirydate WHERE DriverID = @did;
				IF (@class IS NOT NULL AND @class != '')			UPDATE Drivers SET Class = @class WHERE DriverID = @did;
				IF (@licenseno IS NOT NULL AND @licenseno != '' )	UPDATE Drivers SET LicenseNo = @licenseno WHERE DriverID = @did;


				SELECT @details = 'The record ' + CONVERT(varchar(MAX),@did) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy),
					   @newdata = 'PersonID: '+ CONVERT(varchar(MAX),PersonID) +';DriverId: '+ CONVERT(varchar(MAX),DriverID)+';dateLicenseissue: ' +CONVERT(varchar(MAX),(CASE WHEN (DateLicenseIssued = '' OR DateLicenseIssued IS NULL) THEN '' ELSE DateLicenseIssued END))
					              +';LicenseExpirydate: ' +CONVERT(varchar(MAX),(CASE WHEN (DateLicenseExpires = '' OR DateLicenseExpires IS NULL) THEN '' ELSE DateLicenseExpires END)) 
								  +';class: ' +CONVERT(varchar(MAX),Class) +';licenseno: ' +CONVERT(varchar(MAX),LicenseNo)
				FROM Drivers WHERE DriverID = @did;
				EXEC LogAudit 'update', @createdBy , @details, 'Drivers', @did, @origdata, @newdata, 'update';  
				 
		     	SELECT @did [ID];

			END


END
GO
/****** Object:  StoredProcedure [dbo].[AddDriverRelation]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 28/08/2014
-- Description:	Adding the relation of selected drivers of a vehicle , to the vehicle's main driver
-- =============================================
CREATE PROCEDURE [dbo].[AddDriverRelation]
	
	@perId int,
	@chassisNo varchar (MAX),
	@polId int,
	@relation varchar (250),
	@EditedBy int

AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); 
	DECLARE @driverId int; DECLARE @vehId int; DECLARE @edid int;


	SELECT @driverId = DriverID FROM Drivers WHERE PersonID = @perId;
	SELECT @vehId = VehicleID FROM Vehicles WHERE ChassisNo = @chassisNo; 

	--checks to see if the driver is an authorized Driver

	IF EXISTS (SELECT EDID FROM AuthorizedDrivers WHERE DriverID = @driverId AND VehicleID = @vehId AND PolicyID = @polId)
	BEGIN
	        SELECT  @edid =  EDID FROM AuthorizedDrivers WHERE DriverID = @driverId AND VehicleID = @vehId; 

			SELECT @origdata = 'VehicleChassis: ' + @chassisNo + ';DriverID: ' + CONVERT(varchar(MAX),DriverID) 
							  +';Relation: ' + Relation + ';PolicyID: ' + CONVERT(varchar(MAX),@polId) FROM AuthorizedDrivers WHERE VehicleID = @vehId AND DriverID = @driverId;


			UPDATE AuthorizedDrivers SET Relation = @relation, LastModifiedBy= @EditedBy WHERE VehicleID = @vehId AND DriverID = @driverId AND PolicyID = @polId;


			SET @details = ('The record ' + CONVERT(varchar(MAX), @edid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@EditedBy));
		    SET @newdata = 'Relation:' + @relation;

			EXEC LogAudit 'update', @EditedBy , @details, 'AuthorizedDrivers', @edid, @origdata, @newdata, 'update';

	END

	--checks to see if the driver is an excepted Driver

	IF EXISTS (SELECT EDID FROM ExceptedDrivers WHERE DriverID = @driverId AND VehicleID = @vehId AND PolicyID = @polId)
	BEGIN

	        SELECT  @edid =  EDID FROM ExceptedDrivers WHERE DriverID = @driverId AND VehicleID = @vehId; 

			SELECT @origdata = 'VehicleChassis:' + @chassisNo + ';DriverID:' + CONVERT(varchar(MAX),DriverID) 
							  +';Relation:' + Relation +';PolicyID: ' + CONVERT(varchar(MAX),@polId) FROM ExceptedDrivers WHERE VehicleID = @vehId AND DriverID = @driverId;


		    UPDATE ExceptedDrivers SET Relation = @relation WHERE VehicleID = @vehId AND DriverID = @driverId AND PolicyID = @polId;

	  		SET @details = ('The record ' + CONVERT(varchar(MAX), @edid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@EditedBy));
			SET @newdata = 'Relation:' + @relation;

			EXEC LogAudit 'update', @EditedBy , @details, 'ExceptedDrivers', @edid, @origdata, @newdata, 'update';

	END


	--checks to see if the driver is an excluded Driver

	IF EXISTS (SELECT EDID FROM ExcludedDrivers WHERE DriverID = @driverId AND VehicleID = @vehId AND PolicyID = @polId)
	BEGIN

	        SELECT  @edid =  EDID FROM ExcludedDrivers WHERE DriverID = @driverId AND VehicleID = @vehId; 

			SELECT @origdata = 'VehicleChassis:' + @chassisNo + ';DriverID:' + CONVERT(varchar(MAX),DriverID) 
							  +';Relation:' + Relation + ';PolicyID: ' + CONVERT(varchar(MAX),@polId) FROM ExcludedDrivers WHERE VehicleID = @vehId AND DriverID = @driverId;


		    UPDATE ExcludedDrivers SET Relation = @relation WHERE VehicleID = @vehId AND DriverID = @driverId AND PolicyID = @polId;

		    SET @details = ('The record ' + CONVERT(varchar(MAX), @edid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@EditedBy));
			SET @newdata = 'Relation:' + @relation;

			EXEC LogAudit 'update', @EditedBy , @details, 'ExcludedDrivers', @edid, @origdata, @newdata, 'update';
	END
	
	
END

GO
/****** Object:  StoredProcedure [dbo].[AddDriversToVehicle]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 23/07/2014
-- Description:	Searching for a vehicle, given a VIN
-- =============================================
CREATE PROCEDURE [dbo].[AddDriversToVehicle]
	-- Add the parameters for the stored procedure here
	@ChassisNo varchar (50),
	@policyID int,
	@DriverID int,
	@driverType int,
	@createdby int

AS
BEGIN
	
	SET NOCOUNT ON;

    DECLARE @result bit; DECLARE @VID int;
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	SELECT @VID = VehicleID FROM Vehicles WHERE ChassisNo = @ChassisNo;

	--//// If the driver is authorized)//// 
	IF (@driverType = 1)
	BEGIN
		IF EXISTS (SELECT VehicleID FROM AuthorizedDrivers WHERE VehicleID = @VID AND DriverID = @DriverID)
		BEGIN
			SET @result = 0; 
			SELECT @result AS [result];
		END
		ELSE
		BEGIN
			INSERT INTO AuthorizedDrivers (VehicleID, DriverID, CreatedBy, CreatedOn, LastModifiedBy, PolicyID) VALUES (@VID, @DriverID, @createdby, CURRENT_TIMESTAMP, @createdby, @policyID);
			   SET @result = 1; 
		    SELECT @result AS [result];

			SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy)),
					   @origdata = null,
					   @newdata = 'VehicleID: ' + CONVERT(varchar(MAX),@VID) + ';DriverID: ' + CONVERT(varchar(MAX),@DriverID);
			EXEC LogAudit 'insert', @createdBy , @details, 'AuthorizedDrivers', @@IDENTITY, @origdata, @newdata, 'insert';
		END

     END

	 --//// If the driver is excepted)//// 

	IF (@driverType = 2)
	BEGIN
		IF EXISTS (SELECT VehicleID FROM ExceptedDrivers WHERE VehicleID = @VID AND DriverID = @DriverID)
		BEGIN
			SET @result = 0; 
			SELECT @result AS [result];
		END
	ELSE
	BEGIN
			INSERT INTO ExceptedDrivers (VehicleID, DriverID, CreatedBy, CreatedOn, LastModifiedBy, PolicyID) VALUES (@VID, @DriverID, @createdby, CURRENT_TIMESTAMP, @createdby, @policyID);
			   SET @result = 1; 
		    SELECT @result AS [result];

			SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy)),
					@origdata = null,
					@newdata = 'VehicleID: ' + CONVERT(varchar(MAX),@VID) + ';DriverID: ' + CONVERT(varchar(MAX),@DriverID);
			EXEC LogAudit 'insert', @createdBy , @details, 'ExceptedDrivers', @@IDENTITY, @origdata, @newdata, 'insert';
		END

     END


	  --//// If the driver is excluded)//// 

	IF (@driverType = 3)
	BEGIN
		IF EXISTS (SELECT VehicleID FROM ExcludedDrivers WHERE VehicleID = @VID AND DriverID = @DriverID)
		BEGIN
			SET @result = 0; 
			SELECT @result AS [result];
		END
		
	ELSE
		BEGIN
			INSERT INTO ExcludedDrivers (VehicleID, DriverID, CreatedBy, CreatedOn, LastModifiedBy, PolicyID) VALUES (@VID, @DriverID, @createdby, CURRENT_TIMESTAMP, @createdby, @policyID);
			   SET @result = 1; 
		    SELECT @result AS [result];

			SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy)),
					   @origdata = null,
					   @newdata = 'VehicleID: ' + CONVERT(varchar(MAX),@VID) + ';DriverID: ' + CONVERT(varchar(MAX),@DriverID);
			EXEC LogAudit 'insert', @createdBy , @details, 'ExcludedDrivers', @@IDENTITY, @origdata, @newdata, 'insert';
		END

     END

END -- final end

GO
/****** Object:  StoredProcedure [dbo].[AddEmail]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 16/06/2014
-- Description:	Adds an email, belonging to a person in the system
-- =============================================

CREATE PROCEDURE [dbo].[AddEmail]
	-- Add the parameters for the stored procedure here
	
	@email varchar (250)
	
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @result bit;
	IF EXISTS (SELECT EID FROM Emails WHERE email = @email)
	BEGIN
		SELECT EID FROM Emails WHERE email = @email; 
	END
	ELSE
	BEGIN
		INSERT INTO Emails (email) VALUES (@email); 

		SELECT @@IDENTITY as EID;

	END
END


GO
/****** Object:  StoredProcedure [dbo].[AddEmailToCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 09/09/2014
-- Description:	Connects a company to an email in the system
-- =============================================

CREATE PROCEDURE [dbo].[AddEmailToCompany]
	
	@companyId int,
	@email varchar(250),
	@primary bit,
	@createdBy int
	
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @emailID int;
	IF NOT EXISTS (SELECT EID FROM Emails WHERE email = @email)
	
	BEGIN
		INSERT INTO Emails (email, CreatedBy, CreatedOn, LastModifiedBy) VALUES (@email, @createdBy, CURRENT_TIMESTAMP, @createdBy);
		SELECT @emailID = @@IDENTITY;
		SELECT @details = 'The record ' + CONVERT(varchar(MAX),@emailID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby),
			   @origdata = null,
			   @newdata = 'email:' + @email;
		EXEC LogAudit 'insert', @createdby , @details, 'Emails', @emailID, @origdata, @newdata, 'insert';
	END

	ELSE 
	  BEGIN
	     SELECT @emailID = EID FROM Emails WHERE email = @email;
	  END

	IF NOT EXISTS (SELECT CTEID FROM CompanyToEmails WHERE EmailID= @emailID AND CompanyID = @companyId)
		BEGIN 
			INSERT INTO CompanyToEmails (CompanyID, EmailID, PrimaryEmail, CreatedBy, CreatedOn, LastModifiedBy) VALUES (@companyId, @emailId, @primary, @createdBy, CURRENT_TIMESTAMP, @createdBy); 
	
			SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby),
				   @origdata = null,
				   @newdata = 'CompanyID:' + CONVERT(varchar(MAX),@companyID) + ';Company:' + Company.CompanyName + ' ' + Company.TRN + ' ' 
							+ ';EmailID:' + CONVERT(varchar(MAX),@emailID) + ';Email:' + @email + ';PrimaryEmail:' + CONVERT(varchar(MAX),@primary)
			FROM Company WHERE Company.CompanyID = @companyID;


			EXEC LogAudit 'insert', @createdby , @details, 'CompanyToEmails', @@IDENTITY, @origdata, @newdata, 'insert';
	   END
END

GO
/****** Object:  StoredProcedure [dbo].[AddEmailToPerson]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 16/06/2014
-- Description:	Connects a person to an email in the system
-- =============================================

CREATE PROCEDURE [dbo].[AddEmailToPerson]
	-- Add the parameters for the stored procedure here
	
	@personId int,
	@email varchar(250),
	@primary bit,
	@createdBy int
	
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @emailID int;
	
	IF NOT EXISTS (SELECT EID FROM Emails WHERE email = @email)
		BEGIN
			INSERT INTO Emails (email, CreatedBy, CreatedOn, LastModifiedBy) VALUES (@email, @createdBy, CURRENT_TIMESTAMP, @createdBy);
			SELECT @emailID = @@IDENTITY;
			SELECT @details = 'The record ' + CONVERT(varchar(MAX),@emailID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby),
				   @origdata = null,
				   @newdata = 'email:' + @email;
			EXEC LogAudit 'insert', @createdby , @details, 'Emails', @emailID, @origdata, @newdata, 'insert';
		END

	ELSE 
        BEGIN
	      SELECT @emailID = EID FROM Emails WHERE email = @email;
		END

		IF NOT EXISTS (SELECT PTEID FROM PeopleToEmails WHERE EmailID= @emailID AND PeopleID = @personId)
		BEGIN 

			INSERT INTO PeopleToEmails (PeopleID, EmailID, [Primary], CreatedBy, CreatedOn, LastModifiedBy) VALUES (@personId, @emailId, @primary, @createdBy, CURRENT_TIMESTAMP, @createdBy); 
			SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby),
				   @origdata = null,
				   @newdata = 'PersonID:' + CONVERT(varchar(MAX),@personID) + ';Person:' + People.FName + ' ' + People.MName + ' ' + People.LName + ';EmailID:' + CONVERT(varchar(MAX),@emailID) + ';Email:' + Emails.email
			FROM People JOIN PeopleToEmails ON People.PeopleID = PeopleToEmails.PeopleID
						JOIN Emails ON PeopleToEmails.EmailID = Emails.EID
			WHERE People.PeopleID = @personID AND Emails.EID = @emailID;
			EXEC LogAudit 'insert', @createdby , @details, 'PeopleToEmails', @@IDENTITY, @origdata, @newdata, 'insert';
		END


   END

GO
/****** Object:  StoredProcedure [dbo].[AddEmployee]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 09/06/2014
-- Description:	Adds an employee to the system
-- =============================================

 CREATE PROCEDURE [dbo].[AddEmployee]
	-- Add the parameters for the stored procedure here
	@personId int,
	@companyId int,
	@createdby int
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO Employees (PersonID, CompanyID) VALUES (@personId, @companyId);
	SELECT @@IDENTITY AS [ID];
	
	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 
		'delete', 
		@createdby, 
		CURRENT_TIMESTAMP,  
		'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby), 
		'Employees',
		@@IDENTITY,
		'Employee:' +  CONVERT(varchar(MAX),@@IDENTITY) +  ';PersonID:' + CONVERT(varchar(MAX),People.PeopleID) + ';Person:' + People.FName + ' ' + People.MName + ' '  + People.LName  + ';CompanyID:' + CONVERT(varchar(MAX),Company.CompanyID) + ';CompanyName:' + Company.CompanyName,
		null, 
		'delete'
	FROM Employees JOIN Company ON Employees.CompanyID = Company.CompanyID
					JOIN People ON Employees.PersonID = People.PeopleID	
	WHERE Employees.EmployeeID = @@IDENTITY;
	
	
END


GO
/****** Object:  StoredProcedure [dbo].[AddExcepetedDriver]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddExcepetedDriver]
	-- Add the parameters for the stored procedure here
	@polID int,
	@drivID int,
	@vehID int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO ExceptedDrivers (PolicyID, VehicleID, DriverID, CreatedBy, LastModifiedBy) VALUES (@polID, @vehID, @drivID, @uid, @uid);
	
END


GO
/****** Object:  StoredProcedure [dbo].[AddExcludedDriver]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddExcludedDriver]
	-- Add the parameters for the stored procedure here
	@polID int,
	@drivID int,
	@vehID int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	INSERT INTO ExcldExcludedDrivers(VehicleID, DriverID, CreatedBy, LastModifiedBy, PolicyID) VALUES (@vehID, @drivID, @uid, @uid, @polID);
END


GO
/****** Object:  StoredProcedure [dbo].[AddInsuredToPolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date:02/07/2014
-- Description:	Adding an insured to a policy
-- =============================================

CREATE PROCEDURE [dbo].[AddInsuredToPolicy]
	-- Add the parameters for the stored procedure here

	@EDIID int,
	@createdBy int, 
	@policyID int,
	@insuredID int,
	@insuredType int

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	IF (@insuredType = 1)
	BEGIN
		--the insured is a person
	    IF NOT EXISTS(SELECT * FROM PolicyInsured WHERE PolicyID = @policyID AND PersonID = @insuredID)
		BEGIN
			INSERT INTO PolicyInsured (EDIID, CreatedOn,CreatedBy,PolicyID, PersonID, LastModifiedBy) VALUES ((CASE WHEN(@EDIID IS NULL OR @EDIID = 0) THEN NULL ELSE @EDIID END), CURRENT_TIMESTAMP, @createdBy, @policyID, @insuredID, @createdBy);

			SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy)),
					   @origdata = null, 
					   @newdata = ('EDIID:' + CONVERT(varchar(MAX),@EDIID)+ ';PolicyId:' + CONVERT(varchar(MAX),@policyID) + ';InsuredId:' + CONVERT(varchar(MAX),@insuredID) + ';Insured:Individual');
			EXEC LogAudit 'insert', @createdby, @details, 'PolicyInsured',  @@IDENTITY, @origdata, @newdata, 'insert';
		END
	END
    ELSE
	BEGIN
		--the insured is a company
		IF NOT EXISTS(SELECT * FROM PolicyInsured WHERE PolicyID = @policyID AND CompanyID = @insuredID)
		BEGIN
			INSERT INTO PolicyInsured (EDIID, CreatedOn,CreatedBy,PolicyID, CompanyID, LastModifiedBy) VALUES ((CASE WHEN(@EDIID IS NULL OR @EDIID = 0) THEN NULL ELSE @EDIID END), CURRENT_TIMESTAMP, @createdBy, @policyID, @insuredID, @createdBy);

			SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy)),
						@origdata = null, 
						@newdata = ('EDIID:' + CONVERT(varchar(MAX),@EDIID)+ ';PolicyId:' + CONVERT(varchar(MAX),@policyID) + ';InsuredId:' + CONVERT(varchar(MAX),@insuredID) + ';Insured:Company');
			EXEC LogAudit 'insert', @createdby, @details, 'PolicyInsured',  @@IDENTITY, @origdata, @newdata, 'insert';
		END
	END	
END
GO
/****** Object:  StoredProcedure [dbo].[AddIntermediary]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 16/09/2014
-- Description:	Adding an intermediary company to an insurance company
-- =============================================

CREATE PROCEDURE [dbo].[AddIntermediary]

	@CompanyID int,
	@intermediaryCompId int,
	@covernote bit = false,
	@certificate bit = false,
	@enabled bit = false,
	@printlimit int = 0,
	@userID int,
	@intid int = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @id int;

    IF NOT EXISTS (SELECT CTID FROM InsurerToIntermediary WHERE Insurer = @CompanyID AND Intermediary = @intermediaryCompId)
	BEGIN
		
		SELECT @origdata = null;
		
		INSERT INTO InsurerToIntermediary (Insurer, Intermediary, [Certificate],[CoverNote], PrintLimit, [enabled], CreatedBy, CreatedOn, LastModifiedBy, [keep]) 
		VALUES (@CompanyID, @intermediaryCompId, @certificate, @covernote, @printlimit, @enabled, @userID, CURRENT_TIMESTAMP, @userID, 1);

		SET @id = @@IDENTITY;
		
		SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was inserted into the database by ' + CONVERT(varchar(MAX),@userID));
		SELECT @origdata = null;
		SELECT @newdata = 'InsuranceCompId: ' + CONVERT(varchar(MAX), @CompanyID) + 
					      ';IntermediaryCompId:' + CONVERT(varchar(MAX), @intermediaryCompId) +
					      ';Certificate: ' + CONVERT(varchar(MAX), @certificate) +
					      ';CoverNote: ' + CONVERT(varchar(MAX), @covernote) +
					      ';PrintLimit' + CONVERT(varchar(MAX), @printlimit);
		
		EXEC LogAudit 'insert', @userID , @details, 'InsurerToIntermediary',  @@IDENTITY, @origdata, @newdata, 'insert';

	END
	ELSE
	BEGIN
		
		SELECT @origdata = 'InsuranceCompId: ' + CONVERT(varchar(MAX), @CompanyID) + 
						   ';IntermediaryCompId: ' + CONVERT(varchar(MAX), @intermediaryCompId) +
						   ';Certificate: ' + CONVERT(varchar(MAX), [Certificate]) +
						   ';CoverNote: ' + CONVERT(varchar(MAX), [CoverNote]) +
						   ';PrintLimit' + CONVERT(varchar(MAX), PrintLimit),
			  @id = CTID
		FROM InsurerToIntermediary WHERE Insurer = @CompanyID AND Intermediary = @intermediaryCompId;

		UPDATE InsurerToIntermediary
		SET [Certificate] = @certificate,
			[CoverNote] = @covernote,
			PrintLimit = @printlimit,
			[enabled] = @enabled,
			LastModifiedBy = @userID,
			[keep] = 1
		WHERE Insurer = @CompanyID AND Intermediary = @intermediaryCompId AND (CTID = @intid)
	END

	SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was inserted into the database by ' + CONVERT(varchar(MAX),@userID)),
		   @newdata = 'InsuranceCompId: ' + CONVERT(varchar(MAX), @CompanyID) + 
					  ';IntermediaryCompId: ' + CONVERT(varchar(MAX), @intermediaryCompId) +
					  ';Certificate: ' + CONVERT(varchar(MAX), [Certificate]) +
					  ';CoverNote: ' + CONVERT(varchar(MAX), [CoverNote]) +
					  ';PrintLimit' + CONVERT(varchar(MAX), PrintLimit)
	FROM InsurerToIntermediary WHERE Insurer = @CompanyID AND Intermediary = @intermediaryCompId;
	EXEC LogAudit 'insert', @userID , @details, 'InsurerToIntermediary',  @@IDENTITY, @origdata, @newdata, 'insert';


	SELECT @id [ID];

END
GO
/****** Object:  StoredProcedure [dbo].[AddMappingData]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddMappingData]
	-- Add the parameters for the stored procedure here
	@table varchar(max),
	@data varchar(max),
	@compid int,
	@userID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES 


	IF EXISTS(SELECT ID FROM MappingTable WHERE [Table] = @table AND data = @data AND CompanyID = @compid)
	   SELECT ID FROM MappingTable WHERE [Table] = @table AND data = @data;
	 
	ELSE
	  BEGIN
	    
		SELECT	@origdata = NULL;

		INSERT INTO MappingTable ([Table], data, CompanyID) VALUES (@table, @data, @compid);
		SELECT @@IDENTITY [ID];

		SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID)),
			   @newdata = 'ID: ' + CONVERT(varchar(MAX), @@IDENTITY) +';Table: ' + @table +';data '+ @data +';CompanyID: ' + CONVERT(varchar(MAX),@compid);

		EXEC LogAudit 'insert', @userID , @details, 'MappingTable',  @@IDENTITY, @origdata, @newdata, 'insert';

	  END

END

GO
/****** Object:  StoredProcedure [dbo].[AddMortgagee]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddMortgagee]
	-- Add the parameters for the stored procedure here
	@mortgagee varchar(MAX),
	@createdby int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF NOT EXISTS (SELECT * FROM Mortgagees WHERE Mortgagee = @mortgagee) 
	BEGIN
		INSERT INTO Mortgagees (Mortgagee, CreatedBy) VALUES (@mortgagee, @createdby);
		SELECT @@IDENTITY [ID];
	END
	ELSE 
	BEGIN
		SELECT MID [ID] FROM Mortgagees WHERE Mortgagee = @mortgagee;
	END
END

GO
/****** Object:  StoredProcedure [dbo].[AddPerson]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 06/06/14
-- Description:	Adds a person to the database
-- =============================================

CREATE PROCEDURE [dbo].[AddPerson]
	-- Add the parameters for the stored procedure here
	@fname varchar(250),
	@mname varchar(250),
	@lname varchar(250),
	@address int = 0,
	@trn varchar (250),
	@CreatedBy int,
	@dob Date = null
	
AS
BEGIN
	SET NOCOUNT ON;

	 -- checking to see if the person is already in the system (just reurn ID aleady created for them)


	DECLARE @result bit;
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	
	IF NOT EXISTS (SELECT PeopleId FROM People WHERE TRN = @trn)

	BEGIN
		IF ( @address = 0) INSERT INTO People (FName, MName, LName, TRN, CreatedBy, CreatedOn, LastModifiedBy) VALUES (@fname, @mname, @lname, @trn, @CreatedBy, CURRENT_TIMESTAMP, @CreatedBy); 
		ELSE INSERT INTO People (FName, MName, LName, [Address], TRN, CreatedBy, CreatedOn, LastModifiedBy) VALUES (@fname, @mname, @lname, @address, @trn, @CreatedBy, CURRENT_TIMESTAMP, @CreatedBy); 
		SET @result = 1;
		SELECT @@IDENTITY AS [ID], @result as result;

		SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@CreatedBy)),
				   @origdata = null,
				   @newdata = ('FirstName:' + CONVERT(varchar(MAX),@fname)+ ';MiddleName:' + CONVERT(varchar(MAX),@mname) + ';LastName:' + CONVERT(varchar(MAX),@lname) + ';AddressID:' + CONVERT(varchar(MAX),@address) + ';TRN:' + @trn);
	    EXEC LogAudit 'insert', @CreatedBy , @details, 'People',  @@IDENTITY, @origdata, @newdata, 'insert';
	END

	ELSE 
	BEGIN
		
		DECLARE @peopleId int;
		SELECT  @peopleId = PeopleID FROM People WHERE TRN = @trn;

		SELECT @origdata = 'FirstName: ' + CONVERT(varchar(MAX), fname)+ ';MiddleName: ' + CONVERT(varchar(MAX),mname) + ';LastName: ' + CONVERT(varchar(MAX),lname) +';DOB: ' + CONVERT(varchar(MAX),(CASE WHEN (DOB = '' OR DOB IS NULL) THEN '' ELSE DOB END))
		                     FROM People WHERE TRN = @trn;
		                      
		UPDATE People SET FName = @fname, MName = @mname, LName = @lname WHERE TRN = @trn;
		IF (@dob IS NOT NULL) UPDATE People SET DOB = @dob WHERE TRN = @trn;
		
		SELECT @details = ('The record ' + CONVERT(varchar(MAX),@peopleId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@CreatedBy)),
	           @newdata ='FirstName: ' + CONVERT(varchar(MAX),@fname)+ ';MiddleName: ' + CONVERT(varchar(MAX),@mname) + ';LastName: ' + CONVERT(varchar(MAX),@lname) + ';DOB:' + CONVERT(varchar(MAX),(CASE WHEN (@dob = '' OR @dob IS NULL) THEN '' ELSE @dob END));
	    EXEC LogAudit 'update', @CreatedBy , @details, 'People',  @peopleId, @origdata, @newdata, 'update';

		SET @result = 0;
		SELECT @result as result, @peopleId [ID] ;
	END
END

GO
/****** Object:  StoredProcedure [dbo].[AddPhoneNumberToCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 09/09/2014
-- Description:	Connects a company to a phone in the system
-- =============================================

CREATE PROCEDURE [dbo].[AddPhoneNumberToCompany]
	
	@companyId int,
	@phoneNum varchar (250),
	@extension varchar (250),
	@primary bit,
	@createdBy int
	
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @phoneNumID int;

	IF NOT EXISTS (SELECT PID FROM PhoneNumbers WHERE PhoneNo = @phoneNum  AND Ext = @extension)
	BEGIN
		INSERT INTO PhoneNumbers (PhoneNo, Ext, CreatedBy, CreatedOn, LastModifiedBy) VALUES (@phoneNum, @extension, @createdBy, CURRENT_TIMESTAMP, @createdBy);
		SELECT @phoneNumID = @@IDENTITY;
		SELECT @details = 'The record ' + CONVERT(varchar(MAX),@phoneNumID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby),
			   @origdata = null,
			   @newdata = 'PhoneNumber: ' + @phoneNum+ 'Extension: ' + @extension;
		EXEC LogAudit 'insert', @createdby , @details, 'PhoneNumbers', @phoneNumID, @origdata, @newdata, 'insert';
	END

	ELSE SELECT @phoneNumID =PID FROM PhoneNumbers WHERE PhoneNo = @phoneNum  AND Ext = @extension;

	INSERT INTO CompanyToPhones (CompanyID, PhoneID, [Primary], CreatedBy, CreatedOn, LastModifiedBy) VALUES (@companyId, @phoneNumID, @primary, @createdBy, CURRENT_TIMESTAMP, @createdBy); 
	
	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby),
		   @origdata = null,
		   @newdata = 'CompanyID: ' + CONVERT(varchar(MAX),@companyID) + ';Company: ' + Company.CompanyName + ' ' + Company.TRN + ' ' + CONVERT(varchar(MAX),Company.IAJID)
		            + ';PhoneNumberID: ' + CONVERT(varchar(MAX),@phoneNumID) + ';PhoneNumber: ' + @phoneNum + ';PrimaryNumber: ' + CONVERT(varchar(MAX),@primary)
    FROM Company WHERE Company.CompanyID = @companyID;


	EXEC LogAudit 'insert', @createdby , @details, 'CompanyToPhones', @@IDENTITY, @origdata, @newdata, 'insert';
	

END

GO
/****** Object:  StoredProcedure [dbo].[AddPhoneNumberToCompUsers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 09/09/2014
-- Description:	Connects a company to a phone in the system (via a user)
-- =============================================

CREATE PROCEDURE [dbo].[AddPhoneNumberToCompUsers]
	
	@companyId int,
	@phoneNum varchar (250),
	@extension varchar (250),
	@userId int,
	@createdBy int
	
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @phoneNumID int;

	IF NOT EXISTS (SELECT PID FROM PhoneNumbers WHERE PhoneNo = @phoneNum AND Ext = @extension)
	BEGIN
		INSERT INTO PhoneNumbers (PhoneNo, Ext, CreatedBy, CreatedOn, LastModifiedBy) VALUES (@phoneNum, @extension, @createdBy, CURRENT_TIMESTAMP, @createdBy);
		SELECT @phoneNumID = @@IDENTITY;
		SELECT @details = 'The record ' + CONVERT(varchar(MAX),@phoneNumID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby),
			   @origdata = null,
			   @newdata = 'PhoneNumber: ' + @phoneNum+ 'Extension: ' + @extension;
		EXEC LogAudit 'insert', @createdby , @details, 'PhoneNumbers', @phoneNumID, @origdata, @newdata, 'insert';
	END

	ELSE SELECT @phoneNumID =PID FROM PhoneNumbers WHERE PhoneNo = @phoneNum AND Ext = @extension;

	INSERT INTO CompanyToPhones (CompanyID, PhoneID, CreatedBy, CreatedOn, LastModifiedBy,UserID) VALUES (@companyId, @phoneNumID, @createdBy, CURRENT_TIMESTAMP, @createdBy, @userId); 
	
	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby),
		   @origdata = null,
		   @newdata = 'CompanyID: ' + CONVERT(varchar(MAX),@companyID) + ';Company: ' + Company.CompanyName + ' ' + Company.TRN + ' ' + CONVERT(varchar(MAX),Company.IAJID)
		            + ';PhoneNumberID: ' + CONVERT(varchar(MAX),@phoneNumID) + ';PhoneNumber: ' + @phoneNum + ';UserID: ' + CONVERT(varchar(MAX),@userId)
    FROM Company WHERE Company.CompanyID = @companyID;


	EXEC LogAudit 'insert', @createdby , @details, 'CompanyToPhones', @@IDENTITY, @origdata, @newdata, 'insert';
	

END

GO
/****** Object:  StoredProcedure [dbo].[AddPolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date:02/07/2014
-- Description:	Adding a policy to the system
-- =============================================

CREATE PROCEDURE [dbo].[AddPolicy]
	-- Add the parameters for the stored procedure here

	@startDateTime varchar (250),
	@endDateTime varchar (250),
	@policyNumber varchar (50),
	@CreatedBy int,
	@CompanyID int,
	@EDIID varchar(MAX) = null, 
	@policyCoverId int,
	@insuredType varchar (250),
	@scheme bit,
	@billingaddress int,
	@mailingaddress int,
	@mailaddshort varchar(100) = NULL,
	@billaddshort varchar(100) = NULL
	

AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @result bit;
	DECLARE @PolicyID int; DECLARE @cancelled bit;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	DECLARE @smallEnddatetime smalldatetime = @endDateTime;
	DECLARE @end datetime = @smallEnddatetime;

	DECLARE @smallstartdatetime smalldatetime = @startDateTime;
	DECLARE @start datetime = @smallstartdatetime;


	--Will this be needed? (If the policy being added, has already expired, it will be added as a cancelled policy)

	IF (@end < CURRENT_TIMESTAMP)
	BEGIN
		SET @cancelled = CAST(1 as bit);
	END

	ELSE
	BEGIN
	   SET @cancelled = CAST(0 as bit);
	END


	 IF NOT EXISTS (SELECT PolicyID FROM Policies WHERE EDIID= @EDIID AND Policyno = @policyNumber AND PolicyCover =@policyCoverId)

	  BEGIN
	    INSERT INTO Policies (EDIID, CreatedOn,CreatedBy, CompanyID,StartDateTime, EndDateTime,Policyno, PolicyCover, InsuredType, Scheme, LastModifiedBy, Cancelled, CompanyCreatedBy, BillingAddress, MailingAddress, mailing_address_short, billing_address_short) 
		                      VALUES ((CASE WHEN(@EDIID IS NULL OR @EDIID = '') THEN NULL ELSE @EDIID END), CURRENT_TIMESTAMP, @CreatedBy, @CompanyID, @start, @end,  
							           @policyNumber, @policyCoverId, (SELECT PITID FROM PolicyInsuredType WHERE InsuredType = @insuredType), @scheme, @CreatedBy, @cancelled, (SELECT e.CompanyID FROM Employees e LEFT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE u.[UID] = @CreatedBy), @billingaddress, @mailingaddress, @mailaddshort, @billaddshort);

		SET @PolicyID = @@IDENTITY;
		
		SET @result = 1;
		SELECT @result AS [result], @PolicyID AS [ID];

		SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@CreatedBy)),
				   @origdata = null,
				   @newdata = ('EDIID:' + CONVERT(varchar(MAX),@EDIID)+ ';CompanyID:' + CONVERT(varchar(MAX),(SELECT CompanyID FROM Company WHERE IAJID = @CompanyID)) 
				   + ';StartDateTime:' + CONVERT(varchar(MAX),@startDateTime) + ';EndDateTime:' + CONVERT(varchar(MAX),@endDateTime) 
				   + ';PolicyNumber:' + CONVERT(varchar(MAX),@policyNumber) 
				   + ';PolicyCover:' + (SELECT Cover FROM Policycovers WHERE PolicyCovers.ID= @policyCoverId) + ';InsuredType:' +@insuredType);

	    EXEC LogAudit 'insert', @CreatedBy , @details, 'Policy',  @@IDENTITY, @origdata, @newdata, 'insert';

	END

	 ELSE 
	 BEGIN
		 SET @result = 0;
		 SELECT @result AS [result], PolicyID [ID] FROM Policies WHERE EDIID = @EDIID AND Policyno = @policyNumber AND PolicyCover = @policyCoverId;
     END
END

GO
/****** Object:  StoredProcedure [dbo].[AddPolicyCover]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 24/06/2014
-- Description:	Adding a policy cover
-- =============================================

CREATE PROCEDURE [dbo].[AddPolicyCover]
	-- Add the parameters for the stored procedure here
	@prefix varchar(100),
	@policyCover varchar (250),
	@isCommercialBusiness bit,
	@createdBy int,
	@insuredby int = 0


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @result bit;
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	DECLARE @companyID int; 
	SELECT @insuredby = (CASE WHEN (@insuredby = 0) THEN Company.CompanyID ELSE @insuredby END) FROM Users LEFT JOIN Employees ON Employees.EmployeeID = Users.EmployeeID LEFT JOIN Company ON Company.CompanyID = Employees.CompanyID WHERE Users.UID = @createdBy;


	IF NOT EXISTS (SELECT ID FROM PolicyCovers WHERE Cover = @policyCover AND CompanyID = @insuredby)
	BEGIN
	    INSERT INTO PolicyCovers (Prefix, Cover, IsCommercialBusiness, CreatedBy, CreatedOn, LastModifiedby, CompanyID) 
		VALUES (@prefix, @policyCover, @isCommercialBusiness, @createdBy, CURRENT_TIMESTAMP, @createdBy, @insuredby);
		SELECT @@IDENTITY AS [ID];

		SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@CreatedBy)),
				   @origdata = null,
				   @newdata = 'PolicyCover:' + @policyCover+ ';IsCommercialBusiness:' + CONVERT(varchar(MAX),@isCommercialBusiness) + 'Prefix:' + @prefix ;
	    EXEC LogAudit 'insert', @createdBy , @details, 'PolicyCover',  @@IDENTITY, @origdata, @newdata, 'insert';

	END
	ELSE SELECT ID FROM PolicyCovers WHERE Cover = @policyCover AND CompanyID = @insuredby;

END




GO
/****** Object:  StoredProcedure [dbo].[AddTemplateImage]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddTemplateImage]
	-- Add the parameters for the stored procedure here
	@type varchar(50),
	@position varchar(50),
	@location varchar(250),
	@companyid int,
	@userid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF NOT EXISTS (SELECT * FROM TemplateImages WHERE companyid = @companyid AND image_location = @location AND image_position = @position AND template_type = @type)
	BEGIN
		INSERT INTO TemplateImages (template_type, image_position, image_location, companyid, userid)
		VALUES (@type, @position, @location, @companyid, @userid);
	END
END

GO
/****** Object:  StoredProcedure [dbo].[AddUser]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrew Robinson
-- Create date: 30/05/14
-- Description:	Manage the addition of users to the system
-- =============================================
CREATE PROCEDURE [dbo].[AddUser]
	-- Add the parameters for the stored procedure here

	@uname varchar(55),
	@password varchar(max),
	@empId int,
	@createdBy int,
	@admin int
	
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES
	DECLARE @userID int, @compid int;

	SELECT @compid = CompanyID FROM Employees WHERE EmployeeID = @empId;

	INSERT INTO Users (username, [password], EmployeeID, CreatedBy, CreatedOn, LastModifiedby, Deleted, active) VALUES (@uname, @password, @empId, @createdBy, CURRENT_TIMESTAMP, @createdBy, 0, 1);

	SET @userID= @@IDENTITY;
	
	IF (@admin = 1)
	BEGIN
		INSERT INTO UserGroupUserAssoc (UGID, [UID], CreatedBy, CreatedOn, LastModifiedBy) SELECT UGID, @userID, @createdBy, CURRENT_TIMESTAMP, @createdBy FROM UserGroups WHERE UserGroup = 'Company Administrator' AND Company = @compid;
	END

	SELECT @userID AS [ID];

	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@userID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy),
		   @newdata = 'username:' +  @uname +  ';password:' + @password + ';EmployeeID:' + CONVERT(varchar(MAX),@empId);
	EXEC LogAudit 'insert', @createdBy , @details, 'Users', @userID, null, @newdata, 'insert';
END

GO
/****** Object:  StoredProcedure [dbo].[AddUserToContactGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddUserToContactGroup]
	-- Add the parameters for the stored procedure here
	@cgid int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

    -- Insert statements for procedure here
	IF NOT EXISTS (SELECT ID FROM ContactUserAssoc WHERE contactgroup = @cgid AND [UID] = @uid)
	BEGIN
		INSERT INTO ContactUserAssoc (contactgroup, [UID]) VALUES (@cgid, @uid);
		SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
		       @origdata = null,
			   @newdata = 'ContactGroup:' + ContactGroups.name + ';User:' + Users.username
		FROM ContactUserAssoc JOIN ContactGroups ON ContactGroups.ID = ContactUserAssoc.contactgroup
							  JOIN Users ON ContactUserAssoc.UID = Users.UID
		WHERE ContactUserAssoc.ID = @@IDENTITY;
		EXEC LogAudit 'insert', @uid , @details, 'ContactUserAssoc', @@IDENTITY, @origdata, @newdata, 'insert';
	END
END


GO
/****** Object:  StoredProcedure [dbo].[AddVehicle]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 20/06/2014
-- Description:	Adding a vehicle to the system
-- =============================================

CREATE PROCEDURE [dbo].[AddVehicle]
	-- Add the parameters for the stored procedure here
	@ediid varchar(250) = null,
	@make varchar (250),
	@model varchar (250), 
	@modelType varchar (50) = null,
	@bodyType varchar (50),
	@extention varchar (250) = null,
	@registrationNum varchar (50),
	@VIN varchar (250) = null,
	@ChassisNum varchar (250),
	@handDriveType varchar (250) = null, --receives a varchar, to search vehicleHandriveLookUp table for the ID (if it exists)
	@colour varchar (50) = null,
	@engineNo varchar (50) = null,
	@engineModified bit,
	@engineType varchar (5) = null,
	@HPCCUnittype varchar (50),
	@regLocation varchar (250) = null,
	@roofType varchar (50) = null,
	@expiryDateOffitness Date = null,
	@registeredOwner varchar (MAX) = null,
	@restrictedDriving bit,
	@transmissionType varchar (250) = null, -- receives a varchar, to search transmissionType table for the ID (if it exists)
	@seating int = 0,
	@seatingCert int = 0, 
	@tonnage int = 0,
	@vehicleYear int = 0,
	@cylinders int = 0,
	@estAnnualMileage int = 0,
	@mainDriver int = 0, -- the Driver ID of the vehicle's main driver
	@referenceNo varchar(100) = null,
	@createdBy int
	


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @handDriveTypeId int;
	DECLARE @transmissionTypeId int;
	--DECLARE @expiry smalldatetime = @expiryDateOffitness;
	--DECLARE @exp datetime = @expiry;


	--IF THE HAND DRIVE IS EMPTY, THE VALUE IS NULL
	IF (@handDriveType = '')
	BEGIN
		SET @handDriveTypeId= null;
	END
	ELSE
	BEGIN
		IF EXISTS (SELECT VHDLID FROM VehicleHandDriveLookUp WHERE HDType = @handDriveType)
		BEGIN
			SELECT @handDriveTypeId = VHDLID FROM VehicleHandDriveLookUp WHERE HDType = @handDrivetype;
		END
		ELSE
		BEGIN
			INSERT INTO VehicleHandDriveLookUp (HDType, CreatedBy, CreatedOn, LastModifiedBy) Values (@handDriveType, @createdBy, CURRENT_TIMESTAMP, @createdBy);
			SET @handDriveTypeId= @@IDENTITY; 

			SELECT @details = ('The record ' + CONVERT(varchar(MAX),@handDriveTypeId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy)),
					@origdata = null,
					@newdata = 'HandDriveType:' + @handDriveType;
			EXEC LogAudit 'insert', @createdBy , @details, 'VehicleHandDriveTypeLookUp',  @handDriveTypeId, @origdata, @newdata, 'insert';
		END
	 END

	--IF THE TRANSMISSION TYPE IS EMPTY, THE VALUE IS NULL
    IF (@transmissionType = '')
	SET @transmissionTypeId= null;

	ELSE
	BEGIN
		IF EXISTS (SELECT TransmisionTypeID FROM TransmissionTypeLookUp WHERE TransmissionType = @transmissionType)
		BEGIN
			SELECT @transmissionTypeId = TransmisionTypeID FROM TransmissionTypeLookUp WHERE TransmissionType = @transmissionType;
		END
		ELSE
		BEGIN
			INSERT INTO TransmissionTypeLookUp (TransmissionType, CreatedBy, CreatedOn, LastModifiedBy) Values (@transmissionType, @createdBy, CURRENT_TIMESTAMP, @createdBy);
			SET @transmissionTypeId= @@IDENTITY; 
			
			SELECT @details = ('The record ' + CONVERT(varchar(MAX),@transmissionTypeId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy)),
					@origdata = null,
					@newdata = 'TransmissionType:' + @transmissionType;
			EXEC LogAudit 'insert', @createdBy , @details, 'TransmissionTypeLookUp',  @transmissionTypeId, @origdata, @newdata, 'insert';

		END
	END


	IF NOT EXISTS (SELECT VehicleID FROM Vehicles WHERE ChassisNo = @ChassisNum)-- OR VehicleRegistrationNo = @registrationNum OR VIN = @VIN)
	BEGIN
		INSERT INTO Vehicles (ediid, Make, Model, ModelType, BodyType, Extension, VehicleRegistrationNo, VIN, ChassisNo, HandDrive, Colour, Cylinders, EngineNo, 
					EngineModified, EngineType, EstimatedAnnualMileage, ExpiryDateofFitness, HPCCUnitType, MainDriver, ReferenceNo, RegistrationLocation,
					RoofType, Seating, SeatingCert, Tonnage,TransmissionType, VehicleYear, RegisteredOwner, RestrictedDriving, CreatedBy, CreatedOn, LastModifiedBy) 
					VALUES ((CASE WHEN (@ediid = '') THEN NULL ELSE @ediid END), @make, @model, @modelType, @bodyType, @extention , @registrationNum ,(CASE WHEN (@VIN = '' OR @VIN IS NULL) THEN null ELSE @VIN END) , @ChassisNum, @handDriveTypeId , @colour ,@cylinders , @engineNo , @engineModified ,@engineType ,
							@estAnnualMileage,@expiryDateOffitness ,@HPCCUnittype ,(CASE WHEN(@mainDriver = 0) THEN NULL ELSE @mainDriver END) , @referenceNo, @regLocation,@roofType ,@seating ,@seatingCert, @tonnage , @transmissionTypeId, @vehicleYear, 
							@registeredOwner, @restrictedDriving, @createdBy, CURRENT_TIMESTAMP, @createdBy);

        SELECT @@IDENTITY AS [ID], (CASE WHEN(@transmissionTypeId IS NULL) THEN 0 ELSE @transmissionTypeId END) [tid];

		SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy)),
					@origdata = null,
					@newdata = 'Make:' + @make+ ';Model:' + @model+ ';ModelType:' + @modelType+ ';BodyType:' + @bodyType+';Extension:' + @extention+';RegistrationNumber:' + @registrationNum+';VIN:' + @VIN+ ';ChassisNo:' + @ChassisNum+ ';HandDrive:' + @handDriveType+
								';Colour:' + @colour +';Cylinders:' +  CONVERT(varchar(MAX),@cylinders)+ ';EngineNo:' + @engineNo+ ';EngineModified:' + CONVERT(varchar(MAX),@engineModified) + ';EngineType:' + @engineType+ ';EstimatedAnnualMileage:' +  CONVERT(varchar(MAX),@estAnnualMileage) + 
								';ExpirydateOfFitness:' + CONVERT(varchar(MAX),@expiryDateOffitness)+ ';HPCCUnitType:' + @HPCCUnittype +';MainDriverID:' + CONVERT(varchar(MAX),@mainDriver) +';ReferenceNo:' + CONVERT(varchar(MAX),@referenceNo)+';RegistrationLocation:' + @regLocation+';RoofType:' + @roofType+ 
								';Seating:' + CONVERT(varchar(MAX),@seating)+ ';SeatingCert:' + CONVERT(varchar(MAX),@seatingCert)+ ';Tonnage:' + CONVERT(varchar(MAX),@tonnage) + @transmissionType+ ';VehicleYear:' + CONVERT(varchar(MAX),@vehicleYear) + 
								';RegisteredOwner:' + @registeredOwner + ';RestrictedDriving:' + CONVERT(varchar(MAX),@restrictedDriving);
		
		EXEC LogAudit 'insert', @createdBy , @details, 'Vehicles',  @@IDENTITY, @origdata, @newdata, 'insert';
		
	END
	ELSE SELECT v.VehicleID AS [ID], (CASE WHEN (v.TransmissionType IS NULL) THEN 0 ELSE t.TransmisionTypeID END) [tid]
	FROM Vehicles v LEFT JOIN TransmissionTypeLookUp t ON v.TransmissionType = t.TransmisionTypeID
	WHERE v.ChassisNo = @ChassisNum

END

GO
/****** Object:  StoredProcedure [dbo].[AddVehicleCoverNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddVehicleCoverNote]
	-- Add the parameters for the stored procedure here
	@ediid varchar(MAX) = null,
	@alterations text = null,
	@covernoteid varchar(100),
	@effectivedate DateTime,
	@expirydate DateTime,
	@period int,
	@endorseno varchar(100) = null,
	@covernoteno varchar(200) = NULL,
	@vehid int,
	
	@cancelled bit = 0,
	@cancelledbyflat varchar(MAX) = null,
	@cancelledbyint int = 0,
	@cancelledreason varchar(MAX) = null,
	@cancelledon DateTime = null,

	@printpaperno varchar(MAX) = null,
	@lastprintedbyint int = 0,
	@lastprintedbyflat varchar(MAX) = null,
	@printcount int = 0,

	@wording int = 0,
	@polid int = 0,
	@companyid int = 0,
	@createdby int,
	@new bit = 0,
	@import bit = 0,

	@mortgagee varchar(MAX) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @vcnid int;
	SET @vcnid = 0;

	IF (@new = 1) OR (NOT EXISTS(SELECT VehicleCoverNoteID FROM VehicleCoverNote WHERE CoverNoteID = @covernoteid OR EDIID = @ediid) AND ((@companyID IS NOT NULL) OR (@companyid != 0)) AND ((@polid IS NOT NULL) OR (@polid != 0)))
	BEGIN
		
		DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
		
		INSERT INTO VehicleCoverNote (CreatedBy, CompanyID, Alterations, CoverNoteID, EffectiveDate, EndorsementNo, ManualCoverNoteNo, RiskItemNo, Printed, period, PolicyID, EDIID, ExpiryDateTime, RequestPrint, WordingTemplate, Generated, Cancelled, CancelledBy,
									  CancelByFlat, CancelledReason, PrintedPaperNo, LastPrintedBy, LastPrintedByFlat, FirstPrintedByFlat, mortgagee, PrintCount, PrintCountInsurer)
							VALUES	 (@createdby, @companyID, @alterations, @covernoteid, @effectivedate,(CASE WHEN (@endorseno IS NULL OR @endorseno = '') THEN NULL ELSE @endorseno END), 
							         (CASE WHEN (@covernoteno IS NULL OR @covernoteno = '') THEN NULL ELSE @covernoteno END), @vehid, 0, @period, @polid, @ediid, @expirydate, 0, (CASE WHEN @wording = 0 THEN NULL ELSE @wording END), 0, @cancelled, (CASE WHEN(@cancelledbyint=0) THEN NULL ELSE @cancelledbyint END),
									 (CASE WHEN(@cancelledbyflat = '') THEN NULL ELSE @cancelledbyflat END),
									 (CASE WHEN(@cancelledreason = '') THEN NULL ELSE @cancelledreason END),
									 (CASE WHEN(@printpaperno = '') THEN NULL ELSE @printpaperno END),
									 (CASE WHEN(@lastprintedbyint = 0) THEN NULL ELSE @lastprintedbyint END),
									 (CASE WHEN(@lastprintedbyflat = '') THEN NULL ELSE @lastprintedbyflat END),
									 (CASE WHEN(@lastprintedbyflat = '') THEN NULL ELSE @lastprintedbyflat END),
									 (CASE WHEN(@mortgagee IS NULL OR @mortgagee = '') THEN NULL ELSE @mortgagee END), 0, 0);
		SET @vcnid = @@IDENTITY;

		IF EXISTS(SELECT * FROM CompanyCoverNoteCount WHERE CompanyID = @companyid) AND @import = 0
			UPDATE CompanyCoverNoteCount SET [count] = [count] + 1 WHERE CompanyID = @companyid;
		ELSE INSERT INTO CompanyCoverNoteCount (CompanyID, [count]) VALUES (@companyid, 1);

		UPDATE VehiclesUnderPolicy SET covercount = covercount + 1 WHERE PolicyID = @polid AND VehicleID = @vehid; -- to manage the generation limit in the system

		--IF (@new = 1)
		--IF (NOT EXISTS(SELECT VehicleCoverNoteID FROM VehicleCoverNote WHERE CoverNoteID = @covernoteid) AND ((@companyID IS NOT NULL) OR (@companyid != 0)) AND ((@polid IS NOT NULL) OR (@polid != 0)))
		UPDATE VehicleCoverNote 
		SET NoteNo = (SELECT [count] FROM CompanyCoverNoteCount WHERE CompanyID = @companyid),
			CoverNoteID = (CASE 
							WHEN ((CoverNoteID IS NULL OR CoverNoteID = '' OR @new = 1) AND @import = 0) THEN(SELECT CompShortening + '-' + (SELECT CONVERT(varchar(MAX),[count]) FROM CompanyCoverNoteCount WHERE CompanyID = @companyid) FROM Company WHERE CompanyID = @companyid) 
							WHEN ((SELECT COUNT(*) FROM VehicleCoverNote vc WHERE vc.CoverNoteID = CoverNoteID) > 1 AND @import = 0) OR @new = 1 THEN (SELECT CompShortening + '-' + (SELECT CONVERT(varchar(MAX),[count]) FROM CompanyCoverNoteCount WHERE CompanyID = @companyid) FROM Company WHERE CompanyID = @companyid) 
							WHEN (@import = 1) THEN CoverNoteID
							ELSE CoverNoteID 
						END)
		WHERE VehicleCoverNoteID = @vcnid;
		
		SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy),
		       @origdata = null,
			   @newdata = 'CompanyID:'+ CONVERT(varchar(MAX),@companyID) +';Alterations:'+ CONVERT(varchar(MAX),@alterations) + ';CoverNoteID:' +  CONVERT(varchar(MAX),@covernoteid) + ';EffectiveDate:' + CONVERT(varchar(MAX),@effectivedate) + ';ExpiryDate:' + ';EndorsementNo:' + CONVERT(varchar(MAX),@endorseno) + ';CoverNoteNo:' + CONVERT(varchar(MAX),@covernoteno) + ';RiskItemNo:' +  CONVERT(varchar(MAX),@vehid) + ';CreatedBy:' + CONVERT(varchar(MAX),@createdby);
		EXEC LogAudit 'insert', @createdBy , @details, 'VehicleCoverNote', @@IDENTITY, @origdata, @newdata, 'insert';

		
		--SELECT CAST (1 as bit) result;
	END
	ELSE 
	BEGIN
		IF (((@companyID IS NOT NULL) OR (@companyid != 0)) AND ((@polid IS NOT NULL) OR (@polid != 0)))
		BEGIN
			IF (EXISTS(SELECT VehicleCoverNoteID FROM VehicleCoverNote WHERE CoverNoteID = @covernoteid OR EDIID = @ediid)) SELECT @vcnid = VehicleCoverNoteID FROM VehicleCoverNote WHERE CoverNoteID = @covernoteid OR EDIID = @ediid;
			
			SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy),
				   @origdata = 'CompanyID:'+ CONVERT(varchar(MAX),CompanyID) +';Alterations:'+ CONVERT(varchar(MAX),Alterations) + ';CoverNoteID:' +  CONVERT(varchar(MAX),CoverNoteID) + ';EffectiveDate:' + CONVERT(varchar(MAX),EffectiveDate) + ';ExpiryDate:' + ';EndorsementNo:' + CONVERT(varchar(MAX),EndorsementNo) + ';CoverNoteNo:' + CONVERT(varchar(MAX),ManualCoverNoteNo) + ';RiskItemNo:' +  CONVERT(varchar(MAX),RiskItemNo) + ';CreatedBy:' + CONVERT(varchar(MAX),CreatedBy),
				   @newdata = 'CompanyID:'+ CONVERT(varchar(MAX),@companyID) +';Alterations:'+ CONVERT(varchar(MAX),@alterations) + ';CoverNoteID:' +  CONVERT(varchar(MAX),@covernoteid) + ';EffectiveDate:' + CONVERT(varchar(MAX),@effectivedate) + ';ExpiryDate:' + ';EndorsementNo:' + CONVERT(varchar(MAX),@endorseno) + ';CoverNoteNo:' + CONVERT(varchar(MAX),@covernoteno) + ';RiskItemNo:' +  CONVERT(varchar(MAX),@vehid) + ';CreatedBy:' + CONVERT(varchar(MAX),@createdby)
			FROM VehicleCoverNote WHERE EDIID = @ediid;
			EXEC LogAudit 'update', @createdBy , @details, 'VehicleCoverNote', @@IDENTITY, @origdata, @newdata, 'update';


			UPDATE VehicleCoverNote
			SET Alterations = @alterations,
				CoverNoteID = @covernoteid,
				EffectiveDate = @effectivedate,
				ExpiryDateTime = @expirydate,
				period = @period,
				EndorsementNo = @endorseno,
				ManualCoverNoteNo = @covernoteno,
				RiskItemNo = @vehid,
				WordingTemplate = (CASE WHEN (@wording = 0) THEN NULL ELSE @wording END),
				PolicyID = @polid,
				CancelByFlat = (CASE WHEN(@cancelledbyflat = '') THEN NULL ELSE @cancelledbyflat END),
				CancelledReason = (CASE WHEN(@cancelledreason = '') THEN NULL ELSE @cancelledreason END),
				PrintedPaperNo = (CASE WHEN(@printpaperno = '') THEN NULL ELSE @printpaperno END),
				LastPrintedBy = (CASE WHEN(@lastprintedbyint = 0) THEN NULL ELSE @lastprintedbyint END),
				LastPrintedByFlat = (CASE WHEN(@lastprintedbyflat = '') THEN NULL ELSE @lastprintedbyflat END),
				Cancelled =  @cancelled, 
				CancelledBy = (CASE WHEN(@cancelledbyint=0) THEN NULL ELSE @cancelledbyint END),
				CompanyID = @companyid,
				LastModifiedOn = CURRENT_TIMESTAMP
			WHERE EDIID = @ediid;
		
		END

	END

	SELECT @vcnid [ID];

	

END

GO
/****** Object:  StoredProcedure [dbo].[AddVehicleToPolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 10/07/2014
-- Description:	Adding a vehicle to a policy
-- =============================================

CREATE PROCEDURE [dbo].[AddVehicleToPolicy]
	
	@vehicleID int,
	@policyID int,
	@usage int = NULL,
	@mortgagee int = 0,
	@createdBy int

	
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @result bit;


	IF NOT EXISTS (SELECT VUPID FROM VehiclesUnderPolicy WHERE PolicyID = @policyID AND VehicleID = @vehicleID)
	BEGIN

		INSERT INTO VehiclesUnderPolicy (VehicleID, PolicyID, CreatedBy, CreatedOn, VehicleUsage, LastModifiedBy) VALUES (@vehicleID,@policyID, @createdBy, CURRENT_TIMESTAMP, (CASE WHEN(@usage IS NULL OR @usage = 0) THEN NULL ELSE @usage END), @createdBy);
		UPDATE VehiclesUnderPolicy SET mortgagee = (CASE WHEN(@mortgagee = 0) THEN NULL ELSE @mortgagee END) WHERE VUPID = @@IDENTITY;
		SET @result = 1;
	    SELECT @result AS [result];


		SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy)),
				   @origdata = null, 
				   @newdata = ('PolicyID:' + CONVERT(varchar(MAX),@policyID) + ';VeicleID:' + CONVERT(varchar(MAX),@vehicleID));
	    EXEC LogAudit 'insert', @createdby, @details, 'VehiclesUnderPolicy',  @@IDENTITY, @origdata, @newdata, 'insert';
	END

END
GO
/****** Object:  StoredProcedure [dbo].[AddWording]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddWording]
	@createdby int,
	@certtype varchar(30),
	@certid varchar(30),
	@certinscode varchar(30),
	@extcode varchar(30),
	@cover int,
	@usage int,
	@excludeddriver bit,
	@excludedinsured bit,
	@multiplevehicle bit,
	@registeredowner bit,
	@restricteddriver bit,
	@scheme bit,
	@authorizeddrivers varchar(MAX),
	@limits varchar(MAX),
	@vehdesc varchar(MAX),
	@vehregno varchar(MAX),
	@policyholder varchar(MAX),
	@imported bit
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

    INSERT INTO Wording (CreatedBy, CertificateType, CertificateID, CertificateInsuredCode, ExtensionCode, PolicyCover, usageid, ExcludedDriver, MultipleVehicle, RegisteredOwner, RestrictedDriver, Scheme, AuthorizedDrivers, LimitsOfUse, VehicleDesc, VehRegNo, PolicyHolders, Imported)
	VALUES (@createdby, @certtype, @certid, @certinscode, @extcode, @cover, @usage, @excludeddriver, @multiplevehicle, @registeredowner, @restricteddriver, @scheme, @authorizeddrivers, @limits, @vehdesc, @vehregno, @policyholder, @imported);
	

	SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy)),
				@origdata = null, 
				@newdata = 'CertificateType:' + @certtype+ ';Scheme:' + CONVERT(varchar(MAX),@scheme) + ';Usage:' + CONVERT(varchar(MAX),@usage) + ';Imported: ' + CONVERT(varchar(MAX),@imported) +';limits: ' + @limits;
	EXEC LogAudit 'insert', @createdby, @details, 'Wording',  @@IDENTITY, @origdata, @newdata, 'insert';

	
END

GO
/****** Object:  StoredProcedure [dbo].[AnswerQuestion]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AnswerQuestion]
	-- Add the parameters for the stored procedure here
	@username varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT response FROM Users WHERE username = @username;
END

GO
/****** Object:  StoredProcedure [dbo].[ApproveCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 18/08/2014
-- Description:	Approving/signing  certificate
-- =============================================
CREATE PROCEDURE [dbo].[ApproveCertificate]

	@VehicleCertId int, 
	@userId int


AS
BEGIN
	
	SET NOCOUNT ON;

	 DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	
	
	SELECT @origdata = 'CertificateID: ' + CONVERT(varchar(MAX),VehicleCertificateID)+';approvedBy: ' + CONVERT(varchar(MAX),approvedby)+';approvedAt:' + CONVERT(varchar(MAX),approvedon) +';approved:' + CONVERT(varchar(MAX),approved)
					   FROM VehicleCertificates WHERE VehicleCertificateID = @VehicleCertId;


	UPDATE VehicleCertificates SET approvedby = @userId, approvedon = CURRENT_TIMESTAMP, LastModifiedBy = @userId, approved= 1, Cancelled= 0 WHERE VehicleCertificateID = @VehicleCertId;


	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@VehicleCertId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userId),
					   @newdata = 'CertificateID: ' + CONVERT(varchar(MAX),VehicleCertificateID)+ ';approved:' + CONVERT(varchar(MAX),approved) + ';approvedBy:' + CONVERT(varchar(MAX),@userId)+';approvedAt:' + CONVERT(varchar(MAX),CURRENT_TIMESTAMP)
					              FROM VehicleCertificates WHERE VehicleCertificateID = @VehicleCertId;
					             
							
    EXEC LogAudit 'update', @userId , @details, 'VehicleCertificates',  @@IDENTITY, @origdata, @newdata, 'update';


	----Cancelling any cover notes that the vehicle may have
	IF EXISTS (SELECT VehicleCoverNoteID FROM VehicleCoverNote WHERE RiskItemNo = (SELECT RiskItemNo FROM VehicleCertificates WHERE VehicleCertificateID = @VehicleCertId)) --AND approved != NULL)
	   BEGIN
	       DECLARE @val int;

		
		    
	 INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
		   	SELECT 'update', @userId, CURRENT_TIMESTAMP,  
			       'The record ' + CONVERT(varchar(MAX),VehicleCoverNoteID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userId),
				   'VehicleCoverNote', VehicleCoverNoteID,
			       'VehicleCoverNote: '+ CONVERT(varchar(MAX),VehicleCoverNoteID)+ ';Cancelled: ' + CONVERT(varchar(MAX),Cancelled),  
				   'VehicleCoverNote: '+ CONVERT(varchar(MAX),VehicleCoverNoteID)+ ';Cancelled: ' + CONVERT(varchar(MAX),1)+ ';CancelledBy(User): ' + CONVERT(varchar(MAX),@userId) + ';CancelledON: ' + CONVERT(varchar(MAX),CURRENT_TIMESTAMP), 
				   'update' 
		    FROM VehicleCoverNote WHERE RiskItemNo = (SELECT RiskItemNo FROM VehicleCertificates WHERE VehicleCertificateID = @VehicleCertId);


		    UPDATE VehicleCoverNote 
		     SET Cancelled =1 , Approved = 0, CancelledOn = CURRENT_TIMESTAMP, CancelledBy = (SELECT PeopleID FROM People RIGHT JOIN Employees ON People.PeopleID = Employees.PersonID
			                                                                                                RIGHT JOIN Users ON Employees.EmployeeID = Users.EmployeeID
							                                                                                WHERE Users.[UID] = @userId )
	       WHERE RiskItemNo = (SELECT RiskItemNo FROM VehicleCertificates WHERE VehicleCertificateID = @VehicleCertId);
		  
	   END


END

GO
/****** Object:  StoredProcedure [dbo].[ApproveCoverNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ApproveCoverNote]
	-- Add the parameters for the stored procedure here
	@vcnid int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	SELECT @origdata = 'CoverNoteNo:' + CONVERT(varchar(MAX),@vcnid) + ';approved:' + CONVERT(varchar(MAX),approved)
	FROM VehicleCoverNote
	WHERE VehicleCoverNote.VehicleCoverNoteID = @vcnid;

	UPDATE VehicleCoverNote 
	SET approved = 1,
		approvedby = @uid,
		approvedon = CURRENT_TIMESTAMP,
		Cancelled = 0,
		LastModifiedOn  = CURRENT_TIMESTAMP
	WHERE VehicleCoverNoteID = @vcnid;

	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@vcnid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
		   @newdata = 'CoverNoteNo:' + CONVERT(varchar(MAX),@vcnid) + ';approved:' + CONVERT(varchar(MAX),approved)
	FROM VehicleCoverNote WHERE VehicleCoverNoteID = @vcnid;	
	
	EXEC LogAudit 'update', @uid , @details, 'VehicleCoverNote', @vcnid, @origdata, @newdata, 'update';


END


GO
/****** Object:  StoredProcedure [dbo].[AssociateUsageToCover]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AssociateUsageToCover]
	-- Add the parameters for the stored procedure here
	@covid int,
	@usageid int,
	@uid int
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES
   
	INSERT INTO UsagesToCovers (CoverID, UsageID, CreatedBy) VALUES (@covid, @usageid, @uid);

	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
		   @newdata = 'CoverID:' +  CONVERT(varchar(MAX),@covid) +  ';UsageID:' + CONVERT(varchar(MAX),@usageid);
	EXEC LogAudit 'insert', @uid , @details, 'UsagesToCovers', @@IDENTITY, null, @newdata, 'insert';

END


GO
/****** Object:  StoredProcedure [dbo].[AssociateUserGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AssociateUserGroup]
	-- Add the parameters for the stored procedure here
	@UGID int,
	@UID int,
	@createdBy int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF NOT EXISTS (SELECT UGUAID FROM UserGroupUserAssoc WHERE UGID = @UGID AND [UID] = @UID)
	BEGIN
		INSERT INTO UserGroupUserAssoc (UGID, [UID], CreatedBy, CreatedOn, LastModifiedBy) VALUES (@UGID, @UID, @createdBy, CURRENT_TIMESTAMP, @createdBy);
		INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
		SELECT 'insert',
			   @createdBy,
			   CURRENT_TIMESTAMP,
			   'The record ' + CONVERT(varchar(MAX), @@IDENTITY) + ' has been removed from the system by ' + CONVERT(varchar(MAX), @createdBy),
			   'UserGroupUserAssoc',
			   @@IDENTITY,
			   'UGID:' + CONVERT(varchar(MAX), @UGID) + ';UID:' + CONVERT(varchar(MAX), @UID),
			   null,
			   'insert';
	END
END


GO
/****** Object:  StoredProcedure [dbo].[AttachChildToSuperGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AttachChildToSuperGroup]
	-- Add the parameters for the stored procedure here
	@PGID int,
	@CGID int,
	@createdby int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF NOT EXISTS (SELECT SGID FROM SuperGroupMapping WHERE PGID = @PGID AND CGID = @CGID)
	BEGIN
		INSERT INTO SuperGroupMapping (PGID, CGID) VALUES (@PGID, @CGID);
		
		INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
		SELECT 'insert',
			   @createdby,
			   CURRENT_TIMESTAMP,
			   'The record ' + CONVERT(varchar(MAX), @@IDENTITY) + ' has been added from the system by ' + CONVERT(varchar(MAX), @createdby),
			   'SuperGroupMapping',
			   @@IDENTITY,
			   null,
			   'ChildGroupName:' + child.UserGroup + ';ParentGroupName:' + parent.UserGroup,
			   'insert'
		FROM UserGroups child JOIN SuperGroupMapping ON child.UGID = SuperGroupMapping.CGID
							  JOIN UserGroups parent ON parent.UGID = SuperGroupMapping.PGID
		WHERE SuperGroupMapping.SGID = @@IDENTITY;
	END
END


GO
/****** Object:  StoredProcedure [dbo].[AuthenticateUser]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AuthenticateUser]
	-- Add the parameters for the stored procedure here

	@username varchar(55)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES

	DECLARE @result bit;

	SELECT @origdata = 'username:' +  @username +  ';faultylogins:' + CONVERT(varchar(MAX),faultylogins)
	FROM Users
	WHERE username = @username;

	DECLARE @test bit;

	SELECT @test = (CASE WHEN (COUNT(u.[UID])> 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END)
				   FROM Users u LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID
								LEFT JOIN Company c ON e.CompanyID = c.CompanyID
	WHERE u.username = @username AND u.Deleted = 0 AND c.[Enabled] = 1 AND (u.faultylogins < c.faultyloginlimit OR c.faultyloginlimit = 0);--AND u.[password] = @password 

	IF @test = 1
	BEGIN
		IF EXISTS(SELECT UID FROM Users WHERE username = @username AND ((LoggedIn = 1 AND DATEDIFF(MINUTE, LastSuccessful, CURRENT_TIMESTAMP) <= 5) OR active = 0))
		BEGIN
			--THE SESSION IS ALREADY IN USE
			SELECT CAST(1 as bit) as result, CAST (1 as bit) as taken;
		END
		ELSE
		BEGIN
			--UPDATE Users SET faultylogins = 0, LoggedIn = 1, LastSuccessful = CURRENT_TIMESTAMP, LoginGUID = NEWID() WHERE username = @username;
			SELECT	@id = [UID],
					@details = 'The record ' + CONVERT(varchar(MAX),[UID]) + ' was modified from the database by ' + CONVERT(varchar(MAX),[UID]),
					@newdata = 'username:' +  @username +  ';faultylogins:' + CONVERT(varchar(MAX),faultylogins)
			FROM Users
			WHERE username = @username;
			SELECT CAST(1 as bit) as result, CAST (0 as bit) as taken, [password] FROM Users WHERE username = @username;
		END
	END
	ELSE
	BEGIN
		--UPDATE Users SET faultylogins = (faultylogins + 1) WHERE username = @username;
		SELECT	@id = null,
				@details = 'The record ' + CONVERT(varchar(MAX),[UID]) + ' was modified from the database by ' + CONVERT(varchar(MAX),[UID]),
				 @newdata = 'username:' +  @username +  ';faultylogins:' + CONVERT(varchar(MAX),faultylogins)
		FROM Users
		WHERE username = @username;
		

		SELECT CAST(0 as bit) as result, CAST (0 as bit) as taken;
	END
	EXEC LogAudit 'update', @id  , @details, 'Users', @id, @origdata, @newdata, 'update';
	
	
END

GO
/****** Object:  StoredProcedure [dbo].[CancelCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 18/08/2014
-- Description:	Cancelling a vehicle certificate in the system
-- =============================================
CREATE PROCEDURE [dbo].[CancelCertificate]

	@VehicleCertId int, 
	@userId int


AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @result bit; DECLARE @policyId int;


	SELECT @origdata = 'CertificateNo:' + VehicleCertificates.CertificateType +VehicleCertificates.InsuredCode+ CONVERT(varchar(MAX), VehicleCertificates.ExtensionCode)+CONVERT(varchar(MAX),VehicleCertificates.UniqueNum)
	                   +';RiskId:' + CONVERT(varchar(MAX), RiskItemNo)+ ';EffectiveDate:' + CONVERT(varchar(MAX), EffectiveDate)
					   +';ExpiryDate:' + CONVERT(varchar(MAX),ExpiryDate) + ';EndorsementNo:' + CONVERT(varchar(MAX),EndorsementNo)
					   +';PrintedPaperNumber:' + CONVERT(varchar(MAX),PrintedPaperNo)+';approved:' + CONVERT(varchar(MAX), approved)
					   FROM VehicleCertificates WHERE VehicleCertificateID = @VehicleCertId;

	DECLARE @personID int;
	SELECT @personId = PeopleID FROM People RIGHT JOIN Employees ON People.PeopleID = Employees.PersonID
										  RIGHT JOIN Users ON Employees.EmployeeID = Users.EmployeeID
							  WHERE Users.[UID] = @userId;

	UPDATE VehicleCertificates SET Cancelled = 'true', CancelledBy = @personId, CancelledOn = CURRENT_TIMESTAMP, LastModifiedBy= @userId, approved= 0 WHERE VehicleCertificateID = @VehicleCertId;


	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@VehicleCertId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userId),
					   @newdata = 'CertificateNo:' + VehicleCertificates.CertificateType +VehicleCertificates.InsuredCode+ CONVERT(varchar(MAX), VehicleCertificates.ExtensionCode)+CONVERT(varchar(MAX),VehicleCertificates.UniqueNum)
	                              +';RiskId:' + CONVERT(varchar(MAX), RiskItemNo)+ ';EffectiveDate:' + CONVERT(varchar(MAX), EffectiveDate)
					              +';ExpiryDate:' + CONVERT(varchar(MAX),ExpiryDate) + ';EndorsementNo:' + CONVERT(varchar(MAX),EndorsementNo)
					              +';PrintedPaperNumber:' + CONVERT(varchar(MAX),PrintedPaperNo)+';approved:' +CONVERT(varchar(MAX), approved)
								  +';CancelledBy:' + CONVERT(varchar(MAX),@userId)+';CancelledOn:' + CONVERT(varchar(MAX),CancelledOn)
					              FROM VehicleCertificates WHERE VehicleCertificateID = @VehicleCertId;
					             
							
    EXEC LogAudit 'update', @userId , @details, 'VehicleCertificates',  @VehicleCertId, @origdata, @newdata, 'update';


	SET @result = 1;
	SELECT @result AS [result];


	
END

GO
/****** Object:  StoredProcedure [dbo].[CancelCoverNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CancelCoverNote]
	-- Add the parameters for the stored procedure here
	@covernoteid int,
	@cancelledby int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLESSELECT 
	DECLARE @personId varchar(MAX);

	SELECT @origdata = 'VehicleCoverNoteID:' + CONVERT(varchar(MAX), @covernoteid);
	SELECT @personId = PeopleID FROM People RIGHT JOIN Employees ON People.PeopleID = Employees.PersonID
										  RIGHT JOIN Users ON Employees.EmployeeID = Users.EmployeeID
							  WHERE Users.[UID] = @cancelledby;

   
	UPDATE VehicleCoverNote SET CancelledBy = @personId, 
								CancelledOn = CURRENT_TIMESTAMP, 
								Cancelled = 1, approved = 0,
								LastModifiedOn  = CURRENT_TIMESTAMP
	WHERE VehicleCoverNoteID = @covernoteid;

	SELECT @details = ('The record ' + CONVERT(varchar(MAX),@covernoteid) + ' was cancelled from the database by ' + CONVERT(varchar(MAX),@cancelledby)),
		   @newdata = 'VehicleCoverNoteID:' + CONVERT(varchar(MAX), @covernoteid);
	EXEC LogAudit 'update', @cancelledby , @details, 'VehicleCoverNote',  @covernoteid, @origdata, @newdata, 'update';

END


GO
/****** Object:  StoredProcedure [dbo].[CancelPolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 25/09/2014
-- Description:	Cancelling a policy
-- =============================================
CREATE PROCEDURE [dbo].[CancelPolicy]

	@policyID int,
	@modifiedby int
AS
BEGIN
	
	SET NOCOUNT ON;

    
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES
	DECLARE @policyCreatorId int;

	
		SELECT @origdata = 'policyid:' +  CONVERT(varchar(MAX),@policyID) +  ';cancelled:' +  CONVERT(varchar(MAX),Cancelled)
		FROM Policies WHERE PolicyID = @policyID;
		
		UPDATE Policies SET Cancelled = 1, CancelledBy = @modifiedby WHERE PolicyID = @policyID;
		

		SELECT CAST(1 as bit) as result;

		SELECT @details = 'The record ' + CONVERT(varchar(MAX),@policyID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@modifiedby),
			   @newdata = 'policyid:' +  CONVERT(varchar(MAX),@policyID) +  ';cancelled:' +  CONVERT(varchar(MAX),Cancelled) + ';cancelledBy:' +  CONVERT(varchar(MAX),@modifiedby) 
			   FROM Policies WHERE PolicyID = @policyID;
		
		EXEC LogAudit 'update', @modifiedby , @details, 'Policies', @policyID, @origdata, @newdata, 'update';
	END

GO
/****** Object:  StoredProcedure [dbo].[CancelPolicyRisks]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 03/10/2014
-- Description:	Cancelling all vehicles associated with a policy
-- =============================================
CREATE PROCEDURE [dbo].[CancelPolicyRisks]

	@polId int,
	@cancelledby int
	

AS
BEGIN
	
	SET NOCOUNT ON;

	 DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES
	

		INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
		SELECT 'update', @cancelledby, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),(VUPID)) + ' was cancelled from the database by ' + CONVERT(varchar(MAX),@cancelledby),
			   'VehiclesUnderPolicy',VUPID, 'VehicleUnderPolicyID: ' + CONVERT(varchar(MAX), (VUPID))+ ';Vehicle: ' + CONVERT(varchar(MAX), VehicleID)  + ';Cancelled: '+ CONVERT(varchar(MAX), Cancelled),
				'VehicleUnderPolicyID: ' + CONVERT(varchar(MAX), (VUPID))+ ';Vehicle: ' + CONVERT(varchar(MAX), VehicleID)  + ';Cancelled: '+ CONVERT(varchar(MAX), 1) + ';CancelledBy: '+ CONVERT(varchar(MAX), @cancelledby), 'update' 
		FROM VehiclesUnderPolicy WHERE PolicyID = @polId;


		UPDATE VehiclesUnderPolicy SET Cancelled = 1, CancelledBy = @cancelledby WHERE PolicyID = @polId;
	

END

GO
/****** Object:  StoredProcedure [dbo].[CancelRiskToPolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 19/09/2014 
-- Description:	Cancelling any asscoiation of a certain vehicle with all policies it is asscoaited with, within a particular timeframe 
-- =============================================
CREATE PROCEDURE [dbo].[CancelRiskToPolicy]

    @startDateTime varchar (250),
	@endDateTime varchar (250),
	@vehId int,
	@cancelledby int
	

AS
BEGIN
	
	SET NOCOUNT ON;

		DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
		DECLARE @policyId int; DECLARE @VUPID int;
   
	    DECLARE @smallEnddatetime smalldatetime = @endDateTime;
		DECLARE @end datetime = @smallEnddatetime;

		DECLARE @smallstartdatetime smalldatetime = @startDateTime;
		DECLARE @start datetime = @smallstartdatetime;

		
	     SELECT @policyId = Policies.PolicyID FROM Policies LEFT JOIN VehiclesUnderPolicy ON Policies.PolicyID = VehiclesUnderPolicy.PolicyID 
		                             WHERE VehiclesUnderPolicy.VehicleID = @vehId 
			                         AND VehiclesUnderPolicy.Cancelled = 0
									 AND ((@start >= Policies.StartDateTime AND @start <= Policies.EndDateTime) OR (@end <= Policies.EndDateTime AND @end >= Policies.StartDateTime 
									 OR (@start <= Policies.StartDateTime AND @end >= Policies.EndDateTime)));



		      SELECT @VUPID = VUPID FROM VehiclesUnderPolicy WHERE PolicyID = @policyId AND VehicleID = @vehId;

		      SELECT @origdata = 'VehiclesUnderPolicyId:' + CONVERT(varchar(MAX), @VUPID) +'Vehicle:' + CONVERT(varchar(MAX), @vehId) + 'PolicyID:' + CONVERT(varchar(MAX), @vehId) 
							   + 'Cancelled:'+ CONVERT(varchar(MAX), Cancelled)  FROM VehiclesUnderPolicy WHERE VUPID = @VUPID;

			  UPDATE VehiclesUnderPolicy SET Cancelled = 1, CancelledBy = @cancelledby WHERE VUPID = @VUPID;

			  -- If the policy no longer has any active vehicles under policy
			  IF NOT EXISTS (SELECT VehicleID FROM VehiclesUnderPolicy WHERE PolicyID = @policyId AND Cancelled = 0)
				SELECT CAST(0 as bit) as result, @policyId as [PolicyId];

			  ELSE
				SELECT CAST(1 as bit) as result;


			  SELECT @details = ('The record ' + CONVERT(varchar(MAX),(@VUPID)) 
			                     + ' was cancelled from the database by ' + CONVERT(varchar(MAX),@cancelledby)),
					 @newdata = 'VehicleUnderPolicy:' + CONVERT(varchar(MAX), (@VUPID))
					             + 'Cancelled:'+ CONVERT(varchar(MAX), Cancelled)  + 'CancelledBy:'+ CONVERT(varchar(MAX), @cancelledby) 
			 FROM VehiclesUnderPolicy WHERE VUPID = @VUPID;


			  EXEC LogAudit 'update', @cancelledby , @details, 'VehiclesUnderPolicy',  @VUPID, @origdata, @newdata, 'update';
END


GO
/****** Object:  StoredProcedure [dbo].[CancelVehicleToPolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CancelVehicleToPolicy]
	-- Add the parameters for the stored procedure here
	@vehid int,
	@polid int,
	@startDateTime varchar (250),
	@endDateTime varchar (250),
	@uid int
AS
BEGIN
	
	SET NOCOUNT ON;
  
    DECLARE @smallEnddatetime smalldatetime = @endDateTime;
	DECLARE @end datetime = @smallEnddatetime;

	DECLARE @smallstartdatetime smalldatetime = @startDateTime;
	DECLARE @start datetime = @smallstartdatetime;
	DECLARE @policyId int;


	SELECT @policyId = Policies.PolicyID  FROM VehiclesUnderPolicy
		               LEFT JOIN Policies ON Policies.PolicyID = VehiclesUnderPolicy.PolicyID
		                    WHERE VehicleID = @vehid AND  VehiclesUnderPolicy.Cancelled = 0
		                    AND ((@start >= Policies.StartDateTime AND @start <= Policies.EndDateTime) OR (@end <= Policies.EndDateTime AND @end >= Policies.StartDateTime 
				            OR (@start <= Policies.StartDateTime AND @end >= Policies.EndDateTime)));



    INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
		   SELECT 'update', @uid, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),VUPID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid), 
		   'VehiclesUnderPolicy', VUPID, 'VehicleID: '+ CONVERT(varchar(MAX),VehicleID) +';PolicyID: ' + CONVERT(varchar(MAX),VehiclesUnderPolicy.PolicyID) +';Cancelled: ' ++ CONVERT(varchar(MAX),VehiclesUnderPolicy.Cancelled),
		   'VehicleID: '+ CONVERT(varchar(MAX),VehiclesUnderPolicy.VehicleID) +';PolicyID: ' + CONVERT(varchar(MAX),VehiclesUnderPolicy.PolicyID) +';Cancelled: ' + CONVERT(varchar(MAX),1),
		   'update' 
		   FROM VehiclesUnderPolicy
		        LEFT JOIN Policies ON Policies.PolicyID = VehiclesUnderPolicy.PolicyID
		   WHERE VehicleID = @vehid AND  VehiclesUnderPolicy.Cancelled = 0
		        AND ((@start >= Policies.StartDateTime AND @start <= Policies.EndDateTime) OR (@end <= Policies.EndDateTime AND @end >= Policies.StartDateTime 
				OR (@start <= Policies.StartDateTime AND @end >= Policies.EndDateTime)));



	UPDATE VehiclesUnderPolicy 
	SET Cancelled = 1,
		CancelledBy = @uid
	FROM VehiclesUnderPolicy  LEFT JOIN Policies ON Policies.PolicyID = VehiclesUnderPolicy.PolicyID
	WHERE VehicleID = @vehid  AND  VehiclesUnderPolicy.Cancelled = 0
	      AND ((@start >= Policies.StartDateTime AND @start <= Policies.EndDateTime) OR (@end <= Policies.EndDateTime AND @end >= Policies.StartDateTime 
		  OR (@start <= Policies.StartDateTime AND @end >= Policies.EndDateTime))); 


    IF NOT EXISTS (SELECT VehicleID FROM VehiclesUnderPolicy WHERE PolicyID = @policyId AND Cancelled = 0)
	   SELECT CAST(0 as bit) as result, @policyId as [PolicyId];

    ELSE
	   SELECT CAST(1 as bit) result;

END

GO
/****** Object:  StoredProcedure [dbo].[CanCoverBeDeleted]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CanCoverBeDeleted]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN (COUNT(*) > 0) THEN CAST (0 as bit) ELSE CAST(1 as bit) END) [result] FROM 
	(
		SELECT PolicyID [ID] FROM Policies WHERE PolicyCover = @id
		UNION
		SELECT ID FROM Wording WHERE PolicyCover = @id
	)i;


END

GO
/****** Object:  StoredProcedure [dbo].[CanIGenerateCoverNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CanIGenerateCoverNote]
	-- Add the parameters for the stored procedure here
	@risk_id int,
	@policy_id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @insurer_id int, @intermediary_id int, @limit int;
	SELECT @insurer_id = CompanyID, @intermediary_id = CompanyCreatedBy FROM Policies WHERE PolicyID = @policy_id;
	SELECT @limit = PrintLimit FROM InsurerToIntermediary WHERE Insurer = @insurer_id AND Intermediary = @intermediary_id;
	
	SELECT (CASE WHEN(COUNT(*) > 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result]
	FROM VehiclesUnderPolicy vup 
	WHERE PolicyID = @policy_id AND VehicleID = @risk_id AND Cancelled = 0 AND (covercount < @limit OR @intermediary_id = @insurer_id);


END

GO
/****** Object:  StoredProcedure [dbo].[CanIPrintCoverNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CanIPrintCoverNote]
	-- Add the parameters for the stored procedure here
	@vcnid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	--SELECT PrintCount > = CoverNotePrintLimit 
	--FROM Company LEFT JOIN Employees ON Company.CompanyID = Employees.CompanyID
	--			 LEFT JOIN Users ON Employees.EmployeeID = Users.EmployeeID
	--WHERE Users.UID = @uid;
	
	SELECT Company.CoverNotePrintLimit, PrintLimitOverride, PrintCount 
	FROM VehicleCoverNote LEFT JOIN Company ON VehicleCoverNote.CompanyID = Company.CompanyID
	WHERE VehicleCoverNoteID = @vcnid;

END


GO
/****** Object:  StoredProcedure [dbo].[CanISeeThisPerson]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CanISeeThisPerson]
	-- Add the parameters for the stored procedure here
	@personid int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT (CASE WHEN(COUNT(*)>0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result]
	FROM PolicyInsured poli LEFT JOIN Policies p ON poli.PolicyID = p.PolicyID
							LEFT JOIN Company c ON p.CompanyID = c.CompanyID OR p.CompanyCreatedBy = c.CompanyID
							LEFT JOIN Employees e ON e.CompanyID = c.CompanyID
							LEFT JOIN Users u ON u.EmployeeID = e.EmployeeID
	WHERE poli.PersonID = @personid AND u.[UID] = @uid;

END

GO
/****** Object:  StoredProcedure [dbo].[CanIViewPolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CanIViewPolicy]
	-- Add the parameters for the stored procedure here
	@id int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @cid int;
	SELECT @cid = e.CompanyID FROM Employees e LEFT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE u.[UID] = @uid;
	
	SELECT (CASE WHEN(COUNT(p.PolicyID) = 1) THEN CAST (1 as bit) ELSE CAST (0 as bit) END) [result]
	FROM Policies p LEFT JOIN Company ci ON p.CompanyID = ci.CompanyID
					LEFT JOIN Company cc ON p.CompanyCreatedBy = cc.CompanyID
	WHERE (ci.CompanyID = @cid OR cc.CompanyID = @cid) AND p.PolicyID = @id;
END

GO
/****** Object:  StoredProcedure [dbo].[CanIViewWording]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CanIViewWording]
	-- Add the parameters for the stored procedure here
	@wordid int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN(COUNT(*) > 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result]
	FROM Wording w	LEFT JOIN Users u ON w.CreatedBy = u.[UID]
					LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID
	WHERE w.ID = @wordid AND e.CompanyID = (SELECT c.CompanyID FROM Company c LEFT JOIN Employees e ON c.CompanyID = e.CompanyID LEFT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE u.[UID] = @uid);
END

GO
/****** Object:  StoredProcedure [dbo].[CanWordingBeDeleted]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CanWordingBeDeleted]
	-- Add the parameters for the stored procedure here
	@wordid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN(Count(*) > 0) THEN CAST(0 as bit) ELSE CAST(1 as bit) END) [result]
	FROM VehicleCoverNote vcn, VehicleCertificates vc WHERE vcn.WordingTemplate = @wordid OR vc.WordingTemplate = @wordid;
END

GO
/****** Object:  StoredProcedure [dbo].[CertificateCountEffective]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CertificateCountEffective]
	-- Add the parameters for the stored procedure here
	@cid int,
	@month varchar(50),
	@year varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	COUNT(vc.VehicleCertificateID) [count]
	FROM VehicleCertificates vc	LEFT JOIN Policies p ON vc.PolicyID = p.PolicyID
	WHERE	LOWER(DATENAME(YEAR,vc.CreatedOn)) = LOWER(@year) AND 
			LOWER(DATENAME(MONTH, vc.CreatedOn)) = LOWER(@month) AND 
			(p.CompanyID = @cid OR p.CompanyCreatedBy = @cid OR (SELECT iaj FROM Company WHERE CompanyID = @cid) = 1)
END

GO
/****** Object:  StoredProcedure [dbo].[CertificateGenSearch]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CertificateGenSearch]
	-- Add the parameters for the stored procedure here
	@term varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @search varchar(MAX); DECLARE @run bit; DECLARE @i int;
	SET @run = 1; SET @i = 0;
	SET @search = SUBSTRING(@term, 1, (CASE WHEN (CHARINDEX('&', @term) > 0) THEN (CHARINDEX('&', @term) - 1) ELSE LEN(@term) END));
	SET @term = SUBSTRING(@term, 2 + LEN(@search), LEN(@term)); 
	SET @search = '%' + @search + '%';

	CREATE TABLE #TempTable(ID int); CREATE TABLE #TempTable2 (ID int);

	WHILE @run = 1
	BEGIN
		
		SET @i = @i + 1;

    -- Insert statements for procedure here
		IF (@i = 1)
			INSERT INTO #TempTable (ID)
			SELECT vc.VehicleCertificateID [ID]--, c.CompanyName, p.PolicyID [PID], p.Policyno [PolicyNumber], pc.Cover [cover], vc.UniqueNum [unique], vc.EffectiveDate [effective], vc.ExpiryDate [expiry]
			FROM VehicleCertificates vc LEFT JOIN Company c ON vc.CompanyID = c.CompanyID
										LEFT JOIN Vehicles v ON vc.RiskItemNo = v.VehicleID
										LEFT JOIN VehicleCoverNoteState s ON vc.[state] = s.VCNSID
										LEFT JOIN Policies p ON vc.PolicyID = p.PolicyID
										LEFT JOIN PolicyCovers pc ON p.PolicyCover = pc.ID
			WHERE (c.CompanyName LIKE @search
				OR CAST(vc.EffectiveDate as varchar(MAX)) LIKE @search
				OR CAST(vc.ExpiryDate as varchar(MAX)) LIKE @search
				OR CAST(vc.EndorsementNo as varchar(MAX)) LIKE @search
				OR CAST(vc.PrintedPaperNo as varchar(MAX)) LIKE @search
				OR v.VehicleRegistrationNo LIKE @search
				OR v.VIN LIKE @search
				OR CAST(vc.MarksRegNo as varchar(MAX)) LIKE @search
				OR vc.Usage LIKE @search
				OR vc.Scheme LIKE @search
				OR s.[state] LIKE @search
				OR vc.CertificateType LIKE @search
				OR vc.ExtensionCode LIKE @search
				OR CAST(vc.UniqueNum as varchar(MAX)) LIKE @search);

		ELSE 
		BEGIN
			INSERT INTO #TempTable2 (ID) SELECT ID FROM #TempTable;
			DELETE #TempTable;
			INSERT INTO #TempTable (ID)
			SELECT vc.VehicleCertificateID [ID]--, c.CompanyName, p.PolicyID [PID], p.Policyno [PolicyNumber], pc.Cover [cover], vc.UniqueNum [unique], vc.EffectiveDate [effective], vc.ExpiryDate [expiry]
			FROM VehicleCertificates vc LEFT JOIN Company c ON vc.CompanyID = c.CompanyID
										LEFT JOIN Vehicles v ON vc.RiskItemNo = v.VehicleID
										LEFT JOIN VehicleCoverNoteState s ON vc.[state] = s.VCNSID
										LEFT JOIN Policies p ON vc.PolicyID = p.PolicyID
										LEFT JOIN PolicyCovers pc ON p.PolicyCover = pc.ID
			WHERE (c.CompanyName LIKE @search
				OR CAST(vc.EffectiveDate as varchar(MAX)) LIKE @search
				OR CAST(vc.ExpiryDate as varchar(MAX)) LIKE @search
				OR CAST(vc.EndorsementNo as varchar(MAX)) LIKE @search
				OR CAST(vc.PrintedPaperNo as varchar(MAX)) LIKE @search
				OR v.VehicleRegistrationNo LIKE @search
				OR v.VIN LIKE @search
				OR CAST(vc.MarksRegNo as varchar(MAX)) LIKE @search
				OR vc.Usage LIKE @search
				OR vc.Scheme LIKE @search
				OR s.[state] LIKE @search
				OR vc.CertificateType LIKE @search
				OR vc.ExtensionCode LIKE @search
				OR CAST(vc.UniqueNum as varchar(MAX)) LIKE @search)
				AND vc.VehicleCertificateID IN (SELECT ID FROM #TempTable2);
		END

		IF(LEN(@term) = 0) break;
		
		SET @search = SUBSTRING(@term, 1, (CASE WHEN (CHARINDEX('&', @term) > 0) THEN (CHARINDEX('&', @term) - 1) ELSE LEN(@term) END));
		SET @term = SUBSTRING(@term, 2 + LEN(@search), LEN(@term)); 
		SET @search = '%' + @search + '%';
		
		

	END

	SELECT vc.VehicleCertificateID [ID], c.CompanyName, p.PolicyID [PID], p.Policyno [PolicyNumber], pc.Cover [cover], vc.UniqueNum [unique], vc.EffectiveDate [effective], vc.ExpiryDate [expiry]
	FROM VehicleCertificates vc LEFT JOIN Company c ON vc.CompanyID = c.CompanyID
								LEFT JOIN Vehicles v ON vc.RiskItemNo = v.VehicleID
								LEFT JOIN VehicleCoverNoteState s ON vc.[state] = s.VCNSID
								LEFT JOIN Policies p ON vc.PolicyID = p.PolicyID
								LEFT JOIN PolicyCovers pc ON p.PolicyCover = pc.ID
	WHERE vc.VehicleCertificateID IN (SELECT ID FROM #TempTable);

	DROP TABLE #TempTable;DROP TABLE #TempTable2;

END


GO
/****** Object:  StoredProcedure [dbo].[CertIntelCountForTime]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CertIntelCountForTime]
	-- Add the parameters for the stored procedure here
	@start datetime,
	@end datetime,
	@cid int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT c.CompanyID [cid], c.CompanyName [cname], Count(ci.ID) [count]
	FROM CertIntel ci LEFT JOIN  Company c ON ci.companyID = c.CompanyID
	WHERE ci.[date] >= @start AND ci.[date] <= @end
	GROUP BY c.CompanyID, c.CompanyName;

END

GO
/****** Object:  StoredProcedure [dbo].[CheckCompanyByIAJID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CheckCompanyByIAJID]
	-- Add the parameters for the stored procedure here
	@CompanyIAJID varchar (max),
	@compid int = 0
AS
BEGIN
	
	SET NOCOUNT ON;

	
	SELECT (CASE WHEN (COUNT(*) > 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result] FROM Company 
	WHERE IAJID = @CompanyIAJID AND (@compid = 0 OR CompanyID <> @compid)
	
	
	
	--DECLARE @result bit;
 --   IF EXISTS (SELECT CompanyID FROM Company WHERE IAJID = @CompanyIAJID)
	
	--BEGIN
	--	SET @result = 1;
	--	SELECT @result AS [result];
	--END
	

	--ELSE 
	--	BEGIN
	--	SET @result = 0;
	--	SELECT @result AS [result];
	--END
END

GO
/****** Object:  StoredProcedure [dbo].[ClearUserRecordLock]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ClearUserRecordLock]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Users
	SET LoggedIn = 0 
	WHERE LoggedIn = 1 AND DATEDIFF(minute, LastSuccessful, CURRENT_TIMESTAMP) > 5;

END


GO
/****** Object:  StoredProcedure [dbo].[CompanyGenSearch]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CompanyGenSearch]
	-- Add the parameters for the stored procedure here
	@term varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @search varchar(MAX); DECLARE @run bit; DECLARE @i int;
	SET @run = 1; SET @i = 0;
	SET @search = SUBSTRING(@term, 1, (CASE WHEN (CHARINDEX('&', @term) > 0) THEN (CHARINDEX('&', @term) - 1) ELSE LEN(@term) END));
	SET @term = SUBSTRING(@term, 2 + LEN(@search), LEN(@term)); 
	SET @search = '%' + @search + '%';

	CREATE TABLE #TempTable(ID int); CREATE TABLE #TempTable2 (ID int);

	WHILE @run = 1
	BEGIN

		SET @i = @i + 1;

		IF(@i = 1)
		INSERT INTO #TempTable (ID)
		SELECT CompanyID [ID]--, CompanyName [name], IAJID, TRN, CompanyTypes.[Type]
			FROM Company LEFT JOIN CompanyTypes ON Company.[Type] = CompanyTypes.ID
			WHERE CompanyName LIKE @search 
			   OR CAST(IAJID as varchar(MAX)) LIKE @search 
			   OR TRN LIKE @search
			   OR CompanyTypes.[Type] LIKE @search;

		ELSE
		BEGIN
			INSERT INTO #TempTable2 (ID) SELECT ID FROM #TempTable;
			DELETE #TempTable;
			INSERT INTO #TempTable (ID)
			SELECT CompanyID [ID]--, CompanyName [name], IAJID, TRN, CompanyTypes.[Type]
				FROM Company LEFT JOIN CompanyTypes ON Company.[Type] = CompanyTypes.ID
				WHERE (CompanyName LIKE @search 
				   OR CAST(IAJID as varchar(MAX)) LIKE @search 
				   OR TRN LIKE @search
				   OR CompanyTypes.[Type] LIKE @search)
				   AND Company.CompanyID IN (SELECT ID FROM #TempTable2);
		END

		IF(LEN(@term) = 0) break;
		
		SET @search = SUBSTRING(@term, 1, (CASE WHEN (CHARINDEX('&', @term) > 0) THEN (CHARINDEX('&', @term) - 1) ELSE LEN(@term) END));
		SET @term = SUBSTRING(@term, 2 + LEN(@search), LEN(@term)); 
		SET @search = '%' + @search + '%';
		
		

	END

	SELECT CompanyID [ID], CompanyName [name], IAJID, TRN, CompanyTypes.[Type]
		FROM Company LEFT JOIN CompanyTypes ON Company.[Type] = CompanyTypes.ID
		WHERE Company.CompanyID IN (SELECT ID FROM #TempTable);

	DROP TABLE #TempTable;DROP TABLE #TempTable2;
END


GO
/****** Object:  StoredProcedure [dbo].[CopyRoleForCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CopyRoleForCompany]
	-- Add the parameters for the stored procedure here
	@UGID int,
	@cid int,
	@uid int
AS
BEGIN
	
	SET NOCOUNT ON;

	INSERT INTO UserGroups (SuperGroup, UserGroup, Company, CreatedBy, LastModifiedBy, [system], CopyFrom) 
	SELECT SuperGroup, UserGroup, @cid, @uid, @uid, [system], @UGID FROM UserGroups WHERE UGID = @UGID;

	DECLARE @newgroupid int; SET @newgroupid = @@IDENTITY;

	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'insert', @uid, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),UGID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
		   'UserGroups',UGID, null, 'UGID: ' + CONVERT(varchar(MAX),UGID) + ';User Group: ' + UserGroup +';Company' + CONVERT(varchar(MAX),Company) + ';System: ' + CONVERT(varchar(MAX),[system]), 'insert'
	FROM UserGroups WHERE UGID = @newgroupid;



	INSERT INTO PermissionGroupAssoc (UPID, UGID)
	SELECT UPID, @newgroupid FROM PermissionGroupAssoc WHERE UGID = @UGID;

	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'insert', @uid, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX), PGAID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
		   'PermissionGroupAssoc',PGAID, null, 'UPID: ' + CONVERT(varchar(MAX),UPID) + ';UGID: ' + CONVERT(varchar(MAX),UGID), 'insert'
	FROM PermissionGroupAssoc WHERE UGID = @newgroupid;

	 
END

GO
/****** Object:  StoredProcedure [dbo].[CopyStockGroups]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CopyStockGroups]
	-- Add the parameters for the stored procedure here
	@cid int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	INSERT INTO UserGroups (SuperGroup, UserGroup, Company, CreatedBy, LastModifiedBy, [system], CopyFrom, administrative) 
	SELECT SuperGroup, UserGroup, @cid, @uid, @uid, [system], UGID, administrative FROM UserGroups WHERE stock = 1 AND [system] = 0;

	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'insert', @uid, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),UGID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
		   'UserGroups', UGID, null, 'UGID: ' + CONVERT(varchar(MAX),UGID) +';UserGroup: ' + UserGroup+ ';CompanyID:' + CONVERT(varchar(MAX),@cid), 'insert'
	FROM  UserGroups WHERE stock = 1 AND [system] = 0;

	
	DECLARE @newgroupid int; SET @newgroupid = @@IDENTITY;

	INSERT INTO PermissionGroupAssoc (UPID, UGID)
	SELECT up.UPID, ug_new.UGID FROM
	PermissionGroupAssoc up LEFT JOIN UserGroups ug_orig ON up.UGID = ug_orig.UGID LEFT JOIN UserGroups ug_new ON ug_orig.UGID = ug_new.CopyFrom
	WHERE ug_new.Company = @cid;


	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'insert', @uid, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),PGAID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
		   'PermissionGroupAssoc', PGAID, null, 'UPID: ' + CONVERT(varchar(MAX),UPID) +';UGID: ' + CONVERT(varchar(MAX),ug_orig.UGID), 'insert'
	FROM PermissionGroupAssoc up 
	     LEFT JOIN UserGroups ug_orig ON up.UGID = ug_orig.UGID 
		 LEFT JOIN UserGroups ug_new ON ug_orig.UGID = ug_new.CopyFrom
	WHERE ug_new.Company = @cid;

	
END

GO
/****** Object:  StoredProcedure [dbo].[CoverNoteCountEffective]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CoverNoteCountEffective]
	-- Add the parameters for the stored procedure here
	@cid int,
	@month varchar(50),
	@year varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	COUNT(vcn.VehicleCoverNoteID) [count]
	FROM VehicleCoverNote vcn	LEFT JOIN Policies p ON vcn.PolicyID = p.PolicyID 
	WHERE	LOWER(DATENAME(YEAR,vcn.EffectiveDate)) = LOWER(@year) AND 
			LOWER(DATENAME(MONTH, vcn.EffectiveDate)) = LOWER(@month) AND 
			(p.CompanyID = @cid OR p.CompanyCreatedBy = @cid OR (SELECT iaj FROM Company WHERE CompanyID = @cid) = 1)
END

GO
/****** Object:  StoredProcedure [dbo].[CoverNoteGenSearch]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CoverNoteGenSearch]
	-- Add the parameters for the stored procedure here
	@term varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @search varchar(MAX); DECLARE @run bit; DECLARE @i int;
	SET @run = 1; SET @i = 0;
	SET @search = SUBSTRING(@term, 1, (CASE WHEN (CHARINDEX('&', @term) > 0) THEN (CHARINDEX('&', @term) - 1) ELSE LEN(@term) END));
	SET @term = SUBSTRING(@term, 2 + LEN(@search), LEN(@term)); 
	SET @search = '%' + @search + '%';

	CREATE TABLE #TempTable(ID int); CREATE TABLE #TempTable2 (ID int);

	WHILE @run = 1
	BEGIN

		SET @i = @i + 1;
	
    -- Insert statements for procedure here
		IF (@i = 1)
			INSERT INTO #TempTable (ID)
			SELECT vcn.VehicleCoverNoteID [ID]--, vcn.EffectiveDate [start], vcn.period, vcn.Cancelled, vcn.approved, c.CompanyName, vcn.ManualCoverNoteNo [covernoteno]
				FROM VehicleCoverNote vcn LEFT JOIN Company c ON vcn.CompanyID = c.CompanyID
										  LEFT JOIN Vehicles v  ON vcn.RiskItemNo = v.VehicleID
				WHERE (c.CompanyName LIKE @search
				   OR vcn.Alterations LIKE @search
				   OR CAST(vcn.CoverNoteID as varchar(MAX)) LIKE @search
				   OR CAST(vcn.EffectiveDate as varchar(MAX)) LIKE @search
				   OR CAST(vcn.EndorsementNo as varchar(MAX)) LIKE @search
				   OR CAST(vcn.ManualCoverNoteNo as varchar(MAX)) LIKE @search
				   OR v.VehicleRegistrationNo LIKE @search
				   OR v.VIN LIKE @search
				   OR vcn.LimitsOfUse LIKE @search
				   OR CAST(vcn.serial as varchar(MAX)) LIKE @search);

		ELSE
		BEGIN
			INSERT INTO #TempTable2 (ID) SELECT ID FROM #TempTable;
			DELETE #TempTable;
			INSERT INTO #TempTable (ID)
			SELECT vcn.VehicleCoverNoteID [ID]--, vcn.EffectiveDate [start], vcn.period, vcn.Cancelled, vcn.approved, c.CompanyName, vcn.ManualCoverNoteNo [covernoteno]
				FROM VehicleCoverNote vcn LEFT JOIN Company c ON vcn.CompanyID = c.CompanyID
										  LEFT JOIN Vehicles v  ON vcn.RiskItemNo = v.VehicleID
				WHERE (c.CompanyName LIKE @search
				   OR vcn.Alterations LIKE @search
				   OR CAST(vcn.CoverNoteID as varchar(MAX)) LIKE @search
				   OR CAST(vcn.EffectiveDate as varchar(MAX)) LIKE @search
				   OR CAST(vcn.EndorsementNo as varchar(MAX)) LIKE @search
				   OR CAST(vcn.ManualCoverNoteNo as varchar(MAX)) LIKE @search
				   OR v.VehicleRegistrationNo LIKE @search
				   OR v.VIN LIKE @search
				   OR vcn.LimitsOfUse LIKE @search
				   OR CAST(vcn.serial as varchar(MAX)) LIKE @search)
				   AND vcn.VehicleCoverNoteID IN (SELECT ID FROM #TempTable2);
		END

		IF(LEN(@term) = 0) break;
		
		SET @search = SUBSTRING(@term, 1, (CASE WHEN (CHARINDEX('&', @term) > 0) THEN (CHARINDEX('&', @term) - 1) ELSE LEN(@term) END));
		SET @term = SUBSTRING(@term, 2 + LEN(@search), LEN(@term)); 
		SET @search = '%' + @search + '%';

	END

	SELECT vcn.VehicleCoverNoteID [ID], vcn.EffectiveDate [start], vcn.period, vcn.Cancelled, vcn.approved, c.CompanyName, vcn.ManualCoverNoteNo [covernoteno]
			FROM VehicleCoverNote vcn LEFT JOIN Company c ON vcn.CompanyID = c.CompanyID
									  LEFT JOIN Vehicles v  ON vcn.RiskItemNo = v.VehicleID
			WHERE vcn.VehicleCoverNoteID IN (SELECT ID FROM #TempTable);

	DROP TABLE #TempTable;DROP TABLE #TempTable2;
END


GO
/****** Object:  StoredProcedure [dbo].[CoverNoteHistory]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CoverNoteHistory]
	-- Add the parameters for the stored procedure here
	@polid int,
	@vehid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  vcn.VehicleCoverNoteID [id], vcn.EffectiveDate [effective], vcn.period, vcn.ExpiryDateTime [expiry], 
			vcn.Cancelled [cancel], vcn.approved [approve], c.CompanyID [cid], c.CompanyName [cname], vcn.CoverNoteID [covernoteid], 
			vcn.DateFirstPrinted [first]
	FROM VehicleCoverNote vcn LEFT JOIN Company c ON vcn.CompanyID = c.CompanyID
	WHERE vcn.PolicyID = @polid AND vcn.RiskItemNo = @vehid;
END

GO
/****** Object:  StoredProcedure [dbo].[CoverNoteHistoryExist]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CoverNoteHistoryExist]
	-- Add the parameters for the stored procedure here
	@polid int,
	@vehid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN(COUNT(*) > 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result]
	FROM VehicleCoverNote vcn LEFT JOIN Company c ON vcn.CompanyID = c.CompanyID
	WHERE vcn.PolicyID = @polid AND vcn.RiskItemNo = @vehid;
END

GO
/****** Object:  StoredProcedure [dbo].[CoverSearchCerts]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CoverSearchCerts]
	-- Add the parameters for the stored procedure here
	@start datetime,
	@end datetime,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	vc.EDIID [ediid], vc.CertificateNo [certno], p.StartDateTime [effective], p.EndDateTime [expiry], vc.Cancelled [cancelled], 
			peocan.FName [cancelledbyfname], peocan.LName [cancelledbylname], vc.SignedAt [signon], peosign.TRN [signtrn], 
			vc.DateFirstPrinted [firstprinted], vc.LastPrintedOn [lastprinted], peoprint.TRN [lastprintedbytrn], 
			p.StartDateTime [effective], p.EndDateTime [expiry], vc.EndorsementNo [endorsementno], gc.LimitsOfUse [limits], vc.MarksRegNo [marksregno],
			vc.PrintedPaperNo [printedpaperno], risk.VehicleID [risk_id], risk.riskItemNo [risk_item_no], risk.ChassisNo [chassis], vc.PolicyID [polid]
	FROM 
	VehicleCertificates vc	LEFT OUTER JOIN GeneratedCertificate gc ON vc.VehicleCertificateID = gc.VehicleCertificateID
							LEFT JOIN Policies p ON vc.PolicyID = p.PolicyID
							LEFT OUTER JOIN People peocan ON vc.CancelledBy = peocan.PeopleID
							LEFT OUTER JOIN People peoprint ON vc.LastPrintedBy = peoprint.PeopleID
							LEFT OUTER JOIN Users usign ON vc.SignedBy = usign.[UID]
							LEFT JOIN Employees esign ON usign.EmployeeID = esign.EmployeeID
							LEFT JOIN People peosign ON esign.PersonID = peosign.PeopleID
							LEFT JOIN Vehicles risk ON vc.RiskItemNo = risk.VehicleID
	WHERE	vc.CreatedOn >= @start 
		AND vc.CreatedOn <= @end 
		AND vc.CompanyID = (SELECT c.CompanyID FROM Users u LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID LEFT JOIN Company c ON e.CompanyID = c.CompanyID WHERE u.[UID] = @uid);
END

GO
/****** Object:  StoredProcedure [dbo].[CoverSearchCoverNotes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CoverSearchCoverNotes]
	-- Add the parameters for the stored procedure here
	@start datetime,
	@end datetime,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	vcn.EDIID [ediid], vcn.Alterations [alterations], vcn.Cancelled [cancelled], peocan.FName [cancelledbyfname], peocan.LName [cancelledbylname],
			vcn.CancelledReason [cancelledreason], vcn.CancelledOn [cancelledon], vcn.DateFirstPrinted [firstprinted], vcn.LastPrinted [lastprinted],
			peoprint.TRN [lastprintedbytrn], vcn.EffectiveDate [effective], vcn.ExpiryDateTime [expiry], vcn.period [period],
			vcn.EndorsementNo [endorsementno], vcn.LimitsOfUse [limits], vcn.ManualCoverNoteNo [covernoteno], vcn.PrintCount [printcount], vcn.PrintedPaperNo [printedpaperno],
			risk.VehicleID [risk_id], risk.riskItemNo [risk_item_no], risk.ChassisNo [chassis], gcn.mortgagees [mortgagees], gcn.LimitsOfUse [limitsofuse],
			vcn.CoverNoteID [covernoteid], vcn.PolicyID [polid]
	FROM 
	VehicleCoverNote vcn LEFT OUTER JOIN GeneratedCoverNotes gcn ON vcn.VehicleCoverNoteID = gcn.VehicleCoverNoteID
						 LEFT OUTER JOIN People peocan ON vcn.CancelledBy = peocan.PeopleID
						 LEFT OUTER JOIN People peoprint ON vcn.LastPrintedBy = peoprint.PeopleID
						 LEFT JOIN Vehicles risk ON vcn.RiskItemNo = risk.VehicleID
	WHERE	vcn.CreatedOn >= @start 
		AND vcn.CreatedOn <= @end 
		AND vcn.CompanyID = (SELECT c.CompanyID FROM Users u LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID LEFT JOIN Company c ON e.CompanyID = c.CompanyID WHERE u.[UID] = @uid);
END

GO
/****** Object:  StoredProcedure [dbo].[CreateCertificateFormat]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 01/09/2014
-- Description:	Creating a vehicle certificate format
-- =============================================
CREATE PROCEDURE [dbo].[CreateCertificateFormat]
	@title varchar(250),
	@format text,
	@uid int
AS
BEGIN
	
	SET NOCOUNT ON;

    DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	INSERT INTO CertificateFormatting (CreatedBy, createdOn, title, CertificateFormat, LastModifiedBy, Imported) VALUES (@uid, CURRENT_TIMESTAMP, @title, @format, @uid, 0);


	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
					   @origdata = null,
					   @newdata = 'CertificateFormat:' + CONVERT(varchar(MAX),@format)+';Title:' + @title;
					   	             
							
	EXEC LogAudit 'insert', @uid , @details, 'CertificateFormatting',  @@IDENTITY, @origdata, @newdata, 'insert';

END


GO
/****** Object:  StoredProcedure [dbo].[CreateContactGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateContactGroup]
	-- Add the parameters for the stored procedure here
	@uid int,
	@name varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

    -- Insert statements for procedure here
	IF NOT EXISTS (SELECT ID FROM ContactGroups WHERE name = @name AND [Owner] = @uid)
	BEGIN		
		INSERT INTO ContactGroups (name, [Owner]) VALUES (@name, @uid);
		SELECT @@IDENTITY as ID;
		SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
			   @origdata = null,
			   @newdata = 'ContactGroup:' + @name;
		EXEC LogAudit 'insert', @uid , @details, 'ContactGroup', @@IDENTITY, @origdata, @newdata, 'insert';
	END
	ELSE 
		SELECT ID FROM ContactGroups WHERE name = @name AND [Owner] = @uid;

END


GO
/****** Object:  StoredProcedure [dbo].[CreateCoverNoteFormat]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateCoverNoteFormat]
	-- Add the parameters for the stored procedure here
	@title varchar(MAX),
	@format text,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO CoverNoteFormatting (CreatedBy, title, CoverNoteFormat) VALUES (@uid, @title, @format);

END


GO
/****** Object:  StoredProcedure [dbo].[CreateNewUserGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateNewUserGroup]
	@GroupName varchar(250),
	@supergroup bit,
	@administrative bit,
	@UID int,
	@comp int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @result bit, @groupID int;

	IF EXISTS(SELECT UGID FROM UserGroups WHERE UserGroup = @GroupName AND Company = @comp)
	BEGIN
		SET @result = 0;
		SELECT @result AS result;
	END
	ELSE
	BEGIN
		INSERT INTO UserGroups (UserGroup, SuperGroup, CreatedBy, LastModifiedBy, Company, administrative) VALUES (@GroupName, @supergroup, @UID, @UID, (CASE WHEN (@comp = 0) THEN (SELECT CompanyID FROM Employees e LEFT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE u.[UID] = @uid) ELSE @comp END), @administrative);

		SET @groupID = @@IDENTITY; 

		INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
		SELECT 'insert',
			   @UID,
			   CURRENT_TIMESTAMP,
			   'The record ' + CONVERT(varchar(MAX), @@IDENTITY) + ' has been added from the system by ' + CONVERT(varchar(MAX), @UID),
			   'UserGroups',
			   @@IDENTITY,
			   null,
			   'GroupName:' + CONVERT(varchar(MAX), @GroupName),
			   'insert';

		SET @result = 1;
		SELECT @result [result] , @groupID [GroupID];
	END
END


GO
/****** Object:  StoredProcedure [dbo].[CreateNotification]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateNotification]
	-- Add the parameters for the stored procedure here
	@createduid int = null,
	@notefrom int = null,
	@head text,
	@content text,
	@parentnid int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	
	IF (@createduid IS NULL OR @createduid = 0) SELECT @createduid = [UID] FROM Users WHERE username = 'system';
	IF (@notefrom IS NULL OR @notefrom = 0) SELECT @notefrom = [UID] FROM Users WHERE username = 'system';
	
	INSERT INTO Notifications (CreatedBy, NoteFrom, Heading, Content, ParentMessage) VALUES (@createduid, @notefrom, @head, @content, @parentnid);
	SELECT @@IDENTITY as NID;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES
	SELECT @origdata = null,
		   @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createduid),
		   @newdata = 'Notification:' +  CONVERT(varchar(MAX),@@IDENTITY) +  ';NoteFrom:' + CONVERT(varchar(MAX),@notefrom) + ';Heading:' + CONVERT(varchar(MAX),@head) + ';Content:' + CONVERT(varchar(MAX),@content) + ';Seen:'  + CONVERT(varchar(MAX),0)  + ';Read:' + CONVERT(varchar(MAX),0) + ';Deleted:' + CONVERT(varchar(MAX),0) + ';New:' + CONVERT(varchar(MAX),0);
	EXEC LogAudit 'insert', @createduid , @details, 'Notifications', @@IDENTITY, @origdata, @newdata, 'insert';


	


END


GO
/****** Object:  StoredProcedure [dbo].[CreateUsage]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateUsage]
	-- Add the parameters for the stored procedure here
	@usage varchar(MAX),
	@uid int
AS
BEGIN

	SET NOCOUNT ON;

    DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	IF EXISTS (SELECT ID FROM Usages WHERE usage = @usage)
	BEGIN
		SELECT ID FROM Usages WHERE usage = @usage;
	END
	ELSE
	BEGIN
	    

		INSERT INTO Usages (usage, CreatedBy) VALUES (@usage, @uid);
		SELECT @@IDENTITY [ID];

		SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
		       @origdata = null, 
			   @newdata = 'Usage: ' +  @usage;
		
		EXEC LogAudit 'insert', @uid , @details, 'Usages', @@IDENTITY, @origdata, @newdata, 'insert';


	END
	

END

GO
/****** Object:  StoredProcedure [dbo].[CreateVehicleCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 13/08/2014
-- Description:	Creating a vehicle certificate
-- =============================================
CREATE PROCEDURE [dbo].[CreateVehicleCertificate]

	@VehicleID int,
	@CreatedBy int,
	@CompanyID int,
	@PrintedPaperNo int,
	@policyID int,
	@wordingTemplateID int,
	@certNumber varchar (250),
	@uniqueNum int,
	@endorsementno varchar(max) = null,
	@datefirstprint datetime2,
	@lastprintedon datetime2,
	@lastprintflat varchar(max) = null,
	@marksandregno varchar(200) = null,
	@ediid varchar(max) = null,
	@import bit = 0
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

    
	-- CHECK TO ENSURE THAT THE VEHICLE CERTIFICATE IS NOT ALREADY EXISTENT
	
	IF EXISTS (SELECT VehicleCertificateID FROM VehicleCertificates WHERE (RiskItemNo = @VehicleID AND PolicyID = @policyID) OR (EDIID = @ediid)) AND @import = 0
	BEGIN
		SELECT CAST(0 AS bit) AS result;
	END

	ELSE 
	BEGIN
		IF EXISTS (SELECT VehicleCertificateID FROM VehicleCertificates WHERE (RiskItemNo = @VehicleID AND PolicyID = @policyID) OR (EDIID = @ediid)) AND @import = 1
		BEGIN
	  

		  UPDATE VehicleCertificates
		  SET 
				EndorsementNo = @endorsementno,
				LastModifiedBy = @CreatedBy,
				CompanyID = @CompanyID,
				PrintedPaperNo = (CASE WHEN @PrintedPaperNo = 0 THEN NULL ELSE @PrintedPaperNo END),
				WordingTemplate = (CASE WHEN @wordingTemplateID = 0 THEN NULL ELSE @wordingTemplateID END),
				CertificateNo = @certNumber,
				UniqueNum = @uniqueNum,
				DateFirstPrinted = (CASE WHEN(DATEPART(YEAR,@datefirstprint) > 1972) THEN CAST(@datefirstprint as datetime) ELSE NULL END),
				LastPrintedOn = (CASE WHEN(DATEPART(YEAR,@lastprintedon) > 1972) THEN CAST(@lastprintedon as datetime) ELSE NULL END),
				LastPrintedByFlat = @lastprintflat,
				MarksRegNo = @marksandregno,
				EDIID = @ediid
		  WHERE (EDIID = @ediid AND @ediid IS NOT NULL) OR (RiskItemNo = @VehicleID AND PolicyID = @policyID)

		  SELECT CAST(1 AS bit) AS result, VehicleCertificateID [ID] FROM VehicleCertificates WHERE (EDIID = @ediid AND @ediid IS NOT NULL) OR (RiskItemNo = @VehicleID AND PolicyID = @policyID);

		END


		ELSE
		BEGIN
			INSERT INTO VehicleCertificates (CreatedOn, CreatedBy, CompanyID, PrintedPaperNo, RiskItemNo, PrintCount, PrintCountInsurer,
											 LastModifiedBy, approved, CertificateNo, PolicyID, Printed, Generated, WordingTemplate, RequestPrint, UniqueNum,
											 EndorsementNo, DateFirstPrinted, LastPrintedOn, LastPrintedByFlat, FirstPrintedByFlat, MarksRegNo, EDIID)  			
			VALUES (CURRENT_TIMESTAMP, @CreatedBy, @CompanyID, (CASE WHEN @PrintedPaperNo = 0 THEN NULL ELSE @PrintedPaperNo END), @VehicleID, 0, 0, @CreatedBy, 
					0, @certNumber, @policyID, 0, 0, (CASE WHEN @wordingTemplateID = 0 THEN NULL ELSE @wordingTemplateID END),0, @uniqueNum,
					(CASE WHEN(@endorsementno IS NULL OR @endorsementno = '') THEN NULL ELSE @endorsementno END),
					(CASE WHEN(DATEPART(YEAR,@datefirstprint) > 1972) THEN CAST(@datefirstprint as datetime) ELSE NULL END), 
					(CASE WHEN(DATEPART(YEAR,@lastprintedon) > 1972) THEN CAST(@lastprintedon as datetime) ELSE NULL END), 
					(CASE WHEN(@lastprintflat IS NULL OR @lastprintflat = '') THEN NULL ELSE @lastprintflat END),
					(CASE WHEN(@lastprintflat IS NULL OR @lastprintflat = '') THEN NULL ELSE @lastprintflat END),
					(CASE WHEN(@marksandregno IS NULL OR @marksandregno = '') THEN NULL ELSE @marksandregno END),
					(CASE WHEN(@ediid IS NULL OR @ediid = '') THEN NULL ELSE @ediid END));
		   
			SELECT CAST(1 AS bit) AS result ,@@IDENTITY AS [ID];


			SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdBy),
							   @origdata = null,
							   @newdata = 'RiskId:' + CONVERT(varchar(MAX),@VehicleID)
							   +';CompanyID:' + CONVERT(varchar(MAX),@CompanyID) 
							   +';PrintedPaperNumber:' + CONVERT(varchar(MAX),@PrintedPaperNo)
							   +';CertificateNo:' +@certNumber
							   +';PolicyID:' + CONVERT(varchar(MAX),@policyID)
							   +';Wording:' + CONVERT(varchar(MAX),@wordingTemplateID);
					             
							
			EXEC LogAudit 'insert', @createdBy , @details, 'VehicleCertificates',  @@IDENTITY, @origdata, @newdata, 'insert';
		END	
	END

END

GO
/****** Object:  StoredProcedure [dbo].[DeactivateEmployee]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeactivateEmployee]
	-- Add the parameters for the stored procedure here
	@empid int,
	@updateuid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	-- Insert statements for procedure here
	DECLARE @uactive bit; SET @uactive = 0;
	SELECT @uactive = Users.Active FROM Users WHERE EmployeeID = @empid
	IF @uactive = 0
	BEGIN
		DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES
		SELECT @origdata = 'Employee:' +  CONVERT(varchar(MAX),@empid) +  ';PersonID:' + CONVERT(varchar(MAX),People.PeopleID) + ';Person:' + People.FName + ' ' + People.MName + ' '  + People.LName  + ';CompanyID:' + CONVERT(varchar(MAX),Company.CompanyID) + ';CompanyName:' + Company.CompanyName
		FROM Employees	JOIN Company ON Employees.CompanyID = Company.CompanyID
						JOIN People ON Employees.PersonID = People.PeopleID	
		WHERE Employees.EmployeeID = @empid;

		UPDATE Employees SET Active = 0 WHERE EmployeeID = @empid;

		SELECT @details = 'The record ' + CONVERT(varchar(MAX),@empid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@updateuid),
			   @newdata = 'Employee:' +  CONVERT(varchar(MAX),@empid) +  ';PersonID:' + CONVERT(varchar(MAX),People.PeopleID) + ';Person:' + People.FName + ' ' + People.MName + ' '  + People.LName  + ';CompanyID:' + CONVERT(varchar(MAX),Company.CompanyID) + ';CompanyName:' + Company.CompanyName
		FROM Employees	JOIN Company ON Employees.CompanyID = Company.CompanyID
						JOIN People ON Employees.PersonID = People.PeopleID	
		WHERE Employees.EmployeeID = @empid;
		EXEC LogAudit 'update', @updateuid , @details, 'Employees', @empid, @origdata, @newdata, 'update';

		SELECT CAST(1 as bit) as result;
	END
	ELSE
		SELECT CAST(0 as bit) as result;

END


GO
/****** Object:  StoredProcedure [dbo].[DeactivateUser]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeactivateUser]
	-- Add the parameters for the stored procedure here
	@UID int,
	@editedBy int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES
	SELECT @origdata = 'username:' +  username +  ';active:' + CONVERT(varchar(MAX),active) FROM Users WHERE [UID] = @UID;
	UPDATE Users SET active = 0 , LastModifiedBy = @editedBy WHERE [UID] = @UID;
	SELECT CAST(1 as bit) as result;
	SELECT @details = 'The record ' + CONVERT(varchar(MAX),[UID]) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy),
		   @newdata = 'username:' +  username +  ';active:' + CONVERT(varchar(MAX),active)
	FROM Users WHERE [UID] = @UID;
		
	EXEC LogAudit 'update', @editedBy , @details, 'Users', @UID, @origdata, @newdata, 'update';
END


GO
/****** Object:  StoredProcedure [dbo].[DeleteAllUsagesToCovers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteAllUsagesToCovers]
	-- Add the parameters for the stored procedure here
	@covid int,
	@uid int
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES


	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'delete', @uid, CURRENT_TIMESTAMP, 'The record ' + CONVERT(varchar(MAX), ID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
	       'UsagesToCovers', ID, 'CoverID: ' +  CONVERT(varchar(MAX),CoverID) + ';UsageID: ' + CONVERT(varchar(MAX),UsageID) , null , 'delete'
		   FROM UsagesToCovers WHERE CoverID = @covid;

	
	DELETE UsagesToCovers WHERE CoverID = @covid;

END

GO
/****** Object:  StoredProcedure [dbo].[DeleteAllUserGroupsForCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteAllUserGroupsForCompany]
	@cid int,
	@userID int

AS
BEGIN
	
	SET NOCOUNT ON;


	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'delete', @userID, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),UGUAID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID),
		   'UserGroupUserAssoc', UGUAID, 'UGID: ' + CONVERT(varchar(MAX),UserGroupUserAssoc.UGID) +';UserGroup: ' + UserGroups.UserGroup+ ';UID:' + CONVERT(varchar(MAX),UserGroupUserAssoc.UID),
		    null, 'delete'
	FROM  UserGroupUserAssoc 
	      JOIN UserGroups  ON UserGroups.UGID = UserGroupUserAssoc.UGID
	WHERE UserGroupUserAssoc.UGID IN (SELECT UGID FROM UserGroups WHERE Company = @cid); 

	DELETE UserGroupUserAssoc WHERE UGID IN (SELECT UGID FROM UserGroups WHERE Company = @cid);



	
	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'delete', @userID, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),PGAID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID),
		   'PermissionGroupAssoc', PGAID, 'UGID: ' + CONVERT(varchar(MAX),PermissionGroupAssoc.UGID) +';UserGroup: ' + UserGroups.UserGroup+ ';UPID: ' + CONVERT(varchar(MAX),PermissionGroupAssoc.UPID) + 
		   ';User Permission: ' + UserPermissions.[Action] + ' ' + UserPermissions.[Table],
		    null, 'delete'
	FROM  PermissionGroupAssoc 
	      JOIN UserPermissions ON UserPermissions.UPID = PermissionGroupAssoc.UPID
		  JOIN UserGroups  ON UserGroups.UGID = PermissionGroupAssoc.UGID
    WHERE PermissionGroupAssoc.UGID IN (SELECT UGID FROM UserGroups WHERE Company = @cid);

	DELETE PermissionGroupAssoc WHERE UGID IN (SELECT UGID FROM UserGroups WHERE Company = @cid);




	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'delete', @userID, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),UGID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID),
		   'UserGroups', UGID, 'UGID:' + CONVERT(varchar(MAX),UGID) + +';UserGroup: ' +UserGroup,
		    null, 'delete'
	FROM  UserGroups  WHERE Company = @cid;

	DELETE UserGroups WHERE Company = @cid;


END

GO
/****** Object:  StoredProcedure [dbo].[DeleteCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteCompany]
	@CompanyID int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	IF EXISTS ((SELECT EmployeeID FROM Employees WHERE CompanyID = @CompanyID) UNION (SELECT CTID FROM InsurerToIntermediary WHERE Intermediary = @CompanyID) UNION (SELECT PolicyID FROM Policies WHERE CompanyID = @CompanyID))
	BEGIN
		SELECT CAST(0 as bit) [result], 
			  (CASE WHEN (SELECT COUNT(EmployeeID) FROM Employees WHERE CompanyID = @CompanyID) > 0 THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [emp],
			  (CASE WHEN (SELECT COUNT(CTID) FROM InsurerToIntermediary WHERE Intermediary = @CompanyID) > 0 THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [intermediary],
			  (CASE WHEN (SELECT COUNT(PolicyID) FROM Policies WHERE CompanyID = @CompanyID) > 0 THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [policy];
	END
	ELSE
	BEGIN
		UPDATE Company SET LastModifiedBy = @uid WHERE CompanyID = @CompanyID;
		SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
			   @newdata = null,
			   @origdata = 'IAJID:' + CONVERT(varchar(MAX),IAJID) + ';CompanyName:' + CompanyName + ';CompanyType:' + CompanyTypes.Type 
			   FROM Company JOIN CompanyTypes ON Company.Type = CompanyTypes.ID
			   WHERE Company.CompanyID = @CompanyID;

	
	--Delete the policy,and email and phone data-related to the company
		
     INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	 SELECT 'delete', @uid, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),PolicyInsured.PolicyInsuredID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid), 
		    'PolicyInsured', PolicyInsured.PolicyInsuredID, 'PolicyID:'+ CONVERT(varchar(MAX),PolicyID)+ ';EDIID:' + CONVERT(varchar(MAX),PolicyInsured.EDIID)+ 
		    (CASE WHEN (CompanyID IS NOT NULL) THEN ';Insured Type: Comapny ; CompanyID: ' + CONVERT(varchar(MAX),CompanyID) ELSE ';Insured Type: Individual ; PersonID: ' + CONVERT(varchar(MAX),PersonID) END)
		    ,null, 'delete' FROM PolicyInsured WHERE CompanyID = @CompanyID;
	

		DELETE FROM PolicyInsured WHERE CompanyID = @CompanyID; -- need to add logs for this (can use drop policy details)

		EXECUTE DropEmailsFromCompany @CompanyID, @uid;
		EXECUTE DropPhonesNumsFromComp @CompanyID, @uid;

		EXEC DeleteAllUserGroupsForCompany @CompanyID, @uid;

		DELETE Company WHERE CompanyID = @CompanyID;
		

		EXEC LogAudit 'delete', @uid , @details, 'Company', @CompanyID, @origdata, @newdata, 'delete';
		SELECT CAST(1 as bit) [result];
	END
	

END

GO
/****** Object:  StoredProcedure [dbo].[DeleteContactGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteContactGroup]
	-- Add the parameters for the stored procedure here
	@cgid int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	
	---DECIDE TO GO TO STRAIGHT INSERT SINCE IT MAY BE MULTIPLE RECORDS INSERTING
	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'delete', @uid, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),ContactUserAssoc.ID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid), 
		   'ContactUserAssoc', ContactUserAssoc.ID, 'Contact:' + CAST([UID] as varchar(MAX)) + ';ContactGroup:' + ContactGroups.name, null, 'delete'
	FROM ContactUserAssoc JOIN ContactGroups ON ContactUserAssoc.contactgroup = ContactGroups.ID WHERE ContactGroups.ID = @cgid;
	
	
	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@cgid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
		   @newdata = null,
		   @origdata = 'ContactGroup:' + name + ';Owner:' + Users.username
	FROM ContactGroups JOIN Users ON ContactGroups.Owner = Users.UID WHERE ID = @cgid;
	EXEC LogAudit 'delete', @uid , @details, 'ContactGroups', @cgid, @origdata, @newdata, 'delete';
	

	DELETE ContactUserAssoc WHERE contactgroup = @cgid;
	DELETE ContactGroups WHERE ID = @cgid;
END


GO
/****** Object:  StoredProcedure [dbo].[DeleteCover]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteCover]
	-- Add the parameters for the stored procedure here
	@cid int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES


	IF EXISTS (SELECT PolicyID FROM Policies WHERE PolicyCover = @cid)
	BEGIN
		SELECT CAST(0 as bit) [result];
	END
	ELSE 
	BEGIN

	    SELECT @origdata = 'Policy Cover: ' +  CONVERT(varchar(MAX),ID) + ';Deleted: '+  CONVERT(varchar(MAX),Deleted) FROM PolicyCovers WHERE ID = @cid;

		UPDATE PolicyCovers SET LastModifiedBy = @uid, Deleted = 1 WHERE ID = @cid;

		SELECT @newdata = 'Policy Cover: ' +  CONVERT(varchar(MAX),ID) + ';Deleted: '+  CONVERT(varchar(MAX),Deleted),
		       @details = 'The record ' + CONVERT(varchar(MAX), @cid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid)
			   FROM PolicyCovers WHERE ID = @cid;

		EXEC LogAudit 'delete', @uid , @details, 'PolicyCovers', @cid, @origdata, @newdata, 'delete';


		SELECT CAST(1 as bit) [result];
	END
END

GO
/****** Object:  StoredProcedure [dbo].[DeleteDriver]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 23/06/2014
-- Description:	Deleting a driver from the system
-- =============================================

CREATE PROCEDURE [dbo].[DeleteDriver]
	-- Add the parameters for the stored procedure here
	@DriverID int,
	@detetedby int


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @result bit;
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	IF NOT EXISTS (SELECT DriverID FROM Drivers WHERE DriverID = @DriverID)

	BEGIN
		SELECT @result = 0;
	END

	ELSE 
	BEGIN
		--DELETE FROM People WHERE PeopleId = (SELECT DriverID FROM Drivers WHERE DriverID = @DriverID);
		
		SELECT @details = 'The record ' + CONVERT(varchar(MAX),@DriverID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@detetedby),
		       @newdata = null,
			   @origdata = 'PersonFname:' + FName + ';PersonMName:' + MName + ';PersonLname:' + LName
		FROM People JOIN Drivers ON People.PeopleID = Drivers.PersonID
		WHERE Drivers.DriverID = @DriverID;

		EXEC LogAudit 'delete', @detetedby , @details, 'Drivers', @@IDENTITY, @origdata, @newdata, 'delete';
		
		
		DELETE FROM Drivers WHERE DriverID= @DriverID;

		SELECT @result = 1;
     END

END
GO
/****** Object:  StoredProcedure [dbo].[DeleteDriversFromVehicle]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 25/07/2014
-- Description:	Deleting a driver from the authorized, excepted, or excluded list (belonging to a particular vehicle)
-- =============================================
CREATE PROCEDURE [dbo].[DeleteDriversFromVehicle]
	-- Add the parameters for the stored procedure here
	@chassisNo varchar (50),
	@perId int,
	@driverType int, 
	@polId int,
	@editedBy int

AS
BEGIN
	
	SET NOCOUNT ON;

    DECLARE @result bit; DECLARE @VID int; DECLARE @DriverID int;

	SELECT @VID = VehicleID FROM Vehicles WHERE ChassisNo = @chassisNo;
	SELECT @DriverID = DriverID FROM Drivers WHERE PersonID = @perId;


	--//// If the driver is authorised)//// 
	IF (@driverType = 1)
	BEGIN
		IF EXISTS (SELECT VehicleID FROM AuthorizedDrivers WHERE VehicleID = @VID AND DriverID = @DriverID AND PolicyID = @polId)
			BEGIN
			    UPDATE AuthorizedDrivers SET LastModifiedBy = @editedBy WHERE VehicleID = @VID AND DriverID = @DriverID;

				INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
				SELECT 'delete', @editedBy, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),AuthorizedDrivers.EDID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy), 
			    'AuthorizedDrivers', CONVERT(varchar(MAX),AuthorizedDrivers.EDID), ';DriverID: ' + CONVERT(varchar(MAX),DriverID)+ ';VehicleID: '+ CONVERT(varchar(MAX),VehicleID) +';PolicyID: ' + CONVERT(varchar(MAX),@polId)
			    ,null, 'delete' FROM AuthorizedDrivers WHERE VehicleID = @VID AND DriverID = @DriverID AND PolicyID = @polId; 

				DELETE FROM AuthorizedDrivers WHERE VehicleID = @VID AND DriverID = @DriverID AND PolicyID = @polId;
			END	
      END
	 --//// If the driver is excepted)//// 

	   IF (@driverType = 2)
	   BEGIN
		IF EXISTS (SELECT VehicleID FROM ExceptedDrivers WHERE VehicleID = @VID AND DriverID = @DriverID AND PolicyID = @polId)
			BEGIN
			    UPDATE ExceptedDrivers SET LastModifiedBy = @editedBy WHERE VehicleID = @VID AND DriverID = @DriverID;

				INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
				SELECT 'delete', @editedBy, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),ExceptedDrivers.EDID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy), 
			    'ExceptDrivers', CONVERT(varchar(MAX),ExceptedDrivers.EDID), ';DriverID: ' + CONVERT(varchar(MAX),DriverID)+ ';VehicleID: '+ CONVERT(varchar(MAX),VehicleID) +'PolicyID: ' + CONVERT(varchar(MAX),@polId)
			    ,null, 'delete' FROM ExceptedDrivers WHERE VehicleID = @VID AND DriverID = @DriverID AND PolicyID = @polId; 

				DELETE FROM ExceptedDrivers WHERE VehicleID = @VID AND DriverID = @DriverID AND PolicyID = @polId;
			END
		END

	  --//// If the driver is excluded)//// 

	   IF (@driverType = 3)
	   BEGIN
		IF EXISTS (SELECT VehicleID FROM ExcludedDrivers WHERE VehicleID = @VID AND DriverID = @DriverID AND PolicyID = @polId)
			BEGIN
				UPDATE ExceptedDrivers SET LastModifiedBy = @editedBy WHERE VehicleID = @VID AND DriverID = @DriverID;

				INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
				SELECT 'delete', @editedBy, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),ExcludedDrivers.EDID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy), 
			   'ExcludedDrivers', CONVERT(varchar(MAX),ExcludedDrivers.EDID), ';DriverID: ' + CONVERT(varchar(MAX),DriverID)+ ';VehicleID: '+ CONVERT(varchar(MAX),VehicleID) +';PolicyID: ' + CONVERT(varchar(MAX),@polId)
			   ,null, 'delete' FROM ExcludedDrivers WHERE VehicleID = @VID AND DriverID = @DriverID AND PolicyID = @polId; 

				DELETE FROM ExcludedDrivers WHERE VehicleID = @VID AND DriverID = @DriverID AND PolicyID = @polId;
			END
        END
END -- final end
GO
/****** Object:  StoredProcedure [dbo].[DeleteEmployee]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteEmployee]
	-- Add the parameters for the stored procedure here
	@empid int,
	@deletedby int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (SELECT Active FROM Employees WHERE EmployeeID = @empid) = 1
	BEGIN
		SELECT CAST(0 as bit) as result;
	END
	ELSE
	BEGIN
		
		INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
		SELECT 
			'delete', 
			@deletedby, 
			CURRENT_TIMESTAMP,  
			'The record ' + CONVERT(varchar(MAX),@empid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@deletedby), 
			'Employees',
			@empid,
			'Employee:' +  CONVERT(varchar(MAX),@empid) +  ';PersonID:' + CONVERT(varchar(MAX),People.PeopleID) + ';Person:' + People.FName + ' ' + People.MName + ' '  + People.LName  + ';CompanyID:' + CONVERT(varchar(MAX),Company.CompanyID) + ';CompanyName:' + Company.CompanyName,
			null, 
			'delete'
		FROM Employees JOIN Company ON Employees.CompanyID = Company.CompanyID
					   JOIN People ON Employees.PersonID = People.PeopleID	
		WHERE Employees.EmployeeID = @empid;

		DELETE Employees WHERE EmployeeID = @empid;
		SELECT CAST(1 as bit) as result;
	END
END


GO
/****** Object:  StoredProcedure [dbo].[DeletePolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 24/06/2014
-- Description: Deleting a policy from the system
-- =============================================

CREATE PROCEDURE [dbo].[DeletePolicy]
	-- Add the parameters for the stored procedure here
	@PolicyID int,
	@editedBy int

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @result bit;
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	IF NOT EXISTS (SELECT PolicyID FROM Policies WHERE PolicyID = @PolicyID)

	BEGIN
		SELECT @result = 0;
	END

	ELSE
	BEGIN

	   SELECT @origdata = 'EDIID: ' + CONVERT(varchar(MAX),EDIID)+ ';PolicyID: ' + CONVERT(varchar(MAX),PolicyID) +';CompanyID: ' + CONVERT(varchar(MAX),CompanyID) + 
	                      ';StartDateTime: ' + CONVERT(varchar(MAX),StartDateTime) + ';EndDateTime: ' + CONVERT(varchar(MAX),EndDateTime)+ 
						  ';PolicyNumber: ' + CONVERT(varchar(MAX), Policyno) + ';PolicyCoverId: ' + CONVERT(varchar(MAX),PolicyCover) + ';InsuredTypeId: ' + CONVERT(varchar(MAX),InsuredType) 
						  + ';Deleted: ' + CONVERT(varchar(MAX),Deleted) 
						  FROM Policies WHERE PolicyID = @PolicyID;

		UPDATE Policies 
		SET Deleted = 1
		WHERE PolicyID = @PolicyID;
		SELECT CAST(1 as bit) as result;


		SELECT @details = 'The record ' + CONVERT(varchar(MAX),@PolicyID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy),
			   @newdata = 'EDIID: ' + CONVERT(varchar(MAX),EDIID)+ ';PolicyID: ' + CONVERT(varchar(MAX),PolicyID) +';CompanyID: ' + CONVERT(varchar(MAX),CompanyID) + 
	                      ';StartDateTime: ' + CONVERT(varchar(MAX),StartDateTime) + ';EndDateTime: ' + CONVERT(varchar(MAX),EndDateTime)+ 
						  ';PolicyNumber: ' + CONVERT(varchar(MAX), Policyno) + ';PolicyCoverId: ' + CONVERT(varchar(MAX),PolicyCover) + ';InsuredTypeId: ' + CONVERT(varchar(MAX),InsuredType) 
						  + ';Deleted: ' + CONVERT(varchar(MAX),Deleted) 
						  FROM Policies WHERE PolicyID = @PolicyID;
	
		EXEC LogAudit 'delete', @editedBy, @details, 'Policies',  @PolicyID, @origdata, @newdata, 'delete';
		


		--UPDATE VehiclesUnderPolicy SET LastModifiedBy = @editedBy WHERE PolicyID = @PolicyID;
			
		--	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
		--		SELECT 'delete', @editedBy, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),VehiclesUnderPolicy.VUPID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy), 
		--	   'VehiclesUnderPolicy', CONVERT(varchar(MAX),VehiclesUnderPolicy.VUPID), 'PolicyID:'+ CONVERT(varchar(MAX),@PolicyID) + 'VehicleID:'+ CONVERT(varchar(MAX),VehicleID)
		--	   ,null, 'delete' FROM VehiclesUnderPolicy WHERE PolicyID = @PolicyID; 

		--	DELETE FROM VehiclesUnderPolicy WHERE PolicyID = @PolicyId;  ---- ';PersonInsured:' + CONVERT(varchar(MAX),PolicyInsured.PersonID)+ ';CompanyInsured:' + CONVERT(varchar(MAX),PolicyInsured.CompanyID)


	 --   UPDATE PolicyInsured SET LastModifiedBy = @editedBy WHERE PolicyID = @PolicyID;
			
		--	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
		--		SELECT 'delete', @editedBy, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),PolicyInsured.PolicyInsuredID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy), 
		--	    'PolicyInsured', PolicyInsured.PolicyInsuredID, 'PolicyID:'+ CONVERT(varchar(MAX),@PolicyID)+ ';EDIID:' + CONVERT(varchar(MAX),PolicyInsured.EDIID)
		--		, null, 'delete' FROM PolicyInsured WHERE PolicyID = @PolicyID; 
	
		--	DELETE FROM PolicyInsured WHERE PolicyID = @PolicyID
		

		--UPDATE Policies SET LastModifiedBy = @editedBy WHERE PolicyID = @PolicyID;
		--	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy),
		--		   @newdata = null,
		--		   @origdata = 'EDIID:' + CONVERT(varchar(MAX),EDIID)+ ';PolicyID:' + CONVERT(varchar(MAX),PolicyID) +';CompanyID:' + CONVERT(varchar(MAX),CompanyID) + ';StartDateTime:' + CONVERT(varchar(MAX),StartDateTime) + ';EndDateTime:' + CONVERT(varchar(MAX),EndDateTime) + ';PolicyPrefix:' + CONVERT(varchar(MAX),PolicyPrefix) + ';PolicyNumber:' + CONVERT(varchar(MAX), Policyno) + ';PolicyCoverId:' + CONVERT(varchar(MAX),PolicyCover) + ';InsuredTypeId:' + CONVERT(varchar(MAX),InsuredType) FROM Policies WHERE PolicyID = @PolicyID;
		--	DELETE FROM Policies WHERE PolicyID = @PolicyID;
		--EXEC LogAudit 'delete', @editedBy, @details, 'Policies',  @@IDENTITY, @origdata, @newdata, 'delete';
		
		
		--SELECT @result =1;

	END

END




GO
/****** Object:  StoredProcedure [dbo].[DeleteTemplateImage]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteTemplateImage]
	-- Add the parameters for the stored procedure here
	@id int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE TemplateImages WHERE ID = @id;
END

GO
/****** Object:  StoredProcedure [dbo].[DeleteUser]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteUser]
	-- Add the parameters for the stored procedure here
	@uid int,
	@editedBy int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @result bit;
	
	IF EXISTS((SELECT UGUAID FROM UserGroupUserAssoc WHERE [UID] = @uid))
	BEGIN
		SET @result = 0;
		SELECT @result as result;
	END
	ELSE
	BEGIN	
		SET @result = 1;
		
		UPDATE Users SET LastModifiedBy = @editedBy, Deleted = 1 WHERE [UID] = @uid;
		UPDATE Employees SET Active = 0 WHERE EmployeeID = (SELECT EmployeeID FROM Users WHERE [UID] = @uid);
		
		INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
		SELECT 
			'delete', 
			@editedBy, 
			CURRENT_TIMESTAMP,  
			'The record ' + CONVERT(varchar(MAX),@uid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy), 
			'Users',
			@uid,
			'username:' +  CONVERT(varchar(MAX),username),
			null, 
			'delete'
		FROM Users
		WHERE [UID] = @uid;
		
		SELECT @result as result;
	END



END

GO
/****** Object:  StoredProcedure [dbo].[DeleteUserGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteUserGroup]
	-- Add the parameters for the stored procedure here
	@UGID int,
	@deletedby int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @result as bit;
    IF EXISTS (SELECT Users.[UID] FROM Users LEFT JOIN UserGroupUserAssoc ON Users.[UID] = UserGroupUserAssoc.[UID] WHERE UserGroupUserAssoc.UGID = @UGID)
	BEGIN
		SET @result = 0;
	END
	ELSE 
	BEGIN
		IF (SELECT SuperGroup FROM UserGroups WHERE UGID = @UGID) = 1
		BEGIN
			
			
			INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
			SELECT 'delete', @deletedby, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),SGID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@deletedby),
				   'SuperGroupMapping', SGID, 'PGID:' + CONVERT(varchar(MAX),@UGID) + ';CGID:' + CONVERT(varchar(MAX),CGID),
					null, 'delete'
			FROM SuperGroupMapping
			WHERE PGID = @UGID;
			DELETE SuperGroupMapping WHERE PGID = @UGID;
		END
		
		
		INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
		SELECT 'delete', @deletedby, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),PGAID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@deletedby),
				'PermissionGroupAssoc', PGAID, 'UPID:' + CONVERT(varchar(MAX),UPID) + ';UGID:' + CONVERT(varchar(MAX),UGID),
				null, 'delete'
		FROM PermissionGroupAssoc
		WHERE UGID = @UGID;
		DELETE PermissionGroupAssoc WHERE UGID = @UGID;

		
		INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
		SELECT 'delete', @deletedby, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),UGID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@deletedby),
				'UserGroups', UGID, 'UserGroupName:' + UserGroup + ';SuperGroup:' + CONVERT(varchar(MAX),SuperGroup),
				null, 'delete'
		FROM UserGroups
		WHERE UGID = @UGID;
		DELETE UserGroups WHERE UGID = @UGID;

		SET @result = 1;
	END
	SELECT @result as result;
END


GO
/****** Object:  StoredProcedure [dbo].[DeleteVehicle]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 23/06/2014
-- Description:	Deleting a vehicle from the system
-- =============================================

CREATE PROCEDURE [dbo].[DeleteVehicle]
	-- Add the parameters for the stored procedure here
	@vehicleID int,
	@editedBy int


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @result bit;
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	IF NOT EXISTS (SELECT VehicleID FROM Vehicles WHERE VehicleID = @vehicleID)

	BEGIN
		SELECT @result = 0;
	END

	ELSE 
	BEGIN
	--updating the last modified by, adding the user currently deleting the record
		
		UPDATE VehiclesUnderPolicy SET  LastModifiedBy = @editedBy WHERE VehicleID = @vehicleID;
		
		INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
				SELECT 'delete', @editedBy, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),VehiclesUnderPolicy.VUPID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy), 
			   'VehiclesUnderPolicy', CONVERT(varchar(MAX),VehiclesUnderPolicy.VUPID), 'PolicyID:'+ CONVERT(varchar(MAX),VehiclesUnderPolicy.PolicyID) + 'VehicleID:'+ CONVERT(varchar(MAX),VehicleID)
			   ,null, 'delete' FROM VehiclesUnderPolicy WHERE VehicleID = @vehicleID; 

	    DELETE FROM VehiclesUnderPolicy WHERE VehicleID = @vehicleId;



		UPDATE AuthorizedDrivers SET  LastModifiedBy = @editedBy WHERE VehicleID = @vehicleID;

		INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
				SELECT 'delete', @editedBy, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),AuthorizedDrivers.EDID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy), 
			   'AuthorizedDrivers', CONVERT(varchar(MAX),AuthorizedDrivers.EDID), 'DriverID:' + CONVERT(varchar(MAX),DriverID)+ 'VehicleID:'+ CONVERT(varchar(MAX),VehicleID)
			   ,null, 'delete' FROM AuthorizedDrivers WHERE VehicleID = @vehicleID; 

		DELETE FROM AuthorizedDrivers WHERE VehicleID = @vehicleID;



		UPDATE ExceptedDrivers SET  LastModifiedBy = @editedBy WHERE VehicleID = @vehicleID;
		
		INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
				SELECT 'delete', @editedBy, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),ExceptedDrivers.EDID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy), 
			   'ExceptDrivers', CONVERT(varchar(MAX),ExceptedDrivers.EDID), 'DriverID:' + CONVERT(varchar(MAX),DriverID)+ 'VehicleID:'+ CONVERT(varchar(MAX),VehicleID)
			   ,null, 'delete' FROM ExceptedDrivers WHERE VehicleID = @vehicleID; 

		DELETE FROM ExceptedDrivers WHERE VehicleID = @vehicleID;
		


		UPDATE ExcludedDrivers SET  LastModifiedBy = @editedBy WHERE VehicleID = @vehicleID;
		
		INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
				SELECT 'delete', @editedBy, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),ExcludedDrivers.EDID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy), 
			   'ExcludedDrivers', CONVERT(varchar(MAX),ExcludedDrivers.EDID), 'DriverID:' + CONVERT(varchar(MAX),DriverID)+ 'VehicleID:'+ CONVERT(varchar(MAX),VehicleID)
			   ,null, 'delete' FROM ExcludedDrivers WHERE VehicleID = @vehicleID; 

		DELETE FROM ExcludedDrivers WHERE VehicleID = @vehicleID;



		UPDATE Vehicles SET  LastModifiedBy = @editedBy WHERE VehicleID = @vehicleID;
		SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy),
			   @newdata = null,
			   @origdata = 'Make:' + Make+ ';Model:' + Model+ ';ModelType:' + ModelType + ';BodyType:' + BodyType+';Extension:' + Extension+';RegistrationNumber:' + VehicleRegistrationNo +';VIN:' + VIN+';HandDriveId:' + CONVERT(varchar(MAX),HandDrive)+
					       ';Colour:' + Colour +';Cylinders:' + CONVERT(varchar(MAX),Cylinders)+ ';EngineNo:' + EngineNo + ';EngineModified:' + CONVERT(varchar(MAX),EngineModified) + ';EngineType:' + EngineType + ';EstimatedAnnualMileage:' +  CONVERT(varchar(MAX),EstimatedAnnualMileage) + 
						   ';ExpirydateOfFitness:' + CONVERT(varchar(MAX),ExpiryDateofFitness)+ ';HPCCUnitType:'+ HPCCUnitType +';MainDriverID:' + CONVERT(varchar(MAX),MainDriver) +';ReferenceNo:' + CONVERT(varchar(MAX),ReferenceNo)+';RegistrationLocation:' + RegistrationLocation+';RoofType:' + RoofType+ 
					       ';Seating:' +  CONVERT(varchar(MAX),Seating)+ ';SeatingCert:' + CONVERT(varchar(MAX),SeatingCert)+ ';Tonnage:' + CONVERT(varchar(MAX),Tonnage)+ ';TransmissionTypeId:' + CONVERT(varchar(MAX),TransmissionType)+ ';VehicleYear:' + CONVERT(varchar(MAX),VehicleYear) FROM Vehicles WHERE VehicleID = @VehicleID;
		DELETE FROM Vehicles WHERE VehicleID = @vehicleID;
		EXEC LogAudit 'delete', @editedBy, @details, 'Vehicles',  @@IDENTITY, @origdata, @newdata, 'delete';

		SELECT @result = 1;
    END 
END



GO
/****** Object:  StoredProcedure [dbo].[DeleteVehicleCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 14/08/2014
-- Description:	Deleting a Vehicle Certificate
-- =============================================
CREATE PROCEDURE [dbo].[DeleteVehicleCertificate]
	@VehicleCertificateID int,
	@userId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	
	
	SELECT @origdata = 'CertificateNo:' + VehicleCertificates.CertificateType+ VehicleCertificates.InsuredCode+ CONVERT(varchar(MAX), VehicleCertificates.ExtensionCode)+CONVERT(varchar(MAX),VehicleCertificates.UniqueNum)
	                   +';RiskId:' + CONVERT(varchar(MAX), RiskItemNo)+ ';EffectiveDate:' + CONVERT(varchar(MAX), EffectiveDate)
					   +';ExpiryDate:' + CONVERT(varchar(MAX),ExpiryDate) +';EndorsementNo:' + CONVERT(varchar(MAX),EndorsementNo)
					   +';PrintedPaperNumber:' + CONVERT(varchar(MAX),PrintedPaperNo)+';MarksRegNo:' + CONVERT(varchar(MAX),MarksRegNo) +';Usage:' + Usage+';Scheme:' + Scheme + ';State:' + VehiclecoverNoteState.state
					   FROM VehicleCertificates 
							RIGHT JOIN VehicleCoverNoteState ON VehiclecoverNoteState.VCNSID = VehicleCertificates.state
					    WHERE VehicleCertificateID = @VehicleCertificateId;


   DELETE FROM VehicleCertificates  WHERE VehicleCertificates.VehicleCertificateID = @VehicleCertificateID; 

   SELECT @details = 'The record ' + CONVERT(varchar(MAX),@VehicleCertificateId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userId),
		  @newdata = null;
					             
							
    EXEC LogAudit 'delete', @userId , @details, 'VehicleCertificates',  @@IDENTITY, @origdata, @newdata, 'delete';

END

GO
/****** Object:  StoredProcedure [dbo].[DeleteWording]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteWording]
	-- Add the parameters for the stored procedure here
	@ID int,
	@userID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	IF EXISTS(SELECT * FROM VehicleCoverNote vcn, VehicleCertificates vc WHERE vcn.WordingTemplate = @ID OR vc.WordingTemplate = @ID)	
		SELECT CAST(0 as bit) result;
	ELSE
	BEGIN

	   INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
		SELECT 
			'delete', 
			@userID, 
			CURRENT_TIMESTAMP,  
			'The record ' + CONVERT(varchar(MAX),@ID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID), 
			'Wording',
			@ID,
			'WordingID: ' +  CONVERT(varchar(MAX),@ID),
			null, 
			'delete'
		FROM Wording WHERE ID = @ID;

		DELETE Wording WHERE ID = @ID;

		SELECT CAST(1 as bit) result;
	END
END

GO
/****** Object:  StoredProcedure [dbo].[DisableCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DisableCompany]
	-- Add the parameters for the stored procedure here
	@id int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	
	INSERT INTO SystemAuditLog ([Action], ActionUserID, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'disable', @uid, 'Company', @id, (CASE WHEN [Enabled] = 1 THEN 'true' ELSE 'false' END), 'false', 'update'
	FROM Company WHERE CompanyID = @id;

	UPDATE Company 
	SET [Enabled] = 0
	WHERE CompanyID = @id;


END


GO
/****** Object:  StoredProcedure [dbo].[DisableIntermediary]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DisableIntermediary]
	-- Add the parameters for the stored procedure here
	@id int, 
	@userID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
 
    SELECT @origdata = 'InsuranceCompId: ' + CONVERT(varchar(MAX), Insurer)+ 
					   ';IntermediaryCompId: ' + CONVERT(varchar(MAX), Intermediary) +
					   ';Enabled: ' + CONVERT(varchar(MAX), [enabled])	   
		FROM InsurerToIntermediary WHERE CTID = @id;


	UPDATE InsurerToIntermediary SET [enabled] = 0 WHERE CTID = @id;


	SELECT @details = ('The record ' + CONVERT(varchar(MAX),@id) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID)),
			   @newdata = 'InsuranceCompId: ' + CONVERT(varchar(MAX), Insurer)+ 
					   ';IntermediaryCompId: ' + CONVERT(varchar(MAX), Intermediary) +
					   ';Enabled: ' + CONVERT(varchar(MAX), [enabled])	  
		FROM InsurerToIntermediary WHERE CTID = @id;

		EXEC LogAudit 'update', @userID , @details, 'InsurerToIntermediary',  @id, @origdata, @newdata, 'update';
END

GO
/****** Object:  StoredProcedure [dbo].[DisassociateUserGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DisassociateUserGroup]
	-- Add the parameters for the stored procedure here
	@UGID int,
	@UID int,
	@editedBy int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'delete',
			@editedBy,
			CURRENT_TIMESTAMP,
			'The record ' + CONVERT(varchar(MAX), UGUAID) + ' has been removed from the system by ' + CONVERT(varchar(MAX), @editedBy),
			'UserGroupUserAssoc',
			UGUAID,
			'UGID:' + CONVERT(varchar(MAX), @UGID) + ';UID:' + CONVERT(varchar(MAX), @UID),
			null,
			'delete'
	FROM UserGroupUserAssoc WHERE UGID = @UGID AND  [UID] = @UID;;
	
	DELETE UserGroupUserAssoc WHERE UGID = @UGID AND  [UID] = @UID;

END


GO
/****** Object:  StoredProcedure [dbo].[DoesCompanyHaveData]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DoesCompanyHaveData]
	-- Add the parameters for the stored procedure here
	@cid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @result bit;
	SET @result = 0;

	SELECT @result = (CASE WHEN (Count(*) > 0 OR @result = 1) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) FROM Policies WHERE CompanyCreatedBy = @cid OR CompanyID = @cid;
	SELECT @result = (CASE WHEN (Count(*) > 0 OR @result = 1) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) FROM InsurerToIntermediary WHERE Insurer = @cid;
	SELECT @result = (CASE WHEN (Count(*) > 0 OR @result = 1) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) FROM Employees WHERE CompanyID = @cid;

	SELECT @result [result];

END

GO
/****** Object:  StoredProcedure [dbo].[DoesPolicyHaveRisks]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parhment
-- Create date: 26/09/2014
-- Description:	Checking if a policy has uncancelled associations with and vehicles
-- =============================================
CREATE PROCEDURE [dbo].[DoesPolicyHaveRisks]
	
	@PolicyId int
AS
BEGIN
	
	SET NOCOUNT ON;
   
	IF NOT EXISTS (SELECT VehicleID FROM VehiclesUnderPolicy WHERE PolicyID = @policyId AND Cancelled = 0)
	SELECT CAST(0 as bit) as result;

	ELSE
	SELECT CAST(1 as bit) as result;

END


GO
/****** Object:  StoredProcedure [dbo].[DoesPolicyNoExist]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date:  13/03/2015
-- Description:	Testing if a particular policy number already exists
-- =============================================
CREATE PROCEDURE [dbo].[DoesPolicyNoExist]
	
	@policyNo varchar (250),
	@polId int = null
AS
BEGIN

	SET NOCOUNT ON;
   
	DECLARE @result bit;
	SET @result = 0;

	IF (@polId IS NULL)
	BEGIN
	    SELECT @result = (CASE WHEN (Count(*) > 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) FROM Policies WHERE Policyno = @policyNo;
	END

	ELSE
	BEGIN
	     SELECT @result = (CASE WHEN (Count(*) > 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) FROM Policies WHERE Policyno = @policyNo AND PolicyID!= @polId;
	END


	SELECT @result [result];

END

GO
/****** Object:  StoredProcedure [dbo].[DropEmailsFromCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 10/09/2014
-- Description:	Dropping all emails associated with a company
-- =============================================
CREATE PROCEDURE [dbo].[DropEmailsFromCompany]
	
	@CompanyID int,
	@editedBy int
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @peid int; -- LOG AUDIT VARIABLES

    -- Insert statements for procedure here
	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'delete', @editedBy, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),CompanyToEmails.CTEID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy),
		   'CompanyToEmails', CompanyToEmails.CTEID, 'CompanyID:' + CONVERT(varchar(MAX),@CompanyID) + ';Company:' + Company.CompanyName + ' ' + Company.TRN + ';EmailID:' + CONVERT(varchar(MAX),Emails.EID) + ';Email:' + Emails.email,
		    null, 'delete'
	FROM Company JOIN CompanyToEmails ON Company.CompanyID = CompanyToEmails.CompanyID
				 JOIN Emails ON CompanyToEmails.EmailID = Emails.EID
	WHERE CompanyToEmails.CompanyID = @CompanyID;
	

	DELETE CompanyToEmails WHERE CompanyID = @CompanyID;
END


GO
/****** Object:  StoredProcedure [dbo].[DropEmailsFromPerson]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DropEmailsFromPerson] 
	-- Add the parameters for the stored procedure here
	@PersonID int,
	@editedBy int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @peid int; -- LOG AUDIT VARIABLES

    -- Insert statements for procedure here
	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'delete', @editedBy, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),PeopleToEmails.PTEID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy),
		   'PeopleToEmails', PeopleToEmails.PTEID, 'PersonID:' + CONVERT(varchar(MAX),@personID) + ';Person:' + People.FName + ' ' + People.MName + ' ' + People.LName + ';EmailID:' + CONVERT(varchar(MAX),Emails.EID) + ';Email:' + Emails.email,
		    null, 'delete'
	FROM People JOIN PeopleToEmails ON People.PeopleID = PeopleToEmails.PeopleID
				JOIN Emails ON PeopleToEmails.EmailID = Emails.EID
	WHERE PeopleToEmails.PeopleID = @PersonID;
	
	DELETE PeopleToEmails WHERE PeopleID = @PersonID;
END

GO
/****** Object:  StoredProcedure [dbo].[DropPhonesNumsFromComp]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 10/09/2014
-- Description:	Dropping all phone numbers associated with a company
-- =============================================
CREATE PROCEDURE [dbo].[DropPhonesNumsFromComp]
	
	@CompanyID int,
	@editedBy int
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @peid int; -- LOG AUDIT VARIABLES

    
	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)

	SELECT 'delete', @editedBy, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),CompanyToPhones.CTPID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy),
		   'CompanyToPhones', CompanyToPhones.CTPID, 'CompanyID:' + CONVERT(varchar(MAX),@CompanyID) + ';Company:' + Company.CompanyName + ' ' + Company.TRN + ';PhoneID:' + 
		    CONVERT(varchar(MAX),PhoneNumbers.PID) + ';PhoneNumber:' +  CONVERT(varchar(MAX),PhoneNumbers.PhoneNo), null, 'delete'
	FROM Company JOIN CompanyToPhones ON Company.CompanyID = CompanyToPhones.CompanyID
                 JOIN PhoneNumbers ON CompanyToPhones.PhoneID = PhoneNumbers.PID
	WHERE CompanyToPhones.CompanyID = @CompanyID;
	

	DELETE CompanyToPhones WHERE CompanyID = @CompanyID;
END


GO
/****** Object:  StoredProcedure [dbo].[DropPolicyDetails]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  Chrystal Parchment
-- Create date: 22/07/2014
-- Description:	Clears away all the insured (persons and companies), and vehicles associated with a policy
-- =============================================
CREATE PROCEDURE [dbo].[DropPolicyDetails] 
	-- Add the parameters for the stored procedure here
	@PolicyID int,
	@editedBy int

AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @policyInsuredId varchar(MAX);    

	--deleting insured poeple/companies under a policy

	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'delete', @editedBy, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),PolicyInsured.PolicyInsuredID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy), 
		   'PolicyInsured', PolicyInsured.PolicyInsuredID, 'PolicyID:'+ CONVERT(varchar(MAX),@PolicyID)+ ';EDIID:' + CONVERT(varchar(MAX),PolicyInsured.EDIID)+ 
		   (CASE WHEN (CompanyID IS NOT NULL) THEN ';Insured Type: Comapny ; CompanyID: ' + CONVERT(varchar(MAX),CompanyID) ELSE ';Insured Type: Individual ; PersonID: ' + CONVERT(varchar(MAX),PersonID) END)
		   ,null, 'delete' FROM PolicyInsured WHERE PolicyID = @PolicyID; 
	
	DELETE FROM PolicyInsured WHERE PolicyID = @PolicyID;



	--deleting insured vehicles under a policy

	UPDATE VehiclesUnderPolicy SET LastModifiedBy = @editedBy WHERE PolicyID = @PolicyID;

	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy),
			   @newdata = null,
			   @origdata = 'PolicyID:' + CONVERT(varchar(MAX),PolicyID) FROM VehiclesUnderPolicy WHERE PolicyID = @PolicyID;

	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'delete', @editedBy, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),VehiclesUnderPolicy.VUPID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy), 
		   'VehiclesUnderPolicy', CONVERT(varchar(MAX),VehiclesUnderPolicy.VUPID), 'PolicyID:'+ CONVERT(varchar(MAX),@PolicyID) + 'VehicleID:'+ CONVERT(varchar(MAX),VehicleID)
		   ,null, 'delete' FROM VehiclesUnderPolicy WHERE PolicyID = @PolicyID; 
	
	DELETE FROM VehiclesUnderPolicy WHERE PolicyID = @PolicyID;

	
END

GO
/****** Object:  StoredProcedure [dbo].[EditGroupName]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 01/12/2014
-- Description:	Editing a user group's name
-- =============================================
CREATE PROCEDURE [dbo].[EditGroupName]

    @userGroupID int,
	@GroupName varchar(250),
	@UID int


AS
BEGIN
	
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @result bit; DECLARE @compID int;

	SELECT @compID = CompanyID FROM Employees LEFT JOIN Users ON Employees.EmployeeID = Users.EmployeeID WHERE Users.[UID] = @UID;

	IF EXISTS(SELECT UGID FROM UserGroups WHERE UserGroup = @GroupName AND Company= @compID)
	   BEGIN
	     SET @result = 0;
	   END

	ELSE
	    BEGIN
		  UPDATE UserGroups SET UserGroup= @GroupName, LastModifiedBy = @UID WHERE UGID= @userGroupID;

			INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
			SELECT 'insert',
				   @UID,
				   CURRENT_TIMESTAMP,
				   'The record ' + CONVERT(varchar(MAX), @userGroupID) + ' has been added from the system by ' + CONVERT(varchar(MAX), @UID),
				   'UserGroups',
				   @userGroupID,
				   null,
				   'GroupName:' + CONVERT(varchar(MAX), @GroupName),
				   'insert';

		   SET @result = 1;
	  END

	SELECT @result AS result;
END


GO
/****** Object:  StoredProcedure [dbo].[EnableCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EnableCompany]
	-- Add the parameters for the stored procedure here
	@id int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO SystemAuditLog ([Action], ActionUserID, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'disable', @uid, 'Company', @id, (CASE WHEN [Enabled] = 1 THEN 'true' ELSE 'false' END), 'true', 'update'
	FROM Company WHERE CompanyID = @id;


	UPDATE Company 
	SET [Enabled] = 1
	WHERE CompanyID = @id;


END


GO
/****** Object:  StoredProcedure [dbo].[EnableIntermediary]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EnableIntermediary]
	-- Add the parameters for the stored procedure here
	@id int,
	@userID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	 DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
 
     SELECT @origdata = 'InsuranceCompId: ' + CONVERT(varchar(MAX), Insurer)+ 
					   ';IntermediaryCompId: ' + CONVERT(varchar(MAX), Intermediary) +
					   ';Enabled: ' + CONVERT(varchar(MAX), [enabled])	   
		    FROM InsurerToIntermediary WHERE CTID = @id;


	UPDATE InsurerToIntermediary SET [enabled] = 1 WHERE CTID = @id;


	SELECT @details = ('The record ' + CONVERT(varchar(MAX),@id) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID)),
		   @newdata = 'InsuranceCompId: ' + CONVERT(varchar(MAX), Insurer)+ 
					   ';IntermediaryCompId: ' + CONVERT(varchar(MAX), Intermediary) +
					   ';Enabled: ' + CONVERT(varchar(MAX), [enabled])	  
		  FROM InsurerToIntermediary WHERE CTID = @id;

		  EXEC LogAudit 'update', @userID , @details, 'InsurerToIntermediary',  @id, @origdata, @newdata, 'update';

END

GO
/****** Object:  StoredProcedure [dbo].[EnquiryByInsurer]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EnquiryByInsurer]
	-- Add the parameters for the stored procedure here
	@start date,
	@end date,
	@ins int = 0,
	@month bit = false,
	@day bit = false
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	IF (@ins = 0)
	BEGIN
		SELECT	(CASE WHEN (p.CompanyID IS NULL) THEN 0 ELSE p.CompanyID END) [cid], 
				(CASE WHEN (c.CompanyName IS NULL) THEN 'N/A' ELSE c.CompanyName END) [cname], 
				COUNT(enq.ID) [count],
				'Company Name' [fieldname],
				'count' [yaxis]
		FROM Enquiries enq LEFT OUTER JOIN Policies p ON enq.policy = p.PolicyID
						   LEFT JOIN Company c ON p.CompanyID = c.CompanyID
		WHERE RequestedOn <= @end AND RequestedOn >= @start AND (p.CompanyID = @ins OR @ins = 0)
		GROUP BY p.CompanyID, c.CompanyName;
	END
	ELSE 
	BEGIN
		--if not set by user
		IF (@month = @day)
		BEGIN
			IF (DATEDIFF(DAY, @start, @end) > 30)
			BEGIN
				SET @month = 1;
				SET @day = 0;
			END
			ELSE 
			BEGIN
				SET @month = 0;
				SET @day = 1;
			END
		END

		IF (@month = 1)
		BEGIN
			-- per month
			SELECT	DATENAME(Month, RequestedOn) [month], 
					DATENAME(YEAR, RequestedOn) [year],
					COUNT(enq.ID) [count],
					'month' [fieldname],
					'count' [yaxis]
			FROM Enquiries enq LEFT OUTER JOIN Policies p ON enq.policy = p.PolicyID
							   LEFT JOIN Company c ON p.CompanyID = c.CompanyID
			WHERE RequestedOn <= @end AND RequestedOn >= @start AND p.CompanyID = @ins
			GROUP BY DATENAME(Month, RequestedOn), DATENAME(YEAR, RequestedOn);
		END

		IF (@day = 1)
		BEGIN
			-- per day
			SELECT	(CASE WHEN (p.CompanyID IS NULL) THEN 0 ELSE p.CompanyID END) [cid], 
					(CASE WHEN (c.CompanyName IS NULL) THEN 'N/A' ELSE c.CompanyName END) [cname], 
					COUNT(enq.ID) [count],
					'Company Name' [fieldname],
					'count' [yaxis]
			FROM Enquiries enq LEFT OUTER JOIN Policies p ON enq.policy = p.PolicyID
							   LEFT JOIN Company c ON p.CompanyID = c.CompanyID
			WHERE RequestedOn <= @end AND RequestedOn >= @start AND (p.CompanyID = @ins OR @ins = 0)
			GROUP BY p.CompanyID, c.CompanyName;
		END

	END
	--SELECT DATENAME(Month, RequestedOn) FROM Enquiries;
END

GO
/****** Object:  StoredProcedure [dbo].[EnquiryCount]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EnquiryCount]
	-- Add the parameters for the stored procedure here
	@cid int,
	@month varchar(50),
	@year varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	COUNT(enq.ID) [count]
	FROM Enquiries enq LEFT OUTER JOIN Policies p ON enq.policy = p.PolicyID
					   LEFT JOIN Company c ON p.CompanyID = c.CompanyID
	WHERE LOWER(DATENAME(YEAR,RequestedOn)) = LOWER(@year) AND LOWER(DATENAME(MONTH, RequestedOn)) = LOWER(@month) AND (p.CompanyID = @cid OR c.iaj = 1)
	
END

GO
/****** Object:  StoredProcedure [dbo].[ErrorCount]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ErrorCount]
	-- Add the parameters for the stored procedure here
	@cid int,
	@month varchar(50),
	@year varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--DECLARE @cid int, @month varchar(100), @year varchar(10);
	--SET @cid = 9; SET @month = 'march'; SET @year = '2015';
	SELECT COUNT(*) [count]
	FROM error e	LEFT JOIN JsonAudit ja ON e.reqid = ja.ID
					LEFT JOIN Users u ON ja.CreatedBy = u.[UID]
					LEFT JOIN Employees emp ON u.EmployeeID = emp.EmployeeID
					LEFT JOIN Company c ON emp.CompanyID = c.CompanyID
	WHERE	(c.CompanyID = @cid OR c.iaj = 1) AND 
			LOWER(DATENAME(MONTH, ja.CreatedTimeStamp)) = LOWER(@month) AND 
			LOWER(DATENAME(YEAR, ja.CreatedTimeStamp)) = LOWER(@year);
END

GO
/****** Object:  StoredProcedure [dbo].[FaultyLimit]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[FaultyLimit]
	-- Add the parameters for the stored procedure here
	@username varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @UserFaultyLimit int;
	DECLARE @CompanyFaultyLimit int;
	DECLARE @True bit, @False bit;
	SELECT @True = 1, @False = 0;

	SELECT @UserFaultyLimit = faultylogins FROM Users WHERE username = @username;
	SELECT @CompanyFaultyLimit = Company.faultyloginlimit FROM Company LEFT JOIN Employees ON Company.CompanyID = Employees.CompanyID
																	   LEFT JOIN Users ON Employees.EmployeeID = Users.EmployeeID
														  WHERE Users.username = @username;
  	
	SELECT CASE WHEN (@CompanyFaultyLimit <= @UserFaultyLimit AND @CompanyFaultyLimit <> 0) THEN @False ELSE @True END as result;

END

GO
/****** Object:  StoredProcedure [dbo].[FaultyLogin]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[FaultyLogin]
	-- Add the parameters for the stored procedure here
	@username varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @userId int;

	SELECT @userId = [UID] FROM Users WHERE username = @username;

    SELECT @origdata = 'Faulty Login: ' + CONVERT(varchar(MAX), faultylogins) FROM Users WHERE username = @username;

	DECLARE @climit int;
	SELECT @climit = c.faultyloginlimit
	FROM Company c	LEFT JOIN Employees e ON c.CompanyID = e.CompanyID
				   	LEFT JOIN Users u ON e.EmployeeID = u.EmployeeID
	WHERE u.username = @username;	

	UPDATE Users 
	SET faultylogins = (faultylogins + 1),
		active = (CASE
					WHEN (active = 0) THEN active
					WHEN (@climit = 0) THEN active
					WHEN (faultylogins + 1 >= @climit) THEN 0
					ELSE active
				  END )
	WHERE username = @username;


	
	SELECT @details = ('The record ' + CONVERT(varchar(MAX),@userId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userId)),
		   @newdata = 'Faulty Login: ' + CONVERT(varchar(MAX), faultylogins)  FROM Users WHERE username = @username;

		  EXEC LogAudit 'update', @userId , @details, 'Users', @userId , @origdata, @newdata, 'update';

END

GO
/****** Object:  StoredProcedure [dbo].[FirstLoginUpdateData]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[FirstLoginUpdateData]
	-- Add the parameters for the stored procedure here
	@username varchar(250),
	@password varchar(250),
	@question varchar(250),
	@response varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES
	SELECT @origdata = 'username: ' +  username +  ';question: ' + CONVERT(varchar(MAX),question)
	FROM Users WHERE username = @username;


	UPDATE Users SET password = @password,
					 question = @question,
					 response = @response,
					 lastloginattempt = CURRENT_TIMESTAMP,
					 faultylogins = 0,
					 firstlogin = 0
			    WHERE username = @username;

	SELECT @id = [UID],
		   @details = 'The record ' + CONVERT(varchar(MAX),[UID]) + ' was modified from the database by ' + CONVERT(varchar(MAX),[UID]),
		   @newdata = 'username: ' +  username +  ';question: ' + CONVERT(varchar(MAX),question) + ';response: ' + CONVERT(varchar(MAX),response)
	FROM Users WHERE username = @username;
		
	EXEC LogAudit 'update', @id , @details, 'Users', @id, @origdata, @newdata, 'update';
				
END

GO
/****** Object:  StoredProcedure [dbo].[ForceReset]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ForceReset]
	-- Add the parameters for the stored procedure here
	@name varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT password_reset_force [reset] FROM Users WHERE username = @name;
END

GO
/****** Object:  StoredProcedure [dbo].[GeneralSearch]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GeneralSearch]
	-- Add the parameters for the stored procedure here
	@search varchar(MAX),
	@type varchar(MAX) = null,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @cid int;
	SELECT @cid = c.CompanyID FROM Company c RIGHT JOIN Employees e ON c.CompanyID = e.CompanyID
											 RIGHT JOIN Users u ON e.EmployeeID = u.EmployeeID
						      WHERE u.UID = @uid;

	SET @search = '%' + @search + '%';

	IF @type = 'vehicle'
	BEGIN
		SELECT TOP 14 v.VehicleID [ID], 'vehicle' [table], v.VehicleRegistrationNo [name]
			FROM Vehicles v LEFT JOIN VehicleHandDriveLookUp hd ON v.HandDrive = hd.VHDLID
							LEFT JOIN People p ON v.MainDriver = p.PeopleID
							LEFT JOIN TransmissionTypeLookUp trans ON v.TransmissionType = trans.TransmisionTypeID
			WHERE (Make LIKE @search 
 				OR Model LIKE @search 
 				OR ModelType LIKE @search 
				OR BodyType LIKE @search 
				OR Extension LIKE @search 
				OR VehicleRegistrationNo LIKE @search
				OR VIN LIKE @search
				OR hd.HDType LIKE @search
				OR Colour LIKE @search
				OR CAST(Cylinders as varchar(MAX)) LIKE @search
				OR EngineNo LIKE @search
				OR EngineModified LIKE @search
				OR EngineType LIKE @search
				OR CAST(EstimatedAnnualMileage as varchar(MAX)) LIKE @search
				OR HPCCUnitType LIKE @search
				OR p.FName LIKE @search
				OR p.MName LIKE @search
				OR p.LName LIKE @search
				OR p.TRN LIKE @search
				OR CAST(ReferenceNo as varchar(MAX)) LIKE @search
				OR RegistrationLocation LIKE @search
				OR RoofType LIKE @search
				OR CAST(Seating as varchar(MAX)) LIKE @search
				OR CAST(Tonnage as varchar(MAX)) LIKE @search
				OR trans.TransmissionType LIKE @search
				OR CAST(VehicleYear as varchar(MAX)) LIKE @search)
				-- SECURITY CHECK
			   AND ((SELECT COUNT(*)
					FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
											RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
											LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
											LEFT JOIN Users u ON ass.UID = u.UID
					WHERE up.[Table] = 'Vehicles' AND up.[Action] = 'Read' AND u.UID = @uid) > 0)
	END
	
	IF @type = 'company'
	BEGIN
		SELECT TOP 14 CompanyID [ID], 'company' [table], CompanyName [name]  
		FROM Company
		WHERE 
			  (CompanyName LIKE @search
		   OR  CAST(IAJID as varchar(MAX)) LIKE @search
		   OR  TRN LIKE @search)
		  AND ((SELECT COUNT(*)
				FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
										RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
										LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
										LEFT JOIN Users u ON ass.UID = u.UID
				WHERE ((up.[Table] = 'Company' AND up.[Action] = 'Read') OR ug.UserGroup IN ('System Administrator')) AND u.UID = @uid) > 0)
	END

	IF @type = 'user'
	BEGIN
		SELECT TOP 14 u.UID [ID], 'user' [table], u.username [name]
		FROM Users u LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID
					 LEFT JOIN People p ON e.PersonID = p.PeopleID
					 LEFT JOIN Company c ON e.CompanyID = c.CompanyID
					 --LEFT JOIN Employees myemp ON c.CompanyID = myemp.CompanyID
					 --LEFT JOIN Users me ON myemp.EmployeeID = me.EmployeeID
		WHERE (u.Filter = 0 AND u.Deleted = 0 AND u.UID != @uid) 
		   AND (u.username LIKE @search 
		     OR p.FName LIKE @search 
			 OR p.MName LIKE @search 
			 OR p.LName LIKE @search 
			 OR p.TRN LIKE @search) 
		    AND (
			(c.CompanyID = @cid
			AND (SELECT COUNT(*)
				FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
										RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
										LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
										LEFT JOIN Users u ON ass.UID = u.UID
				WHERE (up.[Table] = 'Users' AND up.[Action] = 'Read' AND u.UID = @uid)) > 0)
			 OR (SELECT COUNT(*)
				FROM UserGroups ug LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
								   LEFT JOIN Users u ON ass.UID = u.UID
				WHERE ug.UserGroup IN ('System Administrator') AND u.UID = @uid) > 0
			) 
	END

	IF @type = 'policy'
	BEGIN
		SELECT TOP 14 p.PolicyID [ID], 'policy' [table], p.Policyno [name]
		FROM Policies p LEFT JOIN Company c ON p.CompanyID = c.CompanyID
						LEFT JOIN PolicyCovers pc ON p.PolicyCover = pc.ID
						LEFT JOIN VehiclesUnderPolicy vup ON p.PolicyID = vup.PolicyID
						LEFT JOIN Vehicles v ON vup.VehicleID = v.VehicleID
						LEFT JOIN ExceptedDrivers ecd ON v.VehicleID = ecd.VehicleID AND p.PolicyID = ecd.PolicyID
						LEFT JOIN ExcludedDrivers exd ON v.VehicleID = exd.VehicleID AND p.PolicyID = exd.PolicyID
						LEFT JOIN AuthorizedDrivers aut ON v.VehicleID = aut.VehicleID AND p.PolicyID = aut.PolicyID
						LEFT JOIN VehicleHandDriveLookUp hd ON v.HandDrive = hd.VHDLID
						LEFT JOIN People pe ON v.MainDriver = pe.PeopleID
						LEFT JOIN TransmissionTypeLookUp trans ON v.TransmissionType = trans.TransmisionTypeID
						LEFT OUTER JOIN PolicyInsured ins ON p.PolicyID = ins.PolicyID
						LEFT JOIN People inspe ON ins.PersonID = inspe.PeopleID
		WHERE (inspe.FName LIKE @search
			OR inspe.MName LIKE @search
			OR inspe.LName LIKE @search
			OR inspe.TRN LIKE @search
			OR c.CompanyName LIKE @search
			OR CAST(p.StartDateTime as varchar(MAX)) LIKE @search
			OR CAST(p.EndDateTime as varchar(MAX)) LIKE @search
			OR pc.prefix LIKE @search
			OR p.Policyno LIKE @search
			OR pc.Cover LIKE @search
			OR Make LIKE @search 
			OR Model LIKE @search 
 			OR ModelType LIKE @search 
			OR BodyType LIKE @search 
			OR Extension LIKE @search 
			OR VehicleRegistrationNo LIKE @search
			OR VIN LIKE @search
			OR hd.HDType LIKE @search
			OR Colour LIKE @search
			OR CAST(Cylinders as varchar(MAX)) LIKE @search
			OR EngineNo LIKE @search
			OR EngineModified LIKE @search
			OR EngineType LIKE @search
			OR CAST(EstimatedAnnualMileage as varchar(MAX)) LIKE @search
			OR HPCCUnitType LIKE @search
			OR pe.FName LIKE @search
			OR pe.MName LIKE @search
			OR pe.LName LIKE @search
			OR pe.TRN LIKE @search
			OR CAST(ReferenceNo as varchar(MAX)) LIKE @search
			OR RegistrationLocation LIKE @search
			OR RoofType LIKE @search
			OR CAST(Seating as varchar(MAX)) LIKE @search
			OR CAST(Tonnage as varchar(MAX)) LIKE @search
			OR trans.TransmissionType LIKE @search
			OR CAST(VehicleYear as varchar(MAX)) LIKE @search)
			-- SECURITY CHECK
		   AND (
		   (c.CompanyID = @cid OR ((SELECT COUNT(*) FROM Company c RIGHT JOIN Employees e ON c.CompanyID = e.EmployeeID RIGHT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE c.CompanyID = @cid AND u.UID = p.CreatedBy) > 0)) 
		   AND
		   (SELECT COUNT(*)
				FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
										RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
										LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
										LEFT JOIN Users u ON ass.UID = u.UID
				WHERE ((up.[Table] = 'Policies' AND up.[Action] = 'Read') OR ug.UserGroup IN ('System Administrator')) AND u.UID = @uid) > 0)
	END

	IF @type = 'certificate'
	BEGIN
		SELECT TOP 14 vc.VehicleCertificateID [ID], 'certificate' [table], vc.CertificateType [name]
		FROM VehicleCertificates vc LEFT JOIN Company c ON vc.CompanyID = c.CompanyID
									LEFT JOIN Vehicles v ON vc.RiskItemNo = v.VehicleID
									LEFT JOIN Policies p ON vc.PolicyID = p.PolicyID
		WHERE (c.CompanyName LIKE @search
		   OR CAST(vc.EffectiveDate as varchar(MAX)) LIKE @search
		   OR CAST(vc.ExpiryDate as varchar(MAX)) LIKE @search
		   OR CAST(vc.EndorsementNo as varchar(MAX)) LIKE @search
		   OR CAST(vc.PrintedPaperNo as varchar(MAX)) LIKE @search
		   OR v.VehicleRegistrationNo LIKE @search
		   OR v.VIN LIKE @search
		   OR CAST(vc.MarksRegNo as varchar(MAX)) LIKE @search
		   OR vc.Usage LIKE @search
		   OR vc.Scheme LIKE @search
		   OR vc.CertificateType LIKE @search
		   OR vc.ExtensionCode LIKE @search
		   OR CAST(vc.UniqueNum as varchar(MAX)) LIKE @search)
		   -- SECURITY CHECK
		   AND 
		   (((c.CompanyID = @cid OR p.CompanyID = @cid OR p.CompanyCreatedBy = @cid) AND (SELECT COUNT(*)
									FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
															RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
															LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
															LEFT JOIN Users u ON ass.UID = u.UID
									WHERE ((up.[Table] = 'Certificates' AND up.[Action] = 'Read')) AND u.UID = @uid) > 0)
			OR
			((SELECT COUNT(*) FROM UserGroups ug LEFT JOIN UserGroupUserAssoc uga ON ug.UGID = uga.UGID
			  									LEFT JOIN Users u ON uga.UID = u.UID
								WHERE u.UID = @uid AND ug.UserGroup IN ('System Administrator')) > 0)
			)

	END

	IF @type = 'covernote'
	BEGIN
		SELECT TOP 14 vcn.VehicleCoverNoteID [ID], 'covernote' [table], CAST(vcn.CoverNoteID as varchar(MAX)) [name]
		FROM VehicleCoverNote vcn LEFT JOIN Company c ON vcn.CompanyID = c.CompanyID
								  LEFT JOIN Vehicles v  ON vcn.RiskItemNo = v.VehicleID
								  LEFT JOIN Policies p ON vcn.PolicyID = p.PolicyID
		WHERE (c.CompanyName LIKE @search
		   OR vcn.Alterations LIKE @search
		   OR CAST(vcn.CoverNoteID as varchar(MAX)) LIKE @search
		   OR CAST(vcn.EffectiveDate as varchar(MAX)) LIKE @search
		   OR CAST(vcn.EndorsementNo as varchar(MAX)) LIKE @search
		   OR CAST(vcn.ManualCoverNoteNo as varchar(MAX)) LIKE @search
		   OR v.VehicleRegistrationNo LIKE @search
		   OR v.VIN LIKE @search
		   OR vcn.LimitsOfUse LIKE @search
		   OR CAST(vcn.serial as varchar(MAX)) LIKE @search)
		   -- SECURITY CHECK
		   AND
		   (((c.CompanyID = @cid OR p.CompanyID = @cid OR p.CompanyCreatedBy = @cid) 
						AND (SELECT COUNT(*)
									FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
															RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
															LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
															LEFT JOIN Users u ON ass.UID = u.UID
									WHERE ((up.[Table] = 'CoverNotes' AND up.[Action] = 'Read')) AND u.UID = @uid) > 0)
			OR
			((SELECT COUNT(*) FROM UserGroups ug LEFT JOIN UserGroupUserAssoc uga ON ug.UGID = uga.UGID
			  									LEFT JOIN Users u ON uga.UID = u.UID
								WHERE u.UID = @uid AND ug.UserGroup IN ('System Administrator')) > 0)
			)
	END

	IF @type IS NULL OR @type = ''
	BEGIN

	-- LIMIT OUTPUT TO TOP 10 (for general search results and autofill)
	SELECT TOP 14 * FROM(
		-- SEARCH INTO COMPANY
		SELECT CompanyID [ID], 'company' [table], CompanyName [name]  
		FROM Company c LEFT JOIN CompanyTypes ct ON c.[Type] = ct.ID
		WHERE 
			 (CompanyName LIKE @search
		   OR CAST(IAJID as varchar(MAX)) LIKE @search
		   OR TRN LIKE @search
		   OR CompShortening LIKE @search
		   OR ct.[Type] LIKE @search
		   OR InsurerCode LIKE @search)
		 AND ((SELECT COUNT(*)
				FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
										RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
										LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
										LEFT JOIN Users u ON ass.UID = u.UID
				WHERE ((up.[Table] = 'Company' AND up.[Action] = 'Read') OR ug.UserGroup IN ('System Administrator')) AND u.UID = @uid) > 0)
		UNION
		-- SEARCH INTO Users/People
		SELECT u.UID [ID], 'user' [table], u.username [name]
		FROM Users u LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID
					 LEFT JOIN People p ON e.PersonID = p.PeopleID
					 LEFT JOIN Company c ON e.CompanyID = c.CompanyID
					 LEFT JOIN Employees myemp ON c.CompanyID = myemp.CompanyID
					 LEFT JOIN Users me ON myemp.EmployeeID = me.EmployeeID
		WHERE 
		(
			(u.Filter = 0 AND u.Deleted = 0) 
			AND 
			(
				   u.username LIKE @search 
				OR p.FName LIKE @search 
				OR p.MName LIKE @search 
				OR p.LName LIKE @search 
				OR p.TRN LIKE @search
				OR CAST(p.DOB as varchar(MAX)) LIKE @search
				OR c.CompanyName LIKE @search
				OR c.CompShortening LIKE @search
				OR c.InsurerCode LIKE @search

			)
		)
		-- SECURITY CHECK/LIMIT TO YOUR COMPANY IF NOT SYSTEM ADMIN
		AND (
			(c.CompanyID = @cid
			AND (SELECT COUNT(*)
				FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
										RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
										LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
										LEFT JOIN Users u ON ass.UID = u.UID
				WHERE (up.[Table] = 'Users' AND up.[Action] = 'Read' AND u.UID = @uid)) > 0)
			 OR (SELECT COUNT(*)
				FROM UserGroups ug LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
								   LEFT JOIN Users u ON ass.UID = u.UID
				WHERE (ug.UserGroup IN ('System Administrator')) AND u.UID = @uid) > 0
			) 
		UNION
		-- SEARCH INTO VEHICLES
		SELECT v.VehicleID [ID], 'vehicle' [table], v.VehicleRegistrationNo [name]
		FROM Vehicles v LEFT JOIN VehicleHandDriveLookUp hd ON v.HandDrive = hd.VHDLID
						LEFT JOIN Drivers d ON v.MainDriver = d.DriverID
						LEFT JOIN People p ON d.PersonID = p.PeopleID
						LEFT JOIN TransmissionTypeLookUp trans ON v.TransmissionType = trans.TransmisionTypeID
		WHERE (Make LIKE @search 
 		    OR Model LIKE @search 
 		    OR ModelType LIKE @search 
		    OR BodyType LIKE @search 
		    OR Extension LIKE @search 
		    OR VehicleRegistrationNo LIKE @search
		    OR VIN LIKE @search
		    OR hd.HDType LIKE @search
		    OR Colour LIKE @search
		    OR CAST(Cylinders as varchar(MAX)) LIKE @search
		    OR EngineNo LIKE @search
		    OR EngineModified LIKE @search
		    OR EngineType LIKE @search
		    OR CAST(EstimatedAnnualMileage as varchar(MAX)) LIKE @search
		    OR HPCCUnitType LIKE @search
		    OR p.FName LIKE @search
		    OR p.MName LIKE @search
		    OR p.LName LIKE @search
		    OR p.TRN LIKE @search
		    OR CAST(ReferenceNo as varchar(MAX)) LIKE @search
		    OR RegistrationLocation LIKE @search
		    OR RoofType LIKE @search
		    OR CAST(Seating as varchar(MAX)) LIKE @search
		    OR CAST(Tonnage as varchar(MAX)) LIKE @search
		    OR trans.TransmissionType LIKE @search
		    OR CAST(VehicleYear as varchar(MAX)) LIKE @search
			OR ChassisNo LIKE @search)
			-- SECURITY CHECK
		   AND ((SELECT COUNT(*)
				FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
										RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
										LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
										LEFT JOIN Users u ON ass.UID = u.UID
				WHERE ((up.[Table] = 'Vehicles' AND up.[Action] = 'Read')  OR ug.UserGroup IN ('System Administrator')) AND u.UID = @uid) > 0)
		UNION
		-- SEARCH INTO POLICIES
		SELECT p.PolicyID [ID], 'policy' [table] , p.Policyno [name]
		FROM Policies p LEFT JOIN Company c ON p.CompanyID = c.CompanyID
						LEFT JOIN PolicyCovers pc ON p.PolicyCover = pc.ID
						LEFT JOIN PolicyInsured ins ON p.PolicyID = ins.PolicyID
						LEFT OUTER JOIN People inspe ON ins.PersonID = inspe.PeopleID
						LEFT OUTER JOIN Company comp ON ins.CompanyID = comp.CompanyID
						LEFT JOIN VehiclesUnderPolicy vup ON p.PolicyID = vup.PolicyID
						LEFT JOIN Vehicles v ON vup.VehicleID = v.VehicleID
						LEFT JOIN ExceptedDrivers ecd ON v.VehicleID = ecd.VehicleID AND p.PolicyID = ecd.PolicyID
						LEFT JOIN ExcludedDrivers exd ON v.VehicleID = exd.VehicleID AND p.PolicyID = exd.PolicyID
						LEFT JOIN AuthorizedDrivers aut ON v.VehicleID = aut.VehicleID AND p.PolicyID = aut.PolicyID
						LEFT JOIN VehicleHandDriveLookUp hd ON v.HandDrive = hd.VHDLID
						LEFT JOIN People pe ON v.MainDriver = pe.PeopleID
						LEFT JOIN TransmissionTypeLookUp trans ON v.TransmissionType = trans.TransmisionTypeID
		WHERE (inspe.FName LIKE @search
			OR inspe.MName LIKE @search
			OR inspe.LName LIKE @search
			OR inspe.TRN LIKE @search
			OR c.CompanyName LIKE @search
			OR CAST(p.StartDateTime as varchar(MAX)) LIKE @search
			OR CAST(p.EndDateTime as varchar(MAX)) LIKE @search
			OR comp.CompanyName LIKE @search
			OR comp.IAJID LIKE @search
			OR comp.TRN LIKE @search
			OR inspe.FName LIKE @search OR inspe.MName LIKE @search OR inspe.LName LIKE @search OR inspe.TRN LIKE @search
			OR pc.prefix LIKE @search
			OR p.Policyno LIKE @search
			OR pc.Cover LIKE @search
			OR Make LIKE @search 
			OR Model LIKE @search 
 			OR ModelType LIKE @search 
			OR BodyType LIKE @search 
			OR Extension LIKE @search 
			OR VehicleRegistrationNo LIKE @search
			OR VIN LIKE @search
			OR hd.HDType LIKE @search
			OR Colour LIKE @search
			OR CAST(Cylinders as varchar(MAX)) LIKE @search
			OR EngineNo LIKE @search
			OR EngineModified LIKE @search
			OR EngineType LIKE @search
			OR CAST(EstimatedAnnualMileage as varchar(MAX)) LIKE @search
			OR HPCCUnitType LIKE @search
			OR pe.FName LIKE @search
			OR pe.MName LIKE @search
			OR pe.LName LIKE @search
			OR pe.TRN LIKE @search
			OR CAST(ReferenceNo as varchar(MAX)) LIKE @search
			OR RegistrationLocation LIKE @search
			OR RoofType LIKE @search
			OR CAST(Seating as varchar(MAX)) LIKE @search
			OR CAST(Tonnage as varchar(MAX)) LIKE @search
			OR trans.TransmissionType LIKE @search
			OR CAST(VehicleYear as varchar(MAX)) LIKE @search
			OR ChassisNo LIKE @search)
			-- SECURITY CHECK
		   AND (
		   --(p.CompanyID = @cid OR p.CompanyCreatedBy = @cid) -- OR ((SELECT COUNT(*) FROM Company c RIGHT JOIN Employees e ON c.CompanyID = e.EmployeeID RIGHT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE c.CompanyID = @cid AND u.UID = p.CreatedBy) > 0)) 
		   --AND
		   (SELECT COUNT(*)
				FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
										RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
										LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
										LEFT JOIN Users u ON ass.UID = u.UID
				WHERE (((up.[Table] = 'Policies' AND up.[Action] = 'Read') AND (p.CompanyID = @cid OR p.CompanyCreatedBy = @cid))  OR ug.UserGroup IN ('System Administrator')) AND u.UID = @uid) > 0)
		UNION
		-- SEARCH INTO CERTIFICATES
		SELECT vc.VehicleCertificateID [ID], 'certificate' [table], vc.CertificateNo [name]
		FROM VehicleCertificates vc LEFT JOIN Company c ON vc.CompanyID = c.CompanyID
									LEFT JOIN Vehicles v ON vc.RiskItemNo = v.VehicleID
									LEFT JOIN Policies p ON vc.PolicyID = p.PolicyID
		WHERE (c.CompanyName LIKE @search
		   OR CAST(vc.EffectiveDate as varchar(MAX)) LIKE @search
		   OR CAST(vc.ExpiryDate as varchar(MAX)) LIKE @search
		   OR CAST(vc.EndorsementNo as varchar(MAX)) LIKE @search
		   OR CAST(vc.PrintedPaperNo as varchar(MAX)) LIKE @search
		   OR v.VehicleRegistrationNo LIKE @search
		   OR v.VIN LIKE @search
		   OR CAST(vc.MarksRegNo as varchar(MAX)) LIKE @search
		   OR vc.Usage LIKE @search
		   OR vc.Scheme LIKE @search
		   OR vc.CertificateType LIKE @search
		   OR vc.ExtensionCode LIKE @search
		   OR CAST(vc.UniqueNum as varchar(MAX)) LIKE @search)
		   OR vc.CertificateNo LIKE @search
		   -- SECURITY CHECK
		  AND 
		   (((c.CompanyID = @cid OR p.CompanyID = @cid OR p.CompanyCreatedBy = @cid) AND (SELECT COUNT(*)
									FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
															RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
															LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
															LEFT JOIN Users u ON ass.UID = u.UID
									WHERE ((up.[Table] = 'Certificates' AND up.[Action] = 'Read')) AND u.UID = @uid) > 0)
			OR
			((SELECT COUNT(*) FROM UserGroups ug LEFT JOIN UserGroupUserAssoc uga ON ug.UGID = uga.UGID
			  									LEFT JOIN Users u ON uga.UID = u.UID
								WHERE u.UID = @uid AND ug.UserGroup IN ('System Administrator')) > 0)
			)
		UNION
		-- SEARCH INTO COVER NOTES
		SELECT vcn.VehicleCoverNoteID [ID], 'covernote' [table], CAST(vcn.CoverNoteID as varchar(MAX)) [name]
		FROM VehicleCoverNote vcn LEFT JOIN Company c ON vcn.CompanyID = c.CompanyID
								  LEFT JOIN Vehicles v  ON vcn.RiskItemNo = v.VehicleID
								  LEFT JOIN Policies p ON vcn.PolicyID = p.PolicyID
		WHERE (c.CompanyName LIKE @search
		   OR vcn.Alterations LIKE @search
		   OR CAST(vcn.CoverNoteID as varchar(MAX)) LIKE @search
		   OR CAST(vcn.EffectiveDate as varchar(MAX)) LIKE @search
		   OR CAST(vcn.EndorsementNo as varchar(MAX)) LIKE @search
		   OR CAST(vcn.ManualCoverNoteNo as varchar(MAX)) LIKE @search
		   OR v.VehicleRegistrationNo LIKE @search
		   OR v.VIN LIKE @search
		   OR vcn.LimitsOfUse LIKE @search
		   OR CAST(vcn.serial as varchar(MAX)) LIKE @search)
		   -- SECURITY CHECK
		   AND
		   (((c.CompanyID = @cid OR p.CompanyID = @cid OR p.CompanyCreatedBy = @cid) AND (SELECT COUNT(*)
									FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
															RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
															LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
															LEFT JOIN Users u ON ass.UID = u.UID
									WHERE ((up.[Table] = 'CoverNotes' AND up.[Action] = 'Read')) AND u.UID = @uid) > 0)
			OR
			((SELECT COUNT(*) FROM UserGroups ug LEFT JOIN UserGroupUserAssoc uga ON ug.UGID = uga.UGID
			  									LEFT JOIN Users u ON uga.UID = u.UID
								WHERE u.UID = @uid AND ug.UserGroup IN ('System Administrator')) > 0)
			)
		UNION
			SELECT DISTINCT(peo.PeopleID) [ID], 'people' [table], peo.TRN [name]
			FROM PolicyInsured ins	LEFT JOIN People peo ON ins.PersonID = peo.PeopleID
									LEFT JOIN Policies p ON ins.PolicyID = p.PolicyID
			WHERE (ins.PersonID IS NOT NULL)
				AND (
					peo.TRN LIKE @search
					OR peo.FName LIKE @search
					OR peo.MName LIKE @search
					OR peo.LName LIKE @search
				)
				AND 
				((p.CompanyCreatedBy = @cid OR p.CompanyID = @cid) AND
			   ((SELECT COUNT(*)
					FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
											RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
											LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
											LEFT JOIN Users u ON ass.UID = u.UID
					WHERE ((up.[Table] = 'Policies' AND up.[Action] = 'Read')) AND u.UID = @uid) > 0)
				OR
				((SELECT COUNT(*) FROM UserGroups ug LEFT JOIN UserGroupUserAssoc uga ON ug.UGID = uga.UGID
			  										LEFT JOIN Users u ON uga.UID = u.UID
									WHERE u.UID = @uid AND ug.UserGroup IN ('System Administrator')) > 0))
				
	 )i ORDER BY [table];
	 END


END


GO
/****** Object:  StoredProcedure [dbo].[GeneralSearchNoLimit]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GeneralSearchNoLimit]
	-- Add the parameters for the stored procedure here
	@search varchar(MAX),
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @search = '%' + @search + '%';
	DECLARE @cid int;
	SELECT @cid = c.CompanyID FROM Company c RIGHT JOIN Employees e ON c.CompanyID = e.CompanyID
											 RIGHT JOIN Users u ON e.EmployeeID = u.EmployeeID
						      WHERE u.UID = @uid;

	SELECT * FROM(
		-- SEARCH INTO COMPANY
		SELECT CompanyID [ID], 'company' [table]--, CompanyName [name]  
		FROM Company c LEFT JOIN CompanyTypes ct ON c.[Type] = ct.ID
		WHERE 
			 (CompanyName LIKE @search
		   OR CAST(IAJID as varchar(MAX)) LIKE @search
		   OR TRN LIKE @search
		   OR CompShortening LIKE @search
		   OR ct.[Type] LIKE @search
		   OR InsurerCode LIKE @search)
		 AND ((SELECT COUNT(*)
				FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
										RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
										LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
										LEFT JOIN Users u ON ass.UID = u.UID
				WHERE ((up.[Table] = 'Company' AND up.[Action] = 'Read') OR ug.UserGroup IN ('System Administrator')) AND u.UID = @uid) > 0)
		UNION
		-- SEARCH INTO Users/People
		SELECT u.UID [ID], 'user' [table]--, u.username [name]
		FROM Users u LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID
					 LEFT JOIN People p ON e.PersonID = p.PeopleID
					 LEFT JOIN Company c ON e.CompanyID = c.CompanyID
					 LEFT JOIN Employees myemp ON c.CompanyID = myemp.CompanyID
					 LEFT JOIN Users me ON myemp.EmployeeID = me.EmployeeID
		WHERE 
		(
			(u.Filter = 0 AND u.Deleted = 0) 
			AND 
			(
				   u.username LIKE @search 
				OR p.FName LIKE @search 
				OR p.MName LIKE @search 
				OR p.LName LIKE @search 
				OR p.TRN LIKE @search
				OR CAST(p.DOB as varchar(MAX)) LIKE @search
				OR c.CompanyName LIKE @search
				OR c.CompShortening LIKE @search
				OR c.InsurerCode LIKE @search

			)
		)
		-- SECURITY CHECK/LIMIT TO YOUR COMPANY IF NOT SYSTEM ADMIN
		AND (
			(c.CompanyID = @cid
			AND (SELECT COUNT(*)
				FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
										RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
										LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
										LEFT JOIN Users u ON ass.UID = u.UID
				WHERE (up.[Table] = 'Users' AND up.[Action] = 'Read' AND u.UID = @uid)) > 0)
			 OR (SELECT COUNT(*)
				FROM UserGroups ug LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
								   LEFT JOIN Users u ON ass.UID = u.UID
				WHERE (ug.UserGroup IN ('System Administrator')) AND u.UID = @uid) > 0
			) 
		UNION
		-- SEARCH INTO VEHICLES
		SELECT v.VehicleID [ID], 'vehicle' [table]--, v.VehicleRegistrationNo [name]
		FROM Vehicles v LEFT JOIN VehicleHandDriveLookUp hd ON v.HandDrive = hd.VHDLID
						LEFT JOIN Drivers d ON v.MainDriver = d.DriverID
						LEFT JOIN People p ON d.PersonID = p.PeopleID
						LEFT JOIN TransmissionTypeLookUp trans ON v.TransmissionType = trans.TransmisionTypeID
		WHERE (Make LIKE @search 
 		    OR Model LIKE @search 
 		    OR ModelType LIKE @search 
		    OR BodyType LIKE @search 
		    OR Extension LIKE @search 
		    OR VehicleRegistrationNo LIKE @search
		    OR VIN LIKE @search
		    OR hd.HDType LIKE @search
		    OR Colour LIKE @search
		    OR CAST(Cylinders as varchar(MAX)) LIKE @search
		    OR EngineNo LIKE @search
		    OR EngineModified LIKE @search
		    OR EngineType LIKE @search
		    OR CAST(EstimatedAnnualMileage as varchar(MAX)) LIKE @search
		    OR HPCCUnitType LIKE @search
		    OR p.FName LIKE @search
		    OR p.MName LIKE @search
		    OR p.LName LIKE @search
		    OR p.TRN LIKE @search
		    OR CAST(ReferenceNo as varchar(MAX)) LIKE @search
		    OR RegistrationLocation LIKE @search
		    OR RoofType LIKE @search
		    OR CAST(Seating as varchar(MAX)) LIKE @search
		    OR CAST(Tonnage as varchar(MAX)) LIKE @search
		    OR trans.TransmissionType LIKE @search
		    OR CAST(VehicleYear as varchar(MAX)) LIKE @search
			OR ChassisNo LIKE @search)
			-- SECURITY CHECK
		   AND ((SELECT COUNT(*)
				FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
										RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
										LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
										LEFT JOIN Users u ON ass.UID = u.UID
				WHERE ((up.[Table] = 'Vehicles' AND up.[Action] = 'Read')  OR ug.UserGroup IN ('System Administrator')) AND u.UID = @uid) > 0)
		UNION
		-- SEARCH INTO POLICIES
		SELECT p.PolicyID [ID], 'policy' [table] --, p.Policyno [name]
		FROM Policies p LEFT JOIN Company c ON p.CompanyID = c.CompanyID
						LEFT JOIN PolicyCovers pc ON p.PolicyCover = pc.ID
						LEFT JOIN PolicyInsured ins ON p.PolicyID = ins.PolicyID
						LEFT OUTER JOIN People inspe ON ins.PersonID = inspe.PeopleID
						LEFT OUTER JOIN Company comp ON ins.CompanyID = comp.CompanyID
						LEFT JOIN VehiclesUnderPolicy vup ON p.PolicyID = vup.PolicyID
						LEFT JOIN Vehicles v ON vup.VehicleID = v.VehicleID
						LEFT JOIN ExceptedDrivers ecd ON v.VehicleID = ecd.VehicleID AND p.PolicyID = ecd.PolicyID
						LEFT JOIN ExcludedDrivers exd ON v.VehicleID = exd.VehicleID AND p.PolicyID = exd.PolicyID
						LEFT JOIN AuthorizedDrivers aut ON v.VehicleID = aut.VehicleID AND p.PolicyID = aut.PolicyID
						LEFT JOIN VehicleHandDriveLookUp hd ON v.HandDrive = hd.VHDLID
						LEFT JOIN People pe ON v.MainDriver = pe.PeopleID
						LEFT JOIN TransmissionTypeLookUp trans ON v.TransmissionType = trans.TransmisionTypeID
		WHERE (inspe.FName LIKE @search
			OR inspe.MName LIKE @search
			OR inspe.LName LIKE @search
			OR inspe.TRN LIKE @search
			OR c.CompanyName LIKE @search
			OR CAST(p.StartDateTime as varchar(MAX)) LIKE @search
			OR CAST(p.EndDateTime as varchar(MAX)) LIKE @search
			OR comp.CompanyName LIKE @search
			OR comp.IAJID LIKE @search
			OR comp.TRN LIKE @search
			OR inspe.FName LIKE @search OR inspe.MName LIKE @search OR inspe.LName LIKE @search OR inspe.TRN LIKE @search
			OR pc.prefix LIKE @search
			OR p.Policyno LIKE @search
			OR pc.Cover LIKE @search
			OR Make LIKE @search 
			OR Model LIKE @search 
 			OR ModelType LIKE @search 
			OR BodyType LIKE @search 
			OR Extension LIKE @search 
			OR VehicleRegistrationNo LIKE @search
			OR VIN LIKE @search
			OR hd.HDType LIKE @search
			OR Colour LIKE @search
			OR CAST(Cylinders as varchar(MAX)) LIKE @search
			OR EngineNo LIKE @search
			OR EngineModified LIKE @search
			OR EngineType LIKE @search
			OR CAST(EstimatedAnnualMileage as varchar(MAX)) LIKE @search
			OR HPCCUnitType LIKE @search
			OR pe.FName LIKE @search
			OR pe.MName LIKE @search
			OR pe.LName LIKE @search
			OR pe.TRN LIKE @search
			OR CAST(ReferenceNo as varchar(MAX)) LIKE @search
			OR RegistrationLocation LIKE @search
			OR RoofType LIKE @search
			OR CAST(Seating as varchar(MAX)) LIKE @search
			OR CAST(Tonnage as varchar(MAX)) LIKE @search
			OR trans.TransmissionType LIKE @search
			OR CAST(VehicleYear as varchar(MAX)) LIKE @search
			OR ChassisNo LIKE @search)
			-- SECURITY CHECK
		   AND (
		   --(p.CompanyID = @cid OR p.CompanyCreatedBy = @cid) -- OR ((SELECT COUNT(*) FROM Company c RIGHT JOIN Employees e ON c.CompanyID = e.EmployeeID RIGHT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE c.CompanyID = @cid AND u.UID = p.CreatedBy) > 0)) 
		   --AND
		   (SELECT COUNT(*)
				FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
										RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
										LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
										LEFT JOIN Users u ON ass.UID = u.UID
				WHERE (((up.[Table] = 'Policies' AND up.[Action] = 'Read') AND (p.CompanyID = @cid OR p.CompanyCreatedBy = @cid))  OR ug.UserGroup IN ('System Administrator')) AND u.UID = @uid) > 0)
		UNION
		-- SEARCH INTO CERTIFICATES
		SELECT vc.VehicleCertificateID [ID], 'certificate' [table]--, vc.CertificateType [name]
		FROM VehicleCertificates vc LEFT JOIN Company c ON vc.CompanyID = c.CompanyID
									LEFT JOIN Vehicles v ON vc.RiskItemNo = v.VehicleID
									LEFT JOIN Policies p ON vc.PolicyID = p.PolicyID
		WHERE (c.CompanyName LIKE @search
		   OR CAST(vc.EffectiveDate as varchar(MAX)) LIKE @search
		   OR CAST(vc.ExpiryDate as varchar(MAX)) LIKE @search
		   OR CAST(vc.EndorsementNo as varchar(MAX)) LIKE @search
		   OR CAST(vc.PrintedPaperNo as varchar(MAX)) LIKE @search
		   OR v.VehicleRegistrationNo LIKE @search
		   OR v.VIN LIKE @search
		   OR CAST(vc.MarksRegNo as varchar(MAX)) LIKE @search
		   OR vc.Usage LIKE @search
		   OR vc.Scheme LIKE @search
		   OR vc.CertificateType LIKE @search
		   OR vc.ExtensionCode LIKE @search
		   OR CAST(vc.UniqueNum as varchar(MAX)) LIKE @search)
		   OR vc.CertificateNo LIKE @search
		   -- SECURITY CHECK
		  AND 
		   (((c.CompanyID = @cid OR p.CompanyCreatedBy = @cid OR p.CompanyID = @cid) AND (SELECT COUNT(*)
									FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
															RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
															LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
															LEFT JOIN Users u ON ass.UID = u.UID
									WHERE ((up.[Table] = 'Certificates' AND up.[Action] = 'Read')) AND u.UID = @uid) > 0)
			OR
			((SELECT COUNT(*) FROM UserGroups ug LEFT JOIN UserGroupUserAssoc uga ON ug.UGID = uga.UGID
			  									LEFT JOIN Users u ON uga.UID = u.UID
								WHERE u.UID = @uid AND ug.UserGroup IN ('System Administrator')) > 0)
			)
		UNION
		-- SEARCH INTO COVER NOTES
		SELECT vcn.VehicleCoverNoteID [ID], 'covernote' [table]--, CAST(vcn.CoverNoteID as varchar(MAX)) [name]
		FROM VehicleCoverNote vcn LEFT JOIN Company c ON vcn.CompanyID = c.CompanyID
								  LEFT JOIN Vehicles v  ON vcn.RiskItemNo = v.VehicleID
								  LEFT JOIN Policies p ON vcn.PolicyID = p.PolicyID
		WHERE (c.CompanyName LIKE @search
		   OR vcn.Alterations LIKE @search
		   OR CAST(vcn.CoverNoteID as varchar(MAX)) LIKE @search
		   OR CAST(vcn.EffectiveDate as varchar(MAX)) LIKE @search
		   OR CAST(vcn.EndorsementNo as varchar(MAX)) LIKE @search
		   OR CAST(vcn.ManualCoverNoteNo as varchar(MAX)) LIKE @search
		   OR v.VehicleRegistrationNo LIKE @search
		   OR v.VIN LIKE @search
		   OR vcn.LimitsOfUse LIKE @search
		   OR CAST(vcn.serial as varchar(MAX)) LIKE @search)
		   -- SECURITY CHECK
		   AND
		   (((c.CompanyID = @cid OR p.CompanyCreatedBy = @cid OR p.CompanyID = @cid) AND (SELECT COUNT(*)
									FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
															RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
															LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
															LEFT JOIN Users u ON ass.UID = u.UID
									WHERE ((up.[Table] = 'CoverNotes' AND up.[Action] = 'Read')) AND u.UID = @uid) > 0)
			OR
			((SELECT COUNT(*) FROM UserGroups ug LEFT JOIN UserGroupUserAssoc uga ON ug.UGID = uga.UGID
			  									LEFT JOIN Users u ON uga.UID = u.UID
								WHERE u.UID = @uid AND ug.UserGroup IN ('System Administrator')) > 0)
			)
			UNION
			SELECT DISTINCT(peo.PeopleID) [ID], 'people' [table]--, peo.TRN [name]
			FROM PolicyInsured ins	LEFT JOIN People peo ON ins.PersonID = peo.PeopleID
									LEFT JOIN Policies p ON ins.PolicyID = p.PolicyID
			WHERE (ins.PersonID IS NOT NULL)
				AND (
					peo.TRN LIKE @search
					OR peo.FName LIKE @search
					OR peo.MName LIKE @search
					OR peo.LName LIKE @search
				)
				AND 
				((p.CompanyCreatedBy = @cid OR p.CompanyID = @cid) AND
			   ((SELECT COUNT(*)
					FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
											RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
											LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
											LEFT JOIN Users u ON ass.UID = u.UID
					WHERE ((up.[Table] = 'Policies' AND up.[Action] = 'Read')) AND u.UID = @uid) > 0)
				OR
				((SELECT COUNT(*) FROM UserGroups ug LEFT JOIN UserGroupUserAssoc uga ON ug.UGID = uga.UGID
			  										LEFT JOIN Users u ON uga.UID = u.UID
									WHERE u.UID = @uid AND ug.UserGroup IN ('System Administrator')) > 0))
	 )i ORDER BY [table];
	 --END


END


GO
/****** Object:  StoredProcedure [dbo].[GenerateCoverNoteNo]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GenerateCoverNoteNo]
	-- Add the parameters for the stored procedure here
	@cid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @short varchar(MAX), @count varchar(MAX);
	SELECT @short = CompShortening FROM Company WHERE CompanyID = 1;
	
	SELECT @count = [count] FROM CompanyCoverNoteCount WHERE CompanyID = 1;
	IF (@count IS NULL) SET @count = 1;
	ELSE SET @count = @count + 1;

	SET @count = (CASE
					WHEN (CAST(@count as int)/10 < 1) THEN '000' + @count
					WHEN (CAST(@count as int)/100 < 1) THEN '00' + @count
					WHEN (CAST(@count as int)/1000 < 1) THEN '0' + @count
					ELSE @count 
				END);


	SELECT (CASE WHEN (@short IS NULL) THEN '' ELSE @short END) + '-' + CAST(@count as varchar(MAX)) [result];
END


GO
/****** Object:  StoredProcedure [dbo].[GenerateVehicleCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 13/08/2014
-- Description:	Pulling data about a vehicle, and its coverage
-- =============================================
CREATE PROCEDURE [dbo].[GenerateVehicleCertificate]
	
	@vehyear varchar(max) = null,
    @vehmake varchar(max) = null,
    @vehregno varchar(max) = null,
    @vehext varchar(max) = null,
    @maininsured varchar(max) = null,
    @drivers varchar(max) = null,
    @vehdesc varchar(max) = null,
    @cover varchar(max) = null,
    @vehmodeltype varchar(max) = null,
    @vehmodel varchar(max) = null,
    @authorizedwording varchar(max) = null,
    @mortgagees varchar(max) = null,
    @limitsofuse varchar(max) = null,
	@Usage  varchar(max) = null,
    @PolicyNo  varchar(max) = null,
    @PolicyHoldersAddress varchar(max) = null,
    @CovernoteNo varchar(max) = null,
    @Period varchar(max) = null,
    @effectiveTime varchar(max) = null,
    @expiryTime varchar(max) = null,
    @effectivedate varchar(max) = null,
    @expirydate varchar(max) = null,
    @endorsementNo varchar(max) = null,
    @bodyType varchar(max) = null,
    @seating varchar(max) = null,
    @chassisNo varchar(max) = null,
    @certificateType varchar(max) = null,
    @certitificateNo varchar(max) = null,
	@certificateCode varchar(max) = null,
    @engineNo  varchar(max) = null,
	@HPCC varchar(max) = null,
    @vcid int,
	@uid int
	
	

AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
    
	INSERT INTO GeneratedCertificate (CreatedBy, VehicleYear, VehicleMake, VehicleRegNo, VehExtension, MainInsured, VehDrivers, VehicleDesc, PolicyCover, VehModelType, VehicleModel, 
									 authorizeddrivers, mortgagees, LimitsOfUse, Usage,   PolicyNo,PolicyHolderAddress ,CovernoteNo, Period, effectiveTime ,expiryTime ,effectivedate,
                                     expirydate, endorsementNo, bodyType ,seating ,chassisNo ,certificateType,certitificateNo, CertificateCode, engineNo, VehicleCertificateID, HPCC)
							VALUES	(@uid, @vehyear, @vehmake, @vehregno, @vehext, @maininsured,@drivers, @vehdesc, @cover, @vehmodeltype, @vehmodel,
									 @authorizedwording, @mortgagees, @limitsofuse, @Usage ,@PolicyNo ,@PolicyHoldersAddress ,@CovernoteNo, @Period, @effectiveTime ,@expiryTime ,
									 @effectivedate,@expirydate ,@endorsementNo ,@bodyType ,@seating ,@chassisNo ,@certificateType ,@certitificateNo, @certificateCode, @engineNo ,@vcid, @HPCC);
	

   	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
						   @origdata = null,
						   @newdata = 'VehicleCertificateID:' + CONVERT(varchar(MAX),@@IDENTITY)
						   +';regNo:' + CONVERT(varchar(MAX),@vehregno) 
						   +';vehicleExt:' + CONVERT(varchar(MAX),@vehext)
						   +';MainInsured:' + CONVERT(varchar(MAX),@MainInsured) 
						   +';LimitsOfUse:' + CONVERT(varchar(MAX),@LimitsOfUse)
						   +';policyCover:' + CONVERT(varchar(MAX),@cover)
						   +';vehMake:' + CONVERT(varchar(MAX),@vehMake)
						   +';vehModel:' + CONVERT(varchar(MAX),@vehModel)
						   +';vehModelType:' + CONVERT(varchar(MAX),@vehModelType);
					             
							
	EXEC LogAudit 'insert', @uid , @details, 'GeneratedCertificate',  @@IDENTITY, @origdata, @newdata, 'insert';


	--UPDATING THE VEHICLE CERTIFICATE'S 'GENERATED' FLAG

    SELECT @details = 'The record ' + CONVERT(varchar(MAX),@vcid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
					   @origdata = 'VehicleCertificateID:' + CONVERT(varchar(MAX),@vcid)+';Generated:' + CONVERT(varchar(MAX),Generated)
					   FROM VehicleCertificates WHERE VehicleCertificateID = @vcid;

	UPDATE VehicleCertificates SET Generated = 1 WHERE VehicleCertificateID = @vcid;
	
	 SELECT @newdata = 'VehicleCertificateID:' + CONVERT(varchar(MAX),@vcid)+';Generated:' + CONVERT(varchar(MAX),Generated)
					   FROM VehicleCertificates WHERE VehicleCertificateID = @vcid;				             
							
	EXEC LogAudit 'update', @uid , @details, 'VehicleCertificates',  @vcid, @origdata, @newdata, 'update';


END

GO
/****** Object:  StoredProcedure [dbo].[GetAddress]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAddress]
	-- Add the parameters for the stored procedure here
	@AddressID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [Address].RoadNumber as roadnumber, RoadName.RoadName as roadname,
		   RoadType.RoadType as roadtype, ZipCodes.ZipCode as zipcode, Parishes.PID, Parishes.Parish,
		   Cities.City as city, Countries.CountryName as country
			 FROM [Address] LEFT OUTER JOIN RoadName ON [Address].RoadName = RoadName.RID
							LEFT OUTER JOIN RoadType ON [Address].RoadType = RoadType.RTID
							LEFT OUTER JOIN ZipCodes ON [Address].ZipCode = ZipCodes.ZCID
							LEFT OUTER JOIN Cities ON [Address].City = Cities.CID
							LEFT OUTER JOIN Parishes ON [Address].Parish = Parishes.PID
							LEFT OUTER JOIN Countries ON [Address].Country = Countries.CID
			 WHERE [Address].AID = @AddressID;
END


GO
/****** Object:  StoredProcedure [dbo].[GetAddressFromID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 05/06/14
-- Description:	Getting the address of a person, based on their Id
-- =============================================
  CREATE PROCEDURE [dbo].[GetAddressFromID]
	-- Add the parameters for the stored procedure here

	@ID int
	
AS
BEGIN
	SET NOCOUNT ON;


	DECLARE @AddressId int;
	

	--find address ID for the specific user
	 SELECT @AddressId = Address FROM People WHERE PeopleID = @ID;


	SELECT AID, [Address].RoadNumber, AptNumber, RoadName.RoadName, RoadType.RoadType, ZipCodes.ZipCode, Cities.City, Countries.CountryName, Parishes.Parish, Parishes.PID
	FROM Address RIGHT JOIN RoadName ON Address.RoadName = RoadName.RID
				 RIGHT JOIN RoadType ON Address.RoadType = RoadType.RTID
				 RIGHT JOIN ZipCodes ON Address.ZipCode = ZipCodes.ZCID
				 RIGHT JOIN Cities ON Address.City = Cities.CID
				 RIGHT JOIN Countries ON Address.Country = Countries.CID
				 RIGHT JOIN Parishes ON Address.Parish = Parishes.PID
				 WHERE Address.AID= @AddressId;

END


GO
/****** Object:  StoredProcedure [dbo].[GetAddressRequiredFields]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAddressRequiredFields]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT RoadNumber, RoadName, RoadType, ZipCode, City, Country
		FROM RequireAddressPerCompany
			JOIN 
			 Employees
			ON RequireAddressPerCompany.CompanyID = Employees.CompanyID
			JOIN Users ON Employees.EmployeeID = Users.EmployeeID
	 WHERE Users.UID = @uid;
END


GO
/****** Object:  StoredProcedure [dbo].[GetAdmins]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 22/09/2014
-- Description:	Retrieving all the company admins, of the company under which a vehicle is currently under policy
-- =============================================
CREATE PROCEDURE [dbo].[GetAdmins]
	@vid int,
	@startDateTime varchar (250),
	@endDateTime varchar (250)
AS
BEGIN
	
	SET NOCOUNT ON;

    DECLARE @smallEnddatetime smalldatetime = @endDateTime;
	DECLARE @end datetime = @smallEnddatetime;

	DECLARE @smallstartdatetime smalldatetime = @startDateTime;
	DECLARE @start datetime = @smallstartdatetime;
	
	SELECT Users.UID, Users.username FROM Users WHERE Users.UID IN ( SELECT Users.UID FROM VehiclesUnderPolicy
	                    LEFT JOIN Policies ON Policies.PolicyID = VehiclesUnderPolicy.PolicyID 
						LEFT JOIN Company ON Company.CompanyID = Policies.CompanyID
						LEFT JOIN UserGroupUserAssoc ON UserGroupUserAssoc.UGID = (SELECT UGID FROM UserGroups WHERE UserGroup = 'Company Administrator' AND Company = Company.CompanyID)
						        --   AND UserGroupUserAssoc.UID IN (SELECT UID FROM Users LEFT JOIN Employees ON Employees.EmployeeID = Users.EmployeeID
							       --AND Employees.CompanyID = Company.CompanyID)
						RIGHT JOIN Users ON UserGroupUserAssoc.UID = Users.UID

						WHERE VehiclesUnderPolicy.VehicleID = @vid 
						AND VehiclesUnderPolicy.Cancelled = 0
						AND ((@start >= Policies.StartDateTime AND @start <= Policies.EndDateTime) OR (@end <= Policies.EndDateTime AND @end >= Policies.StartDateTime 
				            OR (@start <= Policies.StartDateTime AND @end >= Policies.EndDateTime))));
END

GO
/****** Object:  StoredProcedure [dbo].[GetAdminsWithCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 22/09/2014
-- Description:	Retrieving all the company admins, of the company under which a vehicle certificate is 
-- =============================================
CREATE PROCEDURE [dbo].[GetAdminsWithCertificate]
	@vehCertId int
AS
BEGIN
	
	SET NOCOUNT ON;
	
    SELECT Users.UID, Users.username FROM Users WHERE Users.UID IN (
                                                       SELECT DISTINCT UserGroupUserAssoc.UID FROM UserGroups
													   INNER JOIN UserGroupUserAssoc ON UserGroupUserAssoc.UGID = UserGroups.UGID
                                                       INNER JOIN PermissionGroupAssoc ON PermissionGroupAssoc.UGID = UserGroupUserAssoc.UGID
						                               INNER JOIN UserPermissions ON UserPermissions.UPID = PermissionGroupAssoc.UPID 
							                                      AND [Table] = 'Certificates' AND [Action] = 'Approve'
	WHERE UserGroups.Company = (SELECT companyID FROM VehicleCertificates WHERE VehicleCertificateID = @vehCertId))

END

GO
/****** Object:  StoredProcedure [dbo].[GetAdminsWithCoverNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 22/09/2014
-- Description:	Retrieving all the company admins, of the company under which a vehicle has a particular cover note
-- =============================================
CREATE PROCEDURE [dbo].[GetAdminsWithCoverNote]
	@vehCoverNoteId int
AS
BEGIN
	
	SET NOCOUNT ON;
	

	SELECT Users.UID, Users.username FROM Users WHERE Users.UID IN (
                                                       SELECT DISTINCT UserGroupUserAssoc.UID FROM UserGroups
													   INNER JOIN UserGroupUserAssoc ON UserGroupUserAssoc.UGID = UserGroups.UGID
                                                       INNER JOIN PermissionGroupAssoc ON PermissionGroupAssoc.UGID = UserGroupUserAssoc.UGID
						                               INNER JOIN UserPermissions ON UserPermissions.UPID = PermissionGroupAssoc.UPID 
							                                      AND [Table] = 'CoverNotes' AND [Action] = 'Approve'
	WHERE UserGroups.Company = (SELECT companyID FROM VehicleCoverNote WHERE VehicleCoverNoteID = @vehCoverNoteId))


	--SELECT Users.UID, Users.username FROM Users WHERE Users.UID IN ( SELECT Users.UID FROM VehicleCoverNote
	--                    RIGHT JOIN Policies ON Policies.PolicyID = VehicleCoverNote.PolicyID 
	--					RIGHT JOIN Company ON Company.CompanyID = Policies.CompanyID
	--					RIGHT JOIN UserGroupUserAssoc ON UserGroupUserAssoc.UGID = (SELECT UGID FROM UserGroups WHERE UserGroup = 'Company Administrator' AND UserGroups.Company= Company.CompanyID)
	--					           AND UserGroupUserAssoc.UID IN (SELECT UID FROM Users RIGHT JOIN Employees ON Employees.EmployeeID = Users.EmployeeID
	--						       AND Employees.CompanyID = Company.CompanyID)

	--					RIGHT JOIN Users ON UserGroupUserAssoc.UID = Users.UID 

	--					WHERE VehicleCoverNote.VehicleCoverNoteID = @vehCoverNoteId);
END

GO
/****** Object:  StoredProcedure [dbo].[GetAllCompanies]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllCompanies]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT CompanyID, IAJID, CompanyName, CompanyTypes.[Type], faultyloginlimit, Company.[Enabled],
		   [Address].AID, [Address].RoadNumber, RoadName.RoadName, 
		   RoadType.RoadType, ZipCodes.ZipCode, Cities.City, Countries.CountryName
	FROM Company LEFT JOIN [CompanyTypes] ON Company.[Type] = CompanyTypes.ID
				 LEFT JOIN [Address] ON Company.CompanyAddress = [Address].AID
				 LEFT OUTER JOIN RoadName ON [Address].RoadName = RoadName.RID
				 LEFT OUTER JOIN RoadType ON RoadType.RTID = [Address].RoadType
				 LEFT OUTER JOIN ZipCodes ON [Address].ZipCode = ZipCodes.ZCID
				 LEFT OUTER JOIN Cities ON Cities.CID = [Address].City
				 LEFT OUTER JOIN Countries ON Countries.CID = [Address].Country;
	
END


GO
/****** Object:  StoredProcedure [dbo].[GetAllCompanyTypes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllCompanyTypes]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [Type] FROM CompanyTypes;
END


GO
/****** Object:  StoredProcedure [dbo].[GetAllCompVehicleCoverNotes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 18/09/2014
-- Description:	Pulling all vehicle cover notes that are owned by a particular insurer
-- =============================================
CREATE PROCEDURE [dbo].[GetAllCompVehicleCoverNotes]
@compId int

AS
BEGIN
	
	SET NOCOUNT ON;

 
	SELECT VehicleCoverNoteID ID, EffectiveDate, period, Vehicles.VIN, Vehicles.VehicleID vehID
	FROM VehicleCoverNote LEFT JOIN Vehicles ON VehicleCoverNote.RiskItemNo = Vehicles.VehicleID 
	     WHERE CompanyID= @compId
		 OR VehicleCoverNote.CreatedBy IN (SELECT Users.UID FROM Employees RIGHT JOIN Users ON Users.EmployeeID = Employees.EmployeeID WHERE Employees.CompanyID = @compId);
		 --OR RiskItemNo IN (SELECT VehiclesUnderPolicy.VehicleID FROM VehiclesUnderPolicy 
		 --RIGHT JOIN Policies ON Policies.CompanyID = @compId WHERE VehiclesUnderPolicy.PolicyID = Policies.PolicyID);

END


GO
/****** Object:  StoredProcedure [dbo].[GetAllErrorsByRequest]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllErrorsByRequest]
	-- Add the parameters for the stored procedure here
	@reqkey uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT e.error_type [type], e.edi_system_id [ediid], pe.edi_system_id [subid]
	FROM error e LEFT JOIN JsonAudit ja ON e.reqid = ja.ID
				 LEFT JOIN error pe ON e.subid = pe.ID
	WHERE ja.[key] = @reqkey;

END

GO
/****** Object:  StoredProcedure [dbo].[GetAllInsurers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 13/10/2014
-- Description:	Retrieving all insurance companies in the system
-- =============================================

CREATE PROCEDURE [dbo].[GetAllInsurers]


AS
BEGIN
	SET NOCOUNT ON;

    SELECT c.CompanyID AS [ID], c.CompanyName, c.IAJID, ct.Type [type], c.[Enabled] [enabled]
	       FROM Company c LEFT JOIN CompanyTypes ct ON c.[Type] = ct.ID
		   WHERE ct.[Type] = 'Insurer';

END

GO
/****** Object:  StoredProcedure [dbo].[GetAllMappingData]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllMappingData]
	-- Add the parameters for the stored procedure here
	@compid int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


	SELECT ID, [Table], RefID, data, [lock] FROM MappingTable 
	WHERE (CompanyID = @compid OR @compid = 0) 
	ORDER BY [lock];

END


GO
/****** Object:  StoredProcedure [dbo].[GetAllMortgagees]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllMortgagees]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Mortgagee FROM Mortgagees ORDER BY Mortgagee ASC;
END

GO
/****** Object:  StoredProcedure [dbo].[GetAllMyNotesCount]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllMyNotesCount]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #Temp(
		ID int IDENTITY,
		NID int,
	);
	INSERT INTO #Temp(NID)
	SELECT DISTINCT children.NID
	FROM Notifications as children LEFT JOIN Notifications as parents ON children.ParentMessage = parents.NID
								   LEFT JOIN NotificationsTo as notes ON children.NID = notes.[Notification]
	WHERE children.NID NOT IN (SELECT parents.NID as PNID FROM Notifications as children INNER JOIN Notifications as parents ON children.ParentMessage = parents.NID)
			AND children.NID IN (SELECT [Notification] FROM NotificationsTo WHERE [UID] = @uid) AND children.Deleted = 0
	ORDER BY children.NID DESC;
		
	SELECT COUNT(*) [count] FROM #Temp;

	DROP TABLE #Temp;

END

GO
/****** Object:  StoredProcedure [dbo].[GetAllMyUserGroupsByID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllMyUserGroupsByID]
	-- Add the parameters for the stored procedure here
	@UserID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT UserGroups.UserGroup
		FROM UserGroups LEFT JOIN UserGroupUserAssoc ON UserGroupUserAssoc.UGID = UserGroups.UGID
						LEFT JOIN Users ON UserGroupUserAssoc.UID = Users.UID
		WHERE Users.UID = @UserID;
END


GO
/****** Object:  StoredProcedure [dbo].[GetAllPeopleIveInsured]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllPeopleIveInsured]
	-- Add the parameters for the stored procedure here
	@cid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (SELECT COUNT(*) FROM Company c LEFT JOIN CompanyTypes ct ON c.[Type] = ct.ID WHERE c.CompanyID = @cid AND ct.[Type] = 'Insurer') > 0
	BEGIN
		--THIS IS AN INSURER PLEASE SHOW ALL OF THIS INSURERS STUFF
		SELECT DISTINCT p.PeopleID [ID], p.FName [fname], p.LName [lname], p.TRN [trn]
				 FROM PolicyInsured polins LEFT JOIN People p ON polins.PersonID = p.PeopleID
										   LEFT JOIN Policies pol ON polins.PolicyID = pol.PolicyID
				 WHERE polins.PersonID IS NOT NULL AND pol.CompanyID = @cid;
	END
	ELSE
	BEGIN
		--NOT INSURER, JUST THE BROKERS STUFF
		SELECT DISTINCT p.PeopleID [ID], p.FName [fname], p.LName [lname], p.TRN [trn]
				 FROM PolicyInsured polins LEFT JOIN People p ON polins.PersonID = p.PeopleID
										   LEFT JOIN Policies pol ON polins.PolicyID = pol.PolicyID
				 WHERE polins.PersonID IS NOT NULL AND pol.CompanyCreatedBy = @cid;
	END

END

GO
/****** Object:  StoredProcedure [dbo].[GetAllPermissions]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllPermissions] 
	-- Add the parameters for the stored procedure here
	@system bit = 0
AS
BEGIN
	SET NOCOUNT ON;

    SELECT UPID, [Table], [Action]
		FROM UserPermissions WHERE @system = 1 OR [system] = 0;
END

GO
/****** Object:  StoredProcedure [dbo].[GetAllPolicyCovers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllPolicyCovers]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ID, Prefix [prefix], Cover [cover], IsCommercialBusiness [commercial] FROM PolicyCovers WHERE Deleted = 0;
END


GO
/****** Object:  StoredProcedure [dbo].[GetAllPolicyCoversForCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllPolicyCoversForCompany]
	-- Add the parameters for the stored procedure here
	@cid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ID, Prefix [prefix], Cover [cover], IsCommercialBusiness [commercial] FROM PolicyCovers WHERE CompanyID = @cid AND Deleted = 0;
END


GO
/****** Object:  StoredProcedure [dbo].[GetAllSubErrorsByRequest]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllSubErrorsByRequest]
	-- Add the parameters for the stored procedure here
	@reqkey uniqueidentifier,
	@ediid varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT e.error_type [type], e.edi_system_id [ediid]
	FROM error e LEFT JOIN error ep ON e.subid = ep.id
				 LEFT JOIN JsonAudit ja ON e.reqid = ja.ID
				 LEFT JOIN JsonAudit jap ON ep.reqid = jap.ID
	WHERE ep.edi_system_id = @ediid AND ja.[key] = @reqkey AND jap.[key] = @reqkey;
END

GO
/****** Object:  StoredProcedure [dbo].[GetAllUsages]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllUsages]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT ID, usage FROM Usages;

END


GO
/****** Object:  StoredProcedure [dbo].[GetAllUserGroupsNotSA]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllUserGroupsNotSA]
	-- Add the parameters for the stored procedure here
	@comID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ug.UGID, ug.UserGroup, c.CompanyID [cid], c.CompanyName [cname], ug.stock, ug.[system]
	FROM UserGroups ug LEFT JOIN Company c ON ug.Company = c.CompanyID
	WHERE c.CompanyID = @comID AND ug.[system] = 0;
END

GO
/****** Object:  StoredProcedure [dbo].[GetAllUsersForAGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllUsersForAGroup]
	-- Add the parameters for the stored procedure here
	@UGID int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	DECLARE @cid int, @groupcid int;
	SELECT @cid = e.CompanyID FROM Employees e LEFT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE u.[UID] = @uid;
	SELECT @groupcid = Company FROM UserGroups WHERE UGID = @UGID;
	
	SELECT Users.[UID], Users.username FROM Users LEFT JOIN UserGroupUserAssoc ON Users.[UID] = UserGroupUserAssoc.[UID]
												  LEFT JOIN Employees e ON Users.EmployeeID = e.EmployeeID
									   WHERE UserGroupUserAssoc.UGID = @UGID AND Filter = 0
									   AND (@groupcid = e.CompanyID OR 
											 ((SELECT COUNT(*) FROM UserGroups ug LEFT JOIN UserGroupUserAssoc uga ON ug.UGID = uga.UGID
																										LEFT JOIN Users u ON uga.[UID] = u.[UID]
																				  WHERE u.[UID] = @uid AND ug.UserGroup = 'System Administrator') > 0) 
												AND (SELECT [system] FROM UserGroups WHERE UGID = @UGID) = 1 );
END


GO
/****** Object:  StoredProcedure [dbo].[GetAllUsersNotForAGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllUsersNotForAGroup]
	-- Add the parameters for the stored procedure here
	@UGID int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	DECLARE @cid int, @groupcid int;
	SELECT @cid = e.CompanyID FROM Employees e LEFT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE u.[UID] = @uid;
	SELECT @groupcid = Company FROM UserGroups WHERE UGID = @UGID;
    
	SELECT Users.[UID], Users.username FROM Users LEFT JOIN Employees e ON Users.EmployeeID = e.EmployeeID
									   WHERE 
											 (@groupcid = e.CompanyID OR 
											 ((SELECT COUNT(*) FROM UserGroups ug LEFT JOIN UserGroupUserAssoc uga ON ug.UGID = uga.UGID
																										LEFT JOIN Users u ON uga.[UID] = u.[UID]
																				  WHERE u.[UID] = @uid AND ug.UserGroup = 'System Administrator') > 0) 
												AND (SELECT [system] FROM UserGroups WHERE UGID = @UGID) = 1 )
									   AND [UID] NOT IN (SELECT [UID] FROM UserGroupUserAssoc WHERE UGID = @UGID) AND Filter = 0;
END


GO
/****** Object:  StoredProcedure [dbo].[GetAllVehicleCertificates]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 13/08/2014
-- Description: Retrieving all vehicle certificates in the system
-- =============================================
CREATE PROCEDURE [dbo].[GetAllVehicleCertificates]
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT VehicleCertificateID as ID,
	       CompanyID,
		   EffectiveDate,
		   ExpiryDate,
		   EndorsementNo,
		   PrintedPaperNo,
		   RiskItemNo,
		   MarksRegNo, 
		   CertificateType, 
		   InsuredCode, 
		   ExtensionCode, 
		   UniqueNum
		 
		FROM VehicleCertificates;
END


GO
/****** Object:  StoredProcedure [dbo].[GetAllVehicleCoverNotes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllVehicleCoverNotes]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT VehicleCoverNoteID ID, EffectiveDate, period, Vehicles.VIN, Vehicles.VehicleID vehID
	FROM VehicleCoverNote LEFT JOIN Vehicles ON VehicleCoverNote.RiskItemNo = Vehicles.VehicleID;
END


GO
/****** Object:  StoredProcedure [dbo].[GetAuditLog]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAuditLog]
	-- Add the parameters for the stored procedure here
	@location varchar(max),
	@recordid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT sa.AuditID [ID], sa.AuditType, p.FName, p.LName, p.PeopleID [pid], sa.ActionTimeStamp [datetime], sa.OrigData [orig], sa.NewData [new]
	FROM SystemAuditLog sa	LEFT JOIN Users u ON sa.ActionUserID = u.[UID]
							LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID
							LEFT JOIN People p ON e.PersonID = p.PeopleID
	WHERE RecordID = @recordid AND LOWER(Location) = LOWER(@location) ORDER BY ActionTimeStamp DESC;
END

GO
/****** Object:  StoredProcedure [dbo].[GetAuthorizedDrivers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAuthorizedDrivers]
	-- Add the parameters for the stored procedure here
	@policyID int,
	@vehicleID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DriverID [driverid] FROM AuthorizedDrivers
	WHERE VehicleID = @vehicleID AND PolicyID = @policyID;

END

GO
/****** Object:  StoredProcedure [dbo].[GetCertFormatting]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date:  01/09/2014
-- Description:	Retrieving a particular certificate format 
-- =============================================
CREATE PROCEDURE [dbo].[GetCertFormatting]
	-- Add the parameters for the stored procedure here
	@formatId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ID, CertificateFormat, title, imported
	FROM CertificateFormatting WHERE ID = @formatId;
END


GO
/****** Object:  StoredProcedure [dbo].[GetCertificateFormats]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 01/09/2014 
-- Description:	Retrieving all vehicle certificate formats.
-- =============================================
CREATE PROCEDURE [dbo].[GetCertificateFormats]
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT ID, title, Imported FROM CertificateFormatting;
END


GO
/****** Object:  StoredProcedure [dbo].[GetCertificateWordingForCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 05/09/2014
-- Description:	Retrieving the certificate wording for a particular company
-- =============================================
CREATE PROCEDURE [dbo].[GetCertificateWordingForCompany]

	@ID int
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT * FROM Wording 
	        LEFT JOIN Company ON Wording.ID = Company.CertificateTemplateID
	WHERE Company.CompanyID = @ID;
END


GO
/****** Object:  StoredProcedure [dbo].[GetCities]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 05/06/14
-- Description:	Pulling city names from the database
-- =============================================
CREATE PROCEDURE [dbo].[GetCities]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   
	SELECT City as CityName FROM Cities;
END


GO
/****** Object:  StoredProcedure [dbo].[GetCompaniesIveInsuredBefore]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCompaniesIveInsuredBefore]
	-- Add the parameters for the stored procedure here
	@cid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT c.CompanyID [ID], c.CompanyName [cname], c.IAJID [iaj]
	FROM Policies p LEFT JOIN PolicyInsured ins ON p.PolicyID = ins.PolicyID
					LEFT JOIN Company c ON ins.CompanyID = c.CompanyID
	WHERE (p.CompanyID = @cid OR p.CompanyCreatedBy = @cid) AND ins.CompanyID IS NOT NULL;


END

GO
/****** Object:  StoredProcedure [dbo].[GetCompanyAddress]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCompanyAddress]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT a.AID, a.AptNumber, a.RoadNumber, rn.RoadName, rt.RoadType, zc.ZipCode, cit.City, co.CountryName, p.PID, p.Parish
	FROM Company c	LEFT JOIN [Address] a ON c.CompanyAddress = a.AID
					LEFT JOIN RoadName rn ON a.RoadName = rn.RID
					LEFT JOIN RoadType rt ON a.RoadType = rt.RTID
					LEFT JOIN ZipCodes zc ON a.ZipCode = zc.ZCID
					LEFT JOIN Cities cit ON a.City = cit.CID
					LEFT JOIN Parishes p ON a.Parish = p.PID
					LEFT JOIN Countries co ON a.Country = co.CID
	WHERE c.CompanyID = @id;
END

GO
/****** Object:  StoredProcedure [dbo].[GetCompanyAdminEmails]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCompanyAdminEmails]
	-- Add the parameters for the stored procedure here
	@CompanyID int 
AS
BEGIN

	SET NOCOUNT ON;
 

	SELECT Emails.email FROM Company -- THE EMAIL ADDRESSES OF THE COMPANY ADMINS
		   LEFT JOIN UserGroups ON UserGroups.Company = @CompanyID AND UserGroups.UserGroup = 'Company Administrator'
		   LEFT JOIN  UserGroupUserAssoc on UserGroupUserAssoc.UGID = UserGroups.UGID 
		   LEFT JOIN Users on Users.UID = UserGroupUserAssoc.UID
	       LEFT JOIN Employees on Employees.EmployeeID = Users.EmployeeID
		   JOIN PeopleToEmails on PeopleToEmails.PeopleID = Employees.PersonID
		  JOIN Emails on Emails.EID = PeopleToEmails.EmailID
	WHERE Company.CompanyID = @CompanyID -- UNION THE USERNAMES OF THE COMPANY ADMINS
	
	UNION (SELECT Users.username [email] FROM Company 
		  LEFT JOIN UserGroups ON UserGroups.Company = @CompanyID AND UserGroups.UserGroup = 'Company Administrator'
		  LEFT JOIN  UserGroupUserAssoc on UserGroupUserAssoc.UGID = UserGroups.UGID 
		  LEFT JOIN Users on Users.UID = UserGroupUserAssoc.UID		
	WHERE Company.CompanyID = @CompanyID );
			



END

GO
/****** Object:  StoredProcedure [dbo].[GetCompanyByIAJID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 08/09/2014
-- Description:	Retrieving a company's information, given the IAJID of the company
-- =============================================
CREATE PROCEDURE [dbo].[GetCompanyByIAJID]
	
	@CompanyIAJID VARCHAR (MAX)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @result bit;
    IF EXISTS (SELECT CompanyID FROM Company WHERE IAJID = @CompanyIAJID)
	
	BEGIN

		SELECT CompanyID, IAJID, CompanyName as companyname, CompanyTypes.[Type] as companytype, faultyloginlimit as floginlimit,
				[Address].AID, Address.RoadNumber, Address.AptNumber, RoadName.RoadName, RoadType.RoadType, 
				ZipCodes.ZipCode, Cities.City, Parishes.Parish, Countries.CountryName, Company.TRN
		FROM Company LEFT OUTER JOIN [CompanyTypes] ON Company.Type = CompanyTypes.ID
					 LEFT OUTER JOIN [Address] ON Company.CompanyAddress = [Address].AID
					 LEFT OUTER JOIN RoadName ON [Address].RoadName = RoadName.RID
					 LEFT OUTER JOIN RoadType ON RoadType.RTID = Address.RoadType
					 LEFT OUTER JOIN ZipCodes ON Address.ZipCode = ZipCodes.ZCID
					 LEFT OUTER JOIN Cities ON Cities.CID = Address.City
					 LEFT OUTER JOIN Parishes ON [Address].Parish = Parishes.PID
					 LEFT OUTER JOIN Countries ON Countries.CID = Address.Country
		WHERE Company.IAJID = @CompanyIAJID;

	END
	

	ELSE 
		BEGIN
		SET @result = 0;
		SELECT @result AS [result];
	END
END

GO
/****** Object:  StoredProcedure [dbo].[GetCompanyByID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCompanyByID]
	-- Add the parameters for the stored procedure here
	@CompanyID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT IAJID, CompanyName as companyname, CompanyTypes.[Type] as companytype, faultyloginlimit as floginlimit,
			[Address].AID, Address.RoadNumber, Address.AptNumber, RoadName.RoadName, RoadType.RoadType, 
			ZipCodes.ZipCode, Cities.City, Parishes.PID, Parishes.Parish, Countries.CountryName, Company.TRN, Company.[Enabled],
			Company.InsurerCode [icode], Company.CompShortening [short]
	FROM Company LEFT OUTER JOIN [CompanyTypes] ON Company.Type = CompanyTypes.ID
				 LEFT OUTER JOIN [Address] ON Company.CompanyAddress = [Address].AID
				 LEFT OUTER JOIN RoadName ON [Address].RoadName = RoadName.RID
				 LEFT OUTER JOIN RoadType ON RoadType.RTID = Address.RoadType
				 LEFT OUTER JOIN ZipCodes ON Address.ZipCode = ZipCodes.ZCID
				 LEFT OUTER JOIN Cities ON Cities.CID = Address.City
				 LEFT OUTER JOIN Parishes ON [Address].Parish = Parishes.PID
				 LEFT OUTER JOIN Countries ON Countries.CID = Address.Country
	WHERE Company.CompanyID = @CompanyID;
	
END

GO
/****** Object:  StoredProcedure [dbo].[GetCompanyByTRN]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCompanyByTRN]
	-- Add the parameters for the stored procedure here
	@trn varchar(14),
	@compID int = 0
AS
BEGIN
	
	SET NOCOUNT ON;

    IF (@compID = 0)
	   SELECT CompanyID [ID] FROM Company WHERE TRN = @trn;

	ELSE
	 SELECT CompanyID [ID] FROM Company WHERE TRN = @trn AND CompanyID != @compID;

END

GO
/****** Object:  StoredProcedure [dbo].[GetCompanyContacts]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 14/10/2014
-- Description:	Pulling the contacts for a company's selected users
-- =============================================
CREATE PROCEDURE [dbo].[GetCompanyContacts]
	-- Add the parameters for the stored procedure here
	@CompID int
AS
BEGIN
	
	SET NOCOUNT ON;

   BEGIN
	SELECT Users.username AS [email], PhoneNumbers.PID, PhoneNumbers.PhoneNo, PhoneNumbers.Ext FROM Company
	       RIGHT JOIN CompanyToPhones ON CompanyToPhones.CompanyID = @compID
		   RIGHT JOIN Users ON Users.UID = CompanyToPhones.UserID 
		   RIGHT JOIN PhoneNumbers ON PhoneNumbers.PID = CompanyToPhones.PhoneID 
     WHERE Company.CompanyID = @CompID;

	END
END


GO
/****** Object:  StoredProcedure [dbo].[GetCompanyCreatedByPolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCompanyCreatedByPolicy]
	-- Add the parameters for the stored procedure here
	@polid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT CompanyCreatedBy [ID] FROM Policies WHERE PolicyID = @polid;
END


GO
/****** Object:  StoredProcedure [dbo].[GetCompanyEmails]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 09/09/2014
-- Description:	Pulling all the emails belonging to a particular company
-- =============================================
CREATE PROCEDURE [dbo].[GetCompanyEmails]
	@compID int
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT Emails.EID, Emails.email, CompanyToEmails.PrimaryEmail
	  FROM Emails LEFT JOIN CompanyToEmails ON CompanyToEmails.EmailID = Emails.EID
				  LEFT JOIN Company ON  Company.CompanyID = CompanyToEmails.CompanyID
	  WHERE Company.CompanyID = @compID;

END


GO
/****** Object:  StoredProcedure [dbo].[GetCompanyId]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 11/06/2014
-- Description:	Retrieving a company Id based on the company name
-- =============================================
CREATE PROCEDURE [dbo].[GetCompanyId]
	-- Add the parameters for the stored procedure here

	@compname varchar(250)
	
AS
BEGIN
	SET NOCOUNT ON;
	SELECT CompanyID FROM Company WHERE CompanyName = @compname; 
END


GO
/****** Object:  StoredProcedure [dbo].[GetCompanyIntermediaries]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 16/09/2014
-- Description:	Retrieving all intermediaries associated with a company
-- =============================================

CREATE PROCEDURE [dbo].[GetCompanyIntermediaries]

	@CompanyID int

AS
BEGIN
	SET NOCOUNT ON;

    SELECT InsurerToIntermediary.Intermediary AS [ID], Company.CompanyName, Company.IAJID 
	       FROM InsurerToIntermediary 
			  JOIN Company ON Company.CompanyID = InsurerToIntermediary.Intermediary 
		   WHERE InsurerToIntermediary.Insurer = @CompanyID;
	 

END

GO
/****** Object:  StoredProcedure [dbo].[GetCompanyNames]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 11/06/2014
-- Description:	Updates a user's information
-- =============================================

CREATE PROCEDURE [dbo].[GetCompanyNames]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	SET NOCOUNT ON;
	SELECT CompanyName FROM Company;

	
END


GO
/****** Object:  StoredProcedure [dbo].[GetCompanyPhoneNums]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 09/09/2014
-- Description:	Pulling all the phpne numbers belonging to a particular company
-- =============================================
CREATE PROCEDURE [dbo].[GetCompanyPhoneNums]
	@compID int
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT PhoneNumbers.PID, PhoneNumbers.PhoneNo, PhoneNumbers.Ext, CompanyToPhones.[Primary]
	  FROM PhoneNumbers LEFT JOIN CompanyToPhones ON CompanyToPhones.PhoneID = PhoneNumbers.PID
				        LEFT JOIN Company ON  Company.CompanyID = CompanyToPhones.CompanyID
	  WHERE Company.CompanyID = @compID;

END


GO
/****** Object:  StoredProcedure [dbo].[GetCompanyTemplateImages]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCompanyTemplateImages]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ID, template_type, image_position, image_location FROM TemplateImages WHERE companyid = @id;
END

GO
/****** Object:  StoredProcedure [dbo].[GetCompanyUsers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 14/10/2014
-- Description:	Retrieving all the users of a company
-- =============================================

CREATE PROCEDURE [dbo].[GetCompanyUsers]
	@compID int
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT Users.UID AS [ID], Users.username, People.TRN, People.FName, People.LName FROM Users 
	                RIGHT JOIN Employees ON Employees.EmployeeID = Users.EmployeeID
	                RIGHT JOIN People ON People.PeopleID = Employees.PersonID
	
	WHERE Users.UID IN ( SELECT Users.UID FROM Employees
						RIGHT JOIN Users ON Users.EmployeeID = Employees.EmployeeID
						WHERE Employees.CompanyID = @compID);

END


GO
/****** Object:  StoredProcedure [dbo].[GetCompanyWithVIN]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date:21/07/2014
-- Description:	Searching for the company that has a particular vehicle under a current policy
-- =============================================
CREATE PROCEDURE [dbo].[GetCompanyWithVIN]
	
	@VIN varchar (50)
AS
BEGIN
	
	SET NOCOUNT ON;

    DECLARE @result bit; DECLARE @compId int;
	IF EXISTS(SELECT VehicleID FROM VehiclesUnderPolicy WHERE VehicleID = (SELECT VehicleID FROM Vehicles WHERE VIN = @VIN))
	      BEGIN

			SET @result = 1; 
			SELECT @compId = CompanyID FROM Policies RIGHT JOIN VehiclesUnderPolicy ON VehiclesUnderPolicy.PolicyID = Policies.PolicyID WHERE VehicleID =(SELECT VehicleID FROM Vehicles WHERE VIN = @VIN);
			SELECT @result AS [result], @compId AS [CompanyID];
		  END
		
	ELSE
	BEGIN
		SET @result = 0;
		SELECT @result as result;
	END
END


GO
/****** Object:  StoredProcedure [dbo].[GetCompCertificates]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 17/09/2014
-- Description: Retrieving vehicle certificates created by a particular company
-- =============================================

CREATE PROCEDURE [dbo].[GetCompCertificates]
@compId int

AS
BEGIN
	SET NOCOUNT ON;

		  SELECT VehicleCertificateID as ID, CompanyID, EndorsementNo, PrintedPaperNo, 
		          RiskItemNo, MarksRegNo, CertificateType, InsuredCode, ExtensionCode, UniqueNum
		  FROM VehicleCertificates WHERE CompanyID = @compId
		  OR VehicleCertificates.CreatedBy IN (SELECT Users.UID FROM Employees RIGHT JOIN Users ON Users.EmployeeID = Employees.EmployeeID WHERE Employees.CompanyID = @compId);
		                           -- OR RiskItemNo IN (SELECT VehiclesUnderPolicy.VehicleID FROM VehiclesUnderPolicy 
								   --RIGHT JOIN Policies ON Policies.CompanyID = @compId WHERE VehiclesUnderPolicy.PolicyID = Policies.PolicyID);
			 
END

GO
/****** Object:  StoredProcedure [dbo].[GetCompCertificatesOnly]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 25/09/2014
-- Description: Retrieving vehicle certificates created by a particular company only
-- =============================================

CREATE PROCEDURE [dbo].[GetCompCertificatesOnly]
@compId int

AS
BEGIN
	SET NOCOUNT ON;

		  SELECT VehicleCertificateID as ID, CompanyID, EffectiveDate, ExpiryDate, EndorsementNo, PrintedPaperNo, 
		          RiskItemNo, MarksRegNo, CertificateType, InsuredCode, ExtensionCode, UniqueNum
		  FROM VehicleCertificates WHERE CompanyID = @compId
		  ANd VehicleCertificates.CreatedBy IN (SELECT Users.UID FROM Employees RIGHT JOIN Users ON Users.EmployeeID = Employees.EmployeeID WHERE Employees.CompanyID = @compId);
		                         
			 
END

GO
/****** Object:  StoredProcedure [dbo].[GetCompCoverNotesOnly]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 25/09/2014
-- Description:	Pulling all vehicle cover notes that are owned by a particular insurer- not including those made in an intermediary
-- =============================================
CREATE PROCEDURE [dbo].[GetCompCoverNotesOnly]
@compId int

AS
BEGIN
	
	SET NOCOUNT ON;

 
	SELECT VehicleCoverNoteID ID, EffectiveDate, period, Vehicles.VIN, Vehicles.VehicleID vehID
	FROM VehicleCoverNote LEFT JOIN Vehicles ON VehicleCoverNote.RiskItemNo = Vehicles.VehicleID 
	     WHERE CompanyID= @compId
		 AND VehicleCoverNote.CreatedBy IN (SELECT Users.UID FROM Employees RIGHT JOIN Users ON Users.EmployeeID = Employees.EmployeeID WHERE Employees.CompanyID = @compId);

END


GO
/****** Object:  StoredProcedure [dbo].[GetCompEmail]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCompEmail]
	@EID int
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT Emails.EID, Emails.email, CompanyToEmails.PrimaryEmail
			FROM Emails LEFT JOIN CompanyToEmails ON Emails.EID = CompanyToEmails.EmailID
			WHERE Emails.EID = @EID;
END


GO
/****** Object:  StoredProcedure [dbo].[GetCompIntermediaryCoverNotes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 18/09/2014
-- Description:	Pulling all vehicle cover notes that are owned by a particular insurer, and created by a particular intermediary company
-- =============================================
CREATE PROCEDURE [dbo].[GetCompIntermediaryCoverNotes]
@compId int, 
@intermediaryCompId int

AS
BEGIN
	
	SET NOCOUNT ON;

 
	SELECT VehicleCoverNoteID ID, EffectiveDate, period, Vehicles.VIN, Vehicles.VehicleID vehID
	FROM VehicleCoverNote LEFT JOIN Vehicles ON VehicleCoverNote.RiskItemNo = Vehicles.VehicleID 
	     WHERE VehicleCoverNote.CompanyID= @compId
		 AND VehicleCoverNote.CreatedBy IN (SELECT Users.UID FROM Employees RIGHT JOIN Users ON Users.EmployeeID = Employees.EmployeeID WHERE Employees.CompanyID = @intermediaryCompId);

END


GO
/****** Object:  StoredProcedure [dbo].[GetCompPhone]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:Chrystal Parchment
-- Create date: 5/10/2014
-- Description:	Pulling a phone number, given phone num Id
-- =============================================
CREATE PROCEDURE [dbo].[GetCompPhone]
	
	@PhoneId int,
	@companyID int


AS
BEGIN
	SET NOCOUNT ON;

	SELECT PhoneNo, Ext, CompanyToPhones.[Primary] FROM PhoneNumbers 
	        LEFT JOIN CompanyToPhones ON CompanyToPhones.PhoneID = PhoneNumbers.PID AND CompanyToPhones.CompanyID = @companyID
    WHERE PhoneNumbers.PID = @PhoneId;


	 
END



GO
/****** Object:  StoredProcedure [dbo].[GetCompUsers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:Chrystal Parchment
-- Create date: 17/09/2014
-- Description:	Pulling all users in the database, asscoiated with a particular user
-- =============================================
CREATE PROCEDURE [dbo].[GetCompUsers]

  @companyId int
	
AS
BEGIN
	
	SET NOCOUNT ON;

           SELECT u.[UID],u.username,u.reset,u.faultylogins,u.lastloginattempt,u.password_reset_force, u.passwordresetrequesttime, u.[active],
				  e.EmployeeID [eid], p.PeopleID [pid], p.TRN, p.FName, p.LName, a.AID, a.RoadNumber [roadno], r.RoadName [rname], rt.RoadType [rtype]
		   FROM Users u LEFT JOIN		Employees e ON u.EmployeeID = e.EmployeeID
						LEFT JOIN		People p ON e.PersonID = p.PeopleID
						LEFT JOIN		[Address] a ON p.[Address] = a.AID
						LEFT OUTER JOIN RoadName r ON a.RoadName = r.RID
						LEFT OUTER JOIN RoadType rt ON a.RoadType = rt.RTID
		   WHERE u.Filter = 0 AND u.Deleted != 1 AND e.CompanyID = @companyId;

END


GO
/****** Object:  StoredProcedure [dbo].[GetCompVehicles]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 17/09/2014
-- Description: Retrieving vehicles created by a particular company, or last edited by said company
-- =============================================

CREATE PROCEDURE [dbo].[GetCompVehicles]
@compId int

AS
BEGIN
	SET NOCOUNT ON;

		  SELECT VehicleID, Make, Model, ModelType, BodyType, Extension, VehicleRegistrationNo, VIN, ChassisNo, HandDrive, Colour, Cylinders, EngineNo, 
		            EngineModified, EngineType, EstimatedAnnualMileage, ExpiryDateofFitness, HPCCUnitType, MainDriver, ReferenceNo, RegistrationLocation,
					RoofType, Seating, SeatingCert, Tonnage, TransmissionType, VehicleYear 
	      FROM Vehicles WHERE LastModifiedBy 
		                        IN (SELECT Users.UID FROM Employees 
								RIGHT JOIN Users ON Users.EmployeeID = Employees.EmployeeID WHERE Employees.CompanyID = @compId)
		                   OR VehicleID 
						        IN (SELECT VehiclesUnderPolicy.VehicleID FROM VehiclesUnderPolicy 
                                RIGHT JOIN Policies ON Policies.CompanyID = @compId WHERE VehiclesUnderPolicy.PolicyID = Policies.PolicyID);
			 
END

GO
/****** Object:  StoredProcedure [dbo].[GetCompVehiclesOnly]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date:  25/09/2014
-- Description: Retrieving vehicles created by a particular company only -not including those made by the intermediaries
-- =============================================

CREATE PROCEDURE [dbo].[GetCompVehiclesOnly]
@compId int

AS
BEGIN
	SET NOCOUNT ON;

		  SELECT VehicleID, Make, Model, ModelType, BodyType, Extension, VehicleRegistrationNo, VIN, HandDrive, Colour, Cylinders, EngineNo, 
		            EngineModified, EngineType, EstimatedAnnualMileage, ExpiryDateofFitness, HPCCUnitType, MainDriver, ReferenceNo, RegistrationLocation,
					RoofType, Seating, SeatingCert, Tonnage, TransmissionType, VehicleYear 
	      FROM Vehicles WHERE CreatedBy 
		                        IN (SELECT Users.UID FROM Employees 
								RIGHT JOIN Users ON Users.EmployeeID = Employees.EmployeeID WHERE Employees.CompanyID = @compId);
		                  -- AND VehicleID 
						  --      IN (SELECT VehiclesUnderPolicy.VehicleID FROM VehiclesUnderPolicy 
                                -- RIGHT JOIN Policies ON Policies.CompanyID = @compId WHERE VehiclesUnderPolicy.PolicyID = Policies.PolicyID 
								--AND VehiclesUnderPolicy.CreatedBy IN (SELECT Users.UID FROM Employees 
								--RIGHT JOIN Users ON Users.EmployeeID = Employees.EmployeeID WHERE Employees.CompanyID = @compId));
			 
END

GO
/****** Object:  StoredProcedure [dbo].[GetContactGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetContactGroup]
	-- Add the parameters for the stored procedure here
	@cgid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ID, name FROM ContactGroups WHERE ID = @cgid;
END


GO
/****** Object:  StoredProcedure [dbo].[GetCountries]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCountries]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT CountryName as Country FROM Countries;
END


GO
/****** Object:  StoredProcedure [dbo].[GetCoutries]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 05/06/14
-- Description:	Pulling country names from the database
-- =============================================
CREATE PROCEDURE [dbo].[GetCoutries]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT CountryName FROM Countries;
	
END


GO
/****** Object:  StoredProcedure [dbo].[GetCover]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 24/06/2014
-- Description:	Checking if a cover exists, and retrieving
-- =============================================

CREATE PROCEDURE [dbo].[GetCover]
	-- Add the parameters for the stored procedure here
	@policyCover varchar (250) 
	--return IsCommercial later, when decided on


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @result bit;

	IF EXISTS (SELECT ID FROM PolicyCovers WHERE Cover = @policyCover)
	BEGIN
	   SET @result = 1;
	   SELECT  @result AS [result], ID AS [ID] FROM PolicyCovers WHERE Cover = @policyCover;
	END

	ELSE 
	BEGIN
	     SET @result = 0;
		 SELECT @result as [result];
	END

END




GO
/****** Object:  StoredProcedure [dbo].[GetCoverByID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCoverByID]
	-- Add the parameters for the stored procedure here
	@cid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Prefix [prefix], Cover, IsCommercialBusiness [commercial] FROM PolicyCovers WHERE ID = @cid;
END


GO
/****** Object:  StoredProcedure [dbo].[GetCoverNoteFormats]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCoverNoteFormats]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT ID, title FROM CoverNoteFormatting;
END


GO
/****** Object:  StoredProcedure [dbo].[GetCoverNotesForVehicle]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCoverNotesForVehicle]
	-- Add the parameters for the stored procedure here
	@vehid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT vcn.VehicleCoverNoteID, vcn.EffectiveDate, vcn.period, vcn.Cancelled, Company.CompanyID, Company.CompanyName
	FROM 
		VehicleCoverNote as vcn LEFT JOIN Vehicles ON vcn.RiskItemNo = Vehicles.VehicleID
								LEFT JOIN Company ON vcn.CompanyID = Company.CompanyID
	WHERE Vehicles.VehicleID = @vehid
	ORDER BY vcn.CreatedOn ASC;
END


GO
/****** Object:  StoredProcedure [dbo].[GetCoverNoteWordingForCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCoverNoteWordingForCompany]
	-- Add the parameters for the stored procedure here
	@ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT w.ID, w.CertificateType, w.CertificateID, w.CertificateInsuredCode, w.ExtensionCode,
		   pc.ID [pcid], pc.Cover [policycover], u.ID [usageid], u.usage,
		   w.ExcludedDriver, w.ExcludedInsured, w.MultipleVehicle, w.RegisteredOwner, w.RestrictedDriver, w.Scheme,
		   w.AuthorizedDrivers, w.LimitsOfUse, w.VehicleDesc, w.VehRegNo, w.PolicyHolders, w.Imported [imported]
	FROM Wording w LEFT JOIN Company c ON w.ID = c.CoverNoteTemplateID
			       LEFT JOIN PolicyCovers pc ON w.PolicyCover = pc.ID
				   LEFT JOIN Usages u ON w.usageid = u.ID
	WHERE c.CompanyID = @ID;
END

GO
/****** Object:  StoredProcedure [dbo].[GetCoversList]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 30/06/2014 
-- Description: Retrieving policy covers
-- =============================================

CREATE PROCEDURE [dbo].[GetCoversList]
	
	@compID int

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @userCompany int;


	SELECT ID, Cover FROM PolicyCovers WHERE PolicyCovers.CompanyID = @compID;
		
END

GO
/****** Object:  StoredProcedure [dbo].[GetCurrentCoverNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCurrentCoverNote]
	-- Add the parameters for the stored procedure here
	@vehid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT VehicleCoverNote.VehicleCoverNoteID
	FROM VehicleCoverNote --LEFT JOIN VehicleCoverNoteState ON VehicleCoverNote.[state] = VehicleCoverNoteState.VCNSID
						  LEFT JOIN Vehicles ON VehicleCoverNote.RiskItemNo = Vehicles.VehicleID
						  LEFT JOIN VehiclesUnderPolicy ON Vehicles.VehicleID = VehiclesUnderPolicy.VehicleID
						  LEFT JOIN Policies ON VehiclesUnderPolicy.PolicyID = Policies.PolicyID
						  --LEFT JOIN PolicyState ON Policies.[state] = PolicyState.PSID
	WHERE VehicleCoverNote.EffectiveDate <= CURRENT_TIMESTAMP
	  AND VehicleCoverNote.ExpiryDateTime >= CURRENT_TIMESTAMP
	  AND Policies.StartDateTime <= CURRENT_TIMESTAMP
	  AND Policies.EndDateTime >= CURRENT_TIMESTAMP
	  AND Vehicles.VehicleID = @vehid;
	 -- AND VehicleCoverNoteState.[state] = 'approved';
	  --AND PolicyState.[state] = 'approved';
END


GO
/****** Object:  StoredProcedure [dbo].[GetDriver]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 07/07/2014
-- Description:	Retrieving a driver from the system
-- =============================================

CREATE PROCEDURE [dbo].[GetDriver]
	-- Add the parameters for the stored procedure here
	@TRN varchar (250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @result bit

	IF EXISTS (SELECT DriverID FROM People RIGHT JOIN Drivers ON Drivers.PersonID = People.PeopleID WHERE People.TRN = @TRN)
	BEGIN
	   SET @result = 1;
	   SELECT @result AS [result], DriverID AS [driverID] FROM People RIGHT JOIN Drivers ON Drivers.PersonID = People.PeopleID WHERE People.TRN = @TRN;
	END

	ELSE 
	BEGIN
	  SET @result = 0;
	  SELECT @result AS [result];
	END
END

GO
/****** Object:  StoredProcedure [dbo].[GetDriverDetails]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetDriverDetails]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  p.PeopleID, p.TRN, p.FName, p.MName, p.LName, d.LicenseNo [license], 
			d.DateLicenseExpires [expiry], d.DateLicenseIssued [issued], d.Class [class]
	FROM Drivers d LEFT JOIN People p ON d.PersonID = p.PeopleID
	WHERE d.DriverID = @id;
END


GO
/****** Object:  StoredProcedure [dbo].[GetDriverFromPerId]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 07/07/2014
-- Description:	Retrieving a driver from the system
-- =============================================

CREATE PROCEDURE [dbo].[GetDriverFromPerId]
	-- Add the parameters for the stored procedure here
	@personID int,
	@type int ,
	@vehId int, 
	@polId int

AS
BEGIN
	SET NOCOUNT ON;

	IF (@type = 1)
	BEGIN

		SELECT People.fname, People.mname, People.lname, People.TRN, AuthorizedDrivers.Relation
		              FROM People 
					  LEFT JOIN Drivers ON Drivers.PersonID = @personID
					  RIGHT JOIN AuthorizedDrivers ON AuthorizedDrivers.DriverID = Drivers.DriverID AND AuthorizedDrivers.VehicleID = @vehId AND AuthorizedDrivers.PolicyID= @polId
					  WHERE people.PeopleID = @PersonID;

	END

	IF (@type = 2)
	BEGIN

	SELECT People.fname, People.mname, People.lname, People.TRN, ExceptedDrivers.Relation
		              FROM People 
					  LEFT JOIN Drivers ON Drivers.PersonID = People.PeopleID
					  RIGHT JOIN ExceptedDrivers ON ExceptedDrivers.DriverID = Drivers.DriverID AND ExceptedDrivers.VehicleID = @vehId AND ExceptedDrivers.PolicyID= @polId
					  WHERE PeopleID = @PersonID;

	END

	IF (@type = 3)
	BEGIN

	SELECT People.fname, People.mname, People.lname, People.TRN, ExcludedDrivers.Relation
		              FROM People 
					  LEFT JOIN Drivers ON Drivers.PersonID = People.PeopleID
					  RIGHT JOIN ExcludedDrivers ON ExcludedDrivers.DriverID = Drivers.DriverID AND ExcludedDrivers.VehicleID = @vehId AND ExcludedDrivers.PolicyID= @polId
					  WHERE PeopleID = @PersonID;


	END
END

GO
/****** Object:  StoredProcedure [dbo].[GetDriversExcepted]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parhment
-- Create date:  25/07/2014
-- Description:	Gets all excepted drivers of a particular driver)
-- =============================================
CREATE PROCEDURE [dbo].[GetDriversExcepted]
	-- Add the parameters for the stored procedure here
	@vin varchar (50)

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT PeopleId, fname, mname, lname, TRN FROM People

	WHERE PeopleId NOT IN (SELECT PeopleId
		FROM Drivers 
		    JOIN People ON People.PeopleID = Drivers.PersonID 
		    JOIN ExceptedDrivers ON ExceptedDrivers.DriverID= Drivers.DriverID 

			WHERE ExceptedDrivers.VehicleID = (SELECT VehicleID FROM Vehicles WHERE VIN = @vin)
			
			);



END
--
			--
--  JOIN ExcludedDrivers ON ExcludedDrivers.DriverID = Drivers.DriverID )

GO
/****** Object:  StoredProcedure [dbo].[GetDriversExcluded]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parhment
-- Create date:  25/07/2014
-- Description:	Gets all excluded drivers of a vehicle
-- =============================================
CREATE PROCEDURE [dbo].[GetDriversExcluded]
	-- Add the parameters for the stored procedure here
	@vin varchar (50)

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT PeopleId, fname, mname, lname, TRN FROM People
	EXCEPT (SELECT PeopleId, fname, mname, lname, TRN 
		FROM Drivers 
		    JOIN People ON People.PeopleID = Drivers.PersonID 
		      JOIN ExcludedDrivers ON ExcludedDrivers.DriverID = Drivers.DriverID


			WHERE ExcludedDrivers.VehicleID = (SELECT VehicleID FROM Vehicles WHERE VIN = @vin));

END


GO
/****** Object:  StoredProcedure [dbo].[GetEmail]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetEmail]
	-- Add the parameters for the stored procedure here
	@EID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Emails.EID, Emails.email, PeopleToEmails.[Primary]
			FROM Emails LEFT JOIN PeopleToEmails ON Emails.EID = PeopleToEmails.EmailID
			WHERE Emails.EID = @EID;
END


GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeIdWithUsername]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 17/06/2014
-- Description:	Retrieving a user's employee Id, given username
-- =============================================
CREATE PROCEDURE [dbo].[GetEmployeeIdWithUsername]
	-- Add the parameters for the stored procedure here
	@username varchar (250)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   
	SELECT EmployeeID FROM Users WHERE username = @username;
END


GO
/****** Object:  StoredProcedure [dbo].[GetEmployeesOfCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetEmployeesOfCompany] 
	-- Add the parameters for the stored procedure here
	@compid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT	e.EmployeeID, p.FName [fname], p.MName [mname], p.LName [lname],
			c.CompanyName, e.Active, u.[UID]
	FROM Employees e LEFT JOIN People p ON e.PersonID = p.PeopleID	
					 LEFT JOIN Company c ON e.CompanyID = c.CompanyID
					 LEFT OUTER JOIN Users u ON e.EmployeeID = u.EmployeeID
	WHERE e.CompanyID = @compid;
		
END


GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeWithEmployeeID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetEmployeeWithEmployeeID]
	-- Add the parameters for the stored procedure here
	@empid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT People.PeopleID, People.FName, People.MName, People.LName, People.TRN,
		   [Address].AID, [Address].RoadNumber, RoadName.RoadName, RoadType.RoadType,
		   ZipCodes.ZipCode, Cities.City, Parishes.Parish, Countries.CountryName, 
		   Company.CompanyID, Company.CompanyName, [Address].AptNumber,
		   Employees.Active
		FROM Employees JOIN People ON Employees.PersonID = People.PeopleID
					   JOIN Company ON Employees.CompanyID = Company.CompanyID
					   JOIN [Address] ON People.[Address] = [Address].AID
					   LEFT OUTER JOIN RoadName ON [Address].RoadName = RoadName.RID
					   LEFT OUTER JOIN RoadType ON [Address].RoadType = RoadType.RTID
					   LEFT OUTER JOIN ZipCodes ON [Address].ZipCode = ZipCodes.ZCID
					   LEFT OUTER JOIN Cities ON [Address].City = Cities.CID
					   LEFT OUTER JOIN Parishes ON [Address].Parish = Parishes.PID
					   LEFT OUTER JOIN Countries ON [Address].Country = Countries.CID
					WHERE Employees.EmployeeID = @empid;
END

GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeWithPersonID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetEmployeeWithPersonID]
	-- Add the parameters for the stored procedure here
	@personID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT EmployeeID, FName, MName, LName, 
		   People.[Address] as aid,
		   [Address].RoadNumber as roadnumber,
		   RoadName.RoadName as roadname,
		   RoadType.RoadType as roadtype, 
		   ZipCodes.ZipCode as zipcode,
		   Cities.City as city, 
		   Countries.CountryName as country
		FROM 
		Employees JOIN People ON Employees.PersonID = People.PeopleID
				  JOIN [Address] ON People.[Address] = [Address].AID
				  JOIN RoadName ON [Address].RoadName = RoadName.RID
				  JOIN RoadType ON [Address].RoadType = RoadType.RTID
				  JOIN ZipCodes ON [Address].ZipCode = ZipCodes.ZCID
				  JOIN Cities ON [Address].City = Cities.CID
				  JOIN Countries ON [Address].Country = Countries.CID
	WHERE Employees.PersonID = @personID;
END


GO
/****** Object:  StoredProcedure [dbo].[GetEnquiry]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetEnquiry]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT license, chassis, RequestedOn, risk, policy, valid FROM Enquiries WHERE ID = @id;
END

GO
/****** Object:  StoredProcedure [dbo].[GetEnquiryRange]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetEnquiryRange]
	-- Add the parameters for the stored procedure here
	@start DATE,
	@end DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ID, license, chassis, RequestedOn, risk, policy
	FROM Enquiries WHERE RequestedOn <= @end AND RequestedOn >= @start;
END

GO
/****** Object:  StoredProcedure [dbo].[GetExceptedDrivers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetExceptedDrivers]
	-- Add the parameters for the stored procedure here
	@policyID int,
	@vehicleID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DriverID [driverid] FROM ExceptedDrivers
	WHERE VehicleID = @vehicleID AND PolicyID = @policyID;

END


GO
/****** Object:  StoredProcedure [dbo].[GetExcludedDrivers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetExcludedDrivers]
	-- Add the parameters for the stored procedure here
	@policyID int,
	@vehicleID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DriverID [driverid] FROM GetExcludedDrivers
	WHERE VehicleID = @vehicleID AND PolicyID = @policyID;

END


GO
/****** Object:  StoredProcedure [dbo].[GetFormatting]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetFormatting]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ID, CoverNoteFormat [format], title, imported
	FROM CoverNoteFormatting WHERE ID = @id;
END


GO
/****** Object:  StoredProcedure [dbo].[GetGenCoverNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetGenCoverNote]
	@id int

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT	[ID], VehicleYear [vehyear], VehicleMake [vehmake], VehicleRegNo [vehregno],
			VehExtension [vehext], MainInsured [maininsured], VehDrivers [drivers], 
			VehicleDesc [vehdesc], PolicyCover [cover], VehModelType [vehmodeltype],
			VehicleModel [vehmodel], authorizeddrivers [authorized], mortgagees [mortgagees],
			LimitsOfUse [limitsofuse], Usage, PolicyNo ,PolicyHolderAddress, CovernoteNo , Period ,
            effectiveTime, expiryTime, effectivedate, expirydate, endorsementNo , bodyType,seating ,
			chassisNo, certificateType, certitificateNo, engineNo, companyCreatedBy, engineNo, HPCC, CertificateCode
	FROM GeneratedCoverNotes
	WHERE VehicleCoverNoteID = @id;
END

GO
/****** Object:  StoredProcedure [dbo].[GetGroupChildren]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetGroupChildren]
	-- Add the parameters for the stored procedure here
	@GID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT UGID, UserGroup, SuperGroup FROM UserGroups JOIN SuperGroupMapping ON UserGroups.UGID = SuperGroupMapping.CGID
			WHERE SuperGroupMapping.PGID = @GID;
END


GO
/****** Object:  StoredProcedure [dbo].[GetGroupsContacts]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetGroupsContacts]
	-- Add the parameters for the stored procedure here
	@cgid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Users.[UID], Users.username FROM ContactUserAssoc JOIN Users ON ContactUserAssoc.UID = Users.UID WHERE contactgroup = @cgid;
END


GO
/****** Object:  StoredProcedure [dbo].[GetHandDriveTypes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parhment
-- Create date: 02/09/2014
-- Description:	Gets all hand-drive types in the system
-- =============================================
CREATE PROCEDURE [dbo].[GetHandDriveTypes]
	
AS
BEGIN
	
	SET NOCOUNT ON;

   
	SELECT HDType FROM VehicleHandDriveLookUp;
END


GO
/****** Object:  StoredProcedure [dbo].[GetInactiveCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 19/08/2014
-- Description:	Retrieving the inactive certificate of a vehicle, under a given policy
-- =============================================
CREATE PROCEDURE [dbo].[GetInactiveCertificate]
	
	@policyID int,
	@VehicleID int
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @CertID int;


	IF EXISTS (SELECT  VehicleCertificateID FROM VehicleCertificates WHERE Vehiclecertificates.PolicyID = @policyID 
	                                                                 AND VehicleCertificates.RiskItemNo= @VehicleID
			                                                         AND (Vehiclecertificates.approved IS NOT NULL AND VehicleCertificates.approved = 0))
     
	 BEGIN
		
		SELECT @CertID = VehicleCertificateID FROM VehicleCertificates WHERE Vehiclecertificates.PolicyID = @policyID 
	                                                                   AND VehicleCertificates.RiskItemNo= @VehicleID
			                                                            AND (Vehiclecertificates.approved IS NOT NULL AND VehicleCertificates.approved = 0);

	   SELECT @CertID as [CertID];

	 END


END

GO
/****** Object:  StoredProcedure [dbo].[GetInsured]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 17/07/2014
-- Description: Getting the insured persons from a policy 
-- =============================================

CREATE PROCEDURE [dbo].[GetInsured]
	-- Add the parameters for the stored procedure here
	@PolicyID int

AS
BEGIN
	SET NOCOUNT ON;

	SELECT People.PeopleID, People.TRN, People.FName, People.MName, People.LName, Address.RoadNumber, RoadName.RoadName, Cities.City, Countries.CountryName
	    FROM 
		PolicyInsured 
		RIGHT JOIN  People ON People.PeopleID = PolicyInsured.PersonID
		LEFT  JOIN  Address On Address.AID = People.Address 
		LEFT  JOIN  RoadName On RoadName.RID = Address.RoadName
		LEFT  JOIN  Cities ON Cities.CID = Address.City
		LEFT  JOIN  Countries ON Countries.CID = Address.City

		WHERE PolicyID = @PolicyID;

END





GO
/****** Object:  StoredProcedure [dbo].[GetInsuredCompanies]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 17/07/2014
-- Description: Getting the insured companies from a policy 
-- =============================================

CREATE PROCEDURE [dbo].[GetInsuredCompanies]
	-- Add the parameters for the stored procedure here
	@PolicyID int

AS
BEGIN
	SET NOCOUNT ON;

	SELECT Company.CompanyID, Company.CompanyName, Company.IAJID, Address.RoadNumber, RoadName.RoadName, Cities.City, Countries.CountryName
	    FROM 
		PolicyInsured 
		RIGHT JOIN Company ON Company.CompanyID = PolicyInsured.CompanyID
		LEFT  JOIN  Address On Address.AID = Company.CompanyAddress
		LEFT  JOIN  RoadName On RoadName.RID = Address.RoadName
		LEFT  JOIN  Cities ON Cities.CID = Address.City
		LEFT  JOIN  Countries ON Countries.CID = Address.City

		WHERE PolicyID = @PolicyID;

END




GO
/****** Object:  StoredProcedure [dbo].[GetInsuredTypes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date:  11/07/2014
-- Description: Retrieving all policy insured types
-- =============================================

CREATE PROCEDURE [dbo].[GetInsuredTypes]
	

AS
BEGIN
	SET NOCOUNT ON;

	SELECT PITID, InsuredType FROM PolicyInsuredType;

END





GO
/****** Object:  StoredProcedure [dbo].[GetInsuredVehicles]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 17/07/2014
-- Description: Getting the insured vehicles under a policy
-- =============================================

CREATE PROCEDURE [dbo].[GetInsuredVehicles]
	-- Add the parameters for the stored procedure here
	@PolicyID int

AS
BEGIN
	SET NOCOUNT ON;

	SELECT VehiclesUnderPolicy.VehicleUsage, Vehicles.VehicleID, Vehicles.VIN, Vehicles.ChassisNo, Vehicles.Make, Vehicles.Model, Vehicles.BodyType
	FROM VehiclesUnderPolicy RIGHT JOIN Vehicles ON Vehicles.VehicleID = VehiclesUnderPolicy.VehicleID
	WHERE PolicyID = @PolicyID AND VehiclesUnderPolicy.Cancelled = 0;


END




GO
/****** Object:  StoredProcedure [dbo].[GetInsurerCode]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetInsurerCode]
	-- Add the parameters for the stored procedure here
	@cid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT InsurerCode [icode] FROM Company WHERE CompanyID = @cid;

END


GO
/****** Object:  StoredProcedure [dbo].[GetInsurerIntermediaries]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetInsurerIntermediaries]
	-- Add the parameters for the stored procedure here
	@cid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT inter.CTID [ID], c.CompanyID [CID], c.CompanyName [cname], inter.[Certificate], inter.CoverNote, inter.PrintLimit
	FROM InsurerToIntermediary inter LEFT JOIN Company c ON inter.Intermediary = c.CompanyID
	WHERE inter.Insurer = @cid;

END

GO
/****** Object:  StoredProcedure [dbo].[GetIntermediaries]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 16/09/2014
-- Description:	Retrieving all intermediaries (brokers and agents)
-- =============================================

CREATE PROCEDURE [dbo].[GetIntermediaries]


AS
BEGIN
	SET NOCOUNT ON;

    SELECT c.CompanyID AS [ID], c.CompanyName, c.IAJID, ct.Type [type], c.[Enabled] [enabled]
	       FROM Company c LEFT JOIN CompanyTypes ct ON c.[Type] = ct.ID
		   WHERE ct.[Type] IN ('Broker','Agent');

END

GO
/****** Object:  StoredProcedure [dbo].[GetIntermediary]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetIntermediary]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT inter.CTID [ID], c.CompanyID [CID], c.CompanyName [cname], inter.[Certificate], inter.CoverNote, inter.PrintLimit, inter.[enabled]
	FROM InsurerToIntermediary inter LEFT JOIN Company c ON inter.Intermediary = c.CompanyID
	WHERE inter.CTID = @id;

END

GO
/****** Object:  StoredProcedure [dbo].[GetIntermediateCompCert]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 18/09/2014
-- Description: Retrieving vehicle certificates created by a particular company
-- =============================================

CREATE PROCEDURE [dbo].[GetIntermediateCompCert]
@compId int,
@intermediaryCompId int 

AS
BEGIN
	SET NOCOUNT ON;

		  SELECT VehicleCertificateID as ID, CompanyID, EffectiveDate, ExpiryDate, EndorsementNo, PrintedPaperNo, 
		          RiskItemNo, MarksRegNo, CertificateType, InsuredCode, ExtensionCode, UniqueNum
		  FROM VehicleCertificates WHERE RiskItemNo IN (SELECT VehiclesUnderPolicy.VehicleID FROM VehiclesUnderPolicy 
								   RIGHT JOIN Policies ON Policies.CompanyID = @compId WHERE VehiclesUnderPolicy.PolicyID = Policies.PolicyID)
								   AND CreatedBy IN (SELECT Users.UID FROM Employees RIGHT JOIN Users ON Users.EmployeeID = Employees.EmployeeID WHERE Employees.CompanyID = @intermediaryCompId);
			 
END

GO
/****** Object:  StoredProcedure [dbo].[GetIntermediateVehicles]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 18/09/2014
-- Description: Retrieving vehicles created by a particular intermediary comapny, asscoiated with a particular insurer
-- =============================================

CREATE PROCEDURE [dbo].[GetIntermediateVehicles]

@compId int,
@intermediaryCompID int

AS
BEGIN
	SET NOCOUNT ON;

		  SELECT VehicleID, Make, Model, ModelType, BodyType, Extension, VehicleRegistrationNo, VIN, HandDrive, Colour, Cylinders, EngineNo, 
		            EngineModified, EngineType, EstimatedAnnualMileage, ExpiryDateofFitness, HPCCUnitType, MainDriver, ReferenceNo, RegistrationLocation,
					RoofType, Seating, SeatingCert, Tonnage, TransmissionType, VehicleYear 
	      FROM Vehicles WHERE VehicleID 
					IN (SELECT VehiclesUnderPolicy.VehicleID FROM VehiclesUnderPolicy 
                    RIGHT JOIN Policies ON Policies.CompanyID = @compId WHERE VehiclesUnderPolicy.PolicyID = Policies.PolicyID) 
					AND LastModifiedBy IN (SELECT Users.UID FROM Employees RIGHT JOIN Users ON Users.EmployeeID = Employees.EmployeeID WHERE Employees.CompanyID = @intermediaryCompID);
			 
END

GO
/****** Object:  StoredProcedure [dbo].[GetKidUID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetKidUID]
	-- Add the parameters for the stored procedure here
	@kid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [uid] FROM RSAKeys WHERE id = @kid;
END

GO
/****** Object:  StoredProcedure [dbo].[GetKPI]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetKPI]
	-- Add the parameters for the stored procedure here
	@cid int,
	@type varchar(100) = null,
	@datemode varchar(100) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT k.id, dm.datemode, k.kpi [count], t.[type], m.mode
	FROM KPI k	LEFT JOIN KPIDateMode dm ON k.datemode = dm.id
				LEFT JOIN KPImode m ON k.mode = m.id
				LEFT JOIN KPItype t ON k.[type] = t.id
	WHERE k.company = @cid AND (dm.datemode = @datemode OR @datemode IS NULL OR @datemode = '') AND (t.[type] = @type OR @type IS NULL OR @type = '');
END

GO
/****** Object:  StoredProcedure [dbo].[GetKPIDateModes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetKPIDateModes]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT datemode [mode] FROM KPIDateMode;
	

END

GO
/****** Object:  StoredProcedure [dbo].[GetKPIModes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetKPIModes]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT mode FROM KPImode;
	

END

GO
/****** Object:  StoredProcedure [dbo].[GetKpis]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetKpis]
	-- Add the parameters for the stored procedure here
	@companyID int,
	@type varchar(100) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT KPI.id, KPItype.[type], KPImode.mode, KPI.kpi [count], KPIDateMode.datemode
	FROM KPI LEFT JOIN KPItype ON KPI.[type] = KPItype.id
			 LEFT JOIN KPImode ON KPI.mode = KPImode.id
			 LEFT JOIN KPIDateMode ON KPI.datemode = KPIDateMode.id
	WHERE company = @companyID AND (@type IS NULL OR @type = '' OR KPItype.[type] = @type)
	ORDER BY KPI.mode DESC;
END

GO
/****** Object:  StoredProcedure [dbo].[GetLastLogin]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 03/10/2014
-- Description:	Retrieving the last time a user was logged in
-- =============================================
CREATE PROCEDURE [dbo].[GetLastLogin]

	@UserId int

AS
BEGIN
	
	SET NOCOUNT ON;

	 SELECT LastSuccessful FROM Users WHERE UID= @UserId;

END


GO
/****** Object:  StoredProcedure [dbo].[GetListofPoliciesWithVehicle]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetListofPoliciesWithVehicle]
	-- Add the parameters for the stored procedure here
	@vehid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PolicyID as ID FROM VehiclesUnderPolicy WHERE VehicleID = @vehid;
END


GO
/****** Object:  StoredProcedure [dbo].[GetMainDriver]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 04/08/2014
-- Description:	Getting the main driver of a vehicle
-- =============================================

CREATE PROCEDURE [dbo].[GetMainDriver]
	-- Add the parameters for the stored procedure here
	@vehID int

AS
BEGIN
	SET NOCOUNT ON;
	
	   SELECT PeopleID, TRN FROM Vehicles 
	   RIGHT JOIN Drivers ON Drivers.DriverID= Vehicles.MainDriver
	   RIGHT JOIN People ON People.PeopleID = Drivers.PersonID WHERE Vehicles.VehicleID = @vehID;

END

GO
/****** Object:  StoredProcedure [dbo].[GetMortgagee]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMortgagee]
	-- Add the parameters for the stored procedure here
	@riskid int,
	@polid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN ((COUNT(*) > 0) AND m.MID IS NOT NULL) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result], m.MID [ID], m.Mortgagee
	FROM VehiclesUnderPolicy vup LEFT JOIN Mortgagees m ON vup.mortgagee = m.MID 
	WHERE vup.PolicyID = @polid AND vup.VehicleID = @riskid
	GROUP BY m.MID, m.Mortgagee;

END

GO
/****** Object:  StoredProcedure [dbo].[GetMyCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMyCompany]
	-- Add the parameters for the stored procedure here
	@UID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Company.CompanyID, Company.CompanyName, Company.TRN FROM Company LEFT JOIN Employees ON Company.CompanyID = Employees.CompanyID
															   LEFT JOIN Users ON Users.EmployeeID = Employees.EmployeeID
												  WHERE Users.UID = @UID;
END

GO
/****** Object:  StoredProcedure [dbo].[GetMyCompanyAdmins]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMyCompanyAdmins]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT admins.UID 
	FROM Users admins LEFT JOIN Employees empadmin ON admins.EmployeeID = empadmin.EmployeeID
					  LEFT JOIN Company companyadmin ON empadmin.CompanyID = companyadmin.CompanyID
					  LEFT JOIN Company companyme ON companyadmin.CompanyID = companyme.CompanyID
					  LEFT JOIN Employees empme ON companyme.CompanyID = empme.CompanyID
					  LEFT JOIN Users me ON empme.EmployeeID = me.EmployeeID
					  LEFT JOIN UserGroupUserAssoc ON admins.UID = UserGroupUserAssoc.UID
					  LEFT JOIN UserGroups ON UserGroupUserAssoc.UGID = UserGroups.UGID
	WHERE me.UID = @uid AND UserGroups.UserGroup = 'Company Administrator';

END


GO
/****** Object:  StoredProcedure [dbo].[GetMyCompPolicies]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 15/09/2014
-- Description: Retrieving policies belonging to a particular company
-- =============================================

CREATE PROCEDURE [dbo].[GetMyCompPolicies]
@compId int

AS
BEGIN
	SET NOCOUNT ON;

		SELECT PolicyCovers.Cover, PolicyID, PolicyCovers.Prefix [PolicyPrefix], Policyno, EDIID
			FROM Policies
			JOIN PolicyCovers ON PolicyCovers.ID = Policies.PolicyCover
			WHERE (Policies.CompanyID = @compId 
				OR Policies.CreatedBy IN (SELECT Users.UID FROM Employees RIGHT JOIN Users ON Users.EmployeeID = Employees.EmployeeID WHERE Employees.CompanyID = @compId))
				AND Policies.Deleted = 0;
			 
END
GO
/****** Object:  StoredProcedure [dbo].[GetMyCompPoliciesOnly]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 15/09/2014
-- Description: Retrieving policies belonging to a particular company only (not made by intermediaries)
-- =============================================

CREATE PROCEDURE [dbo].[GetMyCompPoliciesOnly]
@compId int

AS
BEGIN
	SET NOCOUNT ON;

		SELECT PolicyCovers.Cover, PolicyID, PolicyCovers.prefix [PolicyPrefix], Policyno, EDIID
			FROM Policies
			JOIN PolicyCovers ON PolicyCovers.ID = Policies.PolicyCover
			WHERE Policies.CompanyID = @compId 
			AND Policies.CreatedBy IN (SELECT Users.UID FROM Employees RIGHT JOIN Users ON Users.EmployeeID = Employees.EmployeeID WHERE Employees.CompanyID = @compId)
			AND Policies.Deleted = 0;
			 
END

GO
/****** Object:  StoredProcedure [dbo].[GetMyContactGroups]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMyContactGroups]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ContactGroups.ID, ContactGroups.name 
				FROM ContactGroups WHERE [Owner] = @uid;
END


GO
/****** Object:  StoredProcedure [dbo].[GetMyDrivers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 28/07/2014
-- Description:	Getting the drivers associated with a vehicle
-- =============================================
CREATE PROCEDURE [dbo].[GetMyDrivers]
	-- Add the parameters for the stored procedure here
	@VehID int,
	@policyid int,
	@driverType int

AS
BEGIN
	
	SET NOCOUNT ON;

    DECLARE @result bit; --DECLARE @driverType int;
	DECLARE @main int;

	  SELECT @main = DriverID FROM Vehicles RIGHT JOIN Drivers ON Drivers.DriverID= Vehicles.MainDriver WHERE Vehicles.VehicleID = @vehID;


	--//// If the driver is authorized)//// 
	IF (@driverType = 1)
		BEGIN
			SELECT PersonID, TRN, Relation, FName, LName FROM Vehicles
							   JOIN AuthorizedDrivers ON AuthorizedDrivers.VehicleID = Vehicles.VehicleID
                               JOIN Drivers On Drivers.DriverID= AuthorizedDrivers.DriverID --AND Drivers.DriverID != Vehicles.MainDriver
							   JOIN People ON Drivers.PersonID = People.PeopleID
							   WHERE Vehicles.VehicleID = @VehID AND AuthorizedDrivers.PolicyID = @policyid;
			  			
		END


	 --//// If the driver is excepted)//// 

	   IF (@driverType = 2)
	   BEGIN
		 SELECT PersonID, TRN, Relation, FName, LName FROM Vehicles
							   JOIN ExceptedDrivers ON ExceptedDrivers.VehicleID = Vehicles.VehicleID
                               JOIN Drivers On Drivers.DriverID= ExceptedDrivers.DriverID 
							   JOIN People ON Drivers.PersonID = People.PeopleID
							   WHERE Vehicles.VehicleID = @VehID AND ExceptedDrivers.PolicyID = @policyid;

     END


	  --//// If the driver is excluded)//// 

	   IF (@driverType = 3)
	   BEGIN
		  SELECT PersonID, TRN, Relation, FName, LName FROM Vehicles
							   JOIN ExcludedDrivers ON ExcludedDrivers.VehicleID = Vehicles.VehicleID
                               JOIN Drivers On Drivers.DriverID= ExcludedDrivers.DriverID 
							   JOIN People ON Drivers.PersonID = People.PeopleID
							   WHERE Vehicles.VehicleID = @VehID AND ExcludedDrivers.PolicyID = @policyid;

     END

END -- final end

GO
/****** Object:  StoredProcedure [dbo].[GetMyEmails]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMyEmails]
	-- Add the parameters for the stored procedure here
	@personID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Emails.EID, Emails.email, PeopleToEmails.[Primary]
			FROM PeopleToEmails 
			       LEFT JOIN Emails ON Emails.EID = PeopleToEmails.EmailID
			WHERE PeopleToEmails.PeopleID = @personID;
			
					 
END
GO
/****** Object:  StoredProcedure [dbo].[GetMyEmployee]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMyEmployee]
	-- Add the parameters for the stored procedure here
	@UID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @result as bit;
	IF EXISTS (SELECT [UID] FROM Users WHERE [UID] = @UID)
	BEGIN		
		SET @result = 1;
		SELECT @result as result, Employees.EmployeeID, Company.CompanyID, Company.CompanyName
			FROM Users RIGHT OUTER JOIN Employees ON Users.EmployeeID = Employees.EmployeeID
					   RIGHT OUTER JOIN Company ON Employees.CompanyID = Company.CompanyID
			WHERE Users.[UID] = @UID;
	END
	ELSE 
	BEGIN
		SET @result = 0;
		SELECT @result as result;
	END


END


GO
/****** Object:  StoredProcedure [dbo].[GetMyInsurers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 16/069/2014
-- Description:	Retrieves all the insurance companies that a particular user (an intermediary) has an association with
-- =============================================

CREATE PROCEDURE [dbo].[GetMyInsurers]
	
	@compID int
	
AS
BEGIN
	SET NOCOUNT ON;
	

    SELECT  CompanyID, IAJID, CompanyName
	        FROM InsurerToIntermediary RIGHT JOIN Company ON Company.CompanyID = InsurerToIntermediary.Insurer
			WHERE  InsurerToIntermediary.Intermediary =  @compID;
		
	

END

GO
/****** Object:  StoredProcedure [dbo].[GetMyIntermediaryPolicies]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 18/09/2014
-- Description: Retrieving policies belonging to a particular company, and created by a particular intermediary company
-- =============================================

CREATE PROCEDURE [dbo].[GetMyIntermediaryPolicies]

@compId int,
@intermediaryCompanyId int

AS
BEGIN
	SET NOCOUNT ON;

		SELECT PolicyCovers.Cover, PolicyID, PolicyCovers.Prefix [PolicyPrefix], Policyno, EDIID
			FROM Policies
			JOIN PolicyCovers ON PolicyCovers.ID = Policies.PolicyCover
			WHERE Policies.CompanyID = @compId 
			AND Policies.CreatedBy IN (SELECT Users.UID FROM Employees RIGHT JOIN Users ON Users.EmployeeID = Employees.EmployeeID WHERE Employees.CompanyID = @intermediaryCompanyId)
			AND Policies.Deleted = 0;
			 
END

GO
/****** Object:  StoredProcedure [dbo].[GetMyNotes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMyNotes]
	-- Add the parameters for the stored procedure here
	@uid int,
	@page int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	IF (@page = 0)
	BEGIN
		SELECT DISTINCT children.NID, children.NoteFrom, CONVERT(varchar(MAX),children.Heading) as Heading, CONVERT(varchar(MAX),children.Content) as Content, children.ParentMessage, children.CreatedOn as [timestamp],
			(SELECT STUFF((SELECT username + ';' FROM Users WHERE Users.[UID] IN (SELECT [UID] FROM NotificationsTo WHERE [Notification] = children.NID)  FOR XML PATH('')),1,0, '')) as NoteTo, children.[Read]
		FROM Notifications as children LEFT JOIN Notifications as parents ON children.ParentMessage = parents.NID
										LEFT JOIN NotificationsTo as notes ON children.NID = notes.[Notification]
		WHERE children.NID NOT IN (SELECT parents.NID as PNID FROM Notifications as children INNER JOIN Notifications as parents ON children.ParentMessage = parents.NID)
				AND children.NID IN (SELECT [Notification] FROM NotificationsTo WHERE [UID] = @uid) AND children.Deleted = 0
		ORDER BY children.NID DESC;
	END
	ELSE
	BEGIN
		
		CREATE TABLE #Temp(
			ID int IDENTITY,
			NID int,
			NoteFrom int,
			Heading varchar(MAX),
			Content varchar(MAX),
			ParentMessage int,
			[timestamp] datetime,
			NoteTo varchar(MAX),
			[read] bit
		);
		INSERT INTO #Temp(NID,NoteFrom, Heading, Content, ParentMessage, [timestamp], NoteTo, [read])
		SELECT DISTINCT children.NID, children.NoteFrom, CONVERT(varchar(MAX),children.Heading) as Heading, CONVERT(varchar(MAX),children.Content) as Content, children.ParentMessage, children.CreatedOn as [timestamp],
			(SELECT STUFF((SELECT username + ';' FROM Users WHERE Users.[UID] IN (SELECT [UID] FROM NotificationsTo WHERE [Notification] = children.NID)  FOR XML PATH('')),1,0, '')) as NoteTo, children.[Read]
		FROM Notifications as children LEFT JOIN Notifications as parents ON children.ParentMessage = parents.NID
										LEFT JOIN NotificationsTo as notes ON children.NID = notes.[Notification]
		WHERE children.NID NOT IN (SELECT parents.NID as PNID FROM Notifications as children INNER JOIN Notifications as parents ON children.ParentMessage = parents.NID)
				AND children.NID IN (SELECT [Notification] FROM NotificationsTo WHERE [UID] = @uid) AND children.Deleted = 0
		ORDER BY children.NID DESC;

		SELECT * FROM #Temp WHERE ID >= (@page * 10) - 9 AND ID <= @page *10;

		DROP TABLE #Temp;
	END
END

GO
/****** Object:  StoredProcedure [dbo].[GetMyNotifications]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMyNotifications]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT DISTINCT children.NID, children.NoteFrom, CONVERT(varchar(MAX),children.Heading) as Heading, CONVERT(varchar(MAX),children.Content) as Content, children.ParentMessage,
		(SELECT STUFF((SELECT username + ';' FROM Users WHERE Users.[UID] IN (SELECT [UID] FROM NotificationsTo WHERE [Notification] = children.NID)  FOR XML PATH('')),1,0, '')) as NoteTo
	FROM Notifications as children LEFT JOIN Notifications as parents ON children.ParentMessage = parents.NID
									LEFT JOIN NotificationsTo as notes ON children.NID = notes.[Notification]
	WHERE children.NID NOT IN (SELECT parents.NID as PNID FROM Notifications as children INNER JOIN Notifications as parents ON children.ParentMessage = parents.NID)
			AND children.NID IN (SELECT [Notification] FROM NotificationsTo WHERE [UID] = @uid)
	ORDER BY children.NID DESC;

END


GO
/****** Object:  StoredProcedure [dbo].[GetMySentNotifications]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMySentNotifications]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Notifications WHERE NoteFrom = @uid ORDER BY CreatedOn DESC;


END


GO
/****** Object:  StoredProcedure [dbo].[GetMyTrash]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMyTrash]
	-- Add the parameters for the stored procedure here
	@uid int,
	@page int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	IF (@page = 0)
	BEGIN
		SELECT DISTINCT children.NID, children.NoteFrom, CONVERT(varchar(MAX),children.Heading) as Heading, CONVERT(varchar(MAX),children.Content) as Content, children.ParentMessage, children.CreatedOn as [timestamp],
			(SELECT STUFF((SELECT username + ';' FROM Users WHERE Users.[UID] IN (SELECT [UID] FROM NotificationsTo WHERE [Notification] = children.NID)  FOR XML PATH('')),1,0, '')) as NoteTo, children.[Read]
		FROM Notifications as children LEFT JOIN Notifications as parents ON children.ParentMessage = parents.NID
										LEFT JOIN NotificationsTo as notes ON children.NID = notes.[Notification]
		WHERE children.NID NOT IN (SELECT parents.NID as PNID FROM Notifications as children INNER JOIN Notifications as parents ON children.ParentMessage = parents.NID)
				AND children.NID IN (SELECT [Notification] FROM NotificationsTo WHERE [UID] = @uid)
				AND children.[Deleted] = 1
		ORDER BY children.NID DESC;
	END
	ELSE
	BEGIN
		
		CREATE TABLE #Temp(
			ID int IDENTITY,
			NID int,
			NoteFrom int,
			Heading varchar(MAX),
			Content varchar(MAX),
			ParentMessage int,
			[timestamp] datetime,
			NoteTo varchar(MAX),
			[read] bit
		);
		INSERT INTO #Temp(NID,NoteFrom, Heading, Content, ParentMessage, [timestamp], NoteTo, [read])
		SELECT DISTINCT children.NID, children.NoteFrom, CONVERT(varchar(MAX),children.Heading) as Heading, CONVERT(varchar(MAX),children.Content) as Content, children.ParentMessage, children.CreatedOn as [timestamp],
			(SELECT STUFF((SELECT username + ';' FROM Users WHERE Users.[UID] IN (SELECT [UID] FROM NotificationsTo WHERE [Notification] = children.NID)  FOR XML PATH('')),1,0, '')) as NoteTo, children.[Read]
		FROM Notifications as children LEFT JOIN Notifications as parents ON children.ParentMessage = parents.NID
										LEFT JOIN NotificationsTo as notes ON children.NID = notes.[Notification]
		WHERE children.NID NOT IN (SELECT parents.NID as PNID FROM Notifications as children INNER JOIN Notifications as parents ON children.ParentMessage = parents.NID)
				AND children.NID IN (SELECT [Notification] FROM NotificationsTo WHERE [UID] = @uid)
				AND children.[Deleted] = 1
		ORDER BY children.NID DESC;

		SELECT * FROM #Temp WHERE ID >= (@page * 10) - 9 AND ID <= @page *10;

		DROP TABLE #Temp;

	END

END

GO
/****** Object:  StoredProcedure [dbo].[GetMyTrashCount]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMyTrashCount]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #Temp(
		ID int IDENTITY,
		NID int,
	);
	INSERT INTO #Temp(NID)
	SELECT DISTINCT children.NID
	FROM Notifications as children LEFT JOIN Notifications as parents ON children.ParentMessage = parents.NID
									LEFT JOIN NotificationsTo as notes ON children.NID = notes.[Notification]
	WHERE children.NID NOT IN (SELECT parents.NID as PNID FROM Notifications as children INNER JOIN Notifications as parents ON children.ParentMessage = parents.NID)
			AND children.NID IN (SELECT [Notification] FROM NotificationsTo WHERE [UID] = @uid)
			AND children.[Deleted] = 1
	ORDER BY children.NID DESC;
		
	SELECT COUNT(*) [count] FROM #Temp;

	DROP TABLE #Temp;

END

GO
/****** Object:  StoredProcedure [dbo].[GetMyUserGroups]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMyUserGroups]
	-- Add the parameters for the stored procedure here
	@UID int,
	@gettingid int = 0 -- id of user viewing data
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (@gettingid = 0)
	BEGIN
		IF (SELECT Count(*) FROM UserGroupUserAssoc uga LEFT JOIN UserGroups ug ON uga.UGID = ug.UGID WHERE uga.[UID] = @UID AND ug.[system] = 1) > 0
		BEGIN
			SELECT UserGroupUserAssoc.UGUAID, UserGroups.UGID, Usergroups.UserGroup, UserGroups.Company, UserGroups.[system] FROM UserGroups JOIN UserGroupUserAssoc ON UserGroups.UGID = UserGroupUserAssoc.UGID
							 JOIN Users ON Users.UID = UserGroupUserAssoc.UID
							 WHERE Users.UID = @UID AND ((UserGroups.Company = (SELECT e.CompanyID FROM Employees e LEFT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE u.[UID] = @UID) AND UserGroups.system = 0) OR UserGroups.system = 1);
		END
		ELSE
		BEGIN
			SELECT UserGroupUserAssoc.UGUAID, UserGroups.UGID, Usergroups.UserGroup, UserGroups.Company, UserGroups.[system] FROM UserGroups JOIN UserGroupUserAssoc ON UserGroups.UGID = UserGroupUserAssoc.UGID
							 JOIN Users ON Users.UID = UserGroupUserAssoc.UID
							 WHERE Users.UID = @UID AND UserGroups.Company = (SELECT e.CompanyID FROM Employees e LEFT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE u.[UID] = @UID) AND UserGroups.system = 0;
		END
	END
	ELSE
	BEGIN
		IF (SELECT Count(*) FROM UserGroupUserAssoc uga LEFT JOIN UserGroups ug ON uga.UGID = ug.UGID WHERE uga.[UID] = @gettingid AND ug.[system] = 1) > 0
		BEGIN
			SELECT UserGroupUserAssoc.UGUAID, UserGroups.UGID, Usergroups.UserGroup, UserGroups.Company, UserGroups.[system] FROM UserGroups JOIN UserGroupUserAssoc ON UserGroups.UGID = UserGroupUserAssoc.UGID
							 JOIN Users ON Users.UID = UserGroupUserAssoc.UID
							 WHERE Users.UID = @UID AND ((UserGroups.Company = (SELECT e.CompanyID FROM Employees e LEFT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE u.[UID] = @UID) AND UserGroups.system = 0) OR UserGroups.system = 1);
		END
		ELSE
		BEGIN
			SELECT UserGroupUserAssoc.UGUAID, UserGroups.UGID, Usergroups.UserGroup, UserGroups.Company, UserGroups.[system] FROM UserGroups JOIN UserGroupUserAssoc ON UserGroups.UGID = UserGroupUserAssoc.UGID
							 JOIN Users ON Users.UID = UserGroupUserAssoc.UID
							 WHERE Users.UID = @UID AND UserGroups.Company = (SELECT e.CompanyID FROM Employees e LEFT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE u.[UID] = @UID) AND UserGroups.system = 0;
		END
	END

END


GO
/****** Object:  StoredProcedure [dbo].[GetNotGroupChildren]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetNotGroupChildren]
	-- Add the parameters for the stored procedure here
	@GID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT UGID, UserGroup, SuperGroup FROM UserGroups WHERE UGID NOT IN (SELECT CGID FROM SuperGroupMapping WHERE PGID = @GID);

END


GO
/****** Object:  StoredProcedure [dbo].[GetNotificationChildren]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetNotificationChildren]
	-- Add the parameters for the stored procedure here
	@nid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT children.NID, children.NoteFrom, CONVERT(varchar(MAX),children.Heading) as Heading, CONVERT(varchar(MAX),children.Content) as Content, children.ParentMessage, children.CreatedOn as [timestamp],
		(SELECT STUFF((SELECT username + ';' FROM Users WHERE Users.[UID] IN (SELECT [UID] FROM NotificationsTo WHERE [Notification] = children.NID)  FOR XML PATH('')),1,0, '')) as NoteTo,
		children.Seen as seen, children.[Read] as [read], children.Deleted as deleted
	FROM Notifications as children LEFT JOIN Notifications as parents ON children.ParentMessage = parents.NID
									LEFT JOIN NotificationsTo as notes ON children.NID = notes.[Notification]
	WHERE children.NID = (SELECT ParentMessage FROM Notifications WHERE NID = @nid);
	
END


GO
/****** Object:  StoredProcedure [dbo].[GetNotificationParent]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetNotificationParent]
	-- Add the parameters for the stored procedure here
	@nid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT children.NID, children.NoteFrom, children.Heading, children.Content, children.CreatedOn as [timestamp],
		(SELECT STUFF((SELECT username + ';' FROM Users WHERE Users.[UID] IN (SELECT [UID] FROM NotificationsTo WHERE [Notification] = children.NID)  FOR XML PATH('')),1,0, '')) as NoteTo
		FROM Notifications as children
		WHERE children.NID = (SELECT ParentMessage FROM Notifications WHERE NID = @nid);
END


GO
/****** Object:  StoredProcedure [dbo].[GetNotMyUserGroups]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetNotMyUserGroups]
	-- Add the parameters for the stored procedure here
	@UID int,
	@gettingid int -- id of user viewing data
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	IF (SELECT Count(*) FROM UserGroupUserAssoc uga LEFT JOIN UserGroups ug ON uga.UGID = ug.UGID WHERE uga.[UID] = @gettingid AND ug.[system] = 1 AND ug.UserGroup = 'System Administrator') > 0
	BEGIN
		SELECT UserGroups.UGID, UserGroup, Company, [system]
			FROM UserGroups
			WHERE ((Company = (SELECT e.CompanyID FROM Employees e LEFT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE u.[UID] = @UID)
			AND [system] = 0
			)
			OR [system] = 1)
			AND
			UGID NOT IN 
			(
				SELECT UGID FROM UserGroupUserAssoc WHERE [UID] = @UID
			);
	END
	ELSE 
	BEGIN
		SELECT UserGroups.UGID, UserGroup, Company, [system]
			FROM UserGroups
			WHERE Company = (SELECT e.CompanyID FROM Employees e LEFT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE u.[UID] = @UID)
			AND [system] = 0
			AND
			UGID NOT IN 
			(
				SELECT UGID FROM UserGroupUserAssoc WHERE [UID] = @UID
			);
	END


	
	
END


GO
/****** Object:  StoredProcedure [dbo].[GetParish]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetParish]
	-- Add the parameters for the stored procedure here
	@parish varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PID FROM Parishes WHERE Parish = @parish;
END


GO
/****** Object:  StoredProcedure [dbo].[GetPermissionID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPermissionID]
	-- Add the parameters for the stored procedure here
	@action varchar(50),
	@table varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT UPID as ID FROM UserPermissions WHERE [Table] = @table AND [Action] = @action;
END


GO
/****** Object:  StoredProcedure [dbo].[GetPerson]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPerson]
	-- Add the parameters for the stored procedure here
	@PersonID int,
	@cmpid int = 0
AS
BEGIN

	SET NOCOUNT ON;

		IF (@cmpid =0) -- Pull the person regardless
			BEGIN 
			  SELECT fname, mname, lname, TRN, [Address] [AID] FROM People WHERE PeopleID = @PersonID;
            END
		 ELSE 
			 BEGIN
			    SELECT DISTINCT fname, mname, lname, TRN, [Address] [AID]
			    FROM People 
						LEFT JOIN PolicyInsured polIns ON polIns.PersonID = People.PeopleID
						LEFT JOIN Policies p ON polIns.PolicyID = p.PolicyID
			    WHERE People.PeopleID = @PersonID AND p.CompanyCreatedBy = @cmpid;
			 END
       END


GO
/****** Object:  StoredProcedure [dbo].[GetPersonAddress]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPersonAddress]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT a.AID, a.AptNumber, a.RoadNumber, rn.RoadName, rt.RoadType, zc.ZipCode, cit.City, co.CountryName, p.PID, p.Parish
	FROM People peo	LEFT JOIN [Address] a ON peo.[Address] = a.AID
					LEFT JOIN RoadName rn ON a.RoadName = rn.RID
					LEFT JOIN RoadType rt ON a.RoadType = rt.RTID
					LEFT JOIN ZipCodes zc ON a.ZipCode = zc.ZCID
					LEFT JOIN Cities cit ON a.City = cit.CID
					LEFT JOIN Parishes p ON a.Parish = p.PID
					LEFT JOIN Countries co ON a.Country = co.CID
	WHERE peo.PeopleID = @id;
END

GO
/****** Object:  StoredProcedure [dbo].[GetPersonByTRN]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 02/09/2014
-- Description:	Pulling a person's data using their TRN
-- =============================================
CREATE PROCEDURE [dbo].[GetPersonByTRN]
	
	@TRN varchar (15)
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT PeopleID AS [ID],fname, mname, lname, TRN FROM People WHERE TRN = @TRN;
END


GO
/****** Object:  StoredProcedure [dbo].[getPersonId]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 10/06/2014
-- Description:	Retrieves a personId based on employee Id
-- =============================================

CREATE PROCEDURE [dbo].[getPersonId]
	-- Add the parameters for the stored procedure here
	
	@empId int
	
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT PersonID FROM Employees WHERE EmployeeID = @empId;
	
END


GO
/****** Object:  StoredProcedure [dbo].[GetPersons]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parhment
-- Create date: 14/07/2014
-- Description:	Gets all persons in system
-- =============================================
CREATE PROCEDURE [dbo].[GetPersons]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PeopleId, fname, mname, lname, TRN FROM People;
END


GO
/****** Object:  StoredProcedure [dbo].[GetPersonWithTRN]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPersonWithTRN]
	-- Add the parameters for the stored procedure here
	@TRN varchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @result bit, @user bit, @person bit, @emp bit;
	IF EXISTS(SELECT PeopleID FROM People WHERE TRN = @TRN)
	BEGIN
		IF EXISTS (SELECT [UID] FROM Users JOIN Employees ON Users.EmployeeID = Employees.EmployeeID
										   JOIN People ON Employees.PersonID = People.PeopleID
								WHERE People.TRN = @TRN)
		BEGIN
			IF ((SELECT u.active FROM Users u LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID LEFT JOIN People p ON e.PersonID = p.PeopleID WHERE p.TRN = @TRN AND u.active = 1) = 1)
			BEGIN
				SET @result = 1; SET @user = 1; SET @person = 0; SET @emp = 0;
				SELECT @result as result, @user as [user], @person as person, @emp as emp,
					   PeopleID, fname, mname, lname, [Address].RoadNumber, [Address].AptNumber, Parishes.Parish,
					   RoadName.RoadName, RoadType.RoadType, ZipCodes.ZipCode,
					   Cities.City, Parishes.Parish, Parishes.PID, Countries.CountryName, Users.UID, Employees.CompanyID
				FROM People LEFT OUTER JOIN [Address] ON People.Address = [Address].AID
							LEFT OUTER JOIN RoadName ON RoadName.RID = Address.RoadName
							LEFT OUTER JOIN RoadType ON RoadType.RTID = Address.RoadType
							LEFT OUTER JOIN ZipCodes ON ZipCodes.ZCID = Address.ZipCode
							LEFT OUTER JOIN Cities ON Address.City = Cities.CID
							LEFT OUTER JOIN Parishes ON [Address].Parish = Parishes.PID
							LEFT OUTER JOIN Countries ON Address.Country = Countries.CID
							LEFT OUTER JOIN Employees ON Employees.PersonID = People.PeopleID
							LEFT OUTER JOIN Users ON Users.EmployeeID = Employees.EmployeeID 
				WHERE People.TRN = @TRN AND Users.active = 1;
			END
			ELSE
			BEGIN
				SET @result = 1; SET @user = 0; SET @person = 1; SET @emp = 0;
				SELECT @result as result, @user as [user], @person as person, @emp as emp,
					   PeopleID, fname, mname, lname, [Address].RoadNumber, [Address].AptNumber, Parishes.Parish,
					   RoadName.RoadName, RoadType.RoadType, ZipCodes.ZipCode,
					   Cities.City, Parishes.Parish, Parishes.PID, Countries.CountryName
				FROM People LEFT OUTER JOIN [Address] ON People.Address = [Address].AID
							LEFT OUTER JOIN RoadName ON RoadName.RID = Address.RoadName
							LEFT OUTER JOIN RoadType ON RoadType.RTID = Address.RoadType
							LEFT OUTER JOIN ZipCodes ON ZipCodes.ZCID = Address.ZipCode
							LEFT OUTER JOIN Cities ON Address.City = Cities.CID
							LEFT OUTER JOIN Parishes ON [Address].Parish = Parishes.PID
							LEFT OUTER JOIN Countries ON Address.Country = Countries.CID
				WHERE People.TRN = @TRN;
			END
		END
		ELSE
			IF EXISTS (SELECT EmployeeID FROM Employees JOIN People ON Employees.PersonID = People.PeopleID WHERE TRN = @TRN)
			BEGIN
				SET @result = 1; SET @user = 0; SET @person = 0; SET @emp = 1;
				SELECT @result as result, @user as [user], @person as person, @emp as emp,
					   PeopleID, fname, mname, lname, [Address].RoadNumber, [Address].AptNumber,
					   RoadName.RoadName, RoadType.RoadType, ZipCodes.ZipCode, Parishes.Parish,
					   Cities.City, Parishes.Parish, Parishes.PID, Countries.CountryName, Employees.CompanyID, Employees.EmployeeID
				FROM People JOIN Employees ON People.PeopleID = Employees.PersonID
							LEFT OUTER JOIN [Address] ON People.[Address] = [Address].AID
							LEFT OUTER JOIN RoadName ON RoadName.RID = Address.RoadName
							LEFT OUTER JOIN RoadType ON RoadType.RTID = [Address].RoadType
							LEFT OUTER JOIN ZipCodes ON ZipCodes.ZCID = Address.ZipCode
							LEFT OUTER JOIN Cities ON Address.City = Cities.CID
							LEFT OUTER JOIN Parishes ON [Address].Parish = Parishes.PID
							LEFT OUTER JOIN Countries ON Address.Country = Countries.CID
				WHERE People.TRN = @TRN;
			END
			ELSE
			BEGIN
				SET @result = 1; SET @user = 0; SET @person = 1; SET @emp = 0;
				SELECT @result as result, @user as [user], @person as person, @emp as emp,
					   PeopleID, fname, mname, lname, [Address].RoadNumber, [Address].AptNumber, Parishes.Parish,
					   RoadName.RoadName, RoadType.RoadType, ZipCodes.ZipCode,
					   Cities.City, Parishes.Parish, Parishes.PID, Countries.CountryName
				FROM People LEFT OUTER JOIN [Address] ON People.[Address] = [Address].AID
							LEFT OUTER JOIN RoadName ON RoadName.RID = Address.RoadName
							LEFT OUTER JOIN RoadType ON RoadType.RTID = [Address].RoadType
							LEFT OUTER JOIN ZipCodes ON ZipCodes.ZCID = Address.ZipCode
							LEFT OUTER JOIN Cities ON Address.City = Cities.CID
							LEFT OUTER JOIN Parishes ON [Address].Parish = Parishes.PID
							LEFT OUTER JOIN Countries ON Address.Country = Countries.CID
				WHERE People.TRN = @TRN;
			END
	END
	ELSE
	BEGIN
		SET @result = 0; SET @user = 0; SET @person = 0; SET @emp = 0;
		SELECT @result as result, @user as [user], @person as person , @emp as emp;
	END
END

GO
/****** Object:  StoredProcedure [dbo].[GetPhone]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:Chrystal Parchment
-- Create date: 5/10/2014
-- Description:	Pulling a phone number, given phone num Id
-- =============================================
CREATE PROCEDURE [dbo].[GetPhone]
	
	@PhoneId int


AS
BEGIN
	SET NOCOUNT ON;

	SELECT PhoneNo, Ext FROM PhoneNumbers WHERE PID = @PhoneId;


	 
END



GO
/****** Object:  StoredProcedure [dbo].[GetPolicies]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 01/07/2014
-- Description: Retrieving policies
-- =============================================

CREATE PROCEDURE [dbo].[GetPolicies]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	SET NOCOUNT ON;

		SELECT PolicyCovers.Cover, PolicyID, PolicyCovers.Prefix [prefix], 
			   PolicyCovers.ID [polcovid], Policyno, EDIID
		FROM Policies LEFT JOIN PolicyCovers ON PolicyCovers.ID = Policies.PolicyCover
		WHERE Policies.Deleted = 0;
			 
END

GO
/****** Object:  StoredProcedure [dbo].[GetPolicyCoveredUnder]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 19/02/2015
-- Description:	Retrieving the policy that a vehicle is covered under, within a given time-frame
-- =============================================
CREATE PROCEDURE [dbo].[GetPolicyCoveredUnder]
	
	@vehId int,
	@startDateTime DateTime,
	@endDateTime DateTime
	
	
AS
BEGIN
	
	SET NOCOUNT ON;

	
	SELECT Policies.PolicyID, Policies.CompanyCreatedBy FROM Policies LEFT JOIN VehiclesUnderPolicy ON Policies.PolicyID = VehiclesUnderPolicy.PolicyID 
	       WHERE VehiclesUnderPolicy.VehicleID = @vehId 
			    AND VehiclesUnderPolicy.Cancelled != 1
				AND Policies.Deleted = 0
				AND ((@startDateTime >= Policies.StartDateTime AND @startDateTime <= Policies.EndDateTime) OR (@endDateTime <= Policies.EndDateTime AND @endDateTime >= Policies.StartDateTime 
									                                                OR (@startDateTime <= Policies.StartDateTime AND @endDateTime >= Policies.EndDateTime)));

END


GO
/****** Object:  StoredProcedure [dbo].[GetPolicyCoverUsages]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPolicyCoverUsages]
	-- Add the parameters for the stored procedure here
	@coverid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT u.ID, u.usage FROM Usages u LEFT JOIN UsagesToCovers uc ON u.ID = uc.UsageID
	WHERE uc.CoverID = @coverid;

END


GO
/****** Object:  StoredProcedure [dbo].[GetPolicyCreator]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parhment
-- Create date: 26/09/2014
-- Description:	Retieving the creator of a policy
-- =============================================
CREATE PROCEDURE [dbo].[GetPolicyCreator]
	
	@PolicyId int
AS
BEGIN
	
	SET NOCOUNT ON;
   
	SELECT CreatedBy AS [UserId] FROM Policies WHERE PolicyID = @PolicyId;

END


GO
/****** Object:  StoredProcedure [dbo].[GetPolicyFromId]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 02/07/2014
-- Description: Getting a policy , from policy ID
-- =============================================

CREATE PROCEDURE [dbo].[GetPolicyFromId]
	-- Add the parameters for the stored procedure here
	@PolicyID int

AS
BEGIN
	SET NOCOUNT ON;

	SELECT Policies.CreatedOn, Policies.CreatedBy, PolicyInsuredType.InsuredType, StartDateTime, EndDateTime, PolicyCovers.Prefix [PolicyPrefix],Policyno, Policies.imported,
	       PolicyCover,PolicyCovers.Cover,Company.CompanyID, Company.CompanyName, Policies.Scheme, Policies.CompanyCreatedBy, creator.CompanyName [creatorCompName], 
		   creator.TRN [creatorCompTRN], Policies.billing_address_short [billshort], Policies.mailing_address_short [mailshort],
		   a.AID, a.AptNumber, a.RoadNumber, rn.RoadName, rt.RoadType, zc.ZipCode, c.City, co.CountryName, p.Parish, p.PID,
		   ma.AID [maid], ma.AptNumber [mapt], ma.RoadNumber [marn], mrn.RoadName [mrn], mrt.RoadType [mrt], mzc.ZipCode [mzc], 
		   mc.City [mc], mco.CountryName [mco], mp.Parish [mp], mp.PID [mpid]
	      
		   FROM  Policies 	
		       LEFT JOIN Company ON Company.CompanyID = Policies.CompanyID
			   LEFT JOIN Company creator ON creator.CompanyID = Policies.CompanyCreatedBy
			   LEFT JOIN PolicyInsuredType ON PolicyInsuredType.PITID = Policies.InsuredType
			   LEFT JOIN PolicyCovers ON  PolicyCovers.ID = Policies.PolicyCover
			   LEFT JOIN [Address] a ON Policies.BillingAddress = a.AID
			   LEFT JOIN RoadName rn ON a.RoadName = rn.RID
			   LEFT JOIN RoadType rt ON a.RoadType = rt.RTID
			   LEFT JOIN ZipCodes zc ON a.ZipCode = zc.ZCID
			   LEFT JOIN Cities c ON a.City = c.CID
			   LEFT JOIN Countries co ON a.Country = co.CID
			   LEFT JOIN Parishes p ON a.Parish = p.PID
			   
			   LEFT JOIN [Address] ma ON Policies.MailingAddress = ma.AID
			   LEFT JOIN RoadName mrn ON a.RoadName = mrn.RID
			   LEFT JOIN RoadType mrt ON a.RoadType = mrt.RTID
			   LEFT JOIN ZipCodes mzc ON a.ZipCode = mzc.ZCID
			   LEFT JOIN Cities mc ON a.City = mc.CID
			   LEFT JOIN Countries mco ON a.Country = mco.CID
			   LEFT JOIN Parishes mp ON a.Parish = mp.PID
			  
		   WHERE Policies.PolicyID = @PolicyID;

END





GO
/****** Object:  StoredProcedure [dbo].[GetPolicyHolders]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPolicyHolders]
	-- Add the parameters for the stored procedure here
	@polid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT c.TRN
	FROM Policies p LEFT JOIN PolicyInsured i ON p.PolicyID = i.PolicyID
					LEFT OUTER JOIN Company c ON i.CompanyID = c.CompanyID
	WHERE p.PolicyID = @polid AND c.TRN IS NOT NULL
	UNION
	SELECT per.TRN
	FROM Policies p LEFT JOIN PolicyInsured i ON p.PolicyID = i.PolicyID
					LEFT OUTER JOIN People per ON i.PersonID = per.PeopleID
	WHERE p.PolicyID = @polid AND per.TRN IS NOT NULL;

END

GO
/****** Object:  StoredProcedure [dbo].[GetPolicyHoldersNames]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPolicyHoldersNames]
	-- Add the parameters for the stored procedure here
	@polid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT c.CompanyName [name]
	FROM Policies p LEFT JOIN PolicyInsured i ON p.PolicyID = i.PolicyID
					LEFT OUTER JOIN Company c ON i.CompanyID = c.CompanyID
	WHERE p.PolicyID = @polid AND c.TRN IS NOT NULL
	UNION
	SELECT per.FName + ' ' + per.LName [name]
	FROM Policies p LEFT JOIN PolicyInsured i ON p.PolicyID = i.PolicyID
					LEFT OUTER JOIN People per ON i.PersonID = per.PeopleID
	WHERE p.PolicyID = @polid AND per.TRN IS NOT NULL;

END

GO
/****** Object:  StoredProcedure [dbo].[GetPolicyInsuredsDropDown]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPolicyInsuredsDropDown]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 'c' + CONVERT(varchar(MAX),c.CompanyID) [id], c.CompanyName [name]
	FROM PolicyInsured ins LEFT JOIN Company c ON ins.CompanyID = c.CompanyID
	WHERE ins.PolicyID = @id AND ins.CompanyID IS NOT NULL
	UNION
	SELECT 'p' + CONVERT(varchar(MAX),p.PeopleID) [id], p.LName + ', ' + p.FName [name]
	FROM PolicyInsured ins LEFT JOIN People p ON ins.PersonID = p.PeopleID
	WHERE ins.PolicyID = @id AND ins.PersonID IS NOT NULL;
END

GO
/****** Object:  StoredProcedure [dbo].[GetPolicyPrefix]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPolicyPrefix]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Prefix [prefix] FROM PolicyCovers WHERE ID = @id;
END

GO
/****** Object:  StoredProcedure [dbo].[GetPossibleDrivers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 08/09/2014
-- Description:	Gets all potential drivers than can be added to a vehicle (ie. the ones that aren't already added to the vehicle)
-- =============================================
CREATE PROCEDURE [dbo].[GetPossibleDrivers]
	
	@chassis varchar(50),
	@polID int,
    @cid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	IF (SELECT COUNT(*) FROM Company c LEFT JOIN CompanyTypes ct ON c.[Type] = ct.ID WHERE c.CompanyID = @cid AND ct.[Type] = 'Insurer') > 0
	BEGIN

		SELECT DISTINCT People.PeopleID, People.FName, People.LName, People.TRN	
		FROM PolicyInsured  LEFT JOIN People ON PolicyInsured.PersonID = People.PeopleID
							LEFT JOIN Policies ON PolicyInsured.PolicyID = Policies.PolicyID
		WHERE PolicyInsured.PersonID IS NOT NULL AND Policies.CompanyID = @cid

			UNION SELECT People.PeopleID, People.FName, People.LName, People.TRN FROM Vehicles 
								LEFT JOIN Drivers ON Drivers.DriverID = Vehicles.MainDriver
								LEFT JOIN People  ON People.PeopleID = Drivers.PersonID
			WHERE Vehicles.ChassisNo = @chassis AND Vehicles.MainDriver IS NOT NULL

			EXCEPT (SELECT People.PeopleID, People.FName, People.LName, People.TRN FROM People JOIN Drivers ON Drivers.PersonID = People.PeopleID WHERE DriverID IN (
			SELECT DriverID FROM ExceptedDrivers LEFT JOIN Vehicles ON ExceptedDrivers.VehicleID = Vehicles.VehicleID WHERE Vehicles.ChassisNo = @chassis AND ExceptedDrivers.PolicyID = @polID
			UNION 
			SELECT DriverID FROM ExcludedDrivers LEFT JOIN Vehicles ON ExcludedDrivers.VehicleID = Vehicles.VehicleID WHERE Vehicles.ChassisNo = @chassis AND ExcludedDrivers.PolicyID = @polID
			UNION
			SELECT DriverID FROM AuthorizedDrivers LEFT JOIN Vehicles ON AuthorizedDrivers.VehicleID = Vehicles.VehicleID WHERE Vehicles.ChassisNo = @chassis AND AuthorizedDrivers.PolicyID = @polID
			));
	END 

	ELSE
	   BEGIN

		SELECT DISTINCT People.PeopleID, People.FName, People.LName, People.TRN	
		FROM PolicyInsured  LEFT JOIN People ON PolicyInsured.PersonID = People.PeopleID
							LEFT JOIN Policies ON PolicyInsured.PolicyID = Policies.PolicyID
		WHERE PolicyInsured.PersonID IS NOT NULL AND Policies.CompanyCreatedBy = @cid

			UNION SELECT People.PeopleID, People.FName, People.LName, People.TRN FROM Vehicles 
								LEFT JOIN Drivers ON Drivers.DriverID = Vehicles.MainDriver
								LEFT JOIN People  ON People.PeopleID = Drivers.PersonID
			WHERE Vehicles.ChassisNo = @chassis AND Vehicles.MainDriver IS NOT NULL

			EXCEPT (SELECT People.PeopleID, People.FName, People.LName, People.TRN FROM People JOIN Drivers ON Drivers.PersonID = People.PeopleID WHERE DriverID IN (
			SELECT DriverID FROM ExceptedDrivers LEFT JOIN Vehicles ON ExceptedDrivers.VehicleID = Vehicles.VehicleID WHERE Vehicles.ChassisNo = @chassis AND ExceptedDrivers.PolicyID = @polID
			UNION 
			SELECT DriverID FROM ExcludedDrivers LEFT JOIN Vehicles ON ExcludedDrivers.VehicleID = Vehicles.VehicleID WHERE Vehicles.ChassisNo = @chassis AND ExcludedDrivers.PolicyID = @polID
			UNION
			SELECT DriverID FROM AuthorizedDrivers LEFT JOIN Vehicles ON AuthorizedDrivers.VehicleID = Vehicles.VehicleID WHERE Vehicles.ChassisNo = @chassis AND AuthorizedDrivers.PolicyID = @polID
			));
	END
END

GO
/****** Object:  StoredProcedure [dbo].[GetPrimaryEmail]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPrimaryEmail]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Emails.EID, Emails.email, PeopleToEmails.[Primary]
	FROM Emails LEFT JOIN PeopleToEmails ON Emails.EID = PeopleToEmails.EmailID
				LEFT JOIN People ON PeopleToEmails.PeopleID = People.PeopleID
				LEFT JOIN Employees ON People.PeopleID = Employees.PersonID
				LEFT JOIN Users ON Employees.EmployeeID = Users.EmployeeID
	WHERE Users.UID = @uid;
END


GO
/****** Object:  StoredProcedure [dbo].[GetQuestion]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetQuestion]
	-- Add the parameters for the stored procedure here
	@username varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [UID] [ID], question FROM Users WHERE username = @username;
END


GO
/****** Object:  StoredProcedure [dbo].[GetRequestKey]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetRequestKey]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT [request], (CASE WHEN (COUNT(*) = 1) THEN (CAST (1 as bit)) ELSE (CAST (0 as bit)) END) [exist] FROM RSAKeys WHERE id = @id  GROUP BY [request];

END

GO
/****** Object:  StoredProcedure [dbo].[GetRoadList]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Retrieves all the road names in the system
-- =============================================
CREATE PROCEDURE [dbo].[GetRoadList]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT RoadName FROM RoadName;
	
END


GO
/****** Object:  StoredProcedure [dbo].[GetRoadNumbers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 05/06/14
-- Description:	Pulling road numbers from the database
-- =============================================
CREATE PROCEDURE [dbo].[GetRoadNumbers]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	 SET NOCOUNT ON;

   
	--SELECT Roadnumber FROM Address;
END


GO
/****** Object:  StoredProcedure [dbo].[GetRoadTypeList]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetRoadTypeList]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT RoadType FROM RoadType;
END


GO
/****** Object:  StoredProcedure [dbo].[GetSessionGUID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetSessionGUID]
	-- Add the parameters for the stored procedure here
	@uid int = 0,
	@uname varchar(55) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (@uid != 0) SELECT LoginGUID FROM Users WHERE [UID] = @uid;
	ELSE IF (@uname IS NOT NULL) SELECT LoginGUID FROM Users WHERE username = @uname;

END


GO
/****** Object:  StoredProcedure [dbo].[GetSessionKey]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetSessionKey]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT [session], (CASE WHEN (COUNT(*) = 1) THEN (CAST (1 as bit)) ELSE (CAST (0 as bit)) END) [exist] FROM RSAKeys WHERE id = @id GROUP BY [session];

END

GO
/****** Object:  StoredProcedure [dbo].[GetShortCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetShortCertificate]
	-- Add the parameters for the stored procedure here
	@id int,
	@cmp int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT p.StartDateTime [EffectiveDate], p.EndDateTime [ExpiryDate], c.CompanyName, vcn.PolicyID, v.VehicleRegistrationNo, v.VehicleID, vcn.CertificateType, p.imported
	FROM VehicleCertificates vcn LEFT JOIN Company c ON vcn.CompanyID = c.CompanyID
								 LEFT JOIN Vehicles v ON vcn.RiskItemNo = v.VehicleID
								 LEFT JOIN Policies p ON vcn.PolicyID = p.PolicyID
	WHERE vcn.VehicleCertificateID = @id AND (p.CompanyCreatedBy = @cmp OR @cmp IS NULL OR @cmp = 0);
END

GO
/****** Object:  StoredProcedure [dbo].[GetShortCode]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 22/04/2015
-- Description:	Retrieves the company shortened code
-- =============================================
CREATE PROCEDURE [dbo].[GetShortCode]

	@cid int
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT CompShortening FROM Company WHERE CompanyID = @cid;

END

GO
/****** Object:  StoredProcedure [dbo].[GetShortCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetShortCompany]
	-- Add the parameters for the stored procedure here
	@id int,
	@cmp int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   IF (@cmp =0) -- Pull the company regardless
   BEGIN 
	  SELECT c.CompanyID, c.CompanyName, c.IAJID, c.TRN, ct.Type [CompanyType], c.[Enabled] [enable]
	      FROM Company c LEFT JOIN CompanyTypes ct ON c.Type = ct.ID  
	  WHERE c.CompanyID = @id;
  END

  ELSE --@cmp is an intermediary company, and only companies "insured" (created under policies by that intermediary) should be pulled
     BEGIN

	  SELECT DISTINCT c.CompanyID, c.CompanyName, c.IAJID, c.TRN, ct.Type [CompanyType], c.[Enabled] [enable]
	      FROM Company c LEFT JOIN CompanyTypes ct ON c.Type = ct.ID  
		                 LEFT JOIN PolicyInsured polIns ON polIns.CompanyID = c.CompanyID
				         LEFT JOIN Policies p ON polIns.PolicyID = p.PolicyID
	  WHERE c.CompanyID = @id AND p.CompanyCreatedBy = @cmp;
     END

END


GO
/****** Object:  StoredProcedure [dbo].[GetShortCoverNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetShortCoverNote]
	-- Add the parameters for the stored procedure here
	@id int,
	@cmp int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT EffectiveDate [effective], ExpiryDateTime [expiry], period, VehicleCoverNote.Cancelled, approved, CompanyName, alterations, ManualCoverNoteNo [covernoteno], Policies.imported
	FROM VehicleCoverNote LEFT JOIN Company ON VehicleCoverNote.CompanyID = Company.CompanyID
	                      LEFT JOIN Policies ON VehicleCoverNote.PolicyID = Policies.PolicyID
	WHERE VehicleCoverNoteID = @id AND (Policies.CompanyCreatedBy = @cmp OR @cmp = 0 OR @cmp IS NULL);
END

GO
/****** Object:  StoredProcedure [dbo].[GetShortPolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetShortPolicy]
	-- Add the parameters for the stored procedure here
	@id int,
	@cmp int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT StartDateTime [start], EndDateTime [end], PolicyInsuredType.InsuredType [insuredtype], PolicyCovers.Prefix [prefix], Policyno [number]
	FROM Policies LEFT JOIN PolicyInsuredType ON Policies.InsuredType = PolicyInsuredType.PITID
				  LEFT JOIN PolicyCovers ON Policies.PolicyCover = PolicyCovers.ID
	WHERE PolicyID = @id AND (Policies.CompanyCreatedBy = @cmp OR @cmp IS NULL OR @cmp = 0);
END

GO
/****** Object:  StoredProcedure [dbo].[GetShortUser]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetShortUser]
	-- Add the parameters for the stored procedure here
	@id int,
	@cmp int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT u.username, e.EmployeeID [eid], p.PeopleID [pid], p.FName [fname], p.LName [lname], p.TRN [trn], c.CompanyName [companyname], u.active
	FROM Users u LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID
				 LEFT JOIN Company c ON e.CompanyID = c.CompanyID
				 LEFT JOIN People p ON e.PersonID = p.PeopleID
	WHERE u.UID = @id AND (c.CompanyID = @cmp OR @cmp IS NULL OR @cmp = 0);
END


GO
/****** Object:  StoredProcedure [dbo].[GetShortVehicle]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetShortVehicle]
	-- Add the parameters for the stored procedure here
	@id int,
	@cmp int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
   IF (@cmp IS NULL) -- Pull the vehicle regardless
       BEGIN
		SELECT v.VehicleID [id], v.VIN, v.VehicleRegistrationNo [regno], v.Make, v.Model
		FROM Vehicles v
		WHERE v.VehicleID = @id ;
	END 
	ELSE --@cmp is an intermediary company, and only vehicles "insured" (created under policies by that intermediary) should be pulled
	  BEGIN
		  SELECT v.VehicleID [id], v.VIN, v.VehicleRegistrationNo [regno], v.Make, v.Model
			FROM Vehicles v LEFT JOIN VehiclesUnderPolicy vup ON v.VehicleID = vup.VehicleID
							LEFT JOIN Policies p ON vup.PolicyID = p.PolicyID
			WHERE v.VehicleID = @id AND p.CompanyCreatedBy = @cmp;
	  END
END

GO
/****** Object:  StoredProcedure [dbo].[GetStockUserGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetStockUserGroup]
	-- Add the parameters for the stored procedure here
	@groupname varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT UGID FROM UserGroups WHERE UserGroup = @groupname AND stock = 1;
END


GO
/****** Object:  StoredProcedure [dbo].[GetSystemGroups]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetSystemGroups]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT UGID, UserGroup
	FROM UserGroups WHERE [system] = 1;
END


GO
/****** Object:  StoredProcedure [dbo].[GetTemplateImage]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetTemplateImage]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT template_type, image_position, image_location, companyid FROM TemplateImages WHERE ID = @id;
END

GO
/****** Object:  StoredProcedure [dbo].[GetThisActiveCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 14/08/2014
-- Description:	Retrieving a Vehicle's active certificate under a given time frame
-- =============================================
CREATE PROCEDURE [dbo].[GetThisActiveCertificate]
	
	@startDateTime varchar (250),
	@endDateTime varchar (250),
	@VehicleID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @CertId int;

	DECLARE @smallEnddatetime smalldatetime = @endDateTime;
	DECLARE @end datetime = @smallEnddatetime;

	DECLARE @smallstartdatetime smalldatetime = @startDateTime;
	DECLARE @start datetime = @smallstartdatetime;



   IF EXISTS (SELECT  VehicleCertificateID FROM VehicleCertificates 
			          LEFT JOIN Policies ON Policies.PolicyID = VehicleCertificates.PolicyID  AND Policies.Deleted = 0 
					  AND ((@start >= Policies.StartDateTime AND @start <= Policies.EndDateTime) 
					                                         OR (@end <= Policies.EndDateTime AND @end >= Policies.StartDateTime 
									                         OR (@start <= Policies.StartDateTime AND @end >= Policies.EndDateTime)))
		              WHERE VehicleCertificates.RiskItemNo = @VehicleID AND (VehicleCertificates.Cancelled IS NULL OR VehicleCertificates.Cancelled = 0)
                                                                        AND VehicleCertificates.approved IS NOT NULL AND approved= 1)
	 BEGIN
		SELECT @CertId = VehicleCertificateID FROM VehicleCertificates 
			          LEFT JOIN Policies ON Policies.PolicyID = VehicleCertificates.PolicyID  AND Policies.Deleted = 0 
					  AND ((@start >= Policies.StartDateTime AND @start <= Policies.EndDateTime) 
					                                         OR (@end <= Policies.EndDateTime AND @end >= Policies.StartDateTime 
									                         OR (@start <= Policies.StartDateTime AND @end >= Policies.EndDateTime)))
		              WHERE VehicleCertificates.RiskItemNo = @VehicleID AND (VehicleCertificates.Cancelled IS NULL OR VehicleCertificates.Cancelled = 0)
                                                                         AND VehicleCertificates.approved IS NOT NULL AND approved= 1;

		SELECT @CertId as [CertID];
	 END

END

GO
/****** Object:  StoredProcedure [dbo].[GetThisActiveCoverNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 14/08/2014
-- Description:	Retrieving a Vehicle's active cover note under a given time frame
-- =============================================
CREATE PROCEDURE [dbo].[GetThisActiveCoverNote]
	
	@startDateTime varchar (250),
	@endDateTime varchar (250),
	@VehicleID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @CoverId int;

	DECLARE @smallEnddatetime smalldatetime = @endDateTime;
	DECLARE @end datetime = @smallEnddatetime;

	DECLARE @smallstartdatetime smalldatetime = @startDateTime;
	DECLARE @start datetime = @smallstartdatetime;



  IF EXISTS (SELECT  VehicleCoverNoteID FROM VehicleCoverNote 
			                            LEFT JOIN Policies ON Policies.PolicyID = VehicleCoverNote.PolicyID  AND Policies.Deleted = 0 
						                          --AND (CURRENT_TIMESTAMP >= Policies.StartDateTime AND CURRENT_TIMESTAMP <= Policies.EndDateTime)
											       AND ((@start >= Policies.StartDateTime AND @start <= Policies.EndDateTime) 
												   OR (@end <= Policies.EndDateTime AND @end >= Policies.StartDateTime 
									               OR (@start <= Policies.StartDateTime AND @end >= Policies.EndDateTime)))
		       WHERE VehicleCoverNote.RiskItemNo = @VehicleID AND (VehicleCoverNote.Cancelled IS NULL OR VehicleCoverNote.Cancelled = 0) 
			                                                  AND VehicleCoverNote.approved= 1
															  AND (CURRENT_TIMESTAMP >= VehicleCoverNote.EffectiveDate AND CURRENT_TIMESTAMP <= VehicleCoverNote.ExpiryDateTime)) 
	
	 BEGIN
		SELECT @CoverId = VehicleCoverNoteID FROM VehicleCoverNote 
			                                LEFT JOIN Policies ON Policies.PolicyID = VehicleCoverNote.PolicyID  AND Policies.Deleted = 0 
						                          --AND (CURRENT_TIMESTAMP >= Policies.StartDateTime AND CURRENT_TIMESTAMP <= Policies.EndDateTime)
											       AND ((@start >= Policies.StartDateTime AND @start <= Policies.EndDateTime) OR (@end <= Policies.EndDateTime AND @end >= Policies.StartDateTime 
									               OR (@start <= Policies.StartDateTime AND @end >= Policies.EndDateTime)))
		       WHERE VehicleCoverNote.RiskItemNo = @VehicleID AND (VehicleCoverNote.Cancelled IS NULL OR VehicleCoverNote.Cancelled = 0) 
			                                                  AND VehicleCoverNote.approved= 1
															  AND (CURRENT_TIMESTAMP >= VehicleCoverNote.EffectiveDate AND CURRENT_TIMESTAMP <= VehicleCoverNote.ExpiryDateTime);

		SELECT @CoverId as [CoverID];
	 END

END

GO
/****** Object:  StoredProcedure [dbo].[GetThisCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 14/08/2014
-- Description:	Retrieving a vehicle certificate, give the certificate ID
-- =============================================
CREATE PROCEDURE [dbo].[GetThisCertificate]
	@VehicleCertID int
AS
BEGIN
	
	SET NOCOUNT ON;

    
	SELECT  Vehicles.VIN, Vehicles.Make, Vehicles.ChassisNo, Vehicles.Model, Vehicles.VehicleYear, Vehicles.ReferenceNo, Vehicles.VehicleRegistrationNo,PeopleID, People.TRN, People.FName, 
	        People.LName, Policies.PolicyID,PolicyCovers.Cover, Policies.Policyno,VehicleCertificates.VehicleCertificateID  AS [ID], Company.CompanyID, Company.CompanyName,
	        Policies.StartDateTime, Policies.EndDateTime ,Policies.imported, VehicleCertificates.LastPrintedOn, VehicleCertificates.EndorsementNo, VehicleCertificates.PrintedPaperNo, 
			VehicleCertificates.RiskItemNo, VehicleCertificates.approvedon, VehicleCertificates.approvedby, VehicleCertificates.MarksRegNo, VehicleCertificates.Cancelled,
			VehicleCertificates.CancelledBy, VehicleCertificates.CancelledOn, VehicleCertificates.Scheme, VehicleCertificates.CertificateNo, Vehicles.Seating, Vehicles.EngineNo,
			VehicleCertificates.WordingTemplate, CertificateType,InsuredCode,ExtensionCode,UniqueNum, Usages.usage, Vehicles.HPCCUnitType, PrintCount, PrintCountInsurer, 
			VehicleCertificates.EffectiveDate, VehicleCertificates.ExpiryDate, VehicleCertificates.FirstPrintedByFlat, VehicleCertificates.LastPrintedByFlat

	        FROM VehicleCertificates
		      LEFT JOIN Vehicles ON Vehicles.VehicleID = VehicleCertificates.RiskItemNo
		      LEFT JOIN Drivers ON Drivers.DriverID = Vehicles.MainDriver 
	          LEFT JOIN People ON People.PeopleID = Drivers.PersonID

			  LEFT JOIN Policies ON VehicleCertificates.PolicyID = Policies.PolicyID
			  LEFT JOIN PolicyCovers ON PolicyCovers.ID = Policies.PolicyCover

			  LEFT JOIN VehiclesUnderPolicy ON VehiclesUnderPolicy.PolicyID = Policies.PolicyID AND VehiclesUnderPolicy.VehicleID= VehicleCertificates.RiskItemNo
			  LEFT JOIN Usages ON Usages.ID = VehiclesUnderPolicy.VehicleUsage

			  LEFT JOIN Company ON Company.CompanyID = VehicleCertificates.CompanyID

		   WHERE VehicleCertificates.VehicleCertificateID = @VehicleCertID;
END

GO
/****** Object:  StoredProcedure [dbo].[GetThisGeneratedCert]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 18/11/2014
-- Description:	Retrieving a vehicle certificate wording, give the certificate ID
-- =============================================
CREATE PROCEDURE [dbo].[GetThisGeneratedCert]
	@VehicleCertID int
AS
BEGIN
	
	SET NOCOUNT ON;

    
	SELECT	GCID [ID], VehicleYear [vehyear], VehicleMake [vehmake], VehicleRegNo [vehregno],
			VehExtension [vehext], MainInsured [maininsured], VehDrivers [drivers], 
			VehicleDesc [vehdesc], PolicyCover [cover], VehModelType [vehmodeltype],
			VehicleModel [vehmodel], authorizeddrivers [authorized], mortgagees [mortgagees],
			LimitsOfUse [limitsofuse], Usage, PolicyNo ,PolicyHolderAddress, CovernoteNo, Period, 
            effectiveTime, expiryTime, effectivedate, expirydate, endorsementNo , bodyType,seating, 
			chassisNo, certificateType, certitificateNo, engineNo, HPCC, CertificateCode
	 FROM GeneratedCertificate WHERE VehicleCertificateID = @VehicleCertID;
END

GO
/****** Object:  StoredProcedure [dbo].[GetThisNotification]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetThisNotification]
	-- Add the parameters for the stored procedure here
	@nid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT children.NID, children.NoteFrom, CONVERT(varchar(MAX),children.Heading) as Heading, CONVERT(varchar(MAX),children.Content) as Content, children.ParentMessage, children.CreatedOn as [timestamp],
		(SELECT STUFF((SELECT username + ';' FROM Users WHERE Users.[UID] IN (SELECT [UID] FROM NotificationsTo WHERE [Notification] = children.NID)  FOR XML PATH('')),1,0, '')) as NoteTo,
		children.Seen as seen, children.[Read] as [read], children.Deleted as deleted
	FROM Notifications as children LEFT JOIN Notifications as parents ON children.ParentMessage = parents.NID
									LEFT JOIN NotificationsTo as notes ON children.NID = notes.[Notification]
	WHERE children.NID = @nid;


END


GO
/****** Object:  StoredProcedure [dbo].[GetThisVehicleActiveCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 14/08/2014
-- Description:	Retrieving a Vehicle's active certificate 
-- =============================================
CREATE PROCEDURE [dbo].[GetThisVehicleActiveCertificate]
	@VehicleID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @CertId int;

   IF EXISTS (SELECT  VehicleCertificateID FROM VehicleCertificates 
			          LEFT JOIN Policies ON Policies.PolicyID = VehicleCertificates.PolicyID  AND Policies.Deleted = 0 
					  AND (CURRENT_TIMESTAMP >= Policies.StartDateTime AND CURRENT_TIMESTAMP <= Policies.EndDateTime)
		              WHERE VehicleCertificates.RiskItemNo = @VehicleID AND (VehicleCertificates.Cancelled IS NULL OR VehicleCertificates.Cancelled = 0))
     BEGIN
		SELECT @CertId = VehicleCertificateID FROM VehicleCertificates 
			                                LEFT JOIN Policies ON Policies.PolicyID = VehicleCertificates.PolicyID  AND Policies.Deleted = 0 
						                    AND (CURRENT_TIMESTAMP >= Policies.StartDateTime AND CURRENT_TIMESTAMP <= Policies.EndDateTime)
		       WHERE VehicleCertificates.RiskItemNo = @VehicleID AND (VehicleCertificates.Cancelled IS NULL OR VehicleCertificates.Cancelled = 0);

		SELECT @CertId as [CertID];
	 END


	 ELSE IF EXISTS (SELECT  VehicleCertificateID FROM VehicleCertificates WHERE VehicleCertificates.RiskItemNo = @VehicleID 
	                 AND  Vehiclecertificates.state= (SELECT VCNSID FROM VehicleCoverNoteState WHERE state='approved'))
	 BEGIN
	    SELECT @CertId = VehicleCertificateID FROM VehicleCertificates WHERE VehicleCertificates.RiskItemNo = @VehicleID 
		              AND  Vehiclecertificates.state= (SELECT VCNSID FROM VehicleCoverNoteState WHERE state='approved');
		SELECT @CertId as [CertID];
	 END


END

GO
/****** Object:  StoredProcedure [dbo].[GetThisVehicleCertificates]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 14/08/2014
-- Description:	Retrieving a Vehicle's certificates
-- =============================================
CREATE PROCEDURE [dbo].[GetThisVehicleCertificates]
	@VehicleID int
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT  Vehicles.VIN, Vehicles.Make, Vehicles.Model, People.PeopleID, People.TRN, People.FName, People.FName, VehicleCertificates.PolicyID, PolicyCovers.Cover, 
			VehicleCertificates.VehicleCertificateID, VehicleCertificates.CompanyID, VehicleCertificates.EffectiveDate, 
			VehicleCertificates.ExpiryDate, VehicleCertificates.LastPrintedOn, VehicleCertificates.EndorsementNo, VehicleCertificates.PrintedPaperNo, 
			VehicleCertificates.RiskItemNo, VehicleCertificates.SignedAt, VehicleCertificates.SignedBy, VehicleCertificates.MarksRegNo, VehicleCertificates.Cancelled,
			VehicleCertificates.CancelledBy, VehicleCertificates.CancelledOn, VehicleCertificates.Usage, VehicleCertificates.Scheme, CertificateType,
			InsuredCode,ExtensionCode,UniqueNum

	       FROM VehicleCertificates
		      RIGHT JOIN Vehicles ON Vehicles.VehicleID = VehicleCertificates.RiskItemNo
		      RIGHT JOIN Drivers ON Drivers.DriverID = Vehicles.MainDriver 
	          RIGHT JOIN People ON People.PeopleID = Drivers.PersonID
		      RIGHT JOIN Policies ON Policies.PolicyID = VehicleCertificates.PolicyID
			
			  RIGHT JOIN PolicyCovers ON PolicyCovers.ID = Policies.PolicyCover


		  WHERE VehicleCertificates.RiskItemNo = @VehicleID;
END


GO
/****** Object:  StoredProcedure [dbo].[GetTransmissionTypes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parhment
-- Create date: 02/09/2014
-- Description:	Gets all transmission types in the system
-- =============================================
CREATE PROCEDURE [dbo].[GetTransmissionTypes]
	
AS
BEGIN
	
	SET NOCOUNT ON;

   
	SELECT TransmissionType FROM TransmissionTypeLookUp;
END


GO
/****** Object:  StoredProcedure [dbo].[GetUnnewNotifications]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUnnewNotifications]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT children.NID, children.NoteFrom, CONVERT(varchar(MAX),children.Heading) as Heading, CONVERT(varchar(MAX),children.Content) as Content, children.ParentMessage, children.CreatedOn as [timestamp],
		(SELECT STUFF((SELECT username + ';' FROM Users WHERE Users.[UID] IN (SELECT [UID] FROM NotificationsTo WHERE [Notification] = children.NID)  FOR XML PATH('')),1,0, '')) as NoteTo
	FROM Notifications as children LEFT JOIN Notifications as parents ON children.ParentMessage = parents.NID
									LEFT JOIN NotificationsTo as notes ON children.NID = notes.[Notification]
	WHERE children.NID NOT IN (SELECT parents.NID as PNID FROM Notifications as children INNER JOIN Notifications as parents ON children.ParentMessage = parents.NID)
			AND children.NID IN (SELECT [Notification] FROM NotificationsTo WHERE [UID] = @uid)
			AND children.[New] = 0
	ORDER BY children.NID DESC;
	
	UPDATE Notifications SET New = 1 WHERE NID IN (SELECT [Notification] FROM NotificationsTo WHERE [UID] = @uid) AND New = 0;
END


GO
/****** Object:  StoredProcedure [dbo].[GetUnreadNotifications]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUnreadNotifications]
	-- Add the parameters for the stored procedure here
	@uid int,
	@page int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF(@page = 0)
	BEGIN
		SELECT DISTINCT children.NID, children.NoteFrom, CONVERT(varchar(MAX),children.Heading) as Heading, CONVERT(varchar(MAX),children.Content) as Content, children.ParentMessage, children.CreatedOn as [timestamp],
			(SELECT STUFF((SELECT username + ';' FROM Users WHERE Users.[UID] IN (SELECT [UID] FROM NotificationsTo WHERE [Notification] = children.NID)  FOR XML PATH('')),1,0, '')) as NoteTo
		FROM Notifications as children LEFT JOIN Notifications as parents ON children.ParentMessage = parents.NID
										LEFT JOIN NotificationsTo as notes ON children.NID = notes.[Notification]
		WHERE children.NID NOT IN (SELECT parents.NID as PNID FROM Notifications as children INNER JOIN Notifications as parents ON children.ParentMessage = parents.NID)
				AND children.NID IN (SELECT [Notification] FROM NotificationsTo WHERE [UID] = @uid)
				AND children.[Read] = 0 AND children.[Deleted] = 0
		ORDER BY children.NID DESC;
	END
	ELSE
	BEGIN
		--DECLARE @page int; SET @page = 2;
		CREATE TABLE #Temp(
			ID int IDENTITY,
			NID int,
			NoteFrom int,
			Heading varchar(MAX),
			Content varchar(MAX),
			ParentMessage int,
			[timestamp] datetime,
			NoteTo varchar(MAX)
		);
		INSERT INTO #Temp(NID,NoteFrom, Heading, Content, ParentMessage, [timestamp], NoteTo)
		SELECT DISTINCT TOP (@page * 10) children.NID, children.NoteFrom, CONVERT(varchar(MAX),children.Heading) as Heading, CONVERT(varchar(MAX),children.Content) as Content, children.ParentMessage, children.CreatedOn as [timestamp],
			(SELECT STUFF((SELECT username + ';' FROM Users WHERE Users.[UID] IN (SELECT [UID] FROM NotificationsTo WHERE [Notification] = children.NID)  FOR XML PATH('')),1,0, '')) as NoteTo
		FROM Notifications as children LEFT JOIN Notifications as parents ON children.ParentMessage = parents.NID
										LEFT JOIN NotificationsTo as notes ON children.NID = notes.[Notification]
		WHERE children.NID NOT IN (SELECT parents.NID as PNID FROM Notifications as children INNER JOIN Notifications as parents ON children.ParentMessage = parents.NID)
				AND children.NID IN (SELECT [Notification] FROM NotificationsTo WHERE [UID] = @uid)
				AND children.[Read] = 0 AND children.[Deleted] = 0
		ORDER BY children.NID DESC;
		
		SELECT * FROM #Temp WHERE ID >= (@page * 10) - 9 AND ID <= @page *10;

		DROP TABLE #Temp;


	END




END

GO
/****** Object:  StoredProcedure [dbo].[GetUnreadNotificationsCount]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUnreadNotificationsCount]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #Temp(
		ID int IDENTITY,
		NID int,
	);
	INSERT INTO #Temp(NID)
	SELECT DISTINCT children.NID
	FROM Notifications as children LEFT JOIN Notifications as parents ON children.ParentMessage = parents.NID
									LEFT JOIN NotificationsTo as notes ON children.NID = notes.[Notification]
	WHERE children.NID NOT IN (SELECT parents.NID as PNID FROM Notifications as children INNER JOIN Notifications as parents ON children.ParentMessage = parents.NID)
			AND children.NID IN (SELECT [Notification] FROM NotificationsTo WHERE [UID] = @uid)
			AND children.[Read] = 0 AND children.[Deleted] = 0
	ORDER BY children.NID DESC;
		
	SELECT COUNT(*) [count] FROM #Temp;

	DROP TABLE #Temp;

END

GO
/****** Object:  StoredProcedure [dbo].[GetUsage]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUsage]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT usage FROM Usages WHERE ID = @id;
END

GO
/****** Object:  StoredProcedure [dbo].[GetUserbyGUID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserbyGUID]
	-- Add the parameters for the stored procedure here
	@guid uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
    SELECT [UID], username FROM Users WHERE resetguid = @guid;

END


GO
/****** Object:  StoredProcedure [dbo].[getUserFromGUID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[getUserFromGUID]
	-- Add the parameters for the stored procedure here
	@guid uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [UID], username FROM Users WHERE LoginGUID = @guid;
END


GO
/****** Object:  StoredProcedure [dbo].[GetUserFromID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 30/05/14
-- Description:	Returning all a user's info given the UserId
-- =============================================
CREATE PROCEDURE [dbo].[GetUserFromID]
	-- Add the parameters for the stored procedure here
	@ID int


AS
BEGIN
	SET NOCOUNT ON;

	SELECT Users.username, Users.reset,Users.faultylogins,Users.lastloginattempt,Users.password_reset_force, Users.passwordresetrequesttime, Users.active,
		   Company.CompanyName,Company.faultyloginlimit as floginlimit, Company.IAJID, CompanyTypes.[Type] as companytype, Company.CompanyID as companyid,
	       People.FName, People.MName, People.LName, People.TRN,People.PeopleID, Address.AptNumber,
		   Address.RoadNumber,RoadName.RoadName, RoadType.RoadType, ZipCodes.Zipcode, Cities.City, Parishes.PID, Parishes.Parish, Countries.CountryName, Employees.EmployeeID

		   FROM Users LEFT JOIN Employees ON Users.EmployeeID = Employees.EmployeeID
		              LEFT JOIN Company ON Company.CompanyID = Employees.CompanyID
					  LEFT JOIN CompanyTypes ON Company.[Type] = CompanyTypes.ID
		              LEFT JOIN People ON People.PeopleID =Employees.PersonID
					  LEFT JOIN [Address] ON [Address].AID =People.Address
					  LEFT JOIN RoadName ON Address.RoadName = RoadName.RID
				      LEFT JOIN RoadType ON Address.RoadType = RoadType.RTID
				      LEFT JOIN ZipCodes ON Address.ZipCode = ZipCodes.ZCID
				      LEFT JOIN Cities ON Address.City = Cities.CID
					  LEFT JOIN Parishes ON [Address].Parish = Parishes.PID
				      LEFT JOIN Countries ON Address.Country = Countries.CID
					  WHERE Users.UID = @ID;


	 --SELECT username,password FROM Users WHERE UID = @ID;    
END


GO
/****** Object:  StoredProcedure [dbo].[GetUserFromUsername]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:Chrystal Parchment
-- Create date: 30/05/14
-- Description:	Returning all a user's info given the username
-- =============================================
CREATE PROCEDURE [dbo].[GetUserFromUsername]
	
	@username varchar (max)


AS
BEGIN
	SET NOCOUNT ON;

	SELECT Users.UID, People.FName, People.MName, People.LName, People.TRN,People.PeopleID
		   FROM Users LEFT JOIN Employees ON Users.EmployeeID = Employees.EmployeeID
		              LEFT JOIN People ON People.PeopleID =Employees.PersonID
					  WHERE Users.username = @username;


	 
END



GO
/****** Object:  StoredProcedure [dbo].[GetUserGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserGroup]
	-- Add the parameters for the stored procedure here
	@UGID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT ug.UserGroup, ug.SuperGroup, ug.stock, ug.Company [cid], c.CompanyName [cname], ug.[system], ug.administrative
	FROM UserGroups ug LEFT JOIN Company c ON ug.Company = c.CompanyID WHERE ug.UGID = @UGID;
END


GO
/****** Object:  StoredProcedure [dbo].[GetUserGroupDetails]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserGroupDetails]
	@GroupName varchar(250),
	@comp_id int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT UGID, UserGroup, SuperGroup FROM UserGroups WHERE UserGroup = @GroupName AND (@comp_id = 0 OR Company = @comp_id);
END

GO
/****** Object:  StoredProcedure [dbo].[GetUserGroups]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserGroups]
	-- Add the parameters for the stored procedure here
	@compid int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT ug.UGID, ug.UserGroup, c.CompanyID [cid], c.CompanyName [cname], ug.stock, ug.[system]
	FROM UserGroups ug LEFT JOIN Company c ON ug.Company = c.CompanyID
	WHERE (@compid = 0 OR  ug.Company = @compid)
	ORDER BY [system] DESC, stock DESC, cname ASC;
END

GO
/****** Object:  StoredProcedure [dbo].[GetUserGroupsWithUserID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserGroupsWithUserID]
	-- Add the parameters for the stored procedure here
	@UserID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT UserGroups.UGID, UserGroup FROM UserGroups LEFT JOIN UserGroupUserAssoc ON UserGroups.UGID = UserGroupUserAssoc.UGID
													  LEFT JOIN Users ON UserGroupUserAssoc.UID = Users.UID
									  WHERE Users.UID = @UserID;
END


GO
/****** Object:  StoredProcedure [dbo].[GetUserNameWithID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserNameWithID]
	-- Add the parameters for the stored procedure here
	@UID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT username FROM Users WHERE [UID] = @UID;
END


GO
/****** Object:  StoredProcedure [dbo].[GetUserPermission]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserPermission]
	-- Add the parameters for the stored procedure here
	@UPID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [Table], [Action] FROM UserPermissions WHERE UPID = @UPID;
END


GO
/****** Object:  StoredProcedure [dbo].[GetUserRoleModels]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserRoleModels]
	-- Add the parameters for the stored procedure here
	@UGID int,
	@UID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DECLARE @true bit, @false bit;
	SET @true = 1; SET @false = 0;
	IF EXISTS (SELECT UGUAID FROM UserGroupUserAssoc JOIN Users ON UserGroupUserAssoc.UID = Users.UID WHERE UserGroupUserAssoc.UGID = @UGID AND UserGroupUserAssoc.UID = @UID)
		SELECT @true as result;
	ELSE
		SELECT @false as result;
	
	
END


GO
/****** Object:  StoredProcedure [dbo].[GetUsers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	    Chrystal Parchment
-- Create date: 09/06/2014
-- Description:	Pulling all users in the database
-- =============================================
 CREATE PROCEDURE [dbo].[GetUsers]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT [UID],username,reset,faultylogins,lastloginattempt,password_reset_force, passwordresetrequesttime, active FROM Users WHERE Filter = 0 AND Deleted != 1;
END


GO
/****** Object:  StoredProcedure [dbo].[GetUsersNotMyContactGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUsersNotMyContactGroup]
	-- Add the parameters for the stored procedure here
	@cgid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [UID], username
		FROM Users 
		WHERE Users.UID NOT IN (SELECT Users.[UID]
								FROM Users LEFT JOIN Employees ON Users.EmployeeID = Employees.EmployeeID
								           RIGHT JOIN ContactUserAssoc ON Users.UID = ContactUserAssoc.UID
								WHERE ContactUserAssoc.contactgroup = @cgid)
			  AND Filter = 0;
END


GO
/****** Object:  StoredProcedure [dbo].[GetUserWithName]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserWithName]
	-- Add the parameters for the stored procedure here
	@username varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [UID], username
	FROM Users WHERE username = @username;
END


GO
/****** Object:  StoredProcedure [dbo].[GetVehicleBodyTypes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystl Parchment
-- Create date: 11/02/2015
-- Description:	Retrieves all the vehicle body types in the system
-- =============================================
CREATE PROCEDURE [dbo].[GetVehicleBodyTypes]
	
AS
BEGIN

	SET NOCOUNT ON;

	SELECT DISTINCT BodyType FROM Vehicles;
	
END

GO
/****** Object:  StoredProcedure [dbo].[GetVehicleByChassis]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetVehicleByChassis]
	-- Add the parameters for the stored procedure here
	@chassis varchar(50),
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (SELECT VehicleID [ID] FROM Vehicles WHERE ChassisNo = @chassis)
		SELECT VehicleID [ID], Vehicles.Make, Vehicles.Model, Vehicles.ModelType, Vehicles.BodyType, Vehicles.Extension, Vehicles.VehicleRegistrationNo, 
	                  Vehicles.VIN, Vehicles.ChassisNo, Vehicles.Colour, Vehicles.VehicleYear  FROM Vehicles WHERE ChassisNo = @chassis;
	--ELSE 
	--BEGIN
	--	INSERT INTO Vehicles (ChassisNo, CreatedBy, LastModifiedBy) VALUES (@chassis, @uid, @uid);
	--	SELECT @@IDENTITY [ID];
	--END


END

GO
/****** Object:  StoredProcedure [dbo].[GetVehicleByRegNo]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 11/02/2015
-- Description:	Retrieving a vehicle, given the Licence/Reg number
-- =============================================
CREATE PROCEDURE [dbo].[GetVehicleByRegNo]
	
	@regNumber varchar(250)
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS (SELECT VehicleID [ID] FROM Vehicles WHERE VehicleRegistrationNo = @regNumber)
		SELECT VehicleID [ID], Vehicles.Make, Vehicles.Model, Vehicles.ModelType, Vehicles.BodyType, Vehicles.Extension,
	                  Vehicles.VIN, Vehicles.ChassisNo, Vehicles.Colour, Vehicles.VehicleYear  FROM Vehicles  WHERE VehicleRegistrationNo = @regNumber;


END

GO
/****** Object:  StoredProcedure [dbo].[GetVehicleByVIN]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 20/06/2014
-- Description:	Gets vehicle's info from ID
-- =============================================

CREATE PROCEDURE [dbo].[GetVehicleByVIN]
	-- Add the parameters for the stored procedure here
	
	@VIN varchar(MAX)


AS
BEGIN
	SET NOCOUNT ON;

	SELECT Vehicles.VehicleID ID, Vehicles.Make, Vehicles.Model, Vehicles.ModelType, Vehicles.BodyType, Vehicles.Extension, Vehicles.VehicleRegistrationNo, Vehicles.VIN, Vehicles.ChassisNo, Vehicles.HandDrive, VehicleHandDriveLookUp.HDType,
	       Vehicles.Colour, Vehicles.Cylinders, Vehicles.EngineNo, Vehicles.EngineModified, Vehicles.EngineType, Vehicles.EstimatedAnnualMileage, Vehicles.ExpiryDateofFitness,
		   Vehicles.HPCCUnitType, Vehicles.MainDriver, Vehicles.ReferenceNo, Vehicles.RegistrationLocation, Vehicles.Rooftype, Vehicles.Seating, Vehicles.SeatingCert,
		   Vehicles.Tonnage, Vehicles.VehicleYear,People.PeopleID, People.TRN, People.FName, People.MName, People.LName, Address.RoadNumber,RoadName.RoadName, 
		   RoadType.RoadType, ZipCodes.Zipcode, Cities.City, Countries.CountryName, TransmissionTypeLookUp.TransmissionType--, Emails.email

		   FROM Vehicles LEFT JOIN VehicleHandDriveLookUp ON Vehicles.HandDrive = VehicleHandDriveLookUp.VHDLID
		                 LEFT JOIN Drivers ON Drivers.DriverID = Vehicles.MainDriver 
						 LEFt JOIN People ON People.PeopleID = Drivers.PersonID
						
		                 LEFT JOIN [Address] ON [Address].AID = People.Address
						 LEFT JOIN RoadName ON Address.RoadName = RoadName.RID
				         LEFT JOIN RoadType ON Address.RoadType = RoadType.RTID
				         LEFT JOIN ZipCodes ON Address.ZipCode = ZipCodes.ZCID
				         LEFT JOIN Cities ON Address.City = Cities.CID
				         LEFt JOIN Countries ON Address.Country = Countries.CID
						 LEFT JOIN TransmissionTypeLookUp ON TransmissionTypeLookUp.TransmisionTypeID = Vehicles.TransmissionType
					    
					     WHERE Vehicles.VIN = @VIN;


END


GO
/****** Object:  StoredProcedure [dbo].[GetVehicleCoverNoteByNumber]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetVehicleCoverNoteByNumber]
	-- Add the parameters for the stored procedure here
	@covernoteno int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Alterations, Company.CompanyName, Company.CompanyID, VehicleCoverNote.Cancelled, VehicleCoverNote.CancelledBy, CancelledOn, DateFirstPrinted, LastPrinted, CoverNoteID, EffectiveDate, 
	       VehicleCoverNote.period, EndorsementNo, PrintCount,  PrintCountInsurer, approved, Vehicles.VehicleID, Vehicles.VIN, Vehicles.Make, Vehicles.ChassisNo, Vehicles.Model, Vehicles.VehicleYear, Vehicles.Seating,
		   Vehicles.VehicleRegistrationNo, Vehicles.ReferenceNo,  Policies.PolicyID, PolicyCovers.Cover, Policies.Policyno, Policies.imported, Usages.usage,  VehicleCoverNote.WordingTemplate, Vehicles.EngineNo,
		   VehicleCoverNote.ExpiryDateTime, VehicleCoverNote.ManualCoverNoteNo [covernoteno], Vehicles.HPCCUnitType, PeopleID, People.TRN, People.FName, People.LName, VehicleCoverNote.mortgagee,
		   VehicleCoverNote.LastPrintedByFlat, VehicleCoverNote.FirstPrintedByFlat
	FROM VehicleCoverNote LEFT JOIN Company ON VehicleCoverNote.CompanyID = Company.CompanyID
						  LEFT JOIN Vehicles ON VehicleCoverNote.RiskItemNo = Vehicles.VehicleID
						  LEFT JOIN Drivers ON Drivers.DriverID = Vehicles.MainDriver 
	                      LEFT JOIN People ON People.PeopleID = Drivers.PersonID
						  LEFT JOIN Policies ON Policies.PolicyID = VehicleCoverNote.PolicyID
						  LEFT JOIN VehiclesUnderPolicy ON VehiclesUnderPolicy.PolicyID = Policies.PolicyID AND VehiclesUnderPolicy.VehicleID= VehicleCoverNote.RiskItemNo
						  LEFT JOIN Usages ON Usages.ID = VehiclesUnderPolicy.VehicleUsage
						  LEFT JOIN PolicyCovers ON PolicyCovers.ID = Policies.PolicyCover
	WHERE VehicleCoverNoteID = @covernoteno;

END

GO
/****** Object:  StoredProcedure [dbo].[GetVehicleFromId]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 20/06/2014
-- Description:	Gets vehicle's info from ID
-- =============================================

CREATE PROCEDURE [dbo].[GetVehicleFromId]
	-- Add the parameters for the stored procedure here
	
	@VehicleID int


AS
BEGIN
	SET NOCOUNT ON;

	SELECT Vehicles.Make, Vehicles.Model, Vehicles.ModelType, Vehicles.BodyType, Vehicles.Extension, Vehicles.VehicleRegistrationNo, Vehicles.VIN, Vehicles.ChassisNo, Vehicles.HandDrive, VehicleHandDriveLookUp.HDType,
	       Vehicles.Colour, Vehicles.Cylinders, Vehicles.EngineNo, Vehicles.EngineModified, Vehicles.EngineType, Vehicles.EstimatedAnnualMileage, Vehicles.ExpiryDateofFitness,
		   Vehicles.HPCCUnitType, Vehicles.MainDriver, Vehicles.ReferenceNo, Vehicles.RegistrationLocation, Vehicles.Rooftype, Vehicles.Seating, Vehicles.SeatingCert,
		   Vehicles.Tonnage, Vehicles.VehicleYear, Vehicles.RegisteredOwner, Vehicles.RestrictedDriving, People.PeopleID, People.TRN, People.FName, People.MName, People.LName, Address.RoadNumber, Address.AptNumber, RoadName.RoadName, 
		   RoadType.RoadType, ZipCodes.Zipcode, Cities.City, Parishes.PID, Parishes.Parish, Countries.CountryName, TransmissionTypeLookUp.TransmissionType, Usages.usage, Usages.ID

		   FROM Vehicles LEFT JOIN VehicleHandDriveLookUp ON Vehicles.HandDrive = VehicleHandDriveLookUp.VHDLID
		                 LEFT JOIN Drivers ON Drivers.DriverID = Vehicles.MainDriver 
						 LEFT JOIN People ON People.PeopleID = Drivers.PersonID
		                 LEFT JOIN [Address] ON [Address].AID = People.Address
						 LEFT JOIN RoadName ON Address.RoadName = RoadName.RID
				         LEFT JOIN RoadType ON Address.RoadType = RoadType.RTID
				         LEFT JOIN ZipCodes ON Address.ZipCode = ZipCodes.ZCID
				         LEFT JOIN Cities ON Address.City = Cities.CID
						 LEFT JOIN Parishes ON [Address].Parish = Parishes.PID
				         LEFT JOIN Countries ON Address.Country = Countries.CID
						 LEFT JOIN TransmissionTypeLookUp ON TransmissionTypeLookUp.TransmisionTypeID = Vehicles.TransmissionType
						 LEFT JOIN Usages ON Usages.ID = Vehicles.Usage
					     WHERE Vehicles.VehicleID = @VehicleID;


END

GO
/****** Object:  StoredProcedure [dbo].[GetVehicleInsurer]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 24/09/2014
-- Description:	Retrieving the company under which a vehicle is currently covered
-- =============================================
CREATE PROCEDURE [dbo].[GetVehicleInsurer]
	@vid int,
	@startDateTime varchar (250),
	@endDateTime varchar (250)
AS
BEGIN
	
	SET NOCOUNT ON;
	
    DECLARE @smallEnddatetime smalldatetime = @endDateTime;
	DECLARE @end datetime = @smallEnddatetime;

	DECLARE @smallstartdatetime smalldatetime = @startDateTime;
	DECLARE @start datetime = @smallstartdatetime;


	SELECT Company.CompanyID, Company.CompanyName, Company.IAJID, Company.TRN FROM Policies 
	                    LEFT JOIN VehiclesUnderPolicy ON VehiclesUnderPolicy.PolicyID = Policies.PolicyID 
	                    LEFT JOIN Company ON Company.CompanyID = Policies.CompanyID
    WHERE VehiclesUnderPolicy.VehicleID = @vid AND VehiclesUnderPolicy.Cancelled != 1
										       AND Policies.Deleted = 0
										       AND ((@start >= Policies.StartDateTime AND @start <= Policies.EndDateTime) OR (@end <= Policies.EndDateTime AND @endDateTime >= Policies.StartDateTime 
										       OR (@start <= Policies.StartDateTime AND @end >= Policies.EndDateTime)));

END

GO
/****** Object:  StoredProcedure [dbo].[GetVehicleMakes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystl Parchment
-- Create date: 11/02/2015
-- Description:	Retrieves all the vehicle makes in the system
-- =============================================
CREATE PROCEDURE [dbo].[GetVehicleMakes]
	
AS
BEGIN

	SET NOCOUNT ON;

	SELECT DISTINCT Make FROM Vehicles;
	
END

GO
/****** Object:  StoredProcedure [dbo].[GetVehicleModels]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystl Parchment
-- Create date: 11/02/2015
-- Description:	Retrieves all the vehicle models in the system
-- =============================================
CREATE PROCEDURE [dbo].[GetVehicleModels]
	
AS
BEGIN

	SET NOCOUNT ON;

	SELECT DISTINCT Model FROM Vehicles;
	
END

GO
/****** Object:  StoredProcedure [dbo].[GetVehicleModelTypes]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystl Parchment
-- Create date: 11/02/2015
-- Description:	Retrieves all the vehicle model types in the system
-- =============================================
CREATE PROCEDURE [dbo].[GetVehicleModelTypes]
	
AS
BEGIN

	SET NOCOUNT ON;

	SELECT DISTINCT ModelType FROM Vehicles;
	
END

GO
/****** Object:  StoredProcedure [dbo].[GetVehiclePolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 05/11/2014
-- Description:	This pulls the policy that a vehicle is covered under (in a certain time frame)
-- =============================================
CREATE PROCEDURE [dbo].[GetVehiclePolicy]
	
	@VehicleID int

AS
BEGIN
	
	SET NOCOUNT ON;
    
	SELECT  Policies.PolicyID
	       FROM Vehicles 
			  LEFT JOIN VehiclesUnderPolicy ON VehiclesUnderPolicy.VehicleID = @VehicleID AND VehiclesUnderPolicy.Cancelled != 1
			  RIGHT JOIN Policies ON Policies.PolicyID = VehiclesUnderPolicy.PolicyID  AND Policies.Deleted = 0 
			             AND (CURRENT_TIMESTAMP BETWEEN Policies.StartDateTime AND Policies.EndDateTime)
		  WHERE Vehicles.VehicleID = @VehicleID;


END


GO
/****** Object:  StoredProcedure [dbo].[GetVehicles]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	    Chrystal Parchment
-- Create date: 26/06/2014
-- Description:	Pulling all vehicles in the database
-- =============================================
CREATE PROCEDURE [dbo].[GetVehicles]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT VehicleID, Make, Model, ModelType, BodyType, Extension, VehicleRegistrationNo, VIN, HandDrive, Colour, Cylinders, EngineNo, 
		            EngineModified, EngineType, EstimatedAnnualMileage, ExpiryDateofFitness, HPCCUnitType, MainDriver, ReferenceNo, RegistrationLocation,
					RoofType, Seating, SeatingCert, Tonnage, TransmissionType, VehicleYear, ChassisNo [chassisno]
	FROM Vehicles;
END


GO
/****** Object:  StoredProcedure [dbo].[GetVehiclesCanBeCertOrCNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetVehiclesCanBeCertOrCNote]
	-- Add the parameters for the stored procedure here
	--@uid int
	@cid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT v.VehicleID [ID], v.ChassisNo [chassis], v.VehicleRegistrationNo [regno], v.Make [make], v.Model [model], v.VIN [vin]
	FROM Vehicles v LEFT OUTER JOIN VehiclesUnderPolicy vup ON v.VehicleID = vup.VehicleID
					LEFT JOIN Policies p ON vup.PolicyID = p.PolicyID
	WHERE vup.Cancelled = 0 AND p.Cancelled = 0 AND p.StartDateTime <= CURRENT_TIMESTAMP AND p.EndDateTime >= CURRENT_TIMESTAMP 
	  AND (p.CompanyID = @cid OR p.CreatedBy IN (SELECT [UID] FROM Users u LEFT JOIN Employees e on u.EmployeeID = e.EmployeeID WHERE e.CompanyID = @cid))
	  AND (SELECT COUNT(*) FROM VehicleCertificates WHERE RiskItemNo = v.VehicleID AND PolicyID = p.PolicyID) = 0;

END

GO
/****** Object:  StoredProcedure [dbo].[GetVehicleWithChassis]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 02/10/2014
-- Description:	Searching for a vehicle, given a chassis number
-- =============================================
CREATE PROCEDURE [dbo].[GetVehicleWithChassis]
	
	@ChassisNo varchar (50)
AS
BEGIN
	
	SET NOCOUNT ON;

    DECLARE @result bit, @vehicle bit, @vehicleid int;
	IF EXISTS(SELECT VehicleID FROM Vehicles WHERE ChassisNo = @ChassisNo)
	      BEGIN
			SET @result = 1; SET @vehicle = 1; 	
			SELECT @result as result, @vehicle as [vehicle], VehicleID AS [VehicleID] FROM Vehicles WHERE ChassisNo = @ChassisNo;
		  END
		
	ELSE
	BEGIN
		SET @result = 0; SET @vehicle = 0; 
		SELECT @result as result, @vehicle as [vehicle];
	END
END


GO
/****** Object:  StoredProcedure [dbo].[GetVehicleWithVIN]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 27/06/2014
-- Description:	Searching for a vehicle, given a VIN
-- =============================================
CREATE PROCEDURE [dbo].[GetVehicleWithVIN]
	-- Add the parameters for the stored procedure here
	@VIN varchar (50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @result bit, @vehicle bit, @vehicleid int;
	IF EXISTS(SELECT VehicleID FROM Vehicles WHERE VIN = @VIN)
	      BEGIN
			SET @result = 1; SET @vehicle = 1; 	
			SELECT @result as result, @vehicle as [vehicle], VehicleID AS [VehicleID] FROM Vehicles WHERE VIN = @VIN;
		  END
		
	ELSE
	BEGIN
		SET @result = 0; SET @vehicle = 0; 
		SELECT @result as result, @vehicle as [vehicle];
	END
END


GO
/****** Object:  StoredProcedure [dbo].[GetVehUsage]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 13/11/2014
-- Description:	Pulling the usage ID of a particular vehicle under a certain policy
-- =============================================
CREATE PROCEDURE [dbo].[GetVehUsage]

		@polID int,
		@vehId int
AS

BEGIN
	
	SET NOCOUNT ON;
	
	SELECT VehicleUsage FROM VehiclesUnderPolicy WHERE VehicleID= @vehId AND PolicyID= @polID;
	
END


GO
/****** Object:  StoredProcedure [dbo].[GetWording]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetWording]
	-- Add the parameters for the stored procedure here
	@ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT w.ID, w.CertificateType, w.CertificateID, w.CertificateInsuredCode, w.ExtensionCode,
		   pc.ID [pcid], pc.Cover [policycover], u.ID [usageid], u.usage, w.VehicleMake, w.VehicleModel, w.VehicleModelType,
		   w.ExcludedDriver, w.ExcludedInsured, w.MultipleVehicle, w.RegisteredOwner, w.RestrictedDriver, w.Scheme,
		   w.AuthorizedDrivers, w.LimitsOfUse, w.VehicleDesc, w.VehRegNo, w.PolicyHolders, w.Imported [imported]
	FROM Wording w LEFT JOIN PolicyCovers pc ON w.PolicyCover = pc.ID
				   LEFT JOIN Usages u ON w.usageid = u.ID
	WHERE w.ID = @ID;
	
END

GO
/****** Object:  StoredProcedure [dbo].[GetWordingBasedOnFilter]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetWordingBasedOnFilter]
	-- Add the parameters for the stored procedure here
	@vehid int,
	@polid int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @isScheme bit, @regOwner bit, @restDriver bit, @mulVeh bit, @exclDri bit, @exclIns bit, @vupid int;

    -- Insert statements for procedure here
	
	SELECT @vupid = VUPID FROM VehiclesUnderPolicy WHERE VehicleID = @vehid AND PolicyID = @polid; 

	SELECT	@isScheme = Scheme  FROM Policies WHERE PolicyID = @polid;
	SELECT	@regOwner = (CASE WHEN (RegisteredOwner IS NOT NULL AND RegisteredOwner != '') THEN CAST(1 as bit) ELSE CAST(0 as bit) END) FROM  Vehicles WHERE VehicleID = @vehid;
	SELECT	@restDriver = RestrictedDriving FROM  Vehicles WHERE VehicleID = @vehid;
	SELECT 	@mulVeh = (CASE WHEN(COUNT(VehicleID) > 1) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) FROM VehiclesUnderPolicy WHERE Cancelled = 0 AND PolicyID = @polid;
	SELECT 	@exclDri = (CASE WHEN(COUNT(EDID) > 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) FROM ExcludedDrivers WHERE VehicleID = @vehid AND PolicyID = @polid;
	SELECT	@exclIns = (CASE WHEN((SELECT COUNT(xd.EDID) 
						FROM ExcludedDrivers xd LEFT JOIN Drivers dr ON xd.DriverID = dr.DriverID
						WHERE xd.PolicyID = @polid AND xd.VehicleID = @vehid AND dr.PersonID IN (SELECT pis.PersonID FROM PolicyInsured pis WHERE pis.PolicyID = @polid)) =  1) 
				  THEN CAST(1 as bit) ELSE CAST(0 as bit) END);

	SELECT ID, CertificateType [certtype]
	FROM Wording 
	WHERE	ExcludedDriver = @exclDri 
		AND ExcludedInsured = @exclIns 
		AND MultipleVehicle = @mulVeh
		AND RegisteredOwner = @regOwner
		AND RestrictedDriver = @restDriver
		AND Scheme = @isScheme
		AND PolicyCover = (SELECT PolicyCover FROM Policies WHERE PolicyID = @polid)
		AND usageid = (SELECT VehicleUsage FROM VehiclesUnderPolicy WHERE VUPID = @vupid);


END

GO
/****** Object:  StoredProcedure [dbo].[GetWordings]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetWordings]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Wording.ID, CertificateType, CertificateID, CertificateInsuredCode, ExtensionCode, PolicyCover, ExcludedDriver, ExcludedInsured, MultipleVehicle, 
		   RegisteredOwner, RestrictedDriver, Scheme, AuthorizedDrivers, LimitsOfUse, VehicleDesc, VehRegNo, PolicyHolders, Imported,
		   PolicyCovers.ID [pcid], PolicyCovers.Cover, Usages.ID [usageid], Usages.usage
	FROM Wording LEFT JOIN PolicyCovers ON Wording.PolicyCover = PolicyCovers.ID	
				 LEFT JOIN Usages ON Wording.usageid = Usages.ID
				 LEFT JOIN Users u ON Wording.CreatedBy = u.[UID]
				 LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID
	WHERE e.CompanyID = (SELECT e.CompanyID FROM Employees e LEFT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE u.[UID] = @uid);
END

GO
/****** Object:  StoredProcedure [dbo].[GetZipList]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetZipList]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT ZipCode as Zip FROM ZipCodes;
END


GO
/****** Object:  StoredProcedure [dbo].[GroupPermissions]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GroupPermissions]
	-- Add the parameters for the stored procedure here
	@GroupID int,
	@system bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT UserPermissions.UPID, UserPermissions.[Table], UserPermissions.[Action]
			 FROM PermissionGroupAssoc JOIN UserPermissions ON PermissionGroupAssoc.UPID = UserPermissions.UPID 
									   LEFT JOIN UserGroups ug ON PermissionGroupAssoc.UGID = ug.UGID
			 WHERE	PermissionGroupAssoc.UGID = @GroupID AND (@system = 1 OR UserPermissions.[system] = 0);
				--AND (UserPermissions.[system] = 1 AND ug.[system] = 1) OR UserPermissions.[system] = 0;
END

GO
/****** Object:  StoredProcedure [dbo].[HasCoverNoteHistory]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date:  16/03/2015
-- Description:	Testing if a particular vehicle has cover note history-
-- =============================================
CREATE PROCEDURE [dbo].[HasCoverNoteHistory]
	
	@riskID int,
	@polID int
AS
BEGIN

	SET NOCOUNT ON;
   
	DECLARE @result bit;
	SET @result = 0;

	SELECT @result = (CASE WHEN (Count(*) > 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) 
	                  FROM VehicleCoverNote  WHERE VehicleCoverNote.RiskItemNo = @riskID AND PolicyID = @polID;

	SELECT @result [result];

END

GO
/****** Object:  StoredProcedure [dbo].[InitializeKPIs]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InitializeKPIs]
	-- Add the parameters for the stored procedure here
	@companyID int,
	@type varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO KPI (mode, [type], company, datemode, kpi)
	SELECT m.id, (SELECT id FROM KPItype WHERE [type] = @type), @companyID, dm.id, 0
	FROM KPImode m, KPIDateMode dm WHERE dm.id NOT IN (
		SELECT KPIDateMode.id
		FROM KPI LEFT JOIN KPItype ON KPI.[type] = KPItype.id
					LEFT JOIN KPIDateMode ON KPI.datemode = KPIDateMode.id
		WHERE company = @companyID AND (KPItype.[type] = @type)
	);


END

GO
/****** Object:  StoredProcedure [dbo].[InsertGenCoverNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertGenCoverNote]
	-- Add the parameters for the stored procedure here
	@vehyear varchar(max) = null,
    @vehmake varchar(max) = null,
    @vehregno varchar(max) = null,
    @vehext varchar(max) = null,
    @maininsured varchar(max) = null,
    @drivers varchar(max) = null,
    @vehdesc varchar(max) = null,
    @cover varchar(max) = null,
    @vehmodeltype varchar(max) = null,
    @vehmodel varchar(max) = null,
    @authorizedwording varchar(max) = null,
    @mortgagees varchar(max) = null,
    @limitsofuse varchar(max) = null,
	@Usage  varchar(max) = null,
    @PolicyNo  varchar(max) = null,
    @PolicyHoldersAddress varchar(max) = null,
    @CovernoteNo varchar(max) = null,
    @Period varchar(max) = null,
    @effectiveTime varchar(max) = null,
    @expiryTime varchar(max) = null,
    @effectivedate varchar(max) = null,
    @expirydate varchar(max) = null,
    @endorsementNo varchar(max) = null,
    @bodyType varchar(max) = null,
    @seating varchar(max) = null,
    @chassisNo varchar(max) = null,
    @certificateType varchar(max) = null,
    @certitificateNo varchar(max) = null,
    @engineNo  varchar(max) = null,
	@companyCreatedBy varchar (max) = null,
	@certificateCode varchar(max) = null,
	@HPCC varchar (max) = null,
    @vcnid int,
	@uid int
AS
BEGIN
	
	SET NOCOUNT ON;

    DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX);
	DECLARE @GeneratedCoverID int; DECLARE @type varchar(MAX);

	IF NOT EXISTS(SELECT * FROM GeneratedCoverNotes WHERE VehicleCoverNoteID = @vcnid)
	BEGIN
		INSERT INTO GeneratedCoverNotes (CreatedBy, VehicleYear, VehicleMake, VehicleRegNo, VehExtension, MainInsured, VehDrivers, VehicleDesc, PolicyCover, VehModelType, VehicleModel, 
										 authorizeddrivers, mortgagees, LimitsOfUse, Usage,   PolicyNo,PolicyHolderAddress ,CovernoteNo, Period, effectiveTime ,expiryTime ,effectivedate,
										 expirydate, endorsementNo, bodyType ,seating ,chassisNo ,certificateType,certitificateNo,engineNo, companyCreatedBy, VehicleCoverNoteID, CertificateCode, HPCC)
								VALUES	(@uid, @vehyear, @vehmake, @vehregno, @vehext, @maininsured,@drivers, @vehdesc, @cover, @vehmodeltype, @vehmodel,
										 @authorizedwording, @mortgagees, @limitsofuse, @Usage ,@PolicyNo ,@PolicyHoldersAddress ,@CovernoteNo, @Period, @effectiveTime ,@expiryTime ,
										 @effectivedate,@expirydate ,@endorsementNo ,@bodyType ,@seating ,@chassisNo ,@certificateType ,@certitificateNo,@engineNo , @companyCreatedBy, @vcnid, @certificateCode, @HPCC);
		SELECT @GeneratedCoverID = @@IDENTITY;
		SELECT @origdata = null, @type = 'insert';

	END
	ELSE
	BEGIN
		SELECT @origdata = 'VehicleCertificateID:' + CONVERT(varchar(MAX),@vcnid)
						   +';regNo:' + CONVERT(varchar(MAX),VehicleRegNo) 
						   +';vehicleExt:' + CONVERT(varchar(MAX),VehExtension)
						   +';MainInsured:' + CONVERT(varchar(MAX),MainInsured) 
						   +';LimitsOfUse:' + CONVERT(varchar(MAX),LimitsOfUse)
						   +';policyCover:' + CONVERT(varchar(MAX),PolicyCover)
						   +';vehMake:' + CONVERT(varchar(MAX),VehicleMake)
						   +';vehModel:' + CONVERT(varchar(MAX),VehicleModel)
						   +';vehModelType:' + CONVERT(varchar(MAX),VehModelType)
						   +';vehyear:' + CONVERT(varchar(MAX),VehicleYear)
						   +';drivers:' + CONVERT(varchar(MAX),VehDrivers)
						   +';vehdesc:' + CONVERT(varchar(MAX),VehicleDesc)
						   +';authorizedwording:' + CONVERT(varchar(MAX),mortgagees),
				@type = 'update'
		FROM GeneratedCoverNotes WHERE VehicleCoverNoteID = @vcnid;

		UPDATE GeneratedCoverNotes
		SET VehicleYear = @vehyear,
			VehicleMake = @vehmake,
			VehicleRegNo = @vehregno,
			VehExtension = @vehext, 
			MainInsured = @maininsured, 
			VehDrivers = @drivers, 
			VehicleDesc = @vehdesc, 
			PolicyCover = @cover, 
			VehModelType = @vehmodeltype, 
			VehicleModel = @vehmodel, 
			authorizeddrivers = @authorizedwording, 
			mortgagees = @mortgagees, 
			LimitsOfUse = @limitsofuse, 
			Usage = @Usage,   
			PolicyNo = @PolicyNo,
			PolicyHolderAddress = @PolicyHoldersAddress,
			CovernoteNo = @CovernoteNo, 
			Period = @Period, 
			effectiveTime = @effectiveTime,
			expiryTime = @expiryTime,
			effectivedate = @effectivedate,
			expirydate = @expirydate, 
			endorsementNo = @endorsementNo, 
			bodyType = @bodyType,
			seating = @seating,
			chassisNo = @chassisNo,
			certificateType = @certificateType,
			certitificateNo = @certitificateNo,
			engineNo = @engineNo, 
			companyCreatedBy = @companyCreatedBy
		WHERE VehicleCoverNoteID = @vcnid;
		
	END

	 SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
			@newdata = 'VehicleCertificateID:' + CONVERT(varchar(MAX),@@IDENTITY)
			+';regNo:' + CONVERT(varchar(MAX),@vehregno) 
			+';vehicleExt:' + CONVERT(varchar(MAX),@vehext)
			+';MainInsured:' + CONVERT(varchar(MAX),@maininsured) 
			+';LimitsOfUse:' + CONVERT(varchar(MAX),@limitsofuse)
			+';policyCover:' + CONVERT(varchar(MAX),@cover)
			+';vehMake:' + CONVERT(varchar(MAX),@vehmake)
			+';vehModel:' + CONVERT(varchar(MAX),@vehmodel)
			+';vehModelType:' + CONVERT(varchar(MAX),@vehmodeltype)
			+';vehyear:' + CONVERT(varchar(MAX),@vehyear)
			+';drivers:' + CONVERT(varchar(MAX),@drivers)
			+';vehdesc:' + CONVERT(varchar(MAX),@vehdesc)
			+';authorizedwording:' + CONVERT(varchar(MAX),@authorizedwording)
			+';mortgagees:' + CONVERT(varchar(MAX),@mortgagees);
					             
							
	EXEC LogAudit @type, @uid , @details, 'GeneratedCoverNotes',  @@IDENTITY, @origdata, @newdata, @type;



	--UPDATING THE VEHICLE COVERNOTE'S 'GENERATED' FLAG

    SELECT @details = 'The record ' + CONVERT(varchar(MAX),@vcnid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
					   @origdata = 'VehicleCovernoteID:' + CONVERT(varchar(MAX),@vcnid)+';Generated:' + CONVERT(varchar(MAX),Generated)
					   FROM VehicleCoverNote WHERE VehicleCoverNoteID = @vcnid;

	UPDATE VehicleCoverNote SET Generated = 1 WHERE VehicleCoverNoteID = @vcnid;
	
	SELECT @newdata = 'VehicleCovernoteID:' + CONVERT(varchar(MAX),@vcnid)+';Generated:' + CONVERT(varchar(MAX),Generated)
					   FROM VehicleCoverNote WHERE VehicleCoverNoteID = @vcnid;     
							
	EXEC LogAudit 'update', @uid , @details, 'VehicleCoverNote',  @vcnid, @origdata, @newdata, 'update';


	SELECT @GeneratedCoverID [ID];

END

GO
/****** Object:  StoredProcedure [dbo].[InsertNewRoad]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertNewRoad]
	-- Add the parameters for the stored procedure here
	@Road varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF ((SELECT COUNT(RID) FROM RoadName WHERE RoadName = @Road)= 0)
	BEGIN
		INSERT INTO RoadName (RoadName) VALUES (@Road);
		SELECT 1 AS result;
	END
	ELSE 
	BEGIN
		SELECT 0 AS result;
	END
END


GO
/****** Object:  StoredProcedure [dbo].[IsActive]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsActive]
	-- Add the parameters for the stored procedure here
	@username varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT active FROM Users WHERE username = @username;
END


GO
/****** Object:  StoredProcedure [dbo].[IsActiveCoverNoteExist]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsActiveCoverNoteExist]
	-- Add the parameters for the stored procedure here
	@vehID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF ((SELECT COUNT(*) FROM VehicleCoverNote WHERE EffectiveDate <= CURRENT_TIMESTAMP AND DATEADD(DAY, period, EffectiveDate) >= CURRENT_TIMESTAMP AND Cancelled = 0 AND RiskItemNo = @vehID) > 0) SELECT CAST(1 as bit) result;
	ELSE SELECT CAST(0 as bit) result;

END


GO
/****** Object:  StoredProcedure [dbo].[IsAdministrative]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsAdministrative]
	-- Add the parameters for the stored procedure here
	@ugid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT administrative [result] FROM UserGroups WHERE UGID = @ugid;
END

GO
/****** Object:  StoredProcedure [dbo].[IsBrokerIntermediary]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsBrokerIntermediary]
	-- Add the parameters for the stored procedure here
	@ins_id int,
	@brk_id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN(COUNT(*) > 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result]
	FROM InsurerToIntermediary 
	WHERE Insurer = @ins_id AND Intermediary = @brk_id AND [enabled] = 1;
END

GO
/****** Object:  StoredProcedure [dbo].[IsCertificateActive]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 19/08/2014
-- Description:	Checking if a particular vehicle certificate is active
-- =============================================
CREATE PROCEDURE [dbo].[IsCertificateActive]
	@vehehicleCertId int
AS
BEGIN
	
	SET NOCOUNT ON;
  DECLARE @result bit;

    IF EXISTS (SELECT VehicleCertificateID FROM VehicleCertificates WHERE VehicleCertificateID= @vehehicleCertId AND approved= 1 )
	BEGIN
		SET @result = 1;
		SELECT @result AS [result];
	END

	ELSE
	BEGIN
		SET @result = 0;
		SELECT @result AS [result];
	END
END

GO
/****** Object:  StoredProcedure [dbo].[IsCertificateCancelled]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 22/08/2014
-- Description:	Checking if a particular certificate is cancelled
-- =============================================
CREATE PROCEDURE [dbo].[IsCertificateCancelled]
	-- Add the parameters for the stored procedure here
	@VehCertificateId int
AS
BEGIN

	SET NOCOUNT ON;
  DECLARE @result bit;

    IF EXISTS (SELECT VehicleCertificateID FROM VehicleCertificates WHERE VehicleCertificateID = @VehCertificateId AND Cancelled IS NOT NULL AND Cancelled = 1)

	BEGIN
		SET @result = 1;
		SELECT @result AS [result];
	END

	ELSE
	BEGIN
		SET @result = 0;
		SELECT @result AS [result];
	END
END

GO
/****** Object:  StoredProcedure [dbo].[IsCertificatePrintable]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 30/09/2014
-- Description:	Determining if a certificate can be printed or not (by a company)
-- =============================================
CREATE PROCEDURE [dbo].[IsCertificatePrintable]
	@vehicleCertID int, 
	@compId int,
	@insurerID int
AS
BEGIN
	
	SET NOCOUNT ON;

	IF (@compId = @insurerID)
	    BEGIN
			SELECT CAST(1 as bit) as result;
	    END

	ELSE
	  BEGIN
	  --Testing to ensure the company is an intermediary
		 IF EXISTS (SELECT CompanyID FROM Company WHERE CompanyID= @compId AND ([Type] =2 OR [Type] = 3))
		 BEGIN
			IF EXISTS (SELECT CTID FROM InsurerToIntermediary WHERE Intermediary=  @compId AND Insurer = @insurerID AND  ([enabled] IS NOT NULL AND [enabled] = 1) AND [Certificate] = 1)
			BEGIN
				 IF EXISTS (SELECT VehicleCertificateID FROM VehicleCertificates WHERE VehicleCertificateID = @vehicleCertID 
										                AND (PrintCount = NULL OR PrintCount < (SELECT PrintLimit FROM InsurerToIntermediary WHERE Insurer=  @insurerID 
														                                                           AND Intermediary = @compId)))
					BEGIN
						SELECT CAST(1 as bit) as result;
					END
				 ELSE
				   BEGIN -- IF the print has exceeded the limit, then test if the over ride is available
				         IF EXISTS (SELECT VehicleCertificateID FROM VehicleCertificates WHERE VehicleCertificateID = @vehicleCertID AND PrintLimitOverride = 1)
					           BEGIN
						          SELECT CAST(1 as bit) as result;
					           END
						ELSE -- No over ride available
						    BEGIN
						         SELECT CAST(0 as bit) as result;
						     END
				   END
			  END
			  ELSE --If the intermediary to insurer relationship does not exist, or if the intermediary is disabled to that insurer
				 BEGIN
					SELECT CAST(0 as bit) as result;
				 END
	       END
       END
  END
GO
/****** Object:  StoredProcedure [dbo].[IsCertificatePrinted]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 26/08/2014
-- Description:	Checking if a certificate has already been printed (at least once)
-- =============================================
CREATE PROCEDURE [dbo].[IsCertificatePrinted]
	
@VehicleCertId int

AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @result bit;

	IF EXISTS (SELECT VehicleCertificateID FROM VehicleCertificates WHERE VehicleCertificateID = @VehicleCertId AND PrintCount > 0)
		BEGIN
		     SET @result = 1;
		END

	ELSE 
		BEGIN
		   SET @result = 0;
		END

	SELECT @result AS [result];



END

GO
/****** Object:  StoredProcedure [dbo].[IsCertificateReal]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsCertificateReal]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN (Count(*) > 0) THEN CAST (1 as bit) ELSE CAST (0 as bit) END) [result]
	FROM VehicleCertificates WHERE VehicleCertificateID = @id;
END

GO
/****** Object:  StoredProcedure [dbo].[IsCompanyEnabled]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsCompanyEnabled]
	-- Add the parameters for the stored procedure here
	@companyID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [Enabled] [result]
	FROM Company 
	WHERE CompanyID = @companyID
END


GO
/****** Object:  StoredProcedure [dbo].[IsCompanyInsurer]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsCompanyInsurer]
	-- Add the parameters for the stored procedure here
	@cid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	IF EXISTS(SELECT * FROM Company c LEFT JOIN CompanyTypes ct ON c.[Type] = ct.ID WHERE c.CompanyID = @cid AND ct.[Type] = 'Insurer')
		SELECT CAST(1 as bit) [result];
	ELSE
		SELECT CAST(0 as bit) [result];

END


GO
/****** Object:  StoredProcedure [dbo].[IsCompanyRegular]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 26/11/2014
-- Description:	Determining if a company is a "regular" company added in the policy, or it is an insurance company/brokerage/agency
-- =============================================

CREATE PROCEDURE [dbo].[IsCompanyRegular]

@compId int

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @result bit;

    IF EXISTS (SELECT CompanyID FROM Company WHERE CompanyID = @compId AND Type IS NULL)
	BEGIN
		SET @result = 1;
		SELECT @result AS result;
	END

	ELSE
	BEGIN
		SET @result = 0;
		SELECT @result AS result;
	END

END

GO
/****** Object:  StoredProcedure [dbo].[IsCompanyShortSet]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsCompanyShortSet]
	-- Add the parameters for the stored procedure here
	@cid int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN (CompShortening IS NULL OR CompShortening = '' OR @cid = 0) THEN (CAST(0 as bit)) ELSE (CAST(1 as bit)) END) [result] FROM Company WHERE CompanyID = @cid;
END


GO
/****** Object:  StoredProcedure [dbo].[IsCoverNoteActive]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 19/08/2014
-- Description:	Checking if a particular vehicle certificate is active/or has been cancelled
-- =============================================
CREATE PROCEDURE [dbo].[IsCoverNoteActive]
	@vehCoverNoteID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  DECLARE @result bit;

    IF EXISTS (SELECT VehicleCoverNoteID FROM VehicleCoverNote WHERE VehicleCoverNoteID = @vehCoverNoteID AND approved = 1)
	BEGIN
		SET @result = 1;
		SELECT @result AS [result];
	END

	ELSE
	BEGIN
		SET @result = 0;
		SELECT @result AS [result];
	END
END


GO
/****** Object:  StoredProcedure [dbo].[IsCoverNoteCancelled]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 22/08/2014
-- Description:	Checking if a particular cover note is cancelled
-- =============================================
CREATE PROCEDURE [dbo].[IsCoverNoteCancelled]
	
	@VehCoverNoteId int
AS
BEGIN

	SET NOCOUNT ON;
  DECLARE @result bit;

    IF EXISTS (SELECT VehicleCoverNoteID FROM VehicleCoverNote WHERE VehicleCoverNoteID = @VehCoverNoteId AND Cancelled IS NOT NULL AND Cancelled = 1)

	BEGIN
		SET @result = 1;
		SELECT @result AS [result];
	END

	ELSE
	BEGIN
		SET @result = 0;
		SELECT @result AS [result];
	END
END

GO
/****** Object:  StoredProcedure [dbo].[IsCoverNoteExistNow]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsCoverNoteExistNow]
	-- Add the parameters for the stored procedure here
	@riskid int,
	@effdate Date,
	@enddate Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN (COUNT(*) > 0) THEN (CAST (1 as bit)) ELSE (CAST (0 as bit)) END) [result]
	FROM VehicleCoverNote vcn 
	WHERE   (vcn.RiskItemNo = @riskid) 
		AND ((@effdate < vcn.ExpiryDateTime AND @effdate > vcn.EffectiveDate) OR (@enddate < vcn.EffectiveDate AND @enddate > vcn.EffectiveDate));
			 --(vcn.EffectiveDate < @effdate AND vcn.EffectiveDate < @enddate))); --testing if cover notes exist in the same time period
END

GO
/****** Object:  StoredProcedure [dbo].[IsCoverNotePrintable]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 30/09/2014
-- Description:	Determining if a cover note can be printed or not (by a company)
-- =============================================
CREATE PROCEDURE [dbo].[IsCoverNotePrintable]
	@vehicleCovNoteID int,
	@compId int,
	@insurerID int
AS
BEGIN
	
	SET NOCOUNT ON;
	
	IF (@compId = @insurerID)
	    BEGIN
			SELECT CAST(1 as bit) as result;
	    END

	ELSE
	  BEGIN
	  --Testing to ensure the company is an intermediary
		 IF EXISTS (SELECT CompanyID FROM Company WHERE CompanyID= @compId AND ([Type] =2 OR [Type] = 3))
		 BEGIN
			IF EXISTS (SELECT CTID FROM InsurerToIntermediary WHERE Intermediary=  @compId AND Insurer = @insurerID AND  ([enabled] IS NOT NULL AND [enabled] = 1) AND [CoverNote] = 1)
			BEGIN
				 IF EXISTS (SELECT VehicleCoverNoteID FROM VehicleCoverNote WHERE VehicleCoverNoteID = @vehicleCovNoteID 
										                AND (PrintCount = NULL OR PrintCount < (SELECT PrintLimit FROM InsurerToIntermediary WHERE Insurer=  @insurerID 
														                                                           AND Intermediary = @compId)))
					BEGIN
						SELECT CAST(1 as bit) as result;
					END
				 ELSE
				   BEGIN
				         IF EXISTS (SELECT VehicleCoverNoteID FROM VehicleCoverNote WHERE VehicleCoverNoteID = @vehicleCovNoteID AND PrintLimitOverride= 1)
										               
						      BEGIN
								  SELECT CAST(1 as bit) as result;
							  END
                        ELSE
						    BEGIN
						         SELECT CAST(0 as bit) as result;
						     END
				   END
			  END
			  ELSE --If the intermediary to insurer relationship does not exist, or if the intermediary is disabled to that insurer
				 BEGIN
					SELECT CAST(0 as bit) as result;
				 END
	       END
       END
  END

GO
/****** Object:  StoredProcedure [dbo].[IsFirstLogin]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsFirstLogin]
	-- Add the parameters for the stored procedure here
	@username varchar(250) = null,
	@uid int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT firstlogin FROM Users 
	WHERE (username = @username OR @username IS NULL) AND ([UID] = @uid OR @uid = 0);
	

END

GO
/****** Object:  StoredProcedure [dbo].[IsFirstLoginState]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsFirstLoginState]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT firstlogin FROM Users WHERE [uid] = @uid;
	
END


GO
/****** Object:  StoredProcedure [dbo].[IsGenerated]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 02/03/2015
-- Description:	Testing if a cover note/Certificate has been generated
-- =============================================
CREATE PROCEDURE [dbo].[IsGenerated]
	
	@id int,
	@type varchar (250)
AS
BEGIN

	SET NOCOUNT ON;
	

	IF (@type = 'CoverNote')

	BEGIN
		IF EXISTS(SELECT VehicleCoverNoteID FROM VehicleCoverNote WHERE VehicleCoverNoteID = @id AND Generated = 1)
		   BEGIN
			  SELECT CAST(1 as bit) [result];
		   END
		ELSE
		   BEGIN
			  SELECT CAST(0 as bit) [result];
		   END
	 END

	ELSE
	BEGIN
		IF EXISTS(SELECT VehicleCertificateID FROM VehicleCertificates WHERE VehicleCertificateID = @id AND Generated = 1)
		   BEGIN
			  SELECT CAST(1 as bit) [result];
		   END
		ELSE
		 BEGIN
			 SELECT CAST(0 as bit) [result];
		 END
	END

END

GO
/****** Object:  StoredProcedure [dbo].[IsInactiveCertificateMade]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 19/08/2014
-- Description:	Checking if a vehicle already has a inactive certificate under a given policy
-- =============================================
CREATE PROCEDURE [dbo].[IsInactiveCertificateMade]
	
	@policyID int,
	@VehicleID int
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @result bit;


	IF EXISTS (SELECT  VehicleCertificateID FROM  VehicleCertificates 
	                                        WHERE Vehiclecertificates.RiskItemNo =  @VehicleID
	                                              AND VehicleCertificates.PolicyID= @policyID
			                                      AND (approved IS NULL OR approved = 0))
     
	 BEGIN
		SET @result = 1;
		SELECT @result AS [result];
	 END


	 ELSE 
	 BEGIN
		SET @result = 0;
		SELECT @result AS [result];
	 END

END

GO
/****** Object:  StoredProcedure [dbo].[IsMultipleRisk]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 18/08/2014
-- Description:	Checking if the policy that a vehicle is under , has multiple risks insured
-- =============================================
CREATE PROCEDURE [dbo].[IsMultipleRisk]
	@polID int
AS
BEGIN 
	
	SET NOCOUNT ON;

    SELECT (CASE WHEN COUNT(VehicleID) > 1 THEN CAST(1 as bit) ELSE CAST(0 as bit) 
			END) as [result] FROM VehiclesUnderPolicy 
    WHERE PolicyID = @polID AND Cancelled =0;

END


GO
/****** Object:  StoredProcedure [dbo].[IsMyNotification]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsMyNotification]
	-- Add the parameters for the stored procedure here
	@nid int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN(COUNT(*) > 0) THEN (CAST (1 as bit)) ELSE (CAST (0 as bit)) END) [result]
	FROM Notifications n LEFT OUTER JOIN NotificationsTo nt ON n.NID = nt.[Notification]
	 WHERE n.NID = @nid AND nt.[UID] = @uid;
END


GO
/****** Object:  StoredProcedure [dbo].[IsPolicyCancelled]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 20/08/2014
-- Description:	Checking if a particular policy is cancelled
-- =============================================
CREATE PROCEDURE [dbo].[IsPolicyCancelled]
	
	@policyId int
AS
BEGIN
	
	SET NOCOUNT ON;
  DECLARE @result bit;

    IF EXISTS (SELECT PolicyID FROM Policies WHERE PolicyID = @policyId AND Cancelled = 1 )
	BEGIN
		SET @result = 1;
		SELECT @result AS [result];
	END

	ELSE
	BEGIN
		SET @result = 0;
		SELECT @result AS [result];
	END
END


GO
/****** Object:  StoredProcedure [dbo].[IsPrintedPaperNoUnique]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsPrintedPaperNoUnique]
	-- Add the parameters for the stored procedure here
	@ppn varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS(SELECT VehicleCoverNoteID FROM VehicleCoverNote WHERE PrintedPaperNo = @ppn)
		SELECT CAST(0 as bit) result;
	ELSE
		SELECT CAST(1 as bit) result;
END


GO
/****** Object:  StoredProcedure [dbo].[IsPrintRequested]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: <Create Date,,>
-- Description:	Checking if a print has been requested by an intermediary company, for a vehicle cert
-- =============================================
CREATE PROCEDURE [dbo].[IsPrintRequested]

	@id int,
	@type varchar (250)
AS
BEGIN

		SET NOCOUNT ON;
		IF (@type = 'Certificate')
		BEGIN
		  IF EXISTS (SELECT VehicleCertificateID FROM VehicleCertificates WHERE VehicleCertificateID = @id)
			 BEGIN
				 IF EXISTS (SELECT VehicleCertificateID FROM VehicleCertificates WHERE VehicleCertificateID = @id AND RequestPrint = 1)
				   BEGIN
					  SELECT CAST(1 as bit) as result;
				   END
				 ELSE 
				   BEGIN
					     SELECT CAST(0 as bit) as result;
				   END
			END
		END

		ELSE -- The type is cover note
		BEGIN
		  IF EXISTS (SELECT VehicleCoverNoteID FROM VehicleCoverNote WHERE VehicleCoverNoteID = @id)
			 BEGIN
				 IF EXISTS (SELECT VehicleCoverNoteID FROM VehicleCoverNote WHERE VehicleCoverNoteID = @id AND RequestPrint = 1)
				   BEGIN
					  SELECT CAST(1 as bit) as result;
				   END
				 ELSE 
				   BEGIN
					     SELECT CAST(0 as bit) as result;
				   END
			END
		END
END

GO
/****** Object:  StoredProcedure [dbo].[IsRealCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsRealCertificate]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN(COUNT(VehicleCertificateID)>0) THEN CAST (1 as bit) ELSE CAST (0 as bit) END) [result] FROM VehicleCertificates WHERE VehicleCertificateID = @id;
END


GO
/****** Object:  StoredProcedure [dbo].[IsRealPolicyID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsRealPolicyID] 
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN(COUNT(PolicyID) > 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result] FROM Policies WHERE PolicyID = @id;
END


GO
/****** Object:  StoredProcedure [dbo].[IsResetAccessible]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsResetAccessible]
	-- Add the parameters for the stored procedure here
	@guid uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
    DECLARE @result bit;
	DECLARE @diffmins int;
	DECLARE @resetrequesttime DATETIME;
	DECLARE @resetAvailable bit;

	SELECT @resetrequesttime = passwordresetrequesttime FROM Users WHERE resetguid = @guid;
	SELECT @resetAvailable = [reset] FROM Users WHERE resetguid = @guid;
	SELECT @diffmins= DATEDIFF(MINUTE, (SELECT passwordresetrequesttime FROM Users WHERE resetguid = @guid), CURRENT_TIMESTAMP);
	IF (@diffmins < 120 AND @resetAvailable = 1) SET @result = 1;
	ELSE SET @result = 0;
	SELECT @result as result;

END

GO
/****** Object:  StoredProcedure [dbo].[IsResetAccessibleByID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsResetAccessibleByID]
	-- Add the parameters for the stored procedure here
	@username varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
    DECLARE @result bit;
	DECLARE @diffmins int;
	DECLARE @resetrequesttime DATETIME;
	DECLARE @resetAvailable bit;

	SELECT @resetrequesttime = passwordresetrequesttime FROM Users WHERE username = @username;
	SELECT @resetAvailable = password_reset_force FROM Users WHERE username = @username;
	SELECT @diffmins= DATEDIFF(MINUTE, (SELECT passwordresetrequesttime FROM Users WHERE username = @username), CURRENT_TIMESTAMP);
	IF (@diffmins < 120 AND @resetAvailable = 1) SET @result = 1;
	ELSE SET @result = 0;
	SELECT @result as result;

END

GO
/****** Object:  StoredProcedure [dbo].[IsSuperGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsSuperGroup]
	-- Add the parameters for the stored procedure here
	@UGID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT SuperGroup FROM UserGroups WHERE UGID = @UGID;
END


GO
/****** Object:  StoredProcedure [dbo].[IsUnderactiveCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 19/08/2014
-- Description:	Checking if a vehicle already has an active certificate
-- =============================================
CREATE PROCEDURE [dbo].[IsUnderactiveCertificate]
	
	@startDateTime varchar (250),
	@endDateTime varchar (250),
	@VehicleID int
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @result bit;

	DECLARE @smallEnddatetime smalldatetime = @endDateTime;
	DECLARE @end datetime = @smallEnddatetime;

	DECLARE @smallstartdatetime smalldatetime = @startDateTime;
	DECLARE @start datetime = @smallstartdatetime;


	IF EXISTS (SELECT  VehicleCertificateID FROM VehicleCertificates 
			           LEFT JOIN Policies ON Policies.PolicyID = VehicleCertificates.PolicyID  AND Policies.Deleted = 0 
								AND ((@start >= Policies.StartDateTime AND @start <= Policies.EndDateTime) 
								OR (@end <= Policies.EndDateTime AND @end >= Policies.StartDateTime 
								OR (@start <= Policies.StartDateTime AND @end >= Policies.EndDateTime)))
		       WHERE VehicleCertificates.RiskItemNo = @VehicleID 
			                    AND (VehicleCertificates.Cancelled IS NULL OR VehicleCertificates.Cancelled = 0) 
			                    AND Vehiclecertificates.approved= 1)
     
	 BEGIN
		SET @result = 1;
		SELECT @result AS [result];
	 END


	 ELSE 
	 BEGIN
		SET @result = 0;
		SELECT @result AS [result];
	 END

END

GO
/****** Object:  StoredProcedure [dbo].[IsUsagesInUse]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsUsagesInUse]
	-- Add the parameters for the stored procedure here
	@id int,
	@cid int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN (COUNT(*)>0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result]
	FROM VehiclesUnderPolicy vup LEFT JOIN Policies p ON vup.PolicyID = p.PolicyID 
	WHERE vup.VehicleUsage = @id AND (@cid = 0 OR p.PolicyCover = @cid);
END

GO
/****** Object:  StoredProcedure [dbo].[IsValidUser]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsValidUser]
	-- Add the parameters for the stored procedure here
	@username varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS(SELECT [UID] FROM Users WHERE username = @username AND Deleted = 0 AND Filter = 0)
		SELECT CAST(1 as bit) [result];
	ELSE
		SELECT CAST(0 as bit) [result];
END


GO
/****** Object:  StoredProcedure [dbo].[IsVehicleCovered]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsVehicleCovered]
	-- Add the parameters for the stored procedure here
	@chassis varchar(MAX) = null,
	@license varchar(MAX) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT (CASE WHEN (COUNT(vcn.VehicleCoverNoteID) > 0 OR COUNT(vc.VehicleCertificateID) > 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result]
	FROM VehiclesUnderPolicy vup LEFT JOIN Policies p ON vup.PolicyID = p.PolicyID
								 LEFT JOIN Vehicles v ON vup.VehicleID = v.VehicleID
								 LEFT OUTER JOIN VehicleCoverNote vcn ON p.PolicyID = vcn.PolicyID AND v.VehicleID = vcn.RiskItemNo AND vcn.approved = 1 AND vcn.Cancelled = 0
								LEFT OUTER JOIN VehicleCertificates vc ON p.PolicyID = vc.PolicyID AND v.VehicleID = vc.RiskItemNo AND vc.approved = 1 AND vc.Cancelled = 0
	WHERE p.StartDateTime <= CURRENT_TIMESTAMP
	  AND p.EndDateTime >= CURRENT_TIMESTAMP
	  AND vup.Cancelled = 0
	  AND p.Cancelled = 0
	  AND  ((v.ChassisNo = @chassis OR @chassis IS NULL) 
			AND
			(v.VehicleRegistrationNo = @license OR @license IS NULL) 
			AND
			(@license IS NOT NULL OR @chassis IS NOT NULL));


END

GO
/****** Object:  StoredProcedure [dbo].[IsVehicleCoveredByID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsVehicleCoveredByID]
	-- Add the parameters for the stored procedure here
	@vehid int,
	@start Datetime,
	@end Datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT --pp.StartDateTime, p.EndDateTime, @effdate [effdate], DATEADD(DAY, @period, @effdate) [enddate]
	(CASE WHEN (COUNT(*) > 0) THEN (CAST (1 as bit)) ELSE (CAST (0 as bit)) END) [result]
	FROM Policies p LEFT JOIN VehiclesUnderPolicy vup ON p.PolicyID = vup.PolicyID
	WHERE vup.VehicleID = @vehid AND p.StartDateTime <= @start AND p.EndDateTime >= @end AND p.Cancelled = 0; -- testing if policy is still valid in time period

END


GO
/****** Object:  StoredProcedure [dbo].[IsVehicleUnderActivePolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 20/08/2014
-- Description:	Checking if a vehicle is under an active policy
-- =============================================
CREATE PROCEDURE [dbo].[IsVehicleUnderActivePolicy]

	@startDateTime varchar (250),
	@endDateTime varchar (250),
	@vehId int,
	@polID int
AS
BEGIN
	
	SET NOCOUNT ON;

    DECLARE @smallEnddatetime smalldatetime = @endDateTime;
	DECLARE @end datetime = @smallEnddatetime;

	DECLARE @smallstartdatetime smalldatetime = @startDateTime;
	DECLARE @start datetime = @smallstartdatetime;

	
	IF EXISTS (SELECT * FROM Policies RIGHT JOIN VehiclesUnderPolicy ON Policies.PolicyID = VehiclesUnderPolicy.PolicyID WHERE VehiclesUnderPolicy.VehicleID = @vehId 
	                                  AND Policies.Cancelled != 1
	                                  AND VehiclesUnderPolicy.Cancelled != 1 
									  AND Policies.Deleted = 0
	                                  AND Policies.PolicyID != @polID
									  AND ((@start >= Policies.StartDateTime AND @start <= Policies.EndDateTime) OR (@end <= Policies.EndDateTime AND @endDateTime >= Policies.StartDateTime 
									  OR (@start <= Policies.StartDateTime AND @end >= Policies.EndDateTime))))

		SELECT CAST(1 AS bit) AS result;
	ELSE 
		SELECT CAST(0 AS bit) AS result;
END


GO
/****** Object:  StoredProcedure [dbo].[IsVehicleUnderPol]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 19/08/2014
-- Description:	Checking whether or not a vehicle is under a policy, that is active (not cancelled), under a particular time-frame
-- =============================================
CREATE PROCEDURE [dbo].[IsVehicleUnderPol]

	    @vehId int
AS
BEGIN
	
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT * FROM Policies LEFT JOIN VehiclesUnderPolicy ON Policies.PolicyID = VehiclesUnderPolicy.PolicyID WHERE VehiclesUnderPolicy.VehicleID = @vehId 
	                                  AND VehiclesUnderPolicy.Cancelled != 1
									  AND Policies.Deleted = 0
									 	 AND (CURRENT_TIMESTAMP >= Policies.StartDateTime AND CURRENT_TIMESTAMP <= Policies.EndDateTime))

		SELECT CAST(1 as bit) as result;
	ELSE 
		SELECT CAST(0 as bit) as result;
END


GO
/****** Object:  StoredProcedure [dbo].[IsVehicleUnderPolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 19/08/2014
-- Description:	Checking whether or not a vehicle is under a policy, that is active (not cancelled), under a particular time-frame
-- =============================================
CREATE PROCEDURE [dbo].[IsVehicleUnderPolicy]

	@startDateTime varchar (250),
	@endDateTime varchar (250),
	@vehId int,
	@policyid int
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @smallEnddatetime smalldatetime = @endDateTime;
	DECLARE @end datetime = @smallEnddatetime;

	DECLARE @smallstartdatetime smalldatetime = @startDateTime;
	DECLARE @start datetime = @smallstartdatetime;
	
	IF (@policyid != 0) 
	  BEGIN
			IF EXISTS (SELECT Policies.PolicyID 
					   FROM   Policies LEFT JOIN VehiclesUnderPolicy ON Policies.PolicyID = VehiclesUnderPolicy.PolicyID 
					   WHERE  VehiclesUnderPolicy.VehicleID = @vehId 
							  AND Policies.PolicyID = @policyid
							  AND VehiclesUnderPolicy.Cancelled = 0
							  AND Policies.Deleted = 0
							  AND ((@start >= Policies.StartDateTime AND @start <= Policies.EndDateTime) OR (@end <= Policies.EndDateTime AND @endDateTime >= Policies.StartDateTime 
								  OR (@start <= Policies.StartDateTime AND @end >= Policies.EndDateTime))))

				SELECT CAST(1 as bit) as result;
			ELSE 
				SELECT CAST(0 as bit) as result;
	 END

	 ELSE
	   BEGIN

	     IF EXISTS (SELECT Policies.PolicyID 
					   FROM   Policies LEFT JOIN VehiclesUnderPolicy ON Policies.PolicyID = VehiclesUnderPolicy.PolicyID 
					   WHERE  VehiclesUnderPolicy.VehicleID = @vehId 
							  AND VehiclesUnderPolicy.Cancelled = 0
							  AND Policies.Deleted = 0
							  AND ((@start >= Policies.StartDateTime AND @start <= Policies.EndDateTime) OR (@end <= Policies.EndDateTime AND @endDateTime >= Policies.StartDateTime 
								  OR (@start <= Policies.StartDateTime AND @end >= Policies.EndDateTime))))

				SELECT CAST(1 as bit) as result;
			ELSE 
				SELECT CAST(0 as bit) as result;
	  END
END

GO
/****** Object:  StoredProcedure [dbo].[IsVehicleUnderPolicyAtMyCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 02/10/2014
-- Description:	Testing if a vehicle is under a policy, at the user's company, within a specific timeframe
-- =============================================
CREATE PROCEDURE [dbo].[IsVehicleUnderPolicyAtMyCompany]
	
	@startDateTime DateTime,
	@endDateTime DateTime,
	@vehId int,
	@compID int
AS
BEGIN
	
	SET NOCOUNT ON;

	
	IF EXISTS (SELECT Policies.PolicyID FROM Policies LEFT JOIN VehiclesUnderPolicy ON Policies.PolicyID = VehiclesUnderPolicy.PolicyID 
	           WHERE VehiclesUnderPolicy.VehicleID = @vehId 
			         AND VehiclesUnderPolicy.Cancelled != 1
					 AND Policies.Deleted = 0
					 AND Policies.CompanyID = @compID
					 AND ((@startDateTime >= Policies.StartDateTime AND @startDateTime <= Policies.EndDateTime) OR (@endDateTime <= Policies.EndDateTime AND @endDateTime >= Policies.StartDateTime 
									                                                            OR (@startDateTime <= Policies.StartDateTime AND @endDateTime >= Policies.EndDateTime))))


		SELECT CAST(1 as bit) as result;
	ELSE 
		SELECT CAST(0 as bit) as result;

END

GO
/****** Object:  StoredProcedure [dbo].[IsVehUnderActiveNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 19/08/2014
-- Description:	Checking if a vehicle already has an active cover note
-- =============================================
CREATE PROCEDURE [dbo].[IsVehUnderActiveNote]
	
   @VehicleID int
	
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @result bit;

	--DECLARE @smallEnddatetime smalldatetime = @endDateTime;
	--DECLARE @end datetime = @smallEnddatetime;

	--DECLARE @smallstartdatetime smalldatetime = @startDateTime;
	--DECLARE @start datetime = @smallstartdatetime;


	IF EXISTS (SELECT  VehicleCoverNoteID FROM VehicleCoverNote 
		       WHERE   RiskItemNo = @VehicleID 
			           AND ((Cancelled IS NULL OR Cancelled = 0) AND approved= 1) 
			           AND (CURRENT_TIMESTAMP >= EffectiveDate AND CURRENT_TIMESTAMP <= ExpiryDateTime))
			                                                    
	 BEGIN
		SET @result = 1;
		SELECT @result AS [result];
	 END


	 ELSE 
	 BEGIN
		SET @result = 0;
		SELECT @result AS [result];
	 END

END

GO
/****** Object:  StoredProcedure [dbo].[isVehUnderThisPolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 22/09/2014
-- Description:	Checking if a vehicle is currently covered by a particular policy
-- =============================================
CREATE PROCEDURE [dbo].[isVehUnderThisPolicy]
	@polId int,
	@vehId int
AS
BEGIN
	
	SET NOCOUNT ON;


	IF EXISTS (SELECT VUPID FROM VehiclesUnderPolicy WHERE PolicyID = @polId AND VehicleID = @vehId AND Cancelled = 0)

		SELECT CAST(1 as bit) as result;
	ELSE 
		SELECT CAST(0 as bit) as result;

END


GO
/****** Object:  StoredProcedure [dbo].[JsonFailed]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[JsonFailed]
	-- Add the parameters for the stored procedure here
	@id int,
	@processed nvarchar(MAX) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (@id != 0 AND @id IS NOT NULL)
	BEGIN
		UPDATE JsonAudit SET failure = 1, completed = 1, inprogress = 0 WHERE ID = @id;
		IF (@processed IS NOT NULL) UPDATE JsonAudit SET processedjson = @processed WHERE ID = @id;
	END
END


GO
/****** Object:  StoredProcedure [dbo].[LockMappingData]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[LockMappingData]
	-- Add the parameters for the stored procedure here
	@mapid int,
	@userID int
AS
BEGIN
	
	SET NOCOUNT ON;

    DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES 

	SELECT	@origdata = 'Company: '+ CONVERT(varchar(MAX),CompanyID) + 'lock: ' + CONVERT(varchar(MAX),[lock]) FROM MappingTable WHERE ID = @mapid;

	UPDATE MappingTable SET [lock] = 1 WHERE ID = @mapid;

	SELECT @details = ('The record ' + CONVERT(varchar(MAX), @mapid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID)),
		   @newdata = 'Company: '+ CONVERT(varchar(MAX),CompanyID) + 'lock: ' + CONVERT(varchar(MAX),[lock]) FROM MappingTable WHERE ID = @mapid;

    EXEC LogAudit 'update', @userID , @details, 'MappingTable',  @@IDENTITY, @origdata, @newdata, 'update';

END
GO
/****** Object:  StoredProcedure [dbo].[LogAudit]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[LogAudit]
	-- Add the parameters for the stored procedure here
	@action text,
	@auid int = null,
	@details text,
	@location text,
	@recordid int,
	@origdata text,
	@newdata text,
	@audittype varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
						VALUES (@action, @auid, CURRENT_TIMESTAMP, @details, @location, @recordid, @origdata, @newdata, @audittype);
END


GO
/****** Object:  StoredProcedure [dbo].[LogCert]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[LogCert]
	-- Add the parameters for the stored procedure here
	@polid int,
	@certid int,
	@date datetime,
	@riskid int,
	@userid int,
	@compid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO CertIntel([date], certID, policyID, riskID, userID, companyID)
	VALUES (@date, @certid, @polid, @riskid, @userid, @compid);

END

GO
/****** Object:  StoredProcedure [dbo].[LogEnquiry]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[LogEnquiry]
	-- Add the parameters for the stored procedure here
	@license varchar(MAX) = NULL,
	@chassis varchar(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Enquiries (chassis, license) 
	VALUES ((CASE WHEN (@chassis = '') THEN NULL ELSE @chassis END), (CASE WHEN (@license = '') THEN NULL ELSE @license END));
	SELECT @@IDENTITY [id];
END

GO
/****** Object:  StoredProcedure [dbo].[LogFailedEmail]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[LogFailedEmail]
	-- Add the parameters for the stored procedure here
	@to varchar(250),
	@body varchar(250),
	@sub varchar(250),
	@error varchar(250),
	@username varchar(250) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @auditdesc varchar(8000);
	SET @auditdesc = 'Email to: ' + @to + '|Message Subject: ' + @sub + '| Message Contents: ' + @body + '|Error System Made: ' + @error;
	INSERT INTO [EmailFailures](AuditDesc, AuditType, UserLogged) VALUES (@auditdesc,'email',(CASE WHEN (@username IS NULL OR @username = '') THEN (SELECT [UID] FROM Users WHERE username = 'system') ELSE (SELECT [UID] FROM Users WHERE username = @username) END));
END


GO
/****** Object:  StoredProcedure [dbo].[Logout]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Logout]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES

	SELECT	@id = @uid,
			@details = 'The record ' + CONVERT(varchar(MAX),@id) + ' was modified from the database by ' + CONVERT(varchar(MAX),[UID]),
			@origdata = 'username:' +  username +  ';faultylogins:' + CONVERT(varchar(MAX),faultylogins) + ';loggedin:' + CONVERT(varchar(MAX), loggedin)
	FROM Users
	WHERE [UID] = @uid

    -- Insert statements for procedure here
	UPDATE Users SET LoggedIn = 0, LoginGUID = NULL WHERE [UID] = @uid;

	SELECT	@newdata = 'username:' +  username +  ';faultylogins:' + CONVERT(varchar(MAX),faultylogins) + ';loggedin:' + CONVERT(varchar(MAX), loggedin)
	FROM Users
	WHERE [UID] = @uid

	EXEC LogAudit 'update', @id  , @details, 'Users', @id, @origdata, @newdata, 'update';

END


GO
/****** Object:  StoredProcedure [dbo].[MakePrintable]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: 30/09/2014
-- Description:	Making a certificate or cover note printable for a user
-- =============================================
CREATE PROCEDURE [dbo].[MakePrintable]
	
	@type varchar(250),
	@ID int,
	@modifiedBy int
	
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX);  -- LOG AUDIT VARIABLES

	IF (@type = 'Certificate')
	BEGIN
	    
		SELECT @origdata = 'VehicleCertificateID: ' + CONVERT(varchar(MAX), @ID) +';LastPrintedBy: ' + CONVERT(varchar(MAX),LastPrintedBy) + ';LastPrintedOn:' + CONVERT(varchar(MAX),LastPrintedOn) 
		                  +';PrintCount:' + CONVERT(varchar(MAX), PrintCount) + ';PrintLimitOverride: '+ CONVERT(varchar(MAX), PrintLimitOverride) 
		FROM VehicleCertificates WHERE VehicleCertificateID =@ID;


		UPDATE VehicleCertificates Set PrintLimitOverride =1, RequestPrint= 0, LastModifiedBy=@modifiedBy
        WHERE VehicleCertificates.VehicleCertificateID = @ID;

	    SELECT CAST(1 as bit) as result;

		SELECT @details = ('The record ' + CONVERT(varchar(MAX),@ID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@modifiedBy)),
			   @newdata = 'VehicleCertificateID:' + CONVERT(varchar(MAX), @ID) +'LastPrintedBy:' + CONVERT(varchar(MAX),LastPrintedBy) + ';LastPrintedOn:' + CONVERT(varchar(MAX),LastPrintedOn) 
	                  +';PrintCount:' + CONVERT(varchar(MAX), PrintCount) + ';PrintLimitOverride: '+ CONVERT(varchar(MAX), PrintLimitOverride)
	          FROM VehicleCertificates WHERE VehicleCertificateID =@ID;

	    EXEC LogAudit 'update', @modifiedBy , @details, 'VehicleCertificates',  @ID, @origdata, @newdata, 'update';

	END

	ELSE
	BEGIN

	    SELECT @origdata = 'VehicleCoverNoteID: ' + CONVERT(varchar(MAX), @ID) + ';LastPrintedBy: ' + CONVERT(varchar(MAX),LastPrintedBy) + ';LastPrintedOn:' + CONVERT(varchar(MAX),LastPrinted) 
		                   +';PrintCount:' + CONVERT(varchar(MAX), PrintCount) + ';PrintLimitOverride:'+ CONVERT(varchar(MAX), PrintLimitOverride) 
		FROM VehicleCoverNote WHERE VehicleCoverNoteID =@ID;   


		UPDATE VehicleCoverNote Set PrintLimitOverride =1, RequestPrint = 0, LastPrintedBy= (SELECT People.PeopleID FROM Users 
		                                                                                     LEFT JOIN Employees ON Employees.EmployeeID = Users.EmployeeID 
																							 LEFT JOIN People ON People.PeopleID = Employees.PersonID WHERE Users.UID = @modifiedBy), 
																							 LastPrinted = CURRENT_TIMESTAMP
	    WHERE VehicleCoverNote.VehicleCoverNoteID = @ID;


	    SELECT CAST(1 as bit) as result;

		SELECT @details = ('The record ' + CONVERT(varchar(MAX),@ID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@modifiedBy)),
			   @newdata = 'VehicleCoverNoteID:' + CONVERT(varchar(MAX), @ID) +'LastPrintedBy:' + CONVERT(varchar(MAX),LastPrintedBy) + ';LastPrintedOn:' + CONVERT(varchar(MAX),LastPrinted) 
	                      +';PrintCount:' + CONVERT(varchar(MAX), PrintCount) + ';PrintLimitOverride:'+ CONVERT(varchar(MAX), PrintLimitOverride)
	          FROM VehicleCoverNote WHERE VehicleCoverNoteID =@ID;

	    EXEC LogAudit 'update', @modifiedBy , @details, 'VehicleCoverNote',  @ID, @origdata, @newdata, 'update';
	END
	
END

GO
/****** Object:  StoredProcedure [dbo].[MarkNotifcationDeleted]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MarkNotifcationDeleted]
	-- Add the parameters for the stored procedure here
	@nid int,
	@updatedby int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES
	SELECT @origdata = 'Notification:' +  CONVERT(varchar(MAX),@nid) +  ';NoteFrom:' + CONVERT(varchar(MAX),NoteFrom) + ';Heading:' + CONVERT(varchar(MAX),Heading) + ';Content:' + CONVERT(varchar(MAX),Content) + ';Seen:'  + CONVERT(varchar(MAX),Seen)  + ';Read:' + CONVERT(varchar(MAX),[Read]) + ';Deleted:' + CONVERT(varchar(MAX),[Deleted]) + ';New:' + CONVERT(varchar(MAX),[New])
		FROM Notifications
		WHERE Notifications.NID = @nid;

	UPDATE Notifications SET [Deleted] = 1 WHERE NID = @nid;

	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@nid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@updatedby),
		   @newdata = 'Notification:' +  CONVERT(varchar(MAX),@nid) +  ';NoteFrom:' + CONVERT(varchar(MAX),NoteFrom) + ';Heading:' + CONVERT(varchar(MAX),Heading) + ';Content:' + CONVERT(varchar(MAX),Content) + ';Seen:'  + CONVERT(varchar(MAX),Seen)  + ';Read:' + CONVERT(varchar(MAX),[Read]) + ';Deleted:' + CONVERT(varchar(MAX),[Deleted]) + ';New:' + CONVERT(varchar(MAX),[New])
		FROM Notifications
		WHERE Notifications.NID = @nid;
	EXEC LogAudit 'update', @updatedby , @details, 'Notifications', @nid, @origdata, @newdata, 'update';


END


GO
/****** Object:  StoredProcedure [dbo].[MarkNotifcationRead]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MarkNotifcationRead]
	-- Add the parameters for the stored procedure here
	@nid int,
	@updatedby int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES
	SELECT @origdata = 'Notification:' +  CONVERT(varchar(MAX),@nid) +  ';NoteFrom:' + CONVERT(varchar(MAX),NoteFrom) + ';Heading:' + CONVERT(varchar(MAX),Heading) + ';Content:' + CONVERT(varchar(MAX),Content) + ';Seen:'  + CONVERT(varchar(MAX),Seen)  + ';Read:' + CONVERT(varchar(MAX),[Read]) + ';Deleted:' + CONVERT(varchar(MAX),[Deleted]) + ';New:' + CONVERT(varchar(MAX),[New])
		FROM Notifications
		WHERE Notifications.NID = @nid;

	UPDATE Notifications SET [Read] = 1 WHERE NID = @nid;

	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@nid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@updatedby),
		   @newdata = 'Notification:' +  CONVERT(varchar(MAX),@nid) +  ';NoteFrom:' + CONVERT(varchar(MAX),NoteFrom) + ';Heading:' + CONVERT(varchar(MAX),Heading) + ';Content:' + CONVERT(varchar(MAX),Content) + ';Seen:'  + CONVERT(varchar(MAX),Seen)  + ';Read:' + CONVERT(varchar(MAX),[Read]) + ';Deleted:' + CONVERT(varchar(MAX),[Deleted]) + ';New:' + CONVERT(varchar(MAX),[New])
		FROM Notifications
		WHERE Notifications.NID = @nid;
	EXEC LogAudit 'update', @updatedby , @details, 'Notifications', @nid, @origdata, @newdata, 'update';
	


END


GO
/****** Object:  StoredProcedure [dbo].[MarkNotifcationSeen]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MarkNotifcationSeen]
	-- Add the parameters for the stored procedure here
	@nid int,
	@updatedby int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES
	SELECT @origdata = 'Notification:' +  CONVERT(varchar(MAX),@nid) +  ';NoteFrom:' + CONVERT(varchar(MAX),NoteFrom) + ';Heading:' + CONVERT(varchar(MAX),Heading) + ';Content:' + CONVERT(varchar(MAX),Content) + ';Seen:'  + CONVERT(varchar(MAX),Seen)  + ';Read:' + CONVERT(varchar(MAX),[Read]) + ';Deleted:' + CONVERT(varchar(MAX),[Deleted]) + ';New:' + CONVERT(varchar(MAX),[New])
		FROM Notifications
		WHERE Notifications.NID = @nid;

	UPDATE Notifications SET [Seen] = 1 WHERE NID = @nid;

	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@nid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@updatedby),
		   @newdata = 'Notification:' +  CONVERT(varchar(MAX),@nid) +  ';NoteFrom:' + CONVERT(varchar(MAX),NoteFrom) + ';Heading:' + CONVERT(varchar(MAX),Heading) + ';Content:' + CONVERT(varchar(MAX),Content) + ';Seen:'  + CONVERT(varchar(MAX),Seen)  + ';Read:' + CONVERT(varchar(MAX),[Read]) + ';Deleted:' + CONVERT(varchar(MAX),[Deleted]) + ';New:' + CONVERT(varchar(MAX),[New])
		FROM Notifications
		WHERE Notifications.NID = @nid;
	EXEC LogAudit 'update', @updatedby , @details, 'Notifications', @nid, @origdata, @newdata, 'update';
END


GO
/****** Object:  StoredProcedure [dbo].[MarkPolicyImported]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MarkPolicyImported]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Policies SET imported = 1 WHERE PolicyID = @id;
END

GO
/****** Object:  StoredProcedure [dbo].[ModifyEmailOfPerson]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 17/06/2014
-- Description:	Updates a person's email saved on the system
-- =============================================

CREATE PROCEDURE [dbo].[ModifyEmailOfPerson]
	-- Add the parameters for the stored procedure here
	@personId int,
	@emailId int
	
	
AS
BEGIN
	SET NOCOUNT ON;


	UPDATE PeopleToEmails SET EmailID = @emailId WHERE PeopleId = @personId;

	--SELECT PeopleId FROM People WHERE PeopleId = @personId;
	
END


GO
/****** Object:  StoredProcedure [dbo].[ModifyPerson]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 10/06/2014
-- Description:	Updates a person's information
-- =============================================

CREATE PROCEDURE [dbo].[ModifyPerson]
	-- Add the parameters for the stored procedure here
	@pid int,
	@fname varchar (250),
	@mname varchar (250),
	@lname varchar (250), 
	@address int,
	@trn varchar (250),
	@EditedBy int
	
	
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @PerId int; SELECT @PerId = PeopleID FROM People WHERE TRN = @trn;

	SELECT @origdata = 'FirstName: ' + FName + ';MiddleName: ' + MName + ';LastName: ' + LName + ';TRN: ' + TRN FROM People WHERE PeopleID = @pid;
	
	UPDATE People SET FName = @fname, MName = @mname, LName = @lname , [Address]= @address, LastModifiedBy = @EditedBy, TRN = @trn WHERE PeopleID = @pid;


	SELECT @details = ('The record ' + CONVERT(varchar(MAX),@PerId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@EditedBy)),
		    @newdata = ('FirstName: ' + CONVERT(varchar(MAX),@fname)+ ';MiddleName: ' + CONVERT(varchar(MAX),@mname) + ';LastName: ' + CONVERT(varchar(MAX),@lname) + ';AddressID: ' + CONVERT(varchar(MAX),@address));
    EXEC LogAudit 'update', @EditedBy , @details, 'People',  @PerId, @origdata, @newdata, 'update';

END

GO
/****** Object:  StoredProcedure [dbo].[modifyPolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 24/06/2014
-- Description: Modifying a policy in the system
-- =============================================

CREATE PROCEDURE [dbo].[modifyPolicy]
	-- Add the parameters for the stored procedure here
	@startDateTime varchar (250),
	@endDateTime varchar (250),
	@InsuredType varchar (250),
	@policyNumber varchar (50),
	@policyCoverId int,
	@scheme bit,
	@billingaddress int,
	@mailingaddress int,
	@policyId int,
	@mailshort varchar(100) = null,
	@billshort varchar(100) = null,
	@editedBy int

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @smallEnddatetime smalldatetime = @endDateTime;
	DECLARE @end datetime = @smallEnddatetime;

	DECLARE @smallstartdatetime smalldatetime = @startDateTime;
	DECLARE @start datetime = @smallstartdatetime;

	SELECT @origdata = 'EDIID:' + CONVERT(varchar(MAX),EDIID)+ ';CompanyID:' + CONVERT(varchar(MAX),Policies.CompanyID) + ';StartDateTime:' + CONVERT(varchar(MAX),StartDateTime) 
	                    +';EndDateTime:' + CONVERT(varchar(MAX),EndDateTime)+';PolicyNumber:' + Policyno + ';PolicyCover:' 
						+ CONVERT(varchar(MAX),PolicyCovers.Cover) + ';InsuredType:' + CONVERT(varchar(MAX),PolicyInsuredType.InsuredType)
			         FROM Policies 
							 JOIN PolicyCovers ON PolicyCovers.ID= Policies.PolicyCover 
							 JOIN PolicyInsuredType ON PolicyInsuredType.PITID = Policies.InsuredType
					 WHERE PolicyID = @policyId;


	UPDATE Policies 
	SET StartDateTime = @start,
		EndDateTime = @end, 
		Policyno = @policyNumber, 
		PolicyCover =@policyCoverId, 
		LastModifiedBy = @editedBy, 
		mailing_address_short = @mailshort, 
		billing_address_short = @billshort,
		InsuredType = (SELECT PITID FROM PolicyInsuredType WHERE InsuredType = @InsuredType), 
		Scheme = @scheme, 
		BillingAddress = @billingaddress, 
		MailingAddress = @mailingaddress
	WHERE PolicyId = @policyId;


	SELECT @details = ('The record ' + CONVERT(varchar(MAX),@policyId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy)),
			@newdata = ('StartDateTime:' + CONVERT(varchar(MAX),@startDateTime) + ';EndDateTime:' + CONVERT(varchar(MAX),@endDateTime) + 
						+ ';PolicyNumber:' + CONVERT(varchar(MAX),@policyNumber) + ';PolicyCover:' 
						+ (SELECT Cover FROM Policycovers WHERE PolicyCovers.ID= @policyCoverId) + ';InsuredType:' + @InsuredType); 

	EXEC LogAudit 'update', @editedBy , @details, 'Policies',  @policyId, @origdata, @newdata, 'update';

END





GO
/****** Object:  StoredProcedure [dbo].[ModifyUser]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 09/06/2014
-- Description:	Updates a user's information
-- =============================================

 CREATE PROCEDURE [dbo].[ModifyUser]
	-- Add the parameters for the stored procedure here
	
	@username varchar (55),
	@UserId int
	
	
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE Users Set username= @username  WHERE UID = @UserId;

	
END


GO
/****** Object:  StoredProcedure [dbo].[ModifyVehicle]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 20/06/2014
-- Description:	Modifies a vehicle to the system
-- =============================================

CREATE PROCEDURE [dbo].[ModifyVehicle]
	-- Add the parameters for the stored procedure here
	@make varchar (250),
	@model varchar (250), 
	@modelType varchar (150)= null,
	@bodyType varchar (150)= null,
	@extention varchar (150)= null,
	@registrationNum varchar (150),
	@handDriveType varchar (250)= null, --receives a varchar, to search vehicleHandriveLookUp table for the ID (if it exists)
	@colour varchar (50)= null,
	@engineNo varchar (150)= null,
	@engineModified bit,
	@engineType varchar (50)= null,
	@HPCCUnittype varchar (150)= null,
	@regLocation varchar (250)= null,
	@roofType varchar (150)= null,
	@transmissionType varchar (250)= null, -- receives a varchar, to search transmissionType table for the ID (if it exists)
	@expiryDateOffitness date= null,
	@RegisteredOwner varchar (MAX)= null,
	@RestrictedDriving bit,
	@seating int= null,
	@seatingCert int= null, 
	@tonnage int= null,
	@vehicleYear int,
	@cylinders int= null,
	@estAnnualMileage int= null,
	@mainDriver int = 0, -- the Driver ID of the vehicle's main driver
	@referenceNo varchar(MAX)= null,
	@VehicleID int, 
	@EditedBy int


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @handDriveTypeId int;
	DECLARE @transmissionTypeId int;
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES


	--IF THE HAND DRIVE IS EMPTY, THE VALUE IS NULL
	IF (@handDriveType = '')
	SET @handDriveTypeId= 0;

	ELSE
	  BEGIN
		 IF EXISTS (SELECT VHDLID FROM VehicleHandDriveLookUp WHERE HDType = @handDriveType)
			 BEGIN
				 SELECT @handDriveTypeId = VHDLID FROM VehicleHandDriveLookUp WHERE HDType = @handDrivetype;
			 END

		  ELSE
			BEGIN
				 INSERT INTO VehicleHandDriveLookUp (HDType, CreatedBy, CreatedOn, LastModifiedBy) Values (@handDriveType, @EditedBy, CURRENT_TIMESTAMP, @EditedBy);
				 SET @handDriveTypeId= @@IDENTITY; 
 
				 SELECT @details = ('The record ' + CONVERT(varchar(MAX),@handDriveTypeId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@EditedBy)),
						   @origdata = null,
						   @newdata = 'HandDriveType:' + @handDriveType;
				 EXEC LogAudit 'insert', @EditedBy , @details, 'VehicleHandDriveTypeLookUp',  @handDriveTypeId, @origdata, @newdata, 'insert';
			END
	  END


	 --IF THE TRANSMISSION TYPE IS EMPTY, THE VALUE IS NULL
    IF (@transmissionType = '')
	SET @transmissionTypeId= null;

	ELSE
	BEGIN
		IF EXISTS (SELECT TransmisionTypeID FROM TransmissionTypeLookUp WHERE TransmissionType = @transmissionType)
			BEGIN
				SELECT @transmissionTypeId = TransmisionTypeID FROM TransmissionTypeLookUp WHERE TransmissionType = @transmissionType;
			END

		ELSE
			BEGIN
				INSERT INTO TransmissionTypeLookUp (TransmissionType, CreatedBy, CreatedOn, LastModifiedBy) Values (@transmissionType, @EditedBy, CURRENT_TIMESTAMP, @EditedBy);
				SET @transmissionTypeId= @@IDENTITY; 

				SELECT @details = ('The record ' + CONVERT(varchar(MAX),@transmissionTypeId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@EditedBy)),
						   @origdata = null,
						   @newdata = 'TransmissionType:' + @transmissionType;
				EXEC LogAudit 'insert', @EditedBy , @details, 'TransmissionTypeLookUp',  @transmissionTypeId, @origdata, @newdata, 'insert';

	    END
     END


		SELECT @details = ('The record ' + CONVERT(varchar(MAX),@VehicleID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@EditedBy)),
					   @origdata = 'Make:' + Make+ ';Model:' + Model+ ';ModelType:' + ModelType + ';BodyType:' + BodyType+';Extension:' + Extension+';RegistrationNumber:' + RegistrationLocation+';VIN:' +  CONVERT(varchar(MAX),CASE WHEN (VIN IS NULL) THEN 'NULL ' ELSE VIN END )+';HandDriveId:' +  CONVERT(varchar(MAX),CASE WHEN (HandDrive IS NULL) THEN '' ELSE HandDrive END )+
					              ';Colour:' + Colour +';Cylinders:' + CONVERT(varchar(MAX),Cylinders)+ ';EngineNo:' + EngineNo + ';EngineModified:' + CONVERT(varchar(MAX),EngineModified) + ';EngineType:' + EngineType + ';EstimatedAnnualMileage:' +  CONVERT(varchar(MAX),EstimatedAnnualMileage) + 
								  ';ExpirydateOfFitness:' + CONVERT(varchar(MAX),ExpiryDateofFitness)+ ';HPCCUnitType:'+ HPCCUnitType +';MainDriverID:' + CONVERT(varchar(MAX),MainDriver) +';ReferenceNo:' + CONVERT(varchar(MAX),ReferenceNo)+';RegistrationLocation:' + RegistrationLocation+';RoofType:' + RoofType+ 
								  ';Seating:' +  CONVERT(varchar(MAX),Seating)+ ';SeatingCert:' + CONVERT(varchar(MAX),SeatingCert)+ ';Tonnage:' + CONVERT(varchar(MAX),Tonnage)+ ';TransmissionTypeId:' + CONVERT(varchar(MAX),CASE WHEN (TransmissionType IS NULL) THEN '' ELSE TransmissionType END )+ ';VehicleYear:' + CONVERT(varchar(MAX),VehicleYear) 
								  FROM Vehicles WHERE VehicleID = @VehicleID;


		UPDATE Vehicles 
		SET Make=@make,  
			Model=@model, 
			ModelType= @modelType, 
			BodyType= @bodyType, 
			Extension= @extention, 
			VehicleRegistrationNo= @registrationNum,
			HandDrive= (CASE WHEN(@handDriveTypeId = 0) THEN NULL ELSE @handDriveTypeId END), 
			Colour=  @colour, 
			Cylinders = @cylinders, 
			EngineNo= @engineNo,  
			EngineModified=  @engineModified,
			EngineType= @engineType, 
			EstimatedAnnualMileage= @estAnnualMileage,
			ExpiryDateofFitness= @expiryDateOffitness, 
			HPCCUnitType= @HPCCUnittype,
			MainDriver= (CASE WHEN(@mainDriver = 0) THEN NULL ELSE @mainDriver END), 
			ReferenceNo= @referenceNo, 
			RegistrationLocation= @regLocation, 
			RoofType= @roofType, 
			Seating= @seating, 
			SeatingCert=@seatingCert,
			Tonnage= @tonnage, 
			TransmissionType= @transmissionTypeId, 
			VehicleYear = @vehicleYear, 
			RegisteredOwner = @RegisteredOwner, 
			RestrictedDriving = @RestrictedDriving,
			LastModifiedBy = @EditedBy 
		WHERE VehicleID = @vehicleID;

					  
		 SET  @newdata = 'Make:' + @make+ ';Model:' + @model+ ';ModelType:' + @modelType+ ';BodyType:' + @bodyType+';Extension:' + @extention+';RegistrationNumber:' + @registrationNum + ';HandDrive:' + @handDriveType+
						';Colour:' + @colour +';Cylinders:' +  CONVERT(varchar(MAX),@cylinders)+ ';EngineNo:' + @engineNo+ ';EngineModified:' + CONVERT(varchar(MAX),@engineModified)+ ';EngineType:' + @engineType+ ';EstimatedAnnualMileage:' +  CONVERT(varchar(MAX),@estAnnualMileage) + 
						';ExpirydateOfFitness:' + CONVERT(varchar(MAX),@expiryDateOffitness)+ ';HPCCUnitType:' + @HPCCUnittype +';MainDriverID:' + CONVERT(varchar(MAX),@mainDriver) +';ReferenceNo:' + CONVERT(varchar(MAX),@referenceNo)+';RegistrationLocation:' + @regLocation+';RoofType:' + @roofType+ 
						';Seating:' + CONVERT(varchar(MAX),@seating)+ ';SeatingCert:' + CONVERT(varchar(MAX),@seatingCert)+ ';Tonnage:' + CONVERT(varchar(MAX),@tonnage)+ ';TransmissionType:' + @transmissionType+ ';VehicleYear:' + CONVERT(varchar(MAX),@vehicleYear)+ 
						';RegisteredOwner:' + @RegisteredOwner + ';RestrictedDriving:' + CONVERT(varchar(MAX),@restrictedDriving);

       EXEC LogAudit 'update', @EditedBy , @details, 'Vehicles',  @VehicleID, @origdata, @newdata, 'update';

END

GO
/****** Object:  StoredProcedure [dbo].[MyCompanyList]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MyCompanyList]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @cid int; SELECT @cid = e.CompanyID FROM Users u LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID WHERE u.[UID] = @uid;
	
	
	SELECT 
	Company.CompanyID [ID] , Company.CompanyName [CompanyName], Company.IAJID [IAJID], CompanyTypes.Type [type], Company.Enabled [enabled],
	[Address].RoadNumber [roadnum], RoadName.RoadName [road], RoadType.RoadType [rtype], ZipCodes.ZipCode [zcode], Cities.City [city], Countries.CountryName [country],
	Parishes.Parish [parish], COUNT(Employees.EmployeeID) [empcount] 
	FROM Company LEFT JOIN Employees ON Company.CompanyID = Employees.CompanyID
				 LEFT JOIN Users u ON u.EmployeeID = Employees.EmployeeID
				 LEFT JOIN CompanyTypes ON Company.Type = CompanyTypes.ID
				 LEFT JOIN [Address] ON company.CompanyAddress = [Address].AID
				 LEFT JOIN RoadName ON [Address].RoadName = RoadName.RID
				 LEFT JOIN RoadType ON [Address].RoadType = RoadType.RTID
				 LEFT JOIN ZipCodes ON [Address].ZipCode = ZipCodes.ZCID
				 LEFT JOIN Parishes ON [Address].Parish = Parishes.PID
				 LEFT JOIN Cities ON [Address].City = Cities.CID
				 LEFT JOIN Countries ON [Address].Country = Countries.CID
	WHERE ((SELECT COUNT(*) FROM Users u LEFT JOIN UserGroupUserAssoc uga ON u.[UID] = uga.[UID]
										 LEFT JOIN UserGroups ug ON uga.UGID = ug.UGID
							WHERE u.[UID] = @uid AND ug.UserGroup IN ('System Administrator','IAJ Administrator')) > 0
		OR
		((Company.CompanyID IN ((SELECT Insurer FROM InsurerToIntermediary WHERE Intermediary = @cid) UNION (SELECT Intermediary FROM InsurerToIntermediary WHERE Insurer = @cid)) OR Company.CompanyID = @cid
			OR Company.CreatedBy IN (SELECT u.[UID] FROM Users u LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID WHERE e.CompanyID = @cid) )
			AND (SELECT COUNT(*) FROM Users u LEFT JOIN UserGroupUserAssoc uga ON u.[UID] = uga.[UID]
										 LEFT JOIN UserGroups ug ON uga.UGID = ug.UGID
										 LEFT JOIN PermissionGroupAssoc pga ON ug.UGID = pga.UGID
										 LEFT JOIN UserPermissions up ON pga.UPID = up.UPID
			WHERE u.[UID] = @uid AND (up.[Action] = 'Read' AND up.[Table] = 'Company')) > 0))
			AND Company.IAJID IS NOT NULL
			
	
	GROUP BY Company.CompanyID, Company.CompanyName, Company.IAJID, CompanyTypes.Type, Company.Enabled,
			 [Address].RoadNumber, RoadName.RoadName, RoadType.RoadType, ZipCodes.ZipCode, Cities.City, Countries.CountryName, Parishes.Parish

END

GO
/****** Object:  StoredProcedure [dbo].[NewPassword]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[NewPassword]
	
	@uid int,
	@pwd varchar(MAX),
	@editby int
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

    SELECT @origdata = 'Username: ' + username + ';Password: ' + [password]  + ';firstlogin: ' +  CONVERT(varchar(MAX),firstlogin) FROM Users WHERE [UID] = @uid;


	UPDATE Users
	SET LastModifiedBy = @editby,
		firstlogin = 1,
		FirstLoginDateTime = CURRENT_TIMESTAMP,
		[password] = @pwd,
		LoginGUID = NEWID()
	WHERE [UID] = @uid;


	SELECT @details = ('The record ' + CONVERT(varchar(MAX),@uid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid)),
		   @newdata =  'Username: ' + username +';Password: ' + [password] + ';firstlogin: ' +  CONVERT(varchar(MAX),firstlogin) FROM Users WHERE [UID] = @uid;

    EXEC LogAudit 'update', @uid , @details, 'Users', @uid , @origdata, @newdata, 'update';


END

GO
/****** Object:  StoredProcedure [dbo].[NotGroupPermissions]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[NotGroupPermissions]
	-- Add the parameters for the stored procedure here
	@UGID int,
	@system bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	up.UPID, up.[Table], up.[Action] 
	FROM	UserPermissions up	
	WHERE	up.UPID NOT IN (SELECT UPID FROM PermissionGroupAssoc WHERE UGID = @UGID) AND (up.[system] = 0 OR @system = 1);

END

GO
/****** Object:  StoredProcedure [dbo].[ParishList]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ParishList]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PID, Parish FROM Parishes ORDER BY Parish;
END


GO
/****** Object:  StoredProcedure [dbo].[PolicySearch]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PolicySearch]
	-- Add the parameters for the stored procedure here
	@term varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @search varchar(MAX); DECLARE @run bit; DECLARE @i int;
	SET @run = 1; SET @i = 0;
	SET @search = SUBSTRING(@term, 1, (CASE WHEN (CHARINDEX('&', @term) > 0) THEN (CHARINDEX('&', @term) - 1) ELSE LEN(@term) END));
	SET @term = SUBSTRING(@term, 2 + LEN(@search), LEN(@term)); 
	SET @search = '%' + @search + '%';

	CREATE TABLE #TempTable(ID int); CREATE TABLE #TempTable2 (ID int);

	WHILE @run = 1
	BEGIN
	
	SET @i = @i + 1;

    -- Insert statements for procedure here
		IF (@i = 1)
			INSERT INTO #TempTable (ID)
			SELECT p.PolicyID [ID]--, c.CompanyID [CompanyID], c.CompanyName [CompanyName], c.IAJID [IAJID], p.StartDateTime [start], p.EndDateTime [end], p.Policyno [pno]
			FROM Policies p LEFT JOIN Company c ON p.CompanyID = c.CompanyID
							LEFT JOIN PolicyCovers pc ON p.PolicyCover = pc.ID
							LEFT JOIN PolicyInsuredType pit ON p.InsuredType = pit.PITID
			WHERE c.CompanyName LIKE @search
			   OR CAST(c.IAJID as varchar(MAX)) LIKE @search
			   OR CAST(p.StartDateTime as varchar(MAX)) LIKE @search
			   OR CAST(p.EndDateTime as varchar(MAX)) LIKE @search
			   OR pc.prefix LIKE @search
			   OR p.Policyno LIKE @search
			   OR pc.Cover LIKE @search
			   OR pit.InsuredType LIKE @search;
		ELSE
		BEGIN
			INSERT INTO #TempTable2 (ID) SELECT ID FROM #TempTable;
			DELETE #TempTable;
			INSERT INTO #TempTable (ID)
			SELECT p.PolicyID [ID]--, c.CompanyID [CompanyID], c.CompanyName [CompanyName], c.IAJID [IAJID], p.StartDateTime [start], p.EndDateTime [end], p.Policyno [pno]
			FROM Policies p LEFT JOIN Company c ON p.CompanyID = c.CompanyID
							LEFT JOIN PolicyCovers pc ON p.PolicyCover = pc.ID
							LEFT JOIN PolicyInsuredType pit ON p.InsuredType = pit.PITID
			WHERE (c.CompanyName LIKE @search
			   OR CAST(c.IAJID as varchar(MAX)) LIKE @search
			   OR CAST(p.StartDateTime as varchar(MAX)) LIKE @search
			   OR CAST(p.EndDateTime as varchar(MAX)) LIKE @search
			   OR pc.prefix LIKE @search
			   OR p.Policyno LIKE @search
			   OR pc.Cover LIKE @search
			   OR pit.InsuredType LIKE @search)
			   AND p.PolicyID IN (SELECT ID FROM #TempTable2);
		END

		IF(LEN(@term) = 0) break;
		
		SET @search = SUBSTRING(@term, 1, (CASE WHEN (CHARINDEX('&', @term) > 0) THEN (CHARINDEX('&', @term) - 1) ELSE LEN(@term) END));
		SET @term = SUBSTRING(@term, 2 + LEN(@search), LEN(@term)); 
		SET @search = '%' + @search + '%';

	END

	SELECT p.PolicyID [ID], c.CompanyID [CompanyID], c.CompanyName [CompanyName], c.IAJID [IAJID], p.StartDateTime [start], p.EndDateTime [end], p.Policyno [pno]
	FROM Policies p LEFT JOIN Company c ON p.CompanyID = c.CompanyID
					LEFT JOIN PolicyCovers pc ON p.PolicyCover = pc.ID
					LEFT JOIN PolicyInsuredType pit ON p.InsuredType = pit.PITID
	WHERE p.PolicyID IN (SELECT ID FROM #TempTable);

	DROP TABLE #TempTable;DROP TABLE #TempTable2;
END


GO
/****** Object:  StoredProcedure [dbo].[PrintCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 26/08/2014
-- Description:	Updating the print information for a vehicle certificate
-- =============================================
CREATE PROCEDURE [dbo].[PrintCertificate]
	
	@VehicleCertId int, 
	@userId int,
	@lastPrintedByFlat Varchar (250)

AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @result int; DECLARE @CompID int;
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); 

	SELECT @CompID = CompanyID FROM Users LEFT JOIN Employees on Employees.EmployeeID = Users.EmployeeID WHERE [UID] = @userId;

	SELECT @details = ('The record ' + CONVERT(varchar(MAX),@VehicleCertId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userId)),
		   @origdata = 'VehicleCertificateID:' + CONVERT(varchar(MAX), @VehicleCertId) + ';LastPrintedBy:' + CONVERT(varchar(MAX),LastPrintedBy) + ';LastPrintedOn:' + CONVERT(varchar(MAX),LastPrintedOn) 
		               +';PrintCount:' + CONVERT(varchar(MAX), PrintCount) + ';PrintLimitOverride:'+ CONVERT(varchar(MAX), PrintLimitOverride) + ';RequestPrint:'+ CONVERT(varchar(MAX), RequestPrint)
	FROM VehicleCertificates WHERE VehicleCertificateID =@VehicleCertId;
	

	   --General Print update
		UPDATE VehicleCertificates SET LastPrintedON = CURRENT_TIMESTAMP ,LastPrintedBy = @userId , PrintCountInsurer =  PrintCountInsurer + 1, LastPrintedByFlat= @lastPrintedByFlat  WHERE VehicleCertificateID = @VehicleCertId AND PrintCountInsurer > 0;
			UPDATE VehicleCertificates SET  DateFirstPrinted = CURRENT_TIMESTAMP, LastPrintedON = CURRENT_TIMESTAMP ,LastPrintedBy = @userId , PrintCountInsurer = 1, LastPrintedByFlat= @lastPrintedByFlat, FirstPrintedByFlat = @lastPrintedByFlat   WHERE VehicleCertificateID = @VehicleCertId AND PrintCountInsurer = 0;

		--Only when an intermediary company prints the cert, is the print count, and over ride affected 
		IF EXISTS (SELECT CompanyID FROM Company WHERE CompanyID= @CompID AND ([Type] =2 OR [Type] = 3))
		   BEGIN
		        UPDATE VehicleCertificates SET PrintLimitOverride = 0 , PrintCount = PrintCount + 1, RequestPrint= 0 WHERE VehicleCertificateID = @VehicleCertId AND PrintCount > 0;
				UPDATE VehicleCertificates SET PrintLimitOverride = 0, PrintCount = 1, RequestPrint= 0 WHERE VehicleCertificateID = @VehicleCertId AND (PrintCount = 0 OR PrintCount IS NULL);
		   END
		

	--LOGS
	SELECT @newdata = 'VehicleCertificateID:' + CONVERT(varchar(MAX), @VehicleCertId) +';LastPrintedBy:' + CONVERT(varchar(MAX),LastPrintedBy) + ';LastPrintedOn:' + 
	                  CONVERT(varchar(MAX),LastPrintedOn) +';PrintCount:' + CONVERT(varchar(MAX), PrintCount) + ';PrintLimitOverride:'+ CONVERT(varchar(MAX), PrintLimitOverride)+
					  ';RequestPrint:'+ CONVERT(varchar(MAX), RequestPrint)
	FROM VehicleCertificates WHERE VehicleCertificateID =@VehicleCertId;

	EXEC LogAudit 'update', @userId , @details, 'VehicleCertificates',  @VehicleCertId, @origdata, @newdata, 'update';


END
GO
/****** Object:  StoredProcedure [dbo].[PrintCoverNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PrintCoverNote]
	-- Add the parameters for the stored procedure here
	@vcnid int,
	@uid int, 
	@lastPrintedByFlat Varchar (250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	--SELECT PrintCount > = CoverNotePrintLimit 
	--FROM Company LEFT JOIN Employees ON Company.CompanyID = Employees.CompanyID
	--			 LEFT JOIN Users ON Employees.EmployeeID = Users.EmployeeID
	--WHERE Users.UID = @uid;
	
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @result bit;
	DECLARE @personid int;
	DECLARE @compID int;
	SELECT @personid = p.PeopleID FROM People p RIGHT JOIN Employees e ON p.PeopleID = e.PersonID RIGHT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE u.[UID] = @uid;

	SELECT @CompID = CompanyID FROM Users LEFT JOIN Employees on Employees.EmployeeID = Users.EmployeeID WHERE [UID] = @uid;

	SELECT @details = ('The record ' + CONVERT(varchar(MAX),@vcnid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid)),
		   @origdata = 'VehicleCoverNoteID:' + CONVERT(varchar(MAX), @vcnid) + ';LastPrintedBy:' + CONVERT(varchar(MAX),LastPrintedBy) 
		               + ';LastPrinted:' + CONVERT(varchar(MAX),LastPrinted) + ';PrintCount:' + CONVERT(varchar(MAX), PrintCount) + ';PrintLimitOverride:'+ CONVERT(varchar(MAX), PrintLimitOverride)
	FROM VehicleCoverNote WHERE VehicleCoverNoteID = @vcnid;
	

	UPDATE VehicleCoverNote SET LastPrintedBy = @personid, LastPrinted = CURRENT_TIMESTAMP, Printed = 1,  PrintCountInsurer = PrintCountInsurer + 1, LastPrintedByFlat= @lastPrintedByFlat WHERE VehicleCoverNoteID = @vcnid AND PrintCountInsurer > 0;
	IF(@@ROWCOUNT > 0) SET @result = 1;

	UPDATE VehicleCoverNote SET DateFirstPrinted = CURRENT_TIMESTAMP, LastPrintedBy = @personid, LastPrinted = CURRENT_TIMESTAMP, Printed = 1,  PrintCountInsurer =  1, LastPrintedByFlat= @lastPrintedByFlat, FirstPrintedByFlat = @lastPrintedByFlat WHERE VehicleCoverNoteID = @vcnid AND (PrintCountInsurer = 0 OR PrintCountInsurer IS NULL);
    IF(@result IS NOT NULL AND @@ROWCOUNT > 0) SET @result = 1;
    

	--Only when an intermediary company prints the cover note, is the print count, and over ride affected 
		IF EXISTS (SELECT CompanyID FROM Company WHERE CompanyID= @CompID AND ([Type] =2 OR [Type] = 3))
		   BEGIN
			 UPDATE VehicleCoverNote SET PrintCount = PrintCount + 1, PrintLimitOverride=0, RequestPrint= 0, LastModifiedOn  = CURRENT_TIMESTAMP WHERE VehicleCoverNoteID = @vcnid AND PrintCount > 0;
			    IF(@@ROWCOUNT > 0) SET @result = 1;
			 UPDATE VehicleCoverNote SET PrintCount = 1, PrintLimitOverride=0, RequestPrint= 0, LastModifiedOn  = CURRENT_TIMESTAMP WHERE VehicleCoverNoteID = @vcnid AND PrintCount = 0;
	 		    IF(@result IS NOT NULL AND @@ROWCOUNT > 0) SET @result = 1;
	       END


	--LOGS
	SELECT @newdata = 'VehicleCoverNoteID:' + CONVERT(varchar(MAX), @vcnid) + ';LastPrintedBy:' + CONVERT(varchar(MAX),LastPrintedBy) + ';LastPrinted:' + CONVERT(varchar(MAX),LastPrinted) + 
	                  ';PrintCount:' + CONVERT(varchar(MAX), PrintCount) + ';PrintLimitOverride:'+ CONVERT(varchar(MAX), PrintLimitOverride)
	FROM VehicleCoverNote WHERE VehicleCoverNoteID = @vcnid;

	EXEC LogAudit 'update', @uid , @details, 'VehicleCoverNote',  @vcnid, @origdata, @newdata, 'update';

	IF(@result IS NULL) SET @result = 0;
	SELECT @result result;
	
	

END

GO
/****** Object:  StoredProcedure [dbo].[PrintRequest]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: <Create Date,,>
-- Description:	Marking a certificate as requested to be printed
-- =============================================
CREATE PROCEDURE [dbo].[PrintRequest]
	@id int,
	@type varchar (250),
	@uid int
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); 

	IF (@type = 'Certificate')
		BEGIN
			IF EXISTS (SELECT VehicleCertificateID FROM VehicleCertificates WHERE VehicleCertificateID = @id)
			 BEGIN

			   SELECT @origdata = 'Certificate ID: ' + CONVERT(varchar(MAX),@id) + ';RequestPrint:' + CONVERT(varchar(MAX),RequestPrint) 
		       FROM VehicleCertificates WHERE VehicleCertificateID =@id;

				UPDATE VehicleCertificates SET RequestPrint=1 WHERE VehicleCertificateID = @id;


				SELECT @details = ('The record ' + CONVERT(varchar(MAX),@id) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid)),
			           @newdata =  'Certificate ID: ' + CONVERT(varchar(MAX),@id) + ';RequestPrint: ' + CONVERT(varchar(MAX),RequestPrint) 
	            FROM VehicleCertificates WHERE VehicleCertificateID =@id;

	            EXEC LogAudit 'update', @uid , @details, 'VehicleCertificates',  @id, @origdata, @newdata, 'update';


				SELECT 1 AS result;
			 END
		END

	ELSE
	   BEGIN
			IF EXISTS (SELECT VehicleCoverNoteID FROM VehicleCoverNote WHERE VehicleCoverNoteID = @id)
			 BEGIN

			    SELECT @origdata = 'Covernote ID: ' + CONVERT(varchar(MAX),@id) + ';RequestPrint:' + CONVERT(varchar(MAX),RequestPrint) 
		        FROM VehicleCertificates WHERE VehicleCertificateID =@id;

				UPDATE VehicleCoverNote SET RequestPrint=1 WHERE VehicleCoverNoteID = @id;


				SELECT @details = ('The record ' + CONVERT(varchar(MAX),@id) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid)),
			           @newdata =  'Covernote ID: ' + CONVERT(varchar(MAX),@id) + ';RequestPrint: ' + CONVERT(varchar(MAX),RequestPrint) 
	            FROM VehicleCoverNote WHERE VehicleCoverNoteID = @id;

	            EXEC LogAudit 'update', @uid , @details, 'VehicleCoverNote',  @id, @origdata, @newdata, 'update';


				SELECT 1 AS result;
			 END
		END
	
END

GO
/****** Object:  StoredProcedure [dbo].[PullVehiclePolData]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 13/08/2014
-- Description:	Pulling data about a vehicle, and its coverage
-- =============================================
CREATE PROCEDURE [dbo].[PullVehiclePolData]
	
	@VehicleID int, 
	@userId int 

AS
BEGIN
	
	SET NOCOUNT ON;

    
	SELECT  Vehicles.VIN, Vehicles.Make, Vehicles.Model, People.PeopleID, People.TRN, People.FName, People.LName, Company.CompanyID, Policies.PolicyID, PolicyCovers.Cover, PolicyCovers.ID [ID]
	       FROM Vehicles 
		      LEFT JOIN Drivers ON Drivers.DriverID = Vehicles.MainDriver 
	          LEFT JOIN People ON People.PeopleID = Drivers.PersonID
			  RIGHT OUTER JOIN VehiclesUnderPolicy ON VehiclesUnderPolicy.VehicleID = @VehicleID AND VehiclesUnderPolicy.Cancelled != 1
			  RIGHT JOIN Policies ON Policies.PolicyID = VehiclesUnderPolicy.PolicyID  AND Policies.Deleted = 0 
						AND (CURRENT_TIMESTAMP BETWEEN Policies.StartDateTime AND Policies.EndDateTime)
			  LEFT JOIN Company ON Policies.CompanyID = Company.CompanyID
			  LEFT JOIN PolicyCovers ON Policies.PolicyCover =  PolicyCovers.ID
		  WHERE Vehicles.VehicleID = @VehicleID;


END

GO
/****** Object:  StoredProcedure [dbo].[PurgeUserGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PurgeUserGroup]
	-- Add the parameters for the stored procedure here
	@GroupID int,
	@deletedby int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	IF ((SELECT COUNT(*) FROM Users u LEFT JOIN UserGroupUserAssoc uga ON u.[UID] = uga.[UID] LEFT JOIN UserGroups ug ON uga.UGID = ug.UGID WHERE u.[UID] = @deletedby AND ug.UserGroup = 'System Administrator') > 0)
	BEGIN
		INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
		SELECT 'delete',
			   @deletedby,
			   CURRENT_TIMESTAMP,
			   'The record ' + CONVERT(varchar(MAX), UGUAID) + ' has been removed from the system by ' + CONVERT(varchar(MAX), @deletedby),
			   'UserGroupUserAssoc',
			   UGUAID,
			   'UGID:' + CONVERT(varchar(MAX), UGID) + ';UID:' + CONVERT(varchar(MAX), [UID]),
			   null,
			   'delete'
		FROM UserGroupUserAssoc WHERE UGID = @GroupID;

		DELETE UserGroupUserAssoc WHERE UGID = @GroupID;
	END
	ELSE
	BEGIN
		--test if user is company admin... should i have this security this deep???
		IF ((SELECT COUNT(*) FROM Users u LEFT JOIN UserGroupUserAssoc uga ON u.[UID] = uga.[UID] LEFT JOIN UserGroups ug ON uga.UGID = ug.UGID WHERE u.[UID] = @deletedby AND ug.UserGroup = 'Company Administrator') > 0)
		BEGIN
			
			DECLARE @companyid int;
			SELECT @companyid = e.CompanyID FROM Users u LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID WHERE u.[UID] = @deletedby;
			
			
			INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
			SELECT 'delete',
				   @deletedby,
				   CURRENT_TIMESTAMP,
				   'The record ' + CONVERT(varchar(MAX), UGUAID) + ' has been removed from the system by ' + CONVERT(varchar(MAX), @deletedby),
				   'UserGroupUserAssoc',
				   UGUAID,
				   'UGID:' + CONVERT(varchar(MAX), UGID) + ';UID:' + CONVERT(varchar(MAX), UserGroupUserAssoc.[UID]),
				   null,
				   'delete'
			FROM UserGroupUserAssoc LEFT JOIN Users ON UserGroupUserAssoc.[UID] = Users.[UID] LEFT JOIN Employees ON Users.EmployeeID = Employees.EmployeeID
			WHERE UserGroupUserAssoc.UGID = @GroupID AND Employees.CompanyID = @companyid;

			DELETE UserGroupUserAssoc WHERE UGID = @GroupID;
		END
	END
END


GO
/****** Object:  StoredProcedure [dbo].[RefreshGroupCopy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RefreshGroupCopy]
	@ugid int,
	@uid int
AS
BEGIN
	
	SET NOCOUNT ON;

    DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; 

	SELECT @origdata= 'UGID: ' + CONVERT(varchar(MAX),@ugid) + 'User Group: ' + UserGroup + 'System: ' + CONVERT(varchar(MAX),[system])
	FROM UserGroups WHERE UGID = @ugid;	

	UPDATE UserGroups
	SET SuperGroup = ug.SuperGroup,
		UserGroup = ug.UserGroup,
		LastModifiedBy = @uid,
		[system] = 0
	FROM UserGroups ug WHERE ug.UGID = CopyFrom AND UGID = @ugid;	
	 
    INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'update', @uid, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),UGID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
		   'UserGroups', UGID, @origdata, 'UGID: ' + CONVERT(varchar(MAX),UGID) + + ';User Group: ' + UserGroup + ';System: ' + CONVERT(varchar(MAX),[system]), 'update'
	FROM UserGroups WHERE UGID = @ugid;	



	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'delete', @uid, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),PGAID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
		   'PermissionGroupAssoc', PGAID, 'UPID: ' +  CONVERT(varchar(MAX),UPID) + 'UGID' + CONVERT(varchar(MAX),UGID), null, 'delete'
	FROM PermissionGroupAssoc WHERE UGID = @ugid;

	DELETE PermissionGroupAssoc WHERE UGID = @ugid;
	


	INSERT INTO PermissionGroupAssoc (UPID, UGID)
	SELECT UPID, @ugid FROM PermissionGroupAssoc WHERE UGID = (SELECT CopyFROM FROM UserGroups WHERE UGID = @ugid);

	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'insert', @uid, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),PGAID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
		   'PermissionGroupAssoc',PGAID, null, 'UPID: ' +  CONVERT(varchar(MAX),UPID) + 'UGID' + CONVERT(varchar(MAX),@ugid), 'insert'
	FROM PermissionGroupAssoc WHERE UGID = @ugid;


END

GO
/****** Object:  StoredProcedure [dbo].[RemoveChildFromSuperGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RemoveChildFromSuperGroup]
	-- Add the parameters for the stored procedure here
	@PGID int,
	@CGID int,
	@deletedby int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	SELECT 'delete', @deletedby, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),SGID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@deletedby),
			'SuperGroupMapping', SGID, 'PGID:' + CONVERT(varchar(MAX),@PGID) + ';CGID:' + CONVERT(varchar(MAX),@CGID), null, 'delete'
	FROM SuperGroupMapping WHERE PGID = @PGID AND CGID = @CGID;


	DELETE SuperGroupMapping WHERE PGID = @PGID AND CGID = @CGID;
END


GO
/****** Object:  StoredProcedure [dbo].[RemoveIntermediary]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 16/09/2014
-- Description:Removing an intermediary company from an insurance company
-- =============================================

CREATE PROCEDURE [dbo].[RemoveIntermediary]

	@intermediaryCompId int,
	@CompanyID int,
	@userID int

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @CTID int;

    IF EXISTS (SELECT CTID FROM InsurerToIntermediary WHERE Insurer = @CompanyID AND Intermediary = @intermediaryCompId)
	   BEGIN
	    
		  SELECT @CTID = CTID FROM InsurerToIntermediary WHERE Insurer = @CompanyID AND Intermediary = @intermediaryCompId;

	          SELECT @origdata = 'InsuranceCompId:' + CONVERT(varchar(MAX), @CompanyID) + 'IntermediaryCompId:' + CONVERT(varchar(MAX), @intermediaryCompId);

		          DELETE FROM InsurerToIntermediary WHERE Insurer = @CompanyID AND Intermediary = @intermediaryCompId;

		    SELECT @details = ('The record ' + CONVERT(varchar(MAX),@CTID) + ' was deleted from the database by ' + CONVERT(varchar(MAX),@userID)),
		           @newdata = null;
	    EXEC LogAudit 'delete', @userID , @details, 'InsurerToIntermediary',  @CTID, @origdata, @newdata, 'delete';
     END

END
GO
/****** Object:  StoredProcedure [dbo].[RemovePreviousIntermediaries]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RemovePreviousIntermediaries]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE InsurerToIntermediary WHERE [keep] = 0 AND Insurer = @id;
	UPDATE InsurerToIntermediary SET [keep] = 0 WHERE Insurer = @id;
END

GO
/****** Object:  StoredProcedure [dbo].[RemoveSuperGroupChildren]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RemoveSuperGroupChildren]
	-- Add the parameters for the stored procedure here
	@UGID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE SuperGroupMapping WHERE PGID = @UGID;
END


GO
/****** Object:  StoredProcedure [dbo].[RemoveUserFromContactGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RemoveUserFromContactGroup]
	-- Add the parameters for the stored procedure here
	@cgid int,
	@uid int,
	@deletedby int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @recordid int; -- LOG AUDIT VARIABLES
	SELECT  @recordid = ContactUserAssoc.ID,
			@details = 'The record ' + CONVERT(varchar(MAX),ContactUserAssoc.ID) + ' was deleted from the database by ' + CONVERT(varchar(MAX),@deletedby),
		    @newdata = null,
			@origdata = 'ContactGroup:' + ContactGroups.name + ';User:' + Users.username
	FROM ContactUserAssoc JOIN ContactGroups ON ContactGroups.ID = ContactUserAssoc.contactgroup
							JOIN Users ON ContactUserAssoc.UID = Users.UID
	WHERE ContactUserAssoc.contactgroup = @cgid AND ContactUserAssoc.[UID] = @uid;
	EXEC LogAudit 'delete', @deletedby , @details, 'ContactUserAssoc', @recordid, @origdata, @newdata, 'delete';

	DELETE ContactUserAssoc WHERE contactgroup = @cgid AND [UID] = @uid;
END


GO
/****** Object:  StoredProcedure [dbo].[RemoveUserPermissionFromGroup]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RemoveUserPermissionFromGroup] 
	-- Add the parameters for the stored procedure here
	@UGID int,
	@UPID int,
	@deletedby int
AS
BEGIN
	
	SET NOCOUNT ON;

	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionDetails, RecordID, Location, OrigData, NewData, AuditType)
	SELECT 'delete', @deletedby, 'The record ' + CONVERT(varchar(MAX), PGAID) + ' was deleted from the system by ' + CONVERT(varchar(MAX), @deletedby),
			PGAID, 'PermissionGroupAssoc', 'UPID:' + CONVERT(varchar(MAX),@UPID) + ';UGID:' + CONVERT(varchar(MAX),@UGID), null, 'delete'
	FROM PermissionGroupAssoc WHERE UPID = @UPID AND UGID = @UGID;
	
	DELETE PermissionGroupAssoc WHERE UPID = @UPID AND UGID = @UGID;
END

GO
/****** Object:  StoredProcedure [dbo].[ResentPasswordToUser]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ResentPasswordToUser]
	-- Add the parameters for the stored procedure here
	@uid int,
	@pword varchar(MAX),
	@changedby int
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

    SELECT @origdata = 'Username: ' + username + ';password: ' +  CONVERT(varchar(MAX),[password]) +  
	                   ';passwordresetrequesttime' + CONVERT(varchar(MAX),(CASE WHEN (passwordresetrequesttime = '' OR passwordresetrequesttime IS NULL) THEN '' ELSE passwordresetrequesttime END))+ 
					   ';password_reset_force' + CONVERT(varchar(MAX),password_reset_force) 
	                    FROM Users WHERE [UID] = @uid;

	UPDATE Users 
	SET [password] = @pword,
	    LastModifiedBy = @changedby,
		password_reset_force = 1,
		passwordresetrequesttime = CURRENT_TIMESTAMP,
		faultylogins = 0
		--firstlogin = 1,
		--FirstLoginDateTime = CURRENT_TIMESTAMP
	WHERE [uid] = @uid;
     

	SELECT @details = ('The record ' + CONVERT(varchar(MAX),@uid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@changedby)),
		   @newdata =  'Username: ' + username + ';password: ' +  CONVERT(varchar(MAX),[password]) +  
	                   ';passwordresetrequesttime' + CONVERT(varchar(MAX),(CASE WHEN (passwordresetrequesttime = '' OR passwordresetrequesttime IS NULL) THEN '' ELSE passwordresetrequesttime END))+ 
					   ';password_reset_force' + CONVERT(varchar(MAX),password_reset_force) 
	                    FROM Users WHERE [UID] = @uid;

    EXEC LogAudit 'update', @changedby , @details, 'Users', @uid , @origdata, @newdata, 'update';
    


END

GO
/****** Object:  StoredProcedure [dbo].[ResetFaultyLogins]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ResetFaultyLogins]
	-- Add the parameters for the stored procedure here
	@username varchar(MAX)
AS
BEGIN

	SET NOCOUNT ON;
    
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @userId int;

	SELECT @userId = [UID] FROM Users WHERE username = @username;

    SELECT @origdata = 'UserName: ' + @username + 'Faulty Login: ' + CONVERT(varchar(MAX), faultylogins) FROM Users WHERE username = @username;

	UPDATE Users SET faultylogins = 0, LoggedIn = 1, LastSuccessful = CURRENT_TIMESTAMP, LoginGUID = NEWID() WHERE username = @username;

		
	SELECT @details = ('The record ' + CONVERT(varchar(MAX),@userId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userId)),
		   @newdata = 'UserName: ' + @username + 'Faulty Login: ' + CONVERT(varchar(MAX), faultylogins)  FROM Users WHERE username = @username;

		  EXEC LogAudit 'update', @userId , @details, 'Users', @userId , @origdata, @newdata, 'update';

END

GO
/****** Object:  StoredProcedure [dbo].[ResetFirstLoginTimer]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ResetFirstLoginTimer]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

    SELECT @origdata = 'Username: ' + username + ';Firstlogin DateTime: ' +  CONVERT(varchar(MAX),(CASE WHEN (FirstLoginDateTime = '' OR FirstLoginDateTime IS NULL) THEN '' ELSE FirstLoginDateTime END)) 
	                    FROM Users WHERE [UID] = @uid;
  

	UPDATE Users SET FirstLoginDateTime = CURRENT_TIMESTAMP WHERE [UID] = @uid;


	SELECT @details = ('The record ' + CONVERT(varchar(MAX),@uid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid)),
		   @newdata =  'Username: ' + username + ';Firstlogin DateTime: ' +  CONVERT(varchar(MAX),(CASE WHEN (FirstLoginDateTime = '' OR FirstLoginDateTime IS NULL) THEN '' ELSE FirstLoginDateTime END)) 
		                FROM Users WHERE [UID] = @uid;

    EXEC LogAudit 'update', @uid , @details, 'Users', @uid , @origdata, @newdata, 'update';

END

GO
/****** Object:  StoredProcedure [dbo].[ResetPassword]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ResetPassword]
	-- Add the parameters for the stored procedure here
	@username varchar(250),
	@editedBy int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--DECLARE @username varchar(250); SET @username = 'a@a.com';
	--DECLARE @editedby int; SET @editedBy = null;
	DECLARE @result bit;
	IF EXISTS(SELECT UID FROM Users WHERE username = @username)
	BEGIN
		DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES

		SELECT @origdata = 'username:' +  @username + ';reset:' + CONVERT(varchar(MAX), [reset]) + ';reset-guid:' + CONVERT(varchar(MAX),resetguid) + ';resetrequesttime:' + CONVERT(varchar(MAX), passwordresetrequesttime)
		FROM Users 
		WHERE username = @username;
		

		IF (@editedby IS NULL) UPDATE Users SET resetguid = NEWID(), reset = 1, passwordresetrequesttime = CURRENT_TIMESTAMP WHERE username = @username;
		ELSE UPDATE Users SET resetguid = NEWID(), reset = 1, passwordresetrequesttime = CURRENT_TIMESTAMP, LastModifiedby = @editedBy WHERE username = @username;
		

		SELECT @id = [UID], 
			   @details = 'The record ' + CONVERT(varchar(MAX),[UID]) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy),
		       @newdata = 'username:' +  @username + ';reset:' + CONVERT(varchar(MAX), [reset]) + ';reset-guid:' + CONVERT(varchar(MAX),resetguid) + ';resetrequesttime:' + CONVERT(varchar(MAX), passwordresetrequesttime)
		FROM Users
		WHERE username = @username;
		
		EXEC LogAudit 'update', @editedBy , @details, 'Users', @id, @origdata, @newdata, 'update';
		
		SET @result = 1;
		SELECT @result as result, resetguid, Users.UID, fname, lname, Emails.email 
															 FROM Users LEFT JOIN Employees ON Users.EmployeeID = Employees.EmployeeID
																		LEFT JOIN People ON Employees.PersonID  = People.PeopleID
																		LEFT JOIN PeopleToEmails ON People.PeopleID = PeopleToEmails.PeopleID
																		LEFT JOIN Emails ON PeopleToEmails.EmailID = Emails.EID
															 WHERE Users.username = @username AND PeopleToEmails.[Primary] = 1;

	END
	ELSE
	BEGIN 
		SET @result = 0;
		SELECT @result as result;
	END 
END

GO
/****** Object:  StoredProcedure [dbo].[ResetQuestions]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ResetQuestions]
	-- Add the parameters for the stored procedure here
	@uid int,
	@pword varchar(MAX),
	@changedby int
AS
BEGIN

	SET NOCOUNT ON;

   DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

    SELECT @origdata = 'Username: ' + username + ';First Login: ' +CONVERT(varchar(MAX),firstlogin) + ';faultylogins: ' +CONVERT(varchar(MAX),faultylogins)+
	                   ';Firstlogin DateTime: ' +  CONVERT(varchar(MAX),(CASE WHEN (FirstLoginDateTime = '' OR FirstLoginDateTime IS NULL) THEN '' ELSE FirstLoginDateTime END)) 
	                   FROM Users WHERE [UID] = @uid;

	
	UPDATE Users 
	SET [password] = @pword,
	    LastModifiedBy = @changedby,
		FirstLoginDateTime = CURRENT_TIMESTAMP,
		firstlogin = 1,
		faultylogins = 0,
		question = null,
		response = null
	WHERE [uid] = @uid;


	SELECT @details = ('The record ' + CONVERT(varchar(MAX),@uid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid)),
		   @newdata =  'Username: ' + username + ';First Login: ' +CONVERT(varchar(MAX),firstlogin) + ';faultylogins: ' +CONVERT(varchar(MAX),faultylogins)+
	                   ';Firstlogin DateTime: ' +  CONVERT(varchar(MAX),(CASE WHEN (FirstLoginDateTime = '' OR FirstLoginDateTime IS NULL) THEN '' ELSE FirstLoginDateTime END)) 
	                   FROM Users WHERE [UID] = @uid;

    EXEC LogAudit 'update', @uid , @details, 'Users', @uid , @origdata, @newdata, 'update';


END

GO
/****** Object:  StoredProcedure [dbo].[ReturnResponse]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ReturnResponse]
	-- Add the parameters for the stored procedure here
	@reqkey uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT inprogress, completed, failure [fail], processedjson, [count]
	FROM JsonAudit
	WHERE [key] = @reqkey;

END


GO
/****** Object:  StoredProcedure [dbo].[RmAuthorizedDriver]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RmAuthorizedDriver]
	-- Add the parameters for the stored procedure here
	@polID int,
	@drivID int,
	@vehID int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	DELETE AuthorizedDrivers WHERE VehicleID = @vehID AND PolicyID = @polID AND DriverID = @drivID;
	
END


GO
/****** Object:  StoredProcedure [dbo].[RmExcepetedDriver]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RmExcepetedDriver]
	-- Add the parameters for the stored procedure here
	@polID int,
	@drivID int,
	@vehID int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	DELETE ExceptedDrivers WHERE VehicleID = @vehID AND PolicyID = @polID AND DriverID = @drivID;
	
END


GO
/****** Object:  StoredProcedure [dbo].[RmExcludedDriver]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RmExcludedDriver]
	-- Add the parameters for the stored procedure here
	@polID int,
	@drivID int,
	@vehID int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	DELETE ExcludedDrivers WHERE VehicleID = @vehID AND PolicyID = @polID AND DriverID = @drivID;
	
END


GO
/****** Object:  StoredProcedure [dbo].[RmUsageFromCover]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RmUsageFromCover]
	-- Add the parameters for the stored procedure here
	@usageid int,
	@coverid int,
	@userid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

     DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	IF (SELECT COUNT(*) FROM VehiclesUnderPolicy vup LEFT JOIN Policies p ON vup.PolicyID = p.PolicyID WHERE vup.VehicleUsage = @usageid AND p.PolicyCover = @coverid) > 0
		SELECT CAST(0 as bit) [result];
	ELSE
	BEGIN

	    SELECT @details = 'The record ' + CONVERT(varchar(MAX),ID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userid),
		       @origdata =  'UsageID: ' +  CONVERT(varchar(MAX),UsageID)+ 'coverID: ' +  CONVERT(varchar(MAX),CoverID),
			   @newdata = null
			   FROM UsagesToCovers WHERE UsageID = @usageid;
		
		EXEC LogAudit 'delete', @userid , @details, 'UsagesToCovers', @@IDENTITY, @origdata, @newdata, 'delete';

		DELETE UsagesToCovers WHERE UsageID = @usageid;
		SELECT CAST(1 as bit) [result];
	END
END

GO
/****** Object:  StoredProcedure [dbo].[SearchUsers]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SearchUsers]
	-- Add the parameters for the stored procedure here
	@search varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [UID],username,reset,faultylogins,lastloginattempt,password_reset_force, passwordresetrequesttime, active FROM Users 
		WHERE username LIKE '%'+@search+'%' AND Filter = 0;
END


GO
/****** Object:  StoredProcedure [dbo].[SendDriverDetails]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SendDriverDetails]
	-- Add the parameters for the stored procedure here
	@id int,
	@license varchar(MAX) = null,
	@issued datetime = null,
	@expiry datetime = null,
	@class varchar(MAX) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS(SELECT * FROM Drivers WHERE DriverID = @id)
	BEGIN
		UPDATE Drivers
		SET [Class] = @class,
			DateLicenseExpires = @expiry,
			DateLicenseIssued = @issued,
			LicenseNo = @license
		WHERE DriverID = @id;	
	END
	

END


GO
/****** Object:  StoredProcedure [dbo].[SendNotificationTo]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SendNotificationTo]
	-- Add the parameters for the stored procedure here
	@nid int,
	@uid int,
	@createdby int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (@createdby IS NULL OR @createdby = 0) SELECT @createdby = [UID] FROM Users WHERE username = 'system';

    -- Insert statements for procedure here
	INSERT INTO NotificationsTo ([Notification], [UID]) VALUES (@nid, @uid);
	
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES
	SELECT @origdata = null,
		   @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby),
		   @newdata = 'Notification:' +  CONVERT(varchar(MAX),@nid) +  ';NoteTo:' + CONVERT(varchar(MAX),@uid);
	EXEC LogAudit 'insert', @createdby , @details, 'NotificationsTo', @@IDENTITY, @origdata, @newdata, 'insert';



END


GO
/****** Object:  StoredProcedure [dbo].[SendResetPasswordData]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SendResetPasswordData]
	-- Add the parameters for the stored procedure here
	@username varchar(250),
	@password varchar(250),
	@editedBy int = null
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int;  DECLARE @EditUserID int;-- LOG AUDIT VARIABLES

	IF(@editedBy IS NULL OR @editedBy = 0) 
	   SELECT @EditUserID = [UID] FROM Users WHERE username = @username;
	ELSE   
	   SELECT @EditUserID = @editedBy;


	SELECT @origdata = 'username:' +  @username + ';password:' + [password]
	FROM Users 
	WHERE username = @username;

	
   UPDATE Users SET password = @password, LastModifiedBy= @EditUserID, [reset]= 0, LoggedIn= (CASE WHEN (@editedBy IS NULL OR @editedBy = 0) THEN 0 ELSE LoggedIn END),faultylogins=0, password_reset_force = 0,  LoginGUID = (CASE WHEN (@editedBy IS NULL OR @editedBy = 0) THEN NULL ELSE LoginGUID END) WHERE username = @username;


	SELECT @id = [UID], 
			   @details = 'The record ' + CONVERT(varchar(MAX),[UID]) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy),
		       @newdata = 'username:' +  @username + ';password:' + [password]
	FROM Users
	WHERE username = @username;
		
	EXEC LogAudit 'update', @EditUserID , @details, 'Users', @id, @origdata, @newdata, 'update';


	SELECT Emails.email, Users.username, Users.UID
			     FROM Emails JOIN PeopleToEmails ON Emails.EID = PeopleToEmails.EmailID
							 JOIN People ON PeopleToEmails.PeopleID = People.PeopleID
							 JOIN Employees ON Employees.PersonID = People.PeopleID
							 JOIN Users ON Employees.EmployeeID = Users.EmployeeID
							 JOIN Company ON Company.CompanyID = Employees.CompanyID
							 JOIN UserGroupUserAssoc ON Users.UID = UserGroupUserAssoc.UID
							 JOIN UserGroups ON UserGroups.UGID = UserGroupUserAssoc.UGID
				 WHERE Company.CompanyID = 
						(SELECT Company.CompanyID FROM Company JOIN Employees ON Company.CompanyID = Employees.CompanyID
															   JOIN Users ON Users.EmployeeID = Employees.EmployeeID
											      WHERE Users.username = @username)
					   AND UserGroups.UserGroup = 'Company Administrator';
END

GO
/****** Object:  StoredProcedure [dbo].[SetUserGroups]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SetUserGroups]
	-- Add the parameters for the stored procedure here
	@UID int,
	@GID int,
	@case varchar(50),
	@createdBy int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (@case = 'add')
	BEGIN
		IF NOT EXISTS (SELECT UGUAID FROM UserGroupUserAssoc WHERE [UID] = @UID AND UGID = @GID)
			INSERT INTO UserGroupUserAssoc ([UID],UGID, CreatedBy, CreatedOn, LastModifiedBy) VALUES (@UID, @GID, @createdBy, CURRENT_TIMESTAMP, @createdBy);
	END
	ELSE 
	BEGIN 
	    UPDATE UserGroupUserAssoc SET LastModifiedBy = @createdBy WHERE [UID] = @UID AND UGID = @GID;
		DELETE UserGroupUserAssoc WHERE [UID] = @UID AND UGID = @GID;
	END
END


GO
/****** Object:  StoredProcedure [dbo].[StillLoggedIn]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[StillLoggedIn]
	-- Add the parameters for the stored procedure here
	@guid uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS(SELECT UID FROM Users WHERE LoginGUID = @guid AND DATEDIFF(MINUTE, LastSuccessful, CURRENT_TIMESTAMP) <= 5 AND LoggedIn = 1)
	   BEGIN
		  UPDATE Users SET LastSuccessful = CURRENT_TIMESTAMP WHERE LoginGUID = @guid;
		  SELECT CAST(1 as bit) result;
	   END

	ELSE 
	   BEGIN 
	     INSERT INTO LoggedOutLog (GUID, MarkedTime) VALUES (@guid, CURRENT_TIMESTAMP);
	     SELECT CAST (0 as bit) result;
	   END
END


GO
/****** Object:  StoredProcedure [dbo].[StoreCount]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[StoreCount] 
	-- Add the parameters for the stored procedure here
	@reqkey uniqueidentifier,
	@count int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE JsonAudit
	SET [count] = @count
	WHERE [key] = @reqkey;

END


GO
/****** Object:  StoredProcedure [dbo].[StoreError]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[StoreError]
	-- Add the parameters for the stored procedure here
	@type varchar(MAX),
	@ediid varchar(MAX),
	@reqkey uniqueidentifier,
	@id varchar(MAX) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO error (error_type, edi_system_id, reqid, subid)
	VALUES (@type, @ediid, (SELECT ID FROM JsonAudit WHERE [key] = @reqkey), (CASE WHEN(@id = '' OR @id IS NULL) THEN NULL ELSE (SELECT ID FROM error WHERE edi_system_id = @id AND reqid = (SELECT ID FROM JsonAudit WHERE [key] = @reqkey)) END));
	
END

GO
/****** Object:  StoredProcedure [dbo].[StoreJson]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[StoreJson]
	-- Add the parameters for the stored procedure here
	@json nvarchar(MAX),
	@method varchar(50),
	@uid int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (@uid IS NULL OR @uid = 0)
	BEGIN
		SELECT @uid = [UID] FROM Users WHERE username = 'system';
	END

	INSERT INTO JsonAudit (CreatedBy, method, json, [key]) VALUES (@uid, @method, CAST(@json as varchar(MAX)), NEWID());
	SELECT [ID], [key] FROM JsonAudit WHERE [ID] = @@IDENTITY;

END

GO
/****** Object:  StoredProcedure [dbo].[StoreRequestKey]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[StoreRequestKey]
	-- Add the parameters for the stored procedure here
	@id int = 0,
	@reqkey varchar(MAX),
	@uid int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @work bit;

	IF (@id = 0)
	BEGIN
		SET @work = 0;
	END
	ELSE
	BEGIN
		UPDATE RSAKeys SET [request] = @reqkey WHERE [id] = @id AND [uid] = @uid;
		IF EXISTS(SELECT * FROM RSAKeys WHERE [id] = @id AND [uid] = @uid)
		BEGIN
			SET @work = 1;
		END
		ELSE
		BEGIN
			SET @work = 0;
		END
	END
	
	SELECT @id [id], @work [work];

END

GO
/****** Object:  StoredProcedure [dbo].[StoreResponse]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[StoreResponse]
	-- Add the parameters for the stored procedure here
	@reqkey uniqueidentifier,
	@resp nvarchar(MAX),
	@count int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE JsonAudit 
	SET processedjson = @resp, completed = 1, inprogress = 0, failure = 0, CompletedTimeStamp = CURRENT_TIMESTAMP, [count] = @count
	WHERE [key] = @reqkey;

END


GO
/****** Object:  StoredProcedure [dbo].[StoreSessionKey]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[StoreSessionKey]
	-- Add the parameters for the stored procedure here
	@id int = 0,
	@sesskey varchar(MAX),
	@uid int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @work bit;

	IF (@id = 0)
	BEGIN
		INSERT INTO RSAKeys ([session], [uid]) VALUES (@sesskey, @uid);
		SELECT @id = @@IDENTITY, @work = 1;

	END
	ELSE
	BEGIN
		UPDATE RSAKeys SET [session] = @sesskey WHERE [id] = @id AND [uid] = @uid;
		IF EXISTS(SELECT * FROM RSAKeys WHERE [id] = @id AND [uid] = @uid)
		BEGIN
			SET @work = 1;
		END
		ELSE
		BEGIN
			SET @work = 0;
		END
	END
	
	SELECT @id [id], @work [work];

END

GO
/****** Object:  StoredProcedure [dbo].[TestCompanyType]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TestCompanyType]
	-- Add the parameters for the stored procedure here
	@cid int,
	@type varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN (LOWER(ct.[Type]) = LOWER(@type)) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result]
	FROM Company c LEFT JOIN CompanyTypes ct ON c.[Type] = ct.ID
	WHERE c.CompanyID = @cid;

END

GO
/****** Object:  StoredProcedure [dbo].[TestCoverNotePossible]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TestCoverNotePossible]
	-- Add the parameters for the stored procedure here
	@riskid int,
	@effdate Date,
	@period int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN(COUNT(*)>0) THEN CAST(0 as bit) ELSE CAST(1 as bit) END) [result]
	FROM VehiclesUnderPolicy vup LEFT OUTER JOIN VehicleCoverNote vcn ON vup.PolicyID = vcn.PolicyID
								 LEFT OUTER JOIN VehicleCertificates vc ON vup.PolicyID = vc.PolicyID
								 LEFT JOIN Policies p ON vup.PolicyID = p.PolicyID

	WHERE (vup.VehicleID = @riskid AND vup.Cancelled = 0) --vehicle must be under policy and not cancelled
	  --AND (vcn.RiskItemNo = @riskid AND ((vcn.EffectiveDate > @effdate AND DATEADD(DAY, vcn.period, vcn.EffectiveDate) > DATEADD(DAY, @period, @effdate)) OR (vcn.EffectiveDate < @effdate AND DATEADD(DAY, vcn.period, vcn.EffectiveDate) < DATEADD(DAY, @period, @effdate)))) --testing if cover notes exist in the same time period
	  --AND (vc.RiskItemNo = @riskid AND ((vc.EffectiveDate > @effdate AND vc.ExpiryDate > DATEADD(DAY, @period, @effdate)) OR (vc.EffectiveDate < @effdate AND vc.ExpiryDate < DATEADD(DAY, @period, @effdate)))) -- testing if certificates exist in the same time period
	  AND (p.StartDateTime <= @effdate AND p.EndDateTime >= DATEADD(DAY, @period, @effdate)); -- testing if policy is still valid in time period



--	  DECLARE @riskid int, @period int, @effdate DATE;
--SET @riskid = 1; SET @period = 30; SET @effdate = '2015-11-20';

--SELECT 
----(CASE WHEN(COUNT(*)>0) THEN CAST(0 as bit) ELSE CAST(1 as bit) END) [result]
--*
--FROM VehiclesUnderPolicy vup LEFT OUTER JOIN VehicleCoverNote vcn ON vup.PolicyID = vcn.PolicyID
--								LEFT OUTER JOIN VehicleCertificates vc ON vup.PolicyID = vc.PolicyID
--								LEFT JOIN Policies p ON vup.PolicyID = p.PolicyID

--WHERE (vup.VehicleID = @riskid AND vup.Cancelled = 0) --vehicle must be under policy and not cancelled
----AND (vcn.RiskItemNo = @riskid AND ((vcn.EffectiveDate > @effdate AND DATEADD(DAY, vcn.period, vcn.EffectiveDate) > DATEADD(DAY, @period, @effdate)) OR (vcn.EffectiveDate < @effdate AND DATEADD(DAY, vcn.period, vcn.EffectiveDate) < DATEADD(DAY, @period, @effdate)))) --testing if cover notes exist in the same time period
----AND (vc.RiskItemNo = @riskid AND ((vc.EffectiveDate > @effdate AND vc.ExpiryDate > DATEADD(DAY, @period, @effdate)) OR (vc.EffectiveDate < @effdate AND vc.ExpiryDate < DATEADD(DAY, @period, @effdate)))) -- testing if certificates exist in the same time period
--AND (p.StartDateTime <= @effdate AND p.EndDateTime >= DATEADD(DAY, @period, @effdate)); -- testing if policy is still valid in time period


--SELECT CAST(@effdate as DATE) [p] FROM VehiclesUnderPolicy vup LEFT JOIN Policies p ON vup.PolicyID = p.PolicyID WHERE vup.VehicleID = @riskid AND p.StartDateTime <= @effdate AND p.EndDateTime >= DATEADD(DAY, @period, @effdate);
--SELECT @effdate [p];
--SELECT DATEADD(DAY, @period, @effdate);
END


GO
/****** Object:  StoredProcedure [dbo].[TestEmployee]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TestEmployee]
	-- Add the parameters for the stored procedure here
	@EmployeeID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @result bit;
	IF EXISTS (SELECT EmployeeID FROM Employees WHERE EmployeeID = @EmployeeID) SET @result = 1;
	ELSE SET @result = 0;
	SELECT @result as result;

END


GO
/****** Object:  StoredProcedure [dbo].[TestFirstLoginTimer]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TestFirstLoginTimer]
	-- Add the parameters for the stored procedure here
	@username varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @result bit;
	SELECT @result = (CASE WHEN (COUNT(*) > 0) THEN CAST (1 as bit) ELSE CAST (0 as bit) END)
	FROM Users WHERE username = @username AND firstlogin = 1 AND DATEDIFF(HOUR, (FirstLoginDateTime), CURRENT_TIMESTAMP) <= 24;

	IF (@result = 0) UPDATE Users SET LoggedIn = 0, LoginGUID = NULL WHERE username = @username;

	SELECT @result [result];

END

GO
/****** Object:  StoredProcedure [dbo].[TestGUID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TestGUID]
	-- Add the parameters for the stored procedure here
	 @GUID uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (@GUID IS NULL) SELECT CAST(0 as bit) [result]
	ELSE
		SELECT (CASE WHEN (Count([UID]) = 1) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result]
		FROM Users WHERE LoginGUID = @GUID AND LoggedIn = 1 AND  DATEDIFF(MINUTE, LastSuccessful, CURRENT_TIMESTAMP) <= 5 AND active = 1;
END


GO
/****** Object:  StoredProcedure [dbo].[TestIAJID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 30/07/2014
-- Description:	Checking ot see if an IAJID exists
-- =============================================

CREATE PROCEDURE [dbo].[TestIAJID]
	-- Add the parameters for the stored procedure here

	@CompanyIAJID int
	

AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @result bit;

	 IF NOT EXISTS (SELECT CompanyID FROM Company WHERE IAJID = @CompanyIAJID)
	 BEGIN
		 SET @result = 0;
		 SELECT @result AS [result];
     END

	 ELSE
		 BEGIN
			 SET @result = 1;
			 SELECT @result AS [result];
		 END
END




GO
/****** Object:  StoredProcedure [dbo].[TestKpiExist]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TestKpiExist]
	-- Add the parameters for the stored procedure here
	@companyID int,
	@type varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT datemode FROM KPIDateMode WHERE id NOT IN (
	SELECT KPIDateMode.id
	FROM KPI LEFT JOIN KPItype ON KPI.[type] = KPItype.id
			 LEFT JOIN KPIDateMode ON KPI.datemode = KPIDateMode.id
	WHERE company = @companyID AND KPItype.[type] = @type);
	
END

GO
/****** Object:  StoredProcedure [dbo].[TestManualCoverNoteNo]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TestManualCoverNoteNo]
	-- Add the parameters for the stored procedure here
	@covnoteno varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN (COUNT(*) > 0) THEN (CAST (0 as bit)) ELSE (CAST(1 as bit)) END) [result]
	FROM VehicleCoverNote WHERE ManualCoverNoteNo = @covnoteno;

END


GO
/****** Object:  StoredProcedure [dbo].[TestPrintedPaperNo]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TestPrintedPaperNo]
	-- Add the parameters for the stored procedure here
	@PPN int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS(SELECT VehicleCertificateID FROM VehicleCertificates WHERE PrintedPaperNo = @PPN)
		SELECT CAST(1 as bit) as result;
	ELSE
		SELECT CAST(0 as bit) as result;
END


GO
/****** Object:  StoredProcedure [dbo].[TestTRN]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TestTRN]
	-- Add the parameters for the stored procedure here
	@uid int,
	@trn varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN (EXISTS (SELECT [UID] FROM Users u LEFT JOIN Employees e ON u.EmployeeID = e.EmployeeID LEFT JOIN People p ON e.PersonID = p.PeopleID WHERE p.TRN = @trn AND [UID] = @uid)) THEN CAST(1 as bit) 
			 WHEN ((SELECT COUNT(*) FROM People WHERE TRN = @trn) = 0) THEN CAST (1 as bit)
			 ELSE CAST (0 as bit) END) [result];


END


GO
/****** Object:  StoredProcedure [dbo].[TestTRNAvailable]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TestTRNAvailable]
	-- Add the parameters for the stored procedure here
	@trn varchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN(COUNT(*) > 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result] FROM People p WHERE p.TRN = @trn;
END

GO
/****** Object:  StoredProcedure [dbo].[testUniqueNumber]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Chrystal Parchment
-- Create date: <Create Date,,>
-- Description:	Testing if a number has already been used by a certificate in the system
-- =============================================
CREATE PROCEDURE [dbo].[testUniqueNumber]
	-- Add the parameters for the stored procedure here
	@compID int,
	@UniqueNum int
AS
BEGIN
	
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS(SELECT VehicleCertificateID FROM VehicleCertificates WHERE UniqueNum = @UniqueNum AND CompanyID= @compID)
		
		SELECT CAST(1 as bit) as result;
	ELSE
		SELECT CAST(0 as bit) as result;
END

GO
/****** Object:  StoredProcedure [dbo].[TestUserGroupUnique]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TestUserGroupUnique]
	
	@usergroupId int = 0,
	@usergroup_name varchar(250),
	@company_id int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN(COUNT(*) = 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result] 
	FROM UserGroups WHERE UserGroup = @usergroup_name AND Company = @company_id AND (UGID != @usergroupId OR @usergroupId = 0);
END

GO
/****** Object:  StoredProcedure [dbo].[TestUsername]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TestUsername]
	-- Add the parameters for the stored procedure here
	@username varchar(55)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @result bit;
	IF EXISTS (SELECT UID FROM Users WHERE username = @username) SET @result = 1;
	ELSE SET @result = 0;
	SELECT @result as result;
END


GO
/****** Object:  StoredProcedure [dbo].[TestWebGUID]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TestWebGUID]
	-- Add the parameters for the stored procedure here
	@GUID uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN (Count([UID]) = 1) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result]
	FROM Users WHERE LoginGUID = @GUID AND LoggedIn = 1 AND active = 1;
END


GO
/****** Object:  StoredProcedure [dbo].[TogglePrintable]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TogglePrintable]
	
	@vcnid int,
	@uid int
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES
   
    SELECT @origdata = 'Vehicle Cover note ID: '+ CONVERT(varchar(MAX),@vcnid)+  ';Printable: ' +  CONVERT(varchar(MAX),Printable)
	FROM VehicleCoverNote WHERE VehicleCoverNoteID= @vcnid;
    
	UPDATE VehicleCoverNote
	SET Printable = ~Printable
	WHERE VehicleCoverNoteID = @vcnid;


	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@vcnid) + ' was modified from the database by' + CONVERT(varchar(MAX),@uid),
		   @newdata = 'Vehicle Cover note ID: '+ CONVERT(varchar(MAX),@vcnid)+  ';Printable: ' +  CONVERT(varchar(MAX),Printable) 
     FROM VehicleCoverNote WHERE VehicleCoverNoteID= @vcnid;
		
   EXEC LogAudit 'update', @uid , @details, 'VehicleCoverNote', @vcnid, @origdata, @newdata, 'update';


END

GO
/****** Object:  StoredProcedure [dbo].[TruePolicyDelete]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TruePolicyDelete]
	-- Add the parameters for the stored procedure here
	@id int,
	@userID int
AS
BEGIN
	
	SET NOCOUNT ON;

    DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

    INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	            SELECT 'delete', @userID, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),VUPID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID), 
		        'VehiclesUnderPolicy', VUPID, 'PolicyID: '+ CONVERT(varchar(MAX),@id)+ ';Vehicle: ' + CONVERT(varchar(MAX),VehicleID) 
		        ,null, 'delete' FROM VehiclesUnderPolicy WHERE PolicyID = @id;

	DELETE VehiclesUnderPolicy WHERE PolicyID = @id;


    INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	            SELECT 'delete', @userID, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),EDID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID), 
		        'ExceptedDrivers', EDID, 'PolicyID: '+ CONVERT(varchar(MAX),@id)+ ';Vehicle: ' + CONVERT(varchar(MAX),VehicleID) +  ';DriverID: ' + CONVERT(varchar(MAX),DriverID)  
		        ,null, 'delete' FROM AuthorizedDrivers WHERE PolicyID = @id;

	DELETE AuthorizedDrivers WHERE PolicyID = @id;


	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	            SELECT 'delete', @userID, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),EDID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID), 
		        'ExceptedDrivers', EDID, 'PolicyID: '+ CONVERT(varchar(MAX),@id)+ ';Vehicle: ' + CONVERT(varchar(MAX),VehicleID) +  ';DriverID: ' + CONVERT(varchar(MAX),DriverID)  
		        ,null, 'delete' FROM ExceptedDrivers WHERE PolicyID = @id;

	DELETE ExceptedDrivers WHERE PolicyID = @id;


	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	            SELECT 'delete', @userID, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),EDID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID), 
		        'ExcludedDrivers', EDID, 'PolicyID: '+ CONVERT(varchar(MAX),@id)+ ';Vehicle: ' + CONVERT(varchar(MAX),VehicleID) +  ';DriverID: ' + CONVERT(varchar(MAX),DriverID)  
		        ,null, 'delete' FROM ExcludedDrivers WHERE PolicyID = @id;

	DELETE ExcludedDrivers WHERE PolicyID = @id;


	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	            SELECT 'delete', @userID, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),PolicyInsuredID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID), 
		        'PolicyInsured', PolicyInsuredID, 'PolicyID: '+ CONVERT(varchar(MAX),@id)+ (CASE WHEN (CompanyID IS NOT NULL) THEN 'Insured Type: Company ; CompanyID: ' + CONVERT(varchar(MAX),CompanyID) ELSE  
		        'Insured Type: Individual ; PersonID: ' + CONVERT(varchar(MAX),PersonID) END), null, 'delete' FROM PolicyInsured WHERE PolicyID = @id;

	DELETE PolicyInsured WHERE PolicyID = @id;


	INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
	            SELECT 'delete', @userID, CURRENT_TIMESTAMP,  'The record ' + CONVERT(varchar(MAX),PolicyID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID), 
		        'Policies', PolicyID, 'PolicyID: '+ CONVERT(varchar(MAX),@id)+ ' ;CompanyID: '+ CONVERT(varchar(MAX),CompanyID)
				, null, 'delete' FROM Policies WHERE PolicyID = @id;

	DELETE Policies WHERE PolicyID = @id;

END


GO
/****** Object:  StoredProcedure [dbo].[UnLockMappingData]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UnLockMappingData]
	-- Add the parameters for the stored procedure here
	@mapid int,
		@userID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

     DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES 

	SELECT	@origdata = 'Company: '+ CONVERT(varchar(MAX),CompanyID) + ';lock: ' + CONVERT(varchar(MAX),[lock]) FROM MappingTable WHERE ID = @mapid;

	UPDATE MappingTable SET [lock] = 0 WHERE ID = @mapid;

	SELECT @details = ('The record ' + CONVERT(varchar(MAX), @mapid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID)),
		   @newdata = 'Company: '+ CONVERT(varchar(MAX),CompanyID) +'lock: ' + CONVERT(varchar(MAX),[lock]) FROM MappingTable WHERE ID = @mapid;

    EXEC LogAudit 'update', @userID , @details, 'MappingTable',  @@IDENTITY, @origdata, @newdata, 'update';

END

GO
/****** Object:  StoredProcedure [dbo].[UpdateAddressRequired]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateAddressRequired]
	-- Add the parameters for the stored procedure here
	@uid int,
	@roadno bit,
	@roadname bit,
	@roadtype bit,
	@zipcode bit,
	@country bit,
	@city bit,
	@createdby int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	DECLARE @ID int;
	SET @ID = 0;
	SELECT @ID = RAPCID FROM RequireAddressPerCompany
		JOIN Employees ON RequireAddressPerCompany.CompanyID = Employees.CompanyID
		JOIN Users ON Employees.EmployeeID = Users.EmployeeID
		WHERE Users.UID = @uid;
	IF (@ID != 0)
		BEGIN
			SELECT @origdata = 'RoadNumber:' + CONVERT(varchar(MAX),RoadNumber) + ';RoadName:' + CONVERT(varchar(MAX),RoadName) + ';RoadType:' + CONVERT(varchar(MAX),RoadType) + ';ZipCode:' + CONVERT(varchar(MAX),ZipCode) + ';Country:' + CONVERT(varchar(MAX),Country) + ';City:' + CONVERT(varchar(MAX),City) FROM RequireAddressPerCompany WHERE RAPCID = @ID;
			UPDATE RequireAddressPerCompany 
			SET RoadNumber = @roadno,
				RoadName = @roadname,
				RoadType = @roadtype,
				ZipCode = @zipcode,
				Country = @country,
				City = @city
			WHERE RAPCID = @ID;
			SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby),
				   @newdata = 'RoadNumber:' + CONVERT(varchar(MAX),@roadno) + ';RoadName:' + CONVERT(varchar(MAX),@roadname) + ';RoadType:' + CONVERT(varchar(MAX),@roadtype) + ';ZipCode:' + CONVERT(varchar(MAX),@zipcode) + ';Country:' + CONVERT(varchar(MAX),@country) + ';City:' + CONVERT(varchar(MAX),@city);
			EXEC LogAudit 'update', @createdby , @details, 'RequiredAddressPerCompany', @ID, @origdata, @newdata, 'update';
		END
	ELSE
		BEGIN
			INSERT INTO RequireAddressPerCompany (CompanyID, RoadNumber, RoadName, RoadType, ZipCode, Country, City)
				VALUES ((SELECT CompanyID FROM Employees JOIN IVIS.dbo.Users ON Employees.EmployeeID = Users.EmployeeID WHERE Users.UID = @uid), @roadno, @roadname, @roadtype, @zipcode, @country, @city);
			SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@createdby),
				   @origdata = null,
				   @newdata = 'RoadNumber:' + CONVERT(varchar(MAX),@roadno) + ';RoadName:' + CONVERT(varchar(MAX),@roadname) + ';RoadType:' + CONVERT(varchar(MAX),@roadtype) + ';ZipCode:' + CONVERT(varchar(MAX),@zipcode) + ';Country:' + CONVERT(varchar(MAX),@country) + ';City:' + CONVERT(varchar(MAX),@city);
			EXEC LogAudit 'insert', @createdby , @details, 'RequiredAddressPerCompany', @@IDENTITY, @origdata, @newdata, 'insert';
		END
END


GO
/****** Object:  StoredProcedure [dbo].[UpdateCertFormatting]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 01/09/2014
-- Description:	Editing a certificate format
-- =============================================
CREATE PROCEDURE [dbo].[UpdateCertFormatting]
	@title varchar(250),
	@format text,
	@certFormatId int,
	@uid int

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	SELECT @origdata = 'CertificateFormat:' + CONVERT(varchar(MAX),CertificateFormat)+';Title:' + title FROM CertificateFormatting WHERE ID = @certFormatId;

    UPDATE CertificateFormatting SET LastModifiedBy = @uid, title = @title, CertificateFormat = @format WHERE ID = @certFormatId;


    SELECT @details = 'The record ' + CONVERT(varchar(MAX),@certFormatId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
					   @newdata = 'CertificateFormat:' + CONVERT(varchar(MAX),@format)+';Title:' + @title;
					   	             
							
	EXEC LogAudit 'update', @uid , @details, 'CertificateFormatting',  @certFormatId, @origdata, @newdata, 'update';

END


GO
/****** Object:  StoredProcedure [dbo].[UpdateCertificatesState]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateCertificatesState]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE VehicleCertificates
	SET [state] = (SELECT VCNSID FROM VehicleCoverNoteState WHERE [state] = 'approved')
	WHERE VehicleCertificateID IN 
		(SELECT vc.VehicleCertificateID FROM VehicleCertificates vc LEFT JOIN VehicleCoverNoteState s ON vc.state = s.VCNSID
										WHERE EffectiveDate <= CURRENT_TIMESTAMP AND s.state = 'pending'); --ACTIVATE THESE

	UPDATE VehicleCertificates
	SET [state] = (SELECT VCNSID FROM VehicleCoverNoteState WHERE [state] = 'expired')
	WHERE VehicleCertificateID IN 
		(SELECT vc.VehicleCertificateID FROM VehicleCertificates vc LEFT JOIN VehicleCoverNoteState s ON vc.state = s.VCNSID
										WHERE ExpiryDate > CURRENT_TIMESTAMP AND s.state = 'approved'); --DEACTIVATE THESE



END


GO
/****** Object:  StoredProcedure [dbo].[UpdateCompany]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateCompany]
	-- Add the parameters for the stored procedure here
	@CompanyID int,
	@IAJID varchar (max) = null,
	@CompanyName varchar(250),
	@CompanyType varchar(50),
	@floginlimit int,
	@aid int,
	@trn varchar(13),
	@enabled bit,
	@inscode varchar(20) = null,
	@short varchar(20) = null,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON 
	SET NOCOUNT ON;

    
	DECLARE  @compType int;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	

	-- This tests are for companies that are not insurers/brokers/agents (regular companies added to the system)-which will have no company type

    IF (@CompanyType = ' ')
		BEGIN 
			SET @compType = null;
		END
	ELSE
		BEGIN

		  IF NOT EXISTS (SELECT ID FROM CompanyTypes WHERE Type = @CompanyType)
			 BEGIN
				 INSERT INTO CompanyTypes([Type]) VALUES (@CompanyType);
				 SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
						   @origdata = null,
						   @newdata = 'CompanyType:' + @CompanyType;
				 EXEC LogAudit 'insert', @uid , @details, 'CompanyTypes', @@IDENTITY, @origdata, @newdata, 'insert';
			 END

		  ELSE
			 SELECT @compType = ID FROM CompanyTypes WHERE [Type] = @CompanyType;
		END


	SELECT @origdata = 'IAJID:' + IAJID + ';CompanyName:' + CompanyName + ';Type:' + CompanyTypes.Type + ';TRN:' + TRN+ ';AddressID:' + CONVERT(varchar(MAX),CompanyAddress)
	FROM Company JOIN CompanyTypes ON Company.Type = CompanyTypes.ID
	WHERE CompanyID = @CompanyID;

	
	UPDATE Company
	SET IAJID = (CASE WHEN(@IAJID IS NULL OR @IAJID = '') THEN NULL ELSE @IAJID END),
		CompanyName = @CompanyName,
		faultyloginlimit = @floginlimit,
		Type = @compType,
		CompanyAddress = @aid,
		LastModifiedBy = @uid,
		TRN = @trn,
		[Enabled] = @enabled,
		InsurerCode = (CASE WHEN(@inscode IS NULL OR @inscode = '') THEN InsurerCode ELSE @inscode END),
		CompShortening = (CASE WHEN(@short IS NULL OR @short = '') THEN CompShortening ELSE @short END)
	FROM Company
	WHERE CompanyID = @CompanyID; 

	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@CompanyID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
		   @newdata = 'IAJID:' + @IAJID + ';CompanyName:' + @CompanyName + ';Type:' + @CompanyType + ';TRN:' + @trn 
		              + ';AddressID:' + CONVERT(varchar(MAX),@aid);
		
	
	EXEC LogAudit 'update', @uid , @details, 'Company', @CompanyID, @origdata, @newdata, 'update';
	 
END

GO
/****** Object:  StoredProcedure [dbo].[UpdateCompanyforemployee]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chrystal Parchment
-- Create date: 12/06/2014
-- Description:	Updates the companyId of an employee, who has switched companies
-- =============================================

 CREATE PROCEDURE [dbo].[UpdateCompanyforemployee]
	-- Add the parameters for the stored procedure here
	@empId int,
	@companyId int,
	@updateuid int
	
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES

	SELECT @origdata = 'Employee:' +  CONVERT(varchar(MAX),@empid) +  ';PersonID:' + CONVERT(varchar(MAX),People.PeopleID) + ';Person:' + People.FName + ' ' + People.MName + ' '  + People.LName  + ';CompanyID:' + CONVERT(varchar(MAX),Company.CompanyID) + ';CompanyName:' + Company.CompanyName
	FROM Employees	JOIN Company ON Employees.CompanyID = Company.CompanyID
					JOIN People ON Employees.PersonID = People.PeopleID	
	WHERE Employees.EmployeeID = @empid;
	
	UPDATE Employees SET CompanyID = @companyId WHERE EmployeeID = @empId;

	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@empid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@updateuid),
		   @newdata = 'Employee:' +  CONVERT(varchar(MAX),@empid) +  ';PersonID:' + CONVERT(varchar(MAX),People.PeopleID) + ';Person:' + People.FName + ' ' + People.MName + ' '  + People.LName  + ';CompanyID:' + CONVERT(varchar(MAX),Company.CompanyID) + ';CompanyName:' + Company.CompanyName
	FROM Employees	JOIN Company ON Employees.CompanyID = Company.CompanyID
					JOIN People ON Employees.PersonID = People.PeopleID	
	WHERE Employees.EmployeeID = @empid;
	EXEC LogAudit 'update', @updateuid , @details, 'Employees', @empid, @origdata, @newdata, 'update';

END


GO
/****** Object:  StoredProcedure [dbo].[UpdateCover]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateCover]
	-- Add the parameters for the stored procedure here
	@cid int,
	@prefix varchar(100),
	@cover varchar(250),
	@commercial bit,
	@uid int
AS
BEGIN

	SET NOCOUNT ON;

     DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES

     SELECT @origdata = 'Policy Cover: ' +  CONVERT(varchar(MAX),ID) + ';Prefix: '+  Prefix+ ';IsCommercialBusiness: '+  CONVERT(varchar(MAX),IsCommercialBusiness) 
	                    +';cover: '+  Cover FROM PolicyCovers WHERE ID = @cid;

	 UPDATE PolicyCovers SET Prefix = @prefix, Cover = @cover, IsCommercialBusiness = @commercial, LastModifiedBy = @uid WHERE ID = @cid;

	 SELECT @newdata ='Policy Cover: ' +  CONVERT(varchar(MAX),@cid) + ';Prefix: '+  @prefix+ ';IsCommercialBusiness: '+  CONVERT(varchar(MAX),@commercial) 
	               + ';cover: '+  @cover,
		   @details = 'The record ' + CONVERT(varchar(MAX), @cid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid);

	 EXEC LogAudit 'update', @uid , @details, 'PolicyCovers', @cid, @origdata, @newdata, 'update';

END


GO
/****** Object:  StoredProcedure [dbo].[UpdateEmployee]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateEmployee]
	-- Add the parameters for the stored procedure here
	@empid int,
	@pid int,
	@cid int,
	@updateduid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES

    -- Insert statements for procedure here

	
	SELECT @origdata = 'Employee:' +  CONVERT(varchar(MAX),@empid) +  ';PersonID:' + CONVERT(varchar(MAX),People.PeopleID) + ';Person:' + People.FName + ' ' + People.MName + ' '  + People.LName  + ';CompanyID:' + CONVERT(varchar(MAX),Company.CompanyID) + ';CompanyName:' + Company.CompanyName
	FROM Employees	JOIN Company ON Employees.CompanyID = Company.CompanyID
					JOIN People ON Employees.PersonID = People.PeopleID	
	WHERE Employees.EmployeeID = @empid;

	UPDATE Employees SET PersonID = @pid, CompanyID = @cid WHERE EmployeeID = @empid;

	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@empid) + ' was modified from the database by ' + CONVERT(varchar(MAX),@updateduid),
		   @newdata = 'Employee:' +  CONVERT(varchar(MAX),@empid) +  ';PersonID:' + CONVERT(varchar(MAX),People.PeopleID) + ';Person:' + People.FName + ' ' + People.MName + ' '  + People.LName  + ';CompanyID:' + CONVERT(varchar(MAX),Company.CompanyID) + ';CompanyName:' + Company.CompanyName
	FROM Employees	JOIN Company ON Employees.CompanyID = Company.CompanyID
					JOIN People ON Employees.PersonID = People.PeopleID	
	WHERE Employees.EmployeeID = @empid;
	EXEC LogAudit 'update', @updateduid , @details, 'Employees', @empid, @origdata, @newdata, 'update';
END


GO
/****** Object:  StoredProcedure [dbo].[UpdateIntermediary]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 16/09/2014
-- Description:	Adding an intermediary company to an insurance company
-- =============================================

CREATE PROCEDURE [dbo].[UpdateIntermediary]

	@CompanyID int,
	@intermediaryCompId int,
	@covernote bit = false,
	@certificate bit = false,
	@enabled bit = false,
	@printlimit int = 0,
	@userID int

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES


    IF EXISTS (SELECT CTID FROM InsurerToIntermediary WHERE Insurer = @CompanyID AND Intermediary = @intermediaryCompId)
	BEGIN
		SELECT @origdata = 'InsuranceCompId: ' + CONVERT(varchar(MAX), @CompanyID) + 
						   ';IntermediaryCompId: ' + CONVERT(varchar(MAX), @intermediaryCompId) +
						   ';Certificate: ' + CONVERT(varchar(MAX), [Certificate]) +
						   ';CoverNote: ' + CONVERT(varchar(MAX), [CoverNote]) +
						   ';PrintLimit' + CONVERT(varchar(MAX), PrintLimit)
		FROM InsurerToIntermediary WHERE Insurer = @CompanyID AND Intermediary = @intermediaryCompId;

		UPDATE InsurerToIntermediary
		SET [Certificate] = @certificate,
			[CoverNote] = @covernote,
			PrintLimit = @printlimit,
			[enabled] = @enabled,
			LastModifiedBy = @userID
		WHERE Insurer = @CompanyID AND Intermediary = @intermediaryCompId;

		SELECT @details = ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was inserted into the database by ' + CONVERT(varchar(MAX),@userID)),
			   @newdata = 'InsuranceCompId: ' + CONVERT(varchar(MAX), @CompanyID) + 
						  ';IntermediaryCompId: ' + CONVERT(varchar(MAX), @intermediaryCompId) +
						  ';Certificate: ' + CONVERT(varchar(MAX), [Certificate]) +
						  ';CoverNote: ' + CONVERT(varchar(MAX), [CoverNote]) +
						  ';PrintLimit' + CONVERT(varchar(MAX), PrintLimit)
		FROM InsurerToIntermediary WHERE Insurer = @CompanyID AND Intermediary = @intermediaryCompId;
		EXEC LogAudit 'insert', @userID , @details, 'InsurerToIntermediary',  @@IDENTITY, @origdata, @newdata, 'update';

	END

	

END
GO
/****** Object:  StoredProcedure [dbo].[UpdateKPI]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateKPI]
	-- Add the parameters for the stored procedure here
	@kpiid int,
	@kpi int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE KPI SET kpi = @kpi WHERE id = @kpiid;


END

GO
/****** Object:  StoredProcedure [dbo].[UpdateMappingData]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateMappingData]
	-- Add the parameters for the stored procedure here
	@id int,
	@refid int,
	@userID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

    -- Insert statements for procedure here
	IF (SELECT [lock] FROM MappingTable WHERE ID = @id) = 0
	BEGIN
	   
	  
		SELECT	@origdata = 'RefID: ' + CONVERT(varchar(MAX), RefID) FROM MappingTable WHERE ID = @id;

		UPDATE MappingTable SET RefID = @refid WHERE ID = @id;


		SELECT @details = ('The record ' + CONVERT(varchar(MAX),@id) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID)),
			   @newdata = 'RefID: ' + CONVERT(varchar(MAX), RefID) FROM MappingTable WHERE ID = @id;

		EXEC LogAudit 'update', @userID , @details, 'MappingTable',  @id, @origdata, @newdata, 'update';


	
		IF (SELECT [Table] FROM MappingTable WHERE ID = @id) = 'parish'
		BEGIN
		   
		    DECLARE @AddresId int;
			SELECT @AddresId = AID FROM [Address] WHERE ParishMap = @id;

		   	SELECT	@origdata = 'Parish: ' + CONVERT(varchar(MAX), Parish) FROM [Address] WHERE AID = @AddresId;

			UPDATE [Address] SET Parish = (CASE WHEN (@refid = 0) THEN NULL ELSE @refid END) WHERE ParishMap = @id;

			SELECT @details = ('The record ' + CONVERT(varchar(MAX),@AddresId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@userID)),
			       @newdata = 'Parish: ' + CONVERT(varchar(MAX), Parish) FROM [Address] WHERE AID = @AddresId;

		    EXEC LogAudit 'update', @userID , @details, 'Address',  @AddresId, @origdata, @newdata, 'update';


		END

		SELECT CAST(1 as bit) [result];
	END
	ELSE SELECT CAST(0 as bit) [result];
END

GO
/****** Object:  StoredProcedure [dbo].[UpdatePerson]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdatePerson]
	-- Add the parameters for the stored procedure here
	@personid int,
	@fname varchar(250),
	@mname varchar(250) = null,
	@lname varchar(250),
	@aid int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	UPDATE People
	SET FName = @fname,
		MName = (CASE WHEN(@mname IS NULL OR @mname = '') THEN NULL ELSE @mname END),
		LName = @lname,
		[Address] = @aid,
		LastModifiedBy = @uid
	WHERE PeopleID = @personid;

END

GO
/****** Object:  StoredProcedure [dbo].[UpdateUserGroupData]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateUserGroupData]
	-- Add the parameters for the stored procedure here
	@UGID int,
	@UserGroupName varchar(250),
	@modifiedby int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); DECLARE @id int; -- LOG AUDIT VARIABLES

	SELECT @origdata = 'UserGroupName:' +  UserGroup +  ';SuperGroup:' + CONVERT(varchar(MAX),SuperGroup)
	FROM UserGroups
	WHERE UGID = @UGID;

	UPDATE UserGroups SET UserGroup = @UserGroupName, LastModifiedBy = @modifiedby WHERE UGID = @UGID;

	SELECT @details = 'The record ' + CONVERT(varchar(MAX),@UGID) + ' was modified from the database by ' + CONVERT(varchar(MAX),@modifiedby),
		   @newdata = 'UserGroupName:' +  UserGroup +  ';SuperGroup:' + CONVERT(varchar(MAX),SuperGroup)
	FROM UserGroups
	WHERE UGID = @UGID;

	EXEC LogAudit 'update', @modifiedby , @details, 'UserGroups', @UGID, @origdata, @newdata, 'update';
END


GO
/****** Object:  StoredProcedure [dbo].[UpdateUserGroupPermission]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateUserGroupPermission]
	-- Add the parameters for the stored procedure here
	@UGID int,
	@UPID int,
	@createdby int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX), @newdata varchar(MAX);

    IF NOT EXISTS(SELECT PGAID FROM PermissionGroupAssoc WHERE UPID = @UPID AND UGID = @UGID)
	BEGIN
		INSERT INTO PermissionGroupAssoc (UPID, UGID) VALUES (@UPID, @UGID);

		SET @newdata = 'UPID:' + CONVERT(varchar(MAX),@UPID) + ';UGID:' + CONVERT(varchar(MAX),@UGID) + 'insert';
		SET @details = 'The record ' + CONVERT(varchar(MAX), @@IDENTITY) + ' was added to the system by ' + CONVERT(varchar(MAX), @createdby);
		EXEC LogAudit 'insert', @createdby, @details, 'PermissionGroupAssoc', @@IDENTITY, null, @newdata, 'insert';
	END
	
END

GO
/****** Object:  StoredProcedure [dbo].[UpdateUserGroupPermissions]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateUserGroupPermissions]
	-- Add the parameters for the stored procedure here
	@UGID int,
	@PermissionTable varchar(255),
	@PermissionAction varchar(255),
	@createdby int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @UPID int;
	--SELECT @UGID = UGID FROM UserGroups WHERE UserGroup = @UserGroupName;
	SELECT @UPID = UPID FROM UserPermissions WHERE [Table] = @PermissionTable AND [Action] = @PermissionAction;

	IF NOT EXISTS (SELECT PGAID FROM PermissionGroupAssoc WHERE UGID = @UGID AND UPID = @UPID)
		BEGIN
			INSERT INTO PermissionGroupAssoc (UPID, UGID) VALUES (@UPID, @UGID);
			INSERT INTO SystemAuditLog ([Action], ActionUserID, ActionTimeStamp, ActionDetails, Location, RecordID, OrigData, NewData, AuditType)
			SELECT 'insert',
				   @createdby,
				   CURRENT_TIMESTAMP,
				   'The record ' + CONVERT(varchar(MAX), @@IDENTITY) + ' has been added from the system by ' + CONVERT(varchar(MAX), @createdby),
				   'PermissionGroupAssoc',
				   @@IDENTITY,
				   null,
				   'UserGroupID:' + CONVERT(varchar(MAX),@UGID) + ';UserGroupName:' + (SELECT UserGroup FROM UserGroups WHERE UGID = @UGID) + ';UserPermissionID:' + CONVERT(varchar(MAX),@UPID) + ';UserPermissionName:' + (SELECT [Action] + [Table] FROM UserPermissions WHERE UPID = @UPID),
				   'insert';
			
		END
	
END

GO
/****** Object:  StoredProcedure [dbo].[UpdateVehicleCertificate]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 14/08/2014
-- Description:	Editing a vehicle certificate
-- =============================================
CREATE PROCEDURE [dbo].[UpdateVehicleCertificate]

    @VehicleCertId int,
	@VehicleID int,
	@editedBy int,
	@wordingTemplateID int,
	@PrintedPaperNo int = null

AS
BEGIN
	
	SET NOCOUNT ON;

    DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES
	DECLARE @result int;

	SELECT @origdata = 'Certificate ID: ' + CONVERT(varchar(MAX), @VehicleCertId)+
	                   ';RiskId: ' + CONVERT(varchar(MAX), RiskItemNo) +';PrintedPaperNumber: ' + CONVERT(varchar(MAX),PrintedPaperNo)+';Wording: ' + CONVERT(varchar(MAX),WordingTemplate)
					    FROM VehicleCertificates WHERE VehicleCertificateID = @VehicleCertId;


	UPDATE VehicleCertificates SET  PrintedPaperNo= @PrintedPaperNo, WordingTemplate = (CASE WHEN @wordingTemplateID = 0 THEN NULL ELSE @wordingTemplateID END),
	                               RiskItemNo= @VehicleID , LastModifiedBy= @editedBy WHERE VehicleCertificateID = @VehicleCertId;


    SELECT @details = 'The record ' + CONVERT(varchar(MAX),@VehicleCertId) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy),
		   @newdata = 'Certificate ID: ' + CONVERT(varchar(MAX), @VehicleCertId)+ ';RiskId: ' + CONVERT(varchar(MAX),RiskItemNo)+';PrintedPaperNumber: ' + CONVERT(varchar(MAX),PrintedPaperNo)+';Wording: ' + CONVERT(varchar(MAX),WordingTemplate)
		               FROM VehicleCertificates WHERE VehicleCertificateID = @VehicleCertId;;
					             
							
	EXEC LogAudit 'update', @editedBy , @details, 'VehicleCertificates',  @VehicleCertId, @origdata, @newdata, 'update';


	SET @result = 1;
	SELECT @result AS [result];

END


GO
/****** Object:  StoredProcedure [dbo].[UpdateVehicleCoverNote]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateVehicleCoverNote]
	-- Add the parameters for the stored procedure here
	@alterations text =null,
	@effectivedate datetime,
	@expirydate datetime,
	@period int,
	@endorsementno varchar(100) = null,
	@covernoteno VARCHAR(250) =null,
	@coverid varchar(100) =null,
	@wording int,
	@covernoteID int,
	@uid int
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); 
	DECLARE @cancelled int ;

	SELECT @cancelled =  Cancelled FROM VehicleCoverNote WHERE VehicleCoverNoteID = @covernoteID;

	IF (@cancelled = 0)
	BEGIN
		
		SELECT @origdata = 'CoverNoteID: ' +  CONVERT(varchar(MAX),covernoteid) +';CompanyID: '+ CONVERT(varchar(MAX),CompanyID) +';Alterations: '+ CONVERT(varchar(MAX),Alterations) + 
		                   ';EffectiveDate: ' + CONVERT(varchar(MAX),effectivedate) + ';ExpiryDate: ' + ';EndorsementNo: ' + CONVERT(varchar(MAX),EndorsementNo) + 
		                   ';RiskItemNo: ' +  CONVERT(varchar(MAX),RiskItemNo) + ';WordingId: ' + CONVERT(varchar(MAX),WordingTemplate) + ';WordingId: ' + CONVERT(varchar(MAX),WordingTemplate)+
						   ';CoverNoteNo: ' + CONVERT(varchar(MAX), ManualCoverNoteNo)
						    FROM VehicleCoverNote WHERE VehicleCoverNoteID = @covernoteid;

							
		UPDATE VehicleCoverNote
		SET Alterations = @alterations,
			EffectiveDate = @effectivedate,
			ExpiryDateTime = @expirydate,
			EndorsementNo = (CASE WHEN (@endorsementno IS NULL OR @endorsementno = '') THEN NULL ELSE @endorsementno END),
			ManualCoverNoteNo = @covernoteno,
			CoverNoteID = @coverid,
			period = @period,
			LastModifiedOn  = CURRENT_TIMESTAMP,
		    WordingTemplate = (CASE WHEN @wording = 0 THEN NULL ELSE @wording END)
		WHERE VehicleCoverNoteID = @covernoteID;
		

		SELECT @details = 'The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@uid),
		       @origdata = null,
			   @newdata = 'CoverNoteID: ' +  CONVERT(varchar(MAX),covernoteid) +';CompanyID: '+ CONVERT(varchar(MAX),CompanyID) +';Alterations: '+ CONVERT(varchar(MAX),Alterations) + 
		                   ';EffectiveDate: ' + CONVERT(varchar(MAX),effectivedate) + ';ExpiryDate: ' + ';EndorsementNo: ' + CONVERT(varchar(MAX),EndorsementNo) + 
		                   ';RiskItemNo: ' +  CONVERT(varchar(MAX),RiskItemNo) + ';WordingId: ' + CONVERT(varchar(MAX),WordingTemplate) + ';WordingId: ' + CONVERT(varchar(MAX),WordingTemplate)+
						   ';CoverNoteNo: ' + CONVERT(varchar(MAX), ManualCoverNoteNo)
						    FROM VehicleCoverNote WHERE VehicleCoverNoteID = @covernoteid;

		EXEC LogAudit 'update', @uid , @details, 'VehicleCoverNote', @@IDENTITY, @origdata, @newdata, 'update';


		SELECT CAST(1 as bit) result;
	END
	ELSE 
	BEGIN
		SELECT CAST(0 as bit) result;
	END

END

GO
/****** Object:  StoredProcedure [dbo].[UpdateWording]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateWording] 
	-- Add the parameters for the stored procedure here
	@ID int,
	@editedby int,
	@certtype varchar(30),
	@certid varchar(30),
	@certinscode varchar(30),
	@extcode varchar(30),
	@cover int,
	@usage int,
	@excludeddriver bit,
	@excludedinsured bit,
	@multiplevehicle bit,
	@registeredowner bit,
	@restricteddriver bit,
	@scheme bit,
	@authorizeddrivers varchar(MAX),
	@limits varchar(MAX),
	@vehdesc varchar(MAX),
	@vehregno varchar(MAX),
	@policyholder varchar(MAX),
	@imported bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @details varchar(MAX); DECLARE @origdata varchar(MAX); DECLARE @newdata varchar(MAX); -- LOG AUDIT VARIABLES

	SELECT @details =  ('The record ' + CONVERT(varchar(MAX),@@IDENTITY) + ' was modified from the database by ' + CONVERT(varchar(MAX),@editedBy)),
		   @origdata =	'CertificateType: ' + CertificateType+ ';CertID:' + CertificateID + ';Cert Ins. Code: ' + CertificateInsuredCode + ';extcode: ' + ExtensionCode + ';cover: '+ CONVERT(varchar(MAX),PolicyCover) + 
						';Usage: ' + CONVERT(varchar(MAX),usageid) + ';excludedDriver: ' + CONVERT(varchar(MAX),ExcludedDriver)+';excludedinsured: ' + CONVERT(varchar(MAX),ExcludedDriver) +
						';multiplevehicle: ' + CONVERT(varchar(MAX),MultipleVehicle)+ ';registeredowner: ' + CONVERT(varchar(MAX),RegisteredOwner)+ ';restricteddriver: ' + CONVERT(varchar(MAX),RestrictedDriver)+
						';Scheme: ' + CONVERT(varchar(MAX),Scheme) + ';authorizeddrivers: ' + AuthorizedDrivers +  ';limits: ' + LimitsOfUse +  ';vehdesc: ' + VehicleDesc + ';vehregno: ' + VehRegNo + 
						';policyholder: ' + PolicyHolders +  ';Imported: '+ CONVERT(varchar(MAX),Imported)
	FROM Wording WHERE ID= @ID;


	UPDATE Wording 
	SET LastModifiedBy = @editedby,
		LastModified = CURRENT_TIMESTAMP,
		CertificateType = @certtype,
		CertificateID = @certid,
		CertificateInsuredCode = @certinscode,
		ExtensionCode = @extcode,
		PolicyCover = @cover,
		usageid = @usage,
		ExcludedDriver = @excludeddriver,
		MultipleVehicle = @multiplevehicle,
		RegisteredOwner = @registeredowner,
		RestrictedDriver = @restricteddriver,
		Scheme = @scheme,
		AuthorizedDrivers = @authorizeddrivers,
		ExcludedInsured = @excludedinsured,
		LimitsOfUse = @limits,
		VehicleDesc = @vehdesc,
		VehRegNo = @vehregno,
		PolicyHolders = @policyholder,
		Imported = @imported
	WHERE ID = @ID;


	SELECT @newdata =	'CertificateType: ' + @certtype+ ';CertID:' + @certid + ';Cert Ins. Code: ' + @certinscode + ';extcode: ' + @extcode + ';cover: '+ CONVERT(varchar(MAX),@cover) + 
						';Usage: ' + CONVERT(varchar(MAX),@usage) + ';excludedDriver: ' + CONVERT(varchar(MAX),@excludeddriver)+';excludedinsured: ' + CONVERT(varchar(MAX),@excludedinsured) +
						';multiplevehicle: ' + CONVERT(varchar(MAX),@multiplevehicle)+ ';registeredowner: ' + CONVERT(varchar(MAX),@registeredowner)+ ';restricteddriver: ' + CONVERT(varchar(MAX),@restricteddriver)+
						';Scheme: ' + CONVERT(varchar(MAX),@scheme) + ';authorizeddrivers: ' + @authorizeddrivers +  ';limits: ' + @limits +  ';vehdesc: ' + @vehdesc + ';vehregno: ' + @vehregno + 
						';policyholder: ' + @policyholder +  ';Imported: '+ CONVERT(varchar(MAX),@imported);
						  
	EXEC LogAudit 'update', @editedby, @details, 'Wording',  @ID, @origdata, @newdata, 'update';

	
END

GO
/****** Object:  StoredProcedure [dbo].[UserGenSearch]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserGenSearch]
	-- Add the parameters for the stored procedure here
	@term varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @search varchar(MAX); DECLARE @run bit; DECLARE @i int;
	SET @run = 1; SET @i = 0;
	SET @search = SUBSTRING(@term, 1, (CASE WHEN (CHARINDEX('&', @term) > 0) THEN (CHARINDEX('&', @term) - 1) ELSE LEN(@term) END));
	SET @term = SUBSTRING(@term, 2 + LEN(@search), LEN(@term)); 
	SET @search = '%' + @search + '%';

	CREATE TABLE #TempTable(ID int); CREATE TABLE #TempTable2 (ID int);

	WHILE @run = 1
	BEGIN
	
		SET @i = @i + 1;

    -- Insert statements for procedure here
		IF (@i = 1)
			INSERT INTO #TempTable (ID)
			SELECT u.UID [ID]--, 'user' [table], u.username [name]
				FROM Users u 
				WHERE u.username LIKE @search;
		ELSE
		BEGIN
			INSERT INTO #TempTable2 (ID) SELECT ID FROM #TempTable;
			DELETE #TempTable;
			INSERT INTO #TempTable (ID)
			SELECT u.UID [ID]--, 'user' [table], u.username [name]
				FROM Users u 
				WHERE u.username LIKE @search AND u.UID IN (SELECT ID FROM #TempTable2);
		END
		
		IF(LEN(@term) = 0) break;
		
		SET @search = SUBSTRING(@term, 1, (CASE WHEN (CHARINDEX('&', @term) > 0) THEN (CHARINDEX('&', @term) - 1) ELSE LEN(@term) END));
		SET @term = SUBSTRING(@term, 2 + LEN(@search), LEN(@term)); 
		SET @search = '%' + @search + '%';

	END

	SELECT u.UID [ID], 'user' [table], u.username [name]
			FROM Users u 
			WHERE u.[UID] IN (SELECT ID FROM #TempTable);

	DROP TABLE #TempTable;DROP TABLE #TempTable2;

			
END


GO
/****** Object:  StoredProcedure [dbo].[UserSearchDeep]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserSearchDeep]
	-- Add the parameters for the stored procedure here
	@term varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @search varchar(MAX); DECLARE @run bit; DECLARE @i int;
	SET @run = 1; SET @i = 0;
	SET @search = SUBSTRING(@term, 1, (CASE WHEN (CHARINDEX('&', @term) > 0) THEN (CHARINDEX('&', @term) - 1) ELSE LEN(@term) END));
	SET @term = SUBSTRING(@term, 2 + LEN(@search), LEN(@term)); 
	SET @search = '%' + @search + '%';

	CREATE TABLE #TempTable(ID int); CREATE TABLE #TempTable2 (ID int);

	WHILE @run = 1
	BEGIN
	
		SET @i = @i + 1;

    -- Insert statements for procedure here
		IF (@i = 1)
			INSERT INTO #TempTable (ID)
			SELECT u.UID [ID]--, 'user' [table], u.username [name]
				FROM Users u 
				WHERE u.username LIKE @search;
		ELSE
		BEGIN
			INSERT INTO #TempTable2 (ID) SELECT ID FROM #TempTable;
			DELETE #TempTable;
			INSERT INTO #TempTable (ID)
			SELECT u.UID [ID]--, 'user' [table], u.username [name]
				FROM Users u 
				WHERE u.username LIKE @search AND u.UID IN (SELECT ID FROM #TempTable2);
		END
		
		IF(LEN(@term) = 0) break;
		
		SET @search = SUBSTRING(@term, 1, (CASE WHEN (CHARINDEX('&', @term) > 0) THEN (CHARINDEX('&', @term) - 1) ELSE LEN(@term) END));
		SET @term = SUBSTRING(@term, 2 + LEN(@search), LEN(@term)); 
		SET @search = '%' + @search + '%';

	END

	SELECT [UID],username,reset,faultylogins,lastloginattempt,password_reset_force, passwordresetrequesttime, active FROM Users 
		WHERE username LIKE '%'+@search+'%' AND Filter = 0 AND [UID] IN (SELECT ID FROM #TempTable);;


	--SELECT u.UID [ID], 'user' [table], u.username [name]
	--		FROM Users u 
	--		WHERE u.[UID] IN (SELECT ID FROM #TempTable);

	DROP TABLE #TempTable;DROP TABLE #TempTable2;
END


GO
/****** Object:  StoredProcedure [dbo].[VehicleCoveredByMyPolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[VehicleCoveredByMyPolicy]
	-- Add the parameters for the stored procedure here
	@vehid int,
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN (COUNT(*) > 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) [result]
	FROM VehiclesUnderPolicy vup LEFT JOIN Policies p ON vup.PolicyID = p.PolicyID
	WHERE	vup.VehicleID = @vehid AND vup.Cancelled = 0 
		AND p.StartDateTime <= CURRENT_TIMESTAMP AND p.EndDateTime >= CURRENT_TIMESTAMP AND p.Cancelled = 0
		AND (SELECT COUNT(*) FROM Employees e LEFT JOIN Users u ON e.EmployeeID = u.EmployeeID WHERE u.[UID] = @uid AND e.CompanyID IN (p.CompanyCreatedBy, p.CompanyID)) > 0
	 
END


GO
/****** Object:  StoredProcedure [dbo].[VehicleGenSearch]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[VehicleGenSearch]
	-- Add the parameters for the stored procedure here
	@term varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @search varchar(MAX); DECLARE @run bit; DECLARE @i int;
	SET @run = 1; SET @i = 0;
	SET @search = SUBSTRING(@term, 1, (CASE WHEN (CHARINDEX('&', @term) > 0) THEN (CHARINDEX('&', @term) - 1) ELSE LEN(@term) END));
	SET @term = SUBSTRING(@term, 2 + LEN(@search), LEN(@term)); 
	SET @search = '%' + @search + '%';

	CREATE TABLE #TempTable(ID int); CREATE TABLE #TempTable2 (ID int);

	WHILE @run = 1
	BEGIN
	
		SET @i = @i + 1;

		-- Insert statements for procedure here
		IF (@i = 1)
			INSERT INTO #TempTable (ID)
			SELECT v.VehicleID [ID]--, v.vin [VIN], v.Make [make], v.Model [model]
				FROM Vehicles v LEFT JOIN VehicleHandDriveLookUp hd ON v.HandDrive = hd.VHDLID
								LEFT JOIN People p ON v.MainDriver = p.PeopleID
								LEFT JOIN TransmissionTypeLookUp trans ON v.TransmissionType = trans.TransmisionTypeID
				WHERE  Make LIKE @search 
 				   OR Model LIKE @search 
				   OR ModelType LIKE @search 
				   OR BodyType LIKE @search 
				   OR Extension LIKE @search 
				   OR VehicleRegistrationNo LIKE @search
				   OR VIN LIKE @search
				   OR hd.HDType LIKE @search
				   OR Colour LIKE @search
				   OR CAST(Cylinders as varchar(MAX)) LIKE @search
				   OR EngineNo LIKE @search
				   OR EngineModified LIKE @search
				   OR EngineType LIKE @search
				   OR CAST(EstimatedAnnualMileage as varchar(MAX)) LIKE @search
				   OR HPCCUnitType LIKE @search
				   OR p.FName LIKE @search
				   OR p.MName LIKE @search
				   OR p.LName LIKE @search
				   OR p.TRN LIKE @search
				   OR CAST(ReferenceNo as varchar(MAX)) LIKE @search
				   OR RegistrationLocation LIKE @search
				   OR RoofType LIKE @search
				   OR CAST(Seating as varchar(MAX)) LIKE @search
				   OR CAST(Tonnage as varchar(MAX)) LIKE @search
				   OR trans.TransmissionType LIKE @search
				   OR CAST(VehicleYear as varchar(MAX)) LIKE @search
				   OR ChassisNo LIKE @search;
		ELSE
		BEGIN
			INSERT INTO #TempTable2 (ID) SELECT ID FROM #TempTable;
			DELETE #TempTable;
			INSERT INTO #TempTable (ID)
			SELECT v.VehicleID [ID]--, v.vin [VIN], v.Make [make], v.Model [model]
				FROM Vehicles v LEFT JOIN VehicleHandDriveLookUp hd ON v.HandDrive = hd.VHDLID
								LEFT JOIN People p ON v.MainDriver = p.PeopleID
								LEFT JOIN TransmissionTypeLookUp trans ON v.TransmissionType = trans.TransmisionTypeID
				WHERE  (Make LIKE @search 
 				   OR Model LIKE @search 
				   OR ModelType LIKE @search 
				   OR BodyType LIKE @search 
				   OR Extension LIKE @search 
				   OR VehicleRegistrationNo LIKE @search
				   OR VIN LIKE @search
				   OR hd.HDType LIKE @search
				   OR Colour LIKE @search
				   OR CAST(Cylinders as varchar(MAX)) LIKE @search
				   OR EngineNo LIKE @search
				   OR EngineModified LIKE @search
				   OR EngineType LIKE @search
				   OR CAST(EstimatedAnnualMileage as varchar(MAX)) LIKE @search
				   OR HPCCUnitType LIKE @search
				   OR p.FName LIKE @search
				   OR p.MName LIKE @search
				   OR p.LName LIKE @search
				   OR p.TRN LIKE @search
				   OR CAST(ReferenceNo as varchar(MAX)) LIKE @search
				   OR RegistrationLocation LIKE @search
				   OR RoofType LIKE @search
				   OR CAST(Seating as varchar(MAX)) LIKE @search
				   OR CAST(Tonnage as varchar(MAX)) LIKE @search
				   OR trans.TransmissionType LIKE @search
				   OR CAST(VehicleYear as varchar(MAX)) LIKE @search)
				   OR ChassisNo LIKE @search
				   AND v.VehicleID IN (SELECT ID FROM #TempTable2);
		END

		IF(LEN(@term) = 0) break;
		
		SET @search = SUBSTRING(@term, 1, (CASE WHEN (CHARINDEX('&', @term) > 0) THEN (CHARINDEX('&', @term) - 1) ELSE LEN(@term) END));
		SET @term = SUBSTRING(@term, 2 + LEN(@search), LEN(@term)); 
		SET @search = '%' + @search + '%';

	END

	SELECT v.VehicleID [ID], v.vin [VIN], v.Make [make], v.Model [model]
	FROM Vehicles v LEFT JOIN VehicleHandDriveLookUp hd ON v.HandDrive = hd.VHDLID
					LEFT JOIN People p ON v.MainDriver = p.PeopleID
					LEFT JOIN TransmissionTypeLookUp trans ON v.TransmissionType = trans.TransmisionTypeID
	WHERE  v.VehicleID IN (SELECT ID FROM #TempTable);

	DROP TABLE #TempTable; DROP TABLE #TempTable2;
	END


GO
/****** Object:  StoredProcedure [dbo].[VehiclesMyCompanyCovered]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[VehiclesMyCompanyCovered]
	-- Add the parameters for the stored procedure here
	@cid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT v.VehicleID [ID], v.ChassisNo [chassis], v.Make [make], v.Model [model], v.VehicleRegistrationNo [regno], v.VIN [vin]
	FROM
	Policies p LEFT JOIN VehiclesUnderPolicy vup ON p.PolicyID = vup.PolicyID
			   LEFT JOIN Vehicles v ON vup.VehicleID = v.VehicleID
			   LEFT OUTER JOIN Company insurer ON p.CompanyID = insurer.CompanyID AND p.CompanyID IS NOT NULL
			   LEFT OUTER JOIN Company broke ON p.CompanyCreatedBy = broke.CompanyID AND p.CompanyCreatedBy IS NOT NULL
	WHERE (p.CompanyID = @cid OR p.CompanyCreatedBy = @cid) AND v.VehicleID IS NOT NULL;
END


GO
/****** Object:  StoredProcedure [dbo].[VehiclesUnderAPolicy]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Chrystal Parchment
-- Create date: 20/08/2014
-- Description:	Retrieving all the vehicles under a policy (active or not)
-- =============================================
CREATE PROCEDURE [dbo].[VehiclesUnderAPolicy]
	@polId int
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT VehicleID FROM VehiclesUnderPolicy WHERE PolicyID = @polId;
    
END


GO
/****** Object:  StoredProcedure [dbo].[WasEnquiryValid]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WasEnquiryValid]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	DECLARE @chassis varchar(MAX), @license varchar(MAX), @valid bit, @covered bit;
	
	SELECT @chassis = (CASE WHEN (chassis IS NULL) THEN NULL ELSE chassis END), @license = license FROM Enquiries WHERE ID = @id;

	SELECT @valid = (CASE WHEN (Count(*) > 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END)
	FROM Vehicles WHERE (ChassisNo = @chassis OR @chassis IS NULL) AND (VehicleRegistrationNo = @license OR @license IS NULL) OR (@chassis IS NOT NULL AND @license IS NOT NULL);

	CREATE TABLE #Temp (cov bit);
	INSERT #Temp (cov)
	EXEC IsVehicleCovered @chassis, @license;
	SELECT TOP 1 @covered = cov FROM #Temp;
	--SELECT @chassis, @license;
	--SELECT * FROM #Temp;
	DROP TABLE #Temp;

	--SELECT @covered;

	DECLARE @i int;
	IF (@covered = 1)
	BEGIN
		SELECT TOP 1 @i = vup.PolicyID 
		FROM VehiclesUnderPolicy vup LEFT JOIN Vehicles v ON vup.VehicleID = v.VehicleID
										LEFT JOIN Policies p ON vup.PolicyID = p.PolicyID
		WHERE	vup.Cancelled = 0 
			AND p.Cancelled = 0
			AND p.StartDateTime <= CURRENT_TIMESTAMP 
			AND p.EndDateTime >= CURRENT_TIMESTAMP
			AND (v.ChassisNo = @chassis OR @chassis IS NULL)
			AND (v.VehicleRegistrationNo = @license OR @license IS NULL)
			AND (@chassis IS NOT NULL OR @license IS NOT NULL)
		ORDER BY vup.VUPID
	END

	--SELECT @i;

	UPDATE Enquiries 
	SET	valid = @valid,
		risk = (CASE
					WHEN (@valid = 1) THEN (SELECT VehicleID FROM Vehicles WHERE (ChassisNo = @chassis OR @chassis IS NULL) AND (VehicleRegistrationNo = @license OR @license IS NULL) OR (@chassis IS NOT NULL AND @license IS NOT NULL))
					ELSE NULL
				END),
		policy = (CASE 
					WHEN (@covered = 1) THEN @i
						
						--(SELECT TOP 1 vup.PolicyID 
						-- FROM VehiclesUnderPolicy vup LEFT JOIN Vehicles v ON vup.VehicleID = v.VehicleID
						--							  LEFT JOIN Policies p ON vup.PolicyID = p.PolicyID
						-- WHERE	vup.Cancelled = 0 
						--	AND p.Cancelled = 0
						--	AND p.StartDateTime <= CURRENT_TIMESTAMP 
						--	AND p.EndDateTime >= CURRENT_TIMESTAMP
						--	AND (v.ChassisNo = @chassis OR @chassis IS NULL)
						--	AND (v.VehicleRegistrationNo = @license OR @license IS NULL)
						--	AND (@chassis IS NOT NULL OR @license IS NOT NULL)
						-- ORDER BY vup.VUPID
						--)
					ELSE NULL
				END)
	WHERE ID = @id;

	IF (@valid = 1 AND @covered = 1)
	BEGIN
		INSERT INTO Enquiries (license, chassis, RequestedOn, risk, policy, [valid])
		SELECT e.license, e.chassis, e.RequestedOn, e.risk, vup.PolicyID, e.[valid]
		FROM Enquiries e LEFT JOIN VehiclesUnderPolicy vup ON e.risk = vup.VehicleID
						 LEFT JOIN Vehicles v ON vup.VehicleID = v.VehicleID
						 LEFT JOIN Policies p ON vup.PolicyID = p.PolicyID
		WHERE	e.ID = @id 
			AND vup.Cancelled = 0 
			AND p.Cancelled = 0
			AND p.StartDateTime <= CURRENT_TIMESTAMP 
			AND p.EndDateTime >= CURRENT_TIMESTAMP
			AND vup.PolicyID <> @i
			AND (v.ChassisNo = @chassis OR @chassis IS NULL)
			AND (v.VehicleRegistrationNo = @license OR @license IS NULL)
			AND (@chassis IS NOT NULL OR @license IS NOT NULL)
		ORDER BY vup.VUPID;
	END

	--SELECT @valid [valid];


END

GO
/****** Object:  StoredProcedure [dbo].[WasPolicyImported]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WasPolicyImported]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT imported [result] FROM Policies WHERE PolicyID = @id;
END

GO
/****** Object:  StoredProcedure [dbo].[WordingExist]    Script Date: 12/05/2015 08:53:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WordingExist]
	-- Add the parameters for the stored procedure here
	@ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (CASE WHEN COUNT(*) > 0 THEN CAST(1 AS bit) ELSE CAST(0 as bit) END) as result
	FROM Wording WHERE ID = @ID;
END


GO
--USE [master]

--ALTER DATABASE [IVIS] MODIFY FILE ( NAME = N'IVIS_log', MAXSIZE = UNLIMITED)
--GO