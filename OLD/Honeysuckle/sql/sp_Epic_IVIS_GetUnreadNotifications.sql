USE [IVIS]
GO

/****** Object:  StoredProcedure [dbo].[Epic_IVIS_GetUnreadNotifications]    Script Date: 12/9/2018 9:09:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Epic_IVIS_GetUnreadNotifications]
	-- Add the parameters for the stored procedure here
	@uid int,
	@page int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Insert statements for procedure here
	IF(@page = 0)
	BEGIN
		SELECT top 505 
		N.NID, 
		N.NoteFrom,
		N.Heading, 
		N.Content,
		N.ParentMessage, 
		N.CreatedOn as [timestamp],
		u.username + ';' as NoteTo
		FROM Notifications N 
		INNER JOIN NotificationsTo nt ON N.NID = nt.Notification
		INNER JOIN users u ON nt.UID = u.UID
		WHERE (u.UID = @uid) and N.Deleted = 0 and N.[Read] = 0 ORDER BY N.NID DESC;
	END
	ELSE
	BEGIN
		DECLARE @Temp TABLE (
			ID int IDENTITY,
			NID int,
			NoteFrom int,
			Heading varchar(MAX),
			Content varchar(MAX),
			ParentMessage int,
			[timestamp] datetime,
			NoteTo varchar(MAX)
		);
		INSERT INTO @Temp(NID,NoteFrom, Heading, Content, ParentMessage, [timestamp], NoteTo)
			SELECT top (@page * 10)
			N.NID, 
			N.NoteFrom,
			N.Heading, 
			N.Content,
			N.ParentMessage, 
			N.CreatedOn as [timestamp],
			u.username + ';' as NoteTo
			FROM Notifications N 
			INNER JOIN NotificationsTo nt ON N.NID = nt.Notification
			INNER JOIN users u ON nt.UID = u.UID
			WHERE (u.UID = @uid) and N.Deleted = 0 and N.[Read] = 0 ORDER BY N.NID DESC;
		
		SELECT * FROM @Temp WHERE ID >= (@page * 10) - 9 AND ID <= @page *10;

	END
END
GO

