USE [IVIS]
GO

/****** Object:  StoredProcedure [dbo].[Epic_IVIS_GetUnreadNotificationsCount]    Script Date: 12/9/2018 9:09:44 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Epic_IVIS_GetUnreadNotificationsCount]
	-- Add the parameters for the stored procedure here
	@uid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT count(NTID) [count] 
	FROM Notifications N
	LEFT JOIN NotificationsTo NT ON N.NID = NT.Notification
	WHERE [UID] = @uid and N.[Read] = 0;

END
GO

