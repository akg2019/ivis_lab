USE IVIS_test; --name of database

EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all" -- disable all constraints


--create administrator and system accounts; with preset passwords, can be changed later
INSERT INTO Users(username, [password], EmployeeID, [reset], question, response, faultylogins, password_reset_force, active, firstlogin, CreatedOn, CreatedBy, FirstLoginDateTime, LastModifiedBy, LoggedIn, Filter, administrator)
VALUES ('administrator', '$2a$10$IWR1FBJZb9bmFHq7GZQuMO7OcHqXCXZ3DWka3J3LNkwNEJ/uVHCRi', 1, 0, 'what up???', '$2a$10$IWR1FBJZb9bmFHq7GZQuMOVH19VA6ChTlz8wMTRyDfRTgE20EBHJa', 0, 0, 1, 0, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 0, 0, 0, 1), -- password = P@$$w0rd
	   ('system', 'test', 2, 0, null, null, 0, 0, 0, 1, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1, 0, 1, 0);
	   
--set last modified and created by flags in db with users just created
UPDATE Users SET LastModifiedBy = @@IDENTITY, CreatedBy = @@IDENTITY, IAJcreated = 0;

--set static var for use later for last modified and created by flags in all data about to be created
DECLARE @uid int; SELECT @uid = [UID] FROM Users WHERE username = 'administrator';

-- kpi static data updates
INSERT INTO KPIDateMode (datemode) VALUES ('year'),('month'),('day');
INSERT INTO KPImode (mode) VALUES ('red'),('yellow'),('green');
INSERT INTO KPItype ([type]) VALUES ('enquiry'), ('covernote'), ('certificate'), ('error'), ('doubleInsured');


--set parishes into system
INSERT INTO Parishes (Parish, CreatedBy, LastModifiedBy)
VALUES ('Hanover', @uid, @uid),
	   ('Saint Elizabeth', @uid, @uid),
	   ('Saint James',@uid, @uid),
	   ('Trelawny', @uid, @uid),
	   ('Westmoreland', @uid, @uid),
	   ('Clarendon', @uid, @uid),
	   ('Manchester', @uid, @uid),
	   ('Saint Ann', @uid, @uid),
	   ('Saint Catherine', @uid, @uid),
	   ('Saint Mary', @uid, @uid),
	   ('Kingston', @uid, @uid),
	   ('Portland',@uid, @uid),
	   ('Saint Andrew', @uid, @uid),
	   ('Saint Thomas', @uid, @uid);

	   
--create company types
INSERT INTO CompanyTypes (ID, [Type]) VALUES (1, 'Insurer'),(2, 'Broker'),(3, 'Agent'), (4, 'Association');

--create company IAJ (to be used as host of system and admin accounts)
INSERT INTO Company(CompanyName, CreatedBy, CreatedOn, LastModifiedBy, TRN, CoverNotePrintLimit, [Enabled], InsurerCode, CompShortening, faultyloginlimit, [Type], iaj)
VALUES ('IAJ', @uid, CURRENT_TIMESTAMP, @uid, '0234567891234',0,1,'IAJ','IAJ', 0, 4, 1);

DECLARE @cid int;
SET @cid = @@IDENTITY;

EXEC InitializeKPIs @cid, 'enquiry';
EXEC InitializeKPIs @cid, 'covernote';
EXEC InitializeKPIs @cid, 'certificate';
EXEC InitializeKPIs @cid, 'error';
EXEC InitializeKPIs @cid, 'doubleInsured';

--insert temp people records for those accounts
INSERT INTO People (FName, MName, LName, CreatedBy, CreatedOn, LastModifiedBy, TRN)
VALUES ('Adm','In','Istrator',@uid,CURRENT_TIMESTAMP, @uid, '1'),
	   ('sys','tem','dude', @uid, CURRENT_TIMESTAMP, @uid, '2');

--assign and address to people created	   
DECLARE @pid int;
SELECT @pid = PID FROM Parishes WHERE Parish = 'Kingston';

EXEC AddAddressToSystem '','','','',@pid,'','','',null,@uid;
DECLARE @aid int; SELECT TOP 1 @aid = AID FROM [Address];

INSERT INTO PeopleAddress (PersonID, AddressID, CurrentAddress, CreatedOn, CreatedBy)
SELECT PeopleID, @aid, 1, CURRENT_TIMESTAMP, @uid FROM People;

INSERT INTO CompanyAddress (CompanyID, AddressID, CurrentAddress, CreatedOn, CreatedBy)
SELECT CompanyID, @aid, 1, CURRENT_TIMESTAMP, @uid FROM Company;

--insert employee records for accounts in IAJ	   
INSERT INTO Employees(PersonID, CompanyID, [Active])
SELECT p.PeopleID, c.CompanyID, 1 FROM People p, Company c;

--create permissions
INSERT INTO UserPermissions ([Table], [Action])
VALUES ('Users','Create'),('Users','Read'),('Users','Update'),('Users','Delete'),('Users','Activate'), ('Users','EditProfile'), ('Users', 'ResetPassword'), ('Users', 'ResetQuestion'),('Users', 'Upload'),
	   ('Company','Create'),('Company','Read'),('Company','Update'),('Company','Delete'),('Company','Enable'),('Company','EditInfo'),
	   ('UserGroups','Create'),('UserGroups','Read'),('UserGroups','Update'),('UserGroups','Delete'),('UserGroups','Upload'),
	   ('Vehicles','Create'),('Vehicles','Read'),('Vehicles','Update'),
	   ('Policies','Create'),('Policies','Read'),('Policies','Update'),('Policies','Cancel'),('Policies','Activate'),
	   ('People','Create'),('People','Read'),
	   ('Certificates','Create'),('Certificates','Read'),('Certificates','Update'),('Certificates','Approve'),('Certificates','Print'),
	   ('CoverNotes','Create'),('CoverNotes','Read'),('CoverNotes','Update'),('CoverNotes','Approve'),('CoverNotes','Print'),
	   ('Address','Create'),
	   ('Import','Import'),('Search','Search'),('Import', 'CoverSearch'),
	   ('Wording','Create'),('Wording','Read'),('Wording','Update'),('Wording','Delete'),
	   ('PolicyCovers','Create'),('PolicyCovers','Read'),('PolicyCovers','Update'),('PolicyCovers','Delete'),
	   ('Usages','Create'),('Usages','Read'),('Usages','Delete'),
	   ('Mapping','Read'),('Mapping','Update'),('Mapping','Delete'),('Mapping','Lock'),
	   ('Intermediaries','Create'),('Intermediaries','Read'),('Intermediaries','Update'),('Intermediaries','Delete'),('Intermediaries','Enable'),
	   ('PolicyHolder','Create'),('PolicyHolder','Read'),('PolicyHolder','Update'),
	   ('BI','Read'),('BI','Cover Chart'),('BI','Inquiries Gauge'),('BI','Error Gauge'),('BI','Cover Chart'),('BI','Cover Note Gauge'),('BI','Certificate Gauge'),('BI','Cover Chart'),('BI','Double Insured Gauge'),('BI','Enquiry Chart'),
	   ('Authorized','Create'),('Authorized','Read'),('Authorized','Delete');
	   
--set system permissions
UPDATE UserPermissions SET [system] = 1 
WHERE [Table] IN ('People') 
  OR ([Table] = 'Users' AND [Action] = 'Delete') 
  OR ([Table] = 'Company' AND [Action] IN ('Create','Enable','Delete')); 

--create System Admin Group	   
INSERT INTO UserGroups (UserGroup, SuperGroup, CreatedAt, CreatedBy, LastModifiedBy, [system], stock, administrative)
VALUES ('System Administrator',0, CURRENT_TIMESTAMP, @uid, @uid, 1, 0, 1);

--give 'administrator' account System Administrator Group
INSERT INTO UserGroupUserAssoc ([UGID], [UID], CreatedBy, CreatedOn, LastModifiedBy)
SELECT UGID, @uid, @uid, CURRENT_TIMESTAMP, @uid FROM UserGroups;

--create IAJ Administrator Group
INSERT INTO UserGroups (UserGroup, SuperGroup, CreatedAt, CreatedBy, LastModifiedBy, [system], [stock], administrative)
VALUES ('IAJ Administrator',0, CURRENT_TIMESTAMP, @uid, @uid, 1, 0, 1);

--create stock Company Administrator group
INSERT INTO UserGroups (UserGroup, SuperGroup, CreatedAt, CreatedBy, LastModifiedBy, [system], [stock], Company, administrative)
SELECT 'Company Administrator',0,CURRENT_TIMESTAMP, @uid, @uid, 0, 1, CompanyID, 1 FROM Company;

-- set initial permissions for IAJ Administrator Group
INSERT INTO PermissionGroupAssoc (UPID, UGID)
SELECT up.UPID, ug.UGID
FROM UserPermissions up, UserGroups ug 
WHERE ug.UserGroup = 'IAJ Administrator'
AND (
		(up.[Table] = 'Users' AND up.[Action] IN ('Create','Read','Update','EditProfile','ResetPassword'))
	OR	(up.[Table] = 'Company' AND up.[Action] IN ('Create','Read','Update','Delete','Enable','EditInfo'))
	OR	(up.[Table] = 'Address' AND up.[Action] IN ('Create','Read','Update','Delete'))
	OR	(up.[Table] = 'Employees' AND up.[Action] = 'Read')
	OR	(up.[Table] = 'People' AND up.[Action] IN ('Create','Read','Update','Delete'))
	OR	(up.[Table] = 'Search' AND up.[Action] = 'Search')
	OR	(up.[Table] = 'Certificates' AND up.[Action] = 'Delete')
	OR  (up.[Table] = 'Authorized')
	);
	
--set initial permissions for Company Administrator Group
INSERT INTO PermissionGroupAssoc (UPID, UGID)
SELECT up.UPID, ug.UGID
FROM UserPermissions up, UserGroups ug 
WHERE ug.UserGroup = 'Company Administrator'
AND (
		(up.[Table] = 'Users' AND up.[Action] IN ('Create','Read','Update','Activate','ResetPassword','ResetQuestion','EditProfile'))
	OR	(up.[Table] = 'Company' AND up.[Action] IN ('Create','Read','Update','EditInfo','Intermediaries','Settings'))
	OR	(up.[Table] = 'Address' AND up.[Action] IN ('Create','Read','Update'))
	OR	(up.[Table] = 'Search' AND up.[Action] = 'Search')
	OR	(up.[Table] = 'UserGroups' AND up.[Action] IN ('Create','Read','Update','Delete'))
	OR	(up.[Table] = 'Wording' AND up.[Action] IN ('Create','Read','Update','Delete'))
	OR	(up.[Table] = 'PolicyCovers' AND up.[Action] IN ('Create','Read','Update','Delete'))
	OR	(up.[Table] = 'Usages' AND up.[Action] IN ('Create','Read','Update','Delete'))
	OR	(up.[Table] = 'UserPermissions' AND up.[Action] IN ('Create','Read','Update','Delete'))
	OR	(up.[Table] = 'Mapping' AND up.[Action] IN ('Create','Read','Update','Delete','Lock'))
	OR	(up.[Table] = 'PolicyHolder' AND up.[Action] IN ('Create','Read','Update','Delete'))
	OR	(up.[Table] = 'Intermediaries' AND up.[Action] IN ('Create','Read','Update','Delete','Enable'))
	OR  (up.[Table] = 'Policies' AND up.[Action] IN ('Read','Create'))
	OR  (up.[Table] = 'BI')
	);
	
--attribute all current groups to IAJ
UPDATE UserGroups SET Company = (SELECT CompanyID FROM Company);

INSERT INTO PolicyInsuredType (InsuredType) VALUES ('Company'),('Individual'),('Individual/Company'),('Joint Insured'),('Blanket');


exec sp_msforeachtable @command1="print '?'", @command2="ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all" --enable all constraints