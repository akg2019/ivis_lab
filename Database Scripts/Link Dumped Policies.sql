CREATE TABLE [dbo].[PolicyTRNDump](
	[Company_ID] [int] NOT NULL,
	[National_ID] [nvarchar](50) NULL,
	[Policy_No] [nvarchar](50) NOT NULL,
	[Source_Account_Code] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO

create procedure linkToSource @fileName nvarchar(50)
AS 

declare @folder nvarchar(50)
declare @location nvarchar(50)
declare @sql nvarchar(100)

select @folder = 'c:/PolicyDumps/'

select @location = @folder + @fileName + '.txt'

--,FORMATFILE=''c:/PolicyDumps/'+@fileName+''

set @sql = N'BULK INSERT PolicyTRNDump FROM ''' + @location +  '''' --''' WITH (ROWTERMINATOR = ''0x0a'')'

exec(@sql)

--Tries to match TRNs that contain non-numerical characters
update Policies set CompanyCreatedBy = c.companyId from ( select * from Company where trn like '%[^0-9]%') c join PolicyTRNDump src on c.TRN = src.National_ID join Policies p on p.Policyno = src.Policy_No where c.Type = 2

--Tries to match TRNs that contain numerical characters
update Policies set CompanyCreatedBy = c.companyId from ( select * from Company where trn not like '%[^0-9]%') c join PolicyTRNDump src on Cast(c.TRN as bigint) = Cast(src.National_ID as bigint) join Policies p on p.Policyno = src.Policy_No where c.Type = 2

