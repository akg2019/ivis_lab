USE [MARCH21]
GO

/****** Object:  StoredProcedure [dbo].[GeneralSearchNoLimitReconStruct]    Script Date: 14/4/2019 4:12:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[GeneralSearchNoLimitReconStruct]
	-- Add the parameters for the stored procedure here
	@search varchar(500),
	@uid int,
	@Chassis varchar(500),
	@Reg varchar(500), 
	@Company varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--Get Vehicle Information

	DECLARE @_VEHICLE bit = 0; IF(@Chassis <> '""' OR @Reg  <> '""')BEGIN SET @_VEHICLE = 1 END
	DECLARE @_COMPANY bit = 0; IF(@Company  <> '""')BEGIN SET @_COMPANY = 1 END
	SET @_VEHICLE = 1
	SET @_COMPANY = 0

	DECLARE @_COMPANYID int;
	
	DECLARE @cid int;
	SELECT @cid = c.CompanyID, @Company = CompanyName FROM Company c RIGHT JOIN Employees e ON c.CompanyID = e.CompanyID
											 RIGHT JOIN Users u ON e.EmployeeID = u.EmployeeID
						      WHERE u.UID = @uid;

--TABLE VARIABLE

 DECLARE @SearchResults TABLE
		(
		      [ID] int, 
			  [table] varchar(500),
			  [CreatedBy] int
			, [company_enable] bit,[company_iajid] varchar(500), [company_name] varchar(500), [company_type] varchar(500), [company_trn] varchar(14) --company
			, [user_name] varchar(55),[user_company_name] varchar(500), [people_fname]varchar(500), [people_lname]varchar(500), [people_trn]varchar(500), [user_enabled] bit  --people
			, [vehicle_reg_no]varchar(500), [vehicle_chassis_no]varchar(500), [vehicle_make]varchar(500), [vehicle_model]varchar(500), [vehicle_under_pol] bit, [vehicle_has_active_cover_note] bit, [vehicle_has_cover_note_history] bit --vehicle
			, [policy_no] varchar(500), [policy_start] datetime, [policy_end] datetime, [policy_insured_type] varchar(500), [policy_cover_prefix] varchar(500) --policy
			, [certificate_no] varchar(500), [certificate_risk_id] int, [certificate_policy_id] int, [certificate_type] varchar(500), [risk_reg_no] varchar(500), [certificate_eff] datetime, [certificate_company_name] varchar(500) --certificate
			, [covernote_no] varchar(500), [covernote_eff] datetime, [covernote_expiry] datetime, [covernote_policy_id] int, [covernote_risk_id] int, [covernote_company_name] varchar(500), [imported] bit --covernote
			, [person_id] int, [peo_fname] varchar(500), [peo_mname] varchar(500), [peo_lname] varchar(500), [peo_trn] varchar(500) --people
		)


--ELIMINATE BOGUS SEARCHES
DECLARE @VCOUNT int;
IF(@Chassis <> '""')BEGIN 
select @VCOUNT = count(v.VehicleID) from Vehicles v where v.ChassisNo = @Chassis
and v.CreatedBy in 
(
SELECT Users.UID
FROM   Users INNER JOIN
Employees ON Users.EmployeeID = Employees.EmployeeID INNER JOIN
Company ON Employees.CompanyID = Company.CompanyID
WHERE (Company.CompanyID = @cid)
)
IF(@VCOUNT IS NULL OR @VCOUNT = 0)BEGIN RETURN SELECT * FROM @SearchResults i ORDER BY i.[table]  END

END

IF(@Reg  <> '""')
BEGIN 
select @VCOUNT = count(v.ID) from VehicleMetaData v where v.VehicleRegistrationNo = @Reg 
and  
(
--SELECT Users.UID
--FROM   Users INNER JOIN
--Employees ON Users.EmployeeID = Employees.EmployeeID INNER JOIN
--Company ON Employees.CompanyID = Company.CompanyID
--WHERE (Company.CompanyID = @cid)
						v.CreatedBy IN (SELECT Users.UID FROM Employees LEFT JOIN Users ON Users.EmployeeID = Employees.EmployeeID WHERE Employees.CompanyID = @cId)
						OR v.CreatedBy IN(
										SELECT Users.UID FROM Employees LEFT JOIN Users ON Users.EmployeeID = Employees.EmployeeID
										 WHERE Employees.CompanyID IN (SELECT Intermediary FROM InsurerToIntermediary iti WHERE
																		iti.Insurer = @cId))

)

IF(@VCOUNT IS NULL OR @VCOUNT = 0)BEGIN RETURN SELECT * FROM @SearchResults i ORDER BY i.[table]  END

END

SELECT @VCOUNT = COUNT(v.VehicleID)
FROM Vehicles v LEFT JOIN VehicleMetaData vmd ON v.VehicleID = vmd.VehicleID AND vmd.[current] = 1
WHERE (vmd.VehicleRegistrationNo = @Reg OR ChassisNo = @Chassis)			
IF(@VCOUNT IS NULL OR @VCOUNT = 0)BEGIN RETURN SELECT * FROM @SearchResults i ORDER BY i.[table]  END		

		
		      
	
 DECLARE @CreatedByTable Table (
	EmployeeID int
 )	
 
 INSERT INTO @CreatedByTable
 SELECT u.UID FROM Users u left join Employees e on u.EmployeeID = e.EmployeeID left join Company c on c.CompanyID = e.CompanyID WHERE c.CompanyID = @cid OR (c.CompanyID IN (SELECT Intermediary FROM InsurerToIntermediary WHERE Insurer = @cid))	



--GET COMPANY NAME IF SEARCH IS VEHICLE SEARCH

 IF(@_VEHICLE = 1 and @cid <> 1) 
 BEGIN 

 PRINT 'THIS IS VEHICLE'

---------------------------------------------------------------------------------------------------
--VEHICLE/RISK DATA

 INSERT INTO @SearchResults (
			  [ID]
             ,[table]
			, [CreatedBy]
			, [company_enable],[company_iajid], [company_name], [company_type], [company_trn]
			, [user_name],[user_company_name], [people_fname], [people_lname], [people_trn], [user_enabled]
			, [vehicle_reg_no], [vehicle_chassis_no], [vehicle_make], [vehicle_model], [vehicle_under_pol], [vehicle_has_active_cover_note], [vehicle_has_cover_note_history]
			, [policy_no], [policy_start], [policy_end], [policy_insured_type], [policy_cover_prefix]
			, [certificate_no], [certificate_risk_id], [certificate_policy_id], [certificate_type], [risk_reg_no], [certificate_eff], [certificate_company_name]
			, [covernote_no], [covernote_eff], [covernote_expiry], [covernote_policy_id], [covernote_risk_id], [covernote_company_name], [imported]
			, [person_id], [peo_fname], [peo_mname], [peo_lname], [peo_trn]
	 )
		SELECT TOP (1) vmd.ID [ID], 
		'vehicle' [table],
			  NULL [CreatedBy]
			, NULL [company_enable], NULL [company_iajid], NULL [company_name], NULL [company_type], NULL [company_trn] -- company
			, NULL [user_name], NULL [user_company_name], NULL [people_fname], NULL [people_lname], NULL [people_trn], NULL [user_enabled] --user
			, vmd.VehicleRegistrationNo [vehicle_reg_no], v.ChassisNo [vehicle_chassis_no], v.Make [vehicle_make], v.Model [vehicle_model] -- vehicle
			, 1 [vehicle_under_pol]
			, 1 [vehicle_has_active_cover_note]
			, 1 [vehicle_has_cover_note_history]
			, NULL [policy_no], NULL [policy_start], NULL [policy_end], NULL [policy_insured_type], NULL [policy_cover_prefix] --policy
			, NULL [certificate_no], NULL [certificate_risk_id], NULL [certificate_policy_id], NULL [certificate_type], NULL [risk_reg_no], NULL [certificate_eff_date], NULL [certificate_company_name] --certificate
			, NULL [covernote_no], NULL [covernote_eff], NULL [covernote_expiry], NULL [covernote_policy_id], NULL [covernote_risk_id], NULL [covernote_company_name], NULL [imported] --covernote
			, NULL [person_id], NULL [peo_fname], NULL [peo_mname], NULL [peo_lname], NULL [peo_trn] --people
		FROM Vehicles v LEFT JOIN VehicleMetaData vmd ON v.VehicleID = vmd.VehicleID AND vmd.[current] = 1
						--LEFT JOIN VehicleHandDriveLookUp hd ON vmd.HandDrive = hd.VHDLID
						--LEFT JOIN Drivers d ON vmd.MainDriver = d.DriverID
						--LEFT JOIN People p ON d.PersonID = p.PeopleID
						--LEFT JOIN TransmissionTypeLookUp trans ON vmd.TransmissionType = trans.TransmisionTypeID
						--LEFT JOIN VehiclesUnderPolicy vup ON vmd.ID = vup.VehicleID
						--LEFT JOIN Policies pol ON vup.PolicyID = pol.PolicyID
						--LEFT JOIN VehicleCertificates vc ON vc.PolicyID = pol.PolicyID
		

		WHERE (
		       vmd.VehicleRegistrationNo = @Reg
			OR ChassisNo = @Chassis
		
			)
			AND V.Make IS NOT NULL
			-- SECURITY CHECK
		   AND (v.IntermediaryId = @cid OR v.CompanyId = @cid)
		  -- AND ((SELECT COUNT(*)
				--FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
				--						RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
				--						LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
				--						LEFT JOIN Users u ON ass.UID = u.UID
				--WHERE ((up.[Table] = 'Vehicles' AND up.[Action] = 'Read')  OR ug.UserGroup IN ('System Administrator')) AND u.UID = @uid) > 0)

---------------------------------------------------------------------------------------------------

--POLICY DATA
	
 INSERT INTO @SearchResults (
			  [ID]
            ,[table]
			, [CreatedBy]
			, [company_enable],[company_iajid], [company_name], [company_type], [company_trn]
			, [user_name],[user_company_name], [people_fname], [people_lname], [people_trn], [user_enabled]
			, [vehicle_reg_no], [vehicle_chassis_no], [vehicle_make], [vehicle_model], [vehicle_under_pol], [vehicle_has_active_cover_note], [vehicle_has_cover_note_history]
			, [policy_no], [policy_start], [policy_end], [policy_insured_type], [policy_cover_prefix]
			, [certificate_no], [certificate_risk_id], [certificate_policy_id], [certificate_type], [risk_reg_no], [certificate_eff], [certificate_company_name]
			, [covernote_no], [covernote_eff], [covernote_expiry], [covernote_policy_id], [covernote_risk_id], [covernote_company_name], [imported]
			, [person_id], [peo_fname], [peo_mname], [peo_lname], [peo_trn]
	 )
	SELECT DISTINCT p.PolicyID [ID], 'policy' [table],
			  NULL [CreatedBy] 
			, NULL [company_enable], NULL [company_iajid], NULL [company_name], NULL [company_type], NULL [company_trn] -- company
			, NULL [user_name], NULL [user_company_name], NULL [people_fname], NULL [people_lname], NULL [people_trn], NULL [user_enabled] --user
			, NULL [vehicle_reg_no], NULL [vehicle_chassis_no], NULL [vehicle_make], NULL [vehicle_model], NULL [vehicle_under_pol], NULL [vehicle_has_active_cover_note], NULL [vehicle_has_cover_note_history] -- vehicle
			, p.Policyno [policy_no], p.StartDateTime [policy_start], p.EndDateTime [policy_end], pit.InsuredType [policy_insured_type], p.Prefix [policy_cover_prefix] --policy
			, NULL [certificate_no], NULL [certificate_risk_id], NULL [certificate_policy_id], NULL [certificate_type], NULL [risk_reg_no], NULL [certificate_eff_date], NULL [certificate_company_name] --certificate
			, NULL [covernote_no], NULL [covernote_eff], NULL [covernote_expiry], NULL [covernote_policy_id], NULL [covernote_risk_id], NULL [covernote_company_name], NULL [imported] --covernote
			, NULL [person_id], NULL [peo_fname], NULL [peo_mname], NULL [peo_lname], NULL [peo_trn] --people
		FROM Policies p LEFT JOIN Company c ON p.CompanyID = c.CompanyID
						LEFT JOIN PolicyInsuredType pit ON p.InsuredType = pit.PITID
						--LEFT JOIN PolicyCovers pc ON p.PolicyCover = pc.ID
						--LEFT JOIN PolicyInsured ins ON p.PolicyID = ins.PolicyID
						--LEFT OUTER JOIN PeopleAddress pa ON ins.PersonID = pa.ID
						--LEFT OUTER JOIN CompanyAddress ca ON ins.CompanyID = ca.ID
						--LEFT JOIN People inspe ON pa.PersonID = inspe.PeopleID
						--LEFT JOIN Company comp ON ca.CompanyID = comp.CompanyID
						LEFT JOIN VehiclesUnderPolicy vup ON p.PolicyID = vup.PolicyID
						LEFT JOIN VehicleMetaData vmd ON vup.VehicleID = vmd.ID
						LEFT JOIN Vehicles v ON vmd.VehicleID = v.VehicleID
						--LEFT JOIN ExceptedDrivers ecd ON v.VehicleID = ecd.VehicleID AND p.PolicyID = ecd.PolicyID
						--LEFT JOIN ExcludedDrivers exd ON v.VehicleID = exd.VehicleID AND p.PolicyID = exd.PolicyID
						--LEFT JOIN AuthorizedDrivers aut ON v.VehicleID = aut.VehicleID AND p.PolicyID = aut.PolicyID
						--LEFT JOIN VehicleHandDriveLookUp hd ON vmd.HandDrive = hd.VHDLID
						--LEFT JOIN Drivers driv ON driv.DriverID = vmd.MainDriver 
						--LEFT JOIN People pe ON vmd.MainDriver = pe.PeopleID
						--LEFT JOIN TransmissionTypeLookUp trans ON vmd.TransmissionType = trans.TransmisionTypeID
						--LEFT OUTER JOIN VehicleCertificates vc ON p.PolicyID = vc.PolicyID
						--LEFT OUTER JOIN VehicleCoverNote vcn ON p.PolicyID = vcn.PolicyID
		

		WHERE (
			   (p.CompanyID = @cid OR p.CompanyCreatedBy =@cid) 
			   AND (vmd.VehicleRegistrationNo = @Reg
			or  v.ChassisNo = @Chassis
			or vmd.VehicleRegistrationNo = @Reg
			or v.ChassisNo = @Chassis)
			--or (p.CreatedBy = @cid and  vmd.VehicleRegistrationNo = @Reg)
			--or (p.CreatedBy = @cid and v.ChassisNo = @Chassis)
			
			) 
			-- SECURITY CHECK
		--   AND (
		--   (SELECT COUNT(*)
		--		FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
		--								RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
		--								LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
		--								LEFT JOIN Users u ON ass.UID = u.UID
		--WHERE (((up.[Table] = 'Policies' AND up.[Action] = 'Read') AND (p.CompanyID = @cid OR p.CompanyCreatedBy = @cid))  OR ug.UserGroup IN ('System Administrator')) AND u.UID = @uid) > 0)
	
---------------------------------------------------------------------------------------------------

--CERTIFICATE DATA

 INSERT INTO @SearchResults (
			  [ID]
            ,[table]
			, [CreatedBy]
			, [company_enable],[company_iajid], [company_name], [company_type], [company_trn]
			, [user_name],[user_company_name], [people_fname], [people_lname], [people_trn], [user_enabled]
			, [vehicle_reg_no], [vehicle_chassis_no], [vehicle_make], [vehicle_model], [vehicle_under_pol], [vehicle_has_active_cover_note], [vehicle_has_cover_note_history]
			, [policy_no], [policy_start], [policy_end], [policy_insured_type], [policy_cover_prefix]
			, [certificate_no], [certificate_risk_id], [certificate_policy_id], [certificate_type], [risk_reg_no], [certificate_eff], [certificate_company_name]
			, [covernote_no], [covernote_eff], [covernote_expiry], [covernote_policy_id], [covernote_risk_id], [covernote_company_name], [imported]
			, [person_id], [peo_fname], [peo_mname], [peo_lname], [peo_trn]
	 )
		SELECT vc.VehicleCertificateID [ID], 'certificate' [table],
		      NULL [CreatedBy]
			, NULL [company_enable], NULL [company_iajid], NULL [company_name], NULL [company_type], NULL [company_trn] -- company
			, NULL [user_name], NULL [user_company_name], NULL [people_fname], NULL [people_lname], NULL [people_trn], NULL [user_enabled] --user
			, NULL [vehicle_reg_no], NULL [vehicle_chassis_no], NULL [vehicle_make], NULL [vehicle_model], NULL [vehicle_under_pol], NULL [vehicle_has_active_cover_note], NULL [vehicle_has_cover_note_history] -- vehicle
			, NULL [policy_no], NULL [policy_start], NULL [policy_end], NULL [policy_insured_type], NULL [policy_cover_prefix] --policy
			, vc.CertificateNo [certificate_no], vmd.ID [certificate_risk_id], p.PolicyID [certificate_policy_id], vc.CertificateType [certificate_type], vmd.VehicleRegistrationNo [risk_reg_no], vc.EffectiveDate [certificate_eff_date], c_ins.CompanyName [certificate_company_name]
			, NULL [covernote_no], NULL [covernote_eff], NULL [covernote_expiry], NULL [covernote_policy_id], NULL [covernote_risk_id], NULL [covernote_company_name], NULL [imported]
			, NULL [person_id], NULL [peo_fname], NULL [peo_mname], NULL [peo_lname], NULL [peo_trn] --people
		FROM VehicleCertificates vc LEFT JOIN Company c ON vc.CompanyID = c.CompanyID
									LEFT JOIN VehicleMetaData vmd ON vc.RiskItemNo = vmd.ID
									LEFT JOIN Vehicles v ON vmd.VehicleID = v.VehicleID
									LEFT JOIN Policies p ON vc.PolicyID = p.PolicyID
									LEFT JOIN Company c_ins ON p.CompanyID = c_ins.CompanyID
		

		WHERE 
		    (c.CompanyID = @cid OR c.IntermediaryId = @cid) and
			( vmd.VehicleRegistrationNo = @Reg
		    OR  v.ChassisNo = @Chassis
			or vmd.VehicleRegistrationNo = @Reg
			or v.ChassisNo = @Chassis)
			
---------------------------------------------------------------------------------------------------			

--COVER NOTE DATA
 INSERT INTO @SearchResults (
			 [ID]
            ,[table]
			, [CreatedBy]
			, [company_enable],[company_iajid], [company_name], [company_type], [company_trn]
			, [user_name],[user_company_name], [people_fname], [people_lname], [people_trn], [user_enabled]
			, [vehicle_reg_no], [vehicle_chassis_no], [vehicle_make], [vehicle_model], [vehicle_under_pol], [vehicle_has_active_cover_note], [vehicle_has_cover_note_history]
			, [policy_no], [policy_start], [policy_end], [policy_insured_type], [policy_cover_prefix]
			, [certificate_no], [certificate_risk_id], [certificate_policy_id], [certificate_type], [risk_reg_no], [certificate_eff], [certificate_company_name]
			, [covernote_no], [covernote_eff], [covernote_expiry], [covernote_policy_id], [covernote_risk_id], [covernote_company_name], [imported]
			, [person_id], [peo_fname], [peo_mname], [peo_lname], [peo_trn]
	 )
	 -- SEARCH INTO COVER NOTES
		SELECT vcn.VehicleCoverNoteID [ID], 'covernote' [table],
			  NULL [CreatedBy]
			, NULL [company_enable], NULL [company_iajid], NULL [company_name], NULL [company_type], NULL [company_trn] -- company
			, NULL [user_name], NULL [user_company_name], NULL [people_fname], NULL [people_lname], NULL [people_trn], NULL [user_enabled] --user
			, NULL [vehicle_reg_no], NULL [vehicle_chassis_no], NULL [vehicle_make], NULL [vehicle_model], NULL [vehicle_under_pol], NULL [vehicle_has_active_cover_note], NULL [vehicle_has_cover_note_history] -- vehicle
			, NULL [policy_no], NULL [policy_start], NULL [policy_end], NULL [policy_insured_type], NULL [policy_cover_prefix] --policy
			, NULL [certificate_no], NULL [certificate_risk_id], NULL [certificate_policy_id], NULL [certificate_type], NULL [risk_reg_no], NULL [certificate_eff_date], NULL [certificate_company_name] --certificate
			, vcn.CoverNoteID [covernote_no], vcn.EffectiveDate [covernote_eff], vcn.ExpiryDateTime [covernote_expiry], p.PolicyID [covernote_policy_id], vmd.ID [covernote_risk_id], c_ins.CompanyName [covernote_company_name], p.imported [imported] --covernote
			, NULL [person_id], NULL [peo_fname], NULL [peo_mname], NULL [peo_lname], NULL [peo_trn] --people
		FROM VehicleCoverNote vcn LEFT JOIN Company c ON vcn.CompanyID = c.CompanyID
								  LEFT JOIN VehicleMetaData vmd ON vcn.RiskItemNo = vmd.ID
								  LEFT JOIN Vehicles v  ON vmd.VehicleID = v.VehicleID
								  LEFT JOIN Policies p ON vcn.PolicyID = p.PolicyID
								  LEFT JOIN Company c_ins ON p.CompanyID = c_ins.CompanyID
		

		WHERE (c.CompanyID = @cid or c.IntermediaryId = @cid) and 
		(vmd.VehicleRegistrationNo = @Reg
		 or v.VIN = @Chassis
		or vmd.VehicleRegistrationNo = @Reg
			or v.ChassisNo = @Chassis)
		

---------------------------------------------------------------------------------------------------

--INSURED DATA
 --INSERT INTO @SearchResults (
	--		  [ID]
 --           ,[table]
	--		, [CreatedBy]
	--		, [company_enable],[company_iajid], [company_name], [company_type], [company_trn]
	--		, [user_name],[user_company_name], [people_fname], [people_lname], [people_trn], [user_enabled]
	--		, [vehicle_reg_no], [vehicle_chassis_no], [vehicle_make], [vehicle_model], [vehicle_under_pol], [vehicle_has_active_cover_note], [vehicle_has_cover_note_history]
	--		, [policy_no], [policy_start], [policy_end], [policy_insured_type], [policy_cover_prefix]
	--		, [certificate_no], [certificate_risk_id], [certificate_policy_id], [certificate_type], [risk_reg_no], [certificate_eff], [certificate_company_name]
	--		, [covernote_no], [covernote_eff], [covernote_expiry], [covernote_policy_id], [covernote_risk_id], [covernote_company_name], [imported]
	--		, [person_id], [peo_fname], [peo_mname], [peo_lname], [peo_trn]
	-- )

	-- SELECT        TOP (1) peo.PeopleID AS ID, 'people' AS [table], NULL AS CreatedBy, NULL AS company_enable, NULL AS company_iajid, NULL AS company_name, NULL AS company_type, NULL AS company_trn, NULL AS user_name, NULL 
 --                        AS user_company_name, NULL AS people_fname, NULL AS people_lname, NULL AS people_trn, NULL AS user_enabled, NULL AS vehicle_reg_no, NULL AS vehicle_chassis_no, NULL AS vehicle_make, NULL 
 --                        AS vehicle_model, NULL AS vehicle_under_pol, NULL AS vehicle_has_active_cover_note, NULL AS vehicle_has_cover_note_history, NULL AS policy_no, NULL AS policy_start, NULL AS policy_end, NULL 
 --                        AS policy_insured_type, NULL AS policy_cover_prefix, NULL AS certificate_no, NULL AS certificate_risk_id, NULL AS certificate_policy_id, NULL AS certificate_type, NULL AS risk_reg_no, NULL AS certificate_eff, NULL 
 --                        AS certificate_company_name, NULL AS covernote_no, NULL AS covernote_eff, NULL AS covernote_expiry, NULL AS covernote_policy_id, NULL AS covernote_risk_id, NULL AS covernote_company_name, NULL AS imported, 
 --                        peo.PeopleID AS person_id, peo.FName AS peo_fname, peo.MName AS peo_mname, peo.LName AS peo_lname, peo.TRN AS peo_trn
	--FROM            PolicyInsured AS ins LEFT OUTER JOIN
	--						 PeopleAddress AS pa ON ins.PersonID = pa.ID LEFT OUTER JOIN
	--						 People AS peo ON pa.PersonID = peo.PeopleID LEFT OUTER JOIN
	--						 Policies AS p ON ins.PolicyID = p.PolicyID LEFT OUTER JOIN
	--						 VehiclesUnderPolicy AS vup ON vup.PolicyID = p.PolicyID LEFT OUTER JOIN
	--						 VehicleMetaData AS vmd ON vmd.ID = vup.VehicleID LEFT OUTER JOIN
	--						 Vehicles AS v ON v.VehicleID = vmd.VehicleID
	--WHERE        (ins.PersonID IS NOT NULL)
	--          AND (p.CompanyID = @cid OR p.CompanyCreatedBy = @cid) 
	--		  AND (vmd.VehicleRegistrationNo = @Reg or v.ChassisNo = @Chassis)

---------------------------------------------------------------------------------------------------
--COMPANY DATA
 --INSERT INTO @SearchResults (
	--		  [ID]
 --           ,[table]
	--		, [CreatedBy]
	--		, [company_enable],[company_iajid], [company_name], [company_type], [company_trn]
	--		, [user_name],[user_company_name], [people_fname], [people_lname], [people_trn], [user_enabled]
	--		, [vehicle_reg_no], [vehicle_chassis_no], [vehicle_make], [vehicle_model], [vehicle_under_pol], [vehicle_has_active_cover_note], [vehicle_has_cover_note_history]
	--		, [policy_no], [policy_start], [policy_end], [policy_insured_type], [policy_cover_prefix]
	--		, [certificate_no], [certificate_risk_id], [certificate_policy_id], [certificate_type], [risk_reg_no], [certificate_eff], [certificate_company_name]
	--		, [covernote_no], [covernote_eff], [covernote_expiry], [covernote_policy_id], [covernote_risk_id], [covernote_company_name], [imported]
	--		, [person_id], [peo_fname], [peo_mname], [peo_lname], [peo_trn]
	-- )
	--SELECT CompanyID [ID], 'company' [table],--, CompanyName [name]  
	--		  CreatedBy [CreatedBy]
	--		, [Enabled] [company_enable], IAJID [company_iajid], CompanyName [company_name], ct.[Type] [company_type], c.TRN [company_trn] -- company
	--		, NULL [user_name], NULL [user_company_name], NULL [people_fname], NULL [people_lname], NULL [people_trn], NULL [user_enabled] --user
	--		, NULL [vehicle_reg_no], NULL [vehicle_chassis_no], NULL [vehicle_make], NULL [vehicle_model], NULL [vehicle_under_pol], NULL [vehicle_has_active_cover_note], NULL [vehicle_has_cover_note_history] -- vehicle
	--		, NULL [policy_no], NULL [policy_start], NULL [policy_end], NULL [policy_insured_type], NULL [policy_cover_prefix] --policy
	--		, NULL [certificate_no], NULL [certificate_risk_id], NULL [certificate_policy_id], NULL [certificate_type], NULL [risk_reg_no], NULL [certificate_eff_date], NULL [certificate_company_name] --certificate
	--		, NULL [covernote_no], NULL [covernote_eff], NULL [covernote_expiry], NULL [covernote_policy_id], NULL [covernote_risk_id], NULL [covernote_company_name], NULL [imported] --covernote
	--		, NULL [person_id], NULL [peo_fname], NULL [peo_mname], NULL [peo_lname], NULL [peo_trn] --people
	--	FROM Company c LEFT JOIN CompanyTypes ct ON c.[Type] = ct.ID
	--	WHERE 			
	--	  CompanyID = @cid OR CompanyInsurer = @cid OR IntermediaryId = @cid
		
---------------------------------------------------------------------------------------------------
END

-----------------------------------------------
--PERFORMS SEARCH FOR COMPANY AND RELATED DATA
-----------------------------------------------

IF(@_COMPANY = 1 and @cid <> 1)
BEGIN

 PRINT 'THIS IS COMPANY'
DECLARE @COMPANY_ID int;

DECLARE @COMPANYTABLE TABLE( CreatedBy int) 


DECLARE @PEOPLEIDS TABLE(PeopleID int);

INSERT INTO @PEOPLEIDS(PeopleID) 
SELECT PeopleID FROM People P WHERE  RTRIM(LTRIM(p.FName)) + ' ' + RTRIM(LTRIM(P.LName)) LIKE'%' + @Company + '%';
declare @c int;
select @c= count(peopleid) from @PEOPLEIDS
print  convert(varchar(10),@c)

--GET COMPANY ASSOCIATED USERS

SELECT @COMPANY_ID = COMPANYID FROM Company WHERE CompanyName = @Company;
---------------------------------------------------------------------------------------------------
--VEHICLE/RISK DATA

 --INSERT INTO @SearchResults (
	--		  [ID]
 --            ,[table]
	--		, [CreatedBy]
	--		, [company_enable],[company_iajid], [company_name], [company_type], [company_trn]
	--		, [user_name],[user_company_name], [people_fname], [people_lname], [people_trn], [user_enabled]
	--		, [vehicle_reg_no], [vehicle_chassis_no], [vehicle_make], [vehicle_model], [vehicle_under_pol], [vehicle_has_active_cover_note], [vehicle_has_cover_note_history]
	--		, [policy_no], [policy_start], [policy_end], [policy_insured_type], [policy_cover_prefix]
	--		, [certificate_no], [certificate_risk_id], [certificate_policy_id], [certificate_type], [risk_reg_no], [certificate_eff], [certificate_company_name]
	--		, [covernote_no], [covernote_eff], [covernote_expiry], [covernote_policy_id], [covernote_risk_id], [covernote_company_name], [imported]
	--		, [person_id], [peo_fname], [peo_mname], [peo_lname], [peo_trn]
	-- )
	--	SELECT vmd.ID [ID], 
	--	'vehicle' [table],
	--		  NULL [CreatedBy]
	--		, NULL [company_enable], NULL [company_iajid], NULL [company_name], NULL [company_type], NULL [company_trn] -- company
	--		, NULL [user_name], NULL [user_company_name], NULL [people_fname], NULL [people_lname], NULL [people_trn], NULL [user_enabled] --user
	--		, vmd.VehicleRegistrationNo [vehicle_reg_no], v.ChassisNo [vehicle_chassis_no], v.Make [vehicle_make], v.Model [vehicle_model] -- vehicle
	--		, (SELECT (CASE WHEN(COUNT(*)>0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) 
	--		   FROM Policies LEFT JOIN VehiclesUnderPolicy ON Policies.PolicyID = VehiclesUnderPolicy.PolicyID  
	--		   WHERE	VehiclesUnderPolicy.VehicleID IN (SELECT ID FROM VehicleMetaData WHERE VehicleID = v.VehicleID)
	--                AND VehiclesUnderPolicy.Cancelled != 1
	--				AND Policies.Deleted = 0
	--				AND (CURRENT_TIMESTAMP >= Policies.StartDateTime AND CURRENT_TIMESTAMP <= Policies.EndDateTime)) [vehicle_under_pol]
	--		, (SELECT  (CASE WHEN(COUNT(*) = 1) THEN CAST(1 as bit) ELSE CAST(0 as bit) END)
	--		   FROM VehicleCoverNote 
	--		   WHERE	RiskItemNo IN (SELECT ID FROM VehicleMetaData WHERE VehicleID = v.VehicleID)
	--			AND ((Cancelled IS NULL OR Cancelled = 0) AND approved= 1) 
	--			AND (CURRENT_TIMESTAMP >= EffectiveDate AND CURRENT_TIMESTAMP <= ExpiryDateTime)) [vehicle_has_active_cover_note]
	--		, (SELECT (CASE WHEN (Count(*) > 0) THEN CAST(1 as bit) ELSE CAST(0 as bit) END) 
	--		   FROM VehicleCoverNote  
	--		   WHERE VehicleCoverNote.RiskItemNo = vmd.ID) [vehicle_has_cover_note_history]
	--		, NULL [policy_no], NULL [policy_start], NULL [policy_end], NULL [policy_insured_type], NULL [policy_cover_prefix] --policy
	--		, NULL [certificate_no], NULL [certificate_risk_id], NULL [certificate_policy_id], NULL [certificate_type], NULL [risk_reg_no], NULL [certificate_eff_date], NULL [certificate_company_name] --certificate
	--		, NULL [covernote_no], NULL [covernote_eff], NULL [covernote_expiry], NULL [covernote_policy_id], NULL [covernote_risk_id], NULL [covernote_company_name], NULL [imported] --covernote
	--		, NULL [person_id], NULL [peo_fname], NULL [peo_mname], NULL [peo_lname], NULL [peo_trn] --people
	--	FROM Vehicles v LEFT JOIN VehicleMetaData vmd ON v.VehicleID = vmd.VehicleID AND vmd.[current] = 1
	--					--LEFT JOIN VehicleHandDriveLookUp hd ON vmd.HandDrive = hd.VHDLID
	--					LEFT JOIN Drivers d ON vmd.MainDriver = d.DriverID
	--					LEFT JOIN People p ON d.PersonID = p.PeopleID
	--					--LEFT JOIN TransmissionTypeLookUp trans ON vmd.TransmissionType = trans.TransmisionTypeID
	--					--LEFT JOIN VehiclesUnderPolicy vup ON vmd.ID = vup.VehicleID
	--					--LEFT JOIN Policies pol ON vup.PolicyID = pol.PolicyID
	--					--LEFT JOIN VehicleCertificates vc ON vc.PolicyID = pol.PolicyID
	

	--	WHERE (
	--	     p.PeopleID IN (SELECT PeopleID FROM @PEOPLEIDS)
	--		)
	--		-- SECURITY CHECK
	--	   AND (p.CompanyId = @cid OR p.IntermediaryId = @cid)
	--	   AND ((SELECT COUNT(*)
	--			FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
	--									RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
	--									LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
	--									LEFT JOIN Users u ON ass.UID = u.UID
	--			WHERE ((up.[Table] = 'Vehicles' AND up.[Action] = 'Read')  OR ug.UserGroup IN ('System Administrator')) AND u.UID = @uid) > 0)

---------------------------------------------------------------------------------------------------

--POLICY DATA
	
 --INSERT INTO @SearchResults (
	--		  [ID]
 --           ,[table]
	--		, [CreatedBy]
	--		, [company_enable],[company_iajid], [company_name], [company_type], [company_trn]
	--		, [user_name],[user_company_name], [people_fname], [people_lname], [people_trn], [user_enabled]
	--		, [vehicle_reg_no], [vehicle_chassis_no], [vehicle_make], [vehicle_model], [vehicle_under_pol], [vehicle_has_active_cover_note], [vehicle_has_cover_note_history]
	--		, [policy_no], [policy_start], [policy_end], [policy_insured_type], [policy_cover_prefix]
	--		, [certificate_no], [certificate_risk_id], [certificate_policy_id], [certificate_type], [risk_reg_no], [certificate_eff], [certificate_company_name]
	--		, [covernote_no], [covernote_eff], [covernote_expiry], [covernote_policy_id], [covernote_risk_id], [covernote_company_name], [imported]
	--		, [person_id], [peo_fname], [peo_mname], [peo_lname], [peo_trn]
	-- )
	--SELECT DISTINCT p.PolicyID [ID], 'policy' [table],
	--		  NULL [CreatedBy] 
	--		, NULL [company_enable], NULL [company_iajid], NULL [company_name], NULL [company_type], NULL [company_trn] -- company
	--		, NULL [user_name], NULL [user_company_name], NULL [people_fname], NULL [people_lname], NULL [people_trn], NULL [user_enabled] --user
	--		, NULL [vehicle_reg_no], NULL [vehicle_chassis_no], NULL [vehicle_make], NULL [vehicle_model], NULL [vehicle_under_pol], NULL [vehicle_has_active_cover_note], NULL [vehicle_has_cover_note_history] -- vehicle
	--		, p.Policyno [policy_no], p.StartDateTime [policy_start], p.EndDateTime [policy_end], pit.InsuredType [policy_insured_type], pc.Prefix [policy_cover_prefix] --policy
	--		, NULL [certificate_no], NULL [certificate_risk_id], NULL [certificate_policy_id], NULL [certificate_type], NULL [risk_reg_no], NULL [certificate_eff_date], NULL [certificate_company_name] --certificate
	--		, NULL [covernote_no], NULL [covernote_eff], NULL [covernote_expiry], NULL [covernote_policy_id], NULL [covernote_risk_id], NULL [covernote_company_name], NULL [imported] --covernote
	--		, NULL [person_id], NULL [peo_fname], NULL [peo_mname], NULL [peo_lname], NULL [peo_trn] --people
	--	FROM Policies p LEFT JOIN Company c ON p.CompanyID = c.CompanyID
	--					LEFT JOIN PolicyInsuredType pit ON p.InsuredType = pit.PITID
	--					LEFT JOIN PolicyCovers pc ON p.PolicyCover = pc.ID
	--					LEFT JOIN PolicyInsured ins ON p.PolicyID = ins.PolicyID
	--					LEFT OUTER JOIN PeopleAddress pa ON ins.PersonID = pa.ID
	--					LEFT OUTER JOIN CompanyAddress ca ON ins.CompanyID = ca.ID
	--					LEFT JOIN People inspe ON pa.PersonID = inspe.PeopleID
	--					--LEFT JOIN Company comp ON ca.CompanyID = comp.CompanyID
	--					LEFT JOIN VehiclesUnderPolicy vup ON p.PolicyID = vup.PolicyID
	--					LEFT JOIN VehicleMetaData vmd ON vup.VehicleID = vmd.ID
	--					LEFT JOIN Vehicles v ON vmd.VehicleID = v.VehicleID
	--					--LEFT JOIN ExceptedDrivers ecd ON v.VehicleID = ecd.VehicleID AND p.PolicyID = ecd.PolicyID
	--					--LEFT JOIN ExcludedDrivers exd ON v.VehicleID = exd.VehicleID AND p.PolicyID = exd.PolicyID
	--					--LEFT JOIN AuthorizedDrivers aut ON v.VehicleID = aut.VehicleID AND p.PolicyID = aut.PolicyID
	--					--LEFT JOIN VehicleHandDriveLookUp hd ON vmd.HandDrive = hd.VHDLID
	--					LEFT JOIN Drivers driv ON driv.DriverID = vmd.MainDriver 
	--					LEFT JOIN People pe ON vmd.MainDriver = pe.PeopleID
	--					--LEFT JOIN TransmissionTypeLookUp trans ON vmd.TransmissionType = trans.TransmisionTypeID
	--					LEFT OUTER JOIN VehicleCertificates vc ON p.PolicyID = vc.PolicyID
	--					LEFT OUTER JOIN VehicleCoverNote vcn ON p.PolicyID = vcn.PolicyID
		

	--	WHERE (
	--			p.CompanyID = @cid OR p.CompanyCreatedBy = @cid
	--		)

	
---------------------------------------------------------------------------------------------------

--CERTIFICATE DATA

 --INSERT INTO @SearchResults (
	--		  [ID]
 --           ,[table]
	--		, [CreatedBy]
	--		, [company_enable],[company_iajid], [company_name], [company_type], [company_trn]
	--		, [user_name],[user_company_name], [people_fname], [people_lname], [people_trn], [user_enabled]
	--		, [vehicle_reg_no], [vehicle_chassis_no], [vehicle_make], [vehicle_model], [vehicle_under_pol], [vehicle_has_active_cover_note], [vehicle_has_cover_note_history]
	--		, [policy_no], [policy_start], [policy_end], [policy_insured_type], [policy_cover_prefix]
	--		, [certificate_no], [certificate_risk_id], [certificate_policy_id], [certificate_type], [risk_reg_no], [certificate_eff], [certificate_company_name]
	--		, [covernote_no], [covernote_eff], [covernote_expiry], [covernote_policy_id], [covernote_risk_id], [covernote_company_name], [imported]
	--		, [person_id], [peo_fname], [peo_mname], [peo_lname], [peo_trn]
	-- )
	--	SELECT vc.VehicleCertificateID [ID], 'certificate' [table],
	--	      NULL [CreatedBy]
	--		, NULL [company_enable], NULL [company_iajid], NULL [company_name], NULL [company_type], NULL [company_trn] -- company
	--		, NULL [user_name], NULL [user_company_name], NULL [people_fname], NULL [people_lname], NULL [people_trn], NULL [user_enabled] --user
	--		, NULL [vehicle_reg_no], NULL [vehicle_chassis_no], NULL [vehicle_make], NULL [vehicle_model], NULL [vehicle_under_pol], NULL [vehicle_has_active_cover_note], NULL [vehicle_has_cover_note_history] -- vehicle
	--		, NULL [policy_no], NULL [policy_start], NULL [policy_end], NULL [policy_insured_type], NULL [policy_cover_prefix] --policy
	--		, vc.CertificateNo [certificate_no], vmd.ID [certificate_risk_id], p.PolicyID [certificate_policy_id], vc.CertificateType [certificate_type], vmd.VehicleRegistrationNo [risk_reg_no], vc.EffectiveDate [certificate_eff_date], c_ins.CompanyName [certificate_company_name]
	--		, NULL [covernote_no], NULL [covernote_eff], NULL [covernote_expiry], NULL [covernote_policy_id], NULL [covernote_risk_id], NULL [covernote_company_name], NULL [imported]
	--		, NULL [person_id], NULL [peo_fname], NULL [peo_mname], NULL [peo_lname], NULL [peo_trn] --people
	--	FROM VehicleCertificates vc LEFT JOIN Company c ON vc.CompanyID = c.CompanyID
	--								LEFT JOIN VehicleMetaData vmd ON vc.RiskItemNo = vmd.ID
	--								LEFT JOIN Vehicles v ON vmd.VehicleID = v.VehicleID
	--								LEFT JOIN Policies p ON vc.PolicyID = p.PolicyID
	--								LEFT JOIN Company c_ins ON p.CompanyID = c_ins.CompanyID
		

	--	WHERE 
	--	    vmd.VehicleRegistrationNo = @Reg
	--				 OR v.ChassisNo = @Chassis
		
	--	   -- SECURITY CHECK
	--	  AND 
	--	   (((vc.CompanyID = @cid OR vc.IntermediaryId = @cid) AND (SELECT COUNT(*)
	--								FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
	--														RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
	--														LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
	--														LEFT JOIN Users u ON ass.UID = u.UID
	--								WHERE up.[Table] = 'Certificates' AND up.[Action] = 'Read' AND u.UID = @uid) > 0)
									
	--		OR
	--		((SELECT COUNT(*) FROM UserGroups ug LEFT JOIN UserGroupUserAssoc uga ON ug.UGID = uga.UGID
	--		  									LEFT JOIN Users u ON uga.UID = u.UID
	--							WHERE u.UID = @uid AND ug.UserGroup IN ('System Administrator') AND ug.[system] = 1) > 0)
	--		)

---------------------------------------------------------------------------------------------------			

--COVER NOTE DATA
 --INSERT INTO @SearchResults (
	--		 [ID]
 --           ,[table]
	--		, [CreatedBy]
	--		, [company_enable],[company_iajid], [company_name], [company_type], [company_trn]
	--		, [user_name],[user_company_name], [people_fname], [people_lname], [people_trn], [user_enabled]
	--		, [vehicle_reg_no], [vehicle_chassis_no], [vehicle_make], [vehicle_model], [vehicle_under_pol], [vehicle_has_active_cover_note], [vehicle_has_cover_note_history]
	--		, [policy_no], [policy_start], [policy_end], [policy_insured_type], [policy_cover_prefix]
	--		, [certificate_no], [certificate_risk_id], [certificate_policy_id], [certificate_type], [risk_reg_no], [certificate_eff], [certificate_company_name]
	--		, [covernote_no], [covernote_eff], [covernote_expiry], [covernote_policy_id], [covernote_risk_id], [covernote_company_name], [imported]
	--		, [person_id], [peo_fname], [peo_mname], [peo_lname], [peo_trn]
	-- )
	-- -- SEARCH INTO COVER NOTES
	--	SELECT vcn.VehicleCoverNoteID [ID], 'covernote' [table],
	--		  NULL [CreatedBy]
	--		, NULL [company_enable], NULL [company_iajid], NULL [company_name], NULL [company_type], NULL [company_trn] -- company
	--		, NULL [user_name], NULL [user_company_name], NULL [people_fname], NULL [people_lname], NULL [people_trn], NULL [user_enabled] --user
	--		, NULL [vehicle_reg_no], NULL [vehicle_chassis_no], NULL [vehicle_make], NULL [vehicle_model], NULL [vehicle_under_pol], NULL [vehicle_has_active_cover_note], NULL [vehicle_has_cover_note_history] -- vehicle
	--		, NULL [policy_no], NULL [policy_start], NULL [policy_end], NULL [policy_insured_type], NULL [policy_cover_prefix] --policy
	--		, NULL [certificate_no], NULL [certificate_risk_id], NULL [certificate_policy_id], NULL [certificate_type], NULL [risk_reg_no], NULL [certificate_eff_date], NULL [certificate_company_name] --certificate
	--		, vcn.CoverNoteID [covernote_no], vcn.EffectiveDate [covernote_eff], vcn.ExpiryDateTime [covernote_expiry], p.PolicyID [covernote_policy_id], vmd.ID [covernote_risk_id], c_ins.CompanyName [covernote_company_name], p.imported [imported] --covernote
	--		, NULL [person_id], NULL [peo_fname], NULL [peo_mname], NULL [peo_lname], NULL [peo_trn] --people
	--	FROM VehicleCoverNote vcn LEFT JOIN Company c ON vcn.CompanyID = c.CompanyID
	--							  LEFT JOIN VehicleMetaData vmd ON vcn.RiskItemNo = vmd.ID
	--							  LEFT JOIN Vehicles v  ON vmd.VehicleID = v.VehicleID
	--							  LEFT JOIN Policies p ON vcn.PolicyID = p.PolicyID
	--							  LEFT JOIN Company c_ins ON p.CompanyID = c_ins.CompanyID
		

	--	WHERE (vcn.CompanyID = @cid OR vcn.IntermediaryId = @cid) AND
	--	(vmd.VehicleRegistrationNo = @Reg
	--	   OR v.VIN = @Chassis)
	--	   -- SECURITY CHECK
	--	   AND
	--	   (((c.CompanyID = @cid OR p.CompanyID = @cid OR p.CompanyCreatedBy = @cid) AND (SELECT COUNT(*)
	--								FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
	--														RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
	--														LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
	--														LEFT JOIN Users u ON ass.UID = u.UID
	--								WHERE ((up.[Table] = 'CoverNotes' AND up.[Action] = 'Read')) AND u.UID = @uid) > 0)
	--		OR
	--		((SELECT COUNT(*) FROM UserGroups ug LEFT JOIN UserGroupUserAssoc uga ON ug.UGID = uga.UGID
	--		  									LEFT JOIN Users u ON uga.UID = u.UID
	--							WHERE u.UID = @uid AND ug.UserGroup IN ('System Administrator')) > 0)
	--		)

---------------------------------------------------------------------------------------------------

--INSURED DATA
 --INSERT INTO @SearchResults (
	--		  [ID]
 --           ,[table]
	--		, [CreatedBy]
	--		, [company_enable],[company_iajid], [company_name], [company_type], [company_trn]
	--		, [user_name],[user_company_name], [people_fname], [people_lname], [people_trn], [user_enabled]
	--		, [vehicle_reg_no], [vehicle_chassis_no], [vehicle_make], [vehicle_model], [vehicle_under_pol], [vehicle_has_active_cover_note], [vehicle_has_cover_note_history]
	--		, [policy_no], [policy_start], [policy_end], [policy_insured_type], [policy_cover_prefix]
	--		, [certificate_no], [certificate_risk_id], [certificate_policy_id], [certificate_type], [risk_reg_no], [certificate_eff], [certificate_company_name]
	--		, [covernote_no], [covernote_eff], [covernote_expiry], [covernote_policy_id], [covernote_risk_id], [covernote_company_name], [imported]
	--		, [person_id], [peo_fname], [peo_mname], [peo_lname], [peo_trn]
	-- )
	--		SELECT DISTINCT(peo.PeopleID) [ID], 'people' [table],
	--			  NULL [CreatedBy]
	--			, NULL [company_enable], NULL [company_iajid], NULL [company_name], NULL [company_type], NULL [company_trn] -- company
	--			, NULL [user_name], NULL [user_company_name], NULL [people_fname], NULL [people_lname], NULL [people_trn], NULL [user_enabled] --user
	--			, NULL [vehicle_reg_no], NULL [vehicle_chassis_no], NULL [vehicle_make], NULL [vehicle_model], NULL [vehicle_under_pol], NULL [vehicle_has_active_cover_note], NULL [vehicle_has_cover_note_history] -- vehicle
	--			, NULL [policy_no], NULL [policy_start], NULL [policy_end], NULL [policy_insured_type], NULL [policy_cover_prefix] --policy
	--			, NULL [certificate_no], NULL [certificate_risk_id], NULL [certificate_policy_id], NULL [certificate_type], NULL [risk_reg_no], NULL [certificate_eff], NULL [certificate_company_name] --certificate
	--			, NULL [covernote_no], NULL [covernote_eff], NULL [covernote_expiry], NULL [covernote_policy_id], NULL [covernote_risk_id], NULL [covernote_company_name], NULL [imported] --covernote
	--			, peo.PeopleID [person_id], peo.FName [peo_fname], peo.MName [peo_mname], peo.LName [peo_lname], peo.TRN [peo_trn] --people
	--		FROM PolicyInsured ins	LEFT JOIN PeopleAddress pa ON ins.PersonID = pa.ID
	--								LEFT JOIN People peo ON pa.PersonID = peo.PeopleID
	--								LEFT JOIN Policies p ON ins.PolicyID = p.PolicyID
	--								--Recent Addition
	--							LEFT JOIN VehiclesUnderPolicy vup on vup.PolicyID = p.PolicyID
	--							LEFT JOIN VehicleMetaData vmd on vmd.ID = vup.VehicleID
	--							LEFT JOIN Vehicles v ON v.VehicleID = vmd.VehicleID
	--							--END
	--		WHERE (ins.PersonID IS NOT NULL)
	--			AND (
	--				ins.CompanyID = @cid OR ins.IntermediaryId = @cid)
				
				
				

---------------------------------------------------------------------------------------------------
--COMPANY DATA
 --INSERT INTO @SearchResults (
	--		  [ID]
 --           ,[table]
	--		, [CreatedBy]
	--		, [company_enable],[company_iajid], [company_name], [company_type], [company_trn]
	--		, [user_name],[user_company_name], [people_fname], [people_lname], [people_trn], [user_enabled]
	--		, [vehicle_reg_no], [vehicle_chassis_no], [vehicle_make], [vehicle_model], [vehicle_under_pol], [vehicle_has_active_cover_note], [vehicle_has_cover_note_history]
	--		, [policy_no], [policy_start], [policy_end], [policy_insured_type], [policy_cover_prefix]
	--		, [certificate_no], [certificate_risk_id], [certificate_policy_id], [certificate_type], [risk_reg_no], [certificate_eff], [certificate_company_name]
	--		, [covernote_no], [covernote_eff], [covernote_expiry], [covernote_policy_id], [covernote_risk_id], [covernote_company_name], [imported]
	--		, [person_id], [peo_fname], [peo_mname], [peo_lname], [peo_trn]
	-- )
	--SELECT CompanyID [ID], 'company' [table],--, CompanyName [name]  
	--		  CreatedBy [CreatedBy]
	--		, [Enabled] [company_enable], IAJID [company_iajid], CompanyName [company_name], ct.[Type] [company_type], c.TRN [company_trn] -- company
	--		, NULL [user_name], NULL [user_company_name], NULL [people_fname], NULL [people_lname], NULL [people_trn], NULL [user_enabled] --user
	--		, NULL [vehicle_reg_no], NULL [vehicle_chassis_no], NULL [vehicle_make], NULL [vehicle_model], NULL [vehicle_under_pol], NULL [vehicle_has_active_cover_note], NULL [vehicle_has_cover_note_history] -- vehicle
	--		, NULL [policy_no], NULL [policy_start], NULL [policy_end], NULL [policy_insured_type], NULL [policy_cover_prefix] --policy
	--		, NULL [certificate_no], NULL [certificate_risk_id], NULL [certificate_policy_id], NULL [certificate_type], NULL [risk_reg_no], NULL [certificate_eff_date], NULL [certificate_company_name] --certificate
	--		, NULL [covernote_no], NULL [covernote_eff], NULL [covernote_expiry], NULL [covernote_policy_id], NULL [covernote_risk_id], NULL [covernote_company_name], NULL [imported] --covernote
	--		, NULL [person_id], NULL [peo_fname], NULL [peo_mname], NULL [peo_lname], NULL [peo_trn] --people
	--	FROM Company c LEFT JOIN CompanyTypes ct ON c.[Type] = ct.ID
	--	WHERE 			
	--	CompanyName = @Company
		   
	--	 AND
	--	 --(c.CompanyID = @cid OR (c.CompanyID  IN (SELECT Intermediary FROM InsurerToIntermediary WHERE Insurer = @cid)) OR (c.CreatedBy IN (SELECT u.UID FROM Users u left join Employees e on u.EmployeeID = e.EmployeeID left join Company c on c.CompanyID = e.CompanyID WHERE c.CompanyID = @cid OR (c.CompanyID IN (SELECT Insurer FROM InsurerToIntermediary WHERE Intermediary = @cid))) AND c.[Type] IS NULL))
 --        (c.CompanyID = @cid OR c.CompanyInsurer = @cid or c.IntermediaryId = @cid)
	--	 AND c.[Type] is null		
	--	 AND
	--	  ((SELECT COUNT(*)
	--			FROM UserPermissions up LEFT JOIN PermissionGroupAssoc per ON up.UPID = per.UPID
	--									RIGHT JOIN UserGroups ug ON per.UGID = ug.UGID
	--									LEFT JOIN UserGroupUserAssoc ass ON ug.UGID = ass.UGID
	--									LEFT JOIN Users u ON ass.UID = u.UID
	--			WHERE ((up.[Table] = 'Company' AND up.[Action] = 'Read') OR ug.UserGroup IN ('System Administrator')) AND u.UID = @uid) > 0)

---------------------------------------------------------------------------------------------------

END


--RETURN SEARCH RESULTS
 SELECT * FROM @SearchResults i ORDER BY i.[table]

END
GO


