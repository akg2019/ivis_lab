declare @policyCursor CURSOR
declare @insuredCursor CURSOR
declare @insureds nvarchar(500)
declare @policyId int
declare @insuredId int

drop table #policy

select p.* into #policy from  Policies p join PolicyInsured ins on p.PolicyId = ins.policyId where ins.PolicyHolder is not null order by p.PolicyID

set @policyCursor = CURSOR for select distinct Policyid from #policy
OPEN @policyCursor FETCH NEXT from @policyCursor into @policyId

while @@FETCH_STATUS = 0
BEGIN
drop table #insureds
select @insureds = ''
print 'POLICY: '+ cast(@policyId as nvarchar(15))
select ins.* into #insureds from Policies p join PolicyInsured ins on p.PolicyID = ins.PolicyID where p.PolicyID = @policyId

set @insuredCursor = CURSOR for select distinct PolicyInsuredID from #insureds
OPEN @insuredCursor FETCH NEXT from @insuredCursor into @insuredId

WHILE @@FETCH_STATUS = 0
BEGIN
print 'Insured: ' + cast(@insuredId as nvarchar(15))

select @insureds = @insureds + ' and '+ (select PolicyHolder from #insureds where PolicyInsuredID = @insuredId)

FETCH NEXT FROM @insuredCursor into @insuredId
END

update Policies set policyHolder = @insureds where PolicyID = @policyId

FETCH NEXT FROM @policyCursor into @policyId
END

drop table #insureds
drop table #policy
